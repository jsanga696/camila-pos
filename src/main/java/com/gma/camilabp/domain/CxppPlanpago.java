/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb5
 */
@Entity
@Table(name = "CXPP_PLANPAGO")
@NamedQueries({
    @NamedQuery(name = "CxppPlanpago.findAll", query = "SELECT c FROM CxppPlanpago c")})
public class CxppPlanpago implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CxppPlanpagoPK cxppPlanpagoPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "CXPPLP_nombre")
    private String cXPPLPnombre;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CXPPLP_flaganticipo")
    private Character cXPPLPflaganticipo;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "CXPPLP_totaldias")
    private BigDecimal cXPPLPtotaldias;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CXPPLP_estado")
    private Character cXPPLPestado;

    public CxppPlanpago() {
    }

    public CxppPlanpago(CxppPlanpagoPK cxppPlanpagoPK) {
        this.cxppPlanpagoPK = cxppPlanpagoPK;
    }

    public CxppPlanpago(CxppPlanpagoPK cxppPlanpagoPK, String cXPPLPnombre, Character cXPPLPflaganticipo, BigDecimal cXPPLPtotaldias, Character cXPPLPestado) {
        this.cxppPlanpagoPK = cxppPlanpagoPK;
        this.cXPPLPnombre = cXPPLPnombre;
        this.cXPPLPflaganticipo = cXPPLPflaganticipo;
        this.cXPPLPtotaldias = cXPPLPtotaldias;
        this.cXPPLPestado = cXPPLPestado;
    }

    public CxppPlanpago(String gENCIAcodigo, String cXPPLPcodigo) {
        this.cxppPlanpagoPK = new CxppPlanpagoPK(gENCIAcodigo, cXPPLPcodigo);
    }

    public CxppPlanpagoPK getCxppPlanpagoPK() {
        return cxppPlanpagoPK;
    }

    public void setCxppPlanpagoPK(CxppPlanpagoPK cxppPlanpagoPK) {
        this.cxppPlanpagoPK = cxppPlanpagoPK;
    }

    public String getCXPPLPnombre() {
        return cXPPLPnombre;
    }

    public void setCXPPLPnombre(String cXPPLPnombre) {
        this.cXPPLPnombre = cXPPLPnombre;
    }

    public Character getCXPPLPflaganticipo() {
        return cXPPLPflaganticipo;
    }

    public void setCXPPLPflaganticipo(Character cXPPLPflaganticipo) {
        this.cXPPLPflaganticipo = cXPPLPflaganticipo;
    }

    public BigDecimal getCXPPLPtotaldias() {
        return cXPPLPtotaldias;
    }

    public void setCXPPLPtotaldias(BigDecimal cXPPLPtotaldias) {
        this.cXPPLPtotaldias = cXPPLPtotaldias;
    }

    public Character getCXPPLPestado() {
        return cXPPLPestado;
    }

    public void setCXPPLPestado(Character cXPPLPestado) {
        this.cXPPLPestado = cXPPLPestado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cxppPlanpagoPK != null ? cxppPlanpagoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CxppPlanpago)) {
            return false;
        }
        CxppPlanpago other = (CxppPlanpago) object;
        if ((this.cxppPlanpagoPK == null && other.cxppPlanpagoPK != null) || (this.cxppPlanpagoPK != null && !this.cxppPlanpagoPK.equals(other.cxppPlanpagoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CxppPlanpago[ cxppPlanpagoPK=" + cxppPlanpagoPK + " ]";
    }
    
}
