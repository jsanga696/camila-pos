/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb6
 */
@Entity
@Table(name = "CXCM_FORMAPAGO")
@NamedQueries({
    @NamedQuery(name = "CxcmFormapago.findAll", query = "SELECT c FROM CxcmFormapago c")})
public class CxcmFormapago implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CxcmFormapagoPK cxcmFormapagoPK;
    @Size(max = 30)
    @Column(name = "CXCFPG_descripcio")
    private String cXCFPGdescripcio;
    @Column(name = "CXCFPG_prioridad")
    private Integer cXCFPGprioridad;
    @Column(name = "CXCFPG_tipo")
    private Character cXCFPGtipo;
    @Column(name = "CXCFPG_estacuenta")
    private Character cXCFPGestacuenta;
    @Column(name = "CXCFPG_estado")
    private Character cXCFPGestado;
    @Column(name = "CXCFPG_tipodeposito")
    private Character cXCFPGtipodeposito;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "CXCFPG_desctofinan")
    private BigDecimal cXCFPGdesctofinan;
    @Column(name = "CXCFPG_credito")
    private Character cXCFPGcredito;
    /*
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cxcmFormapago", fetch = FetchType.LAZY)
    private Collection<CxcmCliente> cxcmClienteCollection;
*/
    public CxcmFormapago() {
    }

    public CxcmFormapago(CxcmFormapagoPK cxcmFormapagoPK) {
        this.cxcmFormapagoPK = cxcmFormapagoPK;
    }

    public CxcmFormapago(String cXCFPGcodigo, String gENCIAcodigo) {
        this.cxcmFormapagoPK = new CxcmFormapagoPK(cXCFPGcodigo, gENCIAcodigo);
    }

    public CxcmFormapagoPK getCxcmFormapagoPK() {
        return cxcmFormapagoPK;
    }

    public void setCxcmFormapagoPK(CxcmFormapagoPK cxcmFormapagoPK) {
        this.cxcmFormapagoPK = cxcmFormapagoPK;
    }

    public String getCXCFPGdescripcio() {
        return cXCFPGdescripcio;
    }

    public void setCXCFPGdescripcio(String cXCFPGdescripcio) {
        this.cXCFPGdescripcio = cXCFPGdescripcio;
    }

    public Integer getCXCFPGprioridad() {
        return cXCFPGprioridad;
    }

    public void setCXCFPGprioridad(Integer cXCFPGprioridad) {
        this.cXCFPGprioridad = cXCFPGprioridad;
    }

    public Character getCXCFPGtipo() {
        return cXCFPGtipo;
    }

    public void setCXCFPGtipo(Character cXCFPGtipo) {
        this.cXCFPGtipo = cXCFPGtipo;
    }

    public Character getCXCFPGestacuenta() {
        return cXCFPGestacuenta;
    }

    public void setCXCFPGestacuenta(Character cXCFPGestacuenta) {
        this.cXCFPGestacuenta = cXCFPGestacuenta;
    }

    public Character getCXCFPGestado() {
        return cXCFPGestado;
    }

    public void setCXCFPGestado(Character cXCFPGestado) {
        this.cXCFPGestado = cXCFPGestado;
    }

    public Character getCXCFPGtipodeposito() {
        return cXCFPGtipodeposito;
    }

    public void setCXCFPGtipodeposito(Character cXCFPGtipodeposito) {
        this.cXCFPGtipodeposito = cXCFPGtipodeposito;
    }

    public BigDecimal getCXCFPGdesctofinan() {
        return cXCFPGdesctofinan;
    }

    public void setCXCFPGdesctofinan(BigDecimal cXCFPGdesctofinan) {
        this.cXCFPGdesctofinan = cXCFPGdesctofinan;
    }

    public Character getCXCFPGcredito() {
        return cXCFPGcredito;
    }

    public void setCXCFPGcredito(Character cXCFPGcredito) {
        this.cXCFPGcredito = cXCFPGcredito;
    }
/*
    public Collection<CxcmCliente> getCxcmClienteCollection() {
        return cxcmClienteCollection;
    }

    public void setCxcmClienteCollection(Collection<CxcmCliente> cxcmClienteCollection) {
        this.cxcmClienteCollection = cxcmClienteCollection;
    }
*/
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cxcmFormapagoPK != null ? cxcmFormapagoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CxcmFormapago)) {
            return false;
        }
        CxcmFormapago other = (CxcmFormapago) object;
        if ((this.cxcmFormapagoPK == null && other.cxcmFormapagoPK != null) || (this.cxcmFormapagoPK != null && !this.cxcmFormapagoPK.equals(other.cxcmFormapagoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CxcmFormapago[ cxcmFormapagoPK=" + cxcmFormapagoPK + " ]";
    }
    
}
