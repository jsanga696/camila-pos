/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb5
 */
@Entity
@Table(name = "CXPP_TIPOSDOCUMENTOS")
@NamedQueries({
    @NamedQuery(name = "CxppTiposdocumentos.findAll", query = "SELECT c FROM CxppTiposdocumentos c")})
public class CxppTiposdocumentos implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CxppTiposdocumentosPK cxppTiposdocumentosPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "CXPTDO_nombre")
    private String cXPTDOnombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "CXPTDO_tipo")
    private String cXPTDOtipo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "CXPTDO_referenciacontable")
    private String cXPTDOreferenciacontable;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "CXPTDO_Transaccion")
    private String cXPTDOTransaccion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "CXPTDO_Subtransaccion")
    private String cXPTDOSubtransaccion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CXPTDO_flagiva")
    private Character cXPTDOflagiva;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CXPTDO_flagretencion")
    private Character cXPTDOflagretencion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CXPTDO_flagcentrocosto")
    private Character cXPTDOflagcentrocosto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CXPTDO_flagimportacion")
    private Character cXPTDOflagimportacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CXPTDO_flagcontabiliza")
    private Character cXPTDOflagcontabiliza;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CXPTDO_flagfactura")
    private Character cXPTDOflagfactura;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CXPTDO_flagreferenciacontable")
    private Character cXPTDOflagreferenciacontable;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CXPTDO_estado")
    private Character cXPTDOestado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CXPTDO_flagdetalle")
    private Character cXPTDOflagdetalle;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CXPTDO_flagcantidad")
    private Character cXPTDOflagcantidad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CXPTDO_flagprecio")
    private Character cXPTDOflagprecio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CXPTDO_flagEmision")
    private Character cXPTDOflagEmision;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CXPTDO_flagAplicacion")
    private Character cXPTDOflagAplicacion;

    public CxppTiposdocumentos() {
    }

    public CxppTiposdocumentos(CxppTiposdocumentosPK cxppTiposdocumentosPK) {
        this.cxppTiposdocumentosPK = cxppTiposdocumentosPK;
    }

    public CxppTiposdocumentos(CxppTiposdocumentosPK cxppTiposdocumentosPK, String cXPTDOnombre, String cXPTDOtipo, String cXPTDOreferenciacontable, String cXPTDOTransaccion, String cXPTDOSubtransaccion, Character cXPTDOflagiva, Character cXPTDOflagretencion, Character cXPTDOflagcentrocosto, Character cXPTDOflagimportacion, Character cXPTDOflagcontabiliza, Character cXPTDOflagfactura, Character cXPTDOflagreferenciacontable, Character cXPTDOestado, Character cXPTDOflagdetalle, Character cXPTDOflagcantidad, Character cXPTDOflagprecio, Character cXPTDOflagEmision, Character cXPTDOflagAplicacion) {
        this.cxppTiposdocumentosPK = cxppTiposdocumentosPK;
        this.cXPTDOnombre = cXPTDOnombre;
        this.cXPTDOtipo = cXPTDOtipo;
        this.cXPTDOreferenciacontable = cXPTDOreferenciacontable;
        this.cXPTDOTransaccion = cXPTDOTransaccion;
        this.cXPTDOSubtransaccion = cXPTDOSubtransaccion;
        this.cXPTDOflagiva = cXPTDOflagiva;
        this.cXPTDOflagretencion = cXPTDOflagretencion;
        this.cXPTDOflagcentrocosto = cXPTDOflagcentrocosto;
        this.cXPTDOflagimportacion = cXPTDOflagimportacion;
        this.cXPTDOflagcontabiliza = cXPTDOflagcontabiliza;
        this.cXPTDOflagfactura = cXPTDOflagfactura;
        this.cXPTDOflagreferenciacontable = cXPTDOflagreferenciacontable;
        this.cXPTDOestado = cXPTDOestado;
        this.cXPTDOflagdetalle = cXPTDOflagdetalle;
        this.cXPTDOflagcantidad = cXPTDOflagcantidad;
        this.cXPTDOflagprecio = cXPTDOflagprecio;
        this.cXPTDOflagEmision = cXPTDOflagEmision;
        this.cXPTDOflagAplicacion = cXPTDOflagAplicacion;
    }

    public CxppTiposdocumentos(String gENCIAcodigo, String cXPTDOcodigo) {
        this.cxppTiposdocumentosPK = new CxppTiposdocumentosPK(gENCIAcodigo, cXPTDOcodigo);
    }

    public CxppTiposdocumentosPK getCxppTiposdocumentosPK() {
        return cxppTiposdocumentosPK;
    }

    public void setCxppTiposdocumentosPK(CxppTiposdocumentosPK cxppTiposdocumentosPK) {
        this.cxppTiposdocumentosPK = cxppTiposdocumentosPK;
    }

    public String getCXPTDOnombre() {
        return cXPTDOnombre;
    }

    public void setCXPTDOnombre(String cXPTDOnombre) {
        this.cXPTDOnombre = cXPTDOnombre;
    }

    public String getCXPTDOtipo() {
        return cXPTDOtipo;
    }

    public void setCXPTDOtipo(String cXPTDOtipo) {
        this.cXPTDOtipo = cXPTDOtipo;
    }

    public String getCXPTDOreferenciacontable() {
        return cXPTDOreferenciacontable;
    }

    public void setCXPTDOreferenciacontable(String cXPTDOreferenciacontable) {
        this.cXPTDOreferenciacontable = cXPTDOreferenciacontable;
    }

    public String getCXPTDOTransaccion() {
        return cXPTDOTransaccion;
    }

    public void setCXPTDOTransaccion(String cXPTDOTransaccion) {
        this.cXPTDOTransaccion = cXPTDOTransaccion;
    }

    public String getCXPTDOSubtransaccion() {
        return cXPTDOSubtransaccion;
    }

    public void setCXPTDOSubtransaccion(String cXPTDOSubtransaccion) {
        this.cXPTDOSubtransaccion = cXPTDOSubtransaccion;
    }

    public Character getCXPTDOflagiva() {
        return cXPTDOflagiva;
    }

    public void setCXPTDOflagiva(Character cXPTDOflagiva) {
        this.cXPTDOflagiva = cXPTDOflagiva;
    }

    public Character getCXPTDOflagretencion() {
        return cXPTDOflagretencion;
    }

    public void setCXPTDOflagretencion(Character cXPTDOflagretencion) {
        this.cXPTDOflagretencion = cXPTDOflagretencion;
    }

    public Character getCXPTDOflagcentrocosto() {
        return cXPTDOflagcentrocosto;
    }

    public void setCXPTDOflagcentrocosto(Character cXPTDOflagcentrocosto) {
        this.cXPTDOflagcentrocosto = cXPTDOflagcentrocosto;
    }

    public Character getCXPTDOflagimportacion() {
        return cXPTDOflagimportacion;
    }

    public void setCXPTDOflagimportacion(Character cXPTDOflagimportacion) {
        this.cXPTDOflagimportacion = cXPTDOflagimportacion;
    }

    public Character getCXPTDOflagcontabiliza() {
        return cXPTDOflagcontabiliza;
    }

    public void setCXPTDOflagcontabiliza(Character cXPTDOflagcontabiliza) {
        this.cXPTDOflagcontabiliza = cXPTDOflagcontabiliza;
    }

    public Character getCXPTDOflagfactura() {
        return cXPTDOflagfactura;
    }

    public void setCXPTDOflagfactura(Character cXPTDOflagfactura) {
        this.cXPTDOflagfactura = cXPTDOflagfactura;
    }

    public Character getCXPTDOflagreferenciacontable() {
        return cXPTDOflagreferenciacontable;
    }

    public void setCXPTDOflagreferenciacontable(Character cXPTDOflagreferenciacontable) {
        this.cXPTDOflagreferenciacontable = cXPTDOflagreferenciacontable;
    }

    public Character getCXPTDOestado() {
        return cXPTDOestado;
    }

    public void setCXPTDOestado(Character cXPTDOestado) {
        this.cXPTDOestado = cXPTDOestado;
    }

    public Character getCXPTDOflagdetalle() {
        return cXPTDOflagdetalle;
    }

    public void setCXPTDOflagdetalle(Character cXPTDOflagdetalle) {
        this.cXPTDOflagdetalle = cXPTDOflagdetalle;
    }

    public Character getCXPTDOflagcantidad() {
        return cXPTDOflagcantidad;
    }

    public void setCXPTDOflagcantidad(Character cXPTDOflagcantidad) {
        this.cXPTDOflagcantidad = cXPTDOflagcantidad;
    }

    public Character getCXPTDOflagprecio() {
        return cXPTDOflagprecio;
    }

    public void setCXPTDOflagprecio(Character cXPTDOflagprecio) {
        this.cXPTDOflagprecio = cXPTDOflagprecio;
    }

    public Character getCXPTDOflagEmision() {
        return cXPTDOflagEmision;
    }

    public void setCXPTDOflagEmision(Character cXPTDOflagEmision) {
        this.cXPTDOflagEmision = cXPTDOflagEmision;
    }

    public Character getCXPTDOflagAplicacion() {
        return cXPTDOflagAplicacion;
    }

    public void setCXPTDOflagAplicacion(Character cXPTDOflagAplicacion) {
        this.cXPTDOflagAplicacion = cXPTDOflagAplicacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cxppTiposdocumentosPK != null ? cxppTiposdocumentosPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CxppTiposdocumentos)) {
            return false;
        }
        CxppTiposdocumentos other = (CxppTiposdocumentos) object;
        if ((this.cxppTiposdocumentosPK == null && other.cxppTiposdocumentosPK != null) || (this.cxppTiposdocumentosPK != null && !this.cxppTiposdocumentosPK.equals(other.cxppTiposdocumentosPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CxppTiposdocumentos[ cxppTiposdocumentosPK=" + cxppTiposdocumentosPK + " ]";
    }
    
}
