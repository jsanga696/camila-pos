/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb5
 */
@Entity
@Table(name = "CBPACT_SOLICITUD_EQUIPO")
@NamedQueries({
    @NamedQuery(name = "CbpactSolicitudEquipo.findAll", query = "SELECT c FROM CbpactSolicitudEquipo c")})
public class CbpactSolicitudEquipo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "fecha_ingreso")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaIngreso;
    @Column(name = "fecha_solicitud")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaSolicitud;
    @Column(name = "fecha_aprobacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaAprobacion;
    @Size(max = 300)
    @Column(name = "observacion")
    private String observacion;
    @Column(name = "usuario_ingreso_solicitud")
    private Long usuarioIngresoSolicitud;
    @Column(name = "usuario_aprobacion")
    private Long usuarioAprobacion;
    @Column(name = "estado")
    private Integer estado;
    @Column(name = "tipo_solicitud")
    private Integer tipoSolicitud;
    @Basic(optional = false)
    @Column(name = "cxcm_cliente_solicitante")
    private String cxcmClienteSolicitante;
    @JoinColumn(name = "equipo", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private CbpactEquipo equipo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "solicitud", fetch = FetchType.LAZY)
    private List<CbpactConsignacion> consignacionesList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "solicitud", fetch = FetchType.LAZY)
    private List<CbpactDevolucion> devolucionesList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "solicitud", fetch = FetchType.LAZY)
    private List<CbpactMantenimiento> mantenimientosList;
    
    @Transient
    private String tipoSolicitudText;
    
    @Transient
    private String nombreSolicitante;
    
    public CbpactSolicitudEquipo() {
        this.fechaSolicitud = new Date();
        this.estado = 1;
        this.tipoSolicitud = 1;
    }

    public CbpactSolicitudEquipo(Long id) {
        this.id = id;
    }

    public CbpactSolicitudEquipo(Long id, String solicitante) {
        this.id = id;
        this.cxcmClienteSolicitante = solicitante;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public Date getFechaSolicitud() {
        return fechaSolicitud;
    }

    public void setFechaSolicitud(Date fechaSolicitud) {
        this.fechaSolicitud = fechaSolicitud;
    }

    public Date getFechaAprobacion() {
        return fechaAprobacion;
    }

    public void setFechaAprobacion(Date fechaAprobacion) {
        this.fechaAprobacion = fechaAprobacion;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Long getUsuarioIngresoSolicitud() {
        return usuarioIngresoSolicitud;
    }

    public void setUsuarioIngresoSolicitud(Long usuarioIngresoSolicitud) {
        this.usuarioIngresoSolicitud = usuarioIngresoSolicitud;
    }

    public Long getUsuarioAprobacion() {
        return usuarioAprobacion;
    }

    public void setUsuarioAprobacion(Long usuarioAprobacion) {
        this.usuarioAprobacion = usuarioAprobacion;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public Integer getTipoSolicitud() {
        return tipoSolicitud;
    }

    public void setTipoSolicitud(Integer tipoSolicitud) {
        this.tipoSolicitud = tipoSolicitud;
    }

    public String getCxcmClienteSolicitante() {
        return cxcmClienteSolicitante;
    }

    public void setCxcmClienteSolicitante(String cxcmClienteSolicitante) {
        this.cxcmClienteSolicitante = cxcmClienteSolicitante;
    }

    public CbpactEquipo getEquipo() {
        return equipo;
    }

    public void setEquipo(CbpactEquipo equipo) {
        this.equipo = equipo;
    }

    public List<CbpactConsignacion> getConsignacionesList() {
        return consignacionesList;
    }

    public void setConsignacionesList(List<CbpactConsignacion> consignacionesList) {
        this.consignacionesList = consignacionesList;
    }

    public List<CbpactDevolucion> getDevolucionesList() {
        return devolucionesList;
    }

    public void setDevolucionesList(List<CbpactDevolucion> devolucionesList) {
        this.devolucionesList = devolucionesList;
    }

    public List<CbpactMantenimiento> getMantenimientosList() {
        return mantenimientosList;
    }

    public void setMantenimientosList(List<CbpactMantenimiento> mantenimientosList) {
        this.mantenimientosList = mantenimientosList;
    }

    public String getTipoSolicitudText() {
        if(tipoSolicitud != null){
            switch(tipoSolicitud){
                case 1:
                    return "CONSIGNACION";
                    
                case 2:
                    return "MANTENIMIENTO";
                    
                case 3:
                    
                    return "DEVOLUCION";
                    
                default:
                    return "";
            }
        }
        return tipoSolicitudText;
    }

    public void setTipoSolicitudText(String tipoSolicitudText) {
        this.tipoSolicitudText = tipoSolicitudText;
    }

    public String getNombreSolicitante() {
        return nombreSolicitante;
    }

    public void setNombreSolicitante(String nombreSolicitante) {
        this.nombreSolicitante = nombreSolicitante;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbpactSolicitudEquipo)) {
            return false;
        }
        CbpactSolicitudEquipo other = (CbpactSolicitudEquipo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CbpactSolicitudEquipo[ id=" + id + " ]";
    }
    
}
