/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb
 */
@Entity
@Table(name = "CBPGENR_BODEGA")
@NamedQueries({
    @NamedQuery(name = "CbpgenrBodega.findAll", query = "SELECT c FROM CbpgenrBodega c")})
public class CbpgenrBodega implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "genbod_codigo")
    private Long genbodCodigo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "genbod_codigo_origin")
    private long genbodCodigoOrigin;
    @Basic(optional = false)
    @NotNull
    @Column(name = "gencia_codigo")
    private long genciaCodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "genbod_secuencia")
    private String genbodSecuencia;
    @Basic(optional = false)
    @NotNull
    @Column(name = "gentbo_codigo")
    private long gentboCodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "genbod_nombre")
    private String genbodNombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "genbod_direccion")
    private String genbodDireccion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "genbod_numegreso")
    private int genbodNumegreso;
    @Basic(optional = false)
    @NotNull
    @Column(name = "genbod_numingreso")
    private int genbodNumingreso;
    @Basic(optional = false)
    @NotNull
    @Column(name = "genbod_comporta")
    private int genbodComporta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "genbod_tipobod")
    private int genbodTipobod;
    @Basic(optional = false)
    @NotNull
    @Column(name = "genbod_bodrelaci")
    private int genbodBodrelaci;
    @Basic(optional = false)
    @NotNull
    @Column(name = "gental_secuencia")
    private int gentalSecuencia;
    @Basic(optional = false)
    @NotNull
    @Column(name = "genbod_estado")
    private boolean genbodEstado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "genbod_creauser")
    private long genbodCreauser;
    @Basic(optional = false)
    @NotNull
    @Column(name = "genbod_creafecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date genbodCreafecha;
    @JoinColumn(name = "genofi_codigo", referencedColumnName = "genofi_codigo")
    @ManyToOne(optional = false)
    private CbpgenrOficina genofiCodigo;
    @Column(name = "FACSUP_codigo")
    private String facsupCodigo;
    //@Formula(value = "(SELECT s.FACSUP_nombre FROM FACR_SUPERVISOR s WHERE s.FACSUP_codigo=FACSUP_codigo)")
    @Transient
    private String supervisor;

    public CbpgenrBodega() {
        this.genbodSecuencia = "";
    }

    public CbpgenrBodega(Long genbodCodigo) {
        this.genbodCodigo = genbodCodigo;
    }

    public CbpgenrBodega(Long genbodCodigo, long genbodCodigoOrigin, long genciaCodigo, String genbodSecuencia, long gentboCodigo, String genbodNombre, String genbodDireccion, int genbodNumegreso, int genbodNumingreso, int genbodComporta, int genbodTipobod, int genbodBodrelaci, int gentalSecuencia, boolean genbodEstado, long genbodCreauser, Date genbodCreafecha) {
        this.genbodCodigo = genbodCodigo;
        this.genbodCodigoOrigin = genbodCodigoOrigin;
        this.genciaCodigo = genciaCodigo;
        this.genbodSecuencia = genbodSecuencia;
        this.gentboCodigo = gentboCodigo;
        this.genbodNombre = genbodNombre;
        this.genbodDireccion = genbodDireccion;
        this.genbodNumegreso = genbodNumegreso;
        this.genbodNumingreso = genbodNumingreso;
        this.genbodComporta = genbodComporta;
        this.genbodTipobod = genbodTipobod;
        this.genbodBodrelaci = genbodBodrelaci;
        this.gentalSecuencia = gentalSecuencia;
        this.genbodEstado = genbodEstado;
        this.genbodCreauser = genbodCreauser;
        this.genbodCreafecha = genbodCreafecha;
    }

    public Long getGenbodCodigo() {
        return genbodCodigo;
    }

    public void setGenbodCodigo(Long genbodCodigo) {
        this.genbodCodigo = genbodCodigo;
    }

    public long getGenbodCodigoOrigin() {
        return genbodCodigoOrigin;
    }

    public void setGenbodCodigoOrigin(long genbodCodigoOrigin) {
        this.genbodCodigoOrigin = genbodCodigoOrigin;
    }

    public long getGenciaCodigo() {
        return genciaCodigo;
    }

    public void setGenciaCodigo(long genciaCodigo) {
        this.genciaCodigo = genciaCodigo;
    }

    public String getGenbodSecuencia() {
        return genbodSecuencia;
    }

    public void setGenbodSecuencia(String genbodSecuencia) {
        this.genbodSecuencia = genbodSecuencia;
    }

    public long getGentboCodigo() {
        return gentboCodigo;
    }

    public void setGentboCodigo(long gentboCodigo) {
        this.gentboCodigo = gentboCodigo;
    }

    public String getGenbodNombre() {
        return genbodNombre;
    }

    public void setGenbodNombre(String genbodNombre) {
        this.genbodNombre = genbodNombre;
    }

    public String getGenbodDireccion() {
        return genbodDireccion;
    }

    public void setGenbodDireccion(String genbodDireccion) {
        this.genbodDireccion = genbodDireccion;
    }

    public int getGenbodNumegreso() {
        return genbodNumegreso;
    }

    public void setGenbodNumegreso(int genbodNumegreso) {
        this.genbodNumegreso = genbodNumegreso;
    }

    public int getGenbodNumingreso() {
        return genbodNumingreso;
    }

    public void setGenbodNumingreso(int genbodNumingreso) {
        this.genbodNumingreso = genbodNumingreso;
    }

    public int getGenbodComporta() {
        return genbodComporta;
    }

    public void setGenbodComporta(int genbodComporta) {
        this.genbodComporta = genbodComporta;
    }

    public int getGenbodTipobod() {
        return genbodTipobod;
    }

    public void setGenbodTipobod(int genbodTipobod) {
        this.genbodTipobod = genbodTipobod;
    }

    public int getGenbodBodrelaci() {
        return genbodBodrelaci;
    }

    public void setGenbodBodrelaci(int genbodBodrelaci) {
        this.genbodBodrelaci = genbodBodrelaci;
    }

    public int getGentalSecuencia() {
        return gentalSecuencia;
    }

    public void setGentalSecuencia(int gentalSecuencia) {
        this.gentalSecuencia = gentalSecuencia;
    }

    public boolean getGenbodEstado() {
        return genbodEstado;
    }

    public void setGenbodEstado(boolean genbodEstado) {
        this.genbodEstado = genbodEstado;
    }

    public long getGenbodCreauser() {
        return genbodCreauser;
    }

    public void setGenbodCreauser(long genbodCreauser) {
        this.genbodCreauser = genbodCreauser;
    }

    public Date getGenbodCreafecha() {
        return genbodCreafecha;
    }

    public void setGenbodCreafecha(Date genbodCreafecha) {
        this.genbodCreafecha = genbodCreafecha;
    }

    public CbpgenrOficina getGenofiCodigo() {
        return genofiCodigo;
    }

    public void setGenofiCodigo(CbpgenrOficina genofiCodigo) {
        this.genofiCodigo = genofiCodigo;
    }

    public String getFacsupCodigo() {
        return facsupCodigo;
    }

    public void setFacsupCodigo(String facsupCodigo) {
        this.facsupCodigo = facsupCodigo;
    }

    public String getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(String supervisor) {
        this.supervisor = supervisor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (genbodCodigo != null ? genbodCodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbpgenrBodega)) {
            return false;
        }
        CbpgenrBodega other = (CbpgenrBodega) object;
        if ((this.genbodCodigo == null && other.genbodCodigo != null) || (this.genbodCodigo != null && !this.genbodCodigo.equals(other.genbodCodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CbpgenrBodega[ genbodCodigo=" + genbodCodigo + " ]";
    }
    
}
