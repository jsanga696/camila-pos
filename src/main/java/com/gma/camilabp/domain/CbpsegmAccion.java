/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb
 */
@Entity
@Table(name = "CBPSEGM_ACCION")
@NamedQueries({
    @NamedQuery(name = "CbpsegmAccion.findAll", query = "SELECT c FROM CbpsegmAccion c")})
public class CbpsegmAccion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "segacc_codigo")
    private Long segaccCodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "segacc_secuencia")
    private String segaccSecuencia;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "segacc_descripcio")
    private String segaccDescripcio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "segacc_estado")
    private boolean segaccEstado;
    @Size(max = 200)
    @Column(name = "segacc_descripcio_amplia")
    private String segaccDescripcioAmplia;
    @Basic(optional = false)
    @NotNull
    @Column(name = "segacc_creafecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date segaccCreafecha;
    @Basic(optional = false)
    @NotNull
    @Column(name = "segacc_creauser")
    private long segaccCreauser;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "segaccCodigo")
    private Collection<CbpsegmPerfil> cbpsegmPerfilCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "segaccCodigo")
    private Collection<CbpsegmPerfilrestricion> cbpsegmPerfilrestricionCollection;
    @JoinColumn(name = "segopc_codigo", referencedColumnName = "segopc_codigo")
    @ManyToOne(optional = false)
    private CbpsegmOpcion segopcCodigo;

    public CbpsegmAccion() {
    }

    public CbpsegmAccion(Long segaccCodigo) {
        this.segaccCodigo = segaccCodigo;
    }

    public CbpsegmAccion(Long segaccCodigo, String segaccSecuencia, String segaccDescripcio, boolean segaccEstado, Date segaccCreafecha, long segaccCreauser) {
        this.segaccCodigo = segaccCodigo;
        this.segaccSecuencia = segaccSecuencia;
        this.segaccDescripcio = segaccDescripcio;
        this.segaccEstado = segaccEstado;
        this.segaccCreafecha = segaccCreafecha;
        this.segaccCreauser = segaccCreauser;
    }

    public Long getSegaccCodigo() {
        return segaccCodigo;
    }

    public void setSegaccCodigo(Long segaccCodigo) {
        this.segaccCodigo = segaccCodigo;
    }

    public String getSegaccSecuencia() {
        return segaccSecuencia;
    }

    public void setSegaccSecuencia(String segaccSecuencia) {
        this.segaccSecuencia = segaccSecuencia;
    }

    public String getSegaccDescripcio() {
        return segaccDescripcio;
    }

    public void setSegaccDescripcio(String segaccDescripcio) {
        this.segaccDescripcio = segaccDescripcio;
    }

    public boolean getSegaccEstado() {
        return segaccEstado;
    }

    public void setSegaccEstado(boolean segaccEstado) {
        this.segaccEstado = segaccEstado;
    }

    public String getSegaccDescripcioAmplia() {
        return segaccDescripcioAmplia;
    }

    public void setSegaccDescripcioAmplia(String segaccDescripcioAmplia) {
        this.segaccDescripcioAmplia = segaccDescripcioAmplia;
    }

    public Date getSegaccCreafecha() {
        return segaccCreafecha;
    }

    public void setSegaccCreafecha(Date segaccCreafecha) {
        this.segaccCreafecha = segaccCreafecha;
    }

    public long getSegaccCreauser() {
        return segaccCreauser;
    }

    public void setSegaccCreauser(long segaccCreauser) {
        this.segaccCreauser = segaccCreauser;
    }

    public Collection<CbpsegmPerfil> getCbpsegmPerfilCollection() {
        return cbpsegmPerfilCollection;
    }

    public void setCbpsegmPerfilCollection(Collection<CbpsegmPerfil> cbpsegmPerfilCollection) {
        this.cbpsegmPerfilCollection = cbpsegmPerfilCollection;
    }

    public Collection<CbpsegmPerfilrestricion> getCbpsegmPerfilrestricionCollection() {
        return cbpsegmPerfilrestricionCollection;
    }

    public void setCbpsegmPerfilrestricionCollection(Collection<CbpsegmPerfilrestricion> cbpsegmPerfilrestricionCollection) {
        this.cbpsegmPerfilrestricionCollection = cbpsegmPerfilrestricionCollection;
    }

    public CbpsegmOpcion getSegopcCodigo() {
        return segopcCodigo;
    }

    public void setSegopcCodigo(CbpsegmOpcion segopcCodigo) {
        this.segopcCodigo = segopcCodigo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (segaccCodigo != null ? segaccCodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbpsegmAccion)) {
            return false;
        }
        CbpsegmAccion other = (CbpsegmAccion) object;
        if ((this.segaccCodigo == null && other.segaccCodigo != null) || (this.segaccCodigo != null && !this.segaccCodigo.equals(other.segaccCodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CbpsegmAccion[ segaccCodigo=" + segaccCodigo + " ]";
    }
    
}
