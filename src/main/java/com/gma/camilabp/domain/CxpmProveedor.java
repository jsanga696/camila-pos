/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb5
 */
@Entity
@Table(name = "CXPM_PROVEEDOR")
@NamedQueries({
    @NamedQuery(name = "CxpmProveedor.findAll", query = "SELECT c FROM CxpmProveedor c")
    , @NamedQuery(name = "CxpmProveedor.findByGENCIAcodigo", query = "SELECT c FROM CxpmProveedor c WHERE c.cxpmProveedorPK.gENCIAcodigo = :gENCIAcodigo")
    , @NamedQuery(name = "CxpmProveedor.findByGENOFIcodigo", query = "SELECT c FROM CxpmProveedor c WHERE c.cxpmProveedorPK.gENOFIcodigo = :gENOFIcodigo")
    , @NamedQuery(name = "CxpmProveedor.findByCXPPROcodigo", query = "SELECT c FROM CxpmProveedor c WHERE c.cxpmProveedorPK.cXPPROcodigo = :cXPPROcodigo")
    , @NamedQuery(name = "CxpmProveedor.findByCXPPROrazonsocia", query = "SELECT c FROM CxpmProveedor c WHERE c.cXPPROrazonsocia = :cXPPROrazonsocia")
    , @NamedQuery(name = "CxpmProveedor.findByCXPPROruccedula", query = "SELECT c FROM CxpmProveedor c WHERE c.cXPPROruccedula = :cXPPROruccedula")
    , @NamedQuery(name = "CxpmProveedor.findByCXPPROtelefonos", query = "SELECT c FROM CxpmProveedor c WHERE c.cXPPROtelefonos = :cXPPROtelefonos")
    , @NamedQuery(name = "CxpmProveedor.findByCXPPROfaxes", query = "SELECT c FROM CxpmProveedor c WHERE c.cXPPROfaxes = :cXPPROfaxes")
    , @NamedQuery(name = "CxpmProveedor.findByCXPPROcelular", query = "SELECT c FROM CxpmProveedor c WHERE c.cXPPROcelular = :cXPPROcelular")
    , @NamedQuery(name = "CxpmProveedor.findByCXPPROdireccion", query = "SELECT c FROM CxpmProveedor c WHERE c.cXPPROdireccion = :cXPPROdireccion")
    , @NamedQuery(name = "CxpmProveedor.findByCXPPROnombcheque", query = "SELECT c FROM CxpmProveedor c WHERE c.cXPPROnombcheque = :cXPPROnombcheque")
    , @NamedQuery(name = "CxpmProveedor.findByCXPPROestado", query = "SELECT c FROM CxpmProveedor c WHERE c.cXPPROestado = :cXPPROestado")
    , @NamedQuery(name = "CxpmProveedor.findByCXPPROcupocredit", query = "SELECT c FROM CxpmProveedor c WHERE c.cXPPROcupocredit = :cXPPROcupocredit")
    , @NamedQuery(name = "CxpmProveedor.findByCXPPROdiascredit", query = "SELECT c FROM CxpmProveedor c WHERE c.cXPPROdiascredit = :cXPPROdiascredit")
    , @NamedQuery(name = "CxpmProveedor.findByCXPPROcomentario", query = "SELECT c FROM CxpmProveedor c WHERE c.cXPPROcomentario = :cXPPROcomentario")
    , @NamedQuery(name = "CxpmProveedor.findByCXPPROretencion", query = "SELECT c FROM CxpmProveedor c WHERE c.cXPPROretencion = :cXPPROretencion")
    , @NamedQuery(name = "CxpmProveedor.findByCXPPROcontribiva", query = "SELECT c FROM CxpmProveedor c WHERE c.cXPPROcontribiva = :cXPPROcontribiva")
    , @NamedQuery(name = "CxpmProveedor.findByCXPPROimprenta", query = "SELECT c FROM CxpmProveedor c WHERE c.cXPPROimprenta = :cXPPROimprenta")
    , @NamedQuery(name = "CxpmProveedor.findByCXPPROautorizacion", query = "SELECT c FROM CxpmProveedor c WHERE c.cXPPROautorizacion = :cXPPROautorizacion")
    , @NamedQuery(name = "CxpmProveedor.findByCXPPROtipoprovee", query = "SELECT c FROM CxpmProveedor c WHERE c.cXPPROtipoprovee = :cXPPROtipoprovee")
    , @NamedQuery(name = "CxpmProveedor.findByCXPPROrefcontabl", query = "SELECT c FROM CxpmProveedor c WHERE c.cXPPROrefcontabl = :cXPPROrefcontabl")
    , @NamedQuery(name = "CxpmProveedor.findByCXPPROcuentacont", query = "SELECT c FROM CxpmProveedor c WHERE c.cXPPROcuentacont = :cXPPROcuentacont")
    , @NamedQuery(name = "CxpmProveedor.findByCXPPROcontacto", query = "SELECT c FROM CxpmProveedor c WHERE c.cXPPROcontacto = :cXPPROcontacto")
    , @NamedQuery(name = "CxpmProveedor.findByCXPPROcuentaANT", query = "SELECT c FROM CxpmProveedor c WHERE c.cXPPROcuentaANT = :cXPPROcuentaANT")
    , @NamedQuery(name = "CxpmProveedor.findByCXPPROfechafactura", query = "SELECT c FROM CxpmProveedor c WHERE c.cXPPROfechafactura = :cXPPROfechafactura")
    , @NamedQuery(name = "CxpmProveedor.findByCXPPROfechancr", query = "SELECT c FROM CxpmProveedor c WHERE c.cXPPROfechancr = :cXPPROfechancr")
    , @NamedQuery(name = "CxpmProveedor.findByCXPPROestcodalt", query = "SELECT c FROM CxpmProveedor c WHERE c.cXPPROestcodalt = :cXPPROestcodalt")
    , @NamedQuery(name = "CxpmProveedor.findByCXPPROimprentaNCR", query = "SELECT c FROM CxpmProveedor c WHERE c.cXPPROimprentaNCR = :cXPPROimprentaNCR")
    , @NamedQuery(name = "CxpmProveedor.findByCXPPROautorizacionNCR", query = "SELECT c FROM CxpmProveedor c WHERE c.cXPPROautorizacionNCR = :cXPPROautorizacionNCR")
    , @NamedQuery(name = "CxpmProveedor.findByCXPPROcodserieFAC", query = "SELECT c FROM CxpmProveedor c WHERE c.cXPPROcodserieFAC = :cXPPROcodserieFAC")
    , @NamedQuery(name = "CxpmProveedor.findByCXPPROcodserieNCR", query = "SELECT c FROM CxpmProveedor c WHERE c.cXPPROcodserieNCR = :cXPPROcodserieNCR")
    , @NamedQuery(name = "CxpmProveedor.findByCXPPROcodestableciFAC", query = "SELECT c FROM CxpmProveedor c WHERE c.cXPPROcodestableciFAC = :cXPPROcodestableciFAC")
    , @NamedQuery(name = "CxpmProveedor.findByCXPPROcodestableciNCR", query = "SELECT c FROM CxpmProveedor c WHERE c.cXPPROcodestableciNCR = :cXPPROcodestableciNCR")
    , @NamedQuery(name = "CxpmProveedor.findByCXPPROcodclirela", query = "SELECT c FROM CxpmProveedor c WHERE c.cXPPROcodclirela = :cXPPROcodclirela")
    , @NamedQuery(name = "CxpmProveedor.findByCOMPROidcodigo", query = "SELECT c FROM CxpmProveedor c WHERE c.cOMPROidcodigo = :cOMPROidcodigo")
    , @NamedQuery(name = "CxpmProveedor.findByCXPPROafectainv", query = "SELECT c FROM CxpmProveedor c WHERE c.cXPPROafectainv = :cXPPROafectainv")
    , @NamedQuery(name = "CxpmProveedor.findByCXPPROtiporise", query = "SELECT c FROM CxpmProveedor c WHERE c.cXPPROtiporise = :cXPPROtiporise")
    , @NamedQuery(name = "CxpmProveedor.findByCXPPROproveegenerico", query = "SELECT c FROM CxpmProveedor c WHERE c.cXPPROproveegenerico = :cXPPROproveegenerico")
    , @NamedQuery(name = "CxpmProveedor.findByCXPPROcodprovgenerico", query = "SELECT c FROM CxpmProveedor c WHERE c.cXPPROcodprovgenerico = :cXPPROcodprovgenerico")
    , @NamedQuery(name = "CxpmProveedor.findByCXPPROnogeneraanexo", query = "SELECT c FROM CxpmProveedor c WHERE c.cXPPROnogeneraanexo = :cXPPROnogeneraanexo")
    , @NamedQuery(name = "CxpmProveedor.findByCXPPROmanejaconta", query = "SELECT c FROM CxpmProveedor c WHERE c.cXPPROmanejaconta = :cXPPROmanejaconta")
    , @NamedQuery(name = "CxpmProveedor.findByCXPPROmanejaliquid", query = "SELECT c FROM CxpmProveedor c WHERE c.cXPPROmanejaliquid = :cXPPROmanejaliquid")
    , @NamedQuery(name = "CxpmProveedor.findByCXPPROemail", query = "SELECT c FROM CxpmProveedor c WHERE c.cXPPROemail = :cXPPROemail")
    , @NamedQuery(name = "CxpmProveedor.findByCXPPROcuentaDFL", query = "SELECT c FROM CxpmProveedor c WHERE c.cXPPROcuentaDFL = :cXPPROcuentaDFL")
    , @NamedQuery(name = "CxpmProveedor.findByCXPPLPcodigo", query = "SELECT c FROM CxpmProveedor c WHERE c.cXPPLPcodigo = :cXPPLPcodigo")
    , @NamedQuery(name = "CxpmProveedor.findByCXPDOCfrmPago", query = "SELECT c FROM CxpmProveedor c WHERE c.cXPDOCfrmPago = :cXPDOCfrmPago")
    , @NamedQuery(name = "CxpmProveedor.findByCXPPROprorelaciATS", query = "SELECT c FROM CxpmProveedor c WHERE c.cXPPROprorelaciATS = :cXPPROprorelaciATS")
    , @NamedQuery(name = "CxpmProveedor.findByCXPPROPaislocalidad", query = "SELECT c FROM CxpmProveedor c WHERE c.cXPPROPaislocalidad = :cXPPROPaislocalidad")
    , @NamedQuery(name = "CxpmProveedor.findByCXPPROcodirela", query = "SELECT c FROM CxpmProveedor c WHERE c.cXPPROcodirela = :cXPPROcodirela")
    , @NamedQuery(name = "CxpmProveedor.findByCXPPROreferencia", query = "SELECT c FROM CxpmProveedor c WHERE c.cXPPROreferencia = :cXPPROreferencia")})
public class CxpmProveedor implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CxpmProveedorPK cxpmProveedorPK;
    @Size(max = 50)
    @Column(name = "CXPPRO_razonsocia")
    private String cXPPROrazonsocia;
    @Size(max = 13)
    @Column(name = "CXPPRO_ruccedula")
    private String cXPPROruccedula;
    @Size(max = 30)
    @Column(name = "CXPPRO_telefonos")
    private String cXPPROtelefonos;
    @Size(max = 30)
    @Column(name = "CXPPRO_faxes")
    private String cXPPROfaxes;
    @Size(max = 8)
    @Column(name = "CXPPRO_celular")
    private String cXPPROcelular;
    @Size(max = 50)
    @Column(name = "CXPPRO_direccion")
    private String cXPPROdireccion;
    @Size(max = 50)
    @Column(name = "CXPPRO_nombcheque")
    private String cXPPROnombcheque;
    @Column(name = "CXPPRO_estado")
    private Character cXPPROestado;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "CXPPRO_cupocredit")
    private BigDecimal cXPPROcupocredit;
    @Column(name = "CXPPRO_diascredit")
    private Short cXPPROdiascredit;
    @Size(max = 50)
    @Column(name = "CXPPRO_comentario")
    private String cXPPROcomentario;
    @Column(name = "CXPPRO_retencion")
    private Character cXPPROretencion;
    @Column(name = "CXPPRO_contribiva")
    private Character cXPPROcontribiva;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 13)
    @Column(name = "CXPPRO_imprenta")
    private String cXPPROimprenta;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 13)
    @Column(name = "CXPPRO_autorizacion")
    private String cXPPROautorizacion;
    @Column(name = "CXPPRO_tipoprovee")
    private Character cXPPROtipoprovee;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "CXPPRO_refcontabl")
    private String cXPPROrefcontabl;
    @Size(max = 20)
    @Column(name = "CXPPRO_cuentacont")
    private String cXPPROcuentacont;
    @Size(max = 50)
    @Column(name = "CXPPRO_contacto")
    private String cXPPROcontacto;
    @Size(max = 20)
    @Column(name = "CXPPRO_cuentaANT")
    private String cXPPROcuentaANT;
    @Column(name = "CXPPRO_fechafactura")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cXPPROfechafactura;
    @Column(name = "CXPPRO_fechancr")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cXPPROfechancr;
    @Column(name = "CXPPRO_estcodalt")
    private Character cXPPROestcodalt;
    @Size(max = 10)
    @Column(name = "CXPPRO_imprentaNCR")
    private String cXPPROimprentaNCR;
    @Size(max = 10)
    @Column(name = "CXPPRO_autorizacionNCR")
    private String cXPPROautorizacionNCR;
    @Size(max = 3)
    @Column(name = "CXPPRO_codserieFAC")
    private String cXPPROcodserieFAC;
    @Size(max = 3)
    @Column(name = "CXPPRO_codserieNCR")
    private String cXPPROcodserieNCR;
    @Size(max = 3)
    @Column(name = "CXPPRO_codestableciFAC")
    private String cXPPROcodestableciFAC;
    @Size(max = 3)
    @Column(name = "CXPPRO_codestableciNCR")
    private String cXPPROcodestableciNCR;
    @Size(max = 6)
    @Column(name = "CXPPRO_codclirela")
    private String cXPPROcodclirela;
    @Basic(optional = false)
    @NotNull
    @Column(name = "COMPRO_idcodigo")
    private int cOMPROidcodigo;
    @Size(max = 1)
    @Column(name = "CXPPRO_afectainv")
    private String cXPPROafectainv;
    @Size(max = 1)
    @Column(name = "CXPPRO_tiporise")
    private String cXPPROtiporise;
    @Size(max = 1)
    @Column(name = "CXPPRO_proveegenerico")
    private String cXPPROproveegenerico;
    @Size(max = 8)
    @Column(name = "CXPPRO_codprovgenerico")
    private String cXPPROcodprovgenerico;
    @Size(max = 1)
    @Column(name = "CXPPRO_nogeneraanexo")
    private String cXPPROnogeneraanexo;
    @Size(max = 1)
    @Column(name = "CXPPRO_manejaconta")
    private String cXPPROmanejaconta;
    @Size(max = 1)
    @Column(name = "CXPPRO_manejaliquid")
    private String cXPPROmanejaliquid;
    @Size(max = 200)
    @Column(name = "CXPPRO_email")
    private String cXPPROemail;
    @Size(max = 20)
    @Column(name = "CXPPRO_cuentaDFL")
    private String cXPPROcuentaDFL;
    @Size(max = 2)
    @Column(name = "CXPPLP_codigo")
    private String cXPPLPcodigo;
    @Size(max = 2)
    @Column(name = "CXPDOC_frmPago")
    private String cXPDOCfrmPago;
    @Size(max = 1)
    @Column(name = "CXPPRO_prorelaciATS")
    private String cXPPROprorelaciATS;
    @Size(max = 5)
    @Column(name = "CXPPRO_Paislocalidad")
    private String cXPPROPaislocalidad;
    @Size(max = 10)
    @Column(name = "CXPPRO_codirela")
    private String cXPPROcodirela;
    @Size(max = 10)
    @Column(name = "CXPPRO_referencia")
    private String cXPPROreferencia;

    public CxpmProveedor() {
        //this.cxpmProveedorPK = new CxpmProveedorPK();
    }

    public CxpmProveedor(CxpmProveedorPK cxpmProveedorPK) {
        this.cxpmProveedorPK = cxpmProveedorPK;
    }

    public CxpmProveedor(CxpmProveedorPK cxpmProveedorPK, String cXPPROimprenta, String cXPPROautorizacion, String cXPPROrefcontabl, int cOMPROidcodigo) {
        this.cxpmProveedorPK = cxpmProveedorPK;
        this.cXPPROimprenta = cXPPROimprenta;
        this.cXPPROautorizacion = cXPPROautorizacion;
        this.cXPPROrefcontabl = cXPPROrefcontabl;
        this.cOMPROidcodigo = cOMPROidcodigo;
    }

    public CxpmProveedor(String gENCIAcodigo, String gENOFIcodigo, String cXPPROcodigo) {
        this.cxpmProveedorPK = new CxpmProveedorPK(gENCIAcodigo, gENOFIcodigo, cXPPROcodigo);
    }

    public CxpmProveedorPK getCxpmProveedorPK() {
        return cxpmProveedorPK;
    }

    public void setCxpmProveedorPK(CxpmProveedorPK cxpmProveedorPK) {
        this.cxpmProveedorPK = cxpmProveedorPK;
    }

    public String getCXPPROrazonsocia() {
        return cXPPROrazonsocia;
    }

    public void setCXPPROrazonsocia(String cXPPROrazonsocia) {
        this.cXPPROrazonsocia = cXPPROrazonsocia;
    }

    public String getCXPPROruccedula() {
        return cXPPROruccedula;
    }

    public void setCXPPROruccedula(String cXPPROruccedula) {
        this.cXPPROruccedula = cXPPROruccedula;
    }

    public String getCXPPROtelefonos() {
        return cXPPROtelefonos;
    }

    public void setCXPPROtelefonos(String cXPPROtelefonos) {
        this.cXPPROtelefonos = cXPPROtelefonos;
    }

    public String getCXPPROfaxes() {
        return cXPPROfaxes;
    }

    public void setCXPPROfaxes(String cXPPROfaxes) {
        this.cXPPROfaxes = cXPPROfaxes;
    }

    public String getCXPPROcelular() {
        return cXPPROcelular;
    }

    public void setCXPPROcelular(String cXPPROcelular) {
        this.cXPPROcelular = cXPPROcelular;
    }

    public String getCXPPROdireccion() {
        return cXPPROdireccion;
    }

    public void setCXPPROdireccion(String cXPPROdireccion) {
        this.cXPPROdireccion = cXPPROdireccion;
    }

    public String getCXPPROnombcheque() {
        return cXPPROnombcheque;
    }

    public void setCXPPROnombcheque(String cXPPROnombcheque) {
        this.cXPPROnombcheque = cXPPROnombcheque;
    }

    public Character getCXPPROestado() {
        return cXPPROestado;
    }

    public void setCXPPROestado(Character cXPPROestado) {
        this.cXPPROestado = cXPPROestado;
    }

    public BigDecimal getCXPPROcupocredit() {
        return cXPPROcupocredit;
    }

    public void setCXPPROcupocredit(BigDecimal cXPPROcupocredit) {
        this.cXPPROcupocredit = cXPPROcupocredit;
    }

    public Short getCXPPROdiascredit() {
        return cXPPROdiascredit;
    }

    public void setCXPPROdiascredit(Short cXPPROdiascredit) {
        this.cXPPROdiascredit = cXPPROdiascredit;
    }

    public String getCXPPROcomentario() {
        return cXPPROcomentario;
    }

    public void setCXPPROcomentario(String cXPPROcomentario) {
        this.cXPPROcomentario = cXPPROcomentario;
    }

    public Character getCXPPROretencion() {
        return cXPPROretencion;
    }

    public void setCXPPROretencion(Character cXPPROretencion) {
        this.cXPPROretencion = cXPPROretencion;
    }

    public Character getCXPPROcontribiva() {
        return cXPPROcontribiva;
    }

    public void setCXPPROcontribiva(Character cXPPROcontribiva) {
        this.cXPPROcontribiva = cXPPROcontribiva;
    }

    public String getCXPPROimprenta() {
        return cXPPROimprenta;
    }

    public void setCXPPROimprenta(String cXPPROimprenta) {
        this.cXPPROimprenta = cXPPROimprenta;
    }

    public String getCXPPROautorizacion() {
        return cXPPROautorizacion;
    }

    public void setCXPPROautorizacion(String cXPPROautorizacion) {
        this.cXPPROautorizacion = cXPPROautorizacion;
    }

    public Character getCXPPROtipoprovee() {
        return cXPPROtipoprovee;
    }

    public void setCXPPROtipoprovee(Character cXPPROtipoprovee) {
        this.cXPPROtipoprovee = cXPPROtipoprovee;
    }

    public String getCXPPROrefcontabl() {
        return cXPPROrefcontabl;
    }

    public void setCXPPROrefcontabl(String cXPPROrefcontabl) {
        this.cXPPROrefcontabl = cXPPROrefcontabl;
    }

    public String getCXPPROcuentacont() {
        return cXPPROcuentacont;
    }

    public void setCXPPROcuentacont(String cXPPROcuentacont) {
        this.cXPPROcuentacont = cXPPROcuentacont;
    }

    public String getCXPPROcontacto() {
        return cXPPROcontacto;
    }

    public void setCXPPROcontacto(String cXPPROcontacto) {
        this.cXPPROcontacto = cXPPROcontacto;
    }

    public String getCXPPROcuentaANT() {
        return cXPPROcuentaANT;
    }

    public void setCXPPROcuentaANT(String cXPPROcuentaANT) {
        this.cXPPROcuentaANT = cXPPROcuentaANT;
    }

    public Date getCXPPROfechafactura() {
        return cXPPROfechafactura;
    }

    public void setCXPPROfechafactura(Date cXPPROfechafactura) {
        this.cXPPROfechafactura = cXPPROfechafactura;
    }

    public Date getCXPPROfechancr() {
        return cXPPROfechancr;
    }

    public void setCXPPROfechancr(Date cXPPROfechancr) {
        this.cXPPROfechancr = cXPPROfechancr;
    }

    public Character getCXPPROestcodalt() {
        return cXPPROestcodalt;
    }

    public void setCXPPROestcodalt(Character cXPPROestcodalt) {
        this.cXPPROestcodalt = cXPPROestcodalt;
    }

    public String getCXPPROimprentaNCR() {
        return cXPPROimprentaNCR;
    }

    public void setCXPPROimprentaNCR(String cXPPROimprentaNCR) {
        this.cXPPROimprentaNCR = cXPPROimprentaNCR;
    }

    public String getCXPPROautorizacionNCR() {
        return cXPPROautorizacionNCR;
    }

    public void setCXPPROautorizacionNCR(String cXPPROautorizacionNCR) {
        this.cXPPROautorizacionNCR = cXPPROautorizacionNCR;
    }

    public String getCXPPROcodserieFAC() {
        return cXPPROcodserieFAC;
    }

    public void setCXPPROcodserieFAC(String cXPPROcodserieFAC) {
        this.cXPPROcodserieFAC = cXPPROcodserieFAC;
    }

    public String getCXPPROcodserieNCR() {
        return cXPPROcodserieNCR;
    }

    public void setCXPPROcodserieNCR(String cXPPROcodserieNCR) {
        this.cXPPROcodserieNCR = cXPPROcodserieNCR;
    }

    public String getCXPPROcodestableciFAC() {
        return cXPPROcodestableciFAC;
    }

    public void setCXPPROcodestableciFAC(String cXPPROcodestableciFAC) {
        this.cXPPROcodestableciFAC = cXPPROcodestableciFAC;
    }

    public String getCXPPROcodestableciNCR() {
        return cXPPROcodestableciNCR;
    }

    public void setCXPPROcodestableciNCR(String cXPPROcodestableciNCR) {
        this.cXPPROcodestableciNCR = cXPPROcodestableciNCR;
    }

    public String getCXPPROcodclirela() {
        return cXPPROcodclirela;
    }

    public void setCXPPROcodclirela(String cXPPROcodclirela) {
        this.cXPPROcodclirela = cXPPROcodclirela;
    }

    public int getCOMPROidcodigo() {
        return cOMPROidcodigo;
    }

    public void setCOMPROidcodigo(int cOMPROidcodigo) {
        this.cOMPROidcodigo = cOMPROidcodigo;
    }

    public String getCXPPROafectainv() {
        return cXPPROafectainv;
    }

    public void setCXPPROafectainv(String cXPPROafectainv) {
        this.cXPPROafectainv = cXPPROafectainv;
    }

    public String getCXPPROtiporise() {
        return cXPPROtiporise;
    }

    public void setCXPPROtiporise(String cXPPROtiporise) {
        this.cXPPROtiporise = cXPPROtiporise;
    }

    public String getCXPPROproveegenerico() {
        return cXPPROproveegenerico;
    }

    public void setCXPPROproveegenerico(String cXPPROproveegenerico) {
        this.cXPPROproveegenerico = cXPPROproveegenerico;
    }

    public String getCXPPROcodprovgenerico() {
        return cXPPROcodprovgenerico;
    }

    public void setCXPPROcodprovgenerico(String cXPPROcodprovgenerico) {
        this.cXPPROcodprovgenerico = cXPPROcodprovgenerico;
    }

    public String getCXPPROnogeneraanexo() {
        return cXPPROnogeneraanexo;
    }

    public void setCXPPROnogeneraanexo(String cXPPROnogeneraanexo) {
        this.cXPPROnogeneraanexo = cXPPROnogeneraanexo;
    }

    public String getCXPPROmanejaconta() {
        return cXPPROmanejaconta;
    }

    public void setCXPPROmanejaconta(String cXPPROmanejaconta) {
        this.cXPPROmanejaconta = cXPPROmanejaconta;
    }

    public String getCXPPROmanejaliquid() {
        return cXPPROmanejaliquid;
    }

    public void setCXPPROmanejaliquid(String cXPPROmanejaliquid) {
        this.cXPPROmanejaliquid = cXPPROmanejaliquid;
    }

    public String getCXPPROemail() {
        return cXPPROemail;
    }

    public void setCXPPROemail(String cXPPROemail) {
        this.cXPPROemail = cXPPROemail;
    }

    public String getCXPPROcuentaDFL() {
        return cXPPROcuentaDFL;
    }

    public void setCXPPROcuentaDFL(String cXPPROcuentaDFL) {
        this.cXPPROcuentaDFL = cXPPROcuentaDFL;
    }

    public String getCXPPLPcodigo() {
        return cXPPLPcodigo;
    }

    public void setCXPPLPcodigo(String cXPPLPcodigo) {
        this.cXPPLPcodigo = cXPPLPcodigo;
    }

    public String getCXPDOCfrmPago() {
        return cXPDOCfrmPago;
    }

    public void setCXPDOCfrmPago(String cXPDOCfrmPago) {
        this.cXPDOCfrmPago = cXPDOCfrmPago;
    }

    public String getCXPPROprorelaciATS() {
        return cXPPROprorelaciATS;
    }

    public void setCXPPROprorelaciATS(String cXPPROprorelaciATS) {
        this.cXPPROprorelaciATS = cXPPROprorelaciATS;
    }

    public String getCXPPROPaislocalidad() {
        return cXPPROPaislocalidad;
    }

    public void setCXPPROPaislocalidad(String cXPPROPaislocalidad) {
        this.cXPPROPaislocalidad = cXPPROPaislocalidad;
    }

    public String getCXPPROcodirela() {
        return cXPPROcodirela;
    }

    public void setCXPPROcodirela(String cXPPROcodirela) {
        this.cXPPROcodirela = cXPPROcodirela;
    }

    public String getCXPPROreferencia() {
        return cXPPROreferencia;
    }

    public void setCXPPROreferencia(String cXPPROreferencia) {
        this.cXPPROreferencia = cXPPROreferencia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cxpmProveedorPK != null ? cxpmProveedorPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CxpmProveedor)) {
            return false;
        }
        CxpmProveedor other = (CxpmProveedor) object;
        if ((this.cxpmProveedorPK == null && other.cxpmProveedorPK != null) || (this.cxpmProveedorPK != null && !this.cxpmProveedorPK.equals(other.cxpmProveedorPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CxpmProveedor[ cxpmProveedorPK=" + cxpmProveedorPK + " ]";
    }
    
}
