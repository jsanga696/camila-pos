/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb2
 */
@Entity
@Table(name = "FACR_MOTVDEVOLU")
@NamedQueries({
    @NamedQuery(name = "FacrMotvdevolu.findAll", query = "SELECT f FROM FacrMotvdevolu f")})
public class FacrMotvdevolu implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected FacrMotvdevoluPK facrMotvdevoluPK;
    @Size(max = 50)
    @Column(name = "FACMDE_descripcio", length = 50)
    private String facmdeDescripcio;
    @Column(name = "FACMDE_estado")
    private String facmdeEstado;
    @Size(max = 1)
    @Column(name = "FACMDE_tipodevol", length = 1)
    private String facmdeTipodevol;

    public FacrMotvdevolu() {
    }

    public FacrMotvdevolu(FacrMotvdevoluPK facrMotvdevoluPK) {
        this.facrMotvdevoluPK = facrMotvdevoluPK;
    }

    public FacrMotvdevolu(String gENCIAcodigo, String gENMODcodigo, String fACMDEcodigo) {
        this.facrMotvdevoluPK = new FacrMotvdevoluPK(gENCIAcodigo, gENMODcodigo, fACMDEcodigo);
    }

    public FacrMotvdevoluPK getFacrMotvdevoluPK() {
        return facrMotvdevoluPK;
    }

    public void setFacrMotvdevoluPK(FacrMotvdevoluPK facrMotvdevoluPK) {
        this.facrMotvdevoluPK = facrMotvdevoluPK;
    }

    public String getFACMDEdescripcio() {
        return facmdeDescripcio;
    }

    public void setFACMDEdescripcio(String fACMDEdescripcio) {
        this.facmdeDescripcio = fACMDEdescripcio;
    }

    public String getFACMDEestado() {
        return facmdeEstado;
    }

    public void setFACMDEestado(String fACMDEestado) {
        this.facmdeEstado = fACMDEestado;
    }

    public String getFACMDEtipodevol() {
        return facmdeTipodevol;
    }

    public void setFACMDEtipodevol(String fACMDEtipodevol) {
        this.facmdeTipodevol = fACMDEtipodevol;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (facrMotvdevoluPK != null ? facrMotvdevoluPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FacrMotvdevolu)) {
            return false;
        }
        FacrMotvdevolu other = (FacrMotvdevolu) object;
        if ((this.facrMotvdevoluPK == null && other.facrMotvdevoluPK != null) || (this.facrMotvdevoluPK != null && !this.facrMotvdevoluPK.equals(other.facrMotvdevoluPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.FacrMotvdevolu[ facrMotvdevoluPK=" + facrMotvdevoluPK + " ]";
    }
    
}
