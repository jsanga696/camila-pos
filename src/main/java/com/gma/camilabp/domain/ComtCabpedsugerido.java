/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
//import org.hibernate.annotations.Formula;

/**
 *
 * @author userdb5
 */
@Entity
@Table(name = "COMT_CABPEDSUGERIDO")
@NamedQueries({
    @NamedQuery(name = "ComtCabpedsugerido.findAll", query = "SELECT c FROM ComtCabpedsugerido c")})
public class ComtCabpedsugerido implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "COMPDS_secuencia")
    private Long cOMPDSsecuencia;
    @Size(max = 4)
    @Column(name = "GENCIA_codigo")
    private String gENCIAcodigo;
    @Size(max = 4)
    @Column(name = "GENOFI_codigo")
    private String gENOFIcodigo;
    @Column(name = "COMPDS_numero")
    private Integer cOMPDSnumero;
    @Size(max = 100)
    @Column(name = "COMPDS_descripcion")
    private String cOMPDSdescripcion;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "COMPDS_monto")
    private BigDecimal cOMPDSmonto;
    @Column(name = "COMPDS_fechaemisi")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cOMPDSfechaemisi;
    @Size(max = 3)
    @Column(name = "SEGUSU_codigo")
    private String sEGUSUcodigo;
    @Size(max = 3)
    @Column(name = "SEGUSU_codigorecibe")
    private String sEGUSUcodigorecibe;
    @Column(name = "SEGUSU_fecharecibe")
    @Temporal(TemporalType.TIMESTAMP)
    private Date sEGUSUfecharecibe;
    @Size(max = 4)
    @Column(name = "COMPDS_codbodega")
    private String cOMPDScodbodega;
    @Column(name = "COMPDS_diasprocesado")
    private Integer cOMPDSdiasprocesado;
    @Size(max = 2)
    @Column(name = "COMPDS_nivelcons")
    private String cOMPDSnivelcons;
    @Size(max = 1)
    @Column(name = "COMPDS_estado")
    private String cOMPDSestado;
    @Size(max = 3)
    @Column(name = "SEGUSU_codigocierra")
    private String sEGUSUcodigocierra;
    @Column(name = "SEGUSU_fechacierra")
    @Temporal(TemporalType.TIMESTAMP)
    private Date sEGUSUfechacierra;
    @Size(max = 200)
    @Column(name = "COMPDS_rutafile")
    private String cOMPDSrutafile;
    @Size(max = 2)
    @Column(name = "COMPDS_nivelfiltro")
    private String cOMPDSnivelfiltro;
    @Size(max = 6)
    @Column(name = "COMPDS_entidfiltro")
    private String cOMPDSentidfiltro;
    @Size(max = 10)
    @Column(name = "CXPPRO_codigo")
    private String cXPPROcodigo;
    //@Formula(value = "(SELECT p.CXPPRO_razonsocia FROM CXPM_PROVEEDOR p WHERE p.GENCIA_codigo=GENCIA_codigo AND p.GENOFI_codigo=GENOFI_codigo AND p.CXPPRO_codigo=CXPPRO_codigo)")
    @Transient
    private String proveedor;
    //@Formula(value = "(SELECT b.genbod_nombre FROM CBPGENR_BODEGA b WHERE b.genbod_secuencia=COMPDS_codbodega)")
    @Transient
    private String bodega;

    public ComtCabpedsugerido() {
    }

    public ComtCabpedsugerido(Long cOMPDSsecuencia) {
        this.cOMPDSsecuencia = cOMPDSsecuencia;
    }

    public Long getCOMPDSsecuencia() {
        return cOMPDSsecuencia;
    }

    public void setCOMPDSsecuencia(Long cOMPDSsecuencia) {
        this.cOMPDSsecuencia = cOMPDSsecuencia;
    }

    public String getGENCIAcodigo() {
        return gENCIAcodigo;
    }

    public void setGENCIAcodigo(String gENCIAcodigo) {
        this.gENCIAcodigo = gENCIAcodigo;
    }

    public String getGENOFIcodigo() {
        return gENOFIcodigo;
    }

    public void setGENOFIcodigo(String gENOFIcodigo) {
        this.gENOFIcodigo = gENOFIcodigo;
    }

    public Integer getCOMPDSnumero() {
        return cOMPDSnumero;
    }

    public void setCOMPDSnumero(Integer cOMPDSnumero) {
        this.cOMPDSnumero = cOMPDSnumero;
    }

    public String getCOMPDSdescripcion() {
        return cOMPDSdescripcion;
    }

    public void setCOMPDSdescripcion(String cOMPDSdescripcion) {
        this.cOMPDSdescripcion = cOMPDSdescripcion;
    }

    public BigDecimal getCOMPDSmonto() {
        return cOMPDSmonto;
    }

    public void setCOMPDSmonto(BigDecimal cOMPDSmonto) {
        this.cOMPDSmonto = cOMPDSmonto;
    }

    public Date getCOMPDSfechaemisi() {
        return cOMPDSfechaemisi;
    }

    public void setCOMPDSfechaemisi(Date cOMPDSfechaemisi) {
        this.cOMPDSfechaemisi = cOMPDSfechaemisi;
    }

    public String getSEGUSUcodigo() {
        return sEGUSUcodigo;
    }

    public void setSEGUSUcodigo(String sEGUSUcodigo) {
        this.sEGUSUcodigo = sEGUSUcodigo;
    }

    public String getSEGUSUcodigorecibe() {
        return sEGUSUcodigorecibe;
    }

    public void setSEGUSUcodigorecibe(String sEGUSUcodigorecibe) {
        this.sEGUSUcodigorecibe = sEGUSUcodigorecibe;
    }

    public Date getSEGUSUfecharecibe() {
        return sEGUSUfecharecibe;
    }

    public void setSEGUSUfecharecibe(Date sEGUSUfecharecibe) {
        this.sEGUSUfecharecibe = sEGUSUfecharecibe;
    }

    public String getCOMPDScodbodega() {
        return cOMPDScodbodega;
    }

    public void setCOMPDScodbodega(String cOMPDScodbodega) {
        this.cOMPDScodbodega = cOMPDScodbodega;
    }

    public Integer getCOMPDSdiasprocesado() {
        return cOMPDSdiasprocesado;
    }

    public void setCOMPDSdiasprocesado(Integer cOMPDSdiasprocesado) {
        this.cOMPDSdiasprocesado = cOMPDSdiasprocesado;
    }

    public String getCOMPDSnivelcons() {
        return cOMPDSnivelcons;
    }

    public void setCOMPDSnivelcons(String cOMPDSnivelcons) {
        this.cOMPDSnivelcons = cOMPDSnivelcons;
    }

    public String getCOMPDSestado() {
        return cOMPDSestado;
    }

    public void setCOMPDSestado(String cOMPDSestado) {
        this.cOMPDSestado = cOMPDSestado;
    }

    public String getSEGUSUcodigocierra() {
        return sEGUSUcodigocierra;
    }

    public void setSEGUSUcodigocierra(String sEGUSUcodigocierra) {
        this.sEGUSUcodigocierra = sEGUSUcodigocierra;
    }

    public Date getSEGUSUfechacierra() {
        return sEGUSUfechacierra;
    }

    public void setSEGUSUfechacierra(Date sEGUSUfechacierra) {
        this.sEGUSUfechacierra = sEGUSUfechacierra;
    }

    public String getCOMPDSrutafile() {
        return cOMPDSrutafile;
    }

    public void setCOMPDSrutafile(String cOMPDSrutafile) {
        this.cOMPDSrutafile = cOMPDSrutafile;
    }

    public String getCOMPDSnivelfiltro() {
        return cOMPDSnivelfiltro;
    }

    public void setCOMPDSnivelfiltro(String cOMPDSnivelfiltro) {
        this.cOMPDSnivelfiltro = cOMPDSnivelfiltro;
    }

    public String getCOMPDSentidfiltro() {
        return cOMPDSentidfiltro;
    }

    public void setCOMPDSentidfiltro(String cOMPDSentidfiltro) {
        this.cOMPDSentidfiltro = cOMPDSentidfiltro;
    }

    public String getCXPPROcodigo() {
        return cXPPROcodigo;
    }

    public void setCXPPROcodigo(String cXPPROcodigo) {
        this.cXPPROcodigo = cXPPROcodigo;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    public String getBodega() {
        return bodega;
    }

    public void setBodega(String bodega) {
        this.bodega = bodega;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cOMPDSsecuencia != null ? cOMPDSsecuencia.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ComtCabpedsugerido)) {
            return false;
        }
        ComtCabpedsugerido other = (ComtCabpedsugerido) object;
        if ((this.cOMPDSsecuencia == null && other.cOMPDSsecuencia != null) || (this.cOMPDSsecuencia != null && !this.cOMPDSsecuencia.equals(other.cOMPDSsecuencia))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.ComtCabpedsugerido[ cOMPDSsecuencia=" + cOMPDSsecuencia + " ]";
    }
    
}
