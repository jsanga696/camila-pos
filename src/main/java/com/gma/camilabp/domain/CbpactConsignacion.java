/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb5
 */
@Entity
@Table(name = "CBPACT_CONSIGNACION")
@NamedQueries({
    @NamedQuery(name = "CbpactConsignacion.findAll", query = "SELECT c FROM CbpactConsignacion c")})
public class CbpactConsignacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @Size(min = 1, max = 30)
    @Column(name = "num_contrato")
    private String numContrato;
    @Column(name = "fecha_ingreso")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaIngreso;
    @Column(name = "fecha_consignacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaConsignacion;
    @Size(max = 300)
    @Column(name = "observacion")
    private String observacion;
    @Column(name = "estado")
    private Integer estado;
    @Column(name = "usuario_ingreso")
    private Long usuarioIngreso;
    @Column(name = "usuario_resolucion")
    private Long usuarioResolucion;
    @JoinColumn(name = "solicitud", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private CbpactSolicitudEquipo solicitud;

    public CbpactConsignacion() {
        this.observacion = "";
    }

    public CbpactConsignacion(Long id) {
        this.id = id;
    }

    public CbpactConsignacion(Long id, String numContrato) {
        this.id = id;
        this.numContrato = numContrato;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUsuarioIngreso() {
        return usuarioIngreso;
    }

    public void setUsuarioIngreso(Long usuarioIngreso) {
        this.usuarioIngreso = usuarioIngreso;
    }

    public Long getUsuarioResolucion() {
        return usuarioResolucion;
    }

    public void setUsuarioResolucion(Long usuarioResolucion) {
        this.usuarioResolucion = usuarioResolucion;
    }

    public CbpactSolicitudEquipo getSolicitud() {
        return solicitud;
    }

    public void setSolicitud(CbpactSolicitudEquipo solicitud) {
        this.solicitud = solicitud;
    }

    public String getNumContrato() {
        return numContrato;
    }

    public void setNumContrato(String numContrato) {
        this.numContrato = numContrato;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public Date getFechaConsignacion() {
        return fechaConsignacion;
    }

    public void setFechaConsignacion(Date fechaConsignacion) {
        this.fechaConsignacion = fechaConsignacion;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbpactConsignacion)) {
            return false;
        }
        CbpactConsignacion other = (CbpactConsignacion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CbpactConsignacion[ id=" + id + " ]";
    }

}
