/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author userdb5
 */
@Embeddable
public class ComtDetpedsugeridoPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "COMPDS_secuencia")
    private Long cOMPDSsecuencia;
    @Basic(optional = false)
    @NotNull
    @Column(name = "COMPDS_linea")
    private int cOMPDSlinea;

    public ComtDetpedsugeridoPK() {
    }

    public ComtDetpedsugeridoPK(Long cOMPDSsecuencia, int cOMPDSlinea) {
        this.cOMPDSsecuencia = cOMPDSsecuencia;
        this.cOMPDSlinea = cOMPDSlinea;
    }

    public Long getCOMPDSsecuencia() {
        return cOMPDSsecuencia;
    }

    public void setCOMPDSsecuencia(Long cOMPDSsecuencia) {
        this.cOMPDSsecuencia = cOMPDSsecuencia;
    }

    public int getCOMPDSlinea() {
        return cOMPDSlinea;
    }

    public void setCOMPDSlinea(int cOMPDSlinea) {
        this.cOMPDSlinea = cOMPDSlinea;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cOMPDSsecuencia != null ? cOMPDSsecuencia.hashCode() : 0);
        hash += (int) cOMPDSlinea;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ComtDetpedsugeridoPK)) {
            return false;
        }
        ComtDetpedsugeridoPK other = (ComtDetpedsugeridoPK) object;
        if (this.cOMPDSsecuencia != other.cOMPDSsecuencia) {
            return false;
        }
        if (this.cOMPDSlinea != other.cOMPDSlinea) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.ComtDetpedsugeridoPK[ cOMPDSsecuencia=" + cOMPDSsecuencia + ", cOMPDSlinea=" + cOMPDSlinea + " ]";
    }
    
}
