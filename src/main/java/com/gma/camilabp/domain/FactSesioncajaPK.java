/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb6
 */
@Embeddable
public class FactSesioncajaPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENCIA_codigo")
    private String gENCIAcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENOFI_codigo")
    private String gENOFIcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENCAJ_codigo")
    private String gENCAJcodigo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FACSCA_fecapertur")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fACSCAfecapertur;

    public FactSesioncajaPK() {
    }

    public FactSesioncajaPK(String gENCIAcodigo, String gENOFIcodigo, String gENCAJcodigo, Date fACSCAfecapertur) {
        this.gENCIAcodigo = gENCIAcodigo;
        this.gENOFIcodigo = gENOFIcodigo;
        this.gENCAJcodigo = gENCAJcodigo;
        this.fACSCAfecapertur = fACSCAfecapertur;
    }

    public String getGENCIAcodigo() {
        return gENCIAcodigo;
    }

    public void setGENCIAcodigo(String gENCIAcodigo) {
        this.gENCIAcodigo = gENCIAcodigo;
    }

    public String getGENOFIcodigo() {
        return gENOFIcodigo;
    }

    public void setGENOFIcodigo(String gENOFIcodigo) {
        this.gENOFIcodigo = gENOFIcodigo;
    }

    public String getGENCAJcodigo() {
        return gENCAJcodigo;
    }

    public void setGENCAJcodigo(String gENCAJcodigo) {
        this.gENCAJcodigo = gENCAJcodigo;
    }

    public Date getFACSCAfecapertur() {
        return fACSCAfecapertur;
    }

    public void setFACSCAfecapertur(Date fACSCAfecapertur) {
        this.fACSCAfecapertur = fACSCAfecapertur;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gENCIAcodigo != null ? gENCIAcodigo.hashCode() : 0);
        hash += (gENOFIcodigo != null ? gENOFIcodigo.hashCode() : 0);
        hash += (gENCAJcodigo != null ? gENCAJcodigo.hashCode() : 0);
        hash += (fACSCAfecapertur != null ? fACSCAfecapertur.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FactSesioncajaPK)) {
            return false;
        }
        FactSesioncajaPK other = (FactSesioncajaPK) object;
        if ((this.gENCIAcodigo == null && other.gENCIAcodigo != null) || (this.gENCIAcodigo != null && !this.gENCIAcodigo.equals(other.gENCIAcodigo))) {
            return false;
        }
        if ((this.gENOFIcodigo == null && other.gENOFIcodigo != null) || (this.gENOFIcodigo != null && !this.gENOFIcodigo.equals(other.gENOFIcodigo))) {
            return false;
        }
        if ((this.gENCAJcodigo == null && other.gENCAJcodigo != null) || (this.gENCAJcodigo != null && !this.gENCAJcodigo.equals(other.gENCAJcodigo))) {
            return false;
        }
        if ((this.fACSCAfecapertur == null && other.fACSCAfecapertur != null) || (this.fACSCAfecapertur != null && !this.fACSCAfecapertur.equals(other.fACSCAfecapertur))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.FactSesioncajaPK[ gENCIAcodigo=" + gENCIAcodigo + ", gENOFIcodigo=" + gENOFIcodigo + ", gENCAJcodigo=" + gENCAJcodigo + ", fACSCAfecapertur=" + fACSCAfecapertur + " ]";
    }
    
}
