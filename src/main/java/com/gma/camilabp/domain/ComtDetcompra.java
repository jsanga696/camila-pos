/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
//import org.hibernate.annotations.Formula;

/**
 *
 * @author userdb5
 */
@Entity
@Table(name = "COMT_DETCOMPRA")
@NamedQueries({
    @NamedQuery(name = "ComtDetcompra.findAll", query = "SELECT c FROM ComtDetcompra c")})
public class ComtDetcompra implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ComtDetcompraPK comtDetcompraPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "COMCCM_numdocumen")
    private int cOMCCMnumdocumen;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENCIA_codigo")
    private String gENCIAcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENOFI_codigo")
    private String gENOFIcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "GENMOD_codigo")
    private String gENMODcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "GENTDO_codigo")
    private String gENTDOcodigo;
    @Size(max = 3)
    @Column(name = "COMDCM_codbodega")
    private String cOMDCMcodbodega;
    @Size(max = 15)
    @Column(name = "INVITM_codigo")
    private String iNVITMcodigo;
    @Column(name = "COMDCM_numlinrela")
    private Integer cOMDCMnumlinrela;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "COMDCM_canticaja")
    private BigDecimal cOMDCMcanticaja;
    @Column(name = "COMDCM_cantiunida")
    private BigDecimal cOMDCMcantiunida;
    @Column(name = "COMDCM_cantifunci")
    private BigDecimal cOMDCMcantifunci;
    @Column(name = "COMDCM_cantfunpen")
    private BigDecimal cOMDCMcantfunpen;
    @Column(name = "COMDCM_cantfundev")
    private BigDecimal cOMDCMcantfundev;
    @Column(name = "COMDCM_cantegresa")
    private BigDecimal cOMDCMcantegresa;
    @Column(name = "COMDCM_cantfactu")
    private BigDecimal cOMDCMcantfactu;
    @Size(max = 10)
    @Column(name = "INVUPR_unibascomp")
    private String iNVUPRunibascomp;
    @Size(max = 10)
    @Column(name = "INVUPR_unirelcomp")
    private String iNVUPRunirelcomp;
    @Column(name = "COMDCM_unidembala")
    private BigDecimal cOMDCMunidembala;
    @Column(name = "COMDCM_peso")
    private BigDecimal cOMDCMpeso;
    @Column(name = "COMDCM_costomov")
    private BigDecimal cOMDCMcostomov;
    @Column(name = "COMDCM_preciomov")
    private BigDecimal cOMDCMpreciomov;
    @Column(name = "COMDCM_porcdescto")
    private BigDecimal cOMDCMporcdescto;
    @Column(name = "COMDCM_subtotamov")
    private BigDecimal cOMDCMsubtotamov;
    @Column(name = "COMDCM_desctomov")
    private BigDecimal cOMDCMdesctomov;
    @Column(name = "COMDCM_totimpumov")
    private BigDecimal cOMDCMtotimpumov;
    @Column(name = "COMDCM_otrosimpuestos")
    private BigDecimal cOMDCMotrosimpuestos;
    @Column(name = "COMDCM_netomov")
    private BigDecimal cOMDCMnetomov;
    @Column(name = "COMDCM_volumen")
    private Long cOMDCMvolumen;
    @Column(name = "CXPICA_codclase")
    private Integer cXPICAcodclase;
    @Size(max = 15)
    @Column(name = "COMDCM_codimpICE")
    private String cOMDCMcodimpICE;
    @Column(name = "COMDCM_valimpICE")
    private BigDecimal cOMDCMvalimpICE;
    //@Formula(value = "(SELECT i.INVITM_nombre FROM INVM_ITEM i WHERE i.INVITM_codigo=INVITM_codigo)")
    @Transient
    private String nombreItem;
    
    @Transient
    private InvmItem producto;

    public ComtDetcompra() {
    }

    public ComtDetcompra(ComtDetcompraPK comtDetcompraPK) {
        this.comtDetcompraPK = comtDetcompraPK;
    }

    public ComtDetcompra(ComtDetcompraPK comtDetcompraPK, int cOMCCMnumdocumen, String gENCIAcodigo, String gENOFIcodigo, String gENMODcodigo, String gENTDOcodigo) {
        this.comtDetcompraPK = comtDetcompraPK;
        this.cOMCCMnumdocumen = cOMCCMnumdocumen;
        this.gENCIAcodigo = gENCIAcodigo;
        this.gENOFIcodigo = gENOFIcodigo;
        this.gENMODcodigo = gENMODcodigo;
        this.gENTDOcodigo = gENTDOcodigo;
    }

    public ComtDetcompra(Long cOMCCMnumsecucial, int cOMDCMnumlinea) {
        this.comtDetcompraPK = new ComtDetcompraPK(cOMCCMnumsecucial, cOMDCMnumlinea);
    }

    public ComtDetcompraPK getComtDetcompraPK() {
        return comtDetcompraPK;
    }

    public void setComtDetcompraPK(ComtDetcompraPK comtDetcompraPK) {
        this.comtDetcompraPK = comtDetcompraPK;
    }

    public int getCOMCCMnumdocumen() {
        return cOMCCMnumdocumen;
    }

    public void setCOMCCMnumdocumen(int cOMCCMnumdocumen) {
        this.cOMCCMnumdocumen = cOMCCMnumdocumen;
    }

    public String getGENCIAcodigo() {
        return gENCIAcodigo;
    }

    public void setGENCIAcodigo(String gENCIAcodigo) {
        this.gENCIAcodigo = gENCIAcodigo;
    }

    public String getGENOFIcodigo() {
        return gENOFIcodigo;
    }

    public void setGENOFIcodigo(String gENOFIcodigo) {
        this.gENOFIcodigo = gENOFIcodigo;
    }

    public String getGENMODcodigo() {
        return gENMODcodigo;
    }

    public void setGENMODcodigo(String gENMODcodigo) {
        this.gENMODcodigo = gENMODcodigo;
    }

    public String getGENTDOcodigo() {
        return gENTDOcodigo;
    }

    public void setGENTDOcodigo(String gENTDOcodigo) {
        this.gENTDOcodigo = gENTDOcodigo;
    }

    public String getCOMDCMcodbodega() {
        return cOMDCMcodbodega;
    }

    public void setCOMDCMcodbodega(String cOMDCMcodbodega) {
        this.cOMDCMcodbodega = cOMDCMcodbodega;
    }

    public String getINVITMcodigo() {
        return iNVITMcodigo;
    }

    public void setINVITMcodigo(String iNVITMcodigo) {
        this.iNVITMcodigo = iNVITMcodigo;
    }

    public Integer getCOMDCMnumlinrela() {
        return cOMDCMnumlinrela;
    }

    public void setCOMDCMnumlinrela(Integer cOMDCMnumlinrela) {
        this.cOMDCMnumlinrela = cOMDCMnumlinrela;
    }

    public BigDecimal getCOMDCMcanticaja() {
        return cOMDCMcanticaja;
    }

    public void setCOMDCMcanticaja(BigDecimal cOMDCMcanticaja) {
        this.cOMDCMcanticaja = cOMDCMcanticaja;
    }

    public BigDecimal getCOMDCMcantiunida() {
        return cOMDCMcantiunida;
    }

    public void setCOMDCMcantiunida(BigDecimal cOMDCMcantiunida) {
        this.cOMDCMcantiunida = cOMDCMcantiunida;
    }

    public BigDecimal getCOMDCMcantifunci() {
        return cOMDCMcantifunci;
    }

    public void setCOMDCMcantifunci(BigDecimal cOMDCMcantifunci) {
        this.cOMDCMcantifunci = cOMDCMcantifunci;
    }

    public BigDecimal getCOMDCMcantfunpen() {
        return cOMDCMcantfunpen;
    }

    public void setCOMDCMcantfunpen(BigDecimal cOMDCMcantfunpen) {
        this.cOMDCMcantfunpen = cOMDCMcantfunpen;
    }

    public BigDecimal getCOMDCMcantfundev() {
        return cOMDCMcantfundev;
    }

    public void setCOMDCMcantfundev(BigDecimal cOMDCMcantfundev) {
        this.cOMDCMcantfundev = cOMDCMcantfundev;
    }

    public BigDecimal getCOMDCMcantegresa() {
        return cOMDCMcantegresa;
    }

    public void setCOMDCMcantegresa(BigDecimal cOMDCMcantegresa) {
        this.cOMDCMcantegresa = cOMDCMcantegresa;
    }

    public BigDecimal getCOMDCMcantfactu() {
        return cOMDCMcantfactu;
    }

    public void setCOMDCMcantfactu(BigDecimal cOMDCMcantfactu) {
        this.cOMDCMcantfactu = cOMDCMcantfactu;
    }

    public String getINVUPRunibascomp() {
        return iNVUPRunibascomp;
    }

    public void setINVUPRunibascomp(String iNVUPRunibascomp) {
        this.iNVUPRunibascomp = iNVUPRunibascomp;
    }

    public String getINVUPRunirelcomp() {
        return iNVUPRunirelcomp;
    }

    public void setINVUPRunirelcomp(String iNVUPRunirelcomp) {
        this.iNVUPRunirelcomp = iNVUPRunirelcomp;
    }

    public BigDecimal getCOMDCMunidembala() {
        return cOMDCMunidembala;
    }

    public void setCOMDCMunidembala(BigDecimal cOMDCMunidembala) {
        this.cOMDCMunidembala = cOMDCMunidembala;
    }

    public BigDecimal getCOMDCMpeso() {
        return cOMDCMpeso;
    }

    public void setCOMDCMpeso(BigDecimal cOMDCMpeso) {
        this.cOMDCMpeso = cOMDCMpeso;
    }

    public BigDecimal getCOMDCMcostomov() {
        return cOMDCMcostomov;
    }

    public void setCOMDCMcostomov(BigDecimal cOMDCMcostomov) {
        this.cOMDCMcostomov = cOMDCMcostomov;
    }

    public BigDecimal getCOMDCMpreciomov() {
        return cOMDCMpreciomov;
    }

    public void setCOMDCMpreciomov(BigDecimal cOMDCMpreciomov) {
        this.cOMDCMpreciomov = cOMDCMpreciomov;
    }

    public BigDecimal getCOMDCMporcdescto() {
        return cOMDCMporcdescto;
    }

    public void setCOMDCMporcdescto(BigDecimal cOMDCMporcdescto) {
        this.cOMDCMporcdescto = cOMDCMporcdescto;
    }

    public BigDecimal getCOMDCMsubtotamov() {
        return cOMDCMsubtotamov;
    }

    public void setCOMDCMsubtotamov(BigDecimal cOMDCMsubtotamov) {
        this.cOMDCMsubtotamov = cOMDCMsubtotamov;
    }

    public BigDecimal getCOMDCMdesctomov() {
        return cOMDCMdesctomov;
    }

    public void setCOMDCMdesctomov(BigDecimal cOMDCMdesctomov) {
        this.cOMDCMdesctomov = cOMDCMdesctomov;
    }

    public BigDecimal getCOMDCMtotimpumov() {
        return cOMDCMtotimpumov;
    }

    public void setCOMDCMtotimpumov(BigDecimal cOMDCMtotimpumov) {
        this.cOMDCMtotimpumov = cOMDCMtotimpumov;
    }

    public BigDecimal getCOMDCMotrosimpuestos() {
        return cOMDCMotrosimpuestos;
    }

    public void setCOMDCMotrosimpuestos(BigDecimal cOMDCMotrosimpuestos) {
        this.cOMDCMotrosimpuestos = cOMDCMotrosimpuestos;
    }

    public BigDecimal getCOMDCMnetomov() {
        return cOMDCMnetomov;
    }

    public void setCOMDCMnetomov(BigDecimal cOMDCMnetomov) {
        this.cOMDCMnetomov = cOMDCMnetomov;
    }

    public Long getCOMDCMvolumen() {
        return cOMDCMvolumen;
    }

    public void setCOMDCMvolumen(Long cOMDCMvolumen) {
        this.cOMDCMvolumen = cOMDCMvolumen;
    }

    public Integer getCXPICAcodclase() {
        return cXPICAcodclase;
    }

    public void setCXPICAcodclase(Integer cXPICAcodclase) {
        this.cXPICAcodclase = cXPICAcodclase;
    }

    public String getCOMDCMcodimpICE() {
        return cOMDCMcodimpICE;
    }

    public void setCOMDCMcodimpICE(String cOMDCMcodimpICE) {
        this.cOMDCMcodimpICE = cOMDCMcodimpICE;
    }

    public BigDecimal getCOMDCMvalimpICE() {
        return cOMDCMvalimpICE;
    }

    public void setCOMDCMvalimpICE(BigDecimal cOMDCMvalimpICE) {
        this.cOMDCMvalimpICE = cOMDCMvalimpICE;
    }

    public InvmItem getProducto() {
        return producto;
    }

    public void setProducto(InvmItem producto) {
        this.producto = producto;
    }

    public String getNombreItem() {
        return nombreItem;
    }

    public void setNombreItem(String nombreItem) {
        this.nombreItem = nombreItem;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (comtDetcompraPK != null ? comtDetcompraPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ComtDetcompra)) {
            return false;
        }
        ComtDetcompra other = (ComtDetcompra) object;
        if ((this.comtDetcompraPK == null && other.comtDetcompraPK != null) || (this.comtDetcompraPK != null && !this.comtDetcompraPK.equals(other.comtDetcompraPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.ComtDetcompra[ comtDetcompraPK=" + comtDetcompraPK + " ]";
    }
    
}
