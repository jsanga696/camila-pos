/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb
 */
@Entity
@Table(name = "CBPGENR_COMPANIA")
@NamedQueries({
    @NamedQuery(name = "CbpgenrCompania.findAll", query = "SELECT c FROM CbpgenrCompania c")})
public class CbpgenrCompania implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "gencia_codigo")
    private Long genciaCodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "gencia_secuencia")
    private String genciaSecuencia;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "gencia_nombre")
    private String genciaNombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "gencia_direccion")
    private String genciaDireccion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "gencia_nombrecomercial")
    private String genciaNombrecomercial;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 13)
    @Column(name = "gencia_ruc")
    private String genciaRuc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "gencia_telefono")
    private String genciaTelefono;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "gencia_email")
    private String genciaEmail;
    @Basic(optional = false)
    @NotNull
    @Column(name = "genpai_codigo")
    private long genpaiCodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "gencia_reprelegal")
    private String genciaReprelegal;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "gencia_ciudad")
    private String genciaCiudad;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "gencia_numresolusion")
    private String genciaNumresolusion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "gencia_nomidentifica")
    private String genciaNomidentifica;
    @Size(max = 15)
    @Column(name = "gencia_eticxcnv01")
    private String genciaEticxcnv01;
    @Size(max = 15)
    @Column(name = "gencia_eticxcnv02")
    private String genciaEticxcnv02;
    @Size(max = 15)
    @Column(name = "gencia_eticxcnv03")
    private String genciaEticxcnv03;
    @Size(max = 15)
    @Column(name = "gencia_eticxcnv04")
    private String genciaEticxcnv04;
    @Size(max = 15)
    @Column(name = "gencia_eticxcnv05")
    private String genciaEticxcnv05;
    @Size(max = 15)
    @Column(name = "gencia_eticxcnv06")
    private String genciaEticxcnv06;
    @Size(max = 15)
    @Column(name = "gencia_eticxcnv07")
    private String genciaEticxcnv07;
    @Size(max = 15)
    @Column(name = "gencia_eticxcnv08")
    private String genciaEticxcnv08;
    @Size(max = 15)
    @Column(name = "gencia_eticxcnv09")
    private String genciaEticxcnv09;
    @Size(max = 15)
    @Column(name = "gencia_etiinvnv01")
    private String genciaEtiinvnv01;
    @Size(max = 15)
    @Column(name = "gencia_etiinvnv02")
    private String genciaEtiinvnv02;
    @Size(max = 15)
    @Column(name = "gencia_etiinvnv03")
    private String genciaEtiinvnv03;
    @Size(max = 15)
    @Column(name = "gencia_etiinvnv04")
    private String genciaEtiinvnv04;
    @Size(max = 15)
    @Column(name = "gencia_etiinvnv05")
    private String genciaEtiinvnv05;
    @Size(max = 50)
    @Column(name = "gencia_etiinvnv06")
    private String genciaEtiinvnv06;
    @Size(max = 50)
    @Column(name = "gencia_etiinvnv07")
    private String genciaEtiinvnv07;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "gencia_basefactur")
    private BigDecimal genciaBasefactur;
    @Column(name = "gencia_ultnivzona")
    private Short genciaUltnivzona;
    @Column(name = "gencia_ultnivinve")
    private Short genciaUltnivinve;
    @Column(name = "gencia_secposteo")
    private Long genciaSecposteo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "gencia_bndajusloc")
    private BigDecimal genciaBndajusloc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "gencia_bndajusbal")
    private BigDecimal genciaBndajusbal;
    @Column(name = "gencia_estado")
    private Boolean genciaEstado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "gencia_creafecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date genciaCreafecha;
    @Basic(optional = false)
    @NotNull
    @Column(name = "gencia_creauser")
    private long genciaCreauser;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 250)
    @Column(name = "gencia_rutalogo")
    private String genciaRutalogo;

    public CbpgenrCompania() {
    }

    public CbpgenrCompania(Long genciaCodigo) {
        this.genciaCodigo = genciaCodigo;
    }

    public CbpgenrCompania(Long genciaCodigo, String genciaSecuencia, String genciaNombre, String genciaDireccion, String genciaNombrecomercial, String genciaRuc, String genciaTelefono, String genciaEmail, long genpaiCodigo, String genciaReprelegal, String genciaCiudad, String genciaNumresolusion, String genciaNomidentifica, BigDecimal genciaBndajusloc, BigDecimal genciaBndajusbal, Date genciaCreafecha, long genciaCreauser, String genciaRutalogo) {
        this.genciaCodigo = genciaCodigo;
        this.genciaSecuencia = genciaSecuencia;
        this.genciaNombre = genciaNombre;
        this.genciaDireccion = genciaDireccion;
        this.genciaNombrecomercial = genciaNombrecomercial;
        this.genciaRuc = genciaRuc;
        this.genciaTelefono = genciaTelefono;
        this.genciaEmail = genciaEmail;
        this.genpaiCodigo = genpaiCodigo;
        this.genciaReprelegal = genciaReprelegal;
        this.genciaCiudad = genciaCiudad;
        this.genciaNumresolusion = genciaNumresolusion;
        this.genciaNomidentifica = genciaNomidentifica;
        this.genciaBndajusloc = genciaBndajusloc;
        this.genciaBndajusbal = genciaBndajusbal;
        this.genciaCreafecha = genciaCreafecha;
        this.genciaCreauser = genciaCreauser;
        this.genciaRutalogo = genciaRutalogo;
    }

    public Long getGenciaCodigo() {
        return genciaCodigo;
    }

    public void setGenciaCodigo(Long genciaCodigo) {
        this.genciaCodigo = genciaCodigo;
    }

    public String getGenciaSecuencia() {
        return genciaSecuencia;
    }

    public void setGenciaSecuencia(String genciaSecuencia) {
        this.genciaSecuencia = genciaSecuencia;
    }

    public String getGenciaNombre() {
        return genciaNombre;
    }

    public void setGenciaNombre(String genciaNombre) {
        this.genciaNombre = genciaNombre;
    }

    public String getGenciaDireccion() {
        return genciaDireccion;
    }

    public void setGenciaDireccion(String genciaDireccion) {
        this.genciaDireccion = genciaDireccion;
    }

    public String getGenciaNombrecomercial() {
        return genciaNombrecomercial;
    }

    public void setGenciaNombrecomercial(String genciaNombrecomercial) {
        this.genciaNombrecomercial = genciaNombrecomercial;
    }
    
    public String getGenciaRuc() {
        return genciaRuc;
    }

    public void setGenciaRuc(String genciaRuc) {
        this.genciaRuc = genciaRuc;
    }

    public String getGenciaTelefono() {
        return genciaTelefono;
    }

    public void setGenciaTelefono(String genciaTelefono) {
        this.genciaTelefono = genciaTelefono;
    }

    public String getGenciaEmail() {
        return genciaEmail;
    }

    public void setGenciaEmail(String genciaEmail) {
        this.genciaEmail = genciaEmail;
    }

    public long getGenpaiCodigo() {
        return genpaiCodigo;
    }

    public void setGenpaiCodigo(long genpaiCodigo) {
        this.genpaiCodigo = genpaiCodigo;
    }

    public String getGenciaReprelegal() {
        return genciaReprelegal;
    }

    public void setGenciaReprelegal(String genciaReprelegal) {
        this.genciaReprelegal = genciaReprelegal;
    }

    public String getGenciaCiudad() {
        return genciaCiudad;
    }

    public void setGenciaCiudad(String genciaCiudad) {
        this.genciaCiudad = genciaCiudad;
    }

    public String getGenciaNumresolusion() {
        return genciaNumresolusion;
    }

    public void setGenciaNumresolusion(String genciaNumresolusion) {
        this.genciaNumresolusion = genciaNumresolusion;
    }

    public String getGenciaNomidentifica() {
        return genciaNomidentifica;
    }

    public void setGenciaNomidentifica(String genciaNomidentifica) {
        this.genciaNomidentifica = genciaNomidentifica;
    }

    public String getGenciaEticxcnv01() {
        return genciaEticxcnv01;
    }

    public void setGenciaEticxcnv01(String genciaEticxcnv01) {
        this.genciaEticxcnv01 = genciaEticxcnv01;
    }

    public String getGenciaEticxcnv02() {
        return genciaEticxcnv02;
    }

    public void setGenciaEticxcnv02(String genciaEticxcnv02) {
        this.genciaEticxcnv02 = genciaEticxcnv02;
    }

    public String getGenciaEticxcnv03() {
        return genciaEticxcnv03;
    }

    public void setGenciaEticxcnv03(String genciaEticxcnv03) {
        this.genciaEticxcnv03 = genciaEticxcnv03;
    }

    public String getGenciaEticxcnv04() {
        return genciaEticxcnv04;
    }

    public void setGenciaEticxcnv04(String genciaEticxcnv04) {
        this.genciaEticxcnv04 = genciaEticxcnv04;
    }

    public String getGenciaEticxcnv05() {
        return genciaEticxcnv05;
    }

    public void setGenciaEticxcnv05(String genciaEticxcnv05) {
        this.genciaEticxcnv05 = genciaEticxcnv05;
    }

    public String getGenciaEticxcnv06() {
        return genciaEticxcnv06;
    }

    public void setGenciaEticxcnv06(String genciaEticxcnv06) {
        this.genciaEticxcnv06 = genciaEticxcnv06;
    }

    public String getGenciaEticxcnv07() {
        return genciaEticxcnv07;
    }

    public void setGenciaEticxcnv07(String genciaEticxcnv07) {
        this.genciaEticxcnv07 = genciaEticxcnv07;
    }

    public String getGenciaEticxcnv08() {
        return genciaEticxcnv08;
    }

    public void setGenciaEticxcnv08(String genciaEticxcnv08) {
        this.genciaEticxcnv08 = genciaEticxcnv08;
    }

    public String getGenciaEticxcnv09() {
        return genciaEticxcnv09;
    }

    public void setGenciaEticxcnv09(String genciaEticxcnv09) {
        this.genciaEticxcnv09 = genciaEticxcnv09;
    }

    public String getGenciaEtiinvnv01() {
        return genciaEtiinvnv01;
    }

    public void setGenciaEtiinvnv01(String genciaEtiinvnv01) {
        this.genciaEtiinvnv01 = genciaEtiinvnv01;
    }

    public String getGenciaEtiinvnv02() {
        return genciaEtiinvnv02;
    }

    public void setGenciaEtiinvnv02(String genciaEtiinvnv02) {
        this.genciaEtiinvnv02 = genciaEtiinvnv02;
    }

    public String getGenciaEtiinvnv03() {
        return genciaEtiinvnv03;
    }

    public void setGenciaEtiinvnv03(String genciaEtiinvnv03) {
        this.genciaEtiinvnv03 = genciaEtiinvnv03;
    }

    public String getGenciaEtiinvnv04() {
        return genciaEtiinvnv04;
    }

    public void setGenciaEtiinvnv04(String genciaEtiinvnv04) {
        this.genciaEtiinvnv04 = genciaEtiinvnv04;
    }

    public String getGenciaEtiinvnv05() {
        return genciaEtiinvnv05;
    }

    public void setGenciaEtiinvnv05(String genciaEtiinvnv05) {
        this.genciaEtiinvnv05 = genciaEtiinvnv05;
    }

    public String getGenciaEtiinvnv06() {
        return genciaEtiinvnv06;
    }

    public void setGenciaEtiinvnv06(String genciaEtiinvnv06) {
        this.genciaEtiinvnv06 = genciaEtiinvnv06;
    }

    public String getGenciaEtiinvnv07() {
        return genciaEtiinvnv07;
    }

    public void setGenciaEtiinvnv07(String genciaEtiinvnv07) {
        this.genciaEtiinvnv07 = genciaEtiinvnv07;
    }

    public BigDecimal getGenciaBasefactur() {
        return genciaBasefactur;
    }

    public void setGenciaBasefactur(BigDecimal genciaBasefactur) {
        this.genciaBasefactur = genciaBasefactur;
    }

    public Short getGenciaUltnivzona() {
        return genciaUltnivzona;
    }

    public void setGenciaUltnivzona(Short genciaUltnivzona) {
        this.genciaUltnivzona = genciaUltnivzona;
    }

    public Short getGenciaUltnivinve() {
        return genciaUltnivinve;
    }

    public void setGenciaUltnivinve(Short genciaUltnivinve) {
        this.genciaUltnivinve = genciaUltnivinve;
    }

    public Long getGenciaSecposteo() {
        return genciaSecposteo;
    }

    public void setGenciaSecposteo(Long genciaSecposteo) {
        this.genciaSecposteo = genciaSecposteo;
    }

    public BigDecimal getGenciaBndajusloc() {
        return genciaBndajusloc;
    }

    public void setGenciaBndajusloc(BigDecimal genciaBndajusloc) {
        this.genciaBndajusloc = genciaBndajusloc;
    }

    public BigDecimal getGenciaBndajusbal() {
        return genciaBndajusbal;
    }

    public void setGenciaBndajusbal(BigDecimal genciaBndajusbal) {
        this.genciaBndajusbal = genciaBndajusbal;
    }

    public Boolean getGenciaEstado() {
        return genciaEstado;
    }

    public void setGenciaEstado(Boolean genciaEstado) {
        this.genciaEstado = genciaEstado;
    }

    public Date getGenciaCreafecha() {
        return genciaCreafecha;
    }

    public void setGenciaCreafecha(Date genciaCreafecha) {
        this.genciaCreafecha = genciaCreafecha;
    }

    public long getGenciaCreauser() {
        return genciaCreauser;
    }

    public void setGenciaCreauser(long genciaCreauser) {
        this.genciaCreauser = genciaCreauser;
    }

    public String getGenciaRutalogo() {
        return genciaRutalogo;
    }

    public void setGenciaRutalogo(String genciaRutalogo) {
        this.genciaRutalogo = genciaRutalogo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (genciaCodigo != null ? genciaCodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbpgenrCompania)) {
            return false;
        }
        CbpgenrCompania other = (CbpgenrCompania) object;
        if ((this.genciaCodigo == null && other.genciaCodigo != null) || (this.genciaCodigo != null && !this.genciaCodigo.equals(other.genciaCodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CbpgenrCompania[ genciaCodigo=" + genciaCodigo + " ]";
    }
    
}
