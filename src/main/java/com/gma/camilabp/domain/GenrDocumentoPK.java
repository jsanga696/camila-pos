/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb6
 */
@Embeddable
public class GenrDocumentoPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENCIA_codigo")
    private String gENCIAcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENOFI_codigo")
    private String gENOFIcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "GENMOD_codigo")
    private String gENMODcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "GENTDO_codigo")
    private String gENTDOcodigo;

    public GenrDocumentoPK() {
    }

    public GenrDocumentoPK(String gENCIAcodigo, String gENOFIcodigo, String gENMODcodigo, String gENTDOcodigo) {
        this.gENCIAcodigo = gENCIAcodigo;
        this.gENOFIcodigo = gENOFIcodigo;
        this.gENMODcodigo = gENMODcodigo;
        this.gENTDOcodigo = gENTDOcodigo;
    }

    public String getGENCIAcodigo() {
        return gENCIAcodigo;
    }

    public void setGENCIAcodigo(String gENCIAcodigo) {
        this.gENCIAcodigo = gENCIAcodigo;
    }

    public String getGENOFIcodigo() {
        return gENOFIcodigo;
    }

    public void setGENOFIcodigo(String gENOFIcodigo) {
        this.gENOFIcodigo = gENOFIcodigo;
    }

    public String getGENMODcodigo() {
        return gENMODcodigo;
    }

    public void setGENMODcodigo(String gENMODcodigo) {
        this.gENMODcodigo = gENMODcodigo;
    }

    public String getGENTDOcodigo() {
        return gENTDOcodigo;
    }

    public void setGENTDOcodigo(String gENTDOcodigo) {
        this.gENTDOcodigo = gENTDOcodigo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gENCIAcodigo != null ? gENCIAcodigo.hashCode() : 0);
        hash += (gENOFIcodigo != null ? gENOFIcodigo.hashCode() : 0);
        hash += (gENMODcodigo != null ? gENMODcodigo.hashCode() : 0);
        hash += (gENTDOcodigo != null ? gENTDOcodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GenrDocumentoPK)) {
            return false;
        }
        GenrDocumentoPK other = (GenrDocumentoPK) object;
        if ((this.gENCIAcodigo == null && other.gENCIAcodigo != null) || (this.gENCIAcodigo != null && !this.gENCIAcodigo.equals(other.gENCIAcodigo))) {
            return false;
        }
        if ((this.gENOFIcodigo == null && other.gENOFIcodigo != null) || (this.gENOFIcodigo != null && !this.gENOFIcodigo.equals(other.gENOFIcodigo))) {
            return false;
        }
        if ((this.gENMODcodigo == null && other.gENMODcodigo != null) || (this.gENMODcodigo != null && !this.gENMODcodigo.equals(other.gENMODcodigo))) {
            return false;
        }
        if ((this.gENTDOcodigo == null && other.gENTDOcodigo != null) || (this.gENTDOcodigo != null && !this.gENTDOcodigo.equals(other.gENTDOcodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.GenrDocumentoPK[ gENCIAcodigo=" + gENCIAcodigo + ", gENOFIcodigo=" + gENOFIcodigo + ", gENMODcodigo=" + gENMODcodigo + ", gENTDOcodigo=" + gENTDOcodigo + " ]";
    }
    
}
