/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
//import org.hibernate.annotations.Formula;

/**
 *
 * @author userdb5
 */
@Entity
@Table(name = "COMT_CABCOMPRA")
@NamedQueries({
    @NamedQuery(name = "ComtCabcompra.findAll", query = "SELECT c FROM ComtCabcompra c")})
public class ComtCabcompra implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "COMCCM_numsecucial")
    private Long cOMCCMnumsecucial;
    @Basic(optional = false)
    @NotNull
    @Column(name = "COMCCM_numdocumen")
    private int cOMCCMnumdocumen;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENCIA_codigo")
    private String gENCIAcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENOFI_codigo")
    private String gENOFIcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "GENMOD_codigo")
    private String gENMODcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "GENTDO_codigo")
    private String gENTDOcodigo;
    @Size(max = 8)
    @Column(name = "CXPPRO_codigo")
    private String cXPPROcodigo;
    @Column(name = "COMCCM_fecrecepci")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cOMCCMfecrecepci;
    @Column(name = "CXPDOC_fechafactura")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cXPDOCfechafactura;
    @Size(max = 50)
    @Column(name = "COMCCM_descricort")
    private String cOMCCMdescricort;
    @Size(max = 200)
    @Column(name = "COMCCM_descrilarg")
    private String cOMCCMdescrilarg;
    @Size(max = 15)
    @Column(name = "COMCCM_numfacprov")
    private String cOMCCMnumfacprov;
    @Size(max = 1)
    @Column(name = "COMCCM_estado")
    private String cOMCCMestado;
    @Size(max = 5)
    @Column(name = "GENMON_codigo")
    private String gENMONcodigo;
    @Size(max = 3)
    @Column(name = "COMTDO_codigorel")
    private String cOMTDOcodigorel;
    @Size(max = 9)
    @Column(name = "COMCCM_numdocurel")
    private String cOMCCMnumdocurel;
    @Column(name = "COMCCM_numsecrel")
    private Integer cOMCCMnumsecrel;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "COMCCM_subtotamov")
    private BigDecimal cOMCCMsubtotamov;
    @Column(name = "COMCCM_desctomov")
    private BigDecimal cOMCCMdesctomov;
    @Column(name = "COMCCM_totimpumov")
    private BigDecimal cOMCCMtotimpumov;
    @Column(name = "COMCCM_netomov")
    private BigDecimal cOMCCMnetomov;
    @Size(max = 1)
    @Column(name = "COMCCM_estuso")
    private String cOMCCMestuso;
    @Column(name = "COMCCM_totalpeso")
    private Long cOMCCMtotalpeso;
    @Column(name = "COMCCM_totalvolum")
    private Long cOMCCMtotalvolum;
    @Size(max = 1)
    @Column(name = "COMCCM_estelimina")
    private String cOMCCMestelimina;
    @Size(max = 3)
    @Column(name = "COMCCM_usuaingreg")
    private String cOMCCMusuaingreg;
    @Column(name = "COMCCM_fechingreg")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cOMCCMfechingreg;
    @Size(max = 1)
    @Column(name = "COMCCM_prepostea")
    private String cOMCCMprepostea;
    @Column(name = "COMCCM_valimpICE")
    private BigDecimal cOMCCMvalimpICE;
    //@Formula(value = "(SELECT p.CXPPRO_razonsocia FROM CXPM_PROVEEDOR p WHERE p.GENCIA_codigo=GENCIA_codigo AND p.GENOFI_codigo=GENOFI_codigo AND p.CXPPRO_codigo=CXPPRO_codigo)")
    @Transient
    private String proveedor;
    //@Formula(value = "(SELECT TOP 1 d.COMDCM_codbodega FROM COMT_DETCOMPRA d WHERE d.COMCCM_numsecucial=COMCCM_numsecucial)")
    @Transient
    private String bodega;

    @Transient
    private List<ComtDetcompra> detalle;
    
    public ComtCabcompra() {
    }

    public ComtCabcompra(Long cOMCCMnumsecucial) {
        this.cOMCCMnumsecucial = cOMCCMnumsecucial;
    }

    public ComtCabcompra(Long cOMCCMnumsecucial, int cOMCCMnumdocumen, String gENCIAcodigo, String gENOFIcodigo, String gENMODcodigo, String gENTDOcodigo) {
        this.cOMCCMnumsecucial = cOMCCMnumsecucial;
        this.cOMCCMnumdocumen = cOMCCMnumdocumen;
        this.gENCIAcodigo = gENCIAcodigo;
        this.gENOFIcodigo = gENOFIcodigo;
        this.gENMODcodigo = gENMODcodigo;
        this.gENTDOcodigo = gENTDOcodigo;
    }

    public Long getCOMCCMnumsecucial() {
        return cOMCCMnumsecucial;
    }

    public void setCOMCCMnumsecucial(Long cOMCCMnumsecucial) {
        this.cOMCCMnumsecucial = cOMCCMnumsecucial;
    }

    public int getCOMCCMnumdocumen() {
        return cOMCCMnumdocumen;
    }

    public void setCOMCCMnumdocumen(int cOMCCMnumdocumen) {
        this.cOMCCMnumdocumen = cOMCCMnumdocumen;
    }

    public String getGENCIAcodigo() {
        return gENCIAcodigo;
    }

    public void setGENCIAcodigo(String gENCIAcodigo) {
        this.gENCIAcodigo = gENCIAcodigo;
    }

    public String getGENOFIcodigo() {
        return gENOFIcodigo;
    }

    public void setGENOFIcodigo(String gENOFIcodigo) {
        this.gENOFIcodigo = gENOFIcodigo;
    }

    public String getGENMODcodigo() {
        return gENMODcodigo;
    }

    public void setGENMODcodigo(String gENMODcodigo) {
        this.gENMODcodigo = gENMODcodigo;
    }

    public String getGENTDOcodigo() {
        return gENTDOcodigo;
    }

    public void setGENTDOcodigo(String gENTDOcodigo) {
        this.gENTDOcodigo = gENTDOcodigo;
    }

    public String getCXPPROcodigo() {
        return cXPPROcodigo;
    }

    public void setCXPPROcodigo(String cXPPROcodigo) {
        this.cXPPROcodigo = cXPPROcodigo;
    }

    public Date getCOMCCMfecrecepci() {
        return cOMCCMfecrecepci;
    }

    public void setCOMCCMfecrecepci(Date cOMCCMfecrecepci) {
        this.cOMCCMfecrecepci = cOMCCMfecrecepci;
    }

    public Date getCXPDOCfechafactura() {
        return cXPDOCfechafactura;
    }

    public void setCXPDOCfechafactura(Date cXPDOCfechafactura) {
        this.cXPDOCfechafactura = cXPDOCfechafactura;
    }

    public String getCOMCCMdescricort() {
        return cOMCCMdescricort;
    }

    public void setCOMCCMdescricort(String cOMCCMdescricort) {
        this.cOMCCMdescricort = cOMCCMdescricort;
    }

    public String getCOMCCMdescrilarg() {
        return cOMCCMdescrilarg;
    }

    public void setCOMCCMdescrilarg(String cOMCCMdescrilarg) {
        this.cOMCCMdescrilarg = cOMCCMdescrilarg;
    }

    public String getCOMCCMnumfacprov() {
        return cOMCCMnumfacprov;
    }

    public void setCOMCCMnumfacprov(String cOMCCMnumfacprov) {
        this.cOMCCMnumfacprov = cOMCCMnumfacprov;
    }

    public String getCOMCCMestado() {
        return cOMCCMestado;
    }

    public void setCOMCCMestado(String cOMCCMestado) {
        this.cOMCCMestado = cOMCCMestado;
    }

    public String getGENMONcodigo() {
        return gENMONcodigo;
    }

    public void setGENMONcodigo(String gENMONcodigo) {
        this.gENMONcodigo = gENMONcodigo;
    }

    public String getCOMTDOcodigorel() {
        return cOMTDOcodigorel;
    }

    public void setCOMTDOcodigorel(String cOMTDOcodigorel) {
        this.cOMTDOcodigorel = cOMTDOcodigorel;
    }

    public String getCOMCCMnumdocurel() {
        return cOMCCMnumdocurel;
    }

    public void setCOMCCMnumdocurel(String cOMCCMnumdocurel) {
        this.cOMCCMnumdocurel = cOMCCMnumdocurel;
    }

    public Integer getCOMCCMnumsecrel() {
        return cOMCCMnumsecrel;
    }

    public void setCOMCCMnumsecrel(Integer cOMCCMnumsecrel) {
        this.cOMCCMnumsecrel = cOMCCMnumsecrel;
    }

    public BigDecimal getCOMCCMsubtotamov() {
        return cOMCCMsubtotamov;
    }

    public void setCOMCCMsubtotamov(BigDecimal cOMCCMsubtotamov) {
        this.cOMCCMsubtotamov = cOMCCMsubtotamov;
    }

    public BigDecimal getCOMCCMdesctomov() {
        return cOMCCMdesctomov;
    }

    public void setCOMCCMdesctomov(BigDecimal cOMCCMdesctomov) {
        this.cOMCCMdesctomov = cOMCCMdesctomov;
    }

    public BigDecimal getCOMCCMtotimpumov() {
        return cOMCCMtotimpumov;
    }

    public void setCOMCCMtotimpumov(BigDecimal cOMCCMtotimpumov) {
        this.cOMCCMtotimpumov = cOMCCMtotimpumov;
    }

    public BigDecimal getCOMCCMnetomov() {
        return cOMCCMnetomov;
    }

    public void setCOMCCMnetomov(BigDecimal cOMCCMnetomov) {
        this.cOMCCMnetomov = cOMCCMnetomov;
    }

    public String getCOMCCMestuso() {
        return cOMCCMestuso;
    }

    public void setCOMCCMestuso(String cOMCCMestuso) {
        this.cOMCCMestuso = cOMCCMestuso;
    }

    public Long getCOMCCMtotalpeso() {
        return cOMCCMtotalpeso;
    }

    public void setCOMCCMtotalpeso(Long cOMCCMtotalpeso) {
        this.cOMCCMtotalpeso = cOMCCMtotalpeso;
    }

    public Long getCOMCCMtotalvolum() {
        return cOMCCMtotalvolum;
    }

    public void setCOMCCMtotalvolum(Long cOMCCMtotalvolum) {
        this.cOMCCMtotalvolum = cOMCCMtotalvolum;
    }

    public String getCOMCCMestelimina() {
        return cOMCCMestelimina;
    }

    public void setCOMCCMestelimina(String cOMCCMestelimina) {
        this.cOMCCMestelimina = cOMCCMestelimina;
    }

    public String getCOMCCMusuaingreg() {
        return cOMCCMusuaingreg;
    }

    public void setCOMCCMusuaingreg(String cOMCCMusuaingreg) {
        this.cOMCCMusuaingreg = cOMCCMusuaingreg;
    }

    public Date getCOMCCMfechingreg() {
        return cOMCCMfechingreg;
    }

    public void setCOMCCMfechingreg(Date cOMCCMfechingreg) {
        this.cOMCCMfechingreg = cOMCCMfechingreg;
    }

    public String getCOMCCMprepostea() {
        return cOMCCMprepostea;
    }

    public void setCOMCCMprepostea(String cOMCCMprepostea) {
        this.cOMCCMprepostea = cOMCCMprepostea;
    }

    public BigDecimal getCOMCCMvalimpICE() {
        return cOMCCMvalimpICE;
    }

    public void setCOMCCMvalimpICE(BigDecimal cOMCCMvalimpICE) {
        this.cOMCCMvalimpICE = cOMCCMvalimpICE;
    }

    public List<ComtDetcompra> getDetalle() {
        return detalle;
    }

    public void setDetalle(List<ComtDetcompra> detalle) {
        this.detalle = detalle;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    public String getBodega() {
        return bodega;
    }

    public void setBodega(String bodega) {
        this.bodega = bodega;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cOMCCMnumsecucial != null ? cOMCCMnumsecucial.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ComtCabcompra)) {
            return false;
        }
        ComtCabcompra other = (ComtCabcompra) object;
        if ((this.cOMCCMnumsecucial == null && other.cOMCCMnumsecucial != null) || (this.cOMCCMnumsecucial != null && !this.cOMCCMnumsecucial.equals(other.cOMCCMnumsecucial))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.ComtCabcompra[ cOMCCMnumsecucial=" + cOMCCMnumsecucial + " ]";
    }
    
}
