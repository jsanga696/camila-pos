/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb
 */
@Entity
@Table(name = "CBPGENP_PARAMOFICINA")
@NamedQueries({
    @NamedQuery(name = "CbpgenpParamoficina.findAll", query = "SELECT c FROM CbpgenpParamoficina c")})
public class CbpgenpParamoficina implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "gencia_codigo")
    private long genciaCodigo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "genapl_codigo")
    private long genaplCodigo;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "genpof_codigo")
    private Long genpofCodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "genpof_etiqueta")
    private String genpofEtiqueta;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "genpof_nombre")
    private String genpofNombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "genpof_nombrevalor")
    private String genpofNombrevalor;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "genpof_tipo")
    private String genpofTipo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "genpof_validacion")
    private String genpofValidacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "genpof_creauser")
    private long genpofCreauser;
    @Basic(optional = false)
    @NotNull
    @Column(name = "genpof_creafecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date genpofCreafecha;
    @Column(name = "genpof_flagmoficar")
    private Boolean genpofFlagmoficar;
    @JoinColumn(name = "genofi_codigo", referencedColumnName = "genofi_codigo")
    @ManyToOne(optional = false)
    private CbpgenrOficina genofiCodigo;

    public CbpgenpParamoficina() {
    }

    public CbpgenpParamoficina(Long genpofCodigo) {
        this.genpofCodigo = genpofCodigo;
    }

    public CbpgenpParamoficina(Long genpofCodigo, long genciaCodigo, long genaplCodigo, String genpofEtiqueta, String genpofNombre, String genpofNombrevalor, String genpofTipo, String genpofValidacion, long genpofCreauser, Date genpofCreafecha) {
        this.genpofCodigo = genpofCodigo;
        this.genciaCodigo = genciaCodigo;
        this.genaplCodigo = genaplCodigo;
        this.genpofEtiqueta = genpofEtiqueta;
        this.genpofNombre = genpofNombre;
        this.genpofNombrevalor = genpofNombrevalor;
        this.genpofTipo = genpofTipo;
        this.genpofValidacion = genpofValidacion;
        this.genpofCreauser = genpofCreauser;
        this.genpofCreafecha = genpofCreafecha;
    }

    public long getGenciaCodigo() {
        return genciaCodigo;
    }

    public void setGenciaCodigo(long genciaCodigo) {
        this.genciaCodigo = genciaCodigo;
    }

    public long getGenaplCodigo() {
        return genaplCodigo;
    }

    public void setGenaplCodigo(long genaplCodigo) {
        this.genaplCodigo = genaplCodigo;
    }

    public Long getGenpofCodigo() {
        return genpofCodigo;
    }

    public void setGenpofCodigo(Long genpofCodigo) {
        this.genpofCodigo = genpofCodigo;
    }

    public String getGenpofEtiqueta() {
        return genpofEtiqueta;
    }

    public void setGenpofEtiqueta(String genpofEtiqueta) {
        this.genpofEtiqueta = genpofEtiqueta;
    }

    public String getGenpofNombre() {
        return genpofNombre;
    }

    public void setGenpofNombre(String genpofNombre) {
        this.genpofNombre = genpofNombre;
    }

    public String getGenpofNombrevalor() {
        return genpofNombrevalor;
    }

    public void setGenpofNombrevalor(String genpofNombrevalor) {
        this.genpofNombrevalor = genpofNombrevalor;
    }

    public String getGenpofTipo() {
        return genpofTipo;
    }

    public void setGenpofTipo(String genpofTipo) {
        this.genpofTipo = genpofTipo;
    }

    public String getGenpofValidacion() {
        return genpofValidacion;
    }

    public void setGenpofValidacion(String genpofValidacion) {
        this.genpofValidacion = genpofValidacion;
    }

    public long getGenpofCreauser() {
        return genpofCreauser;
    }

    public void setGenpofCreauser(long genpofCreauser) {
        this.genpofCreauser = genpofCreauser;
    }

    public Date getGenpofCreafecha() {
        return genpofCreafecha;
    }

    public void setGenpofCreafecha(Date genpofCreafecha) {
        this.genpofCreafecha = genpofCreafecha;
    }

    public Boolean getGenpofFlagmoficar() {
        return genpofFlagmoficar;
    }

    public void setGenpofFlagmoficar(Boolean genpofFlagmoficar) {
        this.genpofFlagmoficar = genpofFlagmoficar;
    }

    public CbpgenrOficina getGenofiCodigo() {
        return genofiCodigo;
    }

    public void setGenofiCodigo(CbpgenrOficina genofiCodigo) {
        this.genofiCodigo = genofiCodigo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (genpofCodigo != null ? genpofCodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbpgenpParamoficina)) {
            return false;
        }
        CbpgenpParamoficina other = (CbpgenpParamoficina) object;
        if ((this.genpofCodigo == null && other.genpofCodigo != null) || (this.genpofCodigo != null && !this.genpofCodigo.equals(other.genpofCodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CbpgenpParamoficina[ genpofCodigo=" + genpofCodigo + " ]";
    }
    
}
