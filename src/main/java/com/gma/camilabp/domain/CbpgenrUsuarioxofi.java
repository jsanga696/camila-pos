/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb
 */
@Entity
@Table(name = "CBPGENR_USUARIOXOFI")
@NamedQueries({
    @NamedQuery(name = "CbpgenrUsuarioxofi.findAll", query = "SELECT c FROM CbpgenrUsuarioxofi c")})
public class CbpgenrUsuarioxofi implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "genuxo_codigo")
    private Long genuxoCodigo;
    @JoinColumn(name = "gencia_codigo", referencedColumnName = "gencia_codigo")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private CbpgenrCompania genciaCodigo;
    @JoinColumn(name = "genofi_codigo", referencedColumnName = "genofi_codigo")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private CbpgenrOficina genofiCodigo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "genbod_codigo")
    private long genbodCodigo;
    @JoinColumn(name = "segusu_codigo", referencedColumnName = "segusu_codigo")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private CbpsegmUsuario segusuCodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "segusu_login")
    private String segusuLogin;
    
    public CbpgenrUsuarioxofi() {
    }

    public CbpgenrUsuarioxofi(Long genuxoCodigo) {
        this.genuxoCodigo = genuxoCodigo;
    }

    public CbpgenrUsuarioxofi(Long genuxoCodigo, long genbodCodigo, String segusuLogin) {
        this.genuxoCodigo = genuxoCodigo;
        this.genbodCodigo = genbodCodigo;
        this.segusuLogin = segusuLogin;
    }

    public Long getGenuxoCodigo() {
        return genuxoCodigo;
    }

    public void setGenuxoCodigo(Long genuxoCodigo) {
        this.genuxoCodigo = genuxoCodigo;
    }

    public CbpgenrCompania getGenciaCodigo() {
        return genciaCodigo;
    }

    public void setGenciaCodigo(CbpgenrCompania genciaCodigo) {
        this.genciaCodigo = genciaCodigo;
    }

    public CbpgenrOficina getGenofiCodigo() {
        return genofiCodigo;
    }

    public void setGenofiCodigo(CbpgenrOficina genofiCodigo) {
        this.genofiCodigo = genofiCodigo;
    }

    public long getGenbodCodigo() {
        return genbodCodigo;
    }

    public void setGenbodCodigo(long genbodCodigo) {
        this.genbodCodigo = genbodCodigo;
    }

    public CbpsegmUsuario getSegusuCodigo() {
        return segusuCodigo;
    }

    public void setSegusuCodigo(CbpsegmUsuario segusuCodigo) {
        this.segusuCodigo = segusuCodigo;
    }

    public String getSegusuLogin() {
        return segusuLogin;
    }

    public void setSegusuLogin(String segusuLogin) {
        this.segusuLogin = segusuLogin;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (genuxoCodigo != null ? genuxoCodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbpgenrUsuarioxofi)) {
            return false;
        }
        CbpgenrUsuarioxofi other = (CbpgenrUsuarioxofi) object;
        if ((this.genuxoCodigo == null && other.genuxoCodigo != null) || (this.genuxoCodigo != null && !this.genuxoCodigo.equals(other.genuxoCodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CbpgenrUsuarioxofi[ genuxoCodigo=" + genuxoCodigo + " ]";
    }
    
}
