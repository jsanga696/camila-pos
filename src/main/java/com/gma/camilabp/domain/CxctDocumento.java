/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb6
 */
@Entity
@Table(name = "CXCT_DOCUMENTO")
@NamedQueries({
    @NamedQuery(name = "CxctDocumento.findAll", query = "SELECT c FROM CxctDocumento c")})
public class CxctDocumento implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CxctDocumentoPK cxctDocumentoPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENOFI_codigo")
    private String gENOFIcodigo;
    @Size(max = 6)
    @Column(name = "CXCCLI_codigo")
    private String cXCCLIcodigo;
    @Column(name = "CXCDEU_fechaemisi")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cXCDEUfechaemisi;
    @Column(name = "CXCDEU_fechavenci")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cXCDEUfechavenci;
    @Size(max = 6)
    @Column(name = "FACVDR_codigo")
    private String fACVDRcodigo;
    @Size(max = 3)
    @Column(name = "CXCFPG_codigo")
    private String cXCFPGcodigo;
    @Size(max = 10)
    @Column(name = "CXCDEU_terminopag")
    private String cXCDEUterminopag;
    @Column(name = "CXCDEU_diasplazo")
    private Short cXCDEUdiasplazo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "GENMOD_codigo")
    private String gENMODcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "GENTDO_codigo")
    private String gENTDOcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "CXCRUT_codigodocu")
    private String cXCRUTcodigodocu;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "CXCDEU_numdocumen")
    private String cXCDEUnumdocumen;
    @Size(max = 9)
    @Column(name = "CXCDEU_numplanif")
    private String cXCDEUnumplanif;
    @Column(name = "CXCDEU_secplanif")
    private Long cXCDEUsecplanif;
    @Size(max = 3)
    @Column(name = "GENCAU_codcausadb")
    private String gENCAUcodcausadb;
    @Size(max = 10)
    @Column(name = "cxcdeu_rutdistcli")
    private String cxcdeuRutdistcli;
    @Size(max = 10)
    @Column(name = "cxcdeu_rutvtacli")
    private String cxcdeuRutvtacli;
    @Size(max = 4)
    @Column(name = "GENCIA_coddocrela")
    private String gENCIAcoddocrela;
    @Size(max = 4)
    @Column(name = "GENOFI_coddocrela")
    private String gENOFIcoddocrela;
    @Size(max = 3)
    @Column(name = "GENMOD_coddocrela")
    private String gENMODcoddocrela;
    @Size(max = 3)
    @Column(name = "GENTDO_coddocrel")
    private String gENTDOcoddocrel;
    @Size(max = 10)
    @Column(name = "CXCRUT_coddocrela")
    private String cXCRUTcoddocrela;
    @Size(max = 9)
    @Column(name = "CXCDEU_numdocrela")
    private String cXCDEUnumdocrela;
    @Size(max = 5)
    @Column(name = "GENMON_codigo")
    private String gENMONcodigo;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "CXCDEU_cotiza")
    private BigDecimal cXCDEUcotiza;
    @Column(name = "CXCDEU_montomov")
    private BigDecimal cXCDEUmontomov;
    @Column(name = "CXCDEU_saldomov")
    private BigDecimal cXCDEUsaldomov;
    @Column(name = "CXCDEU_debitomov")
    private BigDecimal cXCDEUdebitomov;
    @Column(name = "CXCDEU_creditomov")
    private BigDecimal cXCDEUcreditomov;
    @Column(name = "CXCDEU_subtotalmov")
    private BigDecimal cXCDEUsubtotalmov;
    @Column(name = "CXCDEU_descuentomov")
    private BigDecimal cXCDEUdescuentomov;
    @Column(name = "CXCDEU_imptomov")
    private BigDecimal cXCDEUimptomov;
    @Size(max = 50)
    @Column(name = "CXCDEU_concepto")
    private String cXCDEUconcepto;
    @Size(max = 200)
    @Column(name = "CXCDEU_detalle")
    private String cXCDEUdetalle;
    @Size(max = 2)
    @Column(name = "CXCDEU_tipoban")
    private String cXCDEUtipoban;
    @Size(max = 2)
    @Column(name = "CXCDEU_codbanco")
    private String cXCDEUcodbanco;
    @Size(max = 13)
    @Column(name = "CXCDEU_numchq")
    private String cXCDEUnumchq;
    @Size(max = 15)
    @Column(name = "CXCDEU_cuentacte")
    private String cXCDEUcuentacte;
    @Size(max = 1)
    @Column(name = "CXCDEU_estado")
    private String cXCDEUestado;
    @Size(max = 3)
    @Column(name = "CXCDEU_usuaingreg")
    private String cXCDEUusuaingreg;
    @Size(max = 3)
    @Column(name = "CXCDEU_usuamodreg")
    private String cXCDEUusuamodreg;
    @Column(name = "CXCDEU_fechingreg")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cXCDEUfechingreg;
    @Column(name = "CXCDEU_fechmodreg")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cXCDEUfechmodreg;
    @Size(max = 1)
    @Column(name = "CXCDEU_estcontab")
    private String cXCDEUestcontab;
    @Size(max = 15)
    @Column(name = "CXCDEU_codigocont")
    private String cXCDEUcodigocont;
    @Size(max = 9)
    @Column(name = "CXCDEU_secubanco")
    private String cXCDEUsecubanco;
    @Size(max = 20)
    @Column(name = "CXCDEU_ctacontable")
    private String cXCDEUctacontable;
    @Size(max = 18)
    @Column(name = "CXCDEU_codifisco")
    private String cXCDEUcodifisco;
    @Size(max = 20)
    @Column(name = "CXCDEU_codifiscoret")
    private String cXCDEUcodifiscoret;
    @Column(name = "CXCDEU_secfactrela")
    private Integer cXCDEUsecfactrela;
    @Size(max = 5)
    @Column(name = "CXCDEU_motdevolu")
    private String cXCDEUmotdevolu;
    @Size(max = 1)
    @Column(name = "CXCDEU_estaretencion")
    private String cXCDEUestaretencion;
    @Size(max = 40)
    @Column(name = "CXCDEU_codautoriza")
    private String cXCDEUcodautoriza;
    @Column(name = "GENTAL_secuencia")
    private Integer gENTALsecuencia;
    @Column(name = "CXCDEU_baseimponible")
    private BigDecimal cXCDEUbaseimponible;
    @Column(name = "CXCDEU_basecero")
    private BigDecimal cXCDEUbasecero;
    @Column(name = "CXCDEU_imptoservicio")
    private BigDecimal cXCDEUimptoservicio;
    @Column(name = "CXCDEU_imptoICE")
    private BigDecimal cXCDEUimptoICE;
    @Size(max = 50)
    @Column(name = "CXCDEU_autorizacion")
    private String cXCDEUautorizacion;
    @Column(name = "CXCDEU_fechaautoriza")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cXCDEUfechaautoriza;
    @Size(max = 50)
    @Column(name = "CXCDEU_claveacceso")
    private String cXCDEUclaveacceso;
    @Size(max = 5)
    @Column(name = "CXCDEU_codimpuesto")
    private String cXCDEUcodimpuesto;
    @Size(max = 15)
    @Column(name = "CXCDEU_codautorizaimprenta")
    private String cXCDEUcodautorizaimprenta;

    public CxctDocumento() {
    }

    public CxctDocumento(CxctDocumentoPK cxctDocumentoPK) {
        this.cxctDocumentoPK = cxctDocumentoPK;
    }

    public CxctDocumento(CxctDocumentoPK cxctDocumentoPK, String gENOFIcodigo, String gENMODcodigo, String gENTDOcodigo, String cXCRUTcodigodocu, String cXCDEUnumdocumen) {
        this.cxctDocumentoPK = cxctDocumentoPK;
        this.gENOFIcodigo = gENOFIcodigo;
        this.gENMODcodigo = gENMODcodigo;
        this.gENTDOcodigo = gENTDOcodigo;
        this.cXCRUTcodigodocu = cXCRUTcodigodocu;
        this.cXCDEUnumdocumen = cXCDEUnumdocumen;
    }

    public CxctDocumento(String gENCIAcodigo, long cXCDEUnumesecuen) {
        this.cxctDocumentoPK = new CxctDocumentoPK(gENCIAcodigo, cXCDEUnumesecuen);
    }

    public CxctDocumentoPK getCxctDocumentoPK() {
        return cxctDocumentoPK;
    }

    public void setCxctDocumentoPK(CxctDocumentoPK cxctDocumentoPK) {
        this.cxctDocumentoPK = cxctDocumentoPK;
    }

    public String getGENOFIcodigo() {
        return gENOFIcodigo;
    }

    public void setGENOFIcodigo(String gENOFIcodigo) {
        this.gENOFIcodigo = gENOFIcodigo;
    }

    public String getCXCCLIcodigo() {
        return cXCCLIcodigo;
    }

    public void setCXCCLIcodigo(String cXCCLIcodigo) {
        this.cXCCLIcodigo = cXCCLIcodigo;
    }

    public Date getCXCDEUfechaemisi() {
        return cXCDEUfechaemisi;
    }

    public void setCXCDEUfechaemisi(Date cXCDEUfechaemisi) {
        this.cXCDEUfechaemisi = cXCDEUfechaemisi;
    }

    public Date getCXCDEUfechavenci() {
        return cXCDEUfechavenci;
    }

    public void setCXCDEUfechavenci(Date cXCDEUfechavenci) {
        this.cXCDEUfechavenci = cXCDEUfechavenci;
    }

    public String getFACVDRcodigo() {
        return fACVDRcodigo;
    }

    public void setFACVDRcodigo(String fACVDRcodigo) {
        this.fACVDRcodigo = fACVDRcodigo;
    }

    public String getCXCFPGcodigo() {
        return cXCFPGcodigo;
    }

    public void setCXCFPGcodigo(String cXCFPGcodigo) {
        this.cXCFPGcodigo = cXCFPGcodigo;
    }

    public String getCXCDEUterminopag() {
        return cXCDEUterminopag;
    }

    public void setCXCDEUterminopag(String cXCDEUterminopag) {
        this.cXCDEUterminopag = cXCDEUterminopag;
    }

    public Short getCXCDEUdiasplazo() {
        return cXCDEUdiasplazo;
    }

    public void setCXCDEUdiasplazo(Short cXCDEUdiasplazo) {
        this.cXCDEUdiasplazo = cXCDEUdiasplazo;
    }

    public String getGENMODcodigo() {
        return gENMODcodigo;
    }

    public void setGENMODcodigo(String gENMODcodigo) {
        this.gENMODcodigo = gENMODcodigo;
    }

    public String getGENTDOcodigo() {
        return gENTDOcodigo;
    }

    public void setGENTDOcodigo(String gENTDOcodigo) {
        this.gENTDOcodigo = gENTDOcodigo;
    }

    public String getCXCRUTcodigodocu() {
        return cXCRUTcodigodocu;
    }

    public void setCXCRUTcodigodocu(String cXCRUTcodigodocu) {
        this.cXCRUTcodigodocu = cXCRUTcodigodocu;
    }

    public String getCXCDEUnumdocumen() {
        return cXCDEUnumdocumen;
    }

    public void setCXCDEUnumdocumen(String cXCDEUnumdocumen) {
        this.cXCDEUnumdocumen = cXCDEUnumdocumen;
    }

    public String getCXCDEUnumplanif() {
        return cXCDEUnumplanif;
    }

    public void setCXCDEUnumplanif(String cXCDEUnumplanif) {
        this.cXCDEUnumplanif = cXCDEUnumplanif;
    }

    public Long getCXCDEUsecplanif() {
        return cXCDEUsecplanif;
    }

    public void setCXCDEUsecplanif(Long cXCDEUsecplanif) {
        this.cXCDEUsecplanif = cXCDEUsecplanif;
    }

    public String getGENCAUcodcausadb() {
        return gENCAUcodcausadb;
    }

    public void setGENCAUcodcausadb(String gENCAUcodcausadb) {
        this.gENCAUcodcausadb = gENCAUcodcausadb;
    }

    public String getCxcdeuRutdistcli() {
        return cxcdeuRutdistcli;
    }

    public void setCxcdeuRutdistcli(String cxcdeuRutdistcli) {
        this.cxcdeuRutdistcli = cxcdeuRutdistcli;
    }

    public String getCxcdeuRutvtacli() {
        return cxcdeuRutvtacli;
    }

    public void setCxcdeuRutvtacli(String cxcdeuRutvtacli) {
        this.cxcdeuRutvtacli = cxcdeuRutvtacli;
    }

    public String getGENCIAcoddocrela() {
        return gENCIAcoddocrela;
    }

    public void setGENCIAcoddocrela(String gENCIAcoddocrela) {
        this.gENCIAcoddocrela = gENCIAcoddocrela;
    }

    public String getGENOFIcoddocrela() {
        return gENOFIcoddocrela;
    }

    public void setGENOFIcoddocrela(String gENOFIcoddocrela) {
        this.gENOFIcoddocrela = gENOFIcoddocrela;
    }

    public String getGENMODcoddocrela() {
        return gENMODcoddocrela;
    }

    public void setGENMODcoddocrela(String gENMODcoddocrela) {
        this.gENMODcoddocrela = gENMODcoddocrela;
    }

    public String getGENTDOcoddocrel() {
        return gENTDOcoddocrel;
    }

    public void setGENTDOcoddocrel(String gENTDOcoddocrel) {
        this.gENTDOcoddocrel = gENTDOcoddocrel;
    }

    public String getCXCRUTcoddocrela() {
        return cXCRUTcoddocrela;
    }

    public void setCXCRUTcoddocrela(String cXCRUTcoddocrela) {
        this.cXCRUTcoddocrela = cXCRUTcoddocrela;
    }

    public String getCXCDEUnumdocrela() {
        return cXCDEUnumdocrela;
    }

    public void setCXCDEUnumdocrela(String cXCDEUnumdocrela) {
        this.cXCDEUnumdocrela = cXCDEUnumdocrela;
    }

    public String getGENMONcodigo() {
        return gENMONcodigo;
    }

    public void setGENMONcodigo(String gENMONcodigo) {
        this.gENMONcodigo = gENMONcodigo;
    }

    public BigDecimal getCXCDEUcotiza() {
        return cXCDEUcotiza;
    }

    public void setCXCDEUcotiza(BigDecimal cXCDEUcotiza) {
        this.cXCDEUcotiza = cXCDEUcotiza;
    }

    public BigDecimal getCXCDEUmontomov() {
        return cXCDEUmontomov;
    }

    public void setCXCDEUmontomov(BigDecimal cXCDEUmontomov) {
        this.cXCDEUmontomov = cXCDEUmontomov;
    }

    public BigDecimal getCXCDEUsaldomov() {
        return cXCDEUsaldomov;
    }

    public void setCXCDEUsaldomov(BigDecimal cXCDEUsaldomov) {
        this.cXCDEUsaldomov = cXCDEUsaldomov;
    }

    public BigDecimal getCXCDEUdebitomov() {
        return cXCDEUdebitomov;
    }

    public void setCXCDEUdebitomov(BigDecimal cXCDEUdebitomov) {
        this.cXCDEUdebitomov = cXCDEUdebitomov;
    }

    public BigDecimal getCXCDEUcreditomov() {
        return cXCDEUcreditomov;
    }

    public void setCXCDEUcreditomov(BigDecimal cXCDEUcreditomov) {
        this.cXCDEUcreditomov = cXCDEUcreditomov;
    }

    public BigDecimal getCXCDEUsubtotalmov() {
        return cXCDEUsubtotalmov;
    }

    public void setCXCDEUsubtotalmov(BigDecimal cXCDEUsubtotalmov) {
        this.cXCDEUsubtotalmov = cXCDEUsubtotalmov;
    }

    public BigDecimal getCXCDEUdescuentomov() {
        return cXCDEUdescuentomov;
    }

    public void setCXCDEUdescuentomov(BigDecimal cXCDEUdescuentomov) {
        this.cXCDEUdescuentomov = cXCDEUdescuentomov;
    }

    public BigDecimal getCXCDEUimptomov() {
        return cXCDEUimptomov;
    }

    public void setCXCDEUimptomov(BigDecimal cXCDEUimptomov) {
        this.cXCDEUimptomov = cXCDEUimptomov;
    }

    public String getCXCDEUconcepto() {
        return cXCDEUconcepto;
    }

    public void setCXCDEUconcepto(String cXCDEUconcepto) {
        this.cXCDEUconcepto = cXCDEUconcepto;
    }

    public String getCXCDEUdetalle() {
        return cXCDEUdetalle;
    }

    public void setCXCDEUdetalle(String cXCDEUdetalle) {
        this.cXCDEUdetalle = cXCDEUdetalle;
    }

    public String getCXCDEUtipoban() {
        return cXCDEUtipoban;
    }

    public void setCXCDEUtipoban(String cXCDEUtipoban) {
        this.cXCDEUtipoban = cXCDEUtipoban;
    }

    public String getCXCDEUcodbanco() {
        return cXCDEUcodbanco;
    }

    public void setCXCDEUcodbanco(String cXCDEUcodbanco) {
        this.cXCDEUcodbanco = cXCDEUcodbanco;
    }

    public String getCXCDEUnumchq() {
        return cXCDEUnumchq;
    }

    public void setCXCDEUnumchq(String cXCDEUnumchq) {
        this.cXCDEUnumchq = cXCDEUnumchq;
    }

    public String getCXCDEUcuentacte() {
        return cXCDEUcuentacte;
    }

    public void setCXCDEUcuentacte(String cXCDEUcuentacte) {
        this.cXCDEUcuentacte = cXCDEUcuentacte;
    }

    public String getCXCDEUestado() {
        return cXCDEUestado;
    }

    public void setCXCDEUestado(String cXCDEUestado) {
        this.cXCDEUestado = cXCDEUestado;
    }

    public String getCXCDEUusuaingreg() {
        return cXCDEUusuaingreg;
    }

    public void setCXCDEUusuaingreg(String cXCDEUusuaingreg) {
        this.cXCDEUusuaingreg = cXCDEUusuaingreg;
    }

    public String getCXCDEUusuamodreg() {
        return cXCDEUusuamodreg;
    }

    public void setCXCDEUusuamodreg(String cXCDEUusuamodreg) {
        this.cXCDEUusuamodreg = cXCDEUusuamodreg;
    }

    public Date getCXCDEUfechingreg() {
        return cXCDEUfechingreg;
    }

    public void setCXCDEUfechingreg(Date cXCDEUfechingreg) {
        this.cXCDEUfechingreg = cXCDEUfechingreg;
    }

    public Date getCXCDEUfechmodreg() {
        return cXCDEUfechmodreg;
    }

    public void setCXCDEUfechmodreg(Date cXCDEUfechmodreg) {
        this.cXCDEUfechmodreg = cXCDEUfechmodreg;
    }

    public String getCXCDEUestcontab() {
        return cXCDEUestcontab;
    }

    public void setCXCDEUestcontab(String cXCDEUestcontab) {
        this.cXCDEUestcontab = cXCDEUestcontab;
    }

    public String getCXCDEUcodigocont() {
        return cXCDEUcodigocont;
    }

    public void setCXCDEUcodigocont(String cXCDEUcodigocont) {
        this.cXCDEUcodigocont = cXCDEUcodigocont;
    }

    public String getCXCDEUsecubanco() {
        return cXCDEUsecubanco;
    }

    public void setCXCDEUsecubanco(String cXCDEUsecubanco) {
        this.cXCDEUsecubanco = cXCDEUsecubanco;
    }

    public String getCXCDEUctacontable() {
        return cXCDEUctacontable;
    }

    public void setCXCDEUctacontable(String cXCDEUctacontable) {
        this.cXCDEUctacontable = cXCDEUctacontable;
    }

    public String getCXCDEUcodifisco() {
        return cXCDEUcodifisco;
    }

    public void setCXCDEUcodifisco(String cXCDEUcodifisco) {
        this.cXCDEUcodifisco = cXCDEUcodifisco;
    }

    public String getCXCDEUcodifiscoret() {
        return cXCDEUcodifiscoret;
    }

    public void setCXCDEUcodifiscoret(String cXCDEUcodifiscoret) {
        this.cXCDEUcodifiscoret = cXCDEUcodifiscoret;
    }

    public Integer getCXCDEUsecfactrela() {
        return cXCDEUsecfactrela;
    }

    public void setCXCDEUsecfactrela(Integer cXCDEUsecfactrela) {
        this.cXCDEUsecfactrela = cXCDEUsecfactrela;
    }

    public String getCXCDEUmotdevolu() {
        return cXCDEUmotdevolu;
    }

    public void setCXCDEUmotdevolu(String cXCDEUmotdevolu) {
        this.cXCDEUmotdevolu = cXCDEUmotdevolu;
    }

    public String getCXCDEUestaretencion() {
        return cXCDEUestaretencion;
    }

    public void setCXCDEUestaretencion(String cXCDEUestaretencion) {
        this.cXCDEUestaretencion = cXCDEUestaretencion;
    }

    public String getCXCDEUcodautoriza() {
        return cXCDEUcodautoriza;
    }

    public void setCXCDEUcodautoriza(String cXCDEUcodautoriza) {
        this.cXCDEUcodautoriza = cXCDEUcodautoriza;
    }

    public Integer getGENTALsecuencia() {
        return gENTALsecuencia;
    }

    public void setGENTALsecuencia(Integer gENTALsecuencia) {
        this.gENTALsecuencia = gENTALsecuencia;
    }

    public BigDecimal getCXCDEUbaseimponible() {
        return cXCDEUbaseimponible;
    }

    public void setCXCDEUbaseimponible(BigDecimal cXCDEUbaseimponible) {
        this.cXCDEUbaseimponible = cXCDEUbaseimponible;
    }

    public BigDecimal getCXCDEUbasecero() {
        return cXCDEUbasecero;
    }

    public void setCXCDEUbasecero(BigDecimal cXCDEUbasecero) {
        this.cXCDEUbasecero = cXCDEUbasecero;
    }

    public BigDecimal getCXCDEUimptoservicio() {
        return cXCDEUimptoservicio;
    }

    public void setCXCDEUimptoservicio(BigDecimal cXCDEUimptoservicio) {
        this.cXCDEUimptoservicio = cXCDEUimptoservicio;
    }

    public BigDecimal getCXCDEUimptoICE() {
        return cXCDEUimptoICE;
    }

    public void setCXCDEUimptoICE(BigDecimal cXCDEUimptoICE) {
        this.cXCDEUimptoICE = cXCDEUimptoICE;
    }

    public String getCXCDEUautorizacion() {
        return cXCDEUautorizacion;
    }

    public void setCXCDEUautorizacion(String cXCDEUautorizacion) {
        this.cXCDEUautorizacion = cXCDEUautorizacion;
    }

    public Date getCXCDEUfechaautoriza() {
        return cXCDEUfechaautoriza;
    }

    public void setCXCDEUfechaautoriza(Date cXCDEUfechaautoriza) {
        this.cXCDEUfechaautoriza = cXCDEUfechaautoriza;
    }

    public String getCXCDEUclaveacceso() {
        return cXCDEUclaveacceso;
    }

    public void setCXCDEUclaveacceso(String cXCDEUclaveacceso) {
        this.cXCDEUclaveacceso = cXCDEUclaveacceso;
    }

    public String getCXCDEUcodimpuesto() {
        return cXCDEUcodimpuesto;
    }

    public void setCXCDEUcodimpuesto(String cXCDEUcodimpuesto) {
        this.cXCDEUcodimpuesto = cXCDEUcodimpuesto;
    }

    public String getCXCDEUcodautorizaimprenta() {
        return cXCDEUcodautorizaimprenta;
    }

    public void setCXCDEUcodautorizaimprenta(String cXCDEUcodautorizaimprenta) {
        this.cXCDEUcodautorizaimprenta = cXCDEUcodautorizaimprenta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cxctDocumentoPK != null ? cxctDocumentoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CxctDocumento)) {
            return false;
        }
        CxctDocumento other = (CxctDocumento) object;
        if ((this.cxctDocumentoPK == null && other.cxctDocumentoPK != null) || (this.cxctDocumentoPK != null && !this.cxctDocumentoPK.equals(other.cxctDocumentoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CxctDocumento[ cxctDocumentoPK=" + cxctDocumentoPK + " ]";
    }
    
}
