/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb6
 */
@Embeddable
public class CxcmFormapagoPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "CXCFPG_codigo")
    private String cXCFPGcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENCIA_codigo")
    private String gENCIAcodigo;

    public CxcmFormapagoPK() {
    }

    public CxcmFormapagoPK(String cXCFPGcodigo, String gENCIAcodigo) {
        this.cXCFPGcodigo = cXCFPGcodigo;
        this.gENCIAcodigo = gENCIAcodigo;
    }

    public String getCXCFPGcodigo() {
        return cXCFPGcodigo;
    }

    public void setCXCFPGcodigo(String cXCFPGcodigo) {
        this.cXCFPGcodigo = cXCFPGcodigo;
    }

    public String getGENCIAcodigo() {
        return gENCIAcodigo;
    }

    public void setGENCIAcodigo(String gENCIAcodigo) {
        this.gENCIAcodigo = gENCIAcodigo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cXCFPGcodigo != null ? cXCFPGcodigo.hashCode() : 0);
        hash += (gENCIAcodigo != null ? gENCIAcodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CxcmFormapagoPK)) {
            return false;
        }
        CxcmFormapagoPK other = (CxcmFormapagoPK) object;
        if ((this.cXCFPGcodigo == null && other.cXCFPGcodigo != null) || (this.cXCFPGcodigo != null && !this.cXCFPGcodigo.equals(other.cXCFPGcodigo))) {
            return false;
        }
        if ((this.gENCIAcodigo == null && other.gENCIAcodigo != null) || (this.gENCIAcodigo != null && !this.gENCIAcodigo.equals(other.gENCIAcodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CxcmFormapagoPK[ cXCFPGcodigo=" + cXCFPGcodigo + ", gENCIAcodigo=" + gENCIAcodigo + " ]";
    }
    
}
