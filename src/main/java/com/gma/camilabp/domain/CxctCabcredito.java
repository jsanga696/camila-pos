/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb5
 */
@Entity
@Table(name = "CXCT_CABCREDITO")
@NamedQueries({
    @NamedQuery(name = "CxctCabcredito.findAll", query = "SELECT c FROM CxctCabcredito c")})
public class CxctCabcredito implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CxctCabcreditoPK cxctCabcreditoPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENOFI_codigo")
    private String gENOFIcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "GENMOD_codigocr")
    private String gENMODcodigocr;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "GENTDO_codigocr")
    private String gENTDOcodigocr;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "CXCCCR_numdocumcr")
    private String cXCCCRnumdocumcr;
    @Size(max = 5)
    @Column(name = "GENMON_codigo")
    private String gENMONcodigo;
    @Size(max = 6)
    @Column(name = "CXCCLI_codigo")
    private String cXCCLIcodigo;
    @Size(max = 10)
    @Column(name = "cxcccr_rutdistcli")
    private String cxcccrRutdistcli;
    @Size(max = 10)
    @Column(name = "cxcccr_rutvtacli")
    private String cxcccrRutvtacli;
    @Column(name = "CXCCCR_fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cXCCCRfecha;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "CXCCCR_montomov")
    private BigDecimal cXCCCRmontomov;
    @Size(max = 50)
    @Column(name = "CXCCCR_descripcio")
    private String cXCCCRdescripcio;
    @Size(max = 200)
    @Column(name = "CXCCCR_detalle")
    private String cXCCCRdetalle;
    @Size(max = 3)
    @Column(name = "CXCCCR_coddocrela")
    private String cXCCCRcoddocrela;
    @Size(max = 9)
    @Column(name = "CXCCCR_numdocrela")
    private String cXCCCRnumdocrela;
    @Column(name = "CXCCCR_secdocrela")
    private Integer cXCCCRsecdocrela;
    @Size(max = 1)
    @Column(name = "CXCCCR_estado")
    private String cXCCCRestado;
    @Size(max = 6)
    @Column(name = "FACVDR_codigo")
    private String fACVDRcodigo;
    @Size(max = 6)
    @Column(name = "FACREC_codigo")
    private String fACRECcodigo;
    @Size(max = 9)
    @Column(name = "FACDGA_numrecicaj")
    private String fACDGAnumrecicaj;
    @Size(max = 50)
    @Column(name = "CXCCCR_numpapelet")
    private String cXCCCRnumpapelet;
    @Size(max = 2)
    @Column(name = "GENBAN_codigo")
    private String gENBANcodigo;
    @Size(max = 2)
    @Column(name = "GENBAN_tipo")
    private String gENBANtipo;
    @Size(max = 15)
    @Column(name = "BANCTB_numerocta")
    private String bANCTBnumerocta;
    @Size(max = 50)
    @Column(name = "CXCCCR_usuaingreg")
    private String cXCCCRusuaingreg;
    @Size(max = 50)
    @Column(name = "CXCCCR_usuamodreg")
    private String cXCCCRusuamodreg;
    @Column(name = "CXCCCR_fechingreg")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cXCCCRfechingreg;
    @Column(name = "CXCCCR_fechmodreg")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cXCCCRfechmodreg;
    @Size(max = 1)
    @Column(name = "CXCCCR_estcontab")
    private String cXCCCRestcontab;
    @Size(max = 15)
    @Column(name = "CXCCCR_codigocont")
    private String cXCCCRcodigocont;
    @Size(max = 1)
    @Column(name = "CXCCCR_tipocobro")
    private String cXCCCRtipocobro;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cxctCabcredito", fetch = FetchType.LAZY)
    private List<CxctDetdocumcr> cxctDetdocumcrList;

    public CxctCabcredito() {
    }

    public CxctCabcredito(CxctCabcreditoPK cxctCabcreditoPK) {
        this.cxctCabcreditoPK = cxctCabcreditoPK;
    }

    public CxctCabcredito(CxctCabcreditoPK cxctCabcreditoPK, String gENOFIcodigo, String gENMODcodigocr, String gENTDOcodigocr, String cXCCCRnumdocumcr) {
        this.cxctCabcreditoPK = cxctCabcreditoPK;
        this.gENOFIcodigo = gENOFIcodigo;
        this.gENMODcodigocr = gENMODcodigocr;
        this.gENTDOcodigocr = gENTDOcodigocr;
        this.cXCCCRnumdocumcr = cXCCCRnumdocumcr;
    }

    public CxctCabcredito(String gENCIAcodigo, int cXCCCRnumsecuen) {
        this.cxctCabcreditoPK = new CxctCabcreditoPK(gENCIAcodigo, cXCCCRnumsecuen);
    }

    public CxctCabcreditoPK getCxctCabcreditoPK() {
        return cxctCabcreditoPK;
    }

    public void setCxctCabcreditoPK(CxctCabcreditoPK cxctCabcreditoPK) {
        this.cxctCabcreditoPK = cxctCabcreditoPK;
    }

    public String getGENOFIcodigo() {
        return gENOFIcodigo;
    }

    public void setGENOFIcodigo(String gENOFIcodigo) {
        this.gENOFIcodigo = gENOFIcodigo;
    }

    public String getGENMODcodigocr() {
        return gENMODcodigocr;
    }

    public void setGENMODcodigocr(String gENMODcodigocr) {
        this.gENMODcodigocr = gENMODcodigocr;
    }

    public String getGENTDOcodigocr() {
        return gENTDOcodigocr;
    }

    public void setGENTDOcodigocr(String gENTDOcodigocr) {
        this.gENTDOcodigocr = gENTDOcodigocr;
    }

    public String getCXCCCRnumdocumcr() {
        return cXCCCRnumdocumcr;
    }

    public void setCXCCCRnumdocumcr(String cXCCCRnumdocumcr) {
        this.cXCCCRnumdocumcr = cXCCCRnumdocumcr;
    }

    public String getGENMONcodigo() {
        return gENMONcodigo;
    }

    public void setGENMONcodigo(String gENMONcodigo) {
        this.gENMONcodigo = gENMONcodigo;
    }

    public String getCXCCLIcodigo() {
        return cXCCLIcodigo;
    }

    public void setCXCCLIcodigo(String cXCCLIcodigo) {
        this.cXCCLIcodigo = cXCCLIcodigo;
    }

    public String getCxcccrRutdistcli() {
        return cxcccrRutdistcli;
    }

    public void setCxcccrRutdistcli(String cxcccrRutdistcli) {
        this.cxcccrRutdistcli = cxcccrRutdistcli;
    }

    public String getCxcccrRutvtacli() {
        return cxcccrRutvtacli;
    }

    public void setCxcccrRutvtacli(String cxcccrRutvtacli) {
        this.cxcccrRutvtacli = cxcccrRutvtacli;
    }

    public Date getCXCCCRfecha() {
        return cXCCCRfecha;
    }

    public void setCXCCCRfecha(Date cXCCCRfecha) {
        this.cXCCCRfecha = cXCCCRfecha;
    }

    public BigDecimal getCXCCCRmontomov() {
        return cXCCCRmontomov;
    }

    public void setCXCCCRmontomov(BigDecimal cXCCCRmontomov) {
        this.cXCCCRmontomov = cXCCCRmontomov;
    }

    public String getCXCCCRdescripcio() {
        return cXCCCRdescripcio;
    }

    public void setCXCCCRdescripcio(String cXCCCRdescripcio) {
        this.cXCCCRdescripcio = cXCCCRdescripcio;
    }

    public String getCXCCCRdetalle() {
        return cXCCCRdetalle;
    }

    public void setCXCCCRdetalle(String cXCCCRdetalle) {
        this.cXCCCRdetalle = cXCCCRdetalle;
    }

    public String getCXCCCRcoddocrela() {
        return cXCCCRcoddocrela;
    }

    public void setCXCCCRcoddocrela(String cXCCCRcoddocrela) {
        this.cXCCCRcoddocrela = cXCCCRcoddocrela;
    }

    public String getCXCCCRnumdocrela() {
        return cXCCCRnumdocrela;
    }

    public void setCXCCCRnumdocrela(String cXCCCRnumdocrela) {
        this.cXCCCRnumdocrela = cXCCCRnumdocrela;
    }

    public Integer getCXCCCRsecdocrela() {
        return cXCCCRsecdocrela;
    }

    public void setCXCCCRsecdocrela(Integer cXCCCRsecdocrela) {
        this.cXCCCRsecdocrela = cXCCCRsecdocrela;
    }

    public String getCXCCCRestado() {
        return cXCCCRestado;
    }

    public void setCXCCCRestado(String cXCCCRestado) {
        this.cXCCCRestado = cXCCCRestado;
    }

    public String getFACVDRcodigo() {
        return fACVDRcodigo;
    }

    public void setFACVDRcodigo(String fACVDRcodigo) {
        this.fACVDRcodigo = fACVDRcodigo;
    }

    public String getFACRECcodigo() {
        return fACRECcodigo;
    }

    public void setFACRECcodigo(String fACRECcodigo) {
        this.fACRECcodigo = fACRECcodigo;
    }

    public String getFACDGAnumrecicaj() {
        return fACDGAnumrecicaj;
    }

    public void setFACDGAnumrecicaj(String fACDGAnumrecicaj) {
        this.fACDGAnumrecicaj = fACDGAnumrecicaj;
    }

    public String getCXCCCRnumpapelet() {
        return cXCCCRnumpapelet;
    }

    public void setCXCCCRnumpapelet(String cXCCCRnumpapelet) {
        this.cXCCCRnumpapelet = cXCCCRnumpapelet;
    }

    public String getGENBANcodigo() {
        return gENBANcodigo;
    }

    public void setGENBANcodigo(String gENBANcodigo) {
        this.gENBANcodigo = gENBANcodigo;
    }

    public String getGENBANtipo() {
        return gENBANtipo;
    }

    public void setGENBANtipo(String gENBANtipo) {
        this.gENBANtipo = gENBANtipo;
    }

    public String getBANCTBnumerocta() {
        return bANCTBnumerocta;
    }

    public void setBANCTBnumerocta(String bANCTBnumerocta) {
        this.bANCTBnumerocta = bANCTBnumerocta;
    }

    public String getCXCCCRusuaingreg() {
        return cXCCCRusuaingreg;
    }

    public void setCXCCCRusuaingreg(String cXCCCRusuaingreg) {
        this.cXCCCRusuaingreg = cXCCCRusuaingreg;
    }

    public String getCXCCCRusuamodreg() {
        return cXCCCRusuamodreg;
    }

    public void setCXCCCRusuamodreg(String cXCCCRusuamodreg) {
        this.cXCCCRusuamodreg = cXCCCRusuamodreg;
    }

    public Date getCXCCCRfechingreg() {
        return cXCCCRfechingreg;
    }

    public void setCXCCCRfechingreg(Date cXCCCRfechingreg) {
        this.cXCCCRfechingreg = cXCCCRfechingreg;
    }

    public Date getCXCCCRfechmodreg() {
        return cXCCCRfechmodreg;
    }

    public void setCXCCCRfechmodreg(Date cXCCCRfechmodreg) {
        this.cXCCCRfechmodreg = cXCCCRfechmodreg;
    }

    public String getCXCCCRestcontab() {
        return cXCCCRestcontab;
    }

    public void setCXCCCRestcontab(String cXCCCRestcontab) {
        this.cXCCCRestcontab = cXCCCRestcontab;
    }

    public String getCXCCCRcodigocont() {
        return cXCCCRcodigocont;
    }

    public void setCXCCCRcodigocont(String cXCCCRcodigocont) {
        this.cXCCCRcodigocont = cXCCCRcodigocont;
    }

    public String getCXCCCRtipocobro() {
        return cXCCCRtipocobro;
    }

    public void setCXCCCRtipocobro(String cXCCCRtipocobro) {
        this.cXCCCRtipocobro = cXCCCRtipocobro;
    }

    public List<CxctDetdocumcr> getCxctDetdocumcrList() {
        return cxctDetdocumcrList;
    }

    public void setCxctDetdocumcrList(List<CxctDetdocumcr> cxctDetdocumcrList) {
        this.cxctDetdocumcrList = cxctDetdocumcrList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cxctCabcreditoPK != null ? cxctCabcreditoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CxctCabcredito)) {
            return false;
        }
        CxctCabcredito other = (CxctCabcredito) object;
        if ((this.cxctCabcreditoPK == null && other.cxctCabcreditoPK != null) || (this.cxctCabcreditoPK != null && !this.cxctCabcreditoPK.equals(other.cxctCabcreditoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CxctCabcredito[ cxctCabcreditoPK=" + cxctCabcreditoPK + " ]";
    }
    
}
