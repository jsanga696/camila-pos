/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb5
 */
@Entity
@Table(name = "GENR_MONEDA")
@NamedQueries({
    @NamedQuery(name = "GenrMoneda.findAll", query = "SELECT g FROM GenrMoneda g")})
public class GenrMoneda implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "GENMON_codigo")
    private String gENMONcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "GENMON_nombre")
    private String gENMONnombre;
    @Column(name = "GENMON_balance")
    private Character gENMONbalance;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "GENMON_cuentacont")
    private String gENMONcuentacont;

    public GenrMoneda() {
    }

    public GenrMoneda(String gENMONcodigo) {
        this.gENMONcodigo = gENMONcodigo;
    }

    public GenrMoneda(String gENMONcodigo, String gENMONnombre, String gENMONcuentacont) {
        this.gENMONcodigo = gENMONcodigo;
        this.gENMONnombre = gENMONnombre;
        this.gENMONcuentacont = gENMONcuentacont;
    }

    public String getGENMONcodigo() {
        return gENMONcodigo;
    }

    public void setGENMONcodigo(String gENMONcodigo) {
        this.gENMONcodigo = gENMONcodigo;
    }

    public String getGENMONnombre() {
        return gENMONnombre;
    }

    public void setGENMONnombre(String gENMONnombre) {
        this.gENMONnombre = gENMONnombre;
    }

    public Character getGENMONbalance() {
        return gENMONbalance;
    }

    public void setGENMONbalance(Character gENMONbalance) {
        this.gENMONbalance = gENMONbalance;
    }

    public String getGENMONcuentacont() {
        return gENMONcuentacont;
    }

    public void setGENMONcuentacont(String gENMONcuentacont) {
        this.gENMONcuentacont = gENMONcuentacont;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gENMONcodigo != null ? gENMONcodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GenrMoneda)) {
            return false;
        }
        GenrMoneda other = (GenrMoneda) object;
        if ((this.gENMONcodigo == null && other.gENMONcodigo != null) || (this.gENMONcodigo != null && !this.gENMONcodigo.equals(other.gENMONcodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.GenrMoneda[ gENMONcodigo=" + gENMONcodigo + " ]";
    }
    
}
