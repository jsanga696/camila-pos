/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb6
 */
@Entity
@Table(name = "CBPGENM_DENOMINACION")
@NamedQueries({
    @NamedQuery(name = "CbpgenmDenominacion.findAll", query = "SELECT c FROM CbpgenmDenominacion c")})
public class CbpgenmDenominacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "genden_codigo")
    private Long gendenCodigo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "genden_tipo")
    private Long gendenTipo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "genden_tipodes")
    private String gendenTipodes;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "genden_detalle")
    private String gendenDetalle;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "genden_valor")
    private BigDecimal gendenValor;
    @Column(name = "genden_tiporegistro")
    private Long gendenTiporegistro;
    @Transient
    private Long cantidad = 0L;
    @Transient
    private BigDecimal total= new BigDecimal("0.00");

    public CbpgenmDenominacion() {
    }

    public CbpgenmDenominacion(Long gendenCodigo) {
        this.gendenCodigo = gendenCodigo;
    }

    public CbpgenmDenominacion(Long gendenCodigo, Long gendenTipo, String gendenTipodes, String gendenDetalle, BigDecimal gendenValor) {
        this.gendenCodigo = gendenCodigo;
        this.gendenTipo = gendenTipo;
        this.gendenTipodes = gendenTipodes;
        this.gendenDetalle = gendenDetalle;
        this.gendenValor = gendenValor;
    }

    public Long getGendenCodigo() {
        return gendenCodigo;
    }

    public void setGendenCodigo(Long gendenCodigo) {
        this.gendenCodigo = gendenCodigo;
    }

    public Long getGendenTipo() {
        return gendenTipo;
    }

    public void setGendenTipo(Long gendenTipo) {
        this.gendenTipo = gendenTipo;
    }

    public String getGendenTipodes() {
        return gendenTipodes;
    }

    public void setGendenTipodes(String gendenTipodes) {
        this.gendenTipodes = gendenTipodes;
    }

    public String getGendenDetalle() {
        return gendenDetalle;
    }

    public void setGendenDetalle(String gendenDetalle) {
        this.gendenDetalle = gendenDetalle;
    }

    public BigDecimal getGendenValor() {
        return gendenValor;
    }

    public void setGendenValor(BigDecimal gendenValor) {
        this.gendenValor = gendenValor;
    }

    public Long getGendenTiporegistro() {
        return gendenTiporegistro;
    }

    public void setGendenTiporegistro(Long gendenTiporegistro) {
        this.gendenTiporegistro = gendenTiporegistro;
    }

    public Long getCantidad() {
        return cantidad;
    }

    public void setCantidad(Long cantidad) {
        this.cantidad = cantidad;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gendenCodigo != null ? gendenCodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbpgenmDenominacion)) {
            return false;
        }
        CbpgenmDenominacion other = (CbpgenmDenominacion) object;
        if ((this.gendenCodigo == null && other.gendenCodigo != null) || (this.gendenCodigo != null && !this.gendenCodigo.equals(other.gendenCodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CbpgenmDenominacion[ gendenCodigo=" + gendenCodigo + " ]";
    }
    
}
