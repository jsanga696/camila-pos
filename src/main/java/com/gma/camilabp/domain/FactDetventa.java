/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb2
 */
@Entity
@Table(name = "FACT_DETVENTA")
@NamedQueries({
    @NamedQuery(name = "FactDetventa.findAll", query = "SELECT f FROM FactDetventa f")})
public class FactDetventa implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected FactDetventaPK factDetventaPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "GENMOD_codigo", nullable = false, length = 3)
    private String gENMODcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "GENTDO_codigo", nullable = false, length = 3)
    private String gENTDOcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "CXCRUT_codigo", nullable = false, length = 10)
    private String cXCRUTcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "FACCVT_numdocumen", nullable = false, length = 9)
    private String fACCVTnumdocumen;
    @Size(max = 4)
    @Column(name = "FACCVT_codbodega", length = 4)
    private String fACCVTcodbodega;
    @Size(max = 15)
    @Column(name = "INVITM_codigo", length = 15)
    private String iNVITMcodigo;
    @Size(max = 4)
    @Column(name = "FACTVA_tipovtafac", length = 4)
    private String fACTVAtipovtafac;
    @Column(name = "FACDVT_linreladoc")
    private Short fACDVTlinreladoc;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "FACDVT_canticaja", precision = 19, scale = 4)
    private BigDecimal fACDVTcanticaja;
    @Column(name = "FACDVT_cantiunida", precision = 19, scale = 4)
    private BigDecimal fACDVTcantiunida;
    @Column(name = "FACDVT_cantifunci", precision = 19, scale = 4)
    private BigDecimal fACDVTcantifunci;
    @Column(name = "FACDVT_cantfunpen", precision = 19, scale = 4)
    private BigDecimal fACDVTcantfunpen;
    @Column(name = "FACDVT_cantcamfun", precision = 19, scale = 4)
    private BigDecimal fACDVTcantcamfun;
    @Column(name = "FACDVT_cantfundev", precision = 19, scale = 4)
    private BigDecimal fACDVTcantfundev;
    @Column(name = "FACDVT_cantegres", precision = 19, scale = 4)
    private BigDecimal fACDVTcantegres;
    @Column(name = "FACDVT_unidembala")
    private Integer fACDVTunidembala;
    @Column(name = "FACDVT_preciomov", precision = 19, scale = 4)
    private BigDecimal fACDVTpreciomov;
    @Column(name = "FACDVT_precespmov", precision = 19, scale = 4)
    private BigDecimal fACDVTprecespmov;
    @Column(name = "FACDVT_cosingrmov", precision = 19, scale = 4)
    private BigDecimal fACDVTcosingrmov;
    @Column(name = "FACDVT_porcdescto", precision = 19, scale = 4)
    private BigDecimal fACDVTporcdescto;
    @Column(name = "FACDVT_subtotamov", precision = 19, scale = 4)
    private BigDecimal fACDVTsubtotamov;
    @Column(name = "FACDVT_desctomov", precision = 19, scale = 4)
    private BigDecimal fACDVTdesctomov;
    @Column(name = "FACDVT_totimpumov", precision = 19, scale = 4)
    private BigDecimal fACDVTtotimpumov;
    @Column(name = "FACDVT_netomov", precision = 19, scale = 4)
    private BigDecimal fACDVTnetomov;
    @Size(max = 3)
    @Column(name = "FACDVT_codmotivo", length = 3)
    private String fACDVTcodmotivo;
    @Column(name = "FACDVT_canpenplan", precision = 16, scale = 4)
    private BigDecimal fACDVTcanpenplan;
    @Column(name = "FACDVT_totalpeso", precision = 19, scale = 4)
    private BigDecimal fACDVTtotalpeso;
    @Column(name = "FACDVT_totalvolum", precision = 19, scale = 4)
    private BigDecimal fACDVTtotalvolum;
    @Size(max = 10)
    @Column(name = "INVUPR_unibasvent", length = 10)
    private String iNVUPRunibasvent;
    @Size(max = 10)
    @Column(name = "INVUPR_unirelvent", length = 10)
    private String iNVUPRunirelvent;
    @Column(name = "FACDVT_stkpendegr", precision = 16, scale = 4)
    private BigDecimal fACDVTstkpendegr;
    @Column(name = "FACDVT_stkpending", precision = 16, scale = 4)
    private BigDecimal fACDVTstkpending;
    @Size(max = 1)
    @Column(name = "FACDVT_estadoc", length = 1)
    private String fACDVTestadoc;
    @Size(max = 1)
    @Column(name = "FACDVT_tipodetall", length = 1)
    private String fACDVTtipodetall;
    @Size(max = 1)
    @Column(name = "FACDVT_estensamb", length = 1)
    private String fACDVTestensamb;
    @Column(name = "FACDVT_linrelaens")
    private Integer fACDVTlinrelaens;
    @Column(name = "FACDVT_pordsctoad", precision = 16, scale = 4)
    private BigDecimal fACDVTpordsctoad;
    @Size(max = 1)
    @Column(name = "FACDVT_estplantot", length = 1)
    private String fACDVTestplantot;
    @Column(name = "FACDVT_porcencartera", precision = 16, scale = 14)
    private BigDecimal fACDVTporcencartera;
    @Column(name = "FACDVT_fechaemisi")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fACDVTfechaemisi;
    @Size(max = 30)
    @Column(name = "FACDVT_cuentacontable", length = 30)
    private String fACDVTcuentacontable;
    @Size(max = 500)
    @Column(name = "FACDVT_detallefactserv", length = 500)
    private String fACDVTdetallefactserv;
    @Column(name = "FACDVT_impueiva", precision = 19, scale = 4)
    private BigDecimal fACDVTimpueiva;
    @Column(name = "FACDVT_impueice", precision = 19, scale = 4)
    private BigDecimal fACDVTimpueice;
    @Column(name = "FACDVT_impueser", precision = 19, scale = 4)
    private BigDecimal fACDVTimpueser;
    @JoinColumns({
        @JoinColumn(name = "FACCVT_numsecuenc", referencedColumnName = "FACCVT_numsecuenc", nullable = false, insertable = false, updatable = false)
        , @JoinColumn(name = "GENOFI_codigo", referencedColumnName = "GENOFI_codigo", nullable = false, insertable = false, updatable = false)
        , @JoinColumn(name = "GENCIA_codigo", referencedColumnName = "GENCIA_codigo", nullable = false, insertable = false, updatable = false)})
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private FactCabventa factCabventa;

    public FactDetventa() {
    }

    public FactDetventa(FactDetventaPK factDetventaPK) {
        this.factDetventaPK = factDetventaPK;
    }

    public FactDetventa(FactDetventaPK factDetventaPK, String gENMODcodigo, String gENTDOcodigo, String cXCRUTcodigo, String fACCVTnumdocumen) {
        this.factDetventaPK = factDetventaPK;
        this.gENMODcodigo = gENMODcodigo;
        this.gENTDOcodigo = gENTDOcodigo;
        this.cXCRUTcodigo = cXCRUTcodigo;
        this.fACCVTnumdocumen = fACCVTnumdocumen;
    }

    public FactDetventa(String gENCIAcodigo, int fACCVTnumsecuenc, String gENOFIcodigo, short fACDVTlinea) {
        this.factDetventaPK = new FactDetventaPK(gENCIAcodigo, fACCVTnumsecuenc, gENOFIcodigo, fACDVTlinea);
    }

    public FactDetventaPK getFactDetventaPK() {
        return factDetventaPK;
    }

    public void setFactDetventaPK(FactDetventaPK factDetventaPK) {
        this.factDetventaPK = factDetventaPK;
    }

    public String getGENMODcodigo() {
        return gENMODcodigo;
    }

    public void setGENMODcodigo(String gENMODcodigo) {
        this.gENMODcodigo = gENMODcodigo;
    }

    public String getGENTDOcodigo() {
        return gENTDOcodigo;
    }

    public void setGENTDOcodigo(String gENTDOcodigo) {
        this.gENTDOcodigo = gENTDOcodigo;
    }

    public String getCXCRUTcodigo() {
        return cXCRUTcodigo;
    }

    public void setCXCRUTcodigo(String cXCRUTcodigo) {
        this.cXCRUTcodigo = cXCRUTcodigo;
    }

    public String getFACCVTnumdocumen() {
        return fACCVTnumdocumen;
    }

    public void setFACCVTnumdocumen(String fACCVTnumdocumen) {
        this.fACCVTnumdocumen = fACCVTnumdocumen;
    }

    public String getFACCVTcodbodega() {
        return fACCVTcodbodega;
    }

    public void setFACCVTcodbodega(String fACCVTcodbodega) {
        this.fACCVTcodbodega = fACCVTcodbodega;
    }

    public String getINVITMcodigo() {
        return iNVITMcodigo;
    }

    public void setINVITMcodigo(String iNVITMcodigo) {
        this.iNVITMcodigo = iNVITMcodigo;
    }

    public String getFACTVAtipovtafac() {
        return fACTVAtipovtafac;
    }

    public void setFACTVAtipovtafac(String fACTVAtipovtafac) {
        this.fACTVAtipovtafac = fACTVAtipovtafac;
    }

    public Short getFACDVTlinreladoc() {
        return fACDVTlinreladoc;
    }

    public void setFACDVTlinreladoc(Short fACDVTlinreladoc) {
        this.fACDVTlinreladoc = fACDVTlinreladoc;
    }

    public BigDecimal getFACDVTcanticaja() {
        return fACDVTcanticaja;
    }

    public void setFACDVTcanticaja(BigDecimal fACDVTcanticaja) {
        this.fACDVTcanticaja = fACDVTcanticaja;
    }

    public BigDecimal getFACDVTcantiunida() {
        return fACDVTcantiunida;
    }

    public void setFACDVTcantiunida(BigDecimal fACDVTcantiunida) {
        this.fACDVTcantiunida = fACDVTcantiunida;
    }

    public BigDecimal getFACDVTcantifunci() {
        return fACDVTcantifunci;
    }

    public void setFACDVTcantifunci(BigDecimal fACDVTcantifunci) {
        this.fACDVTcantifunci = fACDVTcantifunci;
    }

    public BigDecimal getFACDVTcantfunpen() {
        return fACDVTcantfunpen;
    }

    public void setFACDVTcantfunpen(BigDecimal fACDVTcantfunpen) {
        this.fACDVTcantfunpen = fACDVTcantfunpen;
    }

    public BigDecimal getFACDVTcantcamfun() {
        return fACDVTcantcamfun;
    }

    public void setFACDVTcantcamfun(BigDecimal fACDVTcantcamfun) {
        this.fACDVTcantcamfun = fACDVTcantcamfun;
    }

    public BigDecimal getFACDVTcantfundev() {
        return fACDVTcantfundev;
    }

    public void setFACDVTcantfundev(BigDecimal fACDVTcantfundev) {
        this.fACDVTcantfundev = fACDVTcantfundev;
    }

    public BigDecimal getFACDVTcantegres() {
        return fACDVTcantegres;
    }

    public void setFACDVTcantegres(BigDecimal fACDVTcantegres) {
        this.fACDVTcantegres = fACDVTcantegres;
    }

    public Integer getFACDVTunidembala() {
        return fACDVTunidembala;
    }

    public void setFACDVTunidembala(Integer fACDVTunidembala) {
        this.fACDVTunidembala = fACDVTunidembala;
    }

    public BigDecimal getFACDVTpreciomov() {
        return fACDVTpreciomov;
    }

    public void setFACDVTpreciomov(BigDecimal fACDVTpreciomov) {
        this.fACDVTpreciomov = fACDVTpreciomov;
    }

    public BigDecimal getFACDVTprecespmov() {
        return fACDVTprecespmov;
    }

    public void setFACDVTprecespmov(BigDecimal fACDVTprecespmov) {
        this.fACDVTprecespmov = fACDVTprecespmov;
    }

    public BigDecimal getFACDVTcosingrmov() {
        return fACDVTcosingrmov;
    }

    public void setFACDVTcosingrmov(BigDecimal fACDVTcosingrmov) {
        this.fACDVTcosingrmov = fACDVTcosingrmov;
    }

    public BigDecimal getFACDVTporcdescto() {
        return fACDVTporcdescto;
    }

    public void setFACDVTporcdescto(BigDecimal fACDVTporcdescto) {
        this.fACDVTporcdescto = fACDVTporcdescto;
    }

    public BigDecimal getFACDVTsubtotamov() {
        return fACDVTsubtotamov;
    }

    public void setFACDVTsubtotamov(BigDecimal fACDVTsubtotamov) {
        this.fACDVTsubtotamov = fACDVTsubtotamov;
    }

    public BigDecimal getFACDVTdesctomov() {
        return fACDVTdesctomov;
    }

    public void setFACDVTdesctomov(BigDecimal fACDVTdesctomov) {
        this.fACDVTdesctomov = fACDVTdesctomov;
    }

    public BigDecimal getFACDVTtotimpumov() {
        return fACDVTtotimpumov;
    }

    public void setFACDVTtotimpumov(BigDecimal fACDVTtotimpumov) {
        this.fACDVTtotimpumov = fACDVTtotimpumov;
    }

    public BigDecimal getFACDVTnetomov() {
        return fACDVTnetomov;
    }

    public void setFACDVTnetomov(BigDecimal fACDVTnetomov) {
        this.fACDVTnetomov = fACDVTnetomov;
    }

    public String getFACDVTcodmotivo() {
        return fACDVTcodmotivo;
    }

    public void setFACDVTcodmotivo(String fACDVTcodmotivo) {
        this.fACDVTcodmotivo = fACDVTcodmotivo;
    }

    public BigDecimal getFACDVTcanpenplan() {
        return fACDVTcanpenplan;
    }

    public void setFACDVTcanpenplan(BigDecimal fACDVTcanpenplan) {
        this.fACDVTcanpenplan = fACDVTcanpenplan;
    }

    public BigDecimal getFACDVTtotalpeso() {
        return fACDVTtotalpeso;
    }

    public void setFACDVTtotalpeso(BigDecimal fACDVTtotalpeso) {
        this.fACDVTtotalpeso = fACDVTtotalpeso;
    }

    public BigDecimal getFACDVTtotalvolum() {
        return fACDVTtotalvolum;
    }

    public void setFACDVTtotalvolum(BigDecimal fACDVTtotalvolum) {
        this.fACDVTtotalvolum = fACDVTtotalvolum;
    }

    public String getINVUPRunibasvent() {
        return iNVUPRunibasvent;
    }

    public void setINVUPRunibasvent(String iNVUPRunibasvent) {
        this.iNVUPRunibasvent = iNVUPRunibasvent;
    }

    public String getINVUPRunirelvent() {
        return iNVUPRunirelvent;
    }

    public void setINVUPRunirelvent(String iNVUPRunirelvent) {
        this.iNVUPRunirelvent = iNVUPRunirelvent;
    }

    public BigDecimal getFACDVTstkpendegr() {
        return fACDVTstkpendegr;
    }

    public void setFACDVTstkpendegr(BigDecimal fACDVTstkpendegr) {
        this.fACDVTstkpendegr = fACDVTstkpendegr;
    }

    public BigDecimal getFACDVTstkpending() {
        return fACDVTstkpending;
    }

    public void setFACDVTstkpending(BigDecimal fACDVTstkpending) {
        this.fACDVTstkpending = fACDVTstkpending;
    }

    public String getFACDVTestadoc() {
        return fACDVTestadoc;
    }

    public void setFACDVTestadoc(String fACDVTestadoc) {
        this.fACDVTestadoc = fACDVTestadoc;
    }

    public String getFACDVTtipodetall() {
        return fACDVTtipodetall;
    }

    public void setFACDVTtipodetall(String fACDVTtipodetall) {
        this.fACDVTtipodetall = fACDVTtipodetall;
    }

    public String getFACDVTestensamb() {
        return fACDVTestensamb;
    }

    public void setFACDVTestensamb(String fACDVTestensamb) {
        this.fACDVTestensamb = fACDVTestensamb;
    }

    public Integer getFACDVTlinrelaens() {
        return fACDVTlinrelaens;
    }

    public void setFACDVTlinrelaens(Integer fACDVTlinrelaens) {
        this.fACDVTlinrelaens = fACDVTlinrelaens;
    }

    public BigDecimal getFACDVTpordsctoad() {
        return fACDVTpordsctoad;
    }

    public void setFACDVTpordsctoad(BigDecimal fACDVTpordsctoad) {
        this.fACDVTpordsctoad = fACDVTpordsctoad;
    }

    public String getFACDVTestplantot() {
        return fACDVTestplantot;
    }

    public void setFACDVTestplantot(String fACDVTestplantot) {
        this.fACDVTestplantot = fACDVTestplantot;
    }

    public BigDecimal getFACDVTporcencartera() {
        return fACDVTporcencartera;
    }

    public void setFACDVTporcencartera(BigDecimal fACDVTporcencartera) {
        this.fACDVTporcencartera = fACDVTporcencartera;
    }

    public Date getFACDVTfechaemisi() {
        return fACDVTfechaemisi;
    }

    public void setFACDVTfechaemisi(Date fACDVTfechaemisi) {
        this.fACDVTfechaemisi = fACDVTfechaemisi;
    }

    public String getFACDVTcuentacontable() {
        return fACDVTcuentacontable;
    }

    public void setFACDVTcuentacontable(String fACDVTcuentacontable) {
        this.fACDVTcuentacontable = fACDVTcuentacontable;
    }

    public String getFACDVTdetallefactserv() {
        return fACDVTdetallefactserv;
    }

    public void setFACDVTdetallefactserv(String fACDVTdetallefactserv) {
        this.fACDVTdetallefactserv = fACDVTdetallefactserv;
    }

    public BigDecimal getFACDVTimpueiva() {
        return fACDVTimpueiva;
    }

    public void setFACDVTimpueiva(BigDecimal fACDVTimpueiva) {
        this.fACDVTimpueiva = fACDVTimpueiva;
    }

    public BigDecimal getFACDVTimpueice() {
        return fACDVTimpueice;
    }

    public void setFACDVTimpueice(BigDecimal fACDVTimpueice) {
        this.fACDVTimpueice = fACDVTimpueice;
    }

    public BigDecimal getFACDVTimpueser() {
        return fACDVTimpueser;
    }

    public void setFACDVTimpueser(BigDecimal fACDVTimpueser) {
        this.fACDVTimpueser = fACDVTimpueser;
    }

    public FactCabventa getFactCabventa() {
        return factCabventa;
    }

    public void setFactCabventa(FactCabventa factCabventa) {
        this.factCabventa = factCabventa;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (factDetventaPK != null ? factDetventaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FactDetventa)) {
            return false;
        }
        FactDetventa other = (FactDetventa) object;
        if ((this.factDetventaPK == null && other.factDetventaPK != null) || (this.factDetventaPK != null && !this.factDetventaPK.equals(other.factDetventaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.FactDetventa[ factDetventaPK=" + factDetventaPK + " ]";
    }
    
}
