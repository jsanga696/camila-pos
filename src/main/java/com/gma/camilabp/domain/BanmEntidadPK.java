/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb6
 */
@Embeddable
public class BanmEntidadPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENCIA_codigo")
    private String genciaCodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "BANENT_tipo")
    private String banentTipo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "BANENT_codigo")
    private String banentCodigo;
    
    public BanmEntidadPK() {
    }

    public BanmEntidadPK(String genciaCodigo, String banentTipo, String banentCodigo) {
        this.genciaCodigo = genciaCodigo;
        this.banentTipo = banentTipo;
        this.banentCodigo = banentCodigo;
    }

    public String getGenciaCodigo() {
        return genciaCodigo;
    }

    public void setGenciaCodigo(String genciaCodigo) {
        this.genciaCodigo = genciaCodigo;
    }

    public String getBanentTipo() {
        return banentTipo;
    }

    public void setBanentTipo(String banentTipo) {
        this.banentTipo = banentTipo;
    }

    public String getBanentCodigo() {
        return banentCodigo;
    }

    public void setBanentCodigo(String banentCodigo) {
        this.banentCodigo = banentCodigo;
    }    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (genciaCodigo != null ? genciaCodigo.hashCode() : 0);
        hash += (banentTipo != null ? banentTipo.hashCode() : 0);
        hash += (banentCodigo != null ? banentCodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BanmEntidadPK)) {
            return false;
        }
        BanmEntidadPK other = (BanmEntidadPK) object;
        if ((this.genciaCodigo == null && other.genciaCodigo != null) || (this.genciaCodigo != null && !this.genciaCodigo.equals(other.genciaCodigo))) {
            return false;
        }
        if ((this.banentTipo == null && other.banentTipo != null) || (this.banentTipo != null && !this.banentTipo.equals(other.banentTipo))) {
            return false;
        }
        if ((this.banentCodigo == null && other.banentCodigo != null) || (this.banentCodigo != null && !this.banentCodigo.equals(other.banentCodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.BanmEntidadPK[ genciaCodigo=" + genciaCodigo + ", banentTipo=" + banentTipo + ", banentCodigo=" + banentCodigo + " ]";
    }
    
}
