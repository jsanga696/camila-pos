/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb5
 */
@Embeddable
public class CxctDetdocumcrPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENCIA_codigo")
    private String gENCIAcodigo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CXCDPG_numerosec")
    private int cXCDPGnumerosec;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CXCDPG_numlinea")
    private int cXCDPGnumlinea;

    public CxctDetdocumcrPK() {
    }

    public CxctDetdocumcrPK(String gENCIAcodigo, int cXCDPGnumerosec, int cXCDPGnumlinea) {
        this.gENCIAcodigo = gENCIAcodigo;
        this.cXCDPGnumerosec = cXCDPGnumerosec;
        this.cXCDPGnumlinea = cXCDPGnumlinea;
    }

    public String getGENCIAcodigo() {
        return gENCIAcodigo;
    }

    public void setGENCIAcodigo(String gENCIAcodigo) {
        this.gENCIAcodigo = gENCIAcodigo;
    }

    public int getCXCDPGnumerosec() {
        return cXCDPGnumerosec;
    }

    public void setCXCDPGnumerosec(int cXCDPGnumerosec) {
        this.cXCDPGnumerosec = cXCDPGnumerosec;
    }

    public int getCXCDPGnumlinea() {
        return cXCDPGnumlinea;
    }

    public void setCXCDPGnumlinea(int cXCDPGnumlinea) {
        this.cXCDPGnumlinea = cXCDPGnumlinea;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gENCIAcodigo != null ? gENCIAcodigo.hashCode() : 0);
        hash += (int) cXCDPGnumerosec;
        hash += (int) cXCDPGnumlinea;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CxctDetdocumcrPK)) {
            return false;
        }
        CxctDetdocumcrPK other = (CxctDetdocumcrPK) object;
        if ((this.gENCIAcodigo == null && other.gENCIAcodigo != null) || (this.gENCIAcodigo != null && !this.gENCIAcodigo.equals(other.gENCIAcodigo))) {
            return false;
        }
        if (this.cXCDPGnumerosec != other.cXCDPGnumerosec) {
            return false;
        }
        if (this.cXCDPGnumlinea != other.cXCDPGnumlinea) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CxctDetdocumcrPK[ gENCIAcodigo=" + gENCIAcodigo + ", cXCDPGnumerosec=" + cXCDPGnumerosec + ", cXCDPGnumlinea=" + cXCDPGnumlinea + " ]";
    }
    
}
