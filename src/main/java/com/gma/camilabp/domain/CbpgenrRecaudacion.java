/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

/**
 *
 * @author Acer
 */
@Entity
@Table(name = "CBPGENR_RECAUDACION")
@NamedQueries({
    @NamedQuery(name = "CbpgenrRecaudacion.findAll", query = "SELECT c FROM CbpgenrRecaudacion c")})
public class CbpgenrRecaudacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "genrrec_id", nullable = false)
    private Long genrrecId;
    /*@Column(name = "FACSCA_idsececaja")
    private Long fACSCAidsececaja;*/
    @JoinColumn(name = "FACSCA_idsececaja", referencedColumnName = "FACSCA_idsececaja")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private GenmCaja caja;
    @Column(name = "segtur_numero")
    private Long segturNumero;
    @Size(max = 15)
    @Column(name = "GENPGE_codigo")
    private String gENPGEcodigo;
    @Size(max = 2)
    @Column(name = "BANENT_codigo")
    private String bANENTcodigo;
    @Size(max = 15)
    @Column(name = "BANCTA_codigo")
    private String bANCTAcodigo;
    @Size(max = 4)
    @Column(name = "GENCIA_codigo")
    private String gENCIAcodigo;
    @Size(max = 3)
    @Column(name = "BANTMV_codigo")
    private String bANTMVcodigo;
    @Size(max = 10)
    @Column(name = "genrrec_codigo")
    private String genrrecCodigo;
    @Column(name = "genrrec_ingreso")
    @Temporal(TemporalType.TIMESTAMP)
    private Date genrrecIngreso= new Date();
    @Column(name = "genrrec_movimiento")
    @Temporal(TemporalType.TIMESTAMP)
    private Date genrrecMovimiento= new Date();
    @Size(max = 15)
    @Column(name = "genrrec_forma")
    private String genrrecForma="EFE";
    @Size(max = 1)
    @Column(name = "genrrec_estado")
    private String genrrecEstado="A";
    @Size(max = 20)
    @Column(name = "genrrec_referencia")
    private String genrrecReferencia;
    @Size(max = 20)
    @Column(name = "genrrec_papeleta")
    private String genrrecPapeleta;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "genrrec_valor")
    private BigDecimal genrrecValor;
    @Size(max = 40)
    @Column(name = "genrrec_destinatario")
    private String genrrecDestinatario;
    @Size(max = 50)
    @Column(name = "genrrec_descripcion")
    private String genrrecDescripcion;
    @Size(max = 20)
    @Column(name = "genrrec_documento")
    private String genrrecDocumento;
    @Size(max = 20)
    @Column(name = "genrrec_conciliacion")
    private String genrrecConciliacion;
    @Size(max = 20)
    @Column(name = "genrrec_comprobante")
    private String genrrecComprobante;
    @Column(name = "genrrec_tipotransaccion")
    private Integer genrrecTipotransaccion;
    @JoinColumn(name = "FACSCA_cajatransfer", referencedColumnName = "FACSCA_idsececaja")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private GenmCaja cajaTransfer;
    @Column(name = "genrrec_codigo_ing")
    private String genrrecCodigoIng;
    //@Formula(value = "(SELECT m.BANTMV_nombre FROM BANM_TIPOSMOVIMIENTOS m WHERE m.BANTMV_origenmov ='BAN' AND m.BANTMV_ingresoegreso= (CASE WHEN genrrec_tipotransaccion=1 THEN 'I' WHEN genrrec_tipotransaccion=2 THEN 'E' WHEN genrrec_tipotransaccion=3 THEN 'E' END) AND m.BANTMV_tipoentidad=GENPGE_codigo AND m.BANTMV_codigo=BANTMV_codigo )")
    @Transient
    private String movimiento;

    public CbpgenrRecaudacion() {
    }

    public CbpgenrRecaudacion(Long genrrecId) {
        this.genrrecId = genrrecId;
    }
    
    public void tranformUpper(){
        this.genrrecDescripcion=this.genrrecDescripcion!=null?this.genrrecDescripcion.toUpperCase():"";
        this.genrrecComprobante=this.genrrecComprobante!=null?this.genrrecComprobante.toUpperCase():"";
        this.genrrecConciliacion=this.genrrecConciliacion!=null?this.genrrecConciliacion.toUpperCase():"";
        this.genrrecDocumento=this.genrrecDocumento!=null?this.genrrecDocumento.toUpperCase():"";
        this.genrrecDestinatario=this.genrrecDestinatario!=null?this.genrrecDestinatario.toUpperCase():"";
        this.genrrecReferencia=this.genrrecReferencia!=null?this.genrrecReferencia.toUpperCase():"";
    }
    public Long getGenrrecId() {
        return genrrecId;
    }

    public void setGenrrecId(Long genrrecId) {
        this.genrrecId = genrrecId;
    }

    /*public Long getFACSCAidsececaja() {
        return fACSCAidsececaja;
    }

    public void setFACSCAidsececaja(Long fACSCAidsececaja) {
        this.fACSCAidsececaja = fACSCAidsececaja;
    }*/

    public GenmCaja getCaja() {
        return caja;
    }

    public void setCaja(GenmCaja caja) {
        this.caja = caja;
    }    

    public Long getSegturNumero() {
        return segturNumero;
    }

    public void setSegturNumero(Long segturNumero) {
        this.segturNumero = segturNumero;
    }

    public String getGENPGEcodigo() {
        return gENPGEcodigo;
    }

    public void setGENPGEcodigo(String gENPGEcodigo) {
        this.gENPGEcodigo = gENPGEcodigo;
    }

    public String getBANENTcodigo() {
        return bANENTcodigo;
    }

    public void setBANENTcodigo(String bANENTcodigo) {
        this.bANENTcodigo = bANENTcodigo;
    }

    public String getBANCTAcodigo() {
        return bANCTAcodigo;
    }

    public void setBANCTAcodigo(String bANCTAcodigo) {
        this.bANCTAcodigo = bANCTAcodigo;
    }

    public String getGENCIAcodigo() {
        return gENCIAcodigo;
    }

    public void setGENCIAcodigo(String gENCIAcodigo) {
        this.gENCIAcodigo = gENCIAcodigo;
    }

    public String getBANTMVcodigo() {
        return bANTMVcodigo;
    }

    public void setBANTMVcodigo(String bANTMVcodigo) {
        this.bANTMVcodigo = bANTMVcodigo;
    }

    public String getGenrrecCodigo() {
        return genrrecCodigo;
    }

    public void setGenrrecCodigo(String genrrecCodigo) {
        this.genrrecCodigo = genrrecCodigo;
    }

    public Date getGenrrecIngreso() {
        return genrrecIngreso;
    }

    public void setGenrrecIngreso(Date genrrecIngreso) {
        this.genrrecIngreso = genrrecIngreso;
    }

    public Date getGenrrecMovimiento() {
        return genrrecMovimiento;
    }

    public void setGenrrecMovimiento(Date genrrecMovimiento) {
        this.genrrecMovimiento = genrrecMovimiento;
    }

    public String getGenrrecForma() {
        return genrrecForma;
    }

    public void setGenrrecForma(String genrrecForma) {
        this.genrrecForma = genrrecForma;
    }

    public String getGenrrecEstado() {
        return genrrecEstado;
    }

    public void setGenrrecEstado(String genrrecEstado) {
        this.genrrecEstado = genrrecEstado;
    }

    public String getGenrrecReferencia() {
        return genrrecReferencia;
    }

    public void setGenrrecReferencia(String genrrecReferencia) {
        this.genrrecReferencia = genrrecReferencia;
    }

    public String getGenrrecPapeleta() {
        return genrrecPapeleta;
    }

    public void setGenrrecPapeleta(String genrrecPapeleta) {
        this.genrrecPapeleta = genrrecPapeleta;
    }

    public BigDecimal getGenrrecValor() {
        return genrrecValor;
    }

    public void setGenrrecValor(BigDecimal genrrecValor) {
        this.genrrecValor = genrrecValor;
    }

    public String getGenrrecDestinatario() {
        return genrrecDestinatario;
    }

    public void setGenrrecDestinatario(String genrrecDestinatario) {
        this.genrrecDestinatario = genrrecDestinatario;
    }

    public String getGenrrecDescripcion() {
        return genrrecDescripcion;
    }

    public void setGenrrecDescripcion(String genrrecDescripcion) {
        this.genrrecDescripcion = genrrecDescripcion;
    }

    public String getGenrrecDocumento() {
        return genrrecDocumento;
    }

    public void setGenrrecDocumento(String genrrecDocumento) {
        this.genrrecDocumento = genrrecDocumento;
    }

    public String getGenrrecConciliacion() {
        return genrrecConciliacion;
    }

    public void setGenrrecConciliacion(String genrrecConciliacion) {
        this.genrrecConciliacion = genrrecConciliacion;
    }

    public String getGenrrecComprobante() {
        return genrrecComprobante;
    }

    public void setGenrrecComprobante(String genrrecComprobante) {
        this.genrrecComprobante = genrrecComprobante;
    }

    public Integer getGenrrecTipotransaccion() {
        return genrrecTipotransaccion;
    }

    public void setGenrrecTipotransaccion(Integer genrrecTipotransaccion) {
        this.genrrecTipotransaccion = genrrecTipotransaccion;
    }

    public GenmCaja getCajaTransfer() {
        return cajaTransfer;
    }

    public void setCajaTransfer(GenmCaja cajaTransfer) {
        this.cajaTransfer = cajaTransfer;
    }

    public String getMovimiento() {
        return movimiento;
    }

    public void setMovimiento(String movimiento) {
        this.movimiento = movimiento;
    }

    public String getGenrrecCodigoIng() {
        return genrrecCodigoIng;
    }

    public void setGenrrecCodigoIng(String genrrecCodigoIng) {
        this.genrrecCodigoIng = genrrecCodigoIng;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (genrrecId != null ? genrrecId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbpgenrRecaudacion)) {
            return false;
        }
        CbpgenrRecaudacion other = (CbpgenrRecaudacion) object;
        if ((this.genrrecId == null && other.genrrecId != null) || (this.genrrecId != null && !this.genrrecId.equals(other.genrrecId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CbpgenrRecaudacion[ genrrecId=" + genrrecId + " ]";
    }
    
}
