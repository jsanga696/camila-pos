/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb
 */
@Entity
@Table(name = "CBPGENR_TABLAENUM")
@NamedQueries({
    @NamedQuery(name = "CbpgenrTablaenum.findAll", query = "SELECT c FROM CbpgenrTablaenum c")})
public class CbpgenrTablaenum implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "genten_codigo")
    private Long gentenCodigo;
    @Size(max = 50)
    @Column(name = "genten_enumerado")
    private String gentenEnumerado;
    @Column(name = "genten_indice")
    private Integer gentenIndice;
    @Size(max = 100)
    @Column(name = "genten_descripcion")
    private String gentenDescripcion;
    @Column(name = "genten_orden")
    private Integer gentenOrden;
    @Column(name = "genten_creafecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date gentenCreafecha;
    @Column(name = "genten_creauser")
    private BigInteger gentenCreauser;

    public CbpgenrTablaenum() {
    }

    public CbpgenrTablaenum(Long gentenCodigo) {
        this.gentenCodigo = gentenCodigo;
    }

    public Long getGentenCodigo() {
        return gentenCodigo;
    }

    public void setGentenCodigo(Long gentenCodigo) {
        this.gentenCodigo = gentenCodigo;
    }

    public String getGentenEnumerado() {
        return gentenEnumerado;
    }

    public void setGentenEnumerado(String gentenEnumerado) {
        this.gentenEnumerado = gentenEnumerado;
    }

    public Integer getGentenIndice() {
        return gentenIndice;
    }

    public void setGentenIndice(Integer gentenIndice) {
        this.gentenIndice = gentenIndice;
    }

    public String getGentenDescripcion() {
        return gentenDescripcion;
    }

    public void setGentenDescripcion(String gentenDescripcion) {
        this.gentenDescripcion = gentenDescripcion;
    }

    public Integer getGentenOrden() {
        return gentenOrden;
    }

    public void setGentenOrden(Integer gentenOrden) {
        this.gentenOrden = gentenOrden;
    }

    public Date getGentenCreafecha() {
        return gentenCreafecha;
    }

    public void setGentenCreafecha(Date gentenCreafecha) {
        this.gentenCreafecha = gentenCreafecha;
    }

    public BigInteger getGentenCreauser() {
        return gentenCreauser;
    }

    public void setGentenCreauser(BigInteger gentenCreauser) {
        this.gentenCreauser = gentenCreauser;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gentenCodigo != null ? gentenCodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbpgenrTablaenum)) {
            return false;
        }
        CbpgenrTablaenum other = (CbpgenrTablaenum) object;
        if ((this.gentenCodigo == null && other.gentenCodigo != null) || (this.gentenCodigo != null && !this.gentenCodigo.equals(other.gentenCodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CbpgenrTablaenum[ gentenCodigo=" + gentenCodigo + " ]";
    }
    
}
