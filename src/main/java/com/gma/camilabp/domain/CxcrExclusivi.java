/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb6
 */
@Entity
@Table(name = "CXCR_EXCLUSIVI")
@NamedQueries({
    @NamedQuery(name = "CxcrExclusivi.findAll", query = "SELECT c FROM CxcrExclusivi c")})
public class CxcrExclusivi implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CxcrExclusiviPK cxcrExclusiviPK;
    @Size(max = 30)
    @Column(name = "CXCEXC_descripcio")
    private String cXCEXCdescripcio;
    /*@OneToMany(cascade = CascadeType.ALL, mappedBy = "cxcrExclusivi", fetch = FetchType.LAZY)
    private Collection<CxcmCliente> cxcmClienteCollection;*/

    public CxcrExclusivi() {
    }

    public CxcrExclusivi(CxcrExclusiviPK cxcrExclusiviPK) {
        this.cxcrExclusiviPK = cxcrExclusiviPK;
    }

    public CxcrExclusivi(String gENCIAcodigo, String cXCEXCcodigo) {
        this.cxcrExclusiviPK = new CxcrExclusiviPK(gENCIAcodigo, cXCEXCcodigo);
    }

    public CxcrExclusiviPK getCxcrExclusiviPK() {
        return cxcrExclusiviPK;
    }

    public void setCxcrExclusiviPK(CxcrExclusiviPK cxcrExclusiviPK) {
        this.cxcrExclusiviPK = cxcrExclusiviPK;
    }

    public String getCXCEXCdescripcio() {
        return cXCEXCdescripcio;
    }

    public void setCXCEXCdescripcio(String cXCEXCdescripcio) {
        this.cXCEXCdescripcio = cXCEXCdescripcio;
    }

    /*public Collection<CxcmCliente> getCxcmClienteCollection() {
        return cxcmClienteCollection;
    }

    public void setCxcmClienteCollection(Collection<CxcmCliente> cxcmClienteCollection) {
        this.cxcmClienteCollection = cxcmClienteCollection;
    }*/

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cxcrExclusiviPK != null ? cxcrExclusiviPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CxcrExclusivi)) {
            return false;
        }
        CxcrExclusivi other = (CxcrExclusivi) object;
        if ((this.cxcrExclusiviPK == null && other.cxcrExclusiviPK != null) || (this.cxcrExclusiviPK != null && !this.cxcrExclusiviPK.equals(other.cxcrExclusiviPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CxcrExclusivi[ cxcrExclusiviPK=" + cxcrExclusiviPK + " ]";
    }
    
}
