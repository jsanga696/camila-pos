/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb5
 */
@Entity
@Table(name = "CXPR_ESTABLEPROVEE")
@NamedQueries({
    @NamedQuery(name = "CxprEstableprovee.findAll", query = "SELECT c FROM CxprEstableprovee c")})
public class CxprEstableprovee implements Serializable {

    private static final long serialVersionUID = 1L;
    @Size(max = 4)
    @Column(name = "GENCIA_codigo")
    private String gENCIAcodigo;
    @Size(max = 4)
    @Column(name = "GENOFI_codigo")
    private String gENOFIcodigo;
    @Size(max = 8)
    @Column(name = "CXPPRO_codigo")
    private String cXPPROcodigo;
    @Size(max = 1)
    @Column(name = "CXPPRO_tiposerie")
    private String cXPPROtiposerie;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_SECUENCIA")
    private Long idSecuencia;
    @Size(max = 13)
    @Column(name = "CXPPRO_imprenta")
    private String cXPPROimprenta;
    @Size(max = 13)
    @Column(name = "CXPPRO_autorizacion")
    private String cXPPROautorizacion;
    @Size(max = 3)
    @Column(name = "CXPPRO_codserie")
    private String cXPPROcodserie;
    @Size(max = 3)
    @Column(name = "CXPPRO_codestableci")
    private String cXPPROcodestableci;
    @Column(name = "CXPPRO_fechavalidez")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cXPPROfechavalidez;
    @Size(max = 1)
    @Column(name = "CXPPRO_estado")
    private String cXPPROestado;
    @Column(name = "CXPPRO_numsecdigitos")
    private Integer cXPPROnumsecdigitos;
    @Column(name = "CXPPRO_numsecfindocu")
    private Integer cXPPROnumsecfindocu;
    @Size(max = 1)
    @Column(name = "CXPPRO_docuelectronico")
    private String cXPPROdocuelectronico;

    public CxprEstableprovee() {
    }

    public CxprEstableprovee(Long idSecuencia) {
        this.idSecuencia = idSecuencia;
    }

    public String getGENCIAcodigo() {
        return gENCIAcodigo;
    }

    public void setGENCIAcodigo(String gENCIAcodigo) {
        this.gENCIAcodigo = gENCIAcodigo;
    }

    public String getGENOFIcodigo() {
        return gENOFIcodigo;
    }

    public void setGENOFIcodigo(String gENOFIcodigo) {
        this.gENOFIcodigo = gENOFIcodigo;
    }

    public String getCXPPROcodigo() {
        return cXPPROcodigo;
    }

    public void setCXPPROcodigo(String cXPPROcodigo) {
        this.cXPPROcodigo = cXPPROcodigo;
    }

    public String getCXPPROtiposerie() {
        return cXPPROtiposerie;
    }

    public void setCXPPROtiposerie(String cXPPROtiposerie) {
        this.cXPPROtiposerie = cXPPROtiposerie;
    }

    public String getCXPPROimprenta() {
        return cXPPROimprenta;
    }

    public void setCXPPROimprenta(String cXPPROimprenta) {
        this.cXPPROimprenta = cXPPROimprenta;
    }

    public Long getIdSecuencia() {
        return idSecuencia;
    }

    public void setIdSecuencia(Long idSecuencia) {
        this.idSecuencia = idSecuencia;
    }

    public String getCXPPROautorizacion() {
        return cXPPROautorizacion;
    }

    public void setCXPPROautorizacion(String cXPPROautorizacion) {
        this.cXPPROautorizacion = cXPPROautorizacion;
    }

    public String getCXPPROcodserie() {
        return cXPPROcodserie;
    }

    public void setCXPPROcodserie(String cXPPROcodserie) {
        this.cXPPROcodserie = cXPPROcodserie;
    }

    public String getCXPPROcodestableci() {
        return cXPPROcodestableci;
    }

    public void setCXPPROcodestableci(String cXPPROcodestableci) {
        this.cXPPROcodestableci = cXPPROcodestableci;
    }

    public Date getCXPPROfechavalidez() {
        return cXPPROfechavalidez;
    }

    public void setCXPPROfechavalidez(Date cXPPROfechavalidez) {
        this.cXPPROfechavalidez = cXPPROfechavalidez;
    }

    public String getCXPPROestado() {
        return cXPPROestado;
    }

    public void setCXPPROestado(String cXPPROestado) {
        this.cXPPROestado = cXPPROestado;
    }

    public Integer getCXPPROnumsecdigitos() {
        return cXPPROnumsecdigitos;
    }

    public void setCXPPROnumsecdigitos(Integer cXPPROnumsecdigitos) {
        this.cXPPROnumsecdigitos = cXPPROnumsecdigitos;
    }

    public Integer getCXPPROnumsecfindocu() {
        return cXPPROnumsecfindocu;
    }

    public void setCXPPROnumsecfindocu(Integer cXPPROnumsecfindocu) {
        this.cXPPROnumsecfindocu = cXPPROnumsecfindocu;
    }

    public String getCXPPROdocuelectronico() {
        return cXPPROdocuelectronico;
    }

    public void setCXPPROdocuelectronico(String cXPPROdocuelectronico) {
        this.cXPPROdocuelectronico = cXPPROdocuelectronico;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSecuencia != null ? idSecuencia.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CxprEstableprovee)) {
            return false;
        }
        CxprEstableprovee other = (CxprEstableprovee) object;
        if ((this.idSecuencia == null && other.idSecuencia != null) || (this.idSecuencia != null && !this.idSecuencia.equals(other.idSecuencia))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CxprEstableprovee[ idSecuencia=" + idSecuencia + " ]";
    }
    
}
