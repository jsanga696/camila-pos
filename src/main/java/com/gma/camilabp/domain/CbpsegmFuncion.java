/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb
 */
@Entity
@Table(name = "CBPSEGM_FUNCION")
@NamedQueries({
    @NamedQuery(name = "CbpsegmFuncion.findAll", query = "SELECT c FROM CbpsegmFuncion c")})
public class CbpsegmFuncion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "segfun_codigo")
    private Long segfunCodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "segfun_descripcio")
    private String segfunDescripcio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "segfun_numregis")
    private short segfunNumregis;
    @Basic(optional = false)
    @NotNull
    @Column(name = "segfun_estado")
    private boolean segfunEstado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "segfun_creafecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date segfunCreafecha;
    @Basic(optional = false)
    @NotNull
    @Column(name = "segfun_creauser")
    private long segfunCreauser;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "segfunCodigo")
    private Collection<CbpsegmOpcion> cbpsegmOpcionCollection;

    public CbpsegmFuncion() {
    }

    public CbpsegmFuncion(Long segfunCodigo) {
        this.segfunCodigo = segfunCodigo;
    }

    public CbpsegmFuncion(Long segfunCodigo, String segfunDescripcio, short segfunNumregis, boolean segfunEstado, Date segfunCreafecha, long segfunCreauser) {
        this.segfunCodigo = segfunCodigo;
        this.segfunDescripcio = segfunDescripcio;
        this.segfunNumregis = segfunNumregis;
        this.segfunEstado = segfunEstado;
        this.segfunCreafecha = segfunCreafecha;
        this.segfunCreauser = segfunCreauser;
    }

    public Long getSegfunCodigo() {
        return segfunCodigo;
    }

    public void setSegfunCodigo(Long segfunCodigo) {
        this.segfunCodigo = segfunCodigo;
    }

    public String getSegfunDescripcio() {
        return segfunDescripcio;
    }

    public void setSegfunDescripcio(String segfunDescripcio) {
        this.segfunDescripcio = segfunDescripcio;
    }

    public short getSegfunNumregis() {
        return segfunNumregis;
    }

    public void setSegfunNumregis(short segfunNumregis) {
        this.segfunNumregis = segfunNumregis;
    }

    public boolean getSegfunEstado() {
        return segfunEstado;
    }

    public void setSegfunEstado(boolean segfunEstado) {
        this.segfunEstado = segfunEstado;
    }

    public Date getSegfunCreafecha() {
        return segfunCreafecha;
    }

    public void setSegfunCreafecha(Date segfunCreafecha) {
        this.segfunCreafecha = segfunCreafecha;
    }

    public long getSegfunCreauser() {
        return segfunCreauser;
    }

    public void setSegfunCreauser(long segfunCreauser) {
        this.segfunCreauser = segfunCreauser;
    }

    public Collection<CbpsegmOpcion> getCbpsegmOpcionCollection() {
        return cbpsegmOpcionCollection;
    }

    public void setCbpsegmOpcionCollection(Collection<CbpsegmOpcion> cbpsegmOpcionCollection) {
        this.cbpsegmOpcionCollection = cbpsegmOpcionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (segfunCodigo != null ? segfunCodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbpsegmFuncion)) {
            return false;
        }
        CbpsegmFuncion other = (CbpsegmFuncion) object;
        if ((this.segfunCodigo == null && other.segfunCodigo != null) || (this.segfunCodigo != null && !this.segfunCodigo.equals(other.segfunCodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CbpsegmFuncion[ segfunCodigo=" + segfunCodigo + " ]";
    }
    
}
