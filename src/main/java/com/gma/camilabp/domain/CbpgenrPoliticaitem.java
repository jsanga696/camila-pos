/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author Acer
 */
@Entity
@Table(name = "CBPGENR_POLITICAITEM")
@NamedQueries({
    @NamedQuery(name = "CbpgenrPoliticaitem.findAll", query = "SELECT c FROM CbpgenrPoliticaitem c")})
public class CbpgenrPoliticaitem implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "genpit_codigo")
    private Long genpitCodigo;
    @Size(max = 15)
    @Column(name = "INVITM_codigo")
    private String iNVITMcodigo;
    @Column(name = "genpit_estado")
    private Boolean genpitEstado= Boolean.TRUE;
    @Column(name = "genpit_grupo")
    private Long genpitGrupo= 0L;
    @JoinColumn(name = "genpol_codigo", referencedColumnName = "genpol_codigo")
    @ManyToOne(fetch = FetchType.LAZY)
    private CbpgenrPoliticabase genpolCodigo;

    public CbpgenrPoliticaitem() {
    }

    public CbpgenrPoliticaitem(Long genpitCodigo) {
        this.genpitCodigo = genpitCodigo;
    }

    public CbpgenrPoliticaitem(String iNVITMcodigo) {
        this.iNVITMcodigo = iNVITMcodigo;
    }

    public Long getGenpitCodigo() {
        return genpitCodigo;
    }

    public void setGenpitCodigo(Long genpitCodigo) {
        this.genpitCodigo = genpitCodigo;
    }

    public String getINVITMcodigo() {
        return iNVITMcodigo;
    }

    public void setINVITMcodigo(String iNVITMcodigo) {
        this.iNVITMcodigo = iNVITMcodigo;
    }

    public Boolean getGenpitEstado() {
        return genpitEstado;
    }

    public void setGenpitEstado(Boolean genpitEstado) {
        this.genpitEstado = genpitEstado;
    }

    public Long getGenpitGrupo() {
        return genpitGrupo;
    }

    public void setGenpitGrupo(Long genpitGrupo) {
        this.genpitGrupo = genpitGrupo;
    }

    public CbpgenrPoliticabase getGenpolCodigo() {
        return genpolCodigo;
    }

    public void setGenpolCodigo(CbpgenrPoliticabase genpolCodigo) {
        this.genpolCodigo = genpolCodigo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (genpitCodigo != null ? genpitCodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbpgenrPoliticaitem)) {
            return false;
        }
        CbpgenrPoliticaitem other = (CbpgenrPoliticaitem) object;
        if ((this.genpitCodigo == null && other.genpitCodigo != null) || (this.genpitCodigo != null && !this.genpitCodigo.equals(other.genpitCodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CbpgenrPoliticaitem[ genpitCodigo=" + genpitCodigo + " ]";
    }
    
}
