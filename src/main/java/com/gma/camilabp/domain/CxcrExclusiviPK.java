/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb6
 */
@Embeddable
public class CxcrExclusiviPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENCIA_codigo")
    private String gENCIAcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "CXCEXC_codigo")
    private String cXCEXCcodigo;

    public CxcrExclusiviPK() {
    }

    public CxcrExclusiviPK(String gENCIAcodigo, String cXCEXCcodigo) {
        this.gENCIAcodigo = gENCIAcodigo;
        this.cXCEXCcodigo = cXCEXCcodigo;
    }

    public String getGENCIAcodigo() {
        return gENCIAcodigo;
    }

    public void setGENCIAcodigo(String gENCIAcodigo) {
        this.gENCIAcodigo = gENCIAcodigo;
    }

    public String getCXCEXCcodigo() {
        return cXCEXCcodigo;
    }

    public void setCXCEXCcodigo(String cXCEXCcodigo) {
        this.cXCEXCcodigo = cXCEXCcodigo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gENCIAcodigo != null ? gENCIAcodigo.hashCode() : 0);
        hash += (cXCEXCcodigo != null ? cXCEXCcodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CxcrExclusiviPK)) {
            return false;
        }
        CxcrExclusiviPK other = (CxcrExclusiviPK) object;
        if ((this.gENCIAcodigo == null && other.gENCIAcodigo != null) || (this.gENCIAcodigo != null && !this.gENCIAcodigo.equals(other.gENCIAcodigo))) {
            return false;
        }
        if ((this.cXCEXCcodigo == null && other.cXCEXCcodigo != null) || (this.cXCEXCcodigo != null && !this.cXCEXCcodigo.equals(other.cXCEXCcodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CxcrExclusiviPK[ gENCIAcodigo=" + gENCIAcodigo + ", cXCEXCcodigo=" + cXCEXCcodigo + " ]";
    }
    
}
