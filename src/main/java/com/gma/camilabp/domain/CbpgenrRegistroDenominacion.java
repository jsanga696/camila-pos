/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author userdb6
 */
@Entity
@Table(name = "CBPGENR_REGISTRO_DENOMINACION")
@NamedQueries({
    @NamedQuery(name = "CbpgenrRegistroDenominacion.findAll", query = "SELECT c FROM CbpgenrRegistroDenominacion c")})
public class CbpgenrRegistroDenominacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "genrde_codigo")
    private Long genrdeCodigo;
    /**
     * 1: INGRESO
     * 2: CIERRE
     */
    @Basic(optional = false)
    @NotNull
    @Column(name = "genrde_tipo")
    private Long genrdeTipo=1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "genrde_moneda")
    private BigDecimal genrdeMoneda= new BigDecimal("0.00");
    @Basic(optional = false)
    @NotNull
    @Column(name = "genrde_billete")
    private BigDecimal genrdeBillete= new BigDecimal("0.00");
    @Basic(optional = false)
    @NotNull
    @Column(name = "genrde_fijo")
    private BigDecimal genrdeFijo= new BigDecimal("0.00");
    @Basic(optional = false)
    @NotNull
    @Column(name = "genrde_monto")
    private BigDecimal genrdeMonto= new BigDecimal("0.00");
    @Basic(optional = false)
    @NotNull
    @Column(name = "genrde_fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date genrdeFecha;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "genrdeCodigo", fetch = FetchType.LAZY)
    private Collection<CbpgenrDetregistroDenominacion> cbpgenrDetregistroDenominacionCollection;
    @JoinColumn(name = "segtur_codigo", referencedColumnName = "segtur_codigo")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private CbpsegmTurno segturCodigo;
    @Column(name = "genrde_estado")
    private Boolean estado= Boolean.TRUE;
    @Column(name = "genrde_motivo")
    private String genrdeMotivo;
    @Column(name = "genrde_monetario")
    private BigDecimal genrdeMonetario= new BigDecimal("0.00");
    @Column(name = "genrde_cheque")
    private BigDecimal genrdeCheque= new BigDecimal("0.00");
    @Column(name = "genrde_documento")
    private BigDecimal genrdeDocumento= new BigDecimal("0.00");

    public CbpgenrRegistroDenominacion() {
    }

    public CbpgenrRegistroDenominacion(Long genrdeCodigo) {
        this.genrdeCodigo = genrdeCodigo;
    }

    public CbpgenrRegistroDenominacion(Long genrdeCodigo, Long genrdeTipo, BigDecimal genrdeMonto, Date genrdeFecha) {
        this.genrdeCodigo = genrdeCodigo;
        this.genrdeTipo = genrdeTipo;
        this.genrdeMonto = genrdeMonto;
        this.genrdeFecha = genrdeFecha;
    }

    public Long getGenrdeCodigo() {
        return genrdeCodigo;
    }

    public void setGenrdeCodigo(Long genrdeCodigo) {
        this.genrdeCodigo = genrdeCodigo;
    }

    public Long getGenrdeTipo() {
        return genrdeTipo;
    }

    public void setGenrdeTipo(Long genrdeTipo) {
        this.genrdeTipo = genrdeTipo;
    }

    public BigDecimal getGenrdeMoneda() {
        return genrdeMoneda;
    }

    public void setGenrdeMoneda(BigDecimal genrdeMoneda) {
        this.genrdeMoneda = genrdeMoneda;
    }

    public BigDecimal getGenrdeBillete() {
        return genrdeBillete;
    }

    public void setGenrdeBillete(BigDecimal genrdeBillete) {
        this.genrdeBillete = genrdeBillete;
    }

    public BigDecimal getGenrdeFijo() {
        return genrdeFijo;
    }

    public void setGenrdeFijo(BigDecimal genrdeFijo) {
        this.genrdeFijo = genrdeFijo;
    }

    public BigDecimal getGenrdeMonto() {
        return genrdeMonto;
    }

    public void setGenrdeMonto(BigDecimal genrdeMonto) {
        this.genrdeMonto = genrdeMonto;
    }

    public Date getGenrdeFecha() {
        return genrdeFecha;
    }

    public void setGenrdeFecha(Date genrdeFecha) {
        this.genrdeFecha = genrdeFecha;
    }

    public Collection<CbpgenrDetregistroDenominacion> getCbpgenrDetregistroDenominacionCollection() {
        return cbpgenrDetregistroDenominacionCollection;
    }

    public void setCbpgenrDetregistroDenominacionCollection(Collection<CbpgenrDetregistroDenominacion> cbpgenrDetregistroDenominacionCollection) {
        this.cbpgenrDetregistroDenominacionCollection = cbpgenrDetregistroDenominacionCollection;
    }

    public CbpsegmTurno getSegturCodigo() {
        return segturCodigo;
    }

    public void setSegturCodigo(CbpsegmTurno segturCodigo) {
        this.segturCodigo = segturCodigo;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public String getGenrdeMotivo() {
        return genrdeMotivo;
    }

    public void setGenrdeMotivo(String genrdeMotivo) {
        this.genrdeMotivo = genrdeMotivo;
    }

    public BigDecimal getGenrdeMonetario() {
        return genrdeMonetario;
    }

    public void setGenrdeMonetario(BigDecimal genrdeMonetario) {
        this.genrdeMonetario = genrdeMonetario;
    }

    public BigDecimal getGenrdeCheque() {
        return genrdeCheque;
    }

    public void setGenrdeCheque(BigDecimal genrdeCheque) {
        this.genrdeCheque = genrdeCheque;
    }

    public BigDecimal getGenrdeDocumento() {
        return genrdeDocumento;
    }

    public void setGenrdeDocumento(BigDecimal genrdeDocumento) {
        this.genrdeDocumento = genrdeDocumento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (genrdeCodigo != null ? genrdeCodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbpgenrRegistroDenominacion)) {
            return false;
        }
        CbpgenrRegistroDenominacion other = (CbpgenrRegistroDenominacion) object;
        if ((this.genrdeCodigo == null && other.genrdeCodigo != null) || (this.genrdeCodigo != null && !this.genrdeCodigo.equals(other.genrdeCodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CbpgenrRegistroDenominacion[ genrdeCodigo=" + genrdeCodigo + " ]";
    }
    
}
