/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb6
 */
@Embeddable
public class CxctDocumentoPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENCIA_codigo")
    private String gENCIAcodigo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CXCDEU_numesecuen")
    private Long cXCDEUnumesecuen;

    public CxctDocumentoPK() {
    }

    public CxctDocumentoPK(String gENCIAcodigo, long cXCDEUnumesecuen) {
        this.gENCIAcodigo = gENCIAcodigo;
        this.cXCDEUnumesecuen = cXCDEUnumesecuen;
    }

    public String getGENCIAcodigo() {
        return gENCIAcodigo;
    }

    public void setGENCIAcodigo(String gENCIAcodigo) {
        this.gENCIAcodigo = gENCIAcodigo;
    }

    public Long getCXCDEUnumesecuen() {
        return cXCDEUnumesecuen;
    }

    public void setCXCDEUnumesecuen(Long cXCDEUnumesecuen) {
        this.cXCDEUnumesecuen = cXCDEUnumesecuen;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gENCIAcodigo != null ? gENCIAcodigo.hashCode() : 0);
        hash += cXCDEUnumesecuen;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CxctDocumentoPK)) {
            return false;
        }
        CxctDocumentoPK other = (CxctDocumentoPK) object;
        if ((this.gENCIAcodigo == null && other.gENCIAcodigo != null) || (this.gENCIAcodigo != null && !this.gENCIAcodigo.equals(other.gENCIAcodigo))) {
            return false;
        }
        if (this.cXCDEUnumesecuen != other.cXCDEUnumesecuen) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CxctDocumentoPK[ gENCIAcodigo=" + gENCIAcodigo + ", cXCDEUnumesecuen=" + cXCDEUnumesecuen + " ]";
    }
    
}
