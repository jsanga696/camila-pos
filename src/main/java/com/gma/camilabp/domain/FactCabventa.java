/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import com.gma.camilabp.web.util.Utilidades;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb2
 */
@Entity
@Table(name = "FACT_CABVENTA")
@NamedQueries({
    @NamedQuery(name = "FactCabventa.findAll", query = "SELECT f FROM FactCabventa f")
    , @NamedQuery(name = "FactCabventa.findByGENCIAcodigo", query = "SELECT f FROM FactCabventa f WHERE f.factCabventaPK.gENCIAcodigo = :gENCIAcodigo")
    , @NamedQuery(name = "FactCabventa.findByFACCVTnumsecuenc", query = "SELECT f FROM FactCabventa f WHERE f.factCabventaPK.fACCVTnumsecuenc = :fACCVTnumsecuenc")
    , @NamedQuery(name = "FactCabventa.findByGENOFIcodigo", query = "SELECT f FROM FactCabventa f WHERE f.factCabventaPK.gENOFIcodigo = :gENOFIcodigo")})
public class FactCabventa implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected FactCabventaPK factCabventaPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "GENMOD_codigo", nullable = false, length = 3)
    private String gENMODcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "GENTDO_codigo", nullable = false, length = 3)
    private String gENTDOcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "CXCRUT_codigo", nullable = false, length = 10)
    private String cXCRUTcodigo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FACCVT_numdocumen", nullable = false)
    private int fACCVTnumdocumen;
    @Column(name = "FACCVT_numrelplan")
    private Integer fACCVTnumrelplan;
    @Column(name = "FACCVT_secrelplan")
    private Integer fACCVTsecrelplan;
    @Size(max = 3)
    @Column(name = "GENTDO_docrelacio", length = 3)
    private String gENTDOdocrelacio;
    @Size(max = 10)
    @Column(name = "CXCRUT_rutarela", length = 10)
    private String cXCRUTrutarela;
    @Column(name = "FACCVT_numdocrela")
    private Integer fACCVTnumdocrela;
    @Column(name = "FACCVT_secdocurel")
    private Long fACCVTsecdocurel;
    @Column(name = "FACCVT_numguiarem")
    private Integer fACCVTnumguiarem;
    @Column(name = "FACCVT_fechaemisi")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fACCVTfechaemisi;
    @Column(name = "FACCVT_fechapedid")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fACCVTfechapedid;
    @Column(name = "FACCVT_fechadespa")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fACCVTfechadespa;
    @Column(name = "FACCVT_fechaliqui")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fACCVTfechaliqui;
    @Size(max = 6)
    @Column(name = "FACVDR_codigo", length = 6)
    private String fACVDRcodigo;
    @Size(max = 6)
    @Column(name = "CXCCLI_codigo", length = 6)
    private String cXCCLIcodigo;
    @Size(max = 3)
    @Column(name = "CXCFPG_codigo", length = 3)
    private String cXCFPGcodigo;
    @Size(max = 2)
    @Column(name = "CXCTPG_codigo", length = 2)
    private String cXCTPGcodigo;
    @Size(max = 10)
    @Column(name = "FACCVT_codrutavta", length = 10)
    private String fACCVTcodrutavta;
    @Size(max = 10)
    @Column(name = "FACCVT_rutdistcli", length = 10)
    private String fACCVTrutdistcli;
    @Size(max = 6)
    @Column(name = "FACCVT_codprevend", length = 6)
    private String fACCVTcodprevend;
    @Column(name = "FACCVT_secuenped")
    private Integer fACCVTsecuenped;
    @Size(max = 50)
    @Column(name = "FACCVT_descricort", length = 50)
    private String fACCVTdescricort;
    @Size(max = 100)
    @Column(name = "FACCVT_descrilarg", length = 100)
    private String fACCVTdescrilarg;
    @Size(max = 5)
    @Column(name = "GENMON_codigo", length = 5)
    private String gENMONcodigo;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "FACCVT_cotizacion", precision = 16, scale = 4)
    private BigDecimal fACCVTcotizacion;
    @Size(max = 3)
    @Column(name = "FACCVT_motvdevolu", length = 3)
    private String fACCVTmotvdevolu;
    @Column(name = "FACCVT_porcdscto", precision = 19, scale = 4)
    private BigDecimal fACCVTporcdscto;
    @Column(name = "FACCVT_subtotamov", precision = 19, scale = 4)
    private BigDecimal fACCVTsubtotamov;
    @Column(name = "FACCVT_desctomov", precision = 19, scale = 4)
    private BigDecimal fACCVTdesctomov;
    @Column(name = "FACCVT_totimpumov", precision = 19, scale = 4)
    private BigDecimal fACCVTtotimpumov;
    @Column(name = "FACCVT_netomov", precision = 19, scale = 4)
    private BigDecimal fACCVTnetomov;
    @Column(name = "FACCVT_transpmov", precision = 19, scale = 4)
    private BigDecimal fACCVTtranspmov;
    @Column(name = "FACCVT_recargomov", precision = 19, scale = 4)
    private BigDecimal fACCVTrecargomov;
    @Size(max = 1)
    @Column(name = "FACCVT_estado", length = 1)
    private String fACCVTestado;
    @Size(max = 1)
    @Column(name = "FACCVT_estuso", length = 1)
    private String fACCVTestuso;
    @Size(max = 1)
    @Column(name = "FACCVT_estplandes", length = 1)
    private String fACCVTestplandes;
    @Size(max = 1)
    @Column(name = "FACCVT_estplanfdo", length = 1)
    private String fACCVTestplanfdo;
    @Size(max = 1)
    @Column(name = "FACCVT_estdespach", length = 1)
    private String fACCVTestdespach;
    @Column(name = "FACCVT_totalpeso", precision = 19, scale = 4)
    private BigDecimal fACCVTtotalpeso;
    @Column(name = "FACCVT_totalvolum", precision = 19, scale = 4)
    private BigDecimal fACCVTtotalvolum;
    @Size(max = 3)
    @Column(name = "FACCVT_usuaingreg", length = 3)
    private String fACCVTusuaingreg;
    @Size(max = 3)
    @Column(name = "FACCVT_usuamodreg", length = 3)
    private String fACCVTusuamodreg;
    @Column(name = "FACCVT_fechingreg")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fACCVTfechingreg;
    @Column(name = "FACCVT_fechmodreg")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fACCVTfechmodreg;
    @Size(max = 1)
    @Column(name = "FACCVT_estadocont", length = 1)
    private String fACCVTestadocont;
    @Size(max = 20)
    @Column(name = "FACCVT_transcontab", length = 20)
    private String fACCVTtranscontab;
    @Size(max = 3)
    @Column(name = "FACCVT_codtipped", length = 3)
    private String fACCVTcodtipped;
    @JoinColumn(name = "GENTAL_secuencia", referencedColumnName = "GENTAL_secuencia")
    @ManyToOne(fetch = FetchType.LAZY)
    private GenpTalonarios gENTALsecuencia;
//    @Column(name = "GENTAL_secuencia")
//    private Integer gENTALsecuencia;
    @Column(name = "FACCVT_Totunidpedi", precision = 19, scale = 4)
    private BigDecimal fACCVTTotunidpedi;
    @Size(max = 15)
    @Column(name = "FACDVT_listaprecio", length = 15)
    private String fACDVTlistaprecio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FACCVT_idsecuencia", nullable = false)
    private int fACCVTidsecuencia;
    @Size(max = 50)
    @Column(name = "FACCVT_autorizacion", length = 50)
    private String fACCVTautorizacion;
    @Column(name = "FACCVT_fechaautoriza")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fACCVTfechaautoriza;
    @Size(max = 50)
    @Column(name = "FACCVT_claveacceso", length = 50)
    private String fACCVTclaveacceso;
    @Size(max = 60)
    @Column(name = "FACGUI_autorizacion", length = 60)
    private String fACGUIautorizacion;
    @Column(name = "FACGUI_fechaautoriza")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fACGUIfechaautoriza;
    @Size(max = 60)
    @Column(name = "FACGUI_claveacceso", length = 60)
    private String fACGUIclaveacceso;
    @Size(max = 2)
    @Column(name = "GENTDO_tipocomproban", length = 2)
    private String gENTDOtipocomproban;
    @Size(max = 5)
    @Column(name = "FACCVT_codimpuesto", length = 5)
    private String fACCVTcodimpuesto;
    @Column(name = "FACCVT_impueiva", precision = 19, scale = 4)
    private BigDecimal fACCVTimpueiva;
    @Column(name = "FACCVT_impueice", precision = 19, scale = 4)
    private BigDecimal fACCVTimpueice;
    @Column(name = "FACCVT_impueser", precision = 19, scale = 4)
    private BigDecimal fACCVTimpueser;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "factCabventa", fetch = FetchType.LAZY)
    private List<FactDetventa> factDetventaList;

    public FactCabventa() {
    }

    public FactCabventa(FactCabventaPK factCabventaPK) {
        this.factCabventaPK = factCabventaPK;
    }

    public FactCabventa(FactCabventaPK factCabventaPK, String gENMODcodigo, String gENTDOcodigo, String cXCRUTcodigo, int fACCVTnumdocumen, int fACCVTidsecuencia) {
        this.factCabventaPK = factCabventaPK;
        this.gENMODcodigo = gENMODcodigo;
        this.gENTDOcodigo = gENTDOcodigo;
        this.cXCRUTcodigo = cXCRUTcodigo;
        this.fACCVTnumdocumen = fACCVTnumdocumen;
        this.fACCVTidsecuencia = fACCVTidsecuencia;
    }

    public FactCabventa(String gENCIAcodigo, int fACCVTnumsecuenc, String gENOFIcodigo) {
        this.factCabventaPK = new FactCabventaPK(gENCIAcodigo, fACCVTnumsecuenc, gENOFIcodigo);
    }

    public FactCabventaPK getFactCabventaPK() {
        return factCabventaPK;
    }

    public void setFactCabventaPK(FactCabventaPK factCabventaPK) {
        this.factCabventaPK = factCabventaPK;
    }

    public String getGENMODcodigo() {
        return gENMODcodigo;
    }

    public void setGENMODcodigo(String gENMODcodigo) {
        this.gENMODcodigo = gENMODcodigo;
    }

    public String getGENTDOcodigo() {
        return gENTDOcodigo;
    }

    public void setGENTDOcodigo(String gENTDOcodigo) {
        this.gENTDOcodigo = gENTDOcodigo;
    }

    public String getCXCRUTcodigo() {
        return cXCRUTcodigo;
    }

    public void setCXCRUTcodigo(String cXCRUTcodigo) {
        this.cXCRUTcodigo = cXCRUTcodigo;
    }

    public int getFACCVTnumdocumen() {
        return fACCVTnumdocumen;
    }

    public void setFACCVTnumdocumen(int fACCVTnumdocumen) {
        this.fACCVTnumdocumen = fACCVTnumdocumen;
    }

    public Integer getFACCVTnumrelplan() {
        return fACCVTnumrelplan;
    }

    public void setFACCVTnumrelplan(Integer fACCVTnumrelplan) {
        this.fACCVTnumrelplan = fACCVTnumrelplan;
    }

    public Integer getFACCVTsecrelplan() {
        return fACCVTsecrelplan;
    }

    public void setFACCVTsecrelplan(Integer fACCVTsecrelplan) {
        this.fACCVTsecrelplan = fACCVTsecrelplan;
    }

    public String getGENTDOdocrelacio() {
        return gENTDOdocrelacio;
    }

    public void setGENTDOdocrelacio(String gENTDOdocrelacio) {
        this.gENTDOdocrelacio = gENTDOdocrelacio;
    }

    public String getCXCRUTrutarela() {
        return cXCRUTrutarela;
    }

    public void setCXCRUTrutarela(String cXCRUTrutarela) {
        this.cXCRUTrutarela = cXCRUTrutarela;
    }

    public Integer getFACCVTnumdocrela() {
        return fACCVTnumdocrela;
    }

    public void setFACCVTnumdocrela(Integer fACCVTnumdocrela) {
        this.fACCVTnumdocrela = fACCVTnumdocrela;
    }

    public Long getFACCVTsecdocurel() {
        return fACCVTsecdocurel;
    }

    public void setFACCVTsecdocurel(Long fACCVTsecdocurel) {
        this.fACCVTsecdocurel = fACCVTsecdocurel;
    }

    public Integer getFACCVTnumguiarem() {
        return fACCVTnumguiarem;
    }

    public void setFACCVTnumguiarem(Integer fACCVTnumguiarem) {
        this.fACCVTnumguiarem = fACCVTnumguiarem;
    }

    public Date getFACCVTfechaemisi() {
        return fACCVTfechaemisi;
    }

    public void setFACCVTfechaemisi(Date fACCVTfechaemisi) {
        this.fACCVTfechaemisi = fACCVTfechaemisi;
    }

    public Date getFACCVTfechapedid() {
        return fACCVTfechapedid;
    }

    public void setFACCVTfechapedid(Date fACCVTfechapedid) {
        this.fACCVTfechapedid = fACCVTfechapedid;
    }

    public Date getFACCVTfechadespa() {
        return fACCVTfechadespa;
    }

    public void setFACCVTfechadespa(Date fACCVTfechadespa) {
        this.fACCVTfechadespa = fACCVTfechadespa;
    }

    public Date getFACCVTfechaliqui() {
        return fACCVTfechaliqui;
    }

    public void setFACCVTfechaliqui(Date fACCVTfechaliqui) {
        this.fACCVTfechaliqui = fACCVTfechaliqui;
    }

    public String getFACVDRcodigo() {
        return fACVDRcodigo;
    }

    public void setFACVDRcodigo(String fACVDRcodigo) {
        this.fACVDRcodigo = fACVDRcodigo;
    }

    public String getCXCCLIcodigo() {
        return cXCCLIcodigo;
    }

    public void setCXCCLIcodigo(String cXCCLIcodigo) {
        this.cXCCLIcodigo = cXCCLIcodigo;
    }

    public String getCXCFPGcodigo() {
        return cXCFPGcodigo;
    }

    public void setCXCFPGcodigo(String cXCFPGcodigo) {
        this.cXCFPGcodigo = cXCFPGcodigo;
    }

    public String getCXCTPGcodigo() {
        return cXCTPGcodigo;
    }

    public void setCXCTPGcodigo(String cXCTPGcodigo) {
        this.cXCTPGcodigo = cXCTPGcodigo;
    }

    public String getFACCVTcodrutavta() {
        return fACCVTcodrutavta;
    }

    public void setFACCVTcodrutavta(String fACCVTcodrutavta) {
        this.fACCVTcodrutavta = fACCVTcodrutavta;
    }

    public String getFACCVTrutdistcli() {
        return fACCVTrutdistcli;
    }

    public void setFACCVTrutdistcli(String fACCVTrutdistcli) {
        this.fACCVTrutdistcli = fACCVTrutdistcli;
    }

    public String getFACCVTcodprevend() {
        return fACCVTcodprevend;
    }

    public void setFACCVTcodprevend(String fACCVTcodprevend) {
        this.fACCVTcodprevend = fACCVTcodprevend;
    }

    public Integer getFACCVTsecuenped() {
        return fACCVTsecuenped;
    }

    public void setFACCVTsecuenped(Integer fACCVTsecuenped) {
        this.fACCVTsecuenped = fACCVTsecuenped;
    }

    public String getFACCVTdescricort() {
        return fACCVTdescricort;
    }

    public void setFACCVTdescricort(String fACCVTdescricort) {
        this.fACCVTdescricort = fACCVTdescricort;
    }

    public String getFACCVTdescrilarg() {
        return fACCVTdescrilarg;
    }

    public void setFACCVTdescrilarg(String fACCVTdescrilarg) {
        this.fACCVTdescrilarg = fACCVTdescrilarg;
    }

    public String getGENMONcodigo() {
        return gENMONcodigo;
    }

    public void setGENMONcodigo(String gENMONcodigo) {
        this.gENMONcodigo = gENMONcodigo;
    }

    public BigDecimal getFACCVTcotizacion() {
        return fACCVTcotizacion;
    }

    public void setFACCVTcotizacion(BigDecimal fACCVTcotizacion) {
        this.fACCVTcotizacion = fACCVTcotizacion;
    }

    public String getFACCVTmotvdevolu() {
        return fACCVTmotvdevolu;
    }

    public void setFACCVTmotvdevolu(String fACCVTmotvdevolu) {
        this.fACCVTmotvdevolu = fACCVTmotvdevolu;
    }

    public BigDecimal getFACCVTporcdscto() {
        return fACCVTporcdscto;
    }

    public void setFACCVTporcdscto(BigDecimal fACCVTporcdscto) {
        this.fACCVTporcdscto = fACCVTporcdscto;
    }

    public BigDecimal getFACCVTsubtotamov() {
        return fACCVTsubtotamov;
    }

    public void setFACCVTsubtotamov(BigDecimal fACCVTsubtotamov) {
        this.fACCVTsubtotamov = fACCVTsubtotamov;
    }

    public BigDecimal getFACCVTdesctomov() {
        return fACCVTdesctomov;
    }

    public void setFACCVTdesctomov(BigDecimal fACCVTdesctomov) {
        this.fACCVTdesctomov = fACCVTdesctomov;
    }

    public BigDecimal getFACCVTtotimpumov() {
        return fACCVTtotimpumov;
    }

    public void setFACCVTtotimpumov(BigDecimal fACCVTtotimpumov) {
        this.fACCVTtotimpumov = fACCVTtotimpumov;
    }

    public BigDecimal getFACCVTnetomov() {
        return fACCVTnetomov;
    }

    public void setFACCVTnetomov(BigDecimal fACCVTnetomov) {
        this.fACCVTnetomov = fACCVTnetomov;
    }

    public BigDecimal getFACCVTtranspmov() {
        return fACCVTtranspmov;
    }

    public void setFACCVTtranspmov(BigDecimal fACCVTtranspmov) {
        this.fACCVTtranspmov = fACCVTtranspmov;
    }

    public BigDecimal getFACCVTrecargomov() {
        return fACCVTrecargomov;
    }

    public void setFACCVTrecargomov(BigDecimal fACCVTrecargomov) {
        this.fACCVTrecargomov = fACCVTrecargomov;
    }

    public String getFACCVTestado() {
        return fACCVTestado;
    }

    public void setFACCVTestado(String fACCVTestado) {
        this.fACCVTestado = fACCVTestado;
    }

    public String getFACCVTestuso() {
        return fACCVTestuso;
    }

    public void setFACCVTestuso(String fACCVTestuso) {
        this.fACCVTestuso = fACCVTestuso;
    }

    public String getFACCVTestplandes() {
        return fACCVTestplandes;
    }

    public void setFACCVTestplandes(String fACCVTestplandes) {
        this.fACCVTestplandes = fACCVTestplandes;
    }

    public String getFACCVTestplanfdo() {
        return fACCVTestplanfdo;
    }

    public void setFACCVTestplanfdo(String fACCVTestplanfdo) {
        this.fACCVTestplanfdo = fACCVTestplanfdo;
    }

    public String getFACCVTestdespach() {
        return fACCVTestdespach;
    }

    public void setFACCVTestdespach(String fACCVTestdespach) {
        this.fACCVTestdespach = fACCVTestdespach;
    }

    public BigDecimal getFACCVTtotalpeso() {
        return fACCVTtotalpeso;
    }

    public void setFACCVTtotalpeso(BigDecimal fACCVTtotalpeso) {
        this.fACCVTtotalpeso = fACCVTtotalpeso;
    }

    public BigDecimal getFACCVTtotalvolum() {
        return fACCVTtotalvolum;
    }

    public void setFACCVTtotalvolum(BigDecimal fACCVTtotalvolum) {
        this.fACCVTtotalvolum = fACCVTtotalvolum;
    }

    public String getFACCVTusuaingreg() {
        return fACCVTusuaingreg;
    }

    public void setFACCVTusuaingreg(String fACCVTusuaingreg) {
        this.fACCVTusuaingreg = fACCVTusuaingreg;
    }

    public String getFACCVTusuamodreg() {
        return fACCVTusuamodreg;
    }

    public void setFACCVTusuamodreg(String fACCVTusuamodreg) {
        this.fACCVTusuamodreg = fACCVTusuamodreg;
    }

    public Date getFACCVTfechingreg() {
        return fACCVTfechingreg;
    }

    public void setFACCVTfechingreg(Date fACCVTfechingreg) {
        this.fACCVTfechingreg = fACCVTfechingreg;
    }

    public Date getFACCVTfechmodreg() {
        return fACCVTfechmodreg;
    }

    public void setFACCVTfechmodreg(Date fACCVTfechmodreg) {
        this.fACCVTfechmodreg = fACCVTfechmodreg;
    }

    public String getFACCVTestadocont() {
        return fACCVTestadocont;
    }

    public void setFACCVTestadocont(String fACCVTestadocont) {
        this.fACCVTestadocont = fACCVTestadocont;
    }

    public String getFACCVTtranscontab() {
        return fACCVTtranscontab;
    }

    public void setFACCVTtranscontab(String fACCVTtranscontab) {
        this.fACCVTtranscontab = fACCVTtranscontab;
    }

    public String getFACCVTcodtipped() {
        return fACCVTcodtipped;
    }

    public void setFACCVTcodtipped(String fACCVTcodtipped) {
        this.fACCVTcodtipped = fACCVTcodtipped;
    }

    public GenpTalonarios getGENTALsecuencia() {
        return gENTALsecuencia;
    }

    public void setGENTALsecuencia(GenpTalonarios gENTALsecuencia) {
        this.gENTALsecuencia = gENTALsecuencia;
    }

    public BigDecimal getFACCVTTotunidpedi() {
        return fACCVTTotunidpedi;
    }

    public void setFACCVTTotunidpedi(BigDecimal fACCVTTotunidpedi) {
        this.fACCVTTotunidpedi = fACCVTTotunidpedi;
    }

    public String getFACDVTlistaprecio() {
        return fACDVTlistaprecio;
    }

    public void setFACDVTlistaprecio(String fACDVTlistaprecio) {
        this.fACDVTlistaprecio = fACDVTlistaprecio;
    }

    public int getFACCVTidsecuencia() {
        return fACCVTidsecuencia;
    }

    public void setFACCVTidsecuencia(int fACCVTidsecuencia) {
        this.fACCVTidsecuencia = fACCVTidsecuencia;
    }

    public String getFACCVTautorizacion() {
        return fACCVTautorizacion;
    }

    public void setFACCVTautorizacion(String fACCVTautorizacion) {
        this.fACCVTautorizacion = fACCVTautorizacion;
    }

    public Date getFACCVTfechaautoriza() {
        return fACCVTfechaautoriza;
    }

    public void setFACCVTfechaautoriza(Date fACCVTfechaautoriza) {
        this.fACCVTfechaautoriza = fACCVTfechaautoriza;
    }

    public String getFACCVTclaveacceso() {
        return fACCVTclaveacceso;
    }

    public void setFACCVTclaveacceso(String fACCVTclaveacceso) {
        this.fACCVTclaveacceso = fACCVTclaveacceso;
    }

    public String getFACGUIautorizacion() {
        return fACGUIautorizacion;
    }

    public void setFACGUIautorizacion(String fACGUIautorizacion) {
        this.fACGUIautorizacion = fACGUIautorizacion;
    }

    public Date getFACGUIfechaautoriza() {
        return fACGUIfechaautoriza;
    }

    public void setFACGUIfechaautoriza(Date fACGUIfechaautoriza) {
        this.fACGUIfechaautoriza = fACGUIfechaautoriza;
    }

    public String getFACGUIclaveacceso() {
        return fACGUIclaveacceso;
    }

    public void setFACGUIclaveacceso(String fACGUIclaveacceso) {
        this.fACGUIclaveacceso = fACGUIclaveacceso;
    }

    public String getGENTDOtipocomproban() {
        return gENTDOtipocomproban;
    }

    public void setGENTDOtipocomproban(String gENTDOtipocomproban) {
        this.gENTDOtipocomproban = gENTDOtipocomproban;
    }

    public String getFACCVTcodimpuesto() {
        return fACCVTcodimpuesto;
    }

    public void setFACCVTcodimpuesto(String fACCVTcodimpuesto) {
        this.fACCVTcodimpuesto = fACCVTcodimpuesto;
    }

    public BigDecimal getFACCVTimpueiva() {
        return fACCVTimpueiva;
    }

    public void setFACCVTimpueiva(BigDecimal fACCVTimpueiva) {
        this.fACCVTimpueiva = fACCVTimpueiva;
    }

    public BigDecimal getFACCVTimpueice() {
        return fACCVTimpueice;
    }

    public void setFACCVTimpueice(BigDecimal fACCVTimpueice) {
        this.fACCVTimpueice = fACCVTimpueice;
    }

    public BigDecimal getFACCVTimpueser() {
        return fACCVTimpueser;
    }

    public void setFACCVTimpueser(BigDecimal fACCVTimpueser) {
        this.fACCVTimpueser = fACCVTimpueser;
    }

    public List<FactDetventa> getFactDetventaList() {
        return factDetventaList;
    }

    public void setFactDetventaList(List<FactDetventa> factDetventaList) {
        this.factDetventaList = factDetventaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (factCabventaPK != null ? factCabventaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FactCabventa)) {
            return false;
        }
        FactCabventa other = (FactCabventa) object;
        if ((this.factCabventaPK == null && other.factCabventaPK != null) || (this.factCabventaPK != null && !this.factCabventaPK.equals(other.factCabventaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.FactCabventa[ factCabventaPK=" + factCabventaPK + " ]";
    }

    public String getNumDocumento() {
        if (gENTALsecuencia != null) {
            return gENTALsecuencia.getGENTALcodestable() + "-" + gENTALsecuencia.getGENTALcodptoemis() + "-" + Utilidades.completarCadenaConCeros(fACCVTnumdocumen + "", 9);
        }
        return null;
    }
}
