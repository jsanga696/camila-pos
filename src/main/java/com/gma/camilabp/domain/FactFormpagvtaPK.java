/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb6
 */
@Embeddable
public class FactFormpagvtaPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENCIA_codigo")
    private String gENCIAcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENOFI_codigo")
    private String gENOFIcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENCAJ_codigo")
    private String gENCAJcodigo;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "FACVTC_secuenvta")
    private BigDecimal fACVTCsecuenvta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FACSCA_fecapertur")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fACSCAfecapertur;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FACFPV_secuencia")
    private BigDecimal fACFPVsecuencia;

    public FactFormpagvtaPK() {
    }

    public FactFormpagvtaPK(String gENCIAcodigo, String gENOFIcodigo, String gENCAJcodigo, BigDecimal fACVTCsecuenvta, Date fACSCAfecapertur, BigDecimal fACFPVsecuencia) {
        this.gENCIAcodigo = gENCIAcodigo;
        this.gENOFIcodigo = gENOFIcodigo;
        this.gENCAJcodigo = gENCAJcodigo;
        this.fACVTCsecuenvta = fACVTCsecuenvta;
        this.fACSCAfecapertur = fACSCAfecapertur;
        this.fACFPVsecuencia = fACFPVsecuencia;
    }

    public String getGENCIAcodigo() {
        return gENCIAcodigo;
    }

    public void setGENCIAcodigo(String gENCIAcodigo) {
        this.gENCIAcodigo = gENCIAcodigo;
    }

    public String getGENOFIcodigo() {
        return gENOFIcodigo;
    }

    public void setGENOFIcodigo(String gENOFIcodigo) {
        this.gENOFIcodigo = gENOFIcodigo;
    }

    public String getGENCAJcodigo() {
        return gENCAJcodigo;
    }

    public void setGENCAJcodigo(String gENCAJcodigo) {
        this.gENCAJcodigo = gENCAJcodigo;
    }

    public BigDecimal getFACVTCsecuenvta() {
        return fACVTCsecuenvta;
    }

    public void setFACVTCsecuenvta(BigDecimal fACVTCsecuenvta) {
        this.fACVTCsecuenvta = fACVTCsecuenvta;
    }

    public Date getFACSCAfecapertur() {
        return fACSCAfecapertur;
    }

    public void setFACSCAfecapertur(Date fACSCAfecapertur) {
        this.fACSCAfecapertur = fACSCAfecapertur;
    }

    public BigDecimal getFACFPVsecuencia() {
        return fACFPVsecuencia;
    }

    public void setFACFPVsecuencia(BigDecimal fACFPVsecuencia) {
        this.fACFPVsecuencia = fACFPVsecuencia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gENCIAcodigo != null ? gENCIAcodigo.hashCode() : 0);
        hash += (gENOFIcodigo != null ? gENOFIcodigo.hashCode() : 0);
        hash += (gENCAJcodigo != null ? gENCAJcodigo.hashCode() : 0);
        hash += (fACVTCsecuenvta != null ? fACVTCsecuenvta.hashCode() : 0);
        hash += (fACSCAfecapertur != null ? fACSCAfecapertur.hashCode() : 0);
        hash += (fACFPVsecuencia != null ? fACFPVsecuencia.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FactFormpagvtaPK)) {
            return false;
        }
        FactFormpagvtaPK other = (FactFormpagvtaPK) object;
        if ((this.gENCIAcodigo == null && other.gENCIAcodigo != null) || (this.gENCIAcodigo != null && !this.gENCIAcodigo.equals(other.gENCIAcodigo))) {
            return false;
        }
        if ((this.gENOFIcodigo == null && other.gENOFIcodigo != null) || (this.gENOFIcodigo != null && !this.gENOFIcodigo.equals(other.gENOFIcodigo))) {
            return false;
        }
        if ((this.gENCAJcodigo == null && other.gENCAJcodigo != null) || (this.gENCAJcodigo != null && !this.gENCAJcodigo.equals(other.gENCAJcodigo))) {
            return false;
        }
        if ((this.fACVTCsecuenvta == null && other.fACVTCsecuenvta != null) || (this.fACVTCsecuenvta != null && !this.fACVTCsecuenvta.equals(other.fACVTCsecuenvta))) {
            return false;
        }
        if ((this.fACSCAfecapertur == null && other.fACSCAfecapertur != null) || (this.fACSCAfecapertur != null && !this.fACSCAfecapertur.equals(other.fACSCAfecapertur))) {
            return false;
        }
        if ((this.fACFPVsecuencia == null && other.fACFPVsecuencia != null) || (this.fACFPVsecuencia != null && !this.fACFPVsecuencia.equals(other.fACFPVsecuencia))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.FactFormpagvtaPK[ gENCIAcodigo=" + gENCIAcodigo + ", gENOFIcodigo=" + gENOFIcodigo + ", gENCAJcodigo=" + gENCAJcodigo + ", fACVTCsecuenvta=" + fACVTCsecuenvta + ", fACSCAfecapertur=" + fACSCAfecapertur + ", fACFPVsecuencia=" + fACFPVsecuencia + " ]";
    }
    
}
