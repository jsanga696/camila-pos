/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb5
 */
@Entity
@Table(name = "COMT_DETCOMIMPT")
@NamedQueries({
    @NamedQuery(name = "ComtDetcomimpt.findAll", query = "SELECT c FROM ComtDetcomimpt c")})
public class ComtDetcomimpt implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ComtDetcomimptPK comtDetcomimptPK;
    @Size(max = 9)
    @Column(name = "COMCCM_numdocumen")
    private String cOMCCMnumdocumen;
    @Size(max = 3)
    @Column(name = "GENTDO_codigo")
    private String gENTDOcodigo;
    @Size(max = 15)
    @Column(name = "INVITM_codigo")
    private String iNVITMcodigo;
    @Size(max = 5)
    @Column(name = "GENIMP_codigo")
    private String gENIMPcodigo;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "COMCMI_ordenaplica")
    private BigDecimal cOMCMIordenaplica;
    @Column(name = "COMCMI_porcentaje")
    private BigDecimal cOMCMIporcentaje;
    @Column(name = "COMCMI_basecalmov")
    private BigDecimal cOMCMIbasecalmov;
    @Column(name = "COMCMI_valormov")
    private BigDecimal cOMCMIvalormov;

    public ComtDetcomimpt() {
    }

    public ComtDetcomimpt(ComtDetcomimptPK comtDetcomimptPK) {
        this.comtDetcomimptPK = comtDetcomimptPK;
    }

    public ComtDetcomimpt(int cOMCCMnumsecucial, int cOMDCMnumlinea, int cOMDCMlineareg) {
        this.comtDetcomimptPK = new ComtDetcomimptPK(cOMCCMnumsecucial, cOMDCMnumlinea, cOMDCMlineareg);
    }

    public ComtDetcomimptPK getComtDetcomimptPK() {
        return comtDetcomimptPK;
    }

    public void setComtDetcomimptPK(ComtDetcomimptPK comtDetcomimptPK) {
        this.comtDetcomimptPK = comtDetcomimptPK;
    }

    public String getCOMCCMnumdocumen() {
        return cOMCCMnumdocumen;
    }

    public void setCOMCCMnumdocumen(String cOMCCMnumdocumen) {
        this.cOMCCMnumdocumen = cOMCCMnumdocumen;
    }

    public String getGENTDOcodigo() {
        return gENTDOcodigo;
    }

    public void setGENTDOcodigo(String gENTDOcodigo) {
        this.gENTDOcodigo = gENTDOcodigo;
    }

    public String getINVITMcodigo() {
        return iNVITMcodigo;
    }

    public void setINVITMcodigo(String iNVITMcodigo) {
        this.iNVITMcodigo = iNVITMcodigo;
    }

    public String getGENIMPcodigo() {
        return gENIMPcodigo;
    }

    public void setGENIMPcodigo(String gENIMPcodigo) {
        this.gENIMPcodigo = gENIMPcodigo;
    }

    public BigDecimal getCOMCMIordenaplica() {
        return cOMCMIordenaplica;
    }

    public void setCOMCMIordenaplica(BigDecimal cOMCMIordenaplica) {
        this.cOMCMIordenaplica = cOMCMIordenaplica;
    }

    public BigDecimal getCOMCMIporcentaje() {
        return cOMCMIporcentaje;
    }

    public void setCOMCMIporcentaje(BigDecimal cOMCMIporcentaje) {
        this.cOMCMIporcentaje = cOMCMIporcentaje;
    }

    public BigDecimal getCOMCMIbasecalmov() {
        return cOMCMIbasecalmov;
    }

    public void setCOMCMIbasecalmov(BigDecimal cOMCMIbasecalmov) {
        this.cOMCMIbasecalmov = cOMCMIbasecalmov;
    }

    public BigDecimal getCOMCMIvalormov() {
        return cOMCMIvalormov;
    }

    public void setCOMCMIvalormov(BigDecimal cOMCMIvalormov) {
        this.cOMCMIvalormov = cOMCMIvalormov;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (comtDetcomimptPK != null ? comtDetcomimptPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ComtDetcomimpt)) {
            return false;
        }
        ComtDetcomimpt other = (ComtDetcomimpt) object;
        if ((this.comtDetcomimptPK == null && other.comtDetcomimptPK != null) || (this.comtDetcomimptPK != null && !this.comtDetcomimptPK.equals(other.comtDetcomimptPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.ComtDetcomimpt[ comtDetcomimptPK=" + comtDetcomimptPK + " ]";
    }
    
}
