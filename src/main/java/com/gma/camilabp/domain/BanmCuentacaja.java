/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Acer
 */
@Entity
@Table(name = "BANM_CUENTACAJA")
@NamedQueries({
    @NamedQuery(name = "BanmCuentacaja.findAll", query = "SELECT b FROM BanmCuentacaja b")})
public class BanmCuentacaja implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected BanmCuentacajaPK banmCuentacajaPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENOFI_codigo")
    private String gENOFIcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "BANCTA_nombre")
    private String bANCTAnombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "BANCTA_moneda")
    private String bANCTAmoneda;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BANCTA_fechaapertura")
    @Temporal(TemporalType.TIMESTAMP)
    private Date bANCTAfechaapertura;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BANCTA_fechaultmov")
    @Temporal(TemporalType.TIMESTAMP)
    private Date bANCTAfechaultmov;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "BANCTA_descripcion")
    private String bANCTAdescripcion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "BANCTA_nombretitular")
    private String bANCTAnombretitular;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "BANCTA_nombrefuncionario")
    private String bANCTAnombrefuncionario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BANCTA_tipo")
    private Character bANCTAtipo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "BANCTA_referenciacontable")
    private String bANCTAreferenciacontable;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "BANCTA_saldoinicialbanco")
    private BigDecimal bANCTAsaldoinicialbanco;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BANCTA_saldoactualcompania")
    private BigDecimal bANCTAsaldoactualcompania;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "BANCTA_formatocheque")
    private String bANCTAformatocheque;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BANCTA_flagcomprobante")
    private Character bANCTAflagcomprobante;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BANCTA_flagmanejasaldo")
    private Character bANCTAflagmanejasaldo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BANCTA_flagcobranza")
    private Character bANCTAflagcobranza;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BANCTA_montorestituir")
    private BigDecimal bANCTAmontorestituir;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "BANCTA_usuario")
    private String bANCTAusuario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BANCTA_estado")
    private Character bANCTAestado;
    @Column(name = "BANCTA_fechacierre")
    @Temporal(TemporalType.TIMESTAMP)
    private Date bANCTAfechacierre;
    @Column(name = "BANCTA_saldo31122011")
    private BigDecimal bANCTAsaldo31122011;
    @Column(name = "BANCTA_saldoEstCta31122011")
    private BigDecimal bANCTAsaldoEstCta31122011;
    @Column(name = "BANCTA_saldoNoConcialiados")
    private BigDecimal bANCTAsaldoNoConcialiados;

    public BanmCuentacaja() {
        this.banmCuentacajaPK = new BanmCuentacajaPK();
    }

    public BanmCuentacaja(BanmCuentacajaPK banmCuentacajaPK) {
        this.banmCuentacajaPK = banmCuentacajaPK;
    }

    public BanmCuentacaja(BanmCuentacajaPK banmCuentacajaPK, String gENOFIcodigo, String bANCTAnombre, String bANCTAmoneda, Date bANCTAfechaapertura, Date bANCTAfechaultmov, String bANCTAdescripcion, String bANCTAnombretitular, String bANCTAnombrefuncionario, Character bANCTAtipo, String bANCTAreferenciacontable, BigDecimal bANCTAsaldoinicialbanco, BigDecimal bANCTAsaldoactualcompania, String bANCTAformatocheque, Character bANCTAflagcomprobante, Character bANCTAflagmanejasaldo, Character bANCTAflagcobranza, BigDecimal bANCTAmontorestituir, String bANCTAusuario, Character bANCTAestado) {
        this.banmCuentacajaPK = banmCuentacajaPK;
        this.gENOFIcodigo = gENOFIcodigo;
        this.bANCTAnombre = bANCTAnombre;
        this.bANCTAmoneda = bANCTAmoneda;
        this.bANCTAfechaapertura = bANCTAfechaapertura;
        this.bANCTAfechaultmov = bANCTAfechaultmov;
        this.bANCTAdescripcion = bANCTAdescripcion;
        this.bANCTAnombretitular = bANCTAnombretitular;
        this.bANCTAnombrefuncionario = bANCTAnombrefuncionario;
        this.bANCTAtipo = bANCTAtipo;
        this.bANCTAreferenciacontable = bANCTAreferenciacontable;
        this.bANCTAsaldoinicialbanco = bANCTAsaldoinicialbanco;
        this.bANCTAsaldoactualcompania = bANCTAsaldoactualcompania;
        this.bANCTAformatocheque = bANCTAformatocheque;
        this.bANCTAflagcomprobante = bANCTAflagcomprobante;
        this.bANCTAflagmanejasaldo = bANCTAflagmanejasaldo;
        this.bANCTAflagcobranza = bANCTAflagcobranza;
        this.bANCTAmontorestituir = bANCTAmontorestituir;
        this.bANCTAusuario = bANCTAusuario;
        this.bANCTAestado = bANCTAestado;
    }

    public BanmCuentacaja(String gENCIAcodigo, String bANENTtipo, String bANENTcodigo, String bANCTAcodigo) {
        this.banmCuentacajaPK = new BanmCuentacajaPK(gENCIAcodigo, bANENTtipo, bANENTcodigo, bANCTAcodigo);
    }

    public BanmCuentacajaPK getBanmCuentacajaPK() {
        return banmCuentacajaPK;
    }

    public void setBanmCuentacajaPK(BanmCuentacajaPK banmCuentacajaPK) {
        this.banmCuentacajaPK = banmCuentacajaPK;
    }

    public String getGENOFIcodigo() {
        return gENOFIcodigo;
    }

    public void setGENOFIcodigo(String gENOFIcodigo) {
        this.gENOFIcodigo = gENOFIcodigo;
    }

    public String getBANCTAnombre() {
        return bANCTAnombre;
    }

    public void setBANCTAnombre(String bANCTAnombre) {
        this.bANCTAnombre = bANCTAnombre;
    }

    public String getBANCTAmoneda() {
        return bANCTAmoneda;
    }

    public void setBANCTAmoneda(String bANCTAmoneda) {
        this.bANCTAmoneda = bANCTAmoneda;
    }

    public Date getBANCTAfechaapertura() {
        return bANCTAfechaapertura;
    }

    public void setBANCTAfechaapertura(Date bANCTAfechaapertura) {
        this.bANCTAfechaapertura = bANCTAfechaapertura;
    }

    public Date getBANCTAfechaultmov() {
        return bANCTAfechaultmov;
    }

    public void setBANCTAfechaultmov(Date bANCTAfechaultmov) {
        this.bANCTAfechaultmov = bANCTAfechaultmov;
    }

    public String getBANCTAdescripcion() {
        return bANCTAdescripcion;
    }

    public void setBANCTAdescripcion(String bANCTAdescripcion) {
        this.bANCTAdescripcion = bANCTAdescripcion;
    }

    public String getBANCTAnombretitular() {
        return bANCTAnombretitular;
    }

    public void setBANCTAnombretitular(String bANCTAnombretitular) {
        this.bANCTAnombretitular = bANCTAnombretitular;
    }

    public String getBANCTAnombrefuncionario() {
        return bANCTAnombrefuncionario;
    }

    public void setBANCTAnombrefuncionario(String bANCTAnombrefuncionario) {
        this.bANCTAnombrefuncionario = bANCTAnombrefuncionario;
    }

    public Character getBANCTAtipo() {
        return bANCTAtipo;
    }

    public void setBANCTAtipo(Character bANCTAtipo) {
        this.bANCTAtipo = bANCTAtipo;
    }

    public String getBANCTAreferenciacontable() {
        return bANCTAreferenciacontable;
    }

    public void setBANCTAreferenciacontable(String bANCTAreferenciacontable) {
        this.bANCTAreferenciacontable = bANCTAreferenciacontable;
    }

    public BigDecimal getBANCTAsaldoinicialbanco() {
        return bANCTAsaldoinicialbanco;
    }

    public void setBANCTAsaldoinicialbanco(BigDecimal bANCTAsaldoinicialbanco) {
        this.bANCTAsaldoinicialbanco = bANCTAsaldoinicialbanco;
    }

    public BigDecimal getBANCTAsaldoactualcompania() {
        return bANCTAsaldoactualcompania;
    }

    public void setBANCTAsaldoactualcompania(BigDecimal bANCTAsaldoactualcompania) {
        this.bANCTAsaldoactualcompania = bANCTAsaldoactualcompania;
    }

    public String getBANCTAformatocheque() {
        return bANCTAformatocheque;
    }

    public void setBANCTAformatocheque(String bANCTAformatocheque) {
        this.bANCTAformatocheque = bANCTAformatocheque;
    }

    public Character getBANCTAflagcomprobante() {
        return bANCTAflagcomprobante;
    }

    public void setBANCTAflagcomprobante(Character bANCTAflagcomprobante) {
        this.bANCTAflagcomprobante = bANCTAflagcomprobante;
    }

    public Character getBANCTAflagmanejasaldo() {
        return bANCTAflagmanejasaldo;
    }

    public void setBANCTAflagmanejasaldo(Character bANCTAflagmanejasaldo) {
        this.bANCTAflagmanejasaldo = bANCTAflagmanejasaldo;
    }

    public Character getBANCTAflagcobranza() {
        return bANCTAflagcobranza;
    }

    public void setBANCTAflagcobranza(Character bANCTAflagcobranza) {
        this.bANCTAflagcobranza = bANCTAflagcobranza;
    }

    public BigDecimal getBANCTAmontorestituir() {
        return bANCTAmontorestituir;
    }

    public void setBANCTAmontorestituir(BigDecimal bANCTAmontorestituir) {
        this.bANCTAmontorestituir = bANCTAmontorestituir;
    }

    public String getBANCTAusuario() {
        return bANCTAusuario;
    }

    public void setBANCTAusuario(String bANCTAusuario) {
        this.bANCTAusuario = bANCTAusuario;
    }

    public Character getBANCTAestado() {
        return bANCTAestado;
    }

    public void setBANCTAestado(Character bANCTAestado) {
        this.bANCTAestado = bANCTAestado;
    }

    public Date getBANCTAfechacierre() {
        return bANCTAfechacierre;
    }

    public void setBANCTAfechacierre(Date bANCTAfechacierre) {
        this.bANCTAfechacierre = bANCTAfechacierre;
    }

    public BigDecimal getBANCTAsaldo31122011() {
        return bANCTAsaldo31122011;
    }

    public void setBANCTAsaldo31122011(BigDecimal bANCTAsaldo31122011) {
        this.bANCTAsaldo31122011 = bANCTAsaldo31122011;
    }

    public BigDecimal getBANCTAsaldoEstCta31122011() {
        return bANCTAsaldoEstCta31122011;
    }

    public void setBANCTAsaldoEstCta31122011(BigDecimal bANCTAsaldoEstCta31122011) {
        this.bANCTAsaldoEstCta31122011 = bANCTAsaldoEstCta31122011;
    }

    public BigDecimal getBANCTAsaldoNoConcialiados() {
        return bANCTAsaldoNoConcialiados;
    }

    public void setBANCTAsaldoNoConcialiados(BigDecimal bANCTAsaldoNoConcialiados) {
        this.bANCTAsaldoNoConcialiados = bANCTAsaldoNoConcialiados;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (banmCuentacajaPK != null ? banmCuentacajaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BanmCuentacaja)) {
            return false;
        }
        BanmCuentacaja other = (BanmCuentacaja) object;
        if ((this.banmCuentacajaPK == null && other.banmCuentacajaPK != null) || (this.banmCuentacajaPK != null && !this.banmCuentacajaPK.equals(other.banmCuentacajaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.BanmCuentacaja[ banmCuentacajaPK=" + banmCuentacajaPK + " ]";
    }
    
}
