/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import com.gma.camilabp.model.consultas.TempCbpPcUsuarioOpcion;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Size;
//import org.hibernate.annotations.Formula;

/**
 *
 * @author userdb6
 */
@Entity
@Table(name = "CBPPOS_FACTTEMPORAL")
@NamedQueries({
    @NamedQuery(name = "CbpposFacttemporal.findAll", query = "SELECT c FROM CbpposFacttemporal c")})
public class CbpposFacttemporal implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cbpfact_codigo")
    private Long cbpfactCodigo;
    
    @Column(name = "id_secuencia")
    private Integer idSecuencia;
    @Size(max = 4)
    @Column(name = "gencia_codigo")
    private String genciaCodigo;
    @Size(max = 4)
    @Column(name = "genofi_codigo")
    private String genofiCodigo;
    @Column(name = "faccpd_fechaemisi")
    @Temporal(TemporalType.TIMESTAMP)
    private Date faccpdFechaemisi;
    @Size(max = 1)
    @Column(name = "facvdr_tiporeg")
    private String facvdrTiporeg;
    @Size(max = 6)
    @Column(name = "cxccli_codigo")
    private String cxccliCodigo;
    @Size(max = 6)
    @Column(name = "facvdr_codigo")
    private String facvdrCodigo;
    @Size(max = 10)
    @Column(name = "cxcrut_codigo")
    private String cxcrutCodigo;
    @Size(max = 3)
    @Column(name = "faccpd_etipoped")
    private String faccpdEtipoped;
    @Size(max = 15)
    @Column(name = "invitm_codigo")
    private String invitmCodigo;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "facdpd_cantfuncional")
    private BigDecimal facdpdCantfuncional;
    @Column(name = "facdpd_precio")
    private BigDecimal facdpdPrecio;
    @Column(name = "facdpd_subtotal")
    private BigDecimal facdpdSubtotal;
    @Column(name = "facdpd_descuento")
    private BigDecimal facdpdDescuento;
    @Column(name = "facdpd_impuesto")
    private BigDecimal facdpdImpuesto;
    @Column(name = "facdpd_neto")
    private BigDecimal facdpdNeto;
    @Size(max = 15)
    @Column(name = "facdpd_listaprecio")
    private String facdpdListaprecio;
    @Size(max = 1)
    @Column(name = "impuesto")
    private String impuesto;
    @Column(name = "invitm_peso")
    private BigDecimal invitmPeso;
    @Column(name = "invitm_volumen")
    private BigDecimal invitmVolumen;
    @Column(name = "invitm_unidembala")
    private BigDecimal invitmUnidembala;
    @JoinColumn(name = "cbpfacc_codigo", referencedColumnName = "cbpfacc_codigo")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private CbpposCabfacttemporal cbpfaccCodigo;
    //@Formula(value = "(SELECT i.INVITM_nombre FROM INVM_ITEM i WHERE i.INVITM_codigo=invitm_codigo AND i.GENCIA_CODIGO=gencia_codigo )")
    @Transient
    private String nombreItem;

    public CbpposFacttemporal() {
    }
    
    public CbpposFacttemporal(TempCbpPcUsuarioOpcion r) {
        this.idSecuencia = r.getIdSecuencia();
        this.genciaCodigo= r.getGenciaCodigo();
        this.genofiCodigo= r.getGenofiCodigo();
        this.faccpdFechaemisi= r.getFaccpdFechaemisi();
        this.facvdrTiporeg = r.getFacvdrTiporeg();
        this.cxccliCodigo= r.getCxccliCodigo();
        this.facvdrCodigo= r.getFacvdrCodigo();
        this.cxcrutCodigo= r.getCxcrutCodigo();
        this.faccpdEtipoped= r.getFaccpdEtipoped();
        this.invitmCodigo= r.getInvitmCodigo();
        this.facdpdCantfuncional=r.getFacdpdCantfuncional();
        this.facdpdPrecio= r.getFacdpdPrecio();
        this.facdpdSubtotal=r.getFacdpdSubtotal();
        this.facdpdDescuento=r.getFacdpdDescuento();
        this.facdpdImpuesto=r.getFacdpdImpuesto();
        this.facdpdNeto = r.getFacdpdNeto();
        this.facdpdListaprecio=r.getFacdpdListaprecio();
        this.impuesto = r.getImpuesto();
        this.invitmPeso=r.getInvitmPeso();
        this.invitmVolumen=r.getInvitmVolumen();
        this.invitmUnidembala=r.getInvitmUnidembala();
    }

    public CbpposFacttemporal(Long cbpfactCodigo) {
        this.cbpfactCodigo = cbpfactCodigo;
    }

    public CbpposFacttemporal(Long cbpfactCodigo, Long segusuCodigo) {
        this.cbpfactCodigo = cbpfactCodigo;
    }

    public Long getCbpfactCodigo() {
        return cbpfactCodigo;
    }

    public void setCbpfactCodigo(Long cbpfactCodigo) {
        this.cbpfactCodigo = cbpfactCodigo;
    }

    public Integer getIdSecuencia() {
        return idSecuencia;
    }

    public void setIdSecuencia(Integer idSecuencia) {
        this.idSecuencia = idSecuencia;
    }

    public String getGenciaCodigo() {
        return genciaCodigo;
    }

    public void setGenciaCodigo(String genciaCodigo) {
        this.genciaCodigo = genciaCodigo;
    }

    public String getGenofiCodigo() {
        return genofiCodigo;
    }

    public void setGenofiCodigo(String genofiCodigo) {
        this.genofiCodigo = genofiCodigo;
    }

    public Date getFaccpdFechaemisi() {
        return faccpdFechaemisi;
    }

    public void setFaccpdFechaemisi(Date faccpdFechaemisi) {
        this.faccpdFechaemisi = faccpdFechaemisi;
    }

    public String getFacvdrTiporeg() {
        return facvdrTiporeg;
    }

    public void setFacvdrTiporeg(String facvdrTiporeg) {
        this.facvdrTiporeg = facvdrTiporeg;
    }

    public String getCxccliCodigo() {
        return cxccliCodigo;
    }

    public void setCxccliCodigo(String cxccliCodigo) {
        this.cxccliCodigo = cxccliCodigo;
    }

    public String getFacvdrCodigo() {
        return facvdrCodigo;
    }

    public void setFacvdrCodigo(String facvdrCodigo) {
        this.facvdrCodigo = facvdrCodigo;
    }

    public String getCxcrutCodigo() {
        return cxcrutCodigo;
    }

    public void setCxcrutCodigo(String cxcrutCodigo) {
        this.cxcrutCodigo = cxcrutCodigo;
    }

    public String getFaccpdEtipoped() {
        return faccpdEtipoped;
    }

    public void setFaccpdEtipoped(String faccpdEtipoped) {
        this.faccpdEtipoped = faccpdEtipoped;
    }

    public String getInvitmCodigo() {
        return invitmCodigo;
    }

    public void setInvitmCodigo(String invitmCodigo) {
        this.invitmCodigo = invitmCodigo;
    }

    public BigDecimal getFacdpdCantfuncional() {
        return facdpdCantfuncional;
    }

    public void setFacdpdCantfuncional(BigDecimal facdpdCantfuncional) {
        this.facdpdCantfuncional = facdpdCantfuncional;
    }

    public BigDecimal getFacdpdPrecio() {
        return facdpdPrecio;
    }

    public void setFacdpdPrecio(BigDecimal facdpdPrecio) {
        this.facdpdPrecio = facdpdPrecio;
    }

    public BigDecimal getFacdpdSubtotal() {
        return facdpdSubtotal;
    }

    public void setFacdpdSubtotal(BigDecimal facdpdSubtotal) {
        this.facdpdSubtotal = facdpdSubtotal;
    }

    public BigDecimal getFacdpdDescuento() {
        return facdpdDescuento;
    }

    public void setFacdpdDescuento(BigDecimal facdpdDescuento) {
        this.facdpdDescuento = facdpdDescuento;
    }

    public BigDecimal getFacdpdImpuesto() {
        return facdpdImpuesto;
    }

    public void setFacdpdImpuesto(BigDecimal facdpdImpuesto) {
        this.facdpdImpuesto = facdpdImpuesto;
    }

    public BigDecimal getFacdpdNeto() {
        return facdpdNeto;
    }

    public void setFacdpdNeto(BigDecimal facdpdNeto) {
        this.facdpdNeto = facdpdNeto;
    }

    public String getFacdpdListaprecio() {
        return facdpdListaprecio;
    }

    public void setFacdpdListaprecio(String facdpdListaprecio) {
        this.facdpdListaprecio = facdpdListaprecio;
    }

    public String getImpuesto() {
        return impuesto;
    }

    public void setImpuesto(String impuesto) {
        this.impuesto = impuesto;
    }

    public BigDecimal getInvitmPeso() {
        return invitmPeso;
    }

    public void setInvitmPeso(BigDecimal invitmPeso) {
        this.invitmPeso = invitmPeso;
    }

    public BigDecimal getInvitmVolumen() {
        return invitmVolumen;
    }

    public void setInvitmVolumen(BigDecimal invitmVolumen) {
        this.invitmVolumen = invitmVolumen;
    }

    public BigDecimal getInvitmUnidembala() {
        return invitmUnidembala;
    }

    public void setInvitmUnidembala(BigDecimal invitmUnidembala) {
        this.invitmUnidembala = invitmUnidembala;
    }

    public CbpposCabfacttemporal getCbpfaccCodigo() {
        return cbpfaccCodigo;
    }

    public void setCbpfaccCodigo(CbpposCabfacttemporal cbpfaccCodigo) {
        this.cbpfaccCodigo = cbpfaccCodigo;
    }

    public String getNombreItem() {
        return nombreItem;
    }

    public void setNombreItem(String nombreItem) {
        this.nombreItem = nombreItem;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cbpfactCodigo != null ? cbpfactCodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbpposFacttemporal)) {
            return false;
        }
        CbpposFacttemporal other = (CbpposFacttemporal) object;
        if ((this.cbpfactCodigo == null && other.cbpfactCodigo != null) || (this.cbpfactCodigo != null && !this.cbpfactCodigo.equals(other.cbpfactCodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CbpposFacttemporal[ cbpfactCodigo=" + cbpfactCodigo + " ]";
    }
    
}
