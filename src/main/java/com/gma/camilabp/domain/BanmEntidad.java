/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb6
 */
@Entity
@Table(name = "BANM_ENTIDAD")
@NamedQueries({
    @NamedQuery(name = "BanmEntidad.findAll", query = "SELECT b FROM BanmEntidad b")})
public class BanmEntidad implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected BanmEntidadPK banmEntidadPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "BANENT_nombre")
    private String banentNombre;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BANENT_LocalInternacional")
    private Character banentLocalInternacional;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BANENT_flagseccontrolcheq")
    private Character banentFlagseccontrolcheq;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BANENT_estado")
    private Character banentEstado;

    public BanmEntidad() {
    }

    public BanmEntidad(BanmEntidadPK banmEntidadPK) {
        this.banmEntidadPK = banmEntidadPK;
    }

    public BanmEntidad(BanmEntidadPK banmEntidadPK, String banentNombre, Character banentLocalInternacional, Character banentFlagseccontrolcheq, Character banentEstado) {
        this.banmEntidadPK = banmEntidadPK;
        this.banentNombre = banentNombre;
        this.banentLocalInternacional = banentLocalInternacional;
        this.banentFlagseccontrolcheq = banentFlagseccontrolcheq;
        this.banentEstado = banentEstado;
    }

    public BanmEntidad(String genciaCodigo, String banentTipo, String banentCodigo) {
        this.banmEntidadPK = new BanmEntidadPK(genciaCodigo, banentTipo, banentCodigo);
    }

    public BanmEntidadPK getBanmEntidadPK() {
        return banmEntidadPK;
    }

    public void setBanmEntidadPK(BanmEntidadPK banmEntidadPK) {
        this.banmEntidadPK = banmEntidadPK;
    }

    public String getBanentNombre() {
        return banentNombre;
    }

    public void setBanentNombre(String banentNombre) {
        this.banentNombre = banentNombre;
    }

    public Character getBanentLocalInternacional() {
        return banentLocalInternacional;
    }

    public void setBanentLocalInternacional(Character banentLocalInternacional) {
        this.banentLocalInternacional = banentLocalInternacional;
    }

    public Character getBanentFlagseccontrolcheq() {
        return banentFlagseccontrolcheq;
    }

    public void setBanentFlagseccontrolcheq(Character banentFlagseccontrolcheq) {
        this.banentFlagseccontrolcheq = banentFlagseccontrolcheq;
    }

    public Character getBanentEstado() {
        return banentEstado;
    }

    public void setBanentEstado(Character banentEstado) {
        this.banentEstado = banentEstado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (banmEntidadPK != null ? banmEntidadPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BanmEntidad)) {
            return false;
        }
        BanmEntidad other = (BanmEntidad) object;
        if ((this.banmEntidadPK == null && other.banmEntidadPK != null) || (this.banmEntidadPK != null && !this.banmEntidadPK.equals(other.banmEntidadPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.BanmEntidad[ banmEntidadPK=" + banmEntidadPK + " ]";
    }
    
}
