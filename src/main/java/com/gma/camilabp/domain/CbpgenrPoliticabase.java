/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

/**
 *
 * @author Acer
 */
@Entity
@Table(name = "CBPGENR_POLITICABASE")
@NamedQueries({
    @NamedQuery(name = "CbpgenrPoliticabase.findAll", query = "SELECT c FROM CbpgenrPoliticabase c")})
public class CbpgenrPoliticabase implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "genpol_codigo")
    private Long genpolCodigo;
    @JoinColumn(name = "gencia_codigo", referencedColumnName = "gencia_codigo")
    @ManyToOne(fetch = FetchType.LAZY)
    private CbpgenrCompania genciaCodigo;
    @JoinColumn(name = "genofi_codigo", referencedColumnName = "genofi_codigo")
    @ManyToOne(fetch = FetchType.LAZY)
    private CbpgenrOficina genofiCodigo;
    @Size(max = 50)
    @Column(name = "genpol_nombre")
    private String genpolNombre;
    @Column(name = "genpol_cantidad")
    private Long genpolCantidad;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "genpol_porcentaje")
    private BigDecimal genpolPorcentaje;
    @Column(name = "genpol_estado")
    private Boolean genpolEstado= Boolean.TRUE;
    @JoinColumn(name = "genpol_creusuario", referencedColumnName = "segusu_codigo")
    @ManyToOne(fetch = FetchType.LAZY)
    private CbpsegmUsuario genpolCreusuario;
    @Column(name = "genpol_crefecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date genpolCrefecha;
    @JoinColumn(name = "genpol_ediusuario", referencedColumnName = "segusu_codigo")
    @ManyToOne(fetch = FetchType.LAZY)
    private CbpsegmUsuario genpolEdiusuario;
    @Column(name = "genpol_edifecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date genpolEdifecha;
    @OneToMany(mappedBy = "genpolCodigo", fetch = FetchType.LAZY)
    private Collection<CbpgenrPoliticaitem> cbpgenrPoliticaitemCollection;
    @OneToMany(mappedBy = "genpolCodigo", fetch = FetchType.LAZY)
    private Collection<CbpgenrPoliticabodega> cbpgenrPoliticabodegaCollection;
    @Transient
    private BigDecimal valorFinal;

    public CbpgenrPoliticabase() {
    }

    public CbpgenrPoliticabase(Long genpolCodigo) {
        this.genpolCodigo = genpolCodigo;
    }

    public Long getGenpolCodigo() {
        return genpolCodigo;
    }

    public void setGenpolCodigo(Long genpolCodigo) {
        this.genpolCodigo = genpolCodigo;
    }

    public CbpgenrCompania getGenciaCodigo() {
        return genciaCodigo;
    }

    public void setGenciaCodigo(CbpgenrCompania genciaCodigo) {
        this.genciaCodigo = genciaCodigo;
    }

    public CbpgenrOficina getGenofiCodigo() {
        return genofiCodigo;
    }

    public void setGenofiCodigo(CbpgenrOficina genofiCodigo) {
        this.genofiCodigo = genofiCodigo;
    }    

    public String getGenpolNombre() {
        return genpolNombre;
    }

    public void setGenpolNombre(String genpolNombre) {
        this.genpolNombre = genpolNombre;
    }

    public Long getGenpolCantidad() {
        return genpolCantidad;
    }

    public void setGenpolCantidad(Long genpolCantidad) {
        this.genpolCantidad = genpolCantidad;
    }

    public BigDecimal getGenpolPorcentaje() {
        return genpolPorcentaje;
    }

    public void setGenpolPorcentaje(BigDecimal genpolPorcentaje) {
        this.genpolPorcentaje = genpolPorcentaje;
    }

    public Boolean getGenpolEstado() {
        return genpolEstado;
    }

    public void setGenpolEstado(Boolean genpolEstado) {
        this.genpolEstado = genpolEstado;
    }

    public CbpsegmUsuario getGenpolCreusuario() {
        return genpolCreusuario;
    }

    public void setGenpolCreusuario(CbpsegmUsuario genpolCreusuario) {
        this.genpolCreusuario = genpolCreusuario;
    }

    public Date getGenpolCrefecha() {
        return genpolCrefecha;
    }

    public void setGenpolCrefecha(Date genpolCrefecha) {
        this.genpolCrefecha = genpolCrefecha;
    }

    public CbpsegmUsuario getGenpolEdiusuario() {
        return genpolEdiusuario;
    }

    public void setGenpolEdiusuario(CbpsegmUsuario genpolEdiusuario) {
        this.genpolEdiusuario = genpolEdiusuario;
    }

    public Date getGenpolEdifecha() {
        return genpolEdifecha;
    }

    public void setGenpolEdifecha(Date genpolEdifecha) {
        this.genpolEdifecha = genpolEdifecha;
    }

    public Collection<CbpgenrPoliticaitem> getCbpgenrPoliticaitemCollection() {
        return cbpgenrPoliticaitemCollection;
    }

    public void setCbpgenrPoliticaitemCollection(Collection<CbpgenrPoliticaitem> cbpgenrPoliticaitemCollection) {
        this.cbpgenrPoliticaitemCollection = cbpgenrPoliticaitemCollection;
    }

    public Collection<CbpgenrPoliticabodega> getCbpgenrPoliticabodegaCollection() {
        return cbpgenrPoliticabodegaCollection;
    }

    public void setCbpgenrPoliticabodegaCollection(Collection<CbpgenrPoliticabodega> cbpgenrPoliticabodegaCollection) {
        this.cbpgenrPoliticabodegaCollection = cbpgenrPoliticabodegaCollection;
    }

    public BigDecimal getValorFinal() {
        return valorFinal;
    }

    public void setValorFinal(BigDecimal valorFinal) {
        this.valorFinal = valorFinal;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (genpolCodigo != null ? genpolCodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbpgenrPoliticabase)) {
            return false;
        }
        CbpgenrPoliticabase other = (CbpgenrPoliticabase) object;
        if ((this.genpolCodigo == null && other.genpolCodigo != null) || (this.genpolCodigo != null && !this.genpolCodigo.equals(other.genpolCodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CbpgenrPoliticabase[ genpolCodigo=" + genpolCodigo + " ]";
    }
    
}
