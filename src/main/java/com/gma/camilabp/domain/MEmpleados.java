/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb6
 */
@Entity
@Table(name = "M_EMPLEADOS")
@NamedQueries({
    @NamedQuery(name = "MEmpleados.findAll", query = "SELECT m FROM MEmpleados m")})
public class MEmpleados implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected MEmpleadosPK mEmpleadosPK;
    @Size(max = 10)
    @Column(name = "NOMEMP_cedula")
    private String nOMEMPcedula;
    @Size(max = 50)
    @Column(name = "NOMEMP_nombres")
    private String nOMEMPnombres;
    @Size(max = 50)
    @Column(name = "NOMEMP_apellidos")
    private String nOMEMPapellidos;
    @Size(max = 3)
    @Column(name = "NOMEMP_provincia")
    private String nOMEMPprovincia;
    @Size(max = 25)
    @Column(name = "NOMEMP_ciudad")
    private String nOMEMPciudad;
    @Size(max = 100)
    @Column(name = "NOMEMP_direccion")
    private String nOMEMPdireccion;
    @Size(max = 25)
    @Column(name = "NOMEMP_telefono")
    private String nOMEMPtelefono;
    @Size(max = 15)
    @Column(name = "NOMEMP_celular")
    private String nOMEMPcelular;
    @Size(max = 3)
    @Column(name = "NOMEMP_cargo")
    private String nOMEMPcargo;
    @Basic(optional = false)
    @Size(min = 1, max = 3)
    @Column(name = "BITDEP_codigo")
    private String bITDEPcodigo;
    @Column(name = "NOMEMP_fecnacimien")
    @Temporal(TemporalType.TIMESTAMP)
    private Date nOMEMPfecnacimien;
    @Size(max = 1)
    @Column(name = "NOMEMP_estadocivil")
    private String nOMEMPestadocivil;
    @Size(max = 1)
    @Column(name = "NOMEMP_sexo")
    private String nOMEMPsexo;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "NOMEMP_sueldo")
    private BigDecimal nOMEMPsueldo;
    @Size(max = 1)
    @Column(name = "NOMEMP_imprenta")
    private String nOMEMPimprenta;
    @Size(max = 1)
    @Column(name = "NOMEMP_submater")
    private String nOMEMPsubmater;
    @Column(name = "NOMEMP_fecsubmater")
    @Temporal(TemporalType.TIMESTAMP)
    private Date nOMEMPfecsubmater;
    @Column(name = "NOMEMP_fecsubmaterh")
    @Temporal(TemporalType.TIMESTAMP)
    private Date nOMEMPfecsubmaterh;
    @Size(max = 1)
    @Column(name = "NOMEMP_fondoreserva")
    private String nOMEMPfondoreserva;
    @Size(max = 1)
    @Column(name = "NOMEMP_pdectercer")
    private String nOMEMPpdectercer;
    @Size(max = 1)
    @Column(name = "NOMEMP_pdeccuarto")
    private String nOMEMPpdeccuarto;
    @Size(max = 3)
    @Column(name = "NOMEMP_frmpago")
    private String nOMEMPfrmpago;
    @Size(max = 1)
    @Column(name = "NOMEMP_tipocta")
    private String nOMEMPtipocta;
    @Size(max = 25)
    @Column(name = "NOMEMP_numcta")
    private String nOMEMPnumcta;
    @Size(max = 20)
    @Column(name = "NOMEMP_ctaanticipo")
    private String nOMEMPctaanticipo;
    @Size(max = 20)
    @Column(name = "NOMEMP_ctaprestamo")
    private String nOMEMPctaprestamo;
    @Column(name = "NOMEMP_fecingreso")
    @Temporal(TemporalType.TIMESTAMP)
    private Date nOMEMPfecingreso;
    @Column(name = "NOMEMP_fecmodifica")
    @Temporal(TemporalType.TIMESTAMP)
    private Date nOMEMPfecmodifica;
    @Size(max = 3)
    @Column(name = "NOMEMP_usuingreso")
    private String nOMEMPusuingreso;
    @Size(max = 3)
    @Column(name = "NOMEMP_usumodifica")
    private String nOMEMPusumodifica;
    @Size(max = 1)
    @Column(name = "NOMEMP_estado")
    private String nOMEMPestado;
    @Size(max = 500)
    @Column(name = "NOMEMP_foto")
    private String nOMEMPfoto;
    @Size(max = 3)
    @Column(name = "NOMEMP_grupsanguineo")
    private String nOMEMPgrupsanguineo;
    @Size(max = 1)
    @Column(name = "NOMEMP_contrato")
    private String nOMEMPcontrato;
    @Column(name = "NOMEMP_fecfincontrato")
    @Temporal(TemporalType.TIMESTAMP)
    private Date nOMEMPfecfincontrato;
    @Size(max = 50)
    @Column(name = "NOMEMP_email")
    private String nOMEMPemail;
    @Size(max = 1)
    @Column(name = "NOMEMP_sisIngNeto")
    private String nOMEMPsisIngNeto;
    @Size(max = 2)
    @Column(name = "NOMEMP_codbanco")
    private String nOMEMPcodbanco;
    @Size(max = 1)
    @Column(name = "NOMEMP_discapac")
    private String nOMEMPdiscapac;
    @Size(max = 6)
    @Column(name = "CXCCLI_codigo")
    private String cXCCLIcodigo;
    
    @Transient
    private String nombresCompletos;

    public MEmpleados() {
    }

    public String getNombresCompletos() {
        if(this.nOMEMPnombres == null || this.nOMEMPapellidos == null)
            return "";
        return this.nOMEMPnombres + " " + this.nOMEMPapellidos;
    }

    public void setNombresCompletos(String nombresCompletos) {
        this.nombresCompletos = nombresCompletos;
    }

    
    
    public MEmpleados(MEmpleadosPK mEmpleadosPK) {
        this.mEmpleadosPK = mEmpleadosPK;
    }

    public MEmpleados(MEmpleadosPK mEmpleadosPK, String bITDEPcodigo) {
        this.mEmpleadosPK = mEmpleadosPK;
        this.bITDEPcodigo = bITDEPcodigo;
    }

    public MEmpleados(String gENCIAcodigo, String gENOFIcodigo, int nOMEMPcodigo) {
        this.mEmpleadosPK = new MEmpleadosPK(gENCIAcodigo, gENOFIcodigo, nOMEMPcodigo);
    }

    public MEmpleadosPK getMEmpleadosPK() {
        return mEmpleadosPK;
    }

    public void setMEmpleadosPK(MEmpleadosPK mEmpleadosPK) {
        this.mEmpleadosPK = mEmpleadosPK;
    }

    public String getNOMEMPcedula() {
        return nOMEMPcedula;
    }

    public void setNOMEMPcedula(String nOMEMPcedula) {
        this.nOMEMPcedula = nOMEMPcedula;
    }

    public String getNOMEMPnombres() {
        return nOMEMPnombres;
    }

    public void setNOMEMPnombres(String nOMEMPnombres) {
        this.nOMEMPnombres = nOMEMPnombres;
    }

    public String getNOMEMPapellidos() {
        return nOMEMPapellidos;
    }

    public void setNOMEMPapellidos(String nOMEMPapellidos) {
        this.nOMEMPapellidos = nOMEMPapellidos;
    }

    public String getNOMEMPprovincia() {
        return nOMEMPprovincia;
    }

    public void setNOMEMPprovincia(String nOMEMPprovincia) {
        this.nOMEMPprovincia = nOMEMPprovincia;
    }

    public String getNOMEMPciudad() {
        return nOMEMPciudad;
    }

    public void setNOMEMPciudad(String nOMEMPciudad) {
        this.nOMEMPciudad = nOMEMPciudad;
    }

    public String getNOMEMPdireccion() {
        return nOMEMPdireccion;
    }

    public void setNOMEMPdireccion(String nOMEMPdireccion) {
        this.nOMEMPdireccion = nOMEMPdireccion;
    }

    public String getNOMEMPtelefono() {
        return nOMEMPtelefono;
    }

    public void setNOMEMPtelefono(String nOMEMPtelefono) {
        this.nOMEMPtelefono = nOMEMPtelefono;
    }

    public String getNOMEMPcelular() {
        return nOMEMPcelular;
    }

    public void setNOMEMPcelular(String nOMEMPcelular) {
        this.nOMEMPcelular = nOMEMPcelular;
    }

    public String getNOMEMPcargo() {
        return nOMEMPcargo;
    }

    public void setNOMEMPcargo(String nOMEMPcargo) {
        this.nOMEMPcargo = nOMEMPcargo;
    }

    public String getBITDEPcodigo() {
        return bITDEPcodigo;
    }

    public void setBITDEPcodigo(String bITDEPcodigo) {
        this.bITDEPcodigo = bITDEPcodigo;
    }

    public Date getNOMEMPfecnacimien() {
        return nOMEMPfecnacimien;
    }

    public void setNOMEMPfecnacimien(Date nOMEMPfecnacimien) {
        this.nOMEMPfecnacimien = nOMEMPfecnacimien;
    }

    public String getNOMEMPestadocivil() {
        return nOMEMPestadocivil;
    }

    public void setNOMEMPestadocivil(String nOMEMPestadocivil) {
        this.nOMEMPestadocivil = nOMEMPestadocivil;
    }

    public String getNOMEMPsexo() {
        return nOMEMPsexo;
    }

    public void setNOMEMPsexo(String nOMEMPsexo) {
        this.nOMEMPsexo = nOMEMPsexo;
    }

    public BigDecimal getNOMEMPsueldo() {
        return nOMEMPsueldo;
    }

    public void setNOMEMPsueldo(BigDecimal nOMEMPsueldo) {
        this.nOMEMPsueldo = nOMEMPsueldo;
    }

    public String getNOMEMPimprenta() {
        return nOMEMPimprenta;
    }

    public void setNOMEMPimprenta(String nOMEMPimprenta) {
        this.nOMEMPimprenta = nOMEMPimprenta;
    }

    public String getNOMEMPsubmater() {
        return nOMEMPsubmater;
    }

    public void setNOMEMPsubmater(String nOMEMPsubmater) {
        this.nOMEMPsubmater = nOMEMPsubmater;
    }

    public Date getNOMEMPfecsubmater() {
        return nOMEMPfecsubmater;
    }

    public void setNOMEMPfecsubmater(Date nOMEMPfecsubmater) {
        this.nOMEMPfecsubmater = nOMEMPfecsubmater;
    }

    public Date getNOMEMPfecsubmaterh() {
        return nOMEMPfecsubmaterh;
    }

    public void setNOMEMPfecsubmaterh(Date nOMEMPfecsubmaterh) {
        this.nOMEMPfecsubmaterh = nOMEMPfecsubmaterh;
    }

    public String getNOMEMPfondoreserva() {
        return nOMEMPfondoreserva;
    }

    public void setNOMEMPfondoreserva(String nOMEMPfondoreserva) {
        this.nOMEMPfondoreserva = nOMEMPfondoreserva;
    }

    public String getNOMEMPpdectercer() {
        return nOMEMPpdectercer;
    }

    public void setNOMEMPpdectercer(String nOMEMPpdectercer) {
        this.nOMEMPpdectercer = nOMEMPpdectercer;
    }

    public String getNOMEMPpdeccuarto() {
        return nOMEMPpdeccuarto;
    }

    public void setNOMEMPpdeccuarto(String nOMEMPpdeccuarto) {
        this.nOMEMPpdeccuarto = nOMEMPpdeccuarto;
    }

    public String getNOMEMPfrmpago() {
        return nOMEMPfrmpago;
    }

    public void setNOMEMPfrmpago(String nOMEMPfrmpago) {
        this.nOMEMPfrmpago = nOMEMPfrmpago;
    }

    public String getNOMEMPtipocta() {
        return nOMEMPtipocta;
    }

    public void setNOMEMPtipocta(String nOMEMPtipocta) {
        this.nOMEMPtipocta = nOMEMPtipocta;
    }

    public String getNOMEMPnumcta() {
        return nOMEMPnumcta;
    }

    public void setNOMEMPnumcta(String nOMEMPnumcta) {
        this.nOMEMPnumcta = nOMEMPnumcta;
    }

    public String getNOMEMPctaanticipo() {
        return nOMEMPctaanticipo;
    }

    public void setNOMEMPctaanticipo(String nOMEMPctaanticipo) {
        this.nOMEMPctaanticipo = nOMEMPctaanticipo;
    }

    public String getNOMEMPctaprestamo() {
        return nOMEMPctaprestamo;
    }

    public void setNOMEMPctaprestamo(String nOMEMPctaprestamo) {
        this.nOMEMPctaprestamo = nOMEMPctaprestamo;
    }

    public Date getNOMEMPfecingreso() {
        return nOMEMPfecingreso;
    }

    public void setNOMEMPfecingreso(Date nOMEMPfecingreso) {
        this.nOMEMPfecingreso = nOMEMPfecingreso;
    }

    public Date getNOMEMPfecmodifica() {
        return nOMEMPfecmodifica;
    }

    public void setNOMEMPfecmodifica(Date nOMEMPfecmodifica) {
        this.nOMEMPfecmodifica = nOMEMPfecmodifica;
    }

    public String getNOMEMPusuingreso() {
        return nOMEMPusuingreso;
    }

    public void setNOMEMPusuingreso(String nOMEMPusuingreso) {
        this.nOMEMPusuingreso = nOMEMPusuingreso;
    }

    public String getNOMEMPusumodifica() {
        return nOMEMPusumodifica;
    }

    public void setNOMEMPusumodifica(String nOMEMPusumodifica) {
        this.nOMEMPusumodifica = nOMEMPusumodifica;
    }

    public String getNOMEMPestado() {
        return nOMEMPestado;
    }

    public void setNOMEMPestado(String nOMEMPestado) {
        this.nOMEMPestado = nOMEMPestado;
    }

    public String getNOMEMPfoto() {
        return nOMEMPfoto;
    }

    public void setNOMEMPfoto(String nOMEMPfoto) {
        this.nOMEMPfoto = nOMEMPfoto;
    }

    public String getNOMEMPgrupsanguineo() {
        return nOMEMPgrupsanguineo;
    }

    public void setNOMEMPgrupsanguineo(String nOMEMPgrupsanguineo) {
        this.nOMEMPgrupsanguineo = nOMEMPgrupsanguineo;
    }

    public String getNOMEMPcontrato() {
        return nOMEMPcontrato;
    }

    public void setNOMEMPcontrato(String nOMEMPcontrato) {
        this.nOMEMPcontrato = nOMEMPcontrato;
    }

    public Date getNOMEMPfecfincontrato() {
        return nOMEMPfecfincontrato;
    }

    public void setNOMEMPfecfincontrato(Date nOMEMPfecfincontrato) {
        this.nOMEMPfecfincontrato = nOMEMPfecfincontrato;
    }

    public String getNOMEMPemail() {
        return nOMEMPemail;
    }

    public void setNOMEMPemail(String nOMEMPemail) {
        this.nOMEMPemail = nOMEMPemail;
    }

    public String getNOMEMPsisIngNeto() {
        return nOMEMPsisIngNeto;
    }

    public void setNOMEMPsisIngNeto(String nOMEMPsisIngNeto) {
        this.nOMEMPsisIngNeto = nOMEMPsisIngNeto;
    }

    public String getNOMEMPcodbanco() {
        return nOMEMPcodbanco;
    }

    public void setNOMEMPcodbanco(String nOMEMPcodbanco) {
        this.nOMEMPcodbanco = nOMEMPcodbanco;
    }

    public String getNOMEMPdiscapac() {
        return nOMEMPdiscapac;
    }

    public void setNOMEMPdiscapac(String nOMEMPdiscapac) {
        this.nOMEMPdiscapac = nOMEMPdiscapac;
    }

    public String getCXCCLIcodigo() {
        return cXCCLIcodigo;
    }

    public void setCXCCLIcodigo(String cXCCLIcodigo) {
        this.cXCCLIcodigo = cXCCLIcodigo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (mEmpleadosPK != null ? mEmpleadosPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MEmpleados)) {
            return false;
        }
        MEmpleados other = (MEmpleados) object;
        if ((this.mEmpleadosPK == null && other.mEmpleadosPK != null) || (this.mEmpleadosPK != null && !this.mEmpleadosPK.equals(other.mEmpleadosPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.MEmpleados[ mEmpleadosPK=" + mEmpleadosPK + " ]";
    }
    
}
