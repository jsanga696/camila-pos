/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb6
 */
@Embeddable
public class CxcmClientePK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENCIA_codigo")
    private String gENCIAcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENOFI_codigo")
    private String gENOFIcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "CXCCLI_codigo")
    private String cXCCLIcodigo;

    public CxcmClientePK() {
    }

    public CxcmClientePK(String gENCIAcodigo, String gENOFIcodigo, String cXCCLIcodigo) {
        this.gENCIAcodigo = gENCIAcodigo;
        this.gENOFIcodigo = gENOFIcodigo;
        this.cXCCLIcodigo = cXCCLIcodigo;
    }

    public String getGENCIAcodigo() {
        return gENCIAcodigo;
    }

    public void setGENCIAcodigo(String gENCIAcodigo) {
        this.gENCIAcodigo = gENCIAcodigo;
    }

    public String getGENOFIcodigo() {
        return gENOFIcodigo;
    }

    public void setGENOFIcodigo(String gENOFIcodigo) {
        this.gENOFIcodigo = gENOFIcodigo;
    }

    public String getCXCCLIcodigo() {
        return cXCCLIcodigo;
    }

    public void setCXCCLIcodigo(String cXCCLIcodigo) {
        this.cXCCLIcodigo = cXCCLIcodigo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gENCIAcodigo != null ? gENCIAcodigo.hashCode() : 0);
        hash += (gENOFIcodigo != null ? gENOFIcodigo.hashCode() : 0);
        hash += (cXCCLIcodigo != null ? cXCCLIcodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CxcmClientePK)) {
            return false;
        }
        CxcmClientePK other = (CxcmClientePK) object;
        if ((this.gENCIAcodigo == null && other.gENCIAcodigo != null) || (this.gENCIAcodigo != null && !this.gENCIAcodigo.equals(other.gENCIAcodigo))) {
            return false;
        }
        if ((this.gENOFIcodigo == null && other.gENOFIcodigo != null) || (this.gENOFIcodigo != null && !this.gENOFIcodigo.equals(other.gENOFIcodigo))) {
            return false;
        }
        if ((this.cXCCLIcodigo == null && other.cXCCLIcodigo != null) || (this.cXCCLIcodigo != null && !this.cXCCLIcodigo.equals(other.cXCCLIcodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CxcmClientePK[ gENCIAcodigo=" + gENCIAcodigo + ", gENOFIcodigo=" + gENOFIcodigo + ", cXCCLIcodigo=" + cXCCLIcodigo + " ]";
    }
    
}
