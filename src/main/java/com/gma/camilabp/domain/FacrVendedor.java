/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb6
 */
@Entity
@Table(name = "FACR_VENDEDOR")
@NamedQueries({
    @NamedQuery(name = "FacrVendedor.findAll", query = "SELECT f FROM FacrVendedor f")})
public class FacrVendedor implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected FacrVendedorPK facrVendedorPK;
    @Size(max = 6)
    @Column(name = "FACJVE_codigo")
    private String fACJVEcodigo;
    @Size(max = 6)
    @Column(name = "FACSUP_codigo")
    private String fACSUPcodigo;
    @Size(max = 50)
    @Column(name = "FACVDR_nombre")
    private String fACVDRnombre;
    @Size(max = 2)
    @Column(name = "FACTVE_codigo")
    private String fACTVEcodigo;
    @Column(name = "FACVDR_estactivo")
    private Character fACVDRestactivo;
    @Size(max = 2)
    @Column(name = "FACVDR_puertcomhh")
    private String fACVDRpuertcomhh;
    @Column(name = "FACVDR_estventa")
    private Character fACVDRestventa;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "FACVDR_secpedidos")
    private BigDecimal fACVDRsecpedidos;
    @Column(name = "FACVDR_secnewclte")
    private BigDecimal fACVDRsecnewclte;
    @Size(max = 6)
    @Column(name = "RTKVDR_codcliente")
    private String rTKVDRcodcliente;
    @Column(name = "FACVDR_secpedhh")
    private BigDecimal fACVDRsecpedhh;
    @Column(name = "FACVDR_seccobro")
    private BigDecimal fACVDRseccobro;
    @Column(name = "FACVDR_seccobhh")
    private BigDecimal fACVDRseccobhh;
    @Column(name = "FACVDR_cltesfuera")
    private Character fACVDRcltesfuera;
    @Size(max = 14)
    @Column(name = "RTKMOV_serial")
    private String rTKMOVserial;
    @Size(max = 8)
    @Column(name = "FACVDR_passwordhh")
    private String fACVDRpasswordhh;
    @Column(name = "FACVDR_fechamod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fACVDRfechamod;
    @Column(name = "FACVDR_diasvisita")
    private Short fACVDRdiasvisita;
    @Column(name = "FACVDR_secvisita")
    private Integer fACVDRsecvisita;
    @Column(name = "MobileSync")
    private Character mobileSync;
    @Size(max = 60)
    @Column(name = "FACVDR_direccion")
    private String fACVDRdireccion;
    @Size(max = 15)
    @Column(name = "FACVDR_telefono1")
    private String fACVDRtelefono1;
    @Size(max = 15)
    @Column(name = "FACVDR_telefono2")
    private String fACVDRtelefono2;
    @Size(max = 15)
    @Column(name = "FACVDR_cedula")
    private String fACVDRcedula;
    @Column(name = "FACVDR_estsuspen")
    private Character fACVDRestsuspen;
    @Column(name = "FACVDR_fechaing")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fACVDRfechaing;
    @Size(max = 20)
    @Column(name = "FACVDR_relacional")
    private String fACVDRrelacional;
    @Size(max = 6)
    @Column(name = "CXCGRP_codigo")
    private String cXCGRPcodigo;
    @Column(name = "MobileSyncField")
    private Character mobileSyncField;
    @Column(name = "FACVDR_secdevhh")
    private Integer fACVDRsecdevhh;
    @Column(name = "FACVDR_fechaelimi")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fACVDRfechaelimi;
    @Size(max = 50)
    @Column(name = "FACVDR_nivelestud")
    private String fACVDRnivelestud;
    @Size(max = 4)
    @Column(name = "RTKCRG_codigo")
    private String rTKCRGcodigo;
    @Size(max = 4)
    @Column(name = "RTKCEV_codigo")
    private String rTKCEVcodigo;
    @Column(name = "Sync_estado")
    private Character syncestado;
    @Column(name = "Sync_fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date syncfecha;
    @Column(name = "FACVDR_secanthh")
    private Integer fACVDRsecanthh;
    @Column(name = "FACVDR_seccobpdhh")
    private Integer fACVDRseccobpdhh;
    @Column(name = "FACVDR_seccotizahh")
    private Integer fACVDRseccotizahh;
    @Size(max = 2)
    @Column(name = "FACVDR_inicltenew")
    private String fACVDRinicltenew;
    @Column(name = "FACVDR_estcltenew")
    private Character fACVDRestcltenew;
    @Column(name = "FACVDR_clteextra")
    private Character fACVDRclteextra;
    @Size(max = 6)
    @Column(name = "FACVDR_padre")
    private String fACVDRpadre;
    @Column(name = "FACVDR_IDpocket")
    private Integer fACVDRIDpocket;
    @Column(name = "FACVDR_tipvendedor")
    private Character fACVDRtipvendedor;
    @Size(max = 50)
    @Column(name = "FACVDR_email")
    private String fACVDRemail;
    @Size(max = 1)
    @Column(name = "FACVDR_sexo")
    private String fACVDRsexo;
    @Size(max = 1)
    @Column(name = "FACVDR_estadocivil")
    private String fACVDRestadocivil;
    @Column(name = "FACVDR_fechamacimiento")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fACVDRfechamacimiento;
    @Size(max = 20)
    @Column(name = "FACVDR_ciudad")
    private String fACVDRciudad;
    @Column(name = "FACVDR_mastercli")
    private Integer fACVDRmastercli;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FACVDR_idcodigo")
    private int fACVDRidcodigo;
    @Size(max = 1)
    @Column(name = "FACVDR_tipovend")
    private String fACVDRtipovend;
    @Size(max = 1)
    @Column(name = "FACVDR_reportpocket")
    private String fACVDRreportpocket;
    @Size(max = 25)
    @Column(name = "FACVDR_firstname")
    private String fACVDRfirstname;
    @Size(max = 25)
    @Column(name = "FACVDR_lastname")
    private String fACVDRlastname;
    @Size(max = 10)
    @Column(name = "FACVDR_zonaventa")
    private String fACVDRzonaventa;
    @Size(max = 1)
    @Column(name = "FACVDR_excluvivend")
    private String fACVDRexcluvivend;
    @Size(max = 4)
    @Column(name = "GENBOD_codbodega")
    private String gENBODcodbodega;
    @Size(max = 4)
    @Column(name = "FACVDR_ofirelaci")
    private String fACVDRofirelaci;
    @Size(max = 6)
    @Column(name = "FACVDR_vendrelaci")
    private String fACVDRvendrelaci;
    @Size(max = 1)
    @Column(name = "FACVDR_estacobranza")
    private String fACVDRestacobranza;
    @Size(max = 1)
    @Column(name = "FACVDR_impRCJdispo")
    private String fACVDRimpRCJdispo;
    @Size(max = 1)
    @Column(name = "FACVDR_impPEDdispo")
    private String fACVDRimpPEDdispo;
    @Size(max = 4)
    @Column(name = "FACVDR_oficinadms")
    private String fACVDRoficinadms;
    @Size(max = 1)
    @Column(name = "FACVDR_tipofiltro")
    private String fACVDRtipofiltro;
    @Size(max = 1)
    @Column(name = "FACVDR_aplicadscto")
    private String fACVDRaplicadscto;
    @Column(name = "FACVDR_secreposicion")
    private BigDecimal fACVDRsecreposicion;

    public FacrVendedor() {
    }

    public FacrVendedor(FacrVendedorPK facrVendedorPK) {
        this.facrVendedorPK = facrVendedorPK;
    }

    public FacrVendedor(FacrVendedorPK facrVendedorPK, int fACVDRidcodigo) {
        this.facrVendedorPK = facrVendedorPK;
        this.fACVDRidcodigo = fACVDRidcodigo;
    }

    public FacrVendedor(String gENCIAcodigo, String fACVDRcodigo, String gENOFIcodigo) {
        this.facrVendedorPK = new FacrVendedorPK(gENCIAcodigo, fACVDRcodigo, gENOFIcodigo);
    }

    public FacrVendedorPK getFacrVendedorPK() {
        return facrVendedorPK;
    }

    public void setFacrVendedorPK(FacrVendedorPK facrVendedorPK) {
        this.facrVendedorPK = facrVendedorPK;
    }

    public String getFACJVEcodigo() {
        return fACJVEcodigo;
    }

    public void setFACJVEcodigo(String fACJVEcodigo) {
        this.fACJVEcodigo = fACJVEcodigo;
    }

    public String getFACSUPcodigo() {
        return fACSUPcodigo;
    }

    public void setFACSUPcodigo(String fACSUPcodigo) {
        this.fACSUPcodigo = fACSUPcodigo;
    }

    public String getFACVDRnombre() {
        return fACVDRnombre;
    }

    public void setFACVDRnombre(String fACVDRnombre) {
        this.fACVDRnombre = fACVDRnombre;
    }

    public String getFACTVEcodigo() {
        return fACTVEcodigo;
    }

    public void setFACTVEcodigo(String fACTVEcodigo) {
        this.fACTVEcodigo = fACTVEcodigo;
    }

    public Character getFACVDRestactivo() {
        return fACVDRestactivo;
    }

    public void setFACVDRestactivo(Character fACVDRestactivo) {
        this.fACVDRestactivo = fACVDRestactivo;
    }

    public String getFACVDRpuertcomhh() {
        return fACVDRpuertcomhh;
    }

    public void setFACVDRpuertcomhh(String fACVDRpuertcomhh) {
        this.fACVDRpuertcomhh = fACVDRpuertcomhh;
    }

    public Character getFACVDRestventa() {
        return fACVDRestventa;
    }

    public void setFACVDRestventa(Character fACVDRestventa) {
        this.fACVDRestventa = fACVDRestventa;
    }

    public BigDecimal getFACVDRsecpedidos() {
        return fACVDRsecpedidos;
    }

    public void setFACVDRsecpedidos(BigDecimal fACVDRsecpedidos) {
        this.fACVDRsecpedidos = fACVDRsecpedidos;
    }

    public BigDecimal getFACVDRsecnewclte() {
        return fACVDRsecnewclte;
    }

    public void setFACVDRsecnewclte(BigDecimal fACVDRsecnewclte) {
        this.fACVDRsecnewclte = fACVDRsecnewclte;
    }

    public String getRTKVDRcodcliente() {
        return rTKVDRcodcliente;
    }

    public void setRTKVDRcodcliente(String rTKVDRcodcliente) {
        this.rTKVDRcodcliente = rTKVDRcodcliente;
    }

    public BigDecimal getFACVDRsecpedhh() {
        return fACVDRsecpedhh;
    }

    public void setFACVDRsecpedhh(BigDecimal fACVDRsecpedhh) {
        this.fACVDRsecpedhh = fACVDRsecpedhh;
    }

    public BigDecimal getFACVDRseccobro() {
        return fACVDRseccobro;
    }

    public void setFACVDRseccobro(BigDecimal fACVDRseccobro) {
        this.fACVDRseccobro = fACVDRseccobro;
    }

    public BigDecimal getFACVDRseccobhh() {
        return fACVDRseccobhh;
    }

    public void setFACVDRseccobhh(BigDecimal fACVDRseccobhh) {
        this.fACVDRseccobhh = fACVDRseccobhh;
    }

    public Character getFACVDRcltesfuera() {
        return fACVDRcltesfuera;
    }

    public void setFACVDRcltesfuera(Character fACVDRcltesfuera) {
        this.fACVDRcltesfuera = fACVDRcltesfuera;
    }

    public String getRTKMOVserial() {
        return rTKMOVserial;
    }

    public void setRTKMOVserial(String rTKMOVserial) {
        this.rTKMOVserial = rTKMOVserial;
    }

    public String getFACVDRpasswordhh() {
        return fACVDRpasswordhh;
    }

    public void setFACVDRpasswordhh(String fACVDRpasswordhh) {
        this.fACVDRpasswordhh = fACVDRpasswordhh;
    }

    public Date getFACVDRfechamod() {
        return fACVDRfechamod;
    }

    public void setFACVDRfechamod(Date fACVDRfechamod) {
        this.fACVDRfechamod = fACVDRfechamod;
    }

    public Short getFACVDRdiasvisita() {
        return fACVDRdiasvisita;
    }

    public void setFACVDRdiasvisita(Short fACVDRdiasvisita) {
        this.fACVDRdiasvisita = fACVDRdiasvisita;
    }

    public Integer getFACVDRsecvisita() {
        return fACVDRsecvisita;
    }

    public void setFACVDRsecvisita(Integer fACVDRsecvisita) {
        this.fACVDRsecvisita = fACVDRsecvisita;
    }

    public Character getMobileSync() {
        return mobileSync;
    }

    public void setMobileSync(Character mobileSync) {
        this.mobileSync = mobileSync;
    }

    public String getFACVDRdireccion() {
        return fACVDRdireccion;
    }

    public void setFACVDRdireccion(String fACVDRdireccion) {
        this.fACVDRdireccion = fACVDRdireccion;
    }

    public String getFACVDRtelefono1() {
        return fACVDRtelefono1;
    }

    public void setFACVDRtelefono1(String fACVDRtelefono1) {
        this.fACVDRtelefono1 = fACVDRtelefono1;
    }

    public String getFACVDRtelefono2() {
        return fACVDRtelefono2;
    }

    public void setFACVDRtelefono2(String fACVDRtelefono2) {
        this.fACVDRtelefono2 = fACVDRtelefono2;
    }

    public String getFACVDRcedula() {
        return fACVDRcedula;
    }

    public void setFACVDRcedula(String fACVDRcedula) {
        this.fACVDRcedula = fACVDRcedula;
    }

    public Character getFACVDRestsuspen() {
        return fACVDRestsuspen;
    }

    public void setFACVDRestsuspen(Character fACVDRestsuspen) {
        this.fACVDRestsuspen = fACVDRestsuspen;
    }

    public Date getFACVDRfechaing() {
        return fACVDRfechaing;
    }

    public void setFACVDRfechaing(Date fACVDRfechaing) {
        this.fACVDRfechaing = fACVDRfechaing;
    }

    public String getFACVDRrelacional() {
        return fACVDRrelacional;
    }

    public void setFACVDRrelacional(String fACVDRrelacional) {
        this.fACVDRrelacional = fACVDRrelacional;
    }

    public String getCXCGRPcodigo() {
        return cXCGRPcodigo;
    }

    public void setCXCGRPcodigo(String cXCGRPcodigo) {
        this.cXCGRPcodigo = cXCGRPcodigo;
    }

    public Character getMobileSyncField() {
        return mobileSyncField;
    }

    public void setMobileSyncField(Character mobileSyncField) {
        this.mobileSyncField = mobileSyncField;
    }

    public Integer getFACVDRsecdevhh() {
        return fACVDRsecdevhh;
    }

    public void setFACVDRsecdevhh(Integer fACVDRsecdevhh) {
        this.fACVDRsecdevhh = fACVDRsecdevhh;
    }

    public Date getFACVDRfechaelimi() {
        return fACVDRfechaelimi;
    }

    public void setFACVDRfechaelimi(Date fACVDRfechaelimi) {
        this.fACVDRfechaelimi = fACVDRfechaelimi;
    }

    public String getFACVDRnivelestud() {
        return fACVDRnivelestud;
    }

    public void setFACVDRnivelestud(String fACVDRnivelestud) {
        this.fACVDRnivelestud = fACVDRnivelestud;
    }

    public String getRTKCRGcodigo() {
        return rTKCRGcodigo;
    }

    public void setRTKCRGcodigo(String rTKCRGcodigo) {
        this.rTKCRGcodigo = rTKCRGcodigo;
    }

    public String getRTKCEVcodigo() {
        return rTKCEVcodigo;
    }

    public void setRTKCEVcodigo(String rTKCEVcodigo) {
        this.rTKCEVcodigo = rTKCEVcodigo;
    }

    public Character getSyncestado() {
        return syncestado;
    }

    public void setSyncestado(Character syncestado) {
        this.syncestado = syncestado;
    }

    public Date getSyncfecha() {
        return syncfecha;
    }

    public void setSyncfecha(Date syncfecha) {
        this.syncfecha = syncfecha;
    }

    public Integer getFACVDRsecanthh() {
        return fACVDRsecanthh;
    }

    public void setFACVDRsecanthh(Integer fACVDRsecanthh) {
        this.fACVDRsecanthh = fACVDRsecanthh;
    }

    public Integer getFACVDRseccobpdhh() {
        return fACVDRseccobpdhh;
    }

    public void setFACVDRseccobpdhh(Integer fACVDRseccobpdhh) {
        this.fACVDRseccobpdhh = fACVDRseccobpdhh;
    }

    public Integer getFACVDRseccotizahh() {
        return fACVDRseccotizahh;
    }

    public void setFACVDRseccotizahh(Integer fACVDRseccotizahh) {
        this.fACVDRseccotizahh = fACVDRseccotizahh;
    }

    public String getFACVDRinicltenew() {
        return fACVDRinicltenew;
    }

    public void setFACVDRinicltenew(String fACVDRinicltenew) {
        this.fACVDRinicltenew = fACVDRinicltenew;
    }

    public Character getFACVDRestcltenew() {
        return fACVDRestcltenew;
    }

    public void setFACVDRestcltenew(Character fACVDRestcltenew) {
        this.fACVDRestcltenew = fACVDRestcltenew;
    }

    public Character getFACVDRclteextra() {
        return fACVDRclteextra;
    }

    public void setFACVDRclteextra(Character fACVDRclteextra) {
        this.fACVDRclteextra = fACVDRclteextra;
    }

    public String getFACVDRpadre() {
        return fACVDRpadre;
    }

    public void setFACVDRpadre(String fACVDRpadre) {
        this.fACVDRpadre = fACVDRpadre;
    }

    public Integer getFACVDRIDpocket() {
        return fACVDRIDpocket;
    }

    public void setFACVDRIDpocket(Integer fACVDRIDpocket) {
        this.fACVDRIDpocket = fACVDRIDpocket;
    }

    public Character getFACVDRtipvendedor() {
        return fACVDRtipvendedor;
    }

    public void setFACVDRtipvendedor(Character fACVDRtipvendedor) {
        this.fACVDRtipvendedor = fACVDRtipvendedor;
    }

    public String getFACVDRemail() {
        return fACVDRemail;
    }

    public void setFACVDRemail(String fACVDRemail) {
        this.fACVDRemail = fACVDRemail;
    }

    public String getFACVDRsexo() {
        return fACVDRsexo;
    }

    public void setFACVDRsexo(String fACVDRsexo) {
        this.fACVDRsexo = fACVDRsexo;
    }

    public String getFACVDRestadocivil() {
        return fACVDRestadocivil;
    }

    public void setFACVDRestadocivil(String fACVDRestadocivil) {
        this.fACVDRestadocivil = fACVDRestadocivil;
    }

    public Date getFACVDRfechamacimiento() {
        return fACVDRfechamacimiento;
    }

    public void setFACVDRfechamacimiento(Date fACVDRfechamacimiento) {
        this.fACVDRfechamacimiento = fACVDRfechamacimiento;
    }

    public String getFACVDRciudad() {
        return fACVDRciudad;
    }

    public void setFACVDRciudad(String fACVDRciudad) {
        this.fACVDRciudad = fACVDRciudad;
    }

    public Integer getFACVDRmastercli() {
        return fACVDRmastercli;
    }

    public void setFACVDRmastercli(Integer fACVDRmastercli) {
        this.fACVDRmastercli = fACVDRmastercli;
    }

    public int getFACVDRidcodigo() {
        return fACVDRidcodigo;
    }

    public void setFACVDRidcodigo(int fACVDRidcodigo) {
        this.fACVDRidcodigo = fACVDRidcodigo;
    }

    public String getFACVDRtipovend() {
        return fACVDRtipovend;
    }

    public void setFACVDRtipovend(String fACVDRtipovend) {
        this.fACVDRtipovend = fACVDRtipovend;
    }

    public String getFACVDRreportpocket() {
        return fACVDRreportpocket;
    }

    public void setFACVDRreportpocket(String fACVDRreportpocket) {
        this.fACVDRreportpocket = fACVDRreportpocket;
    }

    public String getFACVDRfirstname() {
        return fACVDRfirstname;
    }

    public void setFACVDRfirstname(String fACVDRfirstname) {
        this.fACVDRfirstname = fACVDRfirstname;
    }

    public String getFACVDRlastname() {
        return fACVDRlastname;
    }

    public void setFACVDRlastname(String fACVDRlastname) {
        this.fACVDRlastname = fACVDRlastname;
    }

    public String getFACVDRzonaventa() {
        return fACVDRzonaventa;
    }

    public void setFACVDRzonaventa(String fACVDRzonaventa) {
        this.fACVDRzonaventa = fACVDRzonaventa;
    }

    public String getFACVDRexcluvivend() {
        return fACVDRexcluvivend;
    }

    public void setFACVDRexcluvivend(String fACVDRexcluvivend) {
        this.fACVDRexcluvivend = fACVDRexcluvivend;
    }

    public String getGENBODcodbodega() {
        return gENBODcodbodega;
    }

    public void setGENBODcodbodega(String gENBODcodbodega) {
        this.gENBODcodbodega = gENBODcodbodega;
    }

    public String getFACVDRofirelaci() {
        return fACVDRofirelaci;
    }

    public void setFACVDRofirelaci(String fACVDRofirelaci) {
        this.fACVDRofirelaci = fACVDRofirelaci;
    }

    public String getFACVDRvendrelaci() {
        return fACVDRvendrelaci;
    }

    public void setFACVDRvendrelaci(String fACVDRvendrelaci) {
        this.fACVDRvendrelaci = fACVDRvendrelaci;
    }

    public String getFACVDRestacobranza() {
        return fACVDRestacobranza;
    }

    public void setFACVDRestacobranza(String fACVDRestacobranza) {
        this.fACVDRestacobranza = fACVDRestacobranza;
    }

    public String getFACVDRimpRCJdispo() {
        return fACVDRimpRCJdispo;
    }

    public void setFACVDRimpRCJdispo(String fACVDRimpRCJdispo) {
        this.fACVDRimpRCJdispo = fACVDRimpRCJdispo;
    }

    public String getFACVDRimpPEDdispo() {
        return fACVDRimpPEDdispo;
    }

    public void setFACVDRimpPEDdispo(String fACVDRimpPEDdispo) {
        this.fACVDRimpPEDdispo = fACVDRimpPEDdispo;
    }

    public String getFACVDRoficinadms() {
        return fACVDRoficinadms;
    }

    public void setFACVDRoficinadms(String fACVDRoficinadms) {
        this.fACVDRoficinadms = fACVDRoficinadms;
    }

    public String getFACVDRtipofiltro() {
        return fACVDRtipofiltro;
    }

    public void setFACVDRtipofiltro(String fACVDRtipofiltro) {
        this.fACVDRtipofiltro = fACVDRtipofiltro;
    }

    public String getFACVDRaplicadscto() {
        return fACVDRaplicadscto;
    }

    public void setFACVDRaplicadscto(String fACVDRaplicadscto) {
        this.fACVDRaplicadscto = fACVDRaplicadscto;
    }

    public BigDecimal getFACVDRsecreposicion() {
        return fACVDRsecreposicion;
    }

    public void setFACVDRsecreposicion(BigDecimal fACVDRsecreposicion) {
        this.fACVDRsecreposicion = fACVDRsecreposicion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (facrVendedorPK != null ? facrVendedorPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FacrVendedor)) {
            return false;
        }
        FacrVendedor other = (FacrVendedor) object;
        if ((this.facrVendedorPK == null && other.facrVendedorPK != null) || (this.facrVendedorPK != null && !this.facrVendedorPK.equals(other.facrVendedorPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.FacrVendedor[ facrVendedorPK=" + facrVendedorPK + " ]";
    }
    
}
