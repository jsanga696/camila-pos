/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb6
 */
@Entity
@Table(name = "CXCR_TERMIPAGO")
@NamedQueries({
    @NamedQuery(name = "CxcrTermipago.findAll", query = "SELECT c FROM CxcrTermipago c")})
public class CxcrTermipago implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "CXCTPG_codigo")
    private String cXCTPGcodigo;
    @Size(max = 15)
    @Column(name = "CXCTPG_descripcio")
    private String cXCTPGdescripcio;
    @Column(name = "CXCTPG_plazo")
    private Short cXCTPGplazo;
    @Column(name = "CXCTPG_estado")
    private Character cXCTPGestado;
    @Size(max = 4)
    @Column(name = "GENCIA_codigo")
    private String gENCIAcodigo;
    /*@OneToMany(mappedBy = "cXCTPGcodigo", fetch = FetchType.LAZY)
    private Collection<CxcmCliente> cxcmClienteCollection;*/

    public CxcrTermipago() {
    }

    public CxcrTermipago(String cXCTPGcodigo) {
        this.cXCTPGcodigo = cXCTPGcodigo;
    }

    public String getCXCTPGcodigo() {
        return cXCTPGcodigo;
    }

    public void setCXCTPGcodigo(String cXCTPGcodigo) {
        this.cXCTPGcodigo = cXCTPGcodigo;
    }

    public String getCXCTPGdescripcio() {
        return cXCTPGdescripcio;
    }

    public void setCXCTPGdescripcio(String cXCTPGdescripcio) {
        this.cXCTPGdescripcio = cXCTPGdescripcio;
    }

    public Short getCXCTPGplazo() {
        return cXCTPGplazo;
    }

    public void setCXCTPGplazo(Short cXCTPGplazo) {
        this.cXCTPGplazo = cXCTPGplazo;
    }

    public Character getCXCTPGestado() {
        return cXCTPGestado;
    }

    public void setCXCTPGestado(Character cXCTPGestado) {
        this.cXCTPGestado = cXCTPGestado;
    }

    public String getGENCIAcodigo() {
        return gENCIAcodigo;
    }

    public void setGENCIAcodigo(String gENCIAcodigo) {
        this.gENCIAcodigo = gENCIAcodigo;
    }

    /*public Collection<CxcmCliente> getCxcmClienteCollection() {
        return cxcmClienteCollection;
    }

    public void setCxcmClienteCollection(Collection<CxcmCliente> cxcmClienteCollection) {
        this.cxcmClienteCollection = cxcmClienteCollection;
    }*/

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cXCTPGcodigo != null ? cXCTPGcodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CxcrTermipago)) {
            return false;
        }
        CxcrTermipago other = (CxcrTermipago) object;
        if ((this.cXCTPGcodigo == null && other.cXCTPGcodigo != null) || (this.cXCTPGcodigo != null && !this.cXCTPGcodigo.equals(other.cXCTPGcodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CxcrTermipago[ cXCTPGcodigo=" + cXCTPGcodigo + " ]";
    }
    
}
