/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb6
 */
@Embeddable
public class InvmItemxbodegaPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENCIA_codigo")
    private String gENCIAcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENOFI_codigo")
    private String gENOFIcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENBOD_codigo")
    private String gENBODcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "INVITM_codigo")
    private String iNVITMcodigo;

    public InvmItemxbodegaPK() {
    }

    public InvmItemxbodegaPK(String gENCIAcodigo, String gENOFIcodigo, String gENBODcodigo, String iNVITMcodigo) {
        this.gENCIAcodigo = gENCIAcodigo;
        this.gENOFIcodigo = gENOFIcodigo;
        this.gENBODcodigo = gENBODcodigo;
        this.iNVITMcodigo = iNVITMcodigo;
    }

    public String getGENCIAcodigo() {
        return gENCIAcodigo;
    }

    public void setGENCIAcodigo(String gENCIAcodigo) {
        this.gENCIAcodigo = gENCIAcodigo;
    }

    public String getGENOFIcodigo() {
        return gENOFIcodigo;
    }

    public void setGENOFIcodigo(String gENOFIcodigo) {
        this.gENOFIcodigo = gENOFIcodigo;
    }

    public String getGENBODcodigo() {
        return gENBODcodigo;
    }

    public void setGENBODcodigo(String gENBODcodigo) {
        this.gENBODcodigo = gENBODcodigo;
    }

    public String getINVITMcodigo() {
        return iNVITMcodigo;
    }

    public void setINVITMcodigo(String iNVITMcodigo) {
        this.iNVITMcodigo = iNVITMcodigo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gENCIAcodigo != null ? gENCIAcodigo.hashCode() : 0);
        hash += (gENOFIcodigo != null ? gENOFIcodigo.hashCode() : 0);
        hash += (gENBODcodigo != null ? gENBODcodigo.hashCode() : 0);
        hash += (iNVITMcodigo != null ? iNVITMcodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InvmItemxbodegaPK)) {
            return false;
        }
        InvmItemxbodegaPK other = (InvmItemxbodegaPK) object;
        if ((this.gENCIAcodigo == null && other.gENCIAcodigo != null) || (this.gENCIAcodigo != null && !this.gENCIAcodigo.equals(other.gENCIAcodigo))) {
            return false;
        }
        if ((this.gENOFIcodigo == null && other.gENOFIcodigo != null) || (this.gENOFIcodigo != null && !this.gENOFIcodigo.equals(other.gENOFIcodigo))) {
            return false;
        }
        if ((this.gENBODcodigo == null && other.gENBODcodigo != null) || (this.gENBODcodigo != null && !this.gENBODcodigo.equals(other.gENBODcodigo))) {
            return false;
        }
        if ((this.iNVITMcodigo == null && other.iNVITMcodigo != null) || (this.iNVITMcodigo != null && !this.iNVITMcodigo.equals(other.iNVITMcodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.InvmItemxbodegaPK[ gENCIAcodigo=" + gENCIAcodigo + ", gENOFIcodigo=" + gENOFIcodigo + ", gENBODcodigo=" + gENBODcodigo + ", iNVITMcodigo=" + iNVITMcodigo + " ]";
    }
    
}
