/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
//import org.hibernate.annotations.Formula;

/**
 *
 * @author Acer
 */
@Entity
@Table(name = "INVT_DETOTROS")
@NamedQueries({
    @NamedQuery(name = "InvtDetotros.findAll", query = "SELECT i FROM InvtDetotros i")})
public class InvtDetotros implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected InvtDetotrosPK invtDetotrosPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "INVCOT_numdocumen")
    private int iNVCOTnumdocumen;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENCIA_codigo")
    private String gENCIAcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENOFI_codigo")
    private String gENOFIcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "GENMOD_codigo")
    private String gENMODcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "GENTDO_codigo")
    private String gENTDOcodigo;
    @Size(max = 15)
    @Column(name = "INVITM_codigo")
    private String iNVITMcodigo;
    @Size(max = 4)
    @Column(name = "GENBOD_codigo")
    private String gENBODcodigo;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "INVDOT_unidembala")
    private BigDecimal iNVDOTunidembala;
    @Column(name = "INVDOT_precio")
    private BigDecimal iNVDOTprecio;
    @Column(name = "INVDOT_canticaja")
    private BigDecimal iNVDOTcanticaja;
    @Column(name = "INVDOT_cantiunida")
    private BigDecimal iNVDOTcantiunida;
    @Column(name = "INVDOT_cantifunci")
    private BigDecimal iNVDOTcantifunci;
    @Column(name = "INVDOT_cantfunpen")
    private BigDecimal iNVDOTcantfunpen;
    @Column(name = "INVDOT_cantegres")
    private BigDecimal iNVDOTcantegres;
    @Size(max = 10)
    @Column(name = "INVUPR_unibasotro")
    private String iNVUPRunibasotro;
    @Size(max = 10)
    @Column(name = "INVUPR_unirelotro")
    private String iNVUPRunirelotro;
    @Column(name = "INVDOT_costingmov")
    private BigDecimal iNVDOTcostingmov;
    @Column(name = "INVDOT_subtotamov")
    private BigDecimal iNVDOTsubtotamov;
    @Column(name = "INVDOT_desctomov")
    private BigDecimal iNVDOTdesctomov;
    @Column(name = "INVDOT_totimpumov")
    private BigDecimal iNVDOTtotimpumov;
    @Column(name = "INVDOT_netomov")
    private BigDecimal iNVDOTnetomov;
    @Column(name = "INVDOT_stkpending")
    private BigDecimal iNVDOTstkpending;
    @Column(name = "INVDOT_stkpendegr")
    private BigDecimal iNVDOTstkpendegr;
    @Size(max = 1)
    @Column(name = "INVDOT_estado")
    private String iNVDOTestado;
    @Column(name = "INVDOT_fechaelabo")
    @Temporal(TemporalType.TIMESTAMP)
    private Date iNVDOTfechaelabo;
    @Column(name = "INVDOT_fechaexpira")
    @Temporal(TemporalType.TIMESTAMP)
    private Date iNVDOTfechaexpira;
    @Size(max = 25)
    @Column(name = "INVDOT_numlote")
    private String iNVDOTnumlote;
    @Size(max = 10)
    @Column(name = "INVDOT_unidRelTrans")
    private String iNVDOTunidRelTrans;
    //@Formula(value = "(SELECT i.INVITM_nombre FROM INVM_ITEM i WHERE i.INVITM_codigo=INVITM_codigo AND i.GENCIA_codigo=GENCIA_codigo)")
    @Transient
    private String nombreItem;

    public InvtDetotros() {
    }

    public InvtDetotros(InvtDetotrosPK invtDetotrosPK) {
        this.invtDetotrosPK = invtDetotrosPK;
    }

    public InvtDetotros(InvtDetotrosPK invtDetotrosPK, int iNVCOTnumdocumen, String gENCIAcodigo, String gENOFIcodigo, String gENMODcodigo, String gENTDOcodigo) {
        this.invtDetotrosPK = invtDetotrosPK;
        this.iNVCOTnumdocumen = iNVCOTnumdocumen;
        this.gENCIAcodigo = gENCIAcodigo;
        this.gENOFIcodigo = gENOFIcodigo;
        this.gENMODcodigo = gENMODcodigo;
        this.gENTDOcodigo = gENTDOcodigo;
    }

    public InvtDetotros(Long iNVCOTnumsecuenc, short iNVDOTlinea) {
        this.invtDetotrosPK = new InvtDetotrosPK(iNVCOTnumsecuenc, iNVDOTlinea);
    }

    public InvtDetotrosPK getInvtDetotrosPK() {
        return invtDetotrosPK;
    }

    public void setInvtDetotrosPK(InvtDetotrosPK invtDetotrosPK) {
        this.invtDetotrosPK = invtDetotrosPK;
    }

    public int getINVCOTnumdocumen() {
        return iNVCOTnumdocumen;
    }

    public void setINVCOTnumdocumen(int iNVCOTnumdocumen) {
        this.iNVCOTnumdocumen = iNVCOTnumdocumen;
    }

    public String getGENCIAcodigo() {
        return gENCIAcodigo;
    }

    public void setGENCIAcodigo(String gENCIAcodigo) {
        this.gENCIAcodigo = gENCIAcodigo;
    }

    public String getGENOFIcodigo() {
        return gENOFIcodigo;
    }

    public void setGENOFIcodigo(String gENOFIcodigo) {
        this.gENOFIcodigo = gENOFIcodigo;
    }

    public String getGENMODcodigo() {
        return gENMODcodigo;
    }

    public void setGENMODcodigo(String gENMODcodigo) {
        this.gENMODcodigo = gENMODcodigo;
    }

    public String getGENTDOcodigo() {
        return gENTDOcodigo;
    }

    public void setGENTDOcodigo(String gENTDOcodigo) {
        this.gENTDOcodigo = gENTDOcodigo;
    }

    public String getINVITMcodigo() {
        return iNVITMcodigo;
    }

    public void setINVITMcodigo(String iNVITMcodigo) {
        this.iNVITMcodigo = iNVITMcodigo;
    }

    public String getGENBODcodigo() {
        return gENBODcodigo;
    }

    public void setGENBODcodigo(String gENBODcodigo) {
        this.gENBODcodigo = gENBODcodigo;
    }

    public BigDecimal getINVDOTunidembala() {
        return iNVDOTunidembala;
    }

    public void setINVDOTunidembala(BigDecimal iNVDOTunidembala) {
        this.iNVDOTunidembala = iNVDOTunidembala;
    }

    public BigDecimal getINVDOTprecio() {
        return iNVDOTprecio;
    }

    public void setINVDOTprecio(BigDecimal iNVDOTprecio) {
        this.iNVDOTprecio = iNVDOTprecio;
    }

    public BigDecimal getINVDOTcanticaja() {
        return iNVDOTcanticaja;
    }

    public void setINVDOTcanticaja(BigDecimal iNVDOTcanticaja) {
        this.iNVDOTcanticaja = iNVDOTcanticaja;
    }

    public BigDecimal getINVDOTcantiunida() {
        return iNVDOTcantiunida;
    }

    public void setINVDOTcantiunida(BigDecimal iNVDOTcantiunida) {
        this.iNVDOTcantiunida = iNVDOTcantiunida;
    }

    public BigDecimal getINVDOTcantifunci() {
        return iNVDOTcantifunci;
    }

    public void setINVDOTcantifunci(BigDecimal iNVDOTcantifunci) {
        this.iNVDOTcantifunci = iNVDOTcantifunci;
    }

    public BigDecimal getINVDOTcantfunpen() {
        return iNVDOTcantfunpen;
    }

    public void setINVDOTcantfunpen(BigDecimal iNVDOTcantfunpen) {
        this.iNVDOTcantfunpen = iNVDOTcantfunpen;
    }

    public BigDecimal getINVDOTcantegres() {
        return iNVDOTcantegres;
    }

    public void setINVDOTcantegres(BigDecimal iNVDOTcantegres) {
        this.iNVDOTcantegres = iNVDOTcantegres;
    }

    public String getINVUPRunibasotro() {
        return iNVUPRunibasotro;
    }

    public void setINVUPRunibasotro(String iNVUPRunibasotro) {
        this.iNVUPRunibasotro = iNVUPRunibasotro;
    }

    public String getINVUPRunirelotro() {
        return iNVUPRunirelotro;
    }

    public void setINVUPRunirelotro(String iNVUPRunirelotro) {
        this.iNVUPRunirelotro = iNVUPRunirelotro;
    }

    public BigDecimal getINVDOTcostingmov() {
        return iNVDOTcostingmov;
    }

    public void setINVDOTcostingmov(BigDecimal iNVDOTcostingmov) {
        this.iNVDOTcostingmov = iNVDOTcostingmov;
    }

    public BigDecimal getINVDOTsubtotamov() {
        return iNVDOTsubtotamov;
    }

    public void setINVDOTsubtotamov(BigDecimal iNVDOTsubtotamov) {
        this.iNVDOTsubtotamov = iNVDOTsubtotamov;
    }

    public BigDecimal getINVDOTdesctomov() {
        return iNVDOTdesctomov;
    }

    public void setINVDOTdesctomov(BigDecimal iNVDOTdesctomov) {
        this.iNVDOTdesctomov = iNVDOTdesctomov;
    }

    public BigDecimal getINVDOTtotimpumov() {
        return iNVDOTtotimpumov;
    }

    public void setINVDOTtotimpumov(BigDecimal iNVDOTtotimpumov) {
        this.iNVDOTtotimpumov = iNVDOTtotimpumov;
    }

    public BigDecimal getINVDOTnetomov() {
        return iNVDOTnetomov;
    }

    public void setINVDOTnetomov(BigDecimal iNVDOTnetomov) {
        this.iNVDOTnetomov = iNVDOTnetomov;
    }

    public BigDecimal getINVDOTstkpending() {
        return iNVDOTstkpending;
    }

    public void setINVDOTstkpending(BigDecimal iNVDOTstkpending) {
        this.iNVDOTstkpending = iNVDOTstkpending;
    }

    public BigDecimal getINVDOTstkpendegr() {
        return iNVDOTstkpendegr;
    }

    public void setINVDOTstkpendegr(BigDecimal iNVDOTstkpendegr) {
        this.iNVDOTstkpendegr = iNVDOTstkpendegr;
    }

    public String getINVDOTestado() {
        return iNVDOTestado;
    }

    public void setINVDOTestado(String iNVDOTestado) {
        this.iNVDOTestado = iNVDOTestado;
    }

    public Date getINVDOTfechaelabo() {
        return iNVDOTfechaelabo;
    }

    public void setINVDOTfechaelabo(Date iNVDOTfechaelabo) {
        this.iNVDOTfechaelabo = iNVDOTfechaelabo;
    }

    public Date getINVDOTfechaexpira() {
        return iNVDOTfechaexpira;
    }

    public void setINVDOTfechaexpira(Date iNVDOTfechaexpira) {
        this.iNVDOTfechaexpira = iNVDOTfechaexpira;
    }

    public String getINVDOTnumlote() {
        return iNVDOTnumlote;
    }

    public void setINVDOTnumlote(String iNVDOTnumlote) {
        this.iNVDOTnumlote = iNVDOTnumlote;
    }

    public String getINVDOTunidRelTrans() {
        return iNVDOTunidRelTrans;
    }

    public void setINVDOTunidRelTrans(String iNVDOTunidRelTrans) {
        this.iNVDOTunidRelTrans = iNVDOTunidRelTrans;
    }

    public String getNombreItem() {
        return nombreItem;
    }

    public void setNombreItem(String nombreItem) {
        this.nombreItem = nombreItem;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (invtDetotrosPK != null ? invtDetotrosPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InvtDetotros)) {
            return false;
        }
        InvtDetotros other = (InvtDetotros) object;
        if ((this.invtDetotrosPK == null && other.invtDetotrosPK != null) || (this.invtDetotrosPK != null && !this.invtDetotrosPK.equals(other.invtDetotrosPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.InvtDetotros[ invtDetotrosPK=" + invtDetotrosPK + " ]";
    }
    
}
