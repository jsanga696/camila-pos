/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
//import org.hibernate.annotations.Formula;

/**
 *
 * @author userdb6
 */
@Entity
@Table(name = "GENM_CAJA")
@NamedQueries({
    @NamedQuery(name = "GenmCaja.findAll", query = "SELECT g FROM GenmCaja g")})
public class GenmCaja implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "FACSCA_idsececaja", nullable = false)
    private Long facscaIdsececaja;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENCIA_codigo")
    private String gENCIAcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENOFI_codigo")
    private String gENOFIcodigo;
    @Size(max = 4)
    @Column(name = "GENCAJ_codigo")
    private String gENCAJcodigo;
    @Size(max = 50)
    @Column(name = "GENCAJ_descripcio")
    private String gENCAJdescripcio;
    @Size(max = 3)
    @Column(name = "SEGUSU_codigo")
    private String sEGUSUcodigo;
    @Size(max = 6)
    @Column(name = "FACVDR_codigo")
    private String fACVDRcodigo;
    @Size(max = 1)
    @Column(name = "GENCAJ_estado")
    private String gENCAJestado="A";
    @Size(max = 1)
    @Column(name = "GENCAJ_estapertur")
    private String gENCAJestapertur="N";
    @Size(max = 4)
    @Column(name = "GENBOD_codigo")
    private String gENBODcodigo;
    @Column(name = "GENCAJ_secuenvta")
    private Integer gENCAJsecuenvta=0;
    @Column(name = "GENCAJ_secvalecaj")
    private Integer gENCAJsecvalecaj=0;
    @JoinColumn(name = "gental_secuencia", referencedColumnName = "gental_secuencia")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private GenpTalonarios gentalSecuencia;
    @JoinColumn(name = "INVLPR_secuencia", referencedColumnName = "INVLPR_secuencia")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private InvrCablistaprecio cablistaprecio;
    @Column(name = "CXCRUT_codigo")
    private String cxcrutCodigo;
    @Column(name = "NOMEMP_codigo")
    private Integer nomempCodigo;
    @Column(name = "GENPGE_codigo")
    private String genpgeCodigo;
    @Column(name = "BANENT_codigo")
    private String banentCodigo;
    @Column(name = "BANCTA_codigo")
    private String banctaCodigo;
    @JoinColumn(name = "gental_secuencia_dev", referencedColumnName = "gental_secuencia")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private GenpTalonarios gentalSecuenciaDev;
    //@Formula(value = "(SELECT b.genbod_nombre FROM CBPGENR_BODEGA b WHERE b.genbod_secuencia=GENBOD_codigo)")
    @Transient
    private String bodega;
    //@Formula(value = "(SELECT o.genofi_nombre FROM CBPGENR_OFICINA o WHERE o.genofi_secuencia=GENOFI_codigo)")
    @Transient
    private String oficina;
    //@Formula(value = "(SELECT p.GENPGE_nombre FROM GENP_PARAMGENERALES p WHERE p.GENPGE_aplicacion='BAN' AND p.GENPGE_tabla='009' AND p.GENPGE_codigo=GENPGE_codigo)")
    @Transient
    private String tipoEntidad;
    //@Formula(value = "(SELECT e.BANENT_nombre FROM BANM_ENTIDAD e WHERE e.BANENT_tipo=GENPGE_codigo AND e.BANENT_codigo=BANENT_codigo)")
    @Transient
    private String entidad;
    //@Formula(value = "(SELECT c.BANCTA_nombre FROM BANM_CUENTACAJA c WHERE c.BANENT_tipo=GENPGE_codigo AND c.BANENT_codigo=BANENT_codigo AND c.BANCTA_codigo=BANCTA_codigo)")
    @Transient
    private String cuenta;
    //@Formula(value = "(SELECT t.segtur_numero FROM CBPSEGM_USUARIO u INNER JOIN CBPSEGM_TURNO t ON (u.segusu_codigo=t.segusu_codigo AND t.segtur_estado=1) WHERE u.segusu_login=segusu_codigo)")
    @Transient
    private Long turno;
    //@Formula(value = "(SELECT s.SEGUSU_codigo+'-'+s.FACSUP_nombre FROM FACR_SUPERVISOR s WHERE s.FACSUP_codigo = (SELECT b.FACSUP_codigo FROM CBPGENR_BODEGA b  WHERE b.genbod_secuencia=GENBOD_codigo))")
    @Transient
    private String supervisor;

    public GenmCaja() {
    }

    public GenmCaja(Long facscaIdsececaja) {
        this.facscaIdsececaja = facscaIdsececaja;
    }

    public GenmCaja(Long facscaIdsececaja, String gENCIAcodigo, String gENOFIcodigo) {
        this.facscaIdsececaja = facscaIdsececaja;
        this.gENCIAcodigo = gENCIAcodigo;
        this.gENOFIcodigo = gENOFIcodigo;
    }

    public Long getFacscaIdsececaja() {
        return facscaIdsececaja;
    }

    public void setFacscaIdsececaja(Long facscaIdsececaja) {
        this.facscaIdsececaja = facscaIdsececaja;
    }

    public String getGENCIAcodigo() {
        return gENCIAcodigo;
    }

    public void setGENCIAcodigo(String gENCIAcodigo) {
        this.gENCIAcodigo = gENCIAcodigo;
    }

    public String getGENOFIcodigo() {
        return gENOFIcodigo;
    }

    public void setGENOFIcodigo(String gENOFIcodigo) {
        this.gENOFIcodigo = gENOFIcodigo;
    }

    public String getGENCAJcodigo() {
        return gENCAJcodigo;
    }

    public void setGENCAJcodigo(String gENCAJcodigo) {
        this.gENCAJcodigo = gENCAJcodigo;
    }

    public String getGENCAJdescripcio() {
        return gENCAJdescripcio;
    }

    public void setGENCAJdescripcio(String gENCAJdescripcio) {
        this.gENCAJdescripcio = gENCAJdescripcio;
    }

    public String getSEGUSUcodigo() {
        return sEGUSUcodigo;
    }

    public void setSEGUSUcodigo(String sEGUSUcodigo) {
        this.sEGUSUcodigo = sEGUSUcodigo;
    }

    public String getFACVDRcodigo() {
        return fACVDRcodigo;
    }

    public void setFACVDRcodigo(String fACVDRcodigo) {
        this.fACVDRcodigo = fACVDRcodigo;
    }

    public String getGENCAJestado() {
        return gENCAJestado;
    }

    public void setGENCAJestado(String gENCAJestado) {
        this.gENCAJestado = gENCAJestado;
    }

    public String getGENCAJestapertur() {
        return gENCAJestapertur;
    }

    public void setGENCAJestapertur(String gENCAJestapertur) {
        this.gENCAJestapertur = gENCAJestapertur;
    }

    public String getGENBODcodigo() {
        return gENBODcodigo;
    }

    public void setGENBODcodigo(String gENBODcodigo) {
        this.gENBODcodigo = gENBODcodigo;
    }

    public Integer getGENCAJsecuenvta() {
        return gENCAJsecuenvta;
    }

    public void setGENCAJsecuenvta(Integer gENCAJsecuenvta) {
        this.gENCAJsecuenvta = gENCAJsecuenvta;
    }

    public Integer getGENCAJsecvalecaj() {
        return gENCAJsecvalecaj;
    }

    public void setGENCAJsecvalecaj(Integer gENCAJsecvalecaj) {
        this.gENCAJsecvalecaj = gENCAJsecvalecaj;
    }

    public GenpTalonarios getGentalSecuencia() {
        return gentalSecuencia;
    }

    public void setGentalSecuencia(GenpTalonarios gentalSecuencia) {
        this.gentalSecuencia = gentalSecuencia;
    }

    public InvrCablistaprecio getCablistaprecio() {
        return cablistaprecio;
    }

    public void setCablistaprecio(InvrCablistaprecio cablistaprecio) {
        this.cablistaprecio = cablistaprecio;
    }

    public String getCxcrutCodigo() {
        return cxcrutCodigo;
    }

    public void setCxcrutCodigo(String cxcrutCodigo) {
        this.cxcrutCodigo = cxcrutCodigo;
    }

    public Integer getNomempCodigo() {
        return nomempCodigo;
    }

    public void setNomempCodigo(Integer nomempCodigo) {
        this.nomempCodigo = nomempCodigo;
    }

    public String getGenpgeCodigo() {
        return genpgeCodigo;
    }

    public void setGenpgeCodigo(String genpgeCodigo) {
        this.genpgeCodigo = genpgeCodigo;
    }

    public String getBanentCodigo() {
        return banentCodigo;
    }

    public void setBanentCodigo(String banentCodigo) {
        this.banentCodigo = banentCodigo;
    }

    public String getBanctaCodigo() {
        return banctaCodigo;
    }

    public void setBanctaCodigo(String banctaCodigo) {
        this.banctaCodigo = banctaCodigo;
    }

    public GenpTalonarios getGentalSecuenciaDev() {
        return gentalSecuenciaDev;
    }

    public void setGentalSecuenciaDev(GenpTalonarios gentalSecuenciaDev) {
        this.gentalSecuenciaDev = gentalSecuenciaDev;
    }

    public String getBodega() {
        return bodega;
    }

    public void setBodega(String bodega) {
        this.bodega = bodega;
    }

    public String getOficina() {
        return oficina;
    }

    public void setOficina(String oficina) {
        this.oficina = oficina;
    }

    public String getTipoEntidad() {
        return tipoEntidad;
    }

    public void setTipoEntidad(String tipoEntidad) {
        this.tipoEntidad = tipoEntidad;
    }

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public Long getTurno() {
        return turno;
    }

    public void setTurno(Long turno) {
        this.turno = turno;
    }

    public String getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(String supervisor) {
        this.supervisor = supervisor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (facscaIdsececaja != null ? facscaIdsececaja.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GenmCaja)) {
            return false;
        }
        GenmCaja other = (GenmCaja) object;
        if ((this.facscaIdsececaja == null && other.facscaIdsececaja != null) || (this.facscaIdsececaja != null && !this.facscaIdsececaja.equals(other.facscaIdsececaja))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.GenmCaja[ facscaIdsececaja=" + facscaIdsececaja + " ]";
    }
    
}
