/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb2
 */
@Embeddable
public class FacrMotvdevoluPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENCIA_codigo", nullable = false, length = 4)
    private String GENCIAcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "GENMOD_codigo", nullable = false, length = 3)
    private String GENMODcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "FACMDE_codigo", nullable = false, length = 3)
    private String FACMDEcodigo;

    public FacrMotvdevoluPK() {
    }

    public FacrMotvdevoluPK(String gENCIAcodigo, String gENMODcodigo, String fACMDEcodigo) {
        this.GENCIAcodigo = gENCIAcodigo;
        this.GENMODcodigo = gENMODcodigo;
        this.FACMDEcodigo = fACMDEcodigo;
    }

    public String getGENCIAcodigo() {
        return GENCIAcodigo;
    }

    public void setGENCIAcodigo(String gENCIAcodigo) {
        this.GENCIAcodigo = gENCIAcodigo;
    }

    public String getGENMODcodigo() {
        return GENMODcodigo;
    }

    public void setGENMODcodigo(String gENMODcodigo) {
        this.GENMODcodigo = gENMODcodigo;
    }

    public String getFACMDEcodigo() {
        return FACMDEcodigo;
    }

    public void setFACMDEcodigo(String fACMDEcodigo) {
        this.FACMDEcodigo = fACMDEcodigo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (GENCIAcodigo != null ? GENCIAcodigo.hashCode() : 0);
        hash += (GENMODcodigo != null ? GENMODcodigo.hashCode() : 0);
        hash += (FACMDEcodigo != null ? FACMDEcodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FacrMotvdevoluPK)) {
            return false;
        }
        FacrMotvdevoluPK other = (FacrMotvdevoluPK) object;
        if ((this.GENCIAcodigo == null && other.GENCIAcodigo != null) || (this.GENCIAcodigo != null && !this.GENCIAcodigo.equals(other.GENCIAcodigo))) {
            return false;
        }
        if ((this.GENMODcodigo == null && other.GENMODcodigo != null) || (this.GENMODcodigo != null && !this.GENMODcodigo.equals(other.GENMODcodigo))) {
            return false;
        }
        if ((this.FACMDEcodigo == null && other.FACMDEcodigo != null) || (this.FACMDEcodigo != null && !this.FACMDEcodigo.equals(other.FACMDEcodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.FacrMotvdevoluPK[ gENCIAcodigo=" + GENCIAcodigo + ", gENMODcodigo=" + GENMODcodigo + ", fACMDEcodigo=" + FACMDEcodigo + " ]";
    }
    
}
