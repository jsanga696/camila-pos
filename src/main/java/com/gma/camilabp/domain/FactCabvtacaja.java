/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
//import org.hibernate.annotations.Formula;

/**
 *
 * @author userdb6
 */
@Entity
@Table(name = "FACT_CABVTACAJA")
@NamedQueries({
    @NamedQuery(name = "FactCabvtacaja.findAll", query = "SELECT f FROM FactCabvtacaja f")})
public class FactCabvtacaja implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "FACVTC_secuenvta")
    private Long facvtcSecuenvta;
    @Column(name = "FACSCA_idsececaja")
    private Integer fACSCAidsececaja;
    @Size(max = 4)
    @Column(name = "GENCIA_codigo")
    private String gENCIAcodigo;
    @Size(max = 4)
    @Column(name = "GENOFI_codigo")
    private String gENOFIcodigo;
    @Size(max = 4)
    @Column(name = "GENCAJ_codigo")
    private String gENCAJcodigo;
    @Size(max = 4)
    @Column(name = "GENTDO_codigo")
    private String gENTDOcodigo;
    @Column(name = "FACSCA_fecapertur")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fACSCAfecapertur;
    @Size(max = 6)
    @Column(name = "CXCCLI_codigo")
    private String cXCCLIcodigo;
    @Size(max = 20)
    @Column(name = "FACVTC_clierucci")
    private String fACVTCclierucci;
    @Size(max = 100)
    @Column(name = "FACVTC_clienombre")
    private String fACVTCclienombre;
    @Size(max = 100)
    @Column(name = "FACVTC_cliedirec")
    private String fACVTCcliedirec;
    @Size(max = 30)
    @Column(name = "FACVTC_telefonos")
    private String fACVTCtelefonos;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "FACVTC_netomov")
    private BigDecimal fACVTCnetomov;
    @Column(name = "FACVTC_desctomov")
    private BigDecimal fACVTCdesctomov;
    @Column(name = "FACVTC_impuesmov")
    private BigDecimal fACVTCimpuesmov;
    @Column(name = "FACVTC_subtotmov")
    private BigDecimal fACVTCsubtotmov;
    @Column(name = "FACVTC_monpagomov")
    private BigDecimal fACVTCmonpagomov;
    @Column(name = "FACVTC_valcotiza")
    private BigDecimal fACVTCvalcotiza;
    @Size(max = 20)
    @Column(name = "FACVTC_mensaje")
    private String fACVTCmensaje;
    @Size(max = 1)
    @Column(name = "FACVTC_estado")
    private String fACVTCestado;
    @Column(name = "FACCVT_impueiva")
    private BigDecimal fACCVTimpueiva;
    @Column(name = "FACCVT_impueice")
    private BigDecimal fACCVTimpueice;
    @Column(name = "FACCVT_impueser")
    private BigDecimal fACCVTimpueser;
    //@Formula(value = "(SELECT v.FACCVT_numdocumen FROM FACT_CABVENTA v WHERE v.FACCVT_numsecuenc=FACVTC_secuenvta)")
    @Transient
    private Long documento;
    //@Formula(value = "(SELECT v.GENTAL_secuencia FROM FACT_CABVENTA v WHERE v.FACCVT_numsecuenc=FACVTC_secuenvta)")
    @Transient
    private Long GENTALSecuencia;
    //t.GENTAL_codestable+'-'+t.GENTAL_codptoemis+'-'+RIGHT('000000000' + CAST(fv.FACCVT_numdocumen AS VARCHAR(9)), 9)
    //@Formula(value = "(SELECT t.GENTAL_codestable+'-'+t.GENTAL_codptoemis+'-'+RIGHT('000000000' + CAST(v.FACCVT_numdocumen AS VARCHAR(9)), 9) FROM FACT_CABVENTA v INNER JOIN GENP_TALONARIOS t ON (v.GENTAL_secuencia=t.GENTAL_secuencia) WHERE v.FACCVT_numsecuenc=FACVTC_secuenvta)")
    @Transient
    private String documentoFact;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "factCabventaPK.fACCVTnumsecuenc")
    private List<FactCabventa> factCabventa;
    
    public FactCabvtacaja() {
    }

    public FactCabvtacaja(Long facvtcSecuenvta) {
        this.facvtcSecuenvta = facvtcSecuenvta;
    }

    public Integer getFACSCAidsececaja() {
        return fACSCAidsececaja;
    }

    public void setFACSCAidsececaja(Integer fACSCAidsececaja) {
        this.fACSCAidsececaja = fACSCAidsececaja;
    }

    public String getGENCIAcodigo() {
        return gENCIAcodigo;
    }

    public void setGENCIAcodigo(String gENCIAcodigo) {
        this.gENCIAcodigo = gENCIAcodigo;
    }

    public String getGENOFIcodigo() {
        return gENOFIcodigo;
    }

    public void setGENOFIcodigo(String gENOFIcodigo) {
        this.gENOFIcodigo = gENOFIcodigo;
    }

    public String getGENCAJcodigo() {
        return gENCAJcodigo;
    }

    public void setGENCAJcodigo(String gENCAJcodigo) {
        this.gENCAJcodigo = gENCAJcodigo;
    }

    public String getGENTDOcodigo() {
        return gENTDOcodigo;
    }

    public void setGENTDOcodigo(String gENTDOcodigo) {
        this.gENTDOcodigo = gENTDOcodigo;
    }

    public Long getFacvtcSecuenvta() {
        return facvtcSecuenvta;
    }

    public void setFacvtcSecuenvta(Long facvtcSecuenvta) {
        this.facvtcSecuenvta = facvtcSecuenvta;
    }

    public Date getFACSCAfecapertur() {
        return fACSCAfecapertur;
    }

    public void setFACSCAfecapertur(Date fACSCAfecapertur) {
        this.fACSCAfecapertur = fACSCAfecapertur;
    }

    public String getCXCCLIcodigo() {
        return cXCCLIcodigo;
    }

    public void setCXCCLIcodigo(String cXCCLIcodigo) {
        this.cXCCLIcodigo = cXCCLIcodigo;
    }

    public String getFACVTCclierucci() {
        return fACVTCclierucci;
    }

    public void setFACVTCclierucci(String fACVTCclierucci) {
        this.fACVTCclierucci = fACVTCclierucci;
    }

    public String getFACVTCclienombre() {
        return fACVTCclienombre;
    }

    public void setFACVTCclienombre(String fACVTCclienombre) {
        this.fACVTCclienombre = fACVTCclienombre;
    }

    public String getFACVTCcliedirec() {
        return fACVTCcliedirec;
    }

    public void setFACVTCcliedirec(String fACVTCcliedirec) {
        this.fACVTCcliedirec = fACVTCcliedirec;
    }

    public String getFACVTCtelefonos() {
        return fACVTCtelefonos;
    }

    public void setFACVTCtelefonos(String fACVTCtelefonos) {
        this.fACVTCtelefonos = fACVTCtelefonos;
    }

    public BigDecimal getFACVTCnetomov() {
        return fACVTCnetomov;
    }

    public void setFACVTCnetomov(BigDecimal fACVTCnetomov) {
        this.fACVTCnetomov = fACVTCnetomov;
    }

    public BigDecimal getFACVTCdesctomov() {
        return fACVTCdesctomov;
    }

    public void setFACVTCdesctomov(BigDecimal fACVTCdesctomov) {
        this.fACVTCdesctomov = fACVTCdesctomov;
    }

    public BigDecimal getFACVTCimpuesmov() {
        return fACVTCimpuesmov;
    }

    public void setFACVTCimpuesmov(BigDecimal fACVTCimpuesmov) {
        this.fACVTCimpuesmov = fACVTCimpuesmov;
    }

    public BigDecimal getFACVTCsubtotmov() {
        return fACVTCsubtotmov;
    }

    public void setFACVTCsubtotmov(BigDecimal fACVTCsubtotmov) {
        this.fACVTCsubtotmov = fACVTCsubtotmov;
    }

    public BigDecimal getFACVTCmonpagomov() {
        return fACVTCmonpagomov;
    }

    public void setFACVTCmonpagomov(BigDecimal fACVTCmonpagomov) {
        this.fACVTCmonpagomov = fACVTCmonpagomov;
    }

    public BigDecimal getFACVTCvalcotiza() {
        return fACVTCvalcotiza;
    }

    public void setFACVTCvalcotiza(BigDecimal fACVTCvalcotiza) {
        this.fACVTCvalcotiza = fACVTCvalcotiza;
    }

    public String getFACVTCmensaje() {
        return fACVTCmensaje;
    }

    public void setFACVTCmensaje(String fACVTCmensaje) {
        this.fACVTCmensaje = fACVTCmensaje;
    }

    public String getFACVTCestado() {
        return fACVTCestado;
    }

    public void setFACVTCestado(String fACVTCestado) {
        this.fACVTCestado = fACVTCestado;
    }

    public BigDecimal getFACCVTimpueiva() {
        return fACCVTimpueiva;
    }

    public void setFACCVTimpueiva(BigDecimal fACCVTimpueiva) {
        this.fACCVTimpueiva = fACCVTimpueiva;
    }

    public BigDecimal getFACCVTimpueice() {
        return fACCVTimpueice;
    }

    public void setFACCVTimpueice(BigDecimal fACCVTimpueice) {
        this.fACCVTimpueice = fACCVTimpueice;
    }

    public BigDecimal getFACCVTimpueser() {
        return fACCVTimpueser;
    }

    public void setFACCVTimpueser(BigDecimal fACCVTimpueser) {
        this.fACCVTimpueser = fACCVTimpueser;
    }

    public Long getDocumento() {
        return documento;
    }

    public void setDocumento(Long documento) {
        this.documento = documento;
    }

    public String getDocumentoFact() {
        return documentoFact;
    }

    public void setDocumentoFact(String documentoFact) {
        this.documentoFact = documentoFact;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (facvtcSecuenvta != null ? facvtcSecuenvta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FactCabvtacaja)) {
            return false;
        }
        FactCabvtacaja other = (FactCabvtacaja) object;
        if ((this.facvtcSecuenvta == null && other.facvtcSecuenvta != null) || (this.facvtcSecuenvta != null && !this.facvtcSecuenvta.equals(other.facvtcSecuenvta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.FactCabvtacaja[ facvtcSecuenvta=" + facvtcSecuenvta + " ]";
    }

    public Long getGENTALSecuencia() {
        return GENTALSecuencia;
    }

    public void setGENTALSecuencia(Long GENTALSecuencia) {
        this.GENTALSecuencia = GENTALSecuencia;
    }

    public List<FactCabventa> getFactCabventa() {
        return factCabventa;
    }

    public void setFactCabventa(List<FactCabventa> factCabventa) {
        this.factCabventa = factCabventa;
    }
    
}
