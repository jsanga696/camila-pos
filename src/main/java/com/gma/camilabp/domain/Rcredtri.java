/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb5
 */
@Entity
@Table(name = "RCREDTRI")
@NamedQueries({
    @NamedQuery(name = "Rcredtri.findAll", query = "SELECT r FROM Rcredtri r")})
public class Rcredtri implements Serializable {

    private static final long serialVersionUID = 1L;
    @Size(max = 4)
    @Column(name = "CODIGO")
    private String codigo;
    @Size(max = 150)
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "CALIVA")
    private Character caliva;
    @Size(max = 20)
    @Column(name = "CUENTACONTABLE")
    private String cuentacontable;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;

    public Rcredtri() {
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public Character getCaliva() {
        return caliva;
    }

    public void setCaliva(Character caliva) {
        this.caliva = caliva;
    }

    public String getCuentacontable() {
        return cuentacontable;
    }

    public void setCuentacontable(String cuentacontable) {
        this.cuentacontable = cuentacontable;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Rcredtri)) {
            return false;
        }
        Rcredtri other = (Rcredtri) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.Rcredtri[ id=" + id + " ]";
    }
    
}
