/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb6
 */
@Embeddable
public class CxcrRutaPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENCIA_codigo")
    private String gENCIAcodigo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CXCRUT_codigo")
    private String cXCRUTcodigo;

    public CxcrRutaPK() {
    }

    public CxcrRutaPK(String gENCIAcodigo, String cXCRUTcodigo) {
        this.gENCIAcodigo = gENCIAcodigo;
        this.cXCRUTcodigo = cXCRUTcodigo;
    }

    public String getGENCIAcodigo() {
        return gENCIAcodigo;
    }

    public void setGENCIAcodigo(String gENCIAcodigo) {
        this.gENCIAcodigo = gENCIAcodigo;
    }

    public String getCXCRUTcodigo() {
        return cXCRUTcodigo;
    }

    public void setCXCRUTcodigo(String cXCRUTcodigo) {
        this.cXCRUTcodigo = cXCRUTcodigo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gENCIAcodigo != null ? gENCIAcodigo.hashCode() : 0);
        hash += (cXCRUTcodigo != null ? cXCRUTcodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CxcrRutaPK)) {
            return false;
        }
        CxcrRutaPK other = (CxcrRutaPK) object;
        if ((this.gENCIAcodigo == null && other.gENCIAcodigo != null) || (this.gENCIAcodigo != null && !this.gENCIAcodigo.equals(other.gENCIAcodigo))) {
            return false;
        }
        if ((this.cXCRUTcodigo == null && other.cXCRUTcodigo != null) || (this.cXCRUTcodigo != null && !this.cXCRUTcodigo.equals(other.cXCRUTcodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CxcrRutaPK[ gENCIAcodigo=" + gENCIAcodigo + ", cXCRUTcodigo=" + cXCRUTcodigo + " ]";
    }
    
}
