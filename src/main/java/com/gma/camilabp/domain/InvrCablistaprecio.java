/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb6
 */
@Entity
@Table(name = "INVR_CABLISTAPRECIO")
@NamedQueries({
    @NamedQuery(name = "InvrCablistaprecio.findAll", query = "SELECT i FROM InvrCablistaprecio i")})
public class InvrCablistaprecio implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "INVLPR_secuencia")
    private Long iNVLPRsecuencia;
    @Size(max = 4)
    @Column(name = "GENCIA_codigo")
    private String gENCIAcodigo;
    @Size(max = 4)
    @Column(name = "GENOFI_codigo")
    private String genofiCodigo;
    @Size(max = 10)
    @Column(name = "INVLPR_codigo")
    private String iNVLPRcodigo;
    @Size(max = 40)
    @Column(name = "INVLPR_nombre")
    private String iNVLPRnombre;
    @Column(name = "INVLPR_fechadesde")
    @Temporal(TemporalType.TIMESTAMP)
    private Date iNVLPRfechadesde;
    @Column(name = "INVLPR_fechahasta")
    @Temporal(TemporalType.TIMESTAMP)
    private Date iNVLPRfechahasta;
    @Column(name = "INVLPR_fechacrea")
    @Temporal(TemporalType.TIMESTAMP)
    private Date iNVLPRfechacrea;
    @Column(name = "INVLPR_fechamodi")
    @Temporal(TemporalType.TIMESTAMP)
    private Date iNVLPRfechamodi;
    @Size(max = 4)
    @Column(name = "GENUSU_codcreacion")
    private String gENUSUcodcreacion;
    @Size(max = 4)
    @Column(name = "GENUSU_codmodifica")
    private String gENUSUcodmodifica;
    @Size(max = 4)
    @Column(name = "INVLPR_tipoLista")
    private String iNVLPRtipoLista;
    @Size(max = 2)
    @Column(name = "INVLPR_tipolistaprecio")
    private String iNVLPRtipolistaprecio;
    @Size(max = 2)
    @Column(name = "INVLPR_atributo")
    private String iNVLPRatributo;
    @Size(max = 1)
    @Column(name = "INVLPR_aplicdescto")
    private String iNVLPRaplicdescto;

    public InvrCablistaprecio() {
    }

    public InvrCablistaprecio(Long iNVLPRsecuencia) {
        this.iNVLPRsecuencia = iNVLPRsecuencia;
    }

    public Long getINVLPRsecuencia() {
        return iNVLPRsecuencia;
    }

    public void setINVLPRsecuencia(Long iNVLPRsecuencia) {
        this.iNVLPRsecuencia = iNVLPRsecuencia;
    }

    public String getGENCIAcodigo() {
        return gENCIAcodigo;
    }

    public void setGENCIAcodigo(String gENCIAcodigo) {
        this.gENCIAcodigo = gENCIAcodigo;
    }

    public String getGenofiCodigo() {
        return genofiCodigo;
    }

    public void setGenofiCodigo(String genofiCodigo) {
        this.genofiCodigo = genofiCodigo;
    }    

    public String getINVLPRcodigo() {
        return iNVLPRcodigo;
    }

    public void setINVLPRcodigo(String iNVLPRcodigo) {
        this.iNVLPRcodigo = iNVLPRcodigo;
    }

    public String getINVLPRnombre() {
        return iNVLPRnombre;
    }

    public void setINVLPRnombre(String iNVLPRnombre) {
        this.iNVLPRnombre = iNVLPRnombre;
    }

    public Date getINVLPRfechadesde() {
        return iNVLPRfechadesde;
    }

    public void setINVLPRfechadesde(Date iNVLPRfechadesde) {
        this.iNVLPRfechadesde = iNVLPRfechadesde;
    }

    public Date getINVLPRfechahasta() {
        return iNVLPRfechahasta;
    }

    public void setINVLPRfechahasta(Date iNVLPRfechahasta) {
        this.iNVLPRfechahasta = iNVLPRfechahasta;
    }

    public Date getINVLPRfechacrea() {
        return iNVLPRfechacrea;
    }

    public void setINVLPRfechacrea(Date iNVLPRfechacrea) {
        this.iNVLPRfechacrea = iNVLPRfechacrea;
    }

    public Date getINVLPRfechamodi() {
        return iNVLPRfechamodi;
    }

    public void setINVLPRfechamodi(Date iNVLPRfechamodi) {
        this.iNVLPRfechamodi = iNVLPRfechamodi;
    }

    public String getGENUSUcodcreacion() {
        return gENUSUcodcreacion;
    }

    public void setGENUSUcodcreacion(String gENUSUcodcreacion) {
        this.gENUSUcodcreacion = gENUSUcodcreacion;
    }

    public String getGENUSUcodmodifica() {
        return gENUSUcodmodifica;
    }

    public void setGENUSUcodmodifica(String gENUSUcodmodifica) {
        this.gENUSUcodmodifica = gENUSUcodmodifica;
    }

    public String getINVLPRtipoLista() {
        return iNVLPRtipoLista;
    }

    public void setINVLPRtipoLista(String iNVLPRtipoLista) {
        this.iNVLPRtipoLista = iNVLPRtipoLista;
    }

    public String getINVLPRtipolistaprecio() {
        return iNVLPRtipolistaprecio;
    }

    public void setINVLPRtipolistaprecio(String iNVLPRtipolistaprecio) {
        this.iNVLPRtipolistaprecio = iNVLPRtipolistaprecio;
    }

    public String getINVLPRatributo() {
        return iNVLPRatributo;
    }

    public void setINVLPRatributo(String iNVLPRatributo) {
        this.iNVLPRatributo = iNVLPRatributo;
    }

    public String getINVLPRaplicdescto() {
        return iNVLPRaplicdescto;
    }

    public void setINVLPRaplicdescto(String iNVLPRaplicdescto) {
        this.iNVLPRaplicdescto = iNVLPRaplicdescto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iNVLPRsecuencia != null ? iNVLPRsecuencia.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InvrCablistaprecio)) {
            return false;
        }
        InvrCablistaprecio other = (InvrCablistaprecio) object;
        if ((this.iNVLPRsecuencia == null && other.iNVLPRsecuencia != null) || (this.iNVLPRsecuencia != null && !this.iNVLPRsecuencia.equals(other.iNVLPRsecuencia))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.InvrCablistaprecio[ iNVLPRsecuencia=" + iNVLPRsecuencia + " ]";
    }
    
}
