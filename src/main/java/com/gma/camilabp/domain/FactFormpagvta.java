/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb6
 */
@Entity
@Table(name = "FACT_FORMPAGVTA")
@NamedQueries({
    @NamedQuery(name = "FactFormpagvta.findAll", query = "SELECT f FROM FactFormpagvta f")})
public class FactFormpagvta implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected FactFormpagvtaPK factFormpagvtaPK;
    @Size(max = 3)
    @Column(name = "FACFPV_codbanco")
    private String fACFPVcodbanco;
    @Column(name = "FACFPV_tipobcotar")
    private Character fACFPVtipobcotar;
    @Size(max = 3)
    @Column(name = "CXCFPG_codigo")
    private String cXCFPGcodigo;
    @Size(max = 18)
    @Column(name = "FACFPV_numctatar")
    private String fACFPVnumctatar;
    @Size(max = 18)
    @Column(name = "FACFPV_numchqbou")
    private String fACFPVnumchqbou;
    @Column(name = "FACFPV_fecvencimi")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fACFPVfecvencimi;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "FACFPV_valor")
    private BigDecimal fACFPVvalor;

    public FactFormpagvta() {
    }

    public FactFormpagvta(FactFormpagvtaPK factFormpagvtaPK) {
        this.factFormpagvtaPK = factFormpagvtaPK;
    }

    public FactFormpagvta(String gENCIAcodigo, String gENOFIcodigo, String gENCAJcodigo, BigDecimal fACVTCsecuenvta, Date fACSCAfecapertur, BigDecimal fACFPVsecuencia) {
        this.factFormpagvtaPK = new FactFormpagvtaPK(gENCIAcodigo, gENOFIcodigo, gENCAJcodigo, fACVTCsecuenvta, fACSCAfecapertur, fACFPVsecuencia);
    }

    public FactFormpagvtaPK getFactFormpagvtaPK() {
        return factFormpagvtaPK;
    }

    public void setFactFormpagvtaPK(FactFormpagvtaPK factFormpagvtaPK) {
        this.factFormpagvtaPK = factFormpagvtaPK;
    }

    public String getFACFPVcodbanco() {
        return fACFPVcodbanco;
    }

    public void setFACFPVcodbanco(String fACFPVcodbanco) {
        this.fACFPVcodbanco = fACFPVcodbanco;
    }

    public Character getFACFPVtipobcotar() {
        return fACFPVtipobcotar;
    }

    public void setFACFPVtipobcotar(Character fACFPVtipobcotar) {
        this.fACFPVtipobcotar = fACFPVtipobcotar;
    }

    public String getCXCFPGcodigo() {
        return cXCFPGcodigo;
    }

    public void setCXCFPGcodigo(String cXCFPGcodigo) {
        this.cXCFPGcodigo = cXCFPGcodigo;
    }

    public String getFACFPVnumctatar() {
        return fACFPVnumctatar;
    }

    public void setFACFPVnumctatar(String fACFPVnumctatar) {
        this.fACFPVnumctatar = fACFPVnumctatar;
    }

    public String getFACFPVnumchqbou() {
        return fACFPVnumchqbou;
    }

    public void setFACFPVnumchqbou(String fACFPVnumchqbou) {
        this.fACFPVnumchqbou = fACFPVnumchqbou;
    }

    public Date getFACFPVfecvencimi() {
        return fACFPVfecvencimi;
    }

    public void setFACFPVfecvencimi(Date fACFPVfecvencimi) {
        this.fACFPVfecvencimi = fACFPVfecvencimi;
    }

    public BigDecimal getFACFPVvalor() {
        return fACFPVvalor;
    }

    public void setFACFPVvalor(BigDecimal fACFPVvalor) {
        this.fACFPVvalor = fACFPVvalor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (factFormpagvtaPK != null ? factFormpagvtaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FactFormpagvta)) {
            return false;
        }
        FactFormpagvta other = (FactFormpagvta) object;
        if ((this.factFormpagvtaPK == null && other.factFormpagvtaPK != null) || (this.factFormpagvtaPK != null && !this.factFormpagvtaPK.equals(other.factFormpagvtaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.FactFormpagvta[ factFormpagvtaPK=" + factFormpagvtaPK + " ]";
    }
    
}
