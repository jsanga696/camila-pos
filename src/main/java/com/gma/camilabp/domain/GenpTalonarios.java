/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb6
 */
@Entity
@Table(name = "GENP_TALONARIOS")
@NamedQueries({
    @NamedQuery(name = "GenpTalonarios.findAll", query = "SELECT g FROM GenpTalonarios g")})
public class GenpTalonarios implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENCIA_codigo")
    private String genciaCodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENOFI_codigo")
    private String genofiCodigo;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "GENTAL_secuencia")
    private Long gENTALsecuencia;
    @Size(max = 5)
    @Column(name = "GENTAL_tipotalon")
    private String gENTALtipotalon;
    @Size(max = 100)
    @Column(name = "GENTAL_descripcion")
    private String gENTALdescripcion;
    @Size(max = 3)
    @Column(name = "GENTAL_codestable")
    private String gENTALcodestable;
    @Size(max = 3)
    @Column(name = "GENTAL_codptoemis")
    private String gENTALcodptoemis;
    @Size(max = 20)
    @Column(name = "GENTAL_autorizacion")
    private String gENTALautorizacion;
    @Column(name = "GENTAL_fechavalidez")
    @Temporal(TemporalType.TIMESTAMP)
    private Date gENTALfechavalidez;
    @Size(max = 1)
    @Column(name = "GENTAL_estado")
    private String gENTALestado;
    @Column(name = "GENTAL_numdijitos")
    private Integer gENTALnumdijitos;
    @Size(max = 10)
    @Column(name = "CXCRUT_codigo")
    private String cXCRUTcodigo;
    @Size(max = 30)
    @Column(name = "GENTAL_numedesde")
    private String gENTALnumedesde;
    @Size(max = 30)
    @Column(name = "GENTAL_numehasta")
    private String gENTALnumehasta;
    @Size(max = 3)
    @Column(name = "GENUSU_codigo")
    private String gENUSUcodigo;
    @Column(name = "GENTAL_fechaing")
    @Temporal(TemporalType.TIMESTAMP)
    private Date gENTALfechaing;
    @Size(max = 50)
    @Column(name = "GENTAL_imprimefact")
    private String gENTALimprimefact;
    @Column(name = "GENTAL_lineadocum")
    private Integer gENTALlineadocum;
    @Size(max = 1)
    @Column(name = "GENTAL_docelectronico")
    private String gENTALdocelectronico;
    @Size(max = 1)
    @Column(name = "GENTAL_esquemaAutov")
    private String gENTALesquemaAutov;
    @Size(max = 100)
    @Column(name = "GENTAL_direccionSucursal")
    private String gENTALdireccionSucursal;

    public GenpTalonarios() {
    }

    public GenpTalonarios(Long gENTALsecuencia) {
        this.gENTALsecuencia = gENTALsecuencia;
    }

    public GenpTalonarios(Long gENTALsecuencia, String genciaCodigo, String genofiCodigo) {
        this.gENTALsecuencia = gENTALsecuencia;
        this.genciaCodigo = genciaCodigo;
        this.genofiCodigo = genofiCodigo;
    }

    public String getGenciaCodigo() {
        return genciaCodigo;
    }

    public void setGenciaCodigo(String genciaCodigo) {
        this.genciaCodigo = genciaCodigo;
    }

    public String getGenofiCodigo() {
        return genofiCodigo;
    }

    public void setGenofiCodigo(String genofiCodigo) {
        this.genofiCodigo = genofiCodigo;
    }

    public Long getGENTALsecuencia() {
        return gENTALsecuencia;
    }

    public void setGENTALsecuencia(Long gENTALsecuencia) {
        this.gENTALsecuencia = gENTALsecuencia;
    }

    public String getGENTALtipotalon() {
        return gENTALtipotalon;
    }

    public void setGENTALtipotalon(String gENTALtipotalon) {
        this.gENTALtipotalon = gENTALtipotalon;
    }

    public String getGENTALdescripcion() {
        return gENTALdescripcion;
    }

    public void setGENTALdescripcion(String gENTALdescripcion) {
        this.gENTALdescripcion = gENTALdescripcion;
    }

    public String getGENTALcodestable() {
        return gENTALcodestable;
    }

    public void setGENTALcodestable(String gENTALcodestable) {
        this.gENTALcodestable = gENTALcodestable;
    }

    public String getGENTALcodptoemis() {
        return gENTALcodptoemis;
    }

    public void setGENTALcodptoemis(String gENTALcodptoemis) {
        this.gENTALcodptoemis = gENTALcodptoemis;
    }

    public String getGENTALautorizacion() {
        return gENTALautorizacion;
    }

    public void setGENTALautorizacion(String gENTALautorizacion) {
        this.gENTALautorizacion = gENTALautorizacion;
    }

    public Date getGENTALfechavalidez() {
        return gENTALfechavalidez;
    }

    public void setGENTALfechavalidez(Date gENTALfechavalidez) {
        this.gENTALfechavalidez = gENTALfechavalidez;
    }

    public String getGENTALestado() {
        return gENTALestado;
    }

    public void setGENTALestado(String gENTALestado) {
        this.gENTALestado = gENTALestado;
    }

    public Integer getGENTALnumdijitos() {
        return gENTALnumdijitos;
    }

    public void setGENTALnumdijitos(Integer gENTALnumdijitos) {
        this.gENTALnumdijitos = gENTALnumdijitos;
    }

    public String getCXCRUTcodigo() {
        return cXCRUTcodigo;
    }

    public void setCXCRUTcodigo(String cXCRUTcodigo) {
        this.cXCRUTcodigo = cXCRUTcodigo;
    }

    public String getGENTALnumedesde() {
        return gENTALnumedesde;
    }

    public void setGENTALnumedesde(String gENTALnumedesde) {
        this.gENTALnumedesde = gENTALnumedesde;
    }

    public String getGENTALnumehasta() {
        return gENTALnumehasta;
    }

    public void setGENTALnumehasta(String gENTALnumehasta) {
        this.gENTALnumehasta = gENTALnumehasta;
    }

    public String getGENUSUcodigo() {
        return gENUSUcodigo;
    }

    public void setGENUSUcodigo(String gENUSUcodigo) {
        this.gENUSUcodigo = gENUSUcodigo;
    }

    public Date getGENTALfechaing() {
        return gENTALfechaing;
    }

    public void setGENTALfechaing(Date gENTALfechaing) {
        this.gENTALfechaing = gENTALfechaing;
    }

    public String getGENTALimprimefact() {
        return gENTALimprimefact;
    }

    public void setGENTALimprimefact(String gENTALimprimefact) {
        this.gENTALimprimefact = gENTALimprimefact;
    }

    public Integer getGENTALlineadocum() {
        return gENTALlineadocum;
    }

    public void setGENTALlineadocum(Integer gENTALlineadocum) {
        this.gENTALlineadocum = gENTALlineadocum;
    }

    public String getGENTALdocelectronico() {
        return gENTALdocelectronico;
    }

    public void setGENTALdocelectronico(String gENTALdocelectronico) {
        this.gENTALdocelectronico = gENTALdocelectronico;
    }

    public String getGENTALesquemaAutov() {
        return gENTALesquemaAutov;
    }

    public void setGENTALesquemaAutov(String gENTALesquemaAutov) {
        this.gENTALesquemaAutov = gENTALesquemaAutov;
    }

    public String getGENTALdireccionSucursal() {
        return gENTALdireccionSucursal;
    }

    public void setGENTALdireccionSucursal(String gENTALdireccionSucursal) {
        this.gENTALdireccionSucursal = gENTALdireccionSucursal;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gENTALsecuencia != null ? gENTALsecuencia.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GenpTalonarios)) {
            return false;
        }
        GenpTalonarios other = (GenpTalonarios) object;
        if ((this.gENTALsecuencia == null && other.gENTALsecuencia != null) || (this.gENTALsecuencia != null && !this.gENTALsecuencia.equals(other.gENTALsecuencia))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.GenpTalonarios[ gENTALsecuencia=" + gENTALsecuencia + " ]";
    }
    
}
