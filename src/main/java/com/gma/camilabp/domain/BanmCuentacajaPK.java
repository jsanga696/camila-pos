/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Acer
 */
@Embeddable
public class BanmCuentacajaPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENCIA_codigo")
    private String gENCIAcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "BANENT_tipo")
    private String bANENTtipo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "BANENT_codigo")
    private String bANENTcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "BANCTA_codigo")
    private String bANCTAcodigo;

    public BanmCuentacajaPK() {
    }

    public BanmCuentacajaPK(String gENCIAcodigo, String bANENTtipo, String bANENTcodigo, String bANCTAcodigo) {
        this.gENCIAcodigo = gENCIAcodigo;
        this.bANENTtipo = bANENTtipo;
        this.bANENTcodigo = bANENTcodigo;
        this.bANCTAcodigo = bANCTAcodigo;
    }

    public String getGENCIAcodigo() {
        return gENCIAcodigo;
    }

    public void setGENCIAcodigo(String gENCIAcodigo) {
        this.gENCIAcodigo = gENCIAcodigo;
    }

    public String getBANENTtipo() {
        return bANENTtipo;
    }

    public void setBANENTtipo(String bANENTtipo) {
        this.bANENTtipo = bANENTtipo;
    }

    public String getBANENTcodigo() {
        return bANENTcodigo;
    }

    public void setBANENTcodigo(String bANENTcodigo) {
        this.bANENTcodigo = bANENTcodigo;
    }

    public String getBANCTAcodigo() {
        return bANCTAcodigo;
    }

    public void setBANCTAcodigo(String bANCTAcodigo) {
        this.bANCTAcodigo = bANCTAcodigo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gENCIAcodigo != null ? gENCIAcodigo.hashCode() : 0);
        hash += (bANENTtipo != null ? bANENTtipo.hashCode() : 0);
        hash += (bANENTcodigo != null ? bANENTcodigo.hashCode() : 0);
        hash += (bANCTAcodigo != null ? bANCTAcodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BanmCuentacajaPK)) {
            return false;
        }
        BanmCuentacajaPK other = (BanmCuentacajaPK) object;
        if ((this.gENCIAcodigo == null && other.gENCIAcodigo != null) || (this.gENCIAcodigo != null && !this.gENCIAcodigo.equals(other.gENCIAcodigo))) {
            return false;
        }
        if ((this.bANENTtipo == null && other.bANENTtipo != null) || (this.bANENTtipo != null && !this.bANENTtipo.equals(other.bANENTtipo))) {
            return false;
        }
        if ((this.bANENTcodigo == null && other.bANENTcodigo != null) || (this.bANENTcodigo != null && !this.bANENTcodigo.equals(other.bANENTcodigo))) {
            return false;
        }
        if ((this.bANCTAcodigo == null && other.bANCTAcodigo != null) || (this.bANCTAcodigo != null && !this.bANCTAcodigo.equals(other.bANCTAcodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.BanmCuentacajaPK[ gENCIAcodigo=" + gENCIAcodigo + ", bANENTtipo=" + bANENTtipo + ", bANENTcodigo=" + bANENTcodigo + ", bANCTAcodigo=" + bANCTAcodigo + " ]";
    }
    
}
