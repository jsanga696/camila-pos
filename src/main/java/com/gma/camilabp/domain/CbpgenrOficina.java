/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb
 */
@Entity
@Table(name = "CBPGENR_OFICINA")
@NamedQueries({
    @NamedQuery(name = "CbpgenrOficina.findAll", query = "SELECT c FROM CbpgenrOficina c")})
public class CbpgenrOficina implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "gencia_codigo")
    private long genciaCodigo;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "genofi_codigo")
    private Long genofiCodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "genofi_secuencia")
    private String genofiSecuencia;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "genofi_nombre")
    private String genofiNombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "genofi_nomComercial")
    private String genofinomComercial;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "genofi_direccion")
    private String genofiDireccion;
    @Size(max = 150)
    @Column(name = "genofi_titulo")
    private String genofiTitulo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 13)
    @Column(name = "genofi_ruc")
    private String genofiRuc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "genofi_telefono")
    private String genofiTelefono;
    @Size(max = 100)
    @Column(name = "genofi_celular")
    private String genofiCelular;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "genofi_fax")
    private String genofiFax;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "genofi_ciudad")
    private String genofiCiudad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "genofi_tipo")
    private int genofiTipo;
    @Column(name = "genofi_fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date genofiFecha;
    @Basic(optional = false)
    @NotNull
    @Column(name = "genofi_estado")
    private boolean genofiEstado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "genofi_regional")
    private boolean genofiRegional;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "genofi_letiniclte")
    private String genofiLetiniclte;
    @Column(name = "genofi_creafecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date genofiCreafecha;
    @Column(name = "genofi_creauser")
    private BigInteger genofiCreauser;
    @Size(max = 15)
    @Column(name = "genofi_codhomologa")
    private String genofiCodhomologa;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "genofiCodigo")
    private Collection<CbpgenpParamoficina> cbpgenpParamoficinaCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "genofiCodigo")
    private Collection<CbpgenrBodega> cbpgenrBodegaCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "genofiCodigo")
    private Collection<CbpactEquipo> cbpactEquipoCollection;
    
    public CbpgenrOficina() {
    }

    public CbpgenrOficina(Long genofiCodigo) {
        this.genofiCodigo = genofiCodigo;
    }

    public CbpgenrOficina(Long genofiCodigo, long genciaCodigo, String genofiSecuencia, String genofiNombre, String genofinomComercial, String genofiDireccion, String genofiRuc, String genofiTelefono, String genofiFax, String genofiCiudad, int genofiTipo, boolean genofiEstado, boolean genofiRegional, String genofiLetiniclte) {
        this.genofiCodigo = genofiCodigo;
        this.genciaCodigo = genciaCodigo;
        this.genofiSecuencia = genofiSecuencia;
        this.genofiNombre = genofiNombre;
        this.genofinomComercial = genofinomComercial;
        this.genofiDireccion = genofiDireccion;
        this.genofiRuc = genofiRuc;
        this.genofiTelefono = genofiTelefono;
        this.genofiFax = genofiFax;
        this.genofiCiudad = genofiCiudad;
        this.genofiTipo = genofiTipo;
        this.genofiEstado = genofiEstado;
        this.genofiRegional = genofiRegional;
        this.genofiLetiniclte = genofiLetiniclte;
    }

    public long getGenciaCodigo() {
        return genciaCodigo;
    }

    public void setGenciaCodigo(long genciaCodigo) {
        this.genciaCodigo = genciaCodigo;
    }

    public Long getGenofiCodigo() {
        return genofiCodigo;
    }

    public void setGenofiCodigo(Long genofiCodigo) {
        this.genofiCodigo = genofiCodigo;
    }

    public String getGenofiSecuencia() {
        return genofiSecuencia;
    }

    public void setGenofiSecuencia(String genofiSecuencia) {
        this.genofiSecuencia = genofiSecuencia;
    }

    public String getGenofiNombre() {
        return genofiNombre;
    }

    public void setGenofiNombre(String genofiNombre) {
        this.genofiNombre = genofiNombre;
    }

    public String getGenofinomComercial() {
        return genofinomComercial;
    }

    public void setGenofinomComercial(String genofinomComercial) {
        this.genofinomComercial = genofinomComercial;
    }

    public String getGenofiDireccion() {
        return genofiDireccion;
    }

    public void setGenofiDireccion(String genofiDireccion) {
        this.genofiDireccion = genofiDireccion;
    }

    public String getGenofiTitulo() {
        return genofiTitulo;
    }

    public void setGenofiTitulo(String genofiTitulo) {
        this.genofiTitulo = genofiTitulo;
    }

    public String getGenofiRuc() {
        return genofiRuc;
    }

    public void setGenofiRuc(String genofiRuc) {
        this.genofiRuc = genofiRuc;
    }

    public String getGenofiTelefono() {
        return genofiTelefono;
    }

    public void setGenofiTelefono(String genofiTelefono) {
        this.genofiTelefono = genofiTelefono;
    }

    public String getGenofiCelular() {
        return genofiCelular;
    }

    public void setGenofiCelular(String genofiCelular) {
        this.genofiCelular = genofiCelular;
    }

    public String getGenofiFax() {
        return genofiFax;
    }

    public void setGenofiFax(String genofiFax) {
        this.genofiFax = genofiFax;
    }

    public String getGenofiCiudad() {
        return genofiCiudad;
    }

    public void setGenofiCiudad(String genofiCiudad) {
        this.genofiCiudad = genofiCiudad;
    }

    public int getGenofiTipo() {
        return genofiTipo;
    }

    public void setGenofiTipo(int genofiTipo) {
        this.genofiTipo = genofiTipo;
    }

    public Date getGenofiFecha() {
        return genofiFecha;
    }

    public void setGenofiFecha(Date genofiFecha) {
        this.genofiFecha = genofiFecha;
    }

    public boolean getGenofiEstado() {
        return genofiEstado;
    }

    public void setGenofiEstado(boolean genofiEstado) {
        this.genofiEstado = genofiEstado;
    }

    public boolean getGenofiRegional() {
        return genofiRegional;
    }

    public void setGenofiRegional(boolean genofiRegional) {
        this.genofiRegional = genofiRegional;
    }

    public String getGenofiLetiniclte() {
        return genofiLetiniclte;
    }

    public void setGenofiLetiniclte(String genofiLetiniclte) {
        this.genofiLetiniclte = genofiLetiniclte;
    }

    public Date getGenofiCreafecha() {
        return genofiCreafecha;
    }

    public void setGenofiCreafecha(Date genofiCreafecha) {
        this.genofiCreafecha = genofiCreafecha;
    }

    public BigInteger getGenofiCreauser() {
        return genofiCreauser;
    }

    public void setGenofiCreauser(BigInteger genofiCreauser) {
        this.genofiCreauser = genofiCreauser;
    }

    public String getGenofiCodhomologa() {
        return genofiCodhomologa;
    }

    public void setGenofiCodhomologa(String genofiCodhomologa) {
        this.genofiCodhomologa = genofiCodhomologa;
    }

    public Collection<CbpgenpParamoficina> getCbpgenpParamoficinaCollection() {
        return cbpgenpParamoficinaCollection;
    }

    public void setCbpgenpParamoficinaCollection(Collection<CbpgenpParamoficina> cbpgenpParamoficinaCollection) {
        this.cbpgenpParamoficinaCollection = cbpgenpParamoficinaCollection;
    }

    public Collection<CbpgenrBodega> getCbpgenrBodegaCollection() {
        return cbpgenrBodegaCollection;
    }

    public void setCbpgenrBodegaCollection(Collection<CbpgenrBodega> cbpgenrBodegaCollection) {
        this.cbpgenrBodegaCollection = cbpgenrBodegaCollection;
    }

    public Collection<CbpactEquipo> getCbpactEquipoCollection() {
        return cbpactEquipoCollection;
    }

    public void setCbpactEquipoCollection(Collection<CbpactEquipo> cbpactEquipoCollection) {
        this.cbpactEquipoCollection = cbpactEquipoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (genofiCodigo != null ? genofiCodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbpgenrOficina)) {
            return false;
        }
        CbpgenrOficina other = (CbpgenrOficina) object;
        if ((this.genofiCodigo == null && other.genofiCodigo != null) || (this.genofiCodigo != null && !this.genofiCodigo.equals(other.genofiCodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CbpgenrOficina[ genofiCodigo=" + genofiCodigo + " ]";
    }
    
}
