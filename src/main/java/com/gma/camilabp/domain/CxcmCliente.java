/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
//import org.hibernate.annotations.Formula;

/**
 *
 * @author userdb6
 */
@Entity
@Table(name = "CXCM_CLIENTE")
@NamedQueries({
    @NamedQuery(name = "CxcmCliente.findAll", query = "SELECT c FROM CxcmCliente c")})
public class CxcmCliente implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CxcmClientePK cxcmClientePK;
    @Size(max = 6)
    @Column(name = "CXCGRP_codiggrupo")
    private String cXCGRPcodiggrupo;
    @Size(max = 100)
    @Column(name = "CXCCLI_razonsocia")
    private String cXCCLIrazonsocia;
    @Size(max = 6)
    @Column(name = "FACVDR_codigo")
    private String fACVDRcodigo;
    @Size(max = 13)
    @Column(name = "CXCCLI_ruc_ci")
    private String cXCCLIrucci;
    @Size(max = 200)
    @Column(name = "CXCCLI_direccion1")
    private String cXCCLIdireccion1;
    @Size(max = 200)
    @Column(name = "CXCCLI_direccion2")
    private String cXCCLIdireccion2;
    @Size(max = 100)
    @Column(name = "CXCCLI_reprelegal")
    private String cXCCLIreprelegal;
    @Size(max = 4)
    @Column(name = "CXCCDI_codigo")
    private String cXCCDIcodigo;
    @Size(max = 100)
    @Column(name = "CXCCLI_contacto")
    private String cXCCLIcontacto;
    @Size(max = 25)
    @Column(name = "CXCCLI_telefono")
    private String cXCCLItelefono;
    @Size(max = 25)
    @Column(name = "CXCCLI_fax")
    private String cXCCLIfax;
    @Size(max = 10)
    @Column(name = "CXCRUT_codigo")
    private String cXCRUTcodigo;
    @Size(max = 25)
    @Column(name = "CXCCLI_celular")
    private String cXCCLIcelular;
    @Size(max = 200)
    @Column(name = "CXCCLI_email")
    private String cXCCLIemail;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "CXCCLI_cupocredit")
    private BigDecimal cXCCLIcupocredit;
    @Column(name = "CXCCLI_diascredi")
    private Short cXCCLIdiascredi;
    @Size(max = 6)
    @Column(name = "CXCSCN_codigo")
    private String cXCSCNcodigo;
    @Size(max = 15)
    @Column(name = "CXCCLI_cuentacont")
    private String cXCCLIcuentacont;
    @Column(name = "CXCCLI_politidesc")
    private Character cXCCLIpolitidesc;
    @Column(name = "CXCCLI_fechaingre")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cXCCLIfechaingre;
    @Column(name = "CXCCLI_fechaelimi")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cXCCLIfechaelimi;
    @Column(name = "CXCCLI_clase")
    private Character cXCCLIclase;
    @Column(name = "CXCCLI_frecu_comp")
    private Long cXCCLIfrecucomp;
    @Column(name = "CXCCLI_fechamodif")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cXCCLIfechamodif;
    @Size(max = 1000)
    @Column(name = "CXCCLI_observa")
    private String cXCCLIobserva;
    @Size(max = 2)
    @Column(name = "FACGCL_coddsctcli")
    private String fACGCLcoddsctcli;
    @Column(name = "CXCCLI_estactivo")
    private String cXCCLIestactivo;
    @Column(name = "CXCCLI_estcredito")
    private Character cXCCLIestcredito;
    @Column(name = "CXCCLI_estretenpe")
    private Character cXCCLIestretenpe;
    @Column(name = "CXCCLI_fecvisides")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cXCCLIfecvisides;
    @Column(name = "CXCCLI_estconespe")
    private Character cXCCLIestconespe;
    @Column(name = "CXCCLI_fecvisihas")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cXCCLIfecvisihas;
    @Column(name = "CXCCLI_estbkorder")
    private Character cXCCLIestbkorder;
    @Column(name = "CXCCLI_estcompete")
    private Character cXCCLIestcompete;
    @Size(max = 18)
    @Column(name = "CXCCLI_codigbarra")
    private String cXCCLIcodigbarra;
    @Lob
    @Column(name = "CXCCLI_foto")
    private byte[] cXCCLIfoto;
    @Column(name = "CXCCLI_estcheque")
    private Character cXCCLIestcheque;
    @Column(name = "CXCCLI_prioridad")
    private Short cXCCLIprioridad;
    @Column(name = "CXCCLI_ordentrega")
    private Long cXCCLIordentrega;
    @Column(name = "CXCCLI_controlcli")
    private Character cXCCLIcontrolcli;
    @Column(name = "CXCCLI_chqafecsal")
    private Character cXCCLIchqafecsal;
    @Column(name = "CXCCLI_chqconsdeu")
    private Character cXCCLIchqconsdeu;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CXCCLI_geogx")
    private BigDecimal cXCCLIgeogx;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CXCCLI_geogy")
    private BigDecimal cXCCLIgeogy;
    @Size(max = 5)
    @Column(name = "CXCCLI_horavis")
    private String cXCCLIhoravis;
    @Column(name = "cxccli_control")
    private Character cxccliControl;
    @Column(name = "CXCCLI_FECHAREACT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cxccliFechareact;
    @Column(name = "CXCCLI_tipo")
    private Character cXCCLItipo;
    @Size(max = 4)
    @Column(name = "CXCCLI_clinivvta")
    private String cXCCLIclinivvta;
    @Size(max = 15)
    @Column(name = "INVITM_codcubeta")
    private String iNVITMcodcubeta;
    @Size(max = 10)
    @Column(name = "CXCTIP_codigo")
    private String cXCTIPcodigo;
    @Size(max = 20)
    @Column(name = "CXCCLI_relacional")
    private String cXCCLIrelacional;
    @Column(name = "CXCCLI_porcdesc")
    private BigDecimal cXCCLIporcdesc;
    @Size(max = 6)
    @Column(name = "RTKLSP_codigo")
    private String rTKLSPcodigo;
    @Column(name = "CXCCLI_premisse")
    private Character cXCCLIpremisse;
    @Size(max = 6)
    @Column(name = "CXCBRR_codigo")
    private String cXCBRRcodigo;
    @Column(name = "CXCCLI_fechacategoria")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cXCCLIfechacategoria;
    @Column(name = "CXCCLI_fechafreccomp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cXCCLIfechafreccomp;
    @Column(name = "CXCCLI_calificacion")
    private Character cXCCLIcalificacion;
    @Column(name = "CXCCLI_estcenprop")
    private Character cXCCLIestcenprop;
    @Column(name = "CXCCLI_estcencomp")
    private Character cXCCLIestcencomp;
    @Column(name = "CXCCLI_estenvprop")
    private Character cXCCLIestenvprop;
    @Column(name = "CXCCLI_estenvcomp")
    private Character cXCCLIestenvcomp;
    @Column(name = "CXCCLI_estacpuprop")
    private Character cXCCLIestacpuprop;
    @Column(name = "CXCCLI_estacpucomp")
    private Character cXCCLIestacpucomp;
    @Size(max = 2)
    @Column(name = "RTKMEC_codigo")
    private String rTKMECcodigo;
    @Column(name = "Sync_estado")
    private Character syncestado;
    @Column(name = "Sync_fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date syncfecha;
    @Size(max = 6)
    @Column(name = "CXCCLI_PADRE")
    private String cxccliPadre;
    @Column(name = "CXCCLI_diasgracia")
    private Short cXCCLIdiasgracia;
    @Column(name = "CXCCLI_estRUCI")
    private Character cXCCLIestRUCI;
    @Column(name = "CXCCLI_destinfact")
    private Character cXCCLIdestinfact;
    @Column(name = "RTKMOB_id")
    private Integer rTKMOBid;
    @Column(name = "FACVDR_secvisita")
    private Integer fACVDRsecvisita;
    @Column(name = "MobileSync")
    private Character mobileSync;
    @Column(name = "RouteSync")
    private Integer routeSync;
    @Column(name = "CXCCLI_ecorpora")
    private Character cXCCLIecorpora;
    @Size(max = 2)
    @Column(name = "CXCTPR_codigo")
    private String cXCTPRcodigo;
    @Size(max = 1000)
    @Column(name = "CXCCLI_comentario")
    private String cXCCLIcomentario;
    @Column(name = "CXCCLI_cltevarios")
    private Character cXCCLIcltevarios;
    @Column(name = "CXCCLI_Solicfact")
    private Character cXCCLISolicfact;
    @Size(max = 15)
    @Column(name = "CXCCLI_codplanta")
    private String cXCCLIcodplanta;
    @Size(max = 500)
    @Column(name = "CXCCLI_rutaimagen")
    private String cXCCLIrutaimagen;
    @Column(name = "CXCCLI_diasacepta")
    private Integer cXCCLIdiasacepta;
    /*@Column(name = "CXCCLI_idcodigo")
    private int cXCCLIidcodigo;*/
    @Size(max = 1)
    @Column(name = "CXCCLI_estejecucion")
    private String cXCCLIestejecucion;
    @Size(max = 100)
    @Column(name = "CXCCLI_observaejecu")
    private String cXCCLIobservaejecu;
    @Size(max = 1)
    @Column(name = "CXCCLI_esteplanaroma")
    private String cXCCLIesteplanaroma;
    @Size(max = 1)
    @Column(name = "CXCCLI_estblockpc")
    private String cXCCLIestblockpc;
    @Size(max = 1)
    @Column(name = "CXCCLI_estblockhc")
    private String cXCCLIestblockhc;
    @Size(max = 1)
    @Column(name = "CXCCLI_estblockfoods")
    private String cXCCLIestblockfoods;
    @Column(name = "CXCCLI_ordenvisita")
    private Integer cXCCLIordenvisita;
    @Size(max = 10)
    @Column(name = "CXCRTM_rutamercad")
    private String cXCRTMrutamercad;
    @Size(max = 1)
    @Column(name = "CXCCLI_estblockcrocante")
    private String cXCCLIestblockcrocante;
    @Size(max = 1)
    @Column(name = "CXCCLI_estcontable")
    private String cXCCLIestcontable;
    @Size(max = 10)
    @Column(name = "CXCCLI_codlistaprecio")
    private String cXCCLIcodlistaprecio;
    @Size(max = 2)
    @Column(name = "CXCCLI_estexhibosecun")
    private String cXCCLIestexhibosecun;
    @Size(max = 2)
    @Column(name = "CXCCLI_estmaterialPOP")
    private String cXCCLIestmaterialPOP;
    @Size(max = 5)
    @Column(name = "CXCEST_codigo")
    private String cXCESTcodigo;
    @Size(max = 5)
    @Column(name = "CXCTP0_codigo")
    private String cXCTP0codigo;
    @Size(max = 6)
    @Column(name = "FACVDR_codvencobranza")
    private String fACVDRcodvencobranza;
    @Size(max = 10)
    @Column(name = "CXCCLI_tipologia")
    private String cXCCLItipologia;
    @Size(max = 10)
    @Column(name = "CXCCLI_codpostal")
    private String cXCCLIcodpostal;
    @Size(max = 10)
    @Column(name = "CXCTPO_codigo")
    private String cXCTPOcodigo;
    @Size(max = 4)
    @Column(name = "CXCCLF_codigo")
    private String cXCCLFcodigo;
    @Size(max = 10)
    @Column(name = "CXCCLA_codigo")
    private String cXCCLAcodigo;
    @Column(name = "CXCSTF_codigo")
    private Integer cXCSTFcodigo;
    @Size(max = 10)
    @Column(name = "CXCRTM_rutaentrega")
    private String cXCRTMrutaentrega;
    @Size(max = 4)
    @Column(name = "CXCMCR_codigo")
    private String cXCMCRcodigo;
    @Size(max = 4)
    @Column(name = "CXCZNA_codigo")
    private String cXCZNAcodigo;
    @Size(max = 4)
    @Column(name = "CXCMZA_codigo")
    private String cXCMZAcodigo;
    @Size(max = 1)
    @Column(name = "CXCCLI_estreserva")
    private String cXCCLIestreserva;
    @Size(max = 1)
    @Column(name = "CXCCLI_consolidafac")
    private String cXCCLIconsolidafac;
    @Column(name = "CXCRUT_coddia")
    private Integer cXCRUTcoddia;
    @Size(max = 1)
    @Column(name = "CXCCLI_estguiaremi")
    private String cXCCLIestguiaremi;
    @Size(max = 1)
    @Column(name = "CXCCLI_cargofletes")
    private String cXCCLIcargofletes;
    @Size(max = 1)
    @Column(name = "CXCCLI_clirelaciATS")
    private String cXCCLIclirelaciATS;
    @Size(max = 50)
    @Column(name = "CXCCLI_geografico")
    private String cXCCLIgeografico;
    @Size(max = 10)
    @Column(name = "FACVDR_codrelacionado")
    private String fACVDRcodrelacionado;
    @Size(max = 5)
    @Column(name = "CXCACO_codigo")
    private String cXCACOcodigo;
    
    @Column(name = "CXCFPG_codigo")
    private String cxcfpgCodigo;
    @Column(name = "CXCTPG_codigo")
    private String cxctpgCodigo;
    /*@JoinColumns({
        @JoinColumn(name = "GENCIA_codigo", referencedColumnName = "GENCIA_codigo" )
        , @JoinColumn(name = "CXCFPG_codigo", referencedColumnName = "CXCFPG_codigo")})
    @ManyToOne(optional = true, fetch = FetchType.LAZY)
    private CxcmFormapago cxcmFormapago;*/
    /*
    @JoinColumns({
        @JoinColumn(name = "GENCIA_codigo", referencedColumnName = "GENCIA_codigo", insertable = false, updatable = false)
        , @JoinColumn(name = "CXCEXC_codigo", referencedColumnName = "CXCEXC_codigo")})
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private CxcrExclusivi cxcrExclusivi;
    @JoinColumn(name = "CXCTPG_codigo", referencedColumnName = "CXCTPG_codigo")
    @ManyToOne(fetch = FetchType.LAZY)
    private CxcrTermipago cXCTPGcodigo;
    @JoinColumns({
        @JoinColumn(name = "GENCIA_codigo", referencedColumnName = "GENCIA_codigo", insertable = false, updatable = false)
        , @JoinColumn(name = "GENOFI_codigo", referencedColumnName = "GENOFI_codigo", insertable = false, updatable = false)})
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private GenrOficina genrOficina;*/
    @Transient
    private String codigoCliente;
    @Transient
    private BigDecimal saldo;
    @Transient
    private BigDecimal disponible;
    //@Formula(value = "(SELECT ISNULL(SUM(d.CXCDEU_saldomov),0.00) FROM CXCT_DOCUMENTO d WHERE d.CXCCLI_CODIGO=CXCCLI_CODIGO AND d.CXCDEU_saldomov > 0 AND  d.GENTDO_codigo IN ('FAC', 'NDB', 'NDS', 'PRO') AND  d.GENOFI_codigo =GENOFI_codigo AND  d.GENCIA_codigo =GENCIA_codigo)")
    @Transient
    private BigDecimal saldoCliente;
    @Transient
    private BigDecimal disponibleCliente;

    public CxcmCliente() {
    }

    public CxcmCliente(CxcmClientePK cxcmClientePK) {
        this.cxcmClientePK = cxcmClientePK;
    }

    public CxcmCliente(CxcmClientePK cxcmClientePK, BigDecimal cXCCLIgeogx, BigDecimal cXCCLIgeogy/*, int cXCCLIidcodigo*/) {
        this.cxcmClientePK = cxcmClientePK;
        this.cXCCLIgeogx = cXCCLIgeogx;
        this.cXCCLIgeogy = cXCCLIgeogy;
        //this.cXCCLIidcodigo = cXCCLIidcodigo;
    }

    public CxcmCliente(String gENCIAcodigo, String gENOFIcodigo, String cXCCLIcodigo) {
        this.cxcmClientePK = new CxcmClientePK(gENCIAcodigo, gENOFIcodigo, cXCCLIcodigo);
    }

    public CxcmClientePK getCxcmClientePK() {
        return cxcmClientePK;
    }

    public void setCxcmClientePK(CxcmClientePK cxcmClientePK) {
        this.cxcmClientePK = cxcmClientePK;
    }

    public String getCXCGRPcodiggrupo() {
        return cXCGRPcodiggrupo;
    }

    public void setCXCGRPcodiggrupo(String cXCGRPcodiggrupo) {
        this.cXCGRPcodiggrupo = cXCGRPcodiggrupo;
    }

    public String getCXCCLIrazonsocia() {
        return cXCCLIrazonsocia;
    }

    public void setCXCCLIrazonsocia(String cXCCLIrazonsocia) {
        this.cXCCLIrazonsocia = cXCCLIrazonsocia;
    }

    public String getFACVDRcodigo() {
        return fACVDRcodigo;
    }

    public void setFACVDRcodigo(String fACVDRcodigo) {
        this.fACVDRcodigo = fACVDRcodigo;
    }

    public String getCXCCLIrucci() {
        return cXCCLIrucci;
    }

    public void setCXCCLIrucci(String cXCCLIrucci) {
        this.cXCCLIrucci = cXCCLIrucci;
    }

    public String getCXCCLIdireccion1() {
        return cXCCLIdireccion1;
    }

    public void setCXCCLIdireccion1(String cXCCLIdireccion1) {
        this.cXCCLIdireccion1 = cXCCLIdireccion1;
    }

    public String getCXCCLIdireccion2() {
        return cXCCLIdireccion2;
    }

    public void setCXCCLIdireccion2(String cXCCLIdireccion2) {
        this.cXCCLIdireccion2 = cXCCLIdireccion2;
    }

    public String getCXCCLIreprelegal() {
        return cXCCLIreprelegal;
    }

    public void setCXCCLIreprelegal(String cXCCLIreprelegal) {
        this.cXCCLIreprelegal = cXCCLIreprelegal;
    }

    public String getCXCCDIcodigo() {
        return cXCCDIcodigo;
    }

    public void setCXCCDIcodigo(String cXCCDIcodigo) {
        this.cXCCDIcodigo = cXCCDIcodigo;
    }

    public String getCXCCLIcontacto() {
        return cXCCLIcontacto;
    }

    public void setCXCCLIcontacto(String cXCCLIcontacto) {
        this.cXCCLIcontacto = cXCCLIcontacto;
    }

    public String getCXCCLItelefono() {
        return cXCCLItelefono;
    }

    public void setCXCCLItelefono(String cXCCLItelefono) {
        this.cXCCLItelefono = cXCCLItelefono;
    }

    public String getCXCCLIfax() {
        return cXCCLIfax;
    }

    public void setCXCCLIfax(String cXCCLIfax) {
        this.cXCCLIfax = cXCCLIfax;
    }

    public String getCXCRUTcodigo() {
        return cXCRUTcodigo;
    }

    public void setCXCRUTcodigo(String cXCRUTcodigo) {
        this.cXCRUTcodigo = cXCRUTcodigo;
    }

    public String getCXCCLIcelular() {
        return cXCCLIcelular;
    }

    public void setCXCCLIcelular(String cXCCLIcelular) {
        this.cXCCLIcelular = cXCCLIcelular;
    }

    public String getCXCCLIemail() {
        return cXCCLIemail;
    }

    public void setCXCCLIemail(String cXCCLIemail) {
        this.cXCCLIemail = cXCCLIemail;
    }

    public BigDecimal getCXCCLIcupocredit() {
        return cXCCLIcupocredit;
    }

    public void setCXCCLIcupocredit(BigDecimal cXCCLIcupocredit) {
        this.cXCCLIcupocredit = cXCCLIcupocredit;
    }

    public Short getCXCCLIdiascredi() {
        return cXCCLIdiascredi;
    }

    public void setCXCCLIdiascredi(Short cXCCLIdiascredi) {
        this.cXCCLIdiascredi = cXCCLIdiascredi;
    }

    public String getCXCSCNcodigo() {
        return cXCSCNcodigo;
    }

    public void setCXCSCNcodigo(String cXCSCNcodigo) {
        this.cXCSCNcodigo = cXCSCNcodigo;
    }

    public String getCXCCLIcuentacont() {
        return cXCCLIcuentacont;
    }

    public void setCXCCLIcuentacont(String cXCCLIcuentacont) {
        this.cXCCLIcuentacont = cXCCLIcuentacont;
    }

    public Character getCXCCLIpolitidesc() {
        return cXCCLIpolitidesc;
    }

    public void setCXCCLIpolitidesc(Character cXCCLIpolitidesc) {
        this.cXCCLIpolitidesc = cXCCLIpolitidesc;
    }

    public Date getCXCCLIfechaingre() {
        return cXCCLIfechaingre;
    }

    public void setCXCCLIfechaingre(Date cXCCLIfechaingre) {
        this.cXCCLIfechaingre = cXCCLIfechaingre;
    }

    public Date getCXCCLIfechaelimi() {
        return cXCCLIfechaelimi;
    }

    public void setCXCCLIfechaelimi(Date cXCCLIfechaelimi) {
        this.cXCCLIfechaelimi = cXCCLIfechaelimi;
    }

    public Character getCXCCLIclase() {
        return cXCCLIclase;
    }

    public void setCXCCLIclase(Character cXCCLIclase) {
        this.cXCCLIclase = cXCCLIclase;
    }

    public Long getCXCCLIfrecucomp() {
        return cXCCLIfrecucomp;
    }

    public void setCXCCLIfrecucomp(Long cXCCLIfrecucomp) {
        this.cXCCLIfrecucomp = cXCCLIfrecucomp;
    }

    public Date getCXCCLIfechamodif() {
        return cXCCLIfechamodif;
    }

    public void setCXCCLIfechamodif(Date cXCCLIfechamodif) {
        this.cXCCLIfechamodif = cXCCLIfechamodif;
    }

    public String getCXCCLIobserva() {
        return cXCCLIobserva;
    }

    public void setCXCCLIobserva(String cXCCLIobserva) {
        this.cXCCLIobserva = cXCCLIobserva;
    }

    public String getFACGCLcoddsctcli() {
        return fACGCLcoddsctcli;
    }

    public void setFACGCLcoddsctcli(String fACGCLcoddsctcli) {
        this.fACGCLcoddsctcli = fACGCLcoddsctcli;
    }

    public String getCXCCLIestactivo() {
        return cXCCLIestactivo;
    }

    public void setCXCCLIestactivo(String cXCCLIestactivo) {
        this.cXCCLIestactivo = cXCCLIestactivo;
    }

    public Character getCXCCLIestcredito() {
        return cXCCLIestcredito;
    }

    public void setCXCCLIestcredito(Character cXCCLIestcredito) {
        this.cXCCLIestcredito = cXCCLIestcredito;
    }

    public Character getCXCCLIestretenpe() {
        return cXCCLIestretenpe;
    }

    public void setCXCCLIestretenpe(Character cXCCLIestretenpe) {
        this.cXCCLIestretenpe = cXCCLIestretenpe;
    }

    public Date getCXCCLIfecvisides() {
        return cXCCLIfecvisides;
    }

    public void setCXCCLIfecvisides(Date cXCCLIfecvisides) {
        this.cXCCLIfecvisides = cXCCLIfecvisides;
    }

    public Character getCXCCLIestconespe() {
        return cXCCLIestconespe;
    }

    public void setCXCCLIestconespe(Character cXCCLIestconespe) {
        this.cXCCLIestconespe = cXCCLIestconespe;
    }

    public Date getCXCCLIfecvisihas() {
        return cXCCLIfecvisihas;
    }

    public void setCXCCLIfecvisihas(Date cXCCLIfecvisihas) {
        this.cXCCLIfecvisihas = cXCCLIfecvisihas;
    }

    public Character getCXCCLIestbkorder() {
        return cXCCLIestbkorder;
    }

    public void setCXCCLIestbkorder(Character cXCCLIestbkorder) {
        this.cXCCLIestbkorder = cXCCLIestbkorder;
    }

    public Character getCXCCLIestcompete() {
        return cXCCLIestcompete;
    }

    public void setCXCCLIestcompete(Character cXCCLIestcompete) {
        this.cXCCLIestcompete = cXCCLIestcompete;
    }

    public String getCXCCLIcodigbarra() {
        return cXCCLIcodigbarra;
    }

    public void setCXCCLIcodigbarra(String cXCCLIcodigbarra) {
        this.cXCCLIcodigbarra = cXCCLIcodigbarra;
    }

    public byte[] getCXCCLIfoto() {
        return cXCCLIfoto;
    }

    public void setCXCCLIfoto(byte[] cXCCLIfoto) {
        this.cXCCLIfoto = cXCCLIfoto;
    }

    public Character getCXCCLIestcheque() {
        return cXCCLIestcheque;
    }

    public void setCXCCLIestcheque(Character cXCCLIestcheque) {
        this.cXCCLIestcheque = cXCCLIestcheque;
    }

    public Short getCXCCLIprioridad() {
        return cXCCLIprioridad;
    }

    public void setCXCCLIprioridad(Short cXCCLIprioridad) {
        this.cXCCLIprioridad = cXCCLIprioridad;
    }

    public Long getCXCCLIordentrega() {
        return cXCCLIordentrega;
    }

    public void setCXCCLIordentrega(Long cXCCLIordentrega) {
        this.cXCCLIordentrega = cXCCLIordentrega;
    }

    public Character getCXCCLIcontrolcli() {
        return cXCCLIcontrolcli;
    }

    public void setCXCCLIcontrolcli(Character cXCCLIcontrolcli) {
        this.cXCCLIcontrolcli = cXCCLIcontrolcli;
    }

    public Character getCXCCLIchqafecsal() {
        return cXCCLIchqafecsal;
    }

    public void setCXCCLIchqafecsal(Character cXCCLIchqafecsal) {
        this.cXCCLIchqafecsal = cXCCLIchqafecsal;
    }

    public Character getCXCCLIchqconsdeu() {
        return cXCCLIchqconsdeu;
    }

    public void setCXCCLIchqconsdeu(Character cXCCLIchqconsdeu) {
        this.cXCCLIchqconsdeu = cXCCLIchqconsdeu;
    }

    public BigDecimal getCXCCLIgeogx() {
        return cXCCLIgeogx;
    }

    public void setCXCCLIgeogx(BigDecimal cXCCLIgeogx) {
        this.cXCCLIgeogx = cXCCLIgeogx;
    }

    public BigDecimal getCXCCLIgeogy() {
        return cXCCLIgeogy;
    }

    public void setCXCCLIgeogy(BigDecimal cXCCLIgeogy) {
        this.cXCCLIgeogy = cXCCLIgeogy;
    }

    public String getCXCCLIhoravis() {
        return cXCCLIhoravis;
    }

    public void setCXCCLIhoravis(String cXCCLIhoravis) {
        this.cXCCLIhoravis = cXCCLIhoravis;
    }

    public Character getCxccliControl() {
        return cxccliControl;
    }

    public void setCxccliControl(Character cxccliControl) {
        this.cxccliControl = cxccliControl;
    }

    public Date getCxccliFechareact() {
        return cxccliFechareact;
    }

    public void setCxccliFechareact(Date cxccliFechareact) {
        this.cxccliFechareact = cxccliFechareact;
    }

    public Character getCXCCLItipo() {
        return cXCCLItipo;
    }

    public void setCXCCLItipo(Character cXCCLItipo) {
        this.cXCCLItipo = cXCCLItipo;
    }

    public String getCXCCLIclinivvta() {
        return cXCCLIclinivvta;
    }

    public void setCXCCLIclinivvta(String cXCCLIclinivvta) {
        this.cXCCLIclinivvta = cXCCLIclinivvta;
    }

    public String getINVITMcodcubeta() {
        return iNVITMcodcubeta;
    }

    public void setINVITMcodcubeta(String iNVITMcodcubeta) {
        this.iNVITMcodcubeta = iNVITMcodcubeta;
    }

    public String getCXCTIPcodigo() {
        return cXCTIPcodigo;
    }

    public void setCXCTIPcodigo(String cXCTIPcodigo) {
        this.cXCTIPcodigo = cXCTIPcodigo;
    }

    public String getCXCCLIrelacional() {
        return cXCCLIrelacional;
    }

    public void setCXCCLIrelacional(String cXCCLIrelacional) {
        this.cXCCLIrelacional = cXCCLIrelacional;
    }

    public BigDecimal getCXCCLIporcdesc() {
        return cXCCLIporcdesc;
    }

    public void setCXCCLIporcdesc(BigDecimal cXCCLIporcdesc) {
        this.cXCCLIporcdesc = cXCCLIporcdesc;
    }

    public String getRTKLSPcodigo() {
        return rTKLSPcodigo;
    }

    public void setRTKLSPcodigo(String rTKLSPcodigo) {
        this.rTKLSPcodigo = rTKLSPcodigo;
    }

    public Character getCXCCLIpremisse() {
        return cXCCLIpremisse;
    }

    public void setCXCCLIpremisse(Character cXCCLIpremisse) {
        this.cXCCLIpremisse = cXCCLIpremisse;
    }

    public String getCXCBRRcodigo() {
        return cXCBRRcodigo;
    }

    public void setCXCBRRcodigo(String cXCBRRcodigo) {
        this.cXCBRRcodigo = cXCBRRcodigo;
    }

    public Date getCXCCLIfechacategoria() {
        return cXCCLIfechacategoria;
    }

    public void setCXCCLIfechacategoria(Date cXCCLIfechacategoria) {
        this.cXCCLIfechacategoria = cXCCLIfechacategoria;
    }

    public Date getCXCCLIfechafreccomp() {
        return cXCCLIfechafreccomp;
    }

    public void setCXCCLIfechafreccomp(Date cXCCLIfechafreccomp) {
        this.cXCCLIfechafreccomp = cXCCLIfechafreccomp;
    }

    public Character getCXCCLIcalificacion() {
        return cXCCLIcalificacion;
    }

    public void setCXCCLIcalificacion(Character cXCCLIcalificacion) {
        this.cXCCLIcalificacion = cXCCLIcalificacion;
    }

    public Character getCXCCLIestcenprop() {
        return cXCCLIestcenprop;
    }

    public void setCXCCLIestcenprop(Character cXCCLIestcenprop) {
        this.cXCCLIestcenprop = cXCCLIestcenprop;
    }

    public Character getCXCCLIestcencomp() {
        return cXCCLIestcencomp;
    }

    public void setCXCCLIestcencomp(Character cXCCLIestcencomp) {
        this.cXCCLIestcencomp = cXCCLIestcencomp;
    }

    public Character getCXCCLIestenvprop() {
        return cXCCLIestenvprop;
    }

    public void setCXCCLIestenvprop(Character cXCCLIestenvprop) {
        this.cXCCLIestenvprop = cXCCLIestenvprop;
    }

    public Character getCXCCLIestenvcomp() {
        return cXCCLIestenvcomp;
    }

    public void setCXCCLIestenvcomp(Character cXCCLIestenvcomp) {
        this.cXCCLIestenvcomp = cXCCLIestenvcomp;
    }

    public Character getCXCCLIestacpuprop() {
        return cXCCLIestacpuprop;
    }

    public void setCXCCLIestacpuprop(Character cXCCLIestacpuprop) {
        this.cXCCLIestacpuprop = cXCCLIestacpuprop;
    }

    public Character getCXCCLIestacpucomp() {
        return cXCCLIestacpucomp;
    }

    public void setCXCCLIestacpucomp(Character cXCCLIestacpucomp) {
        this.cXCCLIestacpucomp = cXCCLIestacpucomp;
    }

    public String getRTKMECcodigo() {
        return rTKMECcodigo;
    }

    public void setRTKMECcodigo(String rTKMECcodigo) {
        this.rTKMECcodigo = rTKMECcodigo;
    }

    public Character getSyncestado() {
        return syncestado;
    }

    public void setSyncestado(Character syncestado) {
        this.syncestado = syncestado;
    }

    public Date getSyncfecha() {
        return syncfecha;
    }

    public void setSyncfecha(Date syncfecha) {
        this.syncfecha = syncfecha;
    }

    public String getCxccliPadre() {
        return cxccliPadre;
    }

    public void setCxccliPadre(String cxccliPadre) {
        this.cxccliPadre = cxccliPadre;
    }

    public Short getCXCCLIdiasgracia() {
        return cXCCLIdiasgracia;
    }

    public void setCXCCLIdiasgracia(Short cXCCLIdiasgracia) {
        this.cXCCLIdiasgracia = cXCCLIdiasgracia;
    }

    public Character getCXCCLIestRUCI() {
        return cXCCLIestRUCI;
    }

    public void setCXCCLIestRUCI(Character cXCCLIestRUCI) {
        this.cXCCLIestRUCI = cXCCLIestRUCI;
    }

    public Character getCXCCLIdestinfact() {
        return cXCCLIdestinfact;
    }

    public void setCXCCLIdestinfact(Character cXCCLIdestinfact) {
        this.cXCCLIdestinfact = cXCCLIdestinfact;
    }

    public Integer getRTKMOBid() {
        return rTKMOBid;
    }

    public void setRTKMOBid(Integer rTKMOBid) {
        this.rTKMOBid = rTKMOBid;
    }

    public Integer getFACVDRsecvisita() {
        return fACVDRsecvisita;
    }

    public void setFACVDRsecvisita(Integer fACVDRsecvisita) {
        this.fACVDRsecvisita = fACVDRsecvisita;
    }

    public Character getMobileSync() {
        return mobileSync;
    }

    public void setMobileSync(Character mobileSync) {
        this.mobileSync = mobileSync;
    }

    public Integer getRouteSync() {
        return routeSync;
    }

    public void setRouteSync(Integer routeSync) {
        this.routeSync = routeSync;
    }

    public Character getCXCCLIecorpora() {
        return cXCCLIecorpora;
    }

    public void setCXCCLIecorpora(Character cXCCLIecorpora) {
        this.cXCCLIecorpora = cXCCLIecorpora;
    }

    public String getCXCTPRcodigo() {
        return cXCTPRcodigo;
    }

    public void setCXCTPRcodigo(String cXCTPRcodigo) {
        this.cXCTPRcodigo = cXCTPRcodigo;
    }

    public String getCXCCLIcomentario() {
        return cXCCLIcomentario;
    }

    public void setCXCCLIcomentario(String cXCCLIcomentario) {
        this.cXCCLIcomentario = cXCCLIcomentario;
    }

    public Character getCXCCLIcltevarios() {
        return cXCCLIcltevarios;
    }

    public void setCXCCLIcltevarios(Character cXCCLIcltevarios) {
        this.cXCCLIcltevarios = cXCCLIcltevarios;
    }

    public Character getCXCCLISolicfact() {
        return cXCCLISolicfact;
    }

    public void setCXCCLISolicfact(Character cXCCLISolicfact) {
        this.cXCCLISolicfact = cXCCLISolicfact;
    }

    public String getCXCCLIcodplanta() {
        return cXCCLIcodplanta;
    }

    public void setCXCCLIcodplanta(String cXCCLIcodplanta) {
        this.cXCCLIcodplanta = cXCCLIcodplanta;
    }

    public String getCXCCLIrutaimagen() {
        return cXCCLIrutaimagen;
    }

    public void setCXCCLIrutaimagen(String cXCCLIrutaimagen) {
        this.cXCCLIrutaimagen = cXCCLIrutaimagen;
    }

    public Integer getCXCCLIdiasacepta() {
        return cXCCLIdiasacepta;
    }

    public void setCXCCLIdiasacepta(Integer cXCCLIdiasacepta) {
        this.cXCCLIdiasacepta = cXCCLIdiasacepta;
    }

    /*public int getCXCCLIidcodigo() {
        return cXCCLIidcodigo;
    }

    public void setCXCCLIidcodigo(int cXCCLIidcodigo) {
        this.cXCCLIidcodigo = cXCCLIidcodigo;
    }*/

    public String getCXCCLIestejecucion() {
        return cXCCLIestejecucion;
    }

    public void setCXCCLIestejecucion(String cXCCLIestejecucion) {
        this.cXCCLIestejecucion = cXCCLIestejecucion;
    }

    public String getCXCCLIobservaejecu() {
        return cXCCLIobservaejecu;
    }

    public void setCXCCLIobservaejecu(String cXCCLIobservaejecu) {
        this.cXCCLIobservaejecu = cXCCLIobservaejecu;
    }

    public String getCXCCLIesteplanaroma() {
        return cXCCLIesteplanaroma;
    }

    public void setCXCCLIesteplanaroma(String cXCCLIesteplanaroma) {
        this.cXCCLIesteplanaroma = cXCCLIesteplanaroma;
    }

    public String getCXCCLIestblockpc() {
        return cXCCLIestblockpc;
    }

    public void setCXCCLIestblockpc(String cXCCLIestblockpc) {
        this.cXCCLIestblockpc = cXCCLIestblockpc;
    }

    public String getCXCCLIestblockhc() {
        return cXCCLIestblockhc;
    }

    public void setCXCCLIestblockhc(String cXCCLIestblockhc) {
        this.cXCCLIestblockhc = cXCCLIestblockhc;
    }

    public String getCXCCLIestblockfoods() {
        return cXCCLIestblockfoods;
    }

    public void setCXCCLIestblockfoods(String cXCCLIestblockfoods) {
        this.cXCCLIestblockfoods = cXCCLIestblockfoods;
    }

    public Integer getCXCCLIordenvisita() {
        return cXCCLIordenvisita;
    }

    public void setCXCCLIordenvisita(Integer cXCCLIordenvisita) {
        this.cXCCLIordenvisita = cXCCLIordenvisita;
    }

    public String getCXCRTMrutamercad() {
        return cXCRTMrutamercad;
    }

    public void setCXCRTMrutamercad(String cXCRTMrutamercad) {
        this.cXCRTMrutamercad = cXCRTMrutamercad;
    }

    public String getCXCCLIestblockcrocante() {
        return cXCCLIestblockcrocante;
    }

    public void setCXCCLIestblockcrocante(String cXCCLIestblockcrocante) {
        this.cXCCLIestblockcrocante = cXCCLIestblockcrocante;
    }

    public String getCXCCLIestcontable() {
        return cXCCLIestcontable;
    }

    public void setCXCCLIestcontable(String cXCCLIestcontable) {
        this.cXCCLIestcontable = cXCCLIestcontable;
    }

    public String getCXCCLIcodlistaprecio() {
        return cXCCLIcodlistaprecio;
    }

    public void setCXCCLIcodlistaprecio(String cXCCLIcodlistaprecio) {
        this.cXCCLIcodlistaprecio = cXCCLIcodlistaprecio;
    }

    public String getCXCCLIestexhibosecun() {
        return cXCCLIestexhibosecun;
    }

    public void setCXCCLIestexhibosecun(String cXCCLIestexhibosecun) {
        this.cXCCLIestexhibosecun = cXCCLIestexhibosecun;
    }

    public String getCXCCLIestmaterialPOP() {
        return cXCCLIestmaterialPOP;
    }

    public void setCXCCLIestmaterialPOP(String cXCCLIestmaterialPOP) {
        this.cXCCLIestmaterialPOP = cXCCLIestmaterialPOP;
    }

    public String getCXCESTcodigo() {
        return cXCESTcodigo;
    }

    public void setCXCESTcodigo(String cXCESTcodigo) {
        this.cXCESTcodigo = cXCESTcodigo;
    }

    public String getCXCTP0codigo() {
        return cXCTP0codigo;
    }

    public void setCXCTP0codigo(String cXCTP0codigo) {
        this.cXCTP0codigo = cXCTP0codigo;
    }

    public String getFACVDRcodvencobranza() {
        return fACVDRcodvencobranza;
    }

    public void setFACVDRcodvencobranza(String fACVDRcodvencobranza) {
        this.fACVDRcodvencobranza = fACVDRcodvencobranza;
    }

    public String getCXCCLItipologia() {
        return cXCCLItipologia;
    }

    public void setCXCCLItipologia(String cXCCLItipologia) {
        this.cXCCLItipologia = cXCCLItipologia;
    }

    public String getCXCCLIcodpostal() {
        return cXCCLIcodpostal;
    }

    public void setCXCCLIcodpostal(String cXCCLIcodpostal) {
        this.cXCCLIcodpostal = cXCCLIcodpostal;
    }

    public String getCXCTPOcodigo() {
        return cXCTPOcodigo;
    }

    public void setCXCTPOcodigo(String cXCTPOcodigo) {
        this.cXCTPOcodigo = cXCTPOcodigo;
    }

    public String getCXCCLFcodigo() {
        return cXCCLFcodigo;
    }

    public void setCXCCLFcodigo(String cXCCLFcodigo) {
        this.cXCCLFcodigo = cXCCLFcodigo;
    }

    public String getCXCCLAcodigo() {
        return cXCCLAcodigo;
    }

    public void setCXCCLAcodigo(String cXCCLAcodigo) {
        this.cXCCLAcodigo = cXCCLAcodigo;
    }

    public Integer getCXCSTFcodigo() {
        return cXCSTFcodigo;
    }

    public void setCXCSTFcodigo(Integer cXCSTFcodigo) {
        this.cXCSTFcodigo = cXCSTFcodigo;
    }

    public String getCXCRTMrutaentrega() {
        return cXCRTMrutaentrega;
    }

    public void setCXCRTMrutaentrega(String cXCRTMrutaentrega) {
        this.cXCRTMrutaentrega = cXCRTMrutaentrega;
    }

    public String getCXCMCRcodigo() {
        return cXCMCRcodigo;
    }

    public void setCXCMCRcodigo(String cXCMCRcodigo) {
        this.cXCMCRcodigo = cXCMCRcodigo;
    }

    public String getCXCZNAcodigo() {
        return cXCZNAcodigo;
    }

    public void setCXCZNAcodigo(String cXCZNAcodigo) {
        this.cXCZNAcodigo = cXCZNAcodigo;
    }

    public String getCXCMZAcodigo() {
        return cXCMZAcodigo;
    }

    public void setCXCMZAcodigo(String cXCMZAcodigo) {
        this.cXCMZAcodigo = cXCMZAcodigo;
    }

    public String getCXCCLIestreserva() {
        return cXCCLIestreserva;
    }

    public void setCXCCLIestreserva(String cXCCLIestreserva) {
        this.cXCCLIestreserva = cXCCLIestreserva;
    }

    public String getCXCCLIconsolidafac() {
        return cXCCLIconsolidafac;
    }

    public void setCXCCLIconsolidafac(String cXCCLIconsolidafac) {
        this.cXCCLIconsolidafac = cXCCLIconsolidafac;
    }

    public Integer getCXCRUTcoddia() {
        return cXCRUTcoddia;
    }

    public void setCXCRUTcoddia(Integer cXCRUTcoddia) {
        this.cXCRUTcoddia = cXCRUTcoddia;
    }

    public String getCXCCLIestguiaremi() {
        return cXCCLIestguiaremi;
    }

    public void setCXCCLIestguiaremi(String cXCCLIestguiaremi) {
        this.cXCCLIestguiaremi = cXCCLIestguiaremi;
    }

    public String getCXCCLIcargofletes() {
        return cXCCLIcargofletes;
    }

    public void setCXCCLIcargofletes(String cXCCLIcargofletes) {
        this.cXCCLIcargofletes = cXCCLIcargofletes;
    }

    public String getCXCCLIclirelaciATS() {
        return cXCCLIclirelaciATS;
    }

    public void setCXCCLIclirelaciATS(String cXCCLIclirelaciATS) {
        this.cXCCLIclirelaciATS = cXCCLIclirelaciATS;
    }

    public String getCXCCLIgeografico() {
        return cXCCLIgeografico;
    }

    public void setCXCCLIgeografico(String cXCCLIgeografico) {
        this.cXCCLIgeografico = cXCCLIgeografico;
    }

    public String getFACVDRcodrelacionado() {
        return fACVDRcodrelacionado;
    }

    public void setFACVDRcodrelacionado(String fACVDRcodrelacionado) {
        this.fACVDRcodrelacionado = fACVDRcodrelacionado;
    }

    public String getCXCACOcodigo() {
        return cXCACOcodigo;
    }

    public void setCXCACOcodigo(String cXCACOcodigo) {
        this.cXCACOcodigo = cXCACOcodigo;
    }


    public String getCodigoCliente() {
        return codigoCliente;
    }

    public String getCxcfpgCodigo() {
        return cxcfpgCodigo;
    }

    public void setCxcfpgCodigo(String cxcfpgCodigo) {
        this.cxcfpgCodigo = cxcfpgCodigo;
    }

    public String getCxctpgCodigo() {
        return cxctpgCodigo;
    }

    public void setCxctpgCodigo(String cxctpgCodigo) {
        this.cxctpgCodigo = cxctpgCodigo;
    }

    /*public CxcmFormapago getCxcmFormapago() {
        return cxcmFormapago;
    }
    
    public void setCxcmFormapago(CxcmFormapago cxcmFormapago) {
        this.cxcmFormapago = cxcmFormapago;
    }*/
    
    /*public CxcrExclusivi getCxcrExclusivi() {
    return cxcrExclusivi;
    }
    public void setCxcrExclusivi(CxcrExclusivi cxcrExclusivi) {
    this.cxcrExclusivi = cxcrExclusivi;
    }
    public CxcrTermipago getCXCTPGcodigo() {
    return cXCTPGcodigo;
    }
    public void setCXCTPGcodigo(CxcrTermipago cXCTPGcodigo) {
    this.cXCTPGcodigo = cXCTPGcodigo;
    }
    public GenrOficina getGenrOficina() {
    return genrOficina;
    }
    public void setGenrOficina(GenrOficina genrOficina) {
    this.genrOficina = genrOficina;
    }*/
    public void setCodigoCliente(String codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

    public BigDecimal getSaldo() {
        return saldo;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }

    public BigDecimal getDisponible() {
        return disponible;
    }

    public void setDisponible(BigDecimal disponible) {
        this.disponible = disponible;
    }

    public BigDecimal getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(BigDecimal saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public BigDecimal getDisponibleCliente() {
        this.disponibleCliente=this.getCXCCLIcupocredit().subtract(this.getSaldoCliente());
        return this.disponibleCliente;
    }

    public void setDisponibleCliente(BigDecimal disponibleCliente) {
        this.disponibleCliente = disponibleCliente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cxcmClientePK != null ? cxcmClientePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CxcmCliente)) {
            return false;
        }
        CxcmCliente other = (CxcmCliente) object;
        if ((this.cxcmClientePK == null && other.cxcmClientePK != null) || (this.cxcmClientePK != null && !this.cxcmClientePK.equals(other.cxcmClientePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CxcmCliente[ cxcmClientePK=" + cxcmClientePK + " ]";
    }
    
}
