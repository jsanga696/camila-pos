/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb6
 */
@Entity
@Table(name = "GENP_PARAMCOMPANIA")
@NamedQueries({
    @NamedQuery(name = "GenpParamcompania.findAll", query = "SELECT g FROM GenpParamcompania g")})
public class GenpParamcompania implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected GenpParamcompaniaPK genpParamcompaniaPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "GENPCO_nombre")
    private String gENPCOnombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "GENPCO_nombrevalor")
    private String gENPCOnombrevalor;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "GENPCO_tipo")
    private String gENPCOtipo="U";
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "GENPCO_validacion")
    private String gENPCOvalidacion="X";

    public GenpParamcompania() {
    }

    public GenpParamcompania(GenpParamcompaniaPK genpParamcompaniaPK) {
        this.genpParamcompaniaPK = genpParamcompaniaPK;
    }

    public GenpParamcompania(GenpParamcompaniaPK genpParamcompaniaPK, String gENPCOnombre, String gENPCOnombrevalor, String gENPCOtipo, String gENPCOvalidacion) {
        this.genpParamcompaniaPK = genpParamcompaniaPK;
        this.gENPCOnombre = gENPCOnombre;
        this.gENPCOnombrevalor = gENPCOnombrevalor;
        this.gENPCOtipo = gENPCOtipo;
        this.gENPCOvalidacion = gENPCOvalidacion;
    }

    public GenpParamcompania(String gENCIAcodigo, String gENAPLcodigo, String gENPCOetiqueta) {
        this.genpParamcompaniaPK = new GenpParamcompaniaPK(gENCIAcodigo, gENAPLcodigo, gENPCOetiqueta);
    }

    public GenpParamcompaniaPK getGenpParamcompaniaPK() {
        return genpParamcompaniaPK;
    }

    public void setGenpParamcompaniaPK(GenpParamcompaniaPK genpParamcompaniaPK) {
        this.genpParamcompaniaPK = genpParamcompaniaPK;
    }

    public String getGENPCOnombre() {
        return gENPCOnombre;
    }

    public void setGENPCOnombre(String gENPCOnombre) {
        this.gENPCOnombre = gENPCOnombre;
    }

    public String getGENPCOnombrevalor() {
        return gENPCOnombrevalor;
    }

    public void setGENPCOnombrevalor(String gENPCOnombrevalor) {
        this.gENPCOnombrevalor = gENPCOnombrevalor;
    }

    public String getGENPCOtipo() {
        return gENPCOtipo;
    }

    public void setGENPCOtipo(String gENPCOtipo) {
        this.gENPCOtipo = gENPCOtipo;
    }

    public String getGENPCOvalidacion() {
        return gENPCOvalidacion;
    }

    public void setGENPCOvalidacion(String gENPCOvalidacion) {
        this.gENPCOvalidacion = gENPCOvalidacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (genpParamcompaniaPK != null ? genpParamcompaniaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GenpParamcompania)) {
            return false;
        }
        GenpParamcompania other = (GenpParamcompania) object;
        if ((this.genpParamcompaniaPK == null && other.genpParamcompaniaPK != null) || (this.genpParamcompaniaPK != null && !this.genpParamcompaniaPK.equals(other.genpParamcompaniaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.GenpParamcompania[ genpParamcompaniaPK=" + genpParamcompaniaPK + " ]";
    }
    
}
