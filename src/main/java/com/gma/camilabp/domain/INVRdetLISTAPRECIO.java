/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
//import org.hibernate.annotations.Formula;

/**
 *
 * @author userdb6
 */
@Entity
@Table(name = "INVR_detLISTAPRECIO")
@NamedQueries({
    @NamedQuery(name = "INVRdetLISTAPRECIO.findAll", query = "SELECT i FROM INVRdetLISTAPRECIO i")})
public class INVRdetLISTAPRECIO implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected INVRdetLISTAPRECIOPK iNVRdetLISTAPRECIOPK;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "INVITM_prelista")
    private BigDecimal iNVITMprelista;
    @Column(name = "INVITM_preciopvp")
    private BigDecimal iNVITMpreciopvp;
    @Column(name = "INVLPR_pocentajedscto")
    private BigDecimal iNVLPRpocentajedscto;    
    //@Formula(value = "(SELECT cl.INVLPR_nombre FROM INVR_CABLISTAPRECIO cl WHERE cl.INVLPR_secuencia=INVLPR_secuencia)")
    @Transient
    private String precioLista;
    //@Formula(value = "(SELECT i.INVITM_nombre FROM INVM_ITEM i WHERE i.INVITM_codigo=INVITM_codigo)")
    @Transient
    private String nombreItem;
    //@Formula(value = "(SELECT ip.INVIPO_porcenta FROM INVR_IMPTOXPROD ip INNER JOIN GENR_IMPUESTO i ON (ip.GENIMP_codigo = i.GENIMP_codigo AND ip.GENCIA_codigo = i.GENCIA_codigo AND i.GENIMP_tipopciosp = 'iva') WHERE ip.INVITM_codigo=INVITM_codigo)")
    @Transient
    private BigDecimal porcImpuesto;
    @Transient
    private BigDecimal impuesto;
    @Transient
    private BigDecimal valor;
    //INVR_IMPTOXPROD
    /*@OneToMany(cascade = CascadeType.ALL, mappedBy = "producto")
    private List<InvmItem> productosList;*/

    public INVRdetLISTAPRECIO() {
    }

    public INVRdetLISTAPRECIO(INVRdetLISTAPRECIOPK iNVRdetLISTAPRECIOPK) {
        this.iNVRdetLISTAPRECIOPK = iNVRdetLISTAPRECIOPK;
    }

    public INVRdetLISTAPRECIO(Long iNVLPRsecuencia, String iNVLPRcodigo, String iNVITMcodigo) {
        this.iNVRdetLISTAPRECIOPK = new INVRdetLISTAPRECIOPK(iNVLPRsecuencia, iNVLPRcodigo, iNVITMcodigo);
    }

    public INVRdetLISTAPRECIOPK getINVRdetLISTAPRECIOPK() {
        return iNVRdetLISTAPRECIOPK;
    }

    public void setINVRdetLISTAPRECIOPK(INVRdetLISTAPRECIOPK iNVRdetLISTAPRECIOPK) {
        this.iNVRdetLISTAPRECIOPK = iNVRdetLISTAPRECIOPK;
    }

    public BigDecimal getINVITMprelista() {
        return iNVITMprelista;
    }

    public void setINVITMprelista(BigDecimal iNVITMprelista) {
        this.iNVITMprelista = iNVITMprelista;
    }

    public BigDecimal getINVITMpreciopvp() {
        return iNVITMpreciopvp;
    }

    public void setINVITMpreciopvp(BigDecimal iNVITMpreciopvp) {
        this.iNVITMpreciopvp = iNVITMpreciopvp;
    }

    public BigDecimal getINVLPRpocentajedscto() {
        return iNVLPRpocentajedscto;
    }

    public void setINVLPRpocentajedscto(BigDecimal iNVLPRpocentajedscto) {
        this.iNVLPRpocentajedscto = iNVLPRpocentajedscto;
    }

    public String getPrecioLista() {
        return precioLista;
    }

    public void setPrecioLista(String precioLista) {
        this.precioLista = precioLista;
    }

    public String getNombreItem() {
        return nombreItem;
    }

    public void setNombreItem(String nombreItem) {
        this.nombreItem = nombreItem;
    }

    public BigDecimal getPorcImpuesto() {
        return porcImpuesto==null?BigDecimal.ZERO:porcImpuesto;
    }

    public void setPorcImpuesto(BigDecimal porcImpuesto) {
        this.porcImpuesto = porcImpuesto;
    }

    public BigDecimal getImpuesto() {
        this.impuesto= this.getINVITMprelista().multiply(this.getPorcImpuesto()).divide(new BigDecimal("100"));
        return this.impuesto;
    }

    public void setImpuesto(BigDecimal impuesto) {
        this.impuesto = impuesto;
    }

    public BigDecimal getValor() {
        this.valor = this.getINVITMprelista().add(this.getImpuesto());
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iNVRdetLISTAPRECIOPK != null ? iNVRdetLISTAPRECIOPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof INVRdetLISTAPRECIO)) {
            return false;
        }
        INVRdetLISTAPRECIO other = (INVRdetLISTAPRECIO) object;
        if ((this.iNVRdetLISTAPRECIOPK == null && other.iNVRdetLISTAPRECIOPK != null) || (this.iNVRdetLISTAPRECIOPK != null && !this.iNVRdetLISTAPRECIOPK.equals(other.iNVRdetLISTAPRECIOPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.INVRdetLISTAPRECIO[ iNVRdetLISTAPRECIOPK=" + iNVRdetLISTAPRECIOPK + " ]";
    }
    
}
