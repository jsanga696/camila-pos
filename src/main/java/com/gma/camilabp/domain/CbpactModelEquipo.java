/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author userdb5
 */
@Entity
@Table(name = "CBPACT_MODEL_EQUIPO")
@NamedQueries({
    @NamedQuery(name = "CbpactModelEquipo.findAll", query = "SELECT c FROM CbpactModelEquipo c")})
public class CbpactModelEquipo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "fecha_ingreso")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaIngreso;
    @Column(name = "fecha_modificacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    @Column(name = "marca")
    private String marca;
    @Column(name = "modelo")
    private String modelo;
    @JoinColumn(name = "genofi_creauser", referencedColumnName = "segusu_codigo")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private CbpsegmUsuario genofiCreauser;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "modelEquipo", fetch = FetchType.LAZY)
    private List<CbpactEquipo> cbpactEquipoList;
    
    
    @Transient
    private Integer tam;
    @Transient
    private Integer cantidad;
    
    public CbpactModelEquipo() {
        this.marca = "";
        this.modelo = "";
    }

    public Integer getTam() {
        if(cbpactEquipoList == null)
            return 0;
        else
            return cbpactEquipoList.size();
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public void setTam(Integer tam) {
        this.tam = tam;
    }

    public CbpactModelEquipo(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }
    
    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public CbpsegmUsuario getGenofiCreauser() {
        return genofiCreauser;
    }

    public void setGenofiCreauser(CbpsegmUsuario genofiCreauser) {
        this.genofiCreauser = genofiCreauser;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public List<CbpactEquipo> getCbpactEquipoList() {
        return cbpactEquipoList;
    }

    public void setCbpactEquipoList(List<CbpactEquipo> cbpactEquipoList) {
        this.cbpactEquipoList = cbpactEquipoList;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbpactModelEquipo)) {
            return false;
        }
        CbpactModelEquipo other = (CbpactModelEquipo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CbpactModelEquipo[ id=" + id + " ]";
    }
    
}
