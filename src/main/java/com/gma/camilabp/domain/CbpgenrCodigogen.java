/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb
 */
@Entity
@Table(name = "CBPGENR_CODIGOGEN")
@NamedQueries({
    @NamedQuery(name = "CbpgenrCodigogen.findAll", query = "SELECT c FROM CbpgenrCodigogen c")})
public class CbpgenrCodigogen implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "gencge_codigo")
    private Long gencgeCodigo;
    @Size(max = 50)
    @Column(name = "gencge_secuencia")
    private String gencgeSecuencia;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "gencge_siglas")
    private String gencgeSiglas;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "gencge_descripcio")
    private String gencgeDescripcio;
    @Size(max = 50)
    @Column(name = "gencge_comentario")
    private String gencgeComentario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "gencge_usucrea")
    private long gencgeUsucrea;
    @Basic(optional = false)
    @NotNull
    @Column(name = "gencge_usumodi")
    private long gencgeUsumodi;
    @Basic(optional = false)
    @NotNull
    @Column(name = "gencge_fechcrea")
    @Temporal(TemporalType.TIMESTAMP)
    private Date gencgeFechcrea;
    @Basic(optional = false)
    @NotNull
    @Column(name = "gencge_fechmodi")
    @Temporal(TemporalType.TIMESTAMP)
    private Date gencgeFechmodi;

    public CbpgenrCodigogen() {
    }

    public CbpgenrCodigogen(Long gencgeCodigo) {
        this.gencgeCodigo = gencgeCodigo;
    }

    public CbpgenrCodigogen(Long gencgeCodigo, String gencgeSiglas, String gencgeDescripcio, long gencgeUsucrea, long gencgeUsumodi, Date gencgeFechcrea, Date gencgeFechmodi) {
        this.gencgeCodigo = gencgeCodigo;
        this.gencgeSiglas = gencgeSiglas;
        this.gencgeDescripcio = gencgeDescripcio;
        this.gencgeUsucrea = gencgeUsucrea;
        this.gencgeUsumodi = gencgeUsumodi;
        this.gencgeFechcrea = gencgeFechcrea;
        this.gencgeFechmodi = gencgeFechmodi;
    }

    public Long getGencgeCodigo() {
        return gencgeCodigo;
    }

    public void setGencgeCodigo(Long gencgeCodigo) {
        this.gencgeCodigo = gencgeCodigo;
    }

    public String getGencgeSecuencia() {
        return gencgeSecuencia;
    }

    public void setGencgeSecuencia(String gencgeSecuencia) {
        this.gencgeSecuencia = gencgeSecuencia;
    }

    public String getGencgeSiglas() {
        return gencgeSiglas;
    }

    public void setGencgeSiglas(String gencgeSiglas) {
        this.gencgeSiglas = gencgeSiglas;
    }

    public String getGencgeDescripcio() {
        return gencgeDescripcio;
    }

    public void setGencgeDescripcio(String gencgeDescripcio) {
        this.gencgeDescripcio = gencgeDescripcio;
    }

    public String getGencgeComentario() {
        return gencgeComentario;
    }

    public void setGencgeComentario(String gencgeComentario) {
        this.gencgeComentario = gencgeComentario;
    }

    public long getGencgeUsucrea() {
        return gencgeUsucrea;
    }

    public void setGencgeUsucrea(long gencgeUsucrea) {
        this.gencgeUsucrea = gencgeUsucrea;
    }

    public long getGencgeUsumodi() {
        return gencgeUsumodi;
    }

    public void setGencgeUsumodi(long gencgeUsumodi) {
        this.gencgeUsumodi = gencgeUsumodi;
    }

    public Date getGencgeFechcrea() {
        return gencgeFechcrea;
    }

    public void setGencgeFechcrea(Date gencgeFechcrea) {
        this.gencgeFechcrea = gencgeFechcrea;
    }

    public Date getGencgeFechmodi() {
        return gencgeFechmodi;
    }

    public void setGencgeFechmodi(Date gencgeFechmodi) {
        this.gencgeFechmodi = gencgeFechmodi;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gencgeCodigo != null ? gencgeCodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbpgenrCodigogen)) {
            return false;
        }
        CbpgenrCodigogen other = (CbpgenrCodigogen) object;
        if ((this.gencgeCodigo == null && other.gencgeCodigo != null) || (this.gencgeCodigo != null && !this.gencgeCodigo.equals(other.gencgeCodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CbpgenrCodigogen[ gencgeCodigo=" + gencgeCodigo + " ]";
    }
    
}
