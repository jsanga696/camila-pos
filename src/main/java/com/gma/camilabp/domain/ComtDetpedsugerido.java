/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;
//import org.hibernate.annotations.Formula;

/**
 *
 * @author userdb5
 */
@Entity
@Table(name = "COMT_DETPEDSUGERIDO")
@NamedQueries({
    @NamedQuery(name = "ComtDetpedsugerido.findAll", query = "SELECT c FROM ComtDetpedsugerido c")})
public class ComtDetpedsugerido implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ComtDetpedsugeridoPK comtDetpedsugeridoPK;
    @Size(max = 15)
    @Column(name = "INVITM_codigo")
    private String iNVITMcodigo;
    @Column(name = "INVITM_invtarioCJ")
    private Integer iNVITMinvtarioCJ;
    @Column(name = "INVITM_invtarioUN")
    private Integer iNVITMinvtarioUN;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "INVITM_costoInventa")
    private BigDecimal iNVITMcostoInventa;
    @Column(name = "INVITM_DiasInventa")
    private BigDecimal iNVITMDiasInventa;
    @Column(name = "COMPDS_cantisugeridas")
    private BigDecimal cOMPDScantisugeridas;
    @Column(name = "COMPDS_cantirecibidas")
    private BigDecimal cOMPDScantirecibidas;
    @Column(name = "COMPDS_cantunidadsugeridas")
    private BigDecimal compdsCantunidadsugeridas;
    @Column(name = "COMPDS_cantunidadrecibidas")
    private BigDecimal compdsCantunidadrecibidas;
    @Column(name = "COMPDS_cantfuncionalsugeridas")
    private BigDecimal compdsCantfuncionalsugeridas;
    @Column(name = "COMPDS_cantfuncionalrecibidas")
    private BigDecimal compdsCantfuncionalrecibidas;
    @Column(name = "COMPDS_unidembalasugeridas")
    private BigDecimal compdsUnidembalasugeridas;
    @Column(name = "COMPDS_unidembalarecibidas")
    private BigDecimal compdsUnidembalarecibidas;
    @Column(name = "COMPDS_stockproveedor")
    private BigDecimal cOMPDSstockproveedor;
    //@Formula(value = "(SELECT i.INVITM_nombre FROM INVM_ITEM i WHERE i.INVITM_codigo=INVITM_codigo)")
    @Transient
    private String nombreItem;

    public ComtDetpedsugerido() {
    }

    public ComtDetpedsugerido(ComtDetpedsugeridoPK comtDetpedsugeridoPK) {
        this.comtDetpedsugeridoPK = comtDetpedsugeridoPK;
    }

    public ComtDetpedsugerido(Long cOMPDSsecuencia, int cOMPDSlinea) {
        this.comtDetpedsugeridoPK = new ComtDetpedsugeridoPK(cOMPDSsecuencia, cOMPDSlinea);
    }

    public ComtDetpedsugeridoPK getComtDetpedsugeridoPK() {
        return comtDetpedsugeridoPK;
    }

    public void setComtDetpedsugeridoPK(ComtDetpedsugeridoPK comtDetpedsugeridoPK) {
        this.comtDetpedsugeridoPK = comtDetpedsugeridoPK;
    }

    public String getINVITMcodigo() {
        return iNVITMcodigo;
    }

    public void setINVITMcodigo(String iNVITMcodigo) {
        this.iNVITMcodigo = iNVITMcodigo;
    }

    public Integer getINVITMinvtarioCJ() {
        return iNVITMinvtarioCJ;
    }

    public void setINVITMinvtarioCJ(Integer iNVITMinvtarioCJ) {
        this.iNVITMinvtarioCJ = iNVITMinvtarioCJ;
    }

    public Integer getINVITMinvtarioUN() {
        return iNVITMinvtarioUN;
    }

    public void setINVITMinvtarioUN(Integer iNVITMinvtarioUN) {
        this.iNVITMinvtarioUN = iNVITMinvtarioUN;
    }

    public BigDecimal getINVITMcostoInventa() {
        return iNVITMcostoInventa;
    }

    public void setINVITMcostoInventa(BigDecimal iNVITMcostoInventa) {
        this.iNVITMcostoInventa = iNVITMcostoInventa;
    }

    public BigDecimal getINVITMDiasInventa() {
        return iNVITMDiasInventa;
    }

    public void setINVITMDiasInventa(BigDecimal iNVITMDiasInventa) {
        this.iNVITMDiasInventa = iNVITMDiasInventa;
    }

    public BigDecimal getCOMPDScantisugeridas() {
        return cOMPDScantisugeridas;
    }

    public void setCOMPDScantisugeridas(BigDecimal cOMPDScantisugeridas) {
        this.cOMPDScantisugeridas = cOMPDScantisugeridas;
    }

    public BigDecimal getCOMPDScantirecibidas() {
        return cOMPDScantirecibidas;
    }

    public void setCOMPDScantirecibidas(BigDecimal cOMPDScantirecibidas) {
        this.cOMPDScantirecibidas = cOMPDScantirecibidas;
    }

    public BigDecimal getCompdsCantunidadsugeridas() {
        return compdsCantunidadsugeridas;
    }

    public void setCompdsCantunidadsugeridas(BigDecimal compdsCantunidadsugeridas) {
        this.compdsCantunidadsugeridas = compdsCantunidadsugeridas;
    }

    public BigDecimal getCompdsCantunidadrecibidas() {
        return compdsCantunidadrecibidas;
    }

    public void setCompdsCantunidadrecibidas(BigDecimal compdsCantunidadrecibidas) {
        this.compdsCantunidadrecibidas = compdsCantunidadrecibidas;
    }

    public BigDecimal getCompdsCantfuncionalsugeridas() {
        return compdsCantfuncionalsugeridas;
    }

    public void setCompdsCantfuncionalsugeridas(BigDecimal compdsCantfuncionalsugeridas) {
        this.compdsCantfuncionalsugeridas = compdsCantfuncionalsugeridas;
    }

    public BigDecimal getCompdsCantfuncionalrecibidas() {
        return compdsCantfuncionalrecibidas;
    }

    public void setCompdsCantfuncionalrecibidas(BigDecimal compdsCantfuncionalrecibidas) {
        this.compdsCantfuncionalrecibidas = compdsCantfuncionalrecibidas;
    }

    public BigDecimal getCompdsUnidembalasugeridas() {
        return compdsUnidembalasugeridas;
    }

    public void setCompdsUnidembalasugeridas(BigDecimal compdsUnidembalasugeridas) {
        this.compdsUnidembalasugeridas = compdsUnidembalasugeridas;
    }

    public BigDecimal getCompdsUnidembalarecibidas() {
        return compdsUnidembalarecibidas;
    }

    public void setCompdsUnidembalarecibidas(BigDecimal compdsUnidembalarecibidas) {
        this.compdsUnidembalarecibidas = compdsUnidembalarecibidas;
    }

    public BigDecimal getCOMPDSstockproveedor() {
        return cOMPDSstockproveedor;
    }

    public void setCOMPDSstockproveedor(BigDecimal cOMPDSstockproveedor) {
        this.cOMPDSstockproveedor = cOMPDSstockproveedor;
    }

    public String getNombreItem() {
        return nombreItem;
    }

    public void setNombreItem(String nombreItem) {
        this.nombreItem = nombreItem;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (comtDetpedsugeridoPK != null ? comtDetpedsugeridoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ComtDetpedsugerido)) {
            return false;
        }
        ComtDetpedsugerido other = (ComtDetpedsugerido) object;
        if ((this.comtDetpedsugeridoPK == null && other.comtDetpedsugeridoPK != null) || (this.comtDetpedsugeridoPK != null && !this.comtDetpedsugeridoPK.equals(other.comtDetpedsugeridoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.ComtDetpedsugerido[ comtDetpedsugeridoPK=" + comtDetpedsugeridoPK + " ]";
    }
    
}
