/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb
 */
@Entity
@Table(name = "CBPSEGM_MODULO")
@NamedQueries({
    @NamedQuery(name = "CbpsegmModulo.findAll", query = "SELECT c FROM CbpsegmModulo c")})
public class CbpsegmModulo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "segmod_codigo")
    private Long segmodCodigo;
    @Basic(optional = false)
    @Size(min = 1, max = 100)
    @Column(name = "segmod_descripcio")
    private String segmodDescripcio;
    @Basic(optional = false)
    @Column(name = "segmod_numregis")
    private short segmodNumregis;
    @Basic(optional = false)
    @NotNull
    @Column(name = "segmod_estado")
    private boolean segmodEstado= true;
    @Basic(optional = false)
    @Column(name = "segmod_nlicencias")
    private short segmodNlicencias=1;
    @Basic(optional = false)
    @NotNull
    @Column(name = "segmod_creafecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date segmodCreafecha= new Date();
    @Basic(optional = false)
    @NotNull
    @Column(name = "segmod_creauser")
    private long segmodCreauser;
    @JoinColumn(name = "segpaq_codigo", referencedColumnName = "segpaq_codigo")
    @ManyToOne(optional = false)
    private CbpsegmPaquete segpaqCodigo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "segmodCodigo")
    private Collection<CbpsegmOpcion> cbpsegmOpcionCollection;

    public CbpsegmModulo() {
    }

    public CbpsegmModulo(Long segmodCodigo) {
        this.segmodCodigo = segmodCodigo;
    }

    public CbpsegmModulo(Long segmodCodigo, String segmodDescripcio, short segmodNumregis, boolean segmodEstado, short segmodNlicencias, Date segmodCreafecha, long segmodCreauser) {
        this.segmodCodigo = segmodCodigo;
        this.segmodDescripcio = segmodDescripcio;
        this.segmodNumregis = segmodNumregis;
        this.segmodEstado = segmodEstado;
        this.segmodNlicencias = segmodNlicencias;
        this.segmodCreafecha = segmodCreafecha;
        this.segmodCreauser = segmodCreauser;
    }

    public Long getSegmodCodigo() {
        return segmodCodigo;
    }

    public void setSegmodCodigo(Long segmodCodigo) {
        this.segmodCodigo = segmodCodigo;
    }

    public String getSegmodDescripcio() {
        return segmodDescripcio;
    }

    public void setSegmodDescripcio(String segmodDescripcio) {
        this.segmodDescripcio = segmodDescripcio;
    }

    public short getSegmodNumregis() {
        return segmodNumregis;
    }

    public void setSegmodNumregis(short segmodNumregis) {
        this.segmodNumregis = segmodNumregis;
    }

    public boolean getSegmodEstado() {
        return segmodEstado;
    }

    public void setSegmodEstado(boolean segmodEstado) {
        this.segmodEstado = segmodEstado;
    }

    public short getSegmodNlicencias() {
        return segmodNlicencias;
    }

    public void setSegmodNlicencias(short segmodNlicencias) {
        this.segmodNlicencias = segmodNlicencias;
    }

    public Date getSegmodCreafecha() {
        return segmodCreafecha;
    }

    public void setSegmodCreafecha(Date segmodCreafecha) {
        this.segmodCreafecha = segmodCreafecha;
    }

    public long getSegmodCreauser() {
        return segmodCreauser;
    }

    public void setSegmodCreauser(long segmodCreauser) {
        this.segmodCreauser = segmodCreauser;
    }

    public CbpsegmPaquete getSegpaqCodigo() {
        return segpaqCodigo;
    }

    public void setSegpaqCodigo(CbpsegmPaquete segpaqCodigo) {
        this.segpaqCodigo = segpaqCodigo;
    }

    public Collection<CbpsegmOpcion> getCbpsegmOpcionCollection() {
        return cbpsegmOpcionCollection;
    }

    public void setCbpsegmOpcionCollection(Collection<CbpsegmOpcion> cbpsegmOpcionCollection) {
        this.cbpsegmOpcionCollection = cbpsegmOpcionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (segmodCodigo != null ? segmodCodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbpsegmModulo)) {
            return false;
        }
        CbpsegmModulo other = (CbpsegmModulo) object;
        if ((this.segmodCodigo == null && other.segmodCodigo != null) || (this.segmodCodigo != null && !this.segmodCodigo.equals(other.segmodCodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CbpsegmModulo[ segmodCodigo=" + segmodCodigo + " ]";
    }
    
}
