/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb
 */
@Entity
@Table(name = "CBPSEGM_OPCION")
@NamedQueries({
    @NamedQuery(name = "CbpsegmOpcion.findAll", query = "SELECT c FROM CbpsegmOpcion c")})
public class CbpsegmOpcion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "segopc_codigo")
    private Long segopcCodigo;
    @Basic(optional = false)
    @Size(min = 1, max = 50)
    @Column(name = "segopc_secuencia")
    private String segopcSecuencia;    
    @JoinColumn(name = "segpaq_codigo", referencedColumnName = "segpaq_codigo")
    @ManyToOne(optional = false)
    private CbpsegmPaquete segpaqCodigo;
    @Basic(optional = false)
    @Size(min = 1, max = 100)
    @Column(name = "segopc_descripcio")
    private String segopcDescripcio;
    @Size(max = 20)
    @Column(name = "segopc_ejecutable")
    private String segopcEjecutable;
    @Basic(optional = false)
    @NotNull
    @Column(name = "segopc_estado")
    private boolean segopcEstado=true;
    @Basic(optional = false)
    @Size(min = 1, max = 500)
    @Column(name = "segopc_url")
    private String segopcUrl;
    @Basic(optional = false)
    @NotNull
    @Column(name = "segpoc_creafecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date segpocCreafecha= new Date();
    @Basic(optional = false)
    @NotNull
    @Column(name = "segopc_creauser")
    private long segopcCreauser;
    @Column(name = "segopc_menu")
    private Boolean segopcMenu=true;
    @Column(name = "segopc_acceso")
    private Boolean segopcAcceso=true;
    @Column(name = "segopc_turno")
    private Boolean segopcTurno=true;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "segopcCodigo")
    private Collection<CbpsegmPerfil> cbpsegmPerfilCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "segopcCodigo")
    private Collection<CbpsegmPerfilrestricion> cbpsegmPerfilrestricionCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "segopcCodigo")
    private Collection<CbpsegmAccion> cbpsegmAccionCollection;
    @JoinColumn(name = "segfun_codigo", referencedColumnName = "segfun_codigo")
    @ManyToOne(optional = false)
    private CbpsegmFuncion segfunCodigo;
    @JoinColumn(name = "segmod_codigo", referencedColumnName = "segmod_codigo")
    @ManyToOne(optional = false)
    private CbpsegmModulo segmodCodigo;

    public CbpsegmOpcion() {
    }

    public CbpsegmOpcion(Long segopcCodigo) {
        this.segopcCodigo = segopcCodigo;
    }

    public CbpsegmOpcion(Long segopcCodigo, String segopcSecuencia, String segopcDescripcio, boolean segopcEstado, String segopcUrl, Date segpocCreafecha, long segopcCreauser) {
        this.segopcCodigo = segopcCodigo;
        this.segopcSecuencia = segopcSecuencia;
        this.segopcDescripcio = segopcDescripcio;
        this.segopcEstado = segopcEstado;
        this.segopcUrl = segopcUrl;
        this.segpocCreafecha = segpocCreafecha;
        this.segopcCreauser = segopcCreauser;
    }

    public Long getSegopcCodigo() {
        return segopcCodigo;
    }

    public void setSegopcCodigo(Long segopcCodigo) {
        this.segopcCodigo = segopcCodigo;
    }

    public String getSegopcSecuencia() {
        return segopcSecuencia;
    }

    public void setSegopcSecuencia(String segopcSecuencia) {
        this.segopcSecuencia = segopcSecuencia;
    }

    public CbpsegmPaquete getSegpaqCodigo() {
        return segpaqCodigo;
    }

    public void setSegpaqCodigo(CbpsegmPaquete segpaqCodigo) {
        this.segpaqCodigo = segpaqCodigo;
    }

    public String getSegopcDescripcio() {
        return segopcDescripcio;
    }

    public void setSegopcDescripcio(String segopcDescripcio) {
        this.segopcDescripcio = segopcDescripcio;
    }

    public String getSegopcEjecutable() {
        return segopcEjecutable;
    }

    public void setSegopcEjecutable(String segopcEjecutable) {
        this.segopcEjecutable = segopcEjecutable;
    }

    public boolean getSegopcEstado() {
        return segopcEstado;
    }

    public void setSegopcEstado(boolean segopcEstado) {
        this.segopcEstado = segopcEstado;
    }

    public String getSegopcUrl() {
        return segopcUrl;
    }

    public void setSegopcUrl(String segopcUrl) {
        this.segopcUrl = segopcUrl;
    }

    public Date getSegpocCreafecha() {
        return segpocCreafecha;
    }

    public void setSegpocCreafecha(Date segpocCreafecha) {
        this.segpocCreafecha = segpocCreafecha;
    }

    public long getSegopcCreauser() {
        return segopcCreauser;
    }

    public void setSegopcCreauser(long segopcCreauser) {
        this.segopcCreauser = segopcCreauser;
    }

    public Collection<CbpsegmPerfil> getCbpsegmPerfilCollection() {
        return cbpsegmPerfilCollection;
    }

    public void setCbpsegmPerfilCollection(Collection<CbpsegmPerfil> cbpsegmPerfilCollection) {
        this.cbpsegmPerfilCollection = cbpsegmPerfilCollection;
    }

    public Collection<CbpsegmPerfilrestricion> getCbpsegmPerfilrestricionCollection() {
        return cbpsegmPerfilrestricionCollection;
    }

    public void setCbpsegmPerfilrestricionCollection(Collection<CbpsegmPerfilrestricion> cbpsegmPerfilrestricionCollection) {
        this.cbpsegmPerfilrestricionCollection = cbpsegmPerfilrestricionCollection;
    }

    public Collection<CbpsegmAccion> getCbpsegmAccionCollection() {
        return cbpsegmAccionCollection;
    }

    public void setCbpsegmAccionCollection(Collection<CbpsegmAccion> cbpsegmAccionCollection) {
        this.cbpsegmAccionCollection = cbpsegmAccionCollection;
    }

    public CbpsegmFuncion getSegfunCodigo() {
        return segfunCodigo;
    }

    public void setSegfunCodigo(CbpsegmFuncion segfunCodigo) {
        this.segfunCodigo = segfunCodigo;
    }

    public CbpsegmModulo getSegmodCodigo() {
        return segmodCodigo;
    }

    public void setSegmodCodigo(CbpsegmModulo segmodCodigo) {
        this.segmodCodigo = segmodCodigo;
    }

    public Boolean getSegopcMenu() {
        return segopcMenu;
    }

    public void setSegopcMenu(Boolean segopcMenu) {
        this.segopcMenu = segopcMenu;
    }

    public Boolean getSegopcAcceso() {
        return segopcAcceso;
    }

    public void setSegopcAcceso(Boolean segopcAcceso) {
        this.segopcAcceso = segopcAcceso;
    }

    public Boolean getSegopcTurno() {
        return segopcTurno;
    }

    public void setSegopcTurno(Boolean segopcTurno) {
        this.segopcTurno = segopcTurno;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (segopcCodigo != null ? segopcCodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbpsegmOpcion)) {
            return false;
        }
        CbpsegmOpcion other = (CbpsegmOpcion) object;
        if ((this.segopcCodigo == null && other.segopcCodigo != null) || (this.segopcCodigo != null && !this.segopcCodigo.equals(other.segopcCodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CbpsegmOpcion[ segopcCodigo=" + segopcCodigo + " ]";
    }
    
}
