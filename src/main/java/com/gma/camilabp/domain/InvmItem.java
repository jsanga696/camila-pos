/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import com.gma.camilabp.model.logica.ItemCantidades;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb6
 */
@Entity
@Table(name = "INVM_ITEM")
@NamedQueries({
    @NamedQuery(name = "InvmItem.findAll", query = "SELECT i FROM InvmItem i")})
public class InvmItem implements Serializable {
    
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected InvmItemPK invmItemPK;
    @Size(max = 15)
    @Column(name = "INVIN1_codin1item")
    private String iNVIN1codin1item;
    @Size(max = 50)
    @Column(name = "INVITM_nombre")
    private String iNVITMnombre;
    @Size(max = 15)
    @Column(name = "INVIN2_codin2item")
    private String iNVIN2codin2item;
    @Size(max = 50)
    @Column(name = "INVITM_nombrecor")
    private String iNVITMnombrecor;
    @Size(max = 15)
    @Column(name = "INVIN3_codin3item")
    private String iNVIN3codin3item;
    @Size(max = 15)
    @Column(name = "INVIN4_codin4item")
    private String iNVIN4codin4item;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "INVITM_porceutili")
    private BigDecimal iNVITMporceutili= new BigDecimal("30");
    @Column(name = "INVITM_porcutimin")
    private BigDecimal iNVITMporcutimin= new BigDecimal("10");
    @Size(max = 2)
    @Column(name = "INVITM_grupodscto")
    private String iNVITMgrupodscto="";
    @Column(name = "INVITM_peso")
    private BigDecimal iNVITMpeso= BigDecimal.ONE;
    @Column(name = "INVITM_volumen")
    private BigDecimal iNVITMvolumen= BigDecimal.ONE;
    @Column(name = "INVITM_unidembala")
    private BigDecimal iNVITMunidembala= BigDecimal.ONE;
    @Size(max = 10)
    @Column(name = "INVUNI_unidadgrup")
    private String iNVUNIunidadgrup="CJ";
    @Size(max = 10)
    @Column(name = "INVUNI_unidadexis")
    private String iNVUNIunidadexis="UN";
    @Column(name = "INVUNI_unidxpalle")
    private BigDecimal iNVUNIunidxpalle;
    @Column(name = "INVITM_stock")
    private BigDecimal iNVITMstock= BigDecimal.ZERO;
    @Column(name = "INVITM_stkpending")
    private BigDecimal iNVITMstkpending= BigDecimal.ZERO;
    @Column(name = "INVITM_stkreserva")
    private BigDecimal iNVITMstkreserva= BigDecimal.ZERO;
    @Column(name = "INVITM_stktransit")
    private BigDecimal iNVITMstktransit= BigDecimal.ZERO;
    @Column(name = "INVITM_stkminimo")
    private BigDecimal iNVITMstkminimo= BigDecimal.ZERO;
    @Column(name = "INVITM_preciopvp")
    private BigDecimal iNVITMpreciopvp= BigDecimal.ZERO;    
    @Column(name = "INVITM_stkmaximo")
    private BigDecimal iNVITMstkmaximo= BigDecimal.ZERO;
    @Column(name = "INVITM_costoprom1")
    private BigDecimal iNVITMcostoprom1= BigDecimal.ZERO;
    @Column(name = "INVITM_precilista")
    private BigDecimal iNVITMprecilista= BigDecimal.ZERO;
    @Column(name = "INVITM_costoprom2")
    private BigDecimal iNVITMcostoprom2= BigDecimal.ZERO;
    @Column(name = "INVITM_costomerc1")
    private BigDecimal iNVITMcostomerc1= BigDecimal.ZERO;
    @Column(name = "INVITM_costomerc2")
    private BigDecimal iNVITMcostomerc2= BigDecimal.ZERO;
    @Column(name = "INVITM_fechaingre")
    @Temporal(TemporalType.TIMESTAMP)
    private Date iNVITMfechaingre= new Date();
    @Column(name = "INVITM_fechabaja")
    @Temporal(TemporalType.TIMESTAMP)
    private Date iNVITMfechabaja;
    @Column(name = "INVITM_fecultvent")
    @Temporal(TemporalType.TIMESTAMP)
    private Date iNVITMfecultvent;    
    @Size(max = 50)
    @Column(name = "INVITM_observacio")
    private String iNVITMobservacio="";
    @Column(name = "INVITM_fecultcomp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date iNVITMfecultcomp;
    @Column(name = "INVITM_esttipo")
    private Character iNVITMesttipo='T';
    @Column(name = "INVITM_estactivo")
    private Character iNVITMestactivo='A';
    //INVTIM_tipoitem,INVITM_estretorna,INVITM_estensamb,INVITM_estproduce,INVITM_estafecinv,INVITM_estdispvta,INVITM_estvtafrac,INVITM_estmovimie,INVITM_estadolote,INVITM_estadserie
    @Size(max = 3)
    @Column(name = "INVTIM_tipoitem")
    private String iNVTIMtipoitem="O";
    @Column(name = "INVITM_estretorna")
    private Character iNVITMestretorna='N';
    @Column(name = "INVITM_estensamb")
    private Character iNVITMestensamb='N';
    @Column(name = "INVITM_estproduce")
    private Character iNVITMestproduce='N';
    @Column(name = "INVITM_estafecinv")
    private Character iNVITMestafecinv='S';
    @Column(name = "INVITM_estdispvta")
    private Character iNVITMestdispvta='S';
    @Column(name = "INVITM_estvtafrac")
    private Character iNVITMestvtafrac='S';
    @Column(name = "INVITM_estmovimie")
    private Character iNVITMestmovimie='U';
    @Size(max = 2)
    @Column(name = "INVITM_codescalon")
    private String iNVITMcodescalon;
    @Column(name = "INVITM_estadolote")
    private Character iNVITMestadolote='N';
    @Column(name = "INVITM_estadserie")
    private Character iNVITMestadserie='N';    
    @Column(name = "INVITM_estadescar")
    private Character iNVITMestadescar='N';
    @Column(name = "INVITM_estactpubl")
    private Character iNVITMestactpubl='N';
    @Size(max = 13)
    @Column(name = "INVITM_ctadescom")
    private String iNVITMctadescom;
    @Size(max = 13)
    @Column(name = "INVITM_ctadevcom")
    private String iNVITMctadevcom;
    @Size(max = 13)
    @Column(name = "INVITM_ctavta")
    private String iNVITMctavta;
    @Size(max = 13)
    @Column(name = "INVITM_ctacosvta")
    private String iNVITMctacosvta;
    @Size(max = 13)
    @Column(name = "INVITM_ctadesvta")
    private String iNVITMctadesvta;
    @Size(max = 13)
    @Column(name = "INVITM_ctadevvta")
    private String iNVITMctadevvta;
    @Size(max = 13)
    @Column(name = "INVITM_ctainventa")
    private String iNVITMctainventa;
    @Size(max = 25)
    @Column(name = "INVITM_codigbarra")
    private String iNVITMcodigbarra;
    @Size(max = 8)
    @Column(name = "INVITM_tipomaterial")
    private String iNVITMtipomaterial;
    @Size(max = 8)
    @Column(name = "INVITM_grupomater")
    private String iNVITMgrupomater;
    @Column(name = "INVITM_abc")
    private Character iNVITMabc;
    @Column(name = "INVITM_valorabc")
    private BigDecimal iNVITMvalorabc;
    @Column(name = "INVITM_cantiabc")
    private BigDecimal iNVITMcantiabc;
    @Column(name = "INVITM_genautolot")
    private Character iNVITMgenautolot;
    @Column(name = "INVITM_batchproduc")
    private BigDecimal iNVITMbatchproduc;
    @Size(max = 15)
    @Column(name = "INVITM_codreceta")
    private String iNVITMcodreceta;
    @Size(max = 15)
    @Column(name = "INVITM_codplancom")
    private String iNVITMcodplancom;
    @Column(name = "INVITM_leatimacum")
    private BigDecimal iNVITMleatimacum;
    @Column(name = "INVITM_dcontcicli")
    private BigDecimal iNVITMdcontcicli;
    @Column(name = "INVITM_ltimeinspe")
    private BigDecimal iNVITMltimeinspe;
    @Column(name = "INVITM_inspereque")
    private Character iNVITMinspereque;
    @Size(max = 4)
    @Column(name = "INVITM_sitioconsum")
    private String iNVITMsitioconsum;
    @Size(max = 4)
    @Column(name = "INVITM_locaconsum")
    private String iNVITMlocaconsum;
    @Column(name = "INVITM_ltimemanuf")
    private BigDecimal iNVITMltimemanuf;
    @Column(name = "INVITM_estamrp")
    private Character iNVITMestamrp;
    @Column(name = "INVITM_estamps")
    private Character iNVITMestamps;
    @Size(max = 15)
    @Column(name = "INVITM_coddrp")
    private String iNVITMcoddrp;
    @Column(name = "INVITM_miniorden")
    private BigDecimal iNVITMminiorden;
    @Column(name = "INVITM_maxiorden")
    private BigDecimal iNVITMmaxiorden;
    @Column(name = "INVITM_multiorden")
    private BigDecimal iNVITMmultiorden;
    @Column(name = "INVITM_periodorde")
    private BigDecimal iNVITMperiodorde;
    @Size(max = 8)
    @Column(name = "INVITM_politicainv")
    private String iNVITMpoliticainv;
    @Column(name = "INVITM_cantiorden")
    private BigDecimal iNVITMcantiorden;
    @Column(name = "INVITM_estfantas")
    private Character iNVITMestfantas;
    @Column(name = "INVITM_estorden")
    private Character iNVITMestorden;
    @Column(name = "INVITM_tratamient")
    private Character iNVITMtratamient;
    @Size(max = 4)
    @Column(name = "INVITM_oficicompra")
    private String iNVITMoficicompra;
    @Column(name = "INVITM_ltimecompra")
    private BigDecimal iNVITMltimecompra;
    @Size(max = 8)
    @Column(name = "INVITM_estrecepoc")
    private String iNVITMestrecepoc;
    @Size(max = 15)
    @Column(name = "INVITM_codruta")
    private String iNVITMcodruta;
    @Column(name = "INVITM_stocksegu")
    private BigDecimal iNVITMstocksegu;
    @Column(name = "INVITM_tiemposegu")
    private BigDecimal iNVITMtiemposegu;
    @Column(name = "INVITM_tiempovida")
    private BigDecimal iNVITMtiempovida;
    @Size(max = 4)
    @Column(name = "INVITM_codprovee")
    private String iNVITMcodprovee;
    @Column(name = "INVITM_tiempcober")
    private BigDecimal iNVITMtiempcober;
    @Size(max = 4)
    @Column(name = "INVITM_codbodega")
    private String iNVITMcodbodega;
    @Size(max = 8)
    @Column(name = "INVITM_lineproduc")
    private String iNVITMlineproduc;
    @Column(name = "INVITM_fecultmodi")
    @Temporal(TemporalType.TIMESTAMP)
    private Date iNVITMfecultmodi;    
    @Column(name = "INVITM_estmovfrac")
    private Character iNVITMestmovfrac='S';
    @Size(max = 10)
    @Column(name = "INVITM_unimedvol")
    private String iNVITMunimedvol="CM3";
    @Size(max = 3)
    @Column(name = "INVITM_tipocombo")
    private String iNVITMtipocombo="";
    @Size(max = 18)
    @Column(name = "INVITM_codrefprov")
    private String iNVITMcodrefprov;
    @Size(max = 20)
    @Column(name = "INVITM_ctacontcxc")
    private String iNVITMctacontcxc;
    @Column(name = "INVITM_estcombprop")
    private Character iNVITMestcombprop;
    @Size(max = 255)
    @Column(name = "item_aux")
    private String itemAux;
    @Size(max = 255)
    @Column(name = "item_aux2")
    private String itemAux2;
    @Size(max = 20)
    @Column(name = "INVITM_ctadesvtapro")
    private String iNVITMctadesvtapro;
    @Size(max = 20)
    @Column(name = "invitm_ctadesvtacort")
    private String invitmCtadesvtacort;
    @Size(max = 20)
    @Column(name = "invitm_ctadesvtaadic")
    private String invitmCtadesvtaadic;
    @Column(name = "INVITM_estcompe")
    private Character iNVITMestcompe='N';
    @Column(name = "INVITM_fechamodif")
    @Temporal(TemporalType.TIMESTAMP)
    private Date iNVITMfechamodif;
    @Size(max = 15)
    @Column(name = "INVITM_codigoenv")
    private String iNVITMcodigoenv;
    @Size(max = 15)
    @Column(name = "INVITM_codigochanc")
    private String iNVITMcodigochanc;
    @Column(name = "INVITM_estcervecero")
    private Character iNVITMestcervecero;
    @Size(max = 2)
    @Column(name = "RTKMEP_codigo")
    private String rTKMEPcodigo;
    @Size(max = 2)
    @Column(name = "RTKGRE_codigo")
    private String rTKGREcodigo;    
    @Column(name = "INVITM_politidesc")
    private Character iNVITMpolitidesc='3';
    @Size(max = 6)
    @Column(name = "INVITM_codin5item")
    private String iNVITMcodin5item;
    @Size(max = 6)
    @Column(name = "STKNIV_codigo")
    private String sTKNIVcodigo;
    @Size(max = 250)
    @Column(name = "invitm_compania")
    private String invitmCompania;
    @Size(max = 1)
    @Column(name = "INVITM_asumereten")
    private String iNVITMasumereten="N";
    @Column(name = "INVITM_idcodigo")
    private Integer iNVITMidcodigo;
    @Size(max = 1)
    @Column(name = "INVITM_manedecimales")
    private String iNVITMmanedecimales="N";
    @Size(max = 15)
    @Column(name = "INVITM_codigoAlice")
    private String iNVITMcodigoAlice;
    @Size(max = 15)
    @Column(name = "INVIN1_codin1itemTMP")
    private String iNVIN1codin1itemTMP;
    @Size(max = 15)
    @Column(name = "INVIN2_codin2itemTMP")
    private String iNVIN2codin2itemTMP;
    @Size(max = 15)
    @Column(name = "INVIN3_codin3itemTMP")
    private String iNVIN3codin3itemTMP;
    @Size(max = 15)
    @Column(name = "INVIN4_codin4itemTMP")
    private String iNVIN4codin4itemTMP;
    @Size(max = 15)
    @Column(name = "INVIN5_codin5itemTMP")
    private String iNVIN5codin5itemTMP;
    @Size(max = 1)
    @Column(name = "INVITM_manedisplay")
    private String iNVITMmanedisplay="N";
    @Size(max = 3)
    @Column(name = "GRPPRO_codigo")
    private String gRPPROcodigo;
    @Column(name = "INVITM_impplastico")
    private Character iNVITMimpplastico='N';
    @Column(name = "INVITM_costomanobra")
    private BigDecimal iNVITMcostomanobra;
    @Column(name = "INVITM_costofabrica")
    private BigDecimal iNVITMcostofabrica;
    @Size(max = 10)
    @Column(name = "INVIN5_codin5item")
    private String iNVIN5codin5item;
    @Size(max = 2)
    @Column(name = "INVITM_tipolistaprecio")
    private String iNVITMtipolistaprecio="LC";
    @Column(name = "INVITM_uniAsignada")
    private Integer iNVITMuniAsignada=0;
    @Size(max = 1)
    @Column(name = "INVITM_estfecha")
    private String iNVITMestfecha="N";
    @Size(max = 10)
    @Column(name = "INVITM_unidmedbase")
    private String iNVITMunidmedbase="CM3";
    @Size(max = 1)
    @Column(name = "INVITM_estamultunid")
    private String iNVITMestamultunid="N";
    @Column(name = "INVITM_desctoproduc")
    private BigDecimal iNVITMdesctoproduc;
    @Size(max = 1)
    @Column(name = "INVITM_estacombo")
    private String iNVITMestacombo;
    @Size(max = 1)
    @Column(name = "INVITM_estaPromocion")
    private String iNVITMestaPromocion;
    @Size(max = 1)
    @Column(name = "INVITM_SubeEquipo")
    private String iNVITMSubeEquipo;
    @Size(max = 1)
    @Column(name = "INVITM_impdetcompo")
    private String iNVITMimpdetcompo;
    @Size(max = 1)
    @Column(name = "INVITM_compopedido")
    private String iNVITMcompopedido;
    @Column(name = "INVITM_uniXDisplay")
    private BigDecimal iNVITMuniXDisplay;
    @Size(max = 1)
    @Column(name = "INVITM_estadoICE")
    private String iNVITMestadoICE;
    @Transient
    private BigDecimal valorImpuesto;
    @Transient
    private BigDecimal cajas;
    @Transient
    private BigDecimal unidad;
    @Transient
    private String codigoItem;
    @Transient
    private BigDecimal cantidadFuncional = BigDecimal.ZERO;
    @Transient
    private BigDecimal costo;
    @Transient
    private BigDecimal subtotalCosto;    
    @Transient
    private BigDecimal valorConImpuesto;
    @Transient
    private String codigoBarra;
    @Transient
    private BigDecimal disponible= BigDecimal.ZERO;    
    @Transient
    private BigDecimal costoCaja;
    @Transient
    private BigDecimal costoItem;    
    @Transient
    private BigDecimal costoCompra= BigDecimal.ZERO;
    @Transient
    private BigDecimal porcDesc;    
    @Transient
    private BigDecimal descuento;
    @Transient
    private BigDecimal impPlastico;
    @Transient
    private Long grupo=0L;
    @Transient
    private Boolean iva=Boolean.TRUE;
    @Transient
    private Boolean itemSugerido = Boolean.FALSE;
    @Transient
    private BigDecimal porcentajeIce= BigDecimal.ZERO;
    @Transient
    private BigDecimal ice= BigDecimal.ZERO;
    
    public InvmItem() {
        cajas = BigDecimal.ZERO;
        unidad = BigDecimal.ZERO;
        descuento = BigDecimal.ZERO;
        porcDesc = BigDecimal.ZERO;
        this.invmItemPK = new InvmItemPK();
    }

    public InvmItem(InvmItemPK invmItemPK) {
        this.invmItemPK = invmItemPK;
    }

    public InvmItem(String gENCIAcodigo, String iNVITMcodigo) {
        this.invmItemPK = new InvmItemPK(gENCIAcodigo, iNVITMcodigo);
    }

    public InvmItemPK getInvmItemPK() {
        return invmItemPK;
    }

    public BigDecimal getValorConImpuesto() {
        return valorConImpuesto;
    }

    public void setValorConImpuesto(BigDecimal valorConImpuesto) {
        this.valorConImpuesto = valorConImpuesto;
    }

    public void setInvmItemPK(InvmItemPK invmItemPK) {
        this.invmItemPK = invmItemPK;
    }

    public String getINVIN1codin1item() {
        return iNVIN1codin1item;
    }

    public void setINVIN1codin1item(String iNVIN1codin1item) {
        this.iNVIN1codin1item = iNVIN1codin1item;
    }

    public String getINVITMnombre() {
        return iNVITMnombre;
    }

    public void setINVITMnombre(String iNVITMnombre) {
        this.iNVITMnombre = iNVITMnombre;
    }

    public String getINVIN2codin2item() {
        return iNVIN2codin2item;
    }

    public void setINVIN2codin2item(String iNVIN2codin2item) {
        this.iNVIN2codin2item = iNVIN2codin2item;
    }

    public String getINVITMnombrecor() {
        return iNVITMnombrecor;
    }

    public void setINVITMnombrecor(String iNVITMnombrecor) {
        this.iNVITMnombrecor = iNVITMnombrecor;
    }

    public String getINVIN3codin3item() {
        return iNVIN3codin3item;
    }

    public void setINVIN3codin3item(String iNVIN3codin3item) {
        this.iNVIN3codin3item = iNVIN3codin3item;
    }

    public String getINVIN4codin4item() {
        return iNVIN4codin4item;
    }

    public void setINVIN4codin4item(String iNVIN4codin4item) {
        this.iNVIN4codin4item = iNVIN4codin4item;
    }

    public BigDecimal getINVITMporceutili() {
        return iNVITMporceutili;
    }

    public void setINVITMporceutili(BigDecimal iNVITMporceutili) {
        this.iNVITMporceutili = iNVITMporceutili;
    }

    public BigDecimal getINVITMporcutimin() {
        return iNVITMporcutimin;
    }

    public void setINVITMporcutimin(BigDecimal iNVITMporcutimin) {
        this.iNVITMporcutimin = iNVITMporcutimin;
    }

    public String getINVITMgrupodscto() {
        return iNVITMgrupodscto;
    }

    public void setINVITMgrupodscto(String iNVITMgrupodscto) {
        this.iNVITMgrupodscto = iNVITMgrupodscto;
    }

    public BigDecimal getINVITMpeso() {
        return iNVITMpeso;
    }

    public void setINVITMpeso(BigDecimal iNVITMpeso) {
        this.iNVITMpeso = iNVITMpeso;
    }

    public BigDecimal getINVITMvolumen() {
        return iNVITMvolumen;
    }

    public void setINVITMvolumen(BigDecimal iNVITMvolumen) {
        this.iNVITMvolumen = iNVITMvolumen;
    }

    public BigDecimal getINVITMunidembala() {
        return iNVITMunidembala;
    }

    public void setINVITMunidembala(BigDecimal iNVITMunidembala) {
        this.iNVITMunidembala = iNVITMunidembala;
    }

    public String getINVUNIunidadgrup() {
        return iNVUNIunidadgrup;
    }

    public void setINVUNIunidadgrup(String iNVUNIunidadgrup) {
        this.iNVUNIunidadgrup = iNVUNIunidadgrup;
    }

    public String getINVUNIunidadexis() {
        return iNVUNIunidadexis;
    }

    public void setINVUNIunidadexis(String iNVUNIunidadexis) {
        this.iNVUNIunidadexis = iNVUNIunidadexis;
    }

    public BigDecimal getINVUNIunidxpalle() {
        return iNVUNIunidxpalle;
    }

    public void setINVUNIunidxpalle(BigDecimal iNVUNIunidxpalle) {
        this.iNVUNIunidxpalle = iNVUNIunidxpalle;
    }

    public BigDecimal getINVITMstock() {
        return iNVITMstock;
    }

    public void setINVITMstock(BigDecimal iNVITMstock) {
        this.iNVITMstock = iNVITMstock;
    }

    public BigDecimal getINVITMstkpending() {
        return iNVITMstkpending;
    }

    public void setINVITMstkpending(BigDecimal iNVITMstkpending) {
        this.iNVITMstkpending = iNVITMstkpending;
    }

    public BigDecimal getINVITMstkreserva() {
        return iNVITMstkreserva;
    }

    public void setINVITMstkreserva(BigDecimal iNVITMstkreserva) {
        this.iNVITMstkreserva = iNVITMstkreserva;
    }

    public BigDecimal getINVITMstktransit() {
        return iNVITMstktransit;
    }

    public void setINVITMstktransit(BigDecimal iNVITMstktransit) {
        this.iNVITMstktransit = iNVITMstktransit;
    }

    public BigDecimal getINVITMstkminimo() {
        return iNVITMstkminimo;
    }

    public void setINVITMstkminimo(BigDecimal iNVITMstkminimo) {
        this.iNVITMstkminimo = iNVITMstkminimo;
    }

    public BigDecimal getINVITMpreciopvp() {
        return iNVITMpreciopvp;
    }

    public void setINVITMpreciopvp(BigDecimal iNVITMpreciopvp) {
        this.iNVITMpreciopvp = iNVITMpreciopvp;
    }

    public BigDecimal getINVITMstkmaximo() {
        return iNVITMstkmaximo;
    }

    public void setINVITMstkmaximo(BigDecimal iNVITMstkmaximo) {
        this.iNVITMstkmaximo = iNVITMstkmaximo;
    }

    public BigDecimal getINVITMcostoprom1() {
        return iNVITMcostoprom1;
    }

    public void setINVITMcostoprom1(BigDecimal iNVITMcostoprom1) {
        this.iNVITMcostoprom1 = iNVITMcostoprom1;
    }

    public BigDecimal getINVITMprecilista() {
        return iNVITMprecilista;
    }

    public void setINVITMprecilista(BigDecimal iNVITMprecilista) {
        this.iNVITMprecilista = iNVITMprecilista;
    }

    public BigDecimal getINVITMcostoprom2() {
        return iNVITMcostoprom2;
    }

    public void setINVITMcostoprom2(BigDecimal iNVITMcostoprom2) {
        this.iNVITMcostoprom2 = iNVITMcostoprom2;
    }

    public BigDecimal getINVITMcostomerc1() {
        return iNVITMcostomerc1;
    }

    public void setINVITMcostomerc1(BigDecimal iNVITMcostomerc1) {
        this.iNVITMcostomerc1 = iNVITMcostomerc1;
    }

    public BigDecimal getINVITMcostomerc2() {
        return iNVITMcostomerc2;
    }

    public void setINVITMcostomerc2(BigDecimal iNVITMcostomerc2) {
        this.iNVITMcostomerc2 = iNVITMcostomerc2;
    }

    public Date getINVITMfechaingre() {
        return iNVITMfechaingre;
    }

    public void setINVITMfechaingre(Date iNVITMfechaingre) {
        this.iNVITMfechaingre = iNVITMfechaingre;
    }

    public Date getINVITMfechabaja() {
        return iNVITMfechabaja;
    }

    public void setINVITMfechabaja(Date iNVITMfechabaja) {
        this.iNVITMfechabaja = iNVITMfechabaja;
    }

    public Date getINVITMfecultvent() {
        return iNVITMfecultvent;
    }

    public void setINVITMfecultvent(Date iNVITMfecultvent) {
        this.iNVITMfecultvent = iNVITMfecultvent;
    }

    public String getINVITMobservacio() {
        return iNVITMobservacio;
    }

    public void setINVITMobservacio(String iNVITMobservacio) {
        this.iNVITMobservacio = iNVITMobservacio;
    }

    public Date getINVITMfecultcomp() {
        return iNVITMfecultcomp;
    }

    public void setINVITMfecultcomp(Date iNVITMfecultcomp) {
        this.iNVITMfecultcomp = iNVITMfecultcomp;
    }

    public Character getINVITMesttipo() {
        return iNVITMesttipo;
    }

    public void setINVITMesttipo(Character iNVITMesttipo) {
        this.iNVITMesttipo = iNVITMesttipo;
    }

    public Character getINVITMestactivo() {
        return iNVITMestactivo;
    }

    public void setINVITMestactivo(Character iNVITMestactivo) {
        this.iNVITMestactivo = iNVITMestactivo;
    }

    public String getINVTIMtipoitem() {
        return iNVTIMtipoitem;
    }

    public void setINVTIMtipoitem(String iNVTIMtipoitem) {
        this.iNVTIMtipoitem = iNVTIMtipoitem;
    }

    public Character getINVITMestretorna() {
        return iNVITMestretorna;
    }

    public void setINVITMestretorna(Character iNVITMestretorna) {
        this.iNVITMestretorna = iNVITMestretorna;
    }

    public Character getINVITMestensamb() {
        return iNVITMestensamb;
    }

    public void setINVITMestensamb(Character iNVITMestensamb) {
        this.iNVITMestensamb = iNVITMestensamb;
    }

    public Character getINVITMestproduce() {
        return iNVITMestproduce;
    }

    public void setINVITMestproduce(Character iNVITMestproduce) {
        this.iNVITMestproduce = iNVITMestproduce;
    }

    public Character getINVITMestafecinv() {
        return iNVITMestafecinv;
    }

    public void setINVITMestafecinv(Character iNVITMestafecinv) {
        this.iNVITMestafecinv = iNVITMestafecinv;
    }

    public Character getINVITMestdispvta() {
        return iNVITMestdispvta;
    }

    public void setINVITMestdispvta(Character iNVITMestdispvta) {
        this.iNVITMestdispvta = iNVITMestdispvta;
    }

    public Character getINVITMestvtafrac() {
        return iNVITMestvtafrac;
    }

    public void setINVITMestvtafrac(Character iNVITMestvtafrac) {
        this.iNVITMestvtafrac = iNVITMestvtafrac;
    }

    public Character getINVITMestmovimie() {
        return iNVITMestmovimie;
    }

    public void setINVITMestmovimie(Character iNVITMestmovimie) {
        this.iNVITMestmovimie = iNVITMestmovimie;
    }

    public String getINVITMcodescalon() {
        return iNVITMcodescalon;
    }

    public void setINVITMcodescalon(String iNVITMcodescalon) {
        this.iNVITMcodescalon = iNVITMcodescalon;
    }

    public Character getINVITMestadolote() {
        return iNVITMestadolote;
    }

    public void setINVITMestadolote(Character iNVITMestadolote) {
        this.iNVITMestadolote = iNVITMestadolote;
    }

    public Character getINVITMestadserie() {
        return iNVITMestadserie;
    }

    public void setINVITMestadserie(Character iNVITMestadserie) {
        this.iNVITMestadserie = iNVITMestadserie;
    }

    public Character getINVITMestadescar() {
        return iNVITMestadescar;
    }

    public void setINVITMestadescar(Character iNVITMestadescar) {
        this.iNVITMestadescar = iNVITMestadescar;
    }

    public Character getINVITMestactpubl() {
        return iNVITMestactpubl;
    }

    public void setINVITMestactpubl(Character iNVITMestactpubl) {
        this.iNVITMestactpubl = iNVITMestactpubl;
    }

    public String getINVITMctadescom() {
        return iNVITMctadescom;
    }

    public void setINVITMctadescom(String iNVITMctadescom) {
        this.iNVITMctadescom = iNVITMctadescom;
    }

    public String getINVITMctadevcom() {
        return iNVITMctadevcom;
    }

    public void setINVITMctadevcom(String iNVITMctadevcom) {
        this.iNVITMctadevcom = iNVITMctadevcom;
    }

    public String getINVITMctavta() {
        return iNVITMctavta;
    }

    public void setINVITMctavta(String iNVITMctavta) {
        this.iNVITMctavta = iNVITMctavta;
    }

    public String getINVITMctacosvta() {
        return iNVITMctacosvta;
    }

    public void setINVITMctacosvta(String iNVITMctacosvta) {
        this.iNVITMctacosvta = iNVITMctacosvta;
    }

    public String getINVITMctadesvta() {
        return iNVITMctadesvta;
    }

    public void setINVITMctadesvta(String iNVITMctadesvta) {
        this.iNVITMctadesvta = iNVITMctadesvta;
    }

    public String getINVITMctadevvta() {
        return iNVITMctadevvta;
    }

    public void setINVITMctadevvta(String iNVITMctadevvta) {
        this.iNVITMctadevvta = iNVITMctadevvta;
    }

    public String getINVITMctainventa() {
        return iNVITMctainventa;
    }

    public void setINVITMctainventa(String iNVITMctainventa) {
        this.iNVITMctainventa = iNVITMctainventa;
    }

    public String getINVITMcodigbarra() {
        return iNVITMcodigbarra;
    }

    public void setINVITMcodigbarra(String iNVITMcodigbarra) {
        this.iNVITMcodigbarra = iNVITMcodigbarra;
    }

    public String getINVITMtipomaterial() {
        return iNVITMtipomaterial;
    }

    public void setINVITMtipomaterial(String iNVITMtipomaterial) {
        this.iNVITMtipomaterial = iNVITMtipomaterial;
    }

    public String getINVITMgrupomater() {
        return iNVITMgrupomater;
    }

    public void setINVITMgrupomater(String iNVITMgrupomater) {
        this.iNVITMgrupomater = iNVITMgrupomater;
    }

    public Character getINVITMabc() {
        return iNVITMabc;
    }

    public void setINVITMabc(Character iNVITMabc) {
        this.iNVITMabc = iNVITMabc;
    }

    public BigDecimal getINVITMvalorabc() {
        return iNVITMvalorabc;
    }

    public void setINVITMvalorabc(BigDecimal iNVITMvalorabc) {
        this.iNVITMvalorabc = iNVITMvalorabc;
    }

    public BigDecimal getINVITMcantiabc() {
        return iNVITMcantiabc;
    }

    public void setINVITMcantiabc(BigDecimal iNVITMcantiabc) {
        this.iNVITMcantiabc = iNVITMcantiabc;
    }

    public Character getINVITMgenautolot() {
        return iNVITMgenautolot;
    }

    public void setINVITMgenautolot(Character iNVITMgenautolot) {
        this.iNVITMgenautolot = iNVITMgenautolot;
    }

    public BigDecimal getINVITMbatchproduc() {
        return iNVITMbatchproduc;
    }

    public void setINVITMbatchproduc(BigDecimal iNVITMbatchproduc) {
        this.iNVITMbatchproduc = iNVITMbatchproduc;
    }

    public String getINVITMcodreceta() {
        return iNVITMcodreceta;
    }

    public void setINVITMcodreceta(String iNVITMcodreceta) {
        this.iNVITMcodreceta = iNVITMcodreceta;
    }

    public String getINVITMcodplancom() {
        return iNVITMcodplancom;
    }

    public void setINVITMcodplancom(String iNVITMcodplancom) {
        this.iNVITMcodplancom = iNVITMcodplancom;
    }

    public BigDecimal getINVITMleatimacum() {
        return iNVITMleatimacum;
    }

    public void setINVITMleatimacum(BigDecimal iNVITMleatimacum) {
        this.iNVITMleatimacum = iNVITMleatimacum;
    }

    public BigDecimal getINVITMdcontcicli() {
        return iNVITMdcontcicli;
    }

    public void setINVITMdcontcicli(BigDecimal iNVITMdcontcicli) {
        this.iNVITMdcontcicli = iNVITMdcontcicli;
    }

    public BigDecimal getINVITMltimeinspe() {
        return iNVITMltimeinspe;
    }

    public void setINVITMltimeinspe(BigDecimal iNVITMltimeinspe) {
        this.iNVITMltimeinspe = iNVITMltimeinspe;
    }

    public Character getINVITMinspereque() {
        return iNVITMinspereque;
    }

    public void setINVITMinspereque(Character iNVITMinspereque) {
        this.iNVITMinspereque = iNVITMinspereque;
    }

    public String getINVITMsitioconsum() {
        return iNVITMsitioconsum;
    }

    public void setINVITMsitioconsum(String iNVITMsitioconsum) {
        this.iNVITMsitioconsum = iNVITMsitioconsum;
    }

    public String getINVITMlocaconsum() {
        return iNVITMlocaconsum;
    }

    public void setINVITMlocaconsum(String iNVITMlocaconsum) {
        this.iNVITMlocaconsum = iNVITMlocaconsum;
    }

    public BigDecimal getINVITMltimemanuf() {
        return iNVITMltimemanuf;
    }

    public void setINVITMltimemanuf(BigDecimal iNVITMltimemanuf) {
        this.iNVITMltimemanuf = iNVITMltimemanuf;
    }

    public Character getINVITMestamrp() {
        return iNVITMestamrp;
    }

    public void setINVITMestamrp(Character iNVITMestamrp) {
        this.iNVITMestamrp = iNVITMestamrp;
    }

    public Character getINVITMestamps() {
        return iNVITMestamps;
    }

    public void setINVITMestamps(Character iNVITMestamps) {
        this.iNVITMestamps = iNVITMestamps;
    }

    public String getINVITMcoddrp() {
        return iNVITMcoddrp;
    }

    public void setINVITMcoddrp(String iNVITMcoddrp) {
        this.iNVITMcoddrp = iNVITMcoddrp;
    }

    public BigDecimal getINVITMminiorden() {
        return iNVITMminiorden;
    }

    public void setINVITMminiorden(BigDecimal iNVITMminiorden) {
        this.iNVITMminiorden = iNVITMminiorden;
    }

    public BigDecimal getINVITMmaxiorden() {
        return iNVITMmaxiorden;
    }

    public void setINVITMmaxiorden(BigDecimal iNVITMmaxiorden) {
        this.iNVITMmaxiorden = iNVITMmaxiorden;
    }

    public BigDecimal getINVITMmultiorden() {
        return iNVITMmultiorden;
    }

    public void setINVITMmultiorden(BigDecimal iNVITMmultiorden) {
        this.iNVITMmultiorden = iNVITMmultiorden;
    }

    public BigDecimal getINVITMperiodorde() {
        return iNVITMperiodorde;
    }

    public void setINVITMperiodorde(BigDecimal iNVITMperiodorde) {
        this.iNVITMperiodorde = iNVITMperiodorde;
    }

    public String getINVITMpoliticainv() {
        return iNVITMpoliticainv;
    }

    public void setINVITMpoliticainv(String iNVITMpoliticainv) {
        this.iNVITMpoliticainv = iNVITMpoliticainv;
    }

    public BigDecimal getINVITMcantiorden() {
        return iNVITMcantiorden;
    }

    public void setINVITMcantiorden(BigDecimal iNVITMcantiorden) {
        this.iNVITMcantiorden = iNVITMcantiorden;
    }

    public Character getINVITMestfantas() {
        return iNVITMestfantas;
    }

    public void setINVITMestfantas(Character iNVITMestfantas) {
        this.iNVITMestfantas = iNVITMestfantas;
    }

    public Character getINVITMestorden() {
        return iNVITMestorden;
    }

    public void setINVITMestorden(Character iNVITMestorden) {
        this.iNVITMestorden = iNVITMestorden;
    }

    public Character getINVITMtratamient() {
        return iNVITMtratamient;
    }

    public void setINVITMtratamient(Character iNVITMtratamient) {
        this.iNVITMtratamient = iNVITMtratamient;
    }

    public String getINVITMoficicompra() {
        return iNVITMoficicompra;
    }

    public void setINVITMoficicompra(String iNVITMoficicompra) {
        this.iNVITMoficicompra = iNVITMoficicompra;
    }

    public BigDecimal getINVITMltimecompra() {
        return iNVITMltimecompra;
    }

    public void setINVITMltimecompra(BigDecimal iNVITMltimecompra) {
        this.iNVITMltimecompra = iNVITMltimecompra;
    }

    public String getINVITMestrecepoc() {
        return iNVITMestrecepoc;
    }

    public void setINVITMestrecepoc(String iNVITMestrecepoc) {
        this.iNVITMestrecepoc = iNVITMestrecepoc;
    }

    public String getINVITMcodruta() {
        return iNVITMcodruta;
    }

    public void setINVITMcodruta(String iNVITMcodruta) {
        this.iNVITMcodruta = iNVITMcodruta;
    }

    public BigDecimal getINVITMstocksegu() {
        return iNVITMstocksegu;
    }

    public void setINVITMstocksegu(BigDecimal iNVITMstocksegu) {
        this.iNVITMstocksegu = iNVITMstocksegu;
    }

    public BigDecimal getINVITMtiemposegu() {
        return iNVITMtiemposegu;
    }

    public void setINVITMtiemposegu(BigDecimal iNVITMtiemposegu) {
        this.iNVITMtiemposegu = iNVITMtiemposegu;
    }

    public BigDecimal getINVITMtiempovida() {
        return iNVITMtiempovida;
    }

    public void setINVITMtiempovida(BigDecimal iNVITMtiempovida) {
        this.iNVITMtiempovida = iNVITMtiempovida;
    }

    public String getINVITMcodprovee() {
        return iNVITMcodprovee;
    }

    public void setINVITMcodprovee(String iNVITMcodprovee) {
        this.iNVITMcodprovee = iNVITMcodprovee;
    }

    public BigDecimal getINVITMtiempcober() {
        return iNVITMtiempcober;
    }

    public void setINVITMtiempcober(BigDecimal iNVITMtiempcober) {
        this.iNVITMtiempcober = iNVITMtiempcober;
    }

    public String getINVITMcodbodega() {
        return iNVITMcodbodega;
    }

    public void setINVITMcodbodega(String iNVITMcodbodega) {
        this.iNVITMcodbodega = iNVITMcodbodega;
    }

    public String getINVITMlineproduc() {
        return iNVITMlineproduc;
    }

    public void setINVITMlineproduc(String iNVITMlineproduc) {
        this.iNVITMlineproduc = iNVITMlineproduc;
    }

    public Date getINVITMfecultmodi() {
        return iNVITMfecultmodi;
    }

    public void setINVITMfecultmodi(Date iNVITMfecultmodi) {
        this.iNVITMfecultmodi = iNVITMfecultmodi;
    }

    public Character getINVITMestmovfrac() {
        return iNVITMestmovfrac;
    }

    public void setINVITMestmovfrac(Character iNVITMestmovfrac) {
        this.iNVITMestmovfrac = iNVITMestmovfrac;
    }

    public String getINVITMunimedvol() {
        return iNVITMunimedvol;
    }

    public void setINVITMunimedvol(String iNVITMunimedvol) {
        this.iNVITMunimedvol = iNVITMunimedvol;
    }

    public String getINVITMtipocombo() {
        return iNVITMtipocombo;
    }

    public void setINVITMtipocombo(String iNVITMtipocombo) {
        this.iNVITMtipocombo = iNVITMtipocombo;
    }

    public String getINVITMcodrefprov() {
        return iNVITMcodrefprov;
    }

    public void setINVITMcodrefprov(String iNVITMcodrefprov) {
        this.iNVITMcodrefprov = iNVITMcodrefprov;
    }

    public String getINVITMctacontcxc() {
        return iNVITMctacontcxc;
    }

    public void setINVITMctacontcxc(String iNVITMctacontcxc) {
        this.iNVITMctacontcxc = iNVITMctacontcxc;
    }

    public Character getINVITMestcombprop() {
        return iNVITMestcombprop;
    }

    public void setINVITMestcombprop(Character iNVITMestcombprop) {
        this.iNVITMestcombprop = iNVITMestcombprop;
    }

    public String getItemAux() {
        return itemAux;
    }

    public void setItemAux(String itemAux) {
        this.itemAux = itemAux;
    }

    public String getItemAux2() {
        return itemAux2;
    }

    public void setItemAux2(String itemAux2) {
        this.itemAux2 = itemAux2;
    }

    public String getINVITMctadesvtapro() {
        return iNVITMctadesvtapro;
    }

    public void setINVITMctadesvtapro(String iNVITMctadesvtapro) {
        this.iNVITMctadesvtapro = iNVITMctadesvtapro;
    }

    public String getInvitmCtadesvtacort() {
        return invitmCtadesvtacort;
    }

    public void setInvitmCtadesvtacort(String invitmCtadesvtacort) {
        this.invitmCtadesvtacort = invitmCtadesvtacort;
    }

    public String getInvitmCtadesvtaadic() {
        return invitmCtadesvtaadic;
    }

    public void setInvitmCtadesvtaadic(String invitmCtadesvtaadic) {
        this.invitmCtadesvtaadic = invitmCtadesvtaadic;
    }

    public Character getINVITMestcompe() {
        return iNVITMestcompe;
    }

    public void setINVITMestcompe(Character iNVITMestcompe) {
        this.iNVITMestcompe = iNVITMestcompe;
    }

    public Date getINVITMfechamodif() {
        return iNVITMfechamodif;
    }

    public void setINVITMfechamodif(Date iNVITMfechamodif) {
        this.iNVITMfechamodif = iNVITMfechamodif;
    }

    public String getINVITMcodigoenv() {
        return iNVITMcodigoenv;
    }

    public void setINVITMcodigoenv(String iNVITMcodigoenv) {
        this.iNVITMcodigoenv = iNVITMcodigoenv;
    }

    public String getINVITMcodigochanc() {
        return iNVITMcodigochanc;
    }

    public void setINVITMcodigochanc(String iNVITMcodigochanc) {
        this.iNVITMcodigochanc = iNVITMcodigochanc;
    }

    public Character getINVITMestcervecero() {
        return iNVITMestcervecero;
    }

    public void setINVITMestcervecero(Character iNVITMestcervecero) {
        this.iNVITMestcervecero = iNVITMestcervecero;
    }

    public String getRTKMEPcodigo() {
        return rTKMEPcodigo;
    }

    public void setRTKMEPcodigo(String rTKMEPcodigo) {
        this.rTKMEPcodigo = rTKMEPcodigo;
    }

    public String getRTKGREcodigo() {
        return rTKGREcodigo;
    }

    public void setRTKGREcodigo(String rTKGREcodigo) {
        this.rTKGREcodigo = rTKGREcodigo;
    }

    public Character getINVITMpolitidesc() {
        return iNVITMpolitidesc;
    }

    public void setINVITMpolitidesc(Character iNVITMpolitidesc) {
        this.iNVITMpolitidesc = iNVITMpolitidesc;
    }

    public String getINVITMcodin5item() {
        return iNVITMcodin5item;
    }

    public void setINVITMcodin5item(String iNVITMcodin5item) {
        this.iNVITMcodin5item = iNVITMcodin5item;
    }

    public String getSTKNIVcodigo() {
        return sTKNIVcodigo;
    }

    public void setSTKNIVcodigo(String sTKNIVcodigo) {
        this.sTKNIVcodigo = sTKNIVcodigo;
    }

    public String getInvitmCompania() {
        return invitmCompania;
    }

    public void setInvitmCompania(String invitmCompania) {
        this.invitmCompania = invitmCompania;
    }

    public String getINVITMasumereten() {
        return iNVITMasumereten;
    }

    public void setINVITMasumereten(String iNVITMasumereten) {
        this.iNVITMasumereten = iNVITMasumereten;
    }

    public Integer getINVITMidcodigo() {
        return iNVITMidcodigo;
    }

    public void setINVITMidcodigo(Integer iNVITMidcodigo) {
        this.iNVITMidcodigo = iNVITMidcodigo;
    }

    public String getINVITMmanedecimales() {
        return iNVITMmanedecimales;
    }

    public void setINVITMmanedecimales(String iNVITMmanedecimales) {
        this.iNVITMmanedecimales = iNVITMmanedecimales;
    }

    public String getINVITMcodigoAlice() {
        return iNVITMcodigoAlice;
    }

    public void setINVITMcodigoAlice(String iNVITMcodigoAlice) {
        this.iNVITMcodigoAlice = iNVITMcodigoAlice;
    }

    public String getINVIN1codin1itemTMP() {
        return iNVIN1codin1itemTMP;
    }

    public void setINVIN1codin1itemTMP(String iNVIN1codin1itemTMP) {
        this.iNVIN1codin1itemTMP = iNVIN1codin1itemTMP;
    }

    public String getINVIN2codin2itemTMP() {
        return iNVIN2codin2itemTMP;
    }

    public void setINVIN2codin2itemTMP(String iNVIN2codin2itemTMP) {
        this.iNVIN2codin2itemTMP = iNVIN2codin2itemTMP;
    }

    public String getINVIN3codin3itemTMP() {
        return iNVIN3codin3itemTMP;
    }

    public void setINVIN3codin3itemTMP(String iNVIN3codin3itemTMP) {
        this.iNVIN3codin3itemTMP = iNVIN3codin3itemTMP;
    }

    public String getINVIN4codin4itemTMP() {
        return iNVIN4codin4itemTMP;
    }

    public void setINVIN4codin4itemTMP(String iNVIN4codin4itemTMP) {
        this.iNVIN4codin4itemTMP = iNVIN4codin4itemTMP;
    }

    public String getINVIN5codin5itemTMP() {
        return iNVIN5codin5itemTMP;
    }

    public void setINVIN5codin5itemTMP(String iNVIN5codin5itemTMP) {
        this.iNVIN5codin5itemTMP = iNVIN5codin5itemTMP;
    }

    public String getINVITMmanedisplay() {
        return iNVITMmanedisplay;
    }

    public void setINVITMmanedisplay(String iNVITMmanedisplay) {
        this.iNVITMmanedisplay = iNVITMmanedisplay;
    }

    public String getGRPPROcodigo() {
        return gRPPROcodigo;
    }

    public void setGRPPROcodigo(String gRPPROcodigo) {
        this.gRPPROcodigo = gRPPROcodigo;
    }

    public Character getINVITMimpplastico() {
        return iNVITMimpplastico;
    }

    public void setINVITMimpplastico(Character iNVITMimpplastico) {
        this.iNVITMimpplastico = iNVITMimpplastico;
    }

    public BigDecimal getINVITMcostomanobra() {
        return iNVITMcostomanobra;
    }

    public void setINVITMcostomanobra(BigDecimal iNVITMcostomanobra) {
        this.iNVITMcostomanobra = iNVITMcostomanobra;
    }

    public BigDecimal getINVITMcostofabrica() {
        return iNVITMcostofabrica;
    }

    public void setINVITMcostofabrica(BigDecimal iNVITMcostofabrica) {
        this.iNVITMcostofabrica = iNVITMcostofabrica;
    }

    public String getINVIN5codin5item() {
        return iNVIN5codin5item;
    }

    public void setINVIN5codin5item(String iNVIN5codin5item) {
        this.iNVIN5codin5item = iNVIN5codin5item;
    }

    public String getINVITMtipolistaprecio() {
        return iNVITMtipolistaprecio;
    }

    public void setINVITMtipolistaprecio(String iNVITMtipolistaprecio) {
        this.iNVITMtipolistaprecio = iNVITMtipolistaprecio;
    }

    public Integer getINVITMuniAsignada() {
        return iNVITMuniAsignada;
    }

    public void setINVITMuniAsignada(Integer iNVITMuniAsignada) {
        this.iNVITMuniAsignada = iNVITMuniAsignada;
    }

    public String getINVITMestfecha() {
        return iNVITMestfecha;
    }

    public void setINVITMestfecha(String iNVITMestfecha) {
        this.iNVITMestfecha = iNVITMestfecha;
    }

    public String getINVITMunidmedbase() {
        return iNVITMunidmedbase;
    }

    public void setINVITMunidmedbase(String iNVITMunidmedbase) {
        this.iNVITMunidmedbase = iNVITMunidmedbase;
    }

    public String getINVITMestamultunid() {
        return iNVITMestamultunid;
    }

    public void setINVITMestamultunid(String iNVITMestamultunid) {
        this.iNVITMestamultunid = iNVITMestamultunid;
    }

    public BigDecimal getINVITMdesctoproduc() {
        return iNVITMdesctoproduc;
    }

    public void setINVITMdesctoproduc(BigDecimal iNVITMdesctoproduc) {
        this.iNVITMdesctoproduc = iNVITMdesctoproduc;
    }

    public String getINVITMestacombo() {
        return iNVITMestacombo;
    }

    public void setINVITMestacombo(String iNVITMestacombo) {
        this.iNVITMestacombo = iNVITMestacombo;
    }

    public String getINVITMestaPromocion() {
        return iNVITMestaPromocion;
    }

    public void setINVITMestaPromocion(String iNVITMestaPromocion) {
        this.iNVITMestaPromocion = iNVITMestaPromocion;
    }

    public String getINVITMSubeEquipo() {
        return iNVITMSubeEquipo;
    }

    public void setINVITMSubeEquipo(String iNVITMSubeEquipo) {
        this.iNVITMSubeEquipo = iNVITMSubeEquipo;
    }

    public String getINVITMimpdetcompo() {
        return iNVITMimpdetcompo;
    }

    public void setINVITMimpdetcompo(String iNVITMimpdetcompo) {
        this.iNVITMimpdetcompo = iNVITMimpdetcompo;
    }

    public String getINVITMcompopedido() {
        return iNVITMcompopedido;
    }

    public void setINVITMcompopedido(String iNVITMcompopedido) {
        this.iNVITMcompopedido = iNVITMcompopedido;
    }

    public BigDecimal getINVITMuniXDisplay() {
        return iNVITMuniXDisplay;
    }

    public void setINVITMuniXDisplay(BigDecimal iNVITMuniXDisplay) {
        this.iNVITMuniXDisplay = iNVITMuniXDisplay;
    }

    public String getINVITMestadoICE() {
        return iNVITMestadoICE;
    }

    public void setINVITMestadoICE(String iNVITMestadoICE) {
        this.iNVITMestadoICE = iNVITMestadoICE;
    }
    
    public String getCodigoItem() {
        return codigoItem;
    }

    public void setCodigoItem(String codigoItem) {
        this.codigoItem = codigoItem;
    }

    public BigDecimal getCantidadFuncional() {
        return cantidadFuncional;
    }

    public void setCantidadFuncional(BigDecimal cantidadFuncional) {
        this.cantidadFuncional = cantidadFuncional;
    }

    /*public INVRdetLISTAPRECIO getPrecio() {
    Session sess = HiberUtil.getSession();
    precio = (INVRdetLISTAPRECIO) sess.get(INVRdetLISTAPRECIO.class, (Serializable) new INVRdetLISTAPRECIOPK(1, listaPrecio, this.getInvmItemPK().getINVITMcodigo()));
    Hibernate.initialize(precio);
    return precio;
    }
    
    public void setPrecio(INVRdetLISTAPRECIO precio) {
    this.precio = precio;
    }*/
    
    public BigDecimal getCosto() {
        return costo;
    }

    public void setCosto(BigDecimal costo) {
        this.costo = costo;
    }

    public BigDecimal getSubtotalCosto() {
        if(subtotalCosto == null)
            return BigDecimal.ZERO;
        return subtotalCosto;
    }

    
    public void setSubtotalCosto(BigDecimal subtotalCosto) {
        this.subtotalCosto = subtotalCosto;
    }

    public BigDecimal getDisponible() {
        return disponible;
    }

    public void setDisponible(BigDecimal disponible) {
        this.disponible = disponible;
    }

    public BigDecimal getCajas() {
        return cajas;
    }

    public void setCajas(BigDecimal cajas) {
        this.cajas = cajas;
    }

    public BigDecimal getUnidad() {
        return unidad;
    }

    public void setUnidad(BigDecimal unidad) {
        this.unidad = unidad;
    }

    public BigDecimal getValorImpuesto() {
        return valorImpuesto;
    }

    public void setValorImpuesto(BigDecimal valorImpuesto) {
        this.valorImpuesto = valorImpuesto;
    }

    public String getCodigoBarra() {
        return codigoBarra;
    }

    public void setCodigoBarra(String codigoBarra) {
        this.codigoBarra = codigoBarra;
    }

    public BigDecimal getCostoCaja() {
        return costoCaja;
    }

    public void setCostoCaja(BigDecimal costoCaja) {
        this.costoCaja = costoCaja;
    }

    public BigDecimal getCostoItem() {
        return costoItem;
    }

    public void setCostoItem(BigDecimal costoItem) {
        this.costoItem = costoItem;
    }

    public BigDecimal getCostoCompra() {
        return costoCompra;
    }

    public void setCostoCompra(BigDecimal costoCompra) {
        this.costoCompra = costoCompra;
    }

    public BigDecimal getPorcDesc() {
        return porcDesc;
    }

    public void setPorcDesc(BigDecimal porcDesc) {
        this.porcDesc = porcDesc;
    }

    public BigDecimal getDescuento() {
        return descuento;
    }

    public void setDescuento(BigDecimal descuento) {
        this.descuento = descuento;
    }

    public BigDecimal getImpPlastico() {
        return impPlastico;
    }

    public void setImpPlastico(BigDecimal impPlastico) {
        this.impPlastico = impPlastico;
    }

    public Long getGrupo() {
        return grupo;
    }

    public void setGrupo(Long grupo) {
        this.grupo = grupo;
    }

    public Boolean getIva() {
        return iva;
    }

    public void setIva(Boolean iva) {
        this.iva = iva;
    }

    public Boolean getItemSugerido() {
        return itemSugerido;
    }

    public void setItemSugerido(Boolean itemSugerido) {
        this.itemSugerido = itemSugerido;
    }

    public BigDecimal getPorcentajeIce() {
        return porcentajeIce;
    }

    public void setPorcentajeIce(BigDecimal porcentajeIce) {
        this.porcentajeIce = porcentajeIce;
    }    

    public BigDecimal getIce() {
        return ice;
    }

    public void setIce(BigDecimal ice) {
        this.ice = ice;
    }
    
    public void actualizarCantidades(ItemCantidades ic){
        this.unidad = ic.getUnidad();
        this.cajas = ic.getCaja();
        this.cantidadFuncional = ic.getFuncional();
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (invmItemPK != null ? invmItemPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InvmItem)) {
            return false;
        }
        InvmItem other = (InvmItem) object;
        if ((this.invmItemPK == null && other.invmItemPK != null) || (this.invmItemPK != null && !this.invmItemPK.equals(other.invmItemPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.InvmItem[ invmItemPK=" + invmItemPK + " ]";
    }    
    
}
