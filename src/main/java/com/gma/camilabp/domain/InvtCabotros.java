/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
//import org.hibernate.annotations.Formula;

/**
 *
 * @author Acer
 */
@Entity
@Table(name = "INVT_CABOTROS")
@NamedQueries({
    @NamedQuery(name = "InvtCabotros.findAll", query = "SELECT i FROM InvtCabotros i")})
public class InvtCabotros implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "INVCOT_numsecuenc")
    private Long iNVCOTnumsecuenc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "INVCOT_numdocumen")
    private int iNVCOTnumdocumen;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENCIA_codigo")
    private String gENCIAcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENOFI_codigo")
    private String gENOFIcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "GENMOD_codigo")
    private String gENMODcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "GENTDO_codigo")
    private String gENTDOcodigo;
    @Size(max = 3)
    @Column(name = "INVCOT_motvajuste")
    private String iNVCOTmotvajuste;
    @Size(max = 9)
    @Column(name = "GENTDO_numplanotr")
    private String gENTDOnumplanotr;
    @Column(name = "INVCOT_fechaemisi")
    @Temporal(TemporalType.TIMESTAMP)
    private Date iNVCOTfechaemisi;
    @Column(name = "INVCOT_fechaingreso")
    @Temporal(TemporalType.TIMESTAMP)
    private Date iNVCOTfechaingreso;
    @Size(max = 4)
    @Column(name = "GENBOD_codigorela")
    private String gENBODcodigorela;
    @Size(max = 3)
    @Column(name = "GENTDO_coddocrela")
    private String gENTDOcoddocrela;
    @Column(name = "INVCOT_numdocrela")
    private Integer iNVCOTnumdocrela;
    @Column(name = "INVCOT_numsecrela")
    private Integer iNVCOTnumsecrela;
    @Size(max = 80)
    @Column(name = "INVCOT_descricort")
    private String iNVCOTdescricort;
    @Size(max = 200)
    @Column(name = "INVCOT_descrilarg")
    private String iNVCOTdescrilarg;
    @Size(max = 5)
    @Column(name = "GENMON_codigo")
    private String gENMONcodigo;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "INVCOT_subtotamov")
    private BigDecimal iNVCOTsubtotamov;
    @Column(name = "INVCOT_desctomov")
    private BigDecimal iNVCOTdesctomov;
    @Column(name = "INVCOT_totimpumov")
    private BigDecimal iNVCOTtotimpumov;
    @Column(name = "INVCOT_netomov")
    private BigDecimal iNVCOTnetomov;
    @Size(max = 1)
    @Column(name = "INVCOT_estado")
    private String iNVCOTestado;
    @Column(name = "INVCOT_totalpeso")
    private BigDecimal iNVCOTtotalpeso;
    @Column(name = "INVCOT_totalvolum")
    private BigDecimal iNVCOTtotalvolum;
    @Size(max = 1)
    @Column(name = "INVCOT_estuso")
    private String iNVCOTestuso;
    @Size(max = 1)
    @Column(name = "INVCOT_estelimina")
    private String iNVCOTestelimina;
    @Size(max = 15)
    @Column(name = "INVCOT_codcliente")
    private String iNVCOTcodcliente;
    @Size(max = 4)
    @Column(name = "GENBOD_codigorige")
    private String gENBODcodigorige;
    @Size(max = 1)
    @Column(name = "INVCOT_estprepost")
    private String iNVCOTestprepost;
    @Size(max = 20)
    @Column(name = "INVCOT_codctacontable")
    private String iNVCOTcodctacontable;
    @Size(max = 2)
    @Column(name = "INVCCO_codigo")
    private String iNVCCOcodigo;
    @Size(max = 3)
    @Column(name = "INVCOT_codusuario")
    private String iNVCOTcodusuario;
    //@Formula(value = "(SELECT b.GENBOD_nombre FROM GENR_BODEGA b WHERE b.GENCIA_codigo=GENCIA_codigo AND b.GENOFI_codigo=GENOFI_codigo AND b.GENBOD_codigo=GENBOD_codigorige)")
    @Transient
    private String bodegaOrigen;
    //@Formula(value = "(SELECT br.GENBOD_nombre FROM GENR_BODEGA br WHERE br.GENCIA_codigo=GENCIA_codigo AND br.GENOFI_codigo=GENOFI_codigo AND br.GENBOD_codigo=GENBOD_codigorela)")
    @Transient
    private String bodegaRelacionada;    

    public InvtCabotros() {
    }

    public InvtCabotros(Long iNVCOTnumsecuenc) {
        this.iNVCOTnumsecuenc = iNVCOTnumsecuenc;
    }

    public InvtCabotros(Long iNVCOTnumsecuenc, int iNVCOTnumdocumen, String gENCIAcodigo, String gENOFIcodigo, String gENMODcodigo, String gENTDOcodigo) {
        this.iNVCOTnumsecuenc = iNVCOTnumsecuenc;
        this.iNVCOTnumdocumen = iNVCOTnumdocumen;
        this.gENCIAcodigo = gENCIAcodigo;
        this.gENOFIcodigo = gENOFIcodigo;
        this.gENMODcodigo = gENMODcodigo;
        this.gENTDOcodigo = gENTDOcodigo;
    }

    public Long getINVCOTnumsecuenc() {
        return iNVCOTnumsecuenc;
    }

    public void setINVCOTnumsecuenc(Long iNVCOTnumsecuenc) {
        this.iNVCOTnumsecuenc = iNVCOTnumsecuenc;
    }

    public int getINVCOTnumdocumen() {
        return iNVCOTnumdocumen;
    }

    public void setINVCOTnumdocumen(int iNVCOTnumdocumen) {
        this.iNVCOTnumdocumen = iNVCOTnumdocumen;
    }

    public String getGENCIAcodigo() {
        return gENCIAcodigo;
    }

    public void setGENCIAcodigo(String gENCIAcodigo) {
        this.gENCIAcodigo = gENCIAcodigo;
    }

    public String getGENOFIcodigo() {
        return gENOFIcodigo;
    }

    public void setGENOFIcodigo(String gENOFIcodigo) {
        this.gENOFIcodigo = gENOFIcodigo;
    }

    public String getGENMODcodigo() {
        return gENMODcodigo;
    }

    public void setGENMODcodigo(String gENMODcodigo) {
        this.gENMODcodigo = gENMODcodigo;
    }

    public String getGENTDOcodigo() {
        return gENTDOcodigo;
    }

    public void setGENTDOcodigo(String gENTDOcodigo) {
        this.gENTDOcodigo = gENTDOcodigo;
    }

    public String getINVCOTmotvajuste() {
        return iNVCOTmotvajuste;
    }

    public void setINVCOTmotvajuste(String iNVCOTmotvajuste) {
        this.iNVCOTmotvajuste = iNVCOTmotvajuste;
    }

    public String getGENTDOnumplanotr() {
        return gENTDOnumplanotr;
    }

    public void setGENTDOnumplanotr(String gENTDOnumplanotr) {
        this.gENTDOnumplanotr = gENTDOnumplanotr;
    }

    public Date getINVCOTfechaemisi() {
        return iNVCOTfechaemisi;
    }

    public void setINVCOTfechaemisi(Date iNVCOTfechaemisi) {
        this.iNVCOTfechaemisi = iNVCOTfechaemisi;
    }

    public Date getINVCOTfechaingreso() {
        return iNVCOTfechaingreso;
    }

    public void setINVCOTfechaingreso(Date iNVCOTfechaingreso) {
        this.iNVCOTfechaingreso = iNVCOTfechaingreso;
    }

    public String getGENBODcodigorela() {
        return gENBODcodigorela;
    }

    public void setGENBODcodigorela(String gENBODcodigorela) {
        this.gENBODcodigorela = gENBODcodigorela;
    }

    public String getGENTDOcoddocrela() {
        return gENTDOcoddocrela;
    }

    public void setGENTDOcoddocrela(String gENTDOcoddocrela) {
        this.gENTDOcoddocrela = gENTDOcoddocrela;
    }

    public Integer getINVCOTnumdocrela() {
        return iNVCOTnumdocrela;
    }

    public void setINVCOTnumdocrela(Integer iNVCOTnumdocrela) {
        this.iNVCOTnumdocrela = iNVCOTnumdocrela;
    }

    public Integer getINVCOTnumsecrela() {
        return iNVCOTnumsecrela;
    }

    public void setINVCOTnumsecrela(Integer iNVCOTnumsecrela) {
        this.iNVCOTnumsecrela = iNVCOTnumsecrela;
    }

    public String getINVCOTdescricort() {
        return iNVCOTdescricort;
    }

    public void setINVCOTdescricort(String iNVCOTdescricort) {
        this.iNVCOTdescricort = iNVCOTdescricort;
    }

    public String getINVCOTdescrilarg() {
        return iNVCOTdescrilarg;
    }

    public void setINVCOTdescrilarg(String iNVCOTdescrilarg) {
        this.iNVCOTdescrilarg = iNVCOTdescrilarg;
    }

    public String getGENMONcodigo() {
        return gENMONcodigo;
    }

    public void setGENMONcodigo(String gENMONcodigo) {
        this.gENMONcodigo = gENMONcodigo;
    }

    public BigDecimal getINVCOTsubtotamov() {
        return iNVCOTsubtotamov;
    }

    public void setINVCOTsubtotamov(BigDecimal iNVCOTsubtotamov) {
        this.iNVCOTsubtotamov = iNVCOTsubtotamov;
    }

    public BigDecimal getINVCOTdesctomov() {
        return iNVCOTdesctomov;
    }

    public void setINVCOTdesctomov(BigDecimal iNVCOTdesctomov) {
        this.iNVCOTdesctomov = iNVCOTdesctomov;
    }

    public BigDecimal getINVCOTtotimpumov() {
        return iNVCOTtotimpumov;
    }

    public void setINVCOTtotimpumov(BigDecimal iNVCOTtotimpumov) {
        this.iNVCOTtotimpumov = iNVCOTtotimpumov;
    }

    public BigDecimal getINVCOTnetomov() {
        return iNVCOTnetomov;
    }

    public void setINVCOTnetomov(BigDecimal iNVCOTnetomov) {
        this.iNVCOTnetomov = iNVCOTnetomov;
    }

    public String getINVCOTestado() {
        return iNVCOTestado;
    }

    public void setINVCOTestado(String iNVCOTestado) {
        this.iNVCOTestado = iNVCOTestado;
    }

    public BigDecimal getINVCOTtotalpeso() {
        return iNVCOTtotalpeso;
    }

    public void setINVCOTtotalpeso(BigDecimal iNVCOTtotalpeso) {
        this.iNVCOTtotalpeso = iNVCOTtotalpeso;
    }

    public BigDecimal getINVCOTtotalvolum() {
        return iNVCOTtotalvolum;
    }

    public void setINVCOTtotalvolum(BigDecimal iNVCOTtotalvolum) {
        this.iNVCOTtotalvolum = iNVCOTtotalvolum;
    }

    public String getINVCOTestuso() {
        return iNVCOTestuso;
    }

    public void setINVCOTestuso(String iNVCOTestuso) {
        this.iNVCOTestuso = iNVCOTestuso;
    }

    public String getINVCOTestelimina() {
        return iNVCOTestelimina;
    }

    public void setINVCOTestelimina(String iNVCOTestelimina) {
        this.iNVCOTestelimina = iNVCOTestelimina;
    }

    public String getINVCOTcodcliente() {
        return iNVCOTcodcliente;
    }

    public void setINVCOTcodcliente(String iNVCOTcodcliente) {
        this.iNVCOTcodcliente = iNVCOTcodcliente;
    }

    public String getGENBODcodigorige() {
        return gENBODcodigorige;
    }

    public void setGENBODcodigorige(String gENBODcodigorige) {
        this.gENBODcodigorige = gENBODcodigorige;
    }

    public String getINVCOTestprepost() {
        return iNVCOTestprepost;
    }

    public void setINVCOTestprepost(String iNVCOTestprepost) {
        this.iNVCOTestprepost = iNVCOTestprepost;
    }

    public String getINVCOTcodctacontable() {
        return iNVCOTcodctacontable;
    }

    public void setINVCOTcodctacontable(String iNVCOTcodctacontable) {
        this.iNVCOTcodctacontable = iNVCOTcodctacontable;
    }

    public String getINVCCOcodigo() {
        return iNVCCOcodigo;
    }

    public void setINVCCOcodigo(String iNVCCOcodigo) {
        this.iNVCCOcodigo = iNVCCOcodigo;
    }

    public String getINVCOTcodusuario() {
        return iNVCOTcodusuario;
    }

    public void setINVCOTcodusuario(String iNVCOTcodusuario) {
        this.iNVCOTcodusuario = iNVCOTcodusuario;
    }

    public String getBodegaOrigen() {
        return bodegaOrigen;
    }

    public void setBodegaOrigen(String bodegaOrigen) {
        this.bodegaOrigen = bodegaOrigen;
    }

    public String getBodegaRelacionada() {
        return bodegaRelacionada;
    }

    public void setBodegaRelacionada(String bodegaRelacionada) {
        this.bodegaRelacionada = bodegaRelacionada;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iNVCOTnumsecuenc != null ? iNVCOTnumsecuenc.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InvtCabotros)) {
            return false;
        }
        InvtCabotros other = (InvtCabotros) object;
        if ((this.iNVCOTnumsecuenc == null && other.iNVCOTnumsecuenc != null) || (this.iNVCOTnumsecuenc != null && !this.iNVCOTnumsecuenc.equals(other.iNVCOTnumsecuenc))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.InvtCabotros[ iNVCOTnumsecuenc=" + iNVCOTnumsecuenc + " ]";
    }
    
}
