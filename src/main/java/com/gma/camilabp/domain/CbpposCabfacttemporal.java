/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Size;
//import org.hibernate.annotations.Formula;

/**
 *
 * @author userdb6
 */
@Entity
@Table(name = "CBPPOS_CABFACTTEMPORAL")
@NamedQueries({
    @NamedQuery(name = "CbpposCabfacttemporal.findAll", query = "SELECT c FROM CbpposCabfacttemporal c")})
public class CbpposCabfacttemporal implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cbpfacc_codigo")
    private Long cbpfaccCodigo;
    @Basic(optional = false)
    @Column(name = "segusu_codigo")
    private Long segusuCodigo;
    @Column(name = "segtur_numero")
    private Long segturNumero;
    @Size(max = 2)
    @Column(name = "cbpfacc_estado")
    private String cbpfaccEstado="A";
    @Column(name = "cbpfacc_fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cbpfaccFecha;
    @Column(name = "cbpfacc_cliente")
    private String cbpfaccCliente;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cbpfaccCodigo", fetch = FetchType.LAZY)
    private Collection<CbpposFacttemporal> detFacttemporalCollection;
    //@Formula(value = "(SELECT c.CXCCLI_razonsocia FROM CXCM_CLIENTE c WHERE c.CXCCLI_codigo=cbpfacc_cliente)")
    @Transient
    private String nombreCliente;

    public CbpposCabfacttemporal() {
    }

    public CbpposCabfacttemporal(Long cbpfaccCodigo) {
        this.cbpfaccCodigo = cbpfaccCodigo;
    }

    public CbpposCabfacttemporal(Long cbpfaccCodigo, Long segusuCodigo) {
        this.cbpfaccCodigo = cbpfaccCodigo;
        this.segusuCodigo = segusuCodigo;
    }

    public Long getCbpfaccCodigo() {
        return cbpfaccCodigo;
    }

    public void setCbpfaccCodigo(Long cbpfaccCodigo) {
        this.cbpfaccCodigo = cbpfaccCodigo;
    }

    public Long getSegusuCodigo() {
        return segusuCodigo;
    }

    public void setSegusuCodigo(Long segusuCodigo) {
        this.segusuCodigo = segusuCodigo;
    }

    public Long getSegturNumero() {
        return segturNumero;
    }

    public void setSegturNumero(Long segturNumero) {
        this.segturNumero = segturNumero;
    }

    public String getCbpfaccEstado() {
        return cbpfaccEstado;
    }

    public void setCbpfaccEstado(String cbpfaccEstado) {
        this.cbpfaccEstado = cbpfaccEstado;
    }

    public Date getCbpfaccFecha() {
        return cbpfaccFecha;
    }

    public void setCbpfaccFecha(Date cbpfaccFecha) {
        this.cbpfaccFecha = cbpfaccFecha;
    }

    public String getCbpfaccCliente() {
        return cbpfaccCliente;
    }

    public void setCbpfaccCliente(String cbpfaccCliente) {
        this.cbpfaccCliente = cbpfaccCliente;
    }

    public Collection<CbpposFacttemporal> getDetFacttemporalCollection() {
        return detFacttemporalCollection;
    }

    public void setDetFacttemporalCollection(Collection<CbpposFacttemporal> detFacttemporalCollection) {
        this.detFacttemporalCollection = detFacttemporalCollection;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cbpfaccCodigo != null ? cbpfaccCodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbpposCabfacttemporal)) {
            return false;
        }
        CbpposCabfacttemporal other = (CbpposCabfacttemporal) object;
        if ((this.cbpfaccCodigo == null && other.cbpfaccCodigo != null) || (this.cbpfaccCodigo != null && !this.cbpfaccCodigo.equals(other.cbpfaccCodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CbpposCabfacttemporal[ cbpfaccCodigo=" + cbpfaccCodigo + " ]";
    }
    
}
