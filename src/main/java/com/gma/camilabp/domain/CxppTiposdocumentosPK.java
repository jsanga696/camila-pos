/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb5
 */
@Embeddable
public class CxppTiposdocumentosPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENCIA_codigo")
    private String gENCIAcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "CXPTDO_codigo")
    private String cXPTDOcodigo;

    public CxppTiposdocumentosPK() {
    }

    public CxppTiposdocumentosPK(String gENCIAcodigo, String cXPTDOcodigo) {
        this.gENCIAcodigo = gENCIAcodigo;
        this.cXPTDOcodigo = cXPTDOcodigo;
    }

    public String getGENCIAcodigo() {
        return gENCIAcodigo;
    }

    public void setGENCIAcodigo(String gENCIAcodigo) {
        this.gENCIAcodigo = gENCIAcodigo;
    }

    public String getCXPTDOcodigo() {
        return cXPTDOcodigo;
    }

    public void setCXPTDOcodigo(String cXPTDOcodigo) {
        this.cXPTDOcodigo = cXPTDOcodigo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gENCIAcodigo != null ? gENCIAcodigo.hashCode() : 0);
        hash += (cXPTDOcodigo != null ? cXPTDOcodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CxppTiposdocumentosPK)) {
            return false;
        }
        CxppTiposdocumentosPK other = (CxppTiposdocumentosPK) object;
        if ((this.gENCIAcodigo == null && other.gENCIAcodigo != null) || (this.gENCIAcodigo != null && !this.gENCIAcodigo.equals(other.gENCIAcodigo))) {
            return false;
        }
        if ((this.cXPTDOcodigo == null && other.cXPTDOcodigo != null) || (this.cXPTDOcodigo != null && !this.cXPTDOcodigo.equals(other.cXPTDOcodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CxppTiposdocumentosPK[ gENCIAcodigo=" + gENCIAcodigo + ", cXPTDOcodigo=" + cXPTDOcodigo + " ]";
    }
    
}
