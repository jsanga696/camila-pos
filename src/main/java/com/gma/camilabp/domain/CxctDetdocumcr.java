/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb5
 */
@Entity
@Table(name = "CXCT_DETDOCUMCR")
@NamedQueries({
    @NamedQuery(name = "CxctDetdocumcr.findAll", query = "SELECT c FROM CxctDetdocumcr c")})
public class CxctDetdocumcr implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CxctDetdocumcrPK cxctDetdocumcrPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENOFI_codigo")
    private String gENOFIcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "GENMOD_codigocr")
    private String gENMODcodigocr;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "GENTDO_codigocr")
    private String gENTDOcodigocr;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 9)
    @Column(name = "CXCCCR_numdocumcr")
    private String cXCCCRnumdocumcr;
    @Size(max = 2)
    @Column(name = "GENBAN_tipo")
    private String gENBANtipo;
    @Size(max = 2)
    @Column(name = "GENBAN_codigo")
    private String gENBANcodigo;
    @Size(max = 6)
    @Column(name = "CXCCLI_codigo")
    private String cXCCLIcodigo;
    @Column(name = "CXCDPG_fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cXCDPGfecha;
    @Size(max = 3)
    @Column(name = "CXCFPG_codigo")
    private String cXCFPGcodigo;
    @Size(max = 5)
    @Column(name = "GENMON_codigo")
    private String gENMONcodigo;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "CXCDPG_cotiza")
    private BigDecimal cXCDPGcotiza;
    @Column(name = "CXCDPG_fechavcto")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cXCDPGfechavcto;
    @Column(name = "CXCDPG_valormov")
    private BigDecimal cXCDPGvalormov;
    @Size(max = 30)
    @Column(name = "CXCDPG_cuentacte")
    private String cXCDPGcuentacte;
    @Size(max = 20)
    @Column(name = "CXCDPG_numerochq")
    private String cXCDPGnumerochq;
    @Column(name = "CXCDPG_fechadepos")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cXCDPGfechadepos;
    @Size(max = 1)
    @Column(name = "CXCDPG_estcheque")
    private String cXCDPGestcheque;
    @Size(max = 9)
    @Column(name = "CXCDPG_secubanco")
    private String cXCDPGsecubanco;
    @Size(max = 9)
    @Column(name = "CXCDPG_secubancodep")
    private String cXCDPGsecubancodep;
    @JoinColumns({
        @JoinColumn(name = "CXCDPG_numerosec", referencedColumnName = "CXCCCR_numsecuen", insertable = false, updatable = false)
        , @JoinColumn(name = "GENCIA_codigo", referencedColumnName = "GENCIA_codigo", insertable = false, updatable = false)})
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private CxctCabcredito cxctCabcredito;

    public CxctDetdocumcr() {
    }

    public CxctDetdocumcr(CxctDetdocumcrPK cxctDetdocumcrPK) {
        this.cxctDetdocumcrPK = cxctDetdocumcrPK;
    }

    public CxctDetdocumcr(CxctDetdocumcrPK cxctDetdocumcrPK, String gENOFIcodigo, String gENMODcodigocr, String gENTDOcodigocr, String cXCCCRnumdocumcr) {
        this.cxctDetdocumcrPK = cxctDetdocumcrPK;
        this.gENOFIcodigo = gENOFIcodigo;
        this.gENMODcodigocr = gENMODcodigocr;
        this.gENTDOcodigocr = gENTDOcodigocr;
        this.cXCCCRnumdocumcr = cXCCCRnumdocumcr;
    }

    public CxctDetdocumcr(String gENCIAcodigo, int cXCDPGnumerosec, int cXCDPGnumlinea) {
        this.cxctDetdocumcrPK = new CxctDetdocumcrPK(gENCIAcodigo, cXCDPGnumerosec, cXCDPGnumlinea);
    }

    public CxctDetdocumcrPK getCxctDetdocumcrPK() {
        return cxctDetdocumcrPK;
    }

    public void setCxctDetdocumcrPK(CxctDetdocumcrPK cxctDetdocumcrPK) {
        this.cxctDetdocumcrPK = cxctDetdocumcrPK;
    }

    public String getGENOFIcodigo() {
        return gENOFIcodigo;
    }

    public void setGENOFIcodigo(String gENOFIcodigo) {
        this.gENOFIcodigo = gENOFIcodigo;
    }

    public String getGENMODcodigocr() {
        return gENMODcodigocr;
    }

    public void setGENMODcodigocr(String gENMODcodigocr) {
        this.gENMODcodigocr = gENMODcodigocr;
    }

    public String getGENTDOcodigocr() {
        return gENTDOcodigocr;
    }

    public void setGENTDOcodigocr(String gENTDOcodigocr) {
        this.gENTDOcodigocr = gENTDOcodigocr;
    }

    public String getCXCCCRnumdocumcr() {
        return cXCCCRnumdocumcr;
    }

    public void setCXCCCRnumdocumcr(String cXCCCRnumdocumcr) {
        this.cXCCCRnumdocumcr = cXCCCRnumdocumcr;
    }

    public String getGENBANtipo() {
        return gENBANtipo;
    }

    public void setGENBANtipo(String gENBANtipo) {
        this.gENBANtipo = gENBANtipo;
    }

    public String getGENBANcodigo() {
        return gENBANcodigo;
    }

    public void setGENBANcodigo(String gENBANcodigo) {
        this.gENBANcodigo = gENBANcodigo;
    }

    public String getCXCCLIcodigo() {
        return cXCCLIcodigo;
    }

    public void setCXCCLIcodigo(String cXCCLIcodigo) {
        this.cXCCLIcodigo = cXCCLIcodigo;
    }

    public Date getCXCDPGfecha() {
        return cXCDPGfecha;
    }

    public void setCXCDPGfecha(Date cXCDPGfecha) {
        this.cXCDPGfecha = cXCDPGfecha;
    }

    public String getCXCFPGcodigo() {
        return cXCFPGcodigo;
    }

    public void setCXCFPGcodigo(String cXCFPGcodigo) {
        this.cXCFPGcodigo = cXCFPGcodigo;
    }

    public String getGENMONcodigo() {
        return gENMONcodigo;
    }

    public void setGENMONcodigo(String gENMONcodigo) {
        this.gENMONcodigo = gENMONcodigo;
    }

    public BigDecimal getCXCDPGcotiza() {
        return cXCDPGcotiza;
    }

    public void setCXCDPGcotiza(BigDecimal cXCDPGcotiza) {
        this.cXCDPGcotiza = cXCDPGcotiza;
    }

    public Date getCXCDPGfechavcto() {
        return cXCDPGfechavcto;
    }

    public void setCXCDPGfechavcto(Date cXCDPGfechavcto) {
        this.cXCDPGfechavcto = cXCDPGfechavcto;
    }

    public BigDecimal getCXCDPGvalormov() {
        return cXCDPGvalormov;
    }

    public void setCXCDPGvalormov(BigDecimal cXCDPGvalormov) {
        this.cXCDPGvalormov = cXCDPGvalormov;
    }

    public String getCXCDPGcuentacte() {
        return cXCDPGcuentacte;
    }

    public void setCXCDPGcuentacte(String cXCDPGcuentacte) {
        this.cXCDPGcuentacte = cXCDPGcuentacte;
    }

    public String getCXCDPGnumerochq() {
        return cXCDPGnumerochq;
    }

    public void setCXCDPGnumerochq(String cXCDPGnumerochq) {
        this.cXCDPGnumerochq = cXCDPGnumerochq;
    }

    public Date getCXCDPGfechadepos() {
        return cXCDPGfechadepos;
    }

    public void setCXCDPGfechadepos(Date cXCDPGfechadepos) {
        this.cXCDPGfechadepos = cXCDPGfechadepos;
    }

    public String getCXCDPGestcheque() {
        return cXCDPGestcheque;
    }

    public void setCXCDPGestcheque(String cXCDPGestcheque) {
        this.cXCDPGestcheque = cXCDPGestcheque;
    }

    public String getCXCDPGsecubanco() {
        return cXCDPGsecubanco;
    }

    public void setCXCDPGsecubanco(String cXCDPGsecubanco) {
        this.cXCDPGsecubanco = cXCDPGsecubanco;
    }

    public String getCXCDPGsecubancodep() {
        return cXCDPGsecubancodep;
    }

    public void setCXCDPGsecubancodep(String cXCDPGsecubancodep) {
        this.cXCDPGsecubancodep = cXCDPGsecubancodep;
    }

    public CxctCabcredito getCxctCabcredito() {
        return cxctCabcredito;
    }

    public void setCxctCabcredito(CxctCabcredito cxctCabcredito) {
        this.cxctCabcredito = cxctCabcredito;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cxctDetdocumcrPK != null ? cxctDetdocumcrPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CxctDetdocumcr)) {
            return false;
        }
        CxctDetdocumcr other = (CxctDetdocumcr) object;
        if ((this.cxctDetdocumcrPK == null && other.cxctDetdocumcrPK != null) || (this.cxctDetdocumcrPK != null && !this.cxctDetdocumcrPK.equals(other.cxctDetdocumcrPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CxctDetdocumcr[ cxctDetdocumcrPK=" + cxctDetdocumcrPK + " ]";
    }
    
}
