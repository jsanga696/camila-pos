/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Acer
 */
@Entity
@Table(name = "CBPGENR_POLITICABODEGA")
@NamedQueries({
    @NamedQuery(name = "CbpgenrPoliticabodega.findAll", query = "SELECT c FROM CbpgenrPoliticabodega c")})
public class CbpgenrPoliticabodega implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "genpbo_codigo")
    private Long genpboCodigo;
    @JoinColumn(name = "genbod_codigo", referencedColumnName = "genbod_codigo")
    @ManyToOne(fetch = FetchType.LAZY)
    private CbpgenrBodega genbodCodigo;
    @Column(name = "genpbo_estado")
    private Boolean genpboEstado= Boolean.TRUE;
    @JoinColumn(name = "genpol_codigo", referencedColumnName = "genpol_codigo")
    @ManyToOne(fetch = FetchType.LAZY)
    private CbpgenrPoliticabase genpolCodigo;

    public CbpgenrPoliticabodega() {
    }

    public CbpgenrPoliticabodega(Long genpboCodigo) {
        this.genpboCodigo = genpboCodigo;
    }

    public CbpgenrPoliticabodega(CbpgenrBodega genbodCodigo) {
        this.genbodCodigo = genbodCodigo;
    }

    public Long getGenpboCodigo() {
        return genpboCodigo;
    }

    public void setGenpboCodigo(Long genpboCodigo) {
        this.genpboCodigo = genpboCodigo;
    }

    public CbpgenrBodega getGenbodCodigo() {
        return genbodCodigo;
    }

    public void setGenbodCodigo(CbpgenrBodega genbodCodigo) {
        this.genbodCodigo = genbodCodigo;
    }

    public Boolean getGenpboEstado() {
        return genpboEstado;
    }

    public void setGenpboEstado(Boolean genpboEstado) {
        this.genpboEstado = genpboEstado;
    }

    public CbpgenrPoliticabase getGenpolCodigo() {
        return genpolCodigo;
    }

    public void setGenpolCodigo(CbpgenrPoliticabase genpolCodigo) {
        this.genpolCodigo = genpolCodigo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (genpboCodigo != null ? genpboCodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbpgenrPoliticabodega)) {
            return false;
        }
        CbpgenrPoliticabodega other = (CbpgenrPoliticabodega) object;
        if ((this.genpboCodigo == null && other.genpboCodigo != null) || (this.genpboCodigo != null && !this.genpboCodigo.equals(other.genpboCodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CbpgenrPoliticabodega[ genpboCodigo=" + genpboCodigo + " ]";
    }
    
}
