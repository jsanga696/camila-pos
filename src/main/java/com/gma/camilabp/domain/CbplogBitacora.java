/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb6
 */
@Entity
@Table(name = "CBPLOG_BITACORA")
@NamedQueries({
    @NamedQuery(name = "CbplogBitacora.findAll", query = "SELECT c FROM CbplogBitacora c")})
public class CbplogBitacora implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cbpbit_codigo")
    private Long cbpbitCodigo;
    @Basic(optional = false)
    @Column(name = "segusu_codigo")
    private Long segusuCodigo;
    @Size(max = 4)
    @Column(name = "gencaj_codigo")
    private String gencajCodigo;
    @Column(name = "segtur_numero")
    private Long segturNumero;
    @Column(name = "cbpbit_fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cbpbitFecha;
    @Size(max = 30)
    @Column(name = "cbpbit_accion")
    private String cbpbitAccion;
    @Size(max = 60)
    @Column(name = "cbpbit_detalle")
    private String cbpbitDetalle;
    @Column(name = "cbpbit_codaccion")
    private int cbpbitCodaccion;

    public CbplogBitacora() {
    }

    public CbplogBitacora(Long cbpbitCodigo) {
        this.cbpbitCodigo = cbpbitCodigo;
    }

    public CbplogBitacora(Long cbpbitCodigo, Long segusuCodigo) {
        this.cbpbitCodigo = cbpbitCodigo;
        this.segusuCodigo = segusuCodigo;
    }

    public Long getCbpbitCodigo() {
        return cbpbitCodigo;
    }

    public void setCbpbitCodigo(Long cbpbitCodigo) {
        this.cbpbitCodigo = cbpbitCodigo;
    }

    public Long getSegusuCodigo() {
        return segusuCodigo;
    }

    public void setSegusuCodigo(Long segusuCodigo) {
        this.segusuCodigo = segusuCodigo;
    }

    public String getGencajCodigo() {
        return gencajCodigo;
    }

    public void setGencajCodigo(String gencajCodigo) {
        this.gencajCodigo = gencajCodigo;
    }

    public Long getSegturNumero() {
        return segturNumero;
    }

    public void setSegturNumero(Long segturNumero) {
        this.segturNumero = segturNumero;
    }

    public Date getCbpbitFecha() {
        return cbpbitFecha;
    }

    public void setCbpbitFecha(Date cbpbitFecha) {
        this.cbpbitFecha = cbpbitFecha;
    }

    public String getCbpbitAccion() {
        return cbpbitAccion;
    }

    public void setCbpbitAccion(String cbpbitAccion) {
        this.cbpbitAccion = cbpbitAccion;
    }

    public String getCbpbitDetalle() {
        return cbpbitDetalle;
    }

    public void setCbpbitDetalle(String cbpbitDetalle) {
        this.cbpbitDetalle = cbpbitDetalle;
    }

    public int getCbpbitCodaccion() {
        return cbpbitCodaccion;
    }

    public void setCbpbitCodaccion(int cbpbitCodaccion) {
        this.cbpbitCodaccion = cbpbitCodaccion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cbpbitCodigo != null ? cbpbitCodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbplogBitacora)) {
            return false;
        }
        CbplogBitacora other = (CbplogBitacora) object;
        if ((this.cbpbitCodigo == null && other.cbpbitCodigo != null) || (this.cbpbitCodigo != null && !this.cbpbitCodigo.equals(other.cbpbitCodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CbplogBitacora[ cbpbitCodigo=" + cbpbitCodigo + " ]";
    }
    
}
