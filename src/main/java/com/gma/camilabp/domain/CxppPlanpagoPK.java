/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb5
 */
@Embeddable
public class CxppPlanpagoPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENCIA_codigo")
    private String gENCIAcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "CXPPLP_codigo")
    private String cXPPLPcodigo;

    public CxppPlanpagoPK() {
    }

    public CxppPlanpagoPK(String gENCIAcodigo, String cXPPLPcodigo) {
        this.gENCIAcodigo = gENCIAcodigo;
        this.cXPPLPcodigo = cXPPLPcodigo;
    }

    public String getGENCIAcodigo() {
        return gENCIAcodigo;
    }

    public void setGENCIAcodigo(String gENCIAcodigo) {
        this.gENCIAcodigo = gENCIAcodigo;
    }

    public String getCXPPLPcodigo() {
        return cXPPLPcodigo;
    }

    public void setCXPPLPcodigo(String cXPPLPcodigo) {
        this.cXPPLPcodigo = cXPPLPcodigo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gENCIAcodigo != null ? gENCIAcodigo.hashCode() : 0);
        hash += (cXPPLPcodigo != null ? cXPPLPcodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CxppPlanpagoPK)) {
            return false;
        }
        CxppPlanpagoPK other = (CxppPlanpagoPK) object;
        if ((this.gENCIAcodigo == null && other.gENCIAcodigo != null) || (this.gENCIAcodigo != null && !this.gENCIAcodigo.equals(other.gENCIAcodigo))) {
            return false;
        }
        if ((this.cXPPLPcodigo == null && other.cXPPLPcodigo != null) || (this.cXPPLPcodigo != null && !this.cXPPLPcodigo.equals(other.cXPPLPcodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CxppPlanpagoPK[ gENCIAcodigo=" + gENCIAcodigo + ", cXPPLPcodigo=" + cXPPLPcodigo + " ]";
    }
    
}
