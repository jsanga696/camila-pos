/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb6
 */
@Embeddable
public class InvrMotvajustePK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENCIA_codigo")
    private String gENCIAcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "INVMAJ_codigo")
    private String iNVMAJcodigo;

    public InvrMotvajustePK() {
    }

    public InvrMotvajustePK(String gENCIAcodigo, String iNVMAJcodigo) {
        this.gENCIAcodigo = gENCIAcodigo;
        this.iNVMAJcodigo = iNVMAJcodigo;
    }

    public String getGENCIAcodigo() {
        return gENCIAcodigo;
    }

    public void setGENCIAcodigo(String gENCIAcodigo) {
        this.gENCIAcodigo = gENCIAcodigo;
    }

    public String getINVMAJcodigo() {
        return iNVMAJcodigo;
    }

    public void setINVMAJcodigo(String iNVMAJcodigo) {
        this.iNVMAJcodigo = iNVMAJcodigo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gENCIAcodigo != null ? gENCIAcodigo.hashCode() : 0);
        hash += (iNVMAJcodigo != null ? iNVMAJcodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InvrMotvajustePK)) {
            return false;
        }
        InvrMotvajustePK other = (InvrMotvajustePK) object;
        if ((this.gENCIAcodigo == null && other.gENCIAcodigo != null) || (this.gENCIAcodigo != null && !this.gENCIAcodigo.equals(other.gENCIAcodigo))) {
            return false;
        }
        if ((this.iNVMAJcodigo == null && other.iNVMAJcodigo != null) || (this.iNVMAJcodigo != null && !this.iNVMAJcodigo.equals(other.iNVMAJcodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.InvrMotvajustePK[ gENCIAcodigo=" + gENCIAcodigo + ", iNVMAJcodigo=" + iNVMAJcodigo + " ]";
    }
    
}
