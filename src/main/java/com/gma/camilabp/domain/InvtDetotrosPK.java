/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Acer
 */
@Embeddable
public class InvtDetotrosPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "INVCOT_numsecuenc")
    private Long iNVCOTnumsecuenc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "INVDOT_linea")
    private short iNVDOTlinea;

    public InvtDetotrosPK() {
    }

    public InvtDetotrosPK(Long iNVCOTnumsecuenc, short iNVDOTlinea) {
        this.iNVCOTnumsecuenc = iNVCOTnumsecuenc;
        this.iNVDOTlinea = iNVDOTlinea;
    }

    public Long getINVCOTnumsecuenc() {
        return iNVCOTnumsecuenc;
    }

    public void setINVCOTnumsecuenc(Long iNVCOTnumsecuenc) {
        this.iNVCOTnumsecuenc = iNVCOTnumsecuenc;
    }

    public short getINVDOTlinea() {
        return iNVDOTlinea;
    }

    public void setINVDOTlinea(short iNVDOTlinea) {
        this.iNVDOTlinea = iNVDOTlinea;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iNVCOTnumsecuenc != null ? iNVCOTnumsecuenc.hashCode() : 0);
        hash += (int) iNVDOTlinea;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InvtDetotrosPK)) {
            return false;
        }
        InvtDetotrosPK other = (InvtDetotrosPK) object;
        if (this.iNVCOTnumsecuenc != other.iNVCOTnumsecuenc) {
            return false;
        }
        if (this.iNVDOTlinea != other.iNVDOTlinea) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.InvtDetotrosPK[ iNVCOTnumsecuenc=" + iNVCOTnumsecuenc + ", iNVDOTlinea=" + iNVDOTlinea + " ]";
    }
    
}
