/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb5
 */
@Entity
@Table(name = "RTIPCOMP")
@NamedQueries({
    @NamedQuery(name = "Rtipcomp.findAll", query = "SELECT r FROM Rtipcomp r")})
public class Rtipcomp implements Serializable {

    private static final long serialVersionUID = 1L;
    @Size(max = 4)
    @Column(name = "CODIGO")
    private String codigo;
    @Size(max = 150)
    @Column(name = "DESCRIP")
    private String descrip;
    @Column(name = "SERIE")
    private Character serie;
    @Size(max = 10)
    @Column(name = "AUT_SRI")
    private String autSri;
    @Column(name = "COMP_VTA")
    private Character compVta;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;

    public Rtipcomp() {
    }

    public Rtipcomp(Long id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public Character getSerie() {
        return serie;
    }

    public void setSerie(Character serie) {
        this.serie = serie;
    }

    public String getAutSri() {
        return autSri;
    }

    public void setAutSri(String autSri) {
        this.autSri = autSri;
    }

    public Character getCompVta() {
        return compVta;
    }

    public void setCompVta(Character compVta) {
        this.compVta = compVta;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Rtipcomp)) {
            return false;
        }
        Rtipcomp other = (Rtipcomp) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.Rtipcomp[ id=" + id + " ]";
    }
    
}
