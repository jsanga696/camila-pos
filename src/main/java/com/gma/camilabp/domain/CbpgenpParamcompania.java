/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb
 */
@Entity
@Table(name = "CBPGENP_PARAMCOMPANIA")
@NamedQueries({
    @NamedQuery(name = "CbpgenpParamcompania.findAll", query = "SELECT c FROM CbpgenpParamcompania c")})
public class CbpgenpParamcompania implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "gencia_codigo")
    private long genciaCodigo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "genapl_codigo")
    private long genaplCodigo;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "genpco_codigo")
    private Long genpcoCodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "genpco_etiqueta")
    private String genpcoEtiqueta;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "genpco_nombre")
    private String genpcoNombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "genpco_nombrevalor")
    private String genpcoNombrevalor;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "genpco_tipo")
    private String genpcoTipo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "genpco_validacion")
    private String genpcoValidacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "genpco_creauser")
    private long genpcoCreauser;
    @Basic(optional = false)
    @NotNull
    @Column(name = "genpco_creafecha")
    private long genpcoCreafecha;
    @Column(name = "genpco_flagmoficar")
    private Boolean genpcoFlagmoficar;

    public CbpgenpParamcompania() {
    }

    public CbpgenpParamcompania(Long genpcoCodigo) {
        this.genpcoCodigo = genpcoCodigo;
    }

    public CbpgenpParamcompania(Long genpcoCodigo, long genciaCodigo, long genaplCodigo, String genpcoEtiqueta, String genpcoNombre, String genpcoNombrevalor, String genpcoTipo, String genpcoValidacion, long genpcoCreauser, long genpcoCreafecha) {
        this.genpcoCodigo = genpcoCodigo;
        this.genciaCodigo = genciaCodigo;
        this.genaplCodigo = genaplCodigo;
        this.genpcoEtiqueta = genpcoEtiqueta;
        this.genpcoNombre = genpcoNombre;
        this.genpcoNombrevalor = genpcoNombrevalor;
        this.genpcoTipo = genpcoTipo;
        this.genpcoValidacion = genpcoValidacion;
        this.genpcoCreauser = genpcoCreauser;
        this.genpcoCreafecha = genpcoCreafecha;
    }

    public long getGenciaCodigo() {
        return genciaCodigo;
    }

    public void setGenciaCodigo(long genciaCodigo) {
        this.genciaCodigo = genciaCodigo;
    }

    public long getGenaplCodigo() {
        return genaplCodigo;
    }

    public void setGenaplCodigo(long genaplCodigo) {
        this.genaplCodigo = genaplCodigo;
    }

    public Long getGenpcoCodigo() {
        return genpcoCodigo;
    }

    public void setGenpcoCodigo(Long genpcoCodigo) {
        this.genpcoCodigo = genpcoCodigo;
    }

    public String getGenpcoEtiqueta() {
        return genpcoEtiqueta;
    }

    public void setGenpcoEtiqueta(String genpcoEtiqueta) {
        this.genpcoEtiqueta = genpcoEtiqueta;
    }

    public String getGenpcoNombre() {
        return genpcoNombre;
    }

    public void setGenpcoNombre(String genpcoNombre) {
        this.genpcoNombre = genpcoNombre;
    }

    public String getGenpcoNombrevalor() {
        return genpcoNombrevalor;
    }

    public void setGenpcoNombrevalor(String genpcoNombrevalor) {
        this.genpcoNombrevalor = genpcoNombrevalor;
    }

    public String getGenpcoTipo() {
        return genpcoTipo;
    }

    public void setGenpcoTipo(String genpcoTipo) {
        this.genpcoTipo = genpcoTipo;
    }

    public String getGenpcoValidacion() {
        return genpcoValidacion;
    }

    public void setGenpcoValidacion(String genpcoValidacion) {
        this.genpcoValidacion = genpcoValidacion;
    }

    public long getGenpcoCreauser() {
        return genpcoCreauser;
    }

    public void setGenpcoCreauser(long genpcoCreauser) {
        this.genpcoCreauser = genpcoCreauser;
    }

    public long getGenpcoCreafecha() {
        return genpcoCreafecha;
    }

    public void setGenpcoCreafecha(long genpcoCreafecha) {
        this.genpcoCreafecha = genpcoCreafecha;
    }

    public Boolean getGenpcoFlagmoficar() {
        return genpcoFlagmoficar;
    }

    public void setGenpcoFlagmoficar(Boolean genpcoFlagmoficar) {
        this.genpcoFlagmoficar = genpcoFlagmoficar;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (genpcoCodigo != null ? genpcoCodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbpgenpParamcompania)) {
            return false;
        }
        CbpgenpParamcompania other = (CbpgenpParamcompania) object;
        if ((this.genpcoCodigo == null && other.genpcoCodigo != null) || (this.genpcoCodigo != null && !this.genpcoCodigo.equals(other.genpcoCodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CbpgenpParamcompania[ genpcoCodigo=" + genpcoCodigo + " ]";
    }
    
}
