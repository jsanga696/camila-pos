/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb6
 */
@Embeddable
public class GenpParamcompaniaPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENCIA_codigo")
    private String gENCIAcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "GENAPL_codigo")
    private String gENAPLcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "GENPCO_etiqueta")
    private String gENPCOetiqueta;

    public GenpParamcompaniaPK() {
    }

    public GenpParamcompaniaPK(String gENCIAcodigo, String gENAPLcodigo, String gENPCOetiqueta) {
        this.gENCIAcodigo = gENCIAcodigo;
        this.gENAPLcodigo = gENAPLcodigo;
        this.gENPCOetiqueta = gENPCOetiqueta;
    }

    public String getGENCIAcodigo() {
        return gENCIAcodigo;
    }

    public void setGENCIAcodigo(String gENCIAcodigo) {
        this.gENCIAcodigo = gENCIAcodigo;
    }

    public String getGENAPLcodigo() {
        return gENAPLcodigo;
    }

    public void setGENAPLcodigo(String gENAPLcodigo) {
        this.gENAPLcodigo = gENAPLcodigo;
    }

    public String getGENPCOetiqueta() {
        return gENPCOetiqueta;
    }

    public void setGENPCOetiqueta(String gENPCOetiqueta) {
        this.gENPCOetiqueta = gENPCOetiqueta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gENCIAcodigo != null ? gENCIAcodigo.hashCode() : 0);
        hash += (gENAPLcodigo != null ? gENAPLcodigo.hashCode() : 0);
        hash += (gENPCOetiqueta != null ? gENPCOetiqueta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GenpParamcompaniaPK)) {
            return false;
        }
        GenpParamcompaniaPK other = (GenpParamcompaniaPK) object;
        if ((this.gENCIAcodigo == null && other.gENCIAcodigo != null) || (this.gENCIAcodigo != null && !this.gENCIAcodigo.equals(other.gENCIAcodigo))) {
            return false;
        }
        if ((this.gENAPLcodigo == null && other.gENAPLcodigo != null) || (this.gENAPLcodigo != null && !this.gENAPLcodigo.equals(other.gENAPLcodigo))) {
            return false;
        }
        if ((this.gENPCOetiqueta == null && other.gENPCOetiqueta != null) || (this.gENPCOetiqueta != null && !this.gENPCOetiqueta.equals(other.gENPCOetiqueta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.GenpParamcompaniaPK[ gENCIAcodigo=" + gENCIAcodigo + ", gENAPLcodigo=" + gENAPLcodigo + ", gENPCOetiqueta=" + gENPCOetiqueta + " ]";
    }
    
}
