/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import com.gma.camilabp.web.util.Utilidades;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author Acer
 */
@Entity
@Table(name = "CBPGENR_CONTROLTRANS")
@NamedQueries({
    @NamedQuery(name = "CbpgenrControltrans.findAll", query = "SELECT c FROM CbpgenrControltrans c")})
public class CbpgenrControltrans implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "genrcon_id")
    private Long genrconId;
    @JoinColumn(name = "genbod_codigo", referencedColumnName = "genbod_codigo")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private CbpgenrBodega genbodCodigo;
    @JoinColumn(name = "segusu_codigo_sup", referencedColumnName = "segusu_codigo")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private CbpsegmUsuario segusuCodigoSup;
    @Column(name = "genrcon_fechaactpass")
    @Temporal(TemporalType.TIMESTAMP)
    private Date genrconFechaact = new Date();
    @Column(name = "genrcon_periodo")
    private Boolean genrconPeriodo= Boolean.FALSE;
    @Column(name = "genrcon_dias")
    private Long genrconDias;
    @Column(name = "genrcon_transaccion")
    private Boolean genrconTransaccion= Boolean.FALSE;
    @Column(name = "genrcon_cantidad")
    private Long genrconCantidad;
    @Column(name = "genrcon_actualizar")
    private Boolean genrconActualizar= Boolean.FALSE;
    @Column(name = "genrcon_estado")
    private Boolean genrconEstado= Boolean.TRUE;
    @JoinColumn(name = "segusu_codigo", referencedColumnName = "segusu_codigo")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private CbpsegmUsuario segusuCodigo;
    @Column(name = "genrcon_registro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date genrconRegistro = new Date();
    @Column(name = "genrcon_actualizacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date genrconActualizacion;
    @Transient
    private Long transacciones;
    
    public CbpgenrControltrans() {
    }

    public CbpgenrControltrans(Long genrconId) {
        this.genrconId = genrconId;
    }

    public Long getGenrconId() {
        return genrconId;
    }

    public CbpgenrBodega getGenbodCodigo() {
        return genbodCodigo;
    }

    public void setGenbodCodigo(CbpgenrBodega genbodCodigo) {
        this.genbodCodigo = genbodCodigo;
    }

    public CbpsegmUsuario getSegusuCodigoSup() {
        return segusuCodigoSup;
    }

    public void setSegusuCodigoSup(CbpsegmUsuario segusuCodigoSup) {
        this.segusuCodigoSup = segusuCodigoSup;
    }

    public void setGenrconId(Long genrconId) {
        this.genrconId = genrconId;
    }

    public Date getGenrconFechaact() {
        return genrconFechaact;
    }

    public void setGenrconFechaact(Date genrconFechaact) {
        this.genrconFechaact = genrconFechaact;
    }

    public Boolean getGenrconPeriodo() {
        return genrconPeriodo;
    }

    public void setGenrconPeriodo(Boolean genrconPeriodo) {
        this.genrconPeriodo = genrconPeriodo;
    }

    public Long getGenrconDias() {
        return genrconDias;
    }

    public void setGenrconDias(Long genrconDias) {
        this.genrconDias = genrconDias;
    }    

    public Boolean getGenrconTransaccion() {
        return genrconTransaccion;
    }

    public void setGenrconTransaccion(Boolean genrconTransaccion) {
        this.genrconTransaccion = genrconTransaccion;
    }

    public Long getGenrconCantidad() {
        return genrconCantidad;
    }

    public void setGenrconCantidad(Long genrconCantidad) {
        this.genrconCantidad = genrconCantidad;
    }

    public Boolean getGenrconActualizar() {
        return genrconActualizar;
    }

    public void setGenrconActualizar(Boolean genrconActualizar) {
        this.genrconActualizar = genrconActualizar;
    }

    public Boolean getGenrconEstado() {
        return genrconEstado;
    }

    public void setGenrconEstado(Boolean genrconEstado) {
        this.genrconEstado = genrconEstado;
    }

    public CbpsegmUsuario getSegusuCodigo() {
        return segusuCodigo;
    }

    public void setSegusuCodigo(CbpsegmUsuario segusuCodigo) {
        this.segusuCodigo = segusuCodigo;
    }

    public Date getGenrconRegistro() {
        return genrconRegistro;
    }

    public void setGenrconRegistro(Date genrconRegistro) {
        this.genrconRegistro = genrconRegistro;
    }

    public Date getGenrconActualizacion() {
        return genrconActualizacion;
    }

    public void setGenrconActualizacion(Date genrconActualizacion) {
        this.genrconActualizacion = genrconActualizacion;
    }

    public int getDias() {
        return Utilidades.diferenciaDias(genrconFechaact, new Date());
    }

    public Long getTransacciones() {
        return transacciones;
    }

    public void setTransacciones(Long transacciones) {
        this.transacciones = transacciones;
    }    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (genrconId != null ? genrconId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbpgenrControltrans)) {
            return false;
        }
        CbpgenrControltrans other = (CbpgenrControltrans) object;
        if ((this.genrconId == null && other.genrconId != null) || (this.genrconId != null && !this.genrconId.equals(other.genrconId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CbpgenrControltrans[ genrconId=" + genrconId + " ]";
    }
    
}
