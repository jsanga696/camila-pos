/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb
 */
@Entity
@Table(name = "CBPGENR_MODULO")
@NamedQueries({
    @NamedQuery(name = "CbpgenrModulo.findAll", query = "SELECT c FROM CbpgenrModulo c")})
public class CbpgenrModulo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "genmod_codigo")
    private Long genmodCodigo;
    @Size(max = 50)
    @Column(name = "genmod_secuencia")
    private String genmodSecuencia;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "genmod_nombre")
    private String genmodNombre;
    @Basic(optional = false)
    @NotNull
    @Column(name = "genmod_estuso")
    private boolean genmodEstuso;

    public CbpgenrModulo() {
    }

    public CbpgenrModulo(Long genmodCodigo) {
        this.genmodCodigo = genmodCodigo;
    }

    public CbpgenrModulo(Long genmodCodigo, String genmodNombre, boolean genmodEstuso) {
        this.genmodCodigo = genmodCodigo;
        this.genmodNombre = genmodNombre;
        this.genmodEstuso = genmodEstuso;
    }

    public Long getGenmodCodigo() {
        return genmodCodigo;
    }

    public void setGenmodCodigo(Long genmodCodigo) {
        this.genmodCodigo = genmodCodigo;
    }

    public String getGenmodSecuencia() {
        return genmodSecuencia;
    }

    public void setGenmodSecuencia(String genmodSecuencia) {
        this.genmodSecuencia = genmodSecuencia;
    }

    public String getGenmodNombre() {
        return genmodNombre;
    }

    public void setGenmodNombre(String genmodNombre) {
        this.genmodNombre = genmodNombre;
    }

    public boolean getGenmodEstuso() {
        return genmodEstuso;
    }

    public void setGenmodEstuso(boolean genmodEstuso) {
        this.genmodEstuso = genmodEstuso;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (genmodCodigo != null ? genmodCodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbpgenrModulo)) {
            return false;
        }
        CbpgenrModulo other = (CbpgenrModulo) object;
        if ((this.genmodCodigo == null && other.genmodCodigo != null) || (this.genmodCodigo != null && !this.genmodCodigo.equals(other.genmodCodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CbpgenrModulo[ genmodCodigo=" + genmodCodigo + " ]";
    }
    
}
