/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb2
 */
@Embeddable
public class CxprRetencionesPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENCIA_codigo", nullable = false, length = 4)
    private String gENCIAcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "CXPRET_codigo", nullable = false, length = 5)
    private String cXPRETcodigo;

    public CxprRetencionesPK() {
    }

    public CxprRetencionesPK(String gENCIAcodigo, String cXPRETcodigo) {
        this.gENCIAcodigo = gENCIAcodigo;
        this.cXPRETcodigo = cXPRETcodigo;
    }

    public String getGENCIAcodigo() {
        return gENCIAcodigo;
    }

    public void setGENCIAcodigo(String gENCIAcodigo) {
        this.gENCIAcodigo = gENCIAcodigo;
    }

    public String getCXPRETcodigo() {
        return cXPRETcodigo;
    }

    public void setCXPRETcodigo(String cXPRETcodigo) {
        this.cXPRETcodigo = cXPRETcodigo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gENCIAcodigo != null ? gENCIAcodigo.hashCode() : 0);
        hash += (cXPRETcodigo != null ? cXPRETcodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CxprRetencionesPK)) {
            return false;
        }
        CxprRetencionesPK other = (CxprRetencionesPK) object;
        if ((this.gENCIAcodigo == null && other.gENCIAcodigo != null) || (this.gENCIAcodigo != null && !this.gENCIAcodigo.equals(other.gENCIAcodigo))) {
            return false;
        }
        if ((this.cXPRETcodigo == null && other.cXPRETcodigo != null) || (this.cXPRETcodigo != null && !this.cXPRETcodigo.equals(other.cXPRETcodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CxprRetencionesPK[ gENCIAcodigo=" + gENCIAcodigo + ", cXPRETcodigo=" + cXPRETcodigo + " ]";
    }
    
}
