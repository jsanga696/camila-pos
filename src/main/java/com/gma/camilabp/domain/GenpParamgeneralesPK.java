/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb5
 */
@Embeddable
public class GenpParamgeneralesPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENCIA_codigo")
    private String gENCIAcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "GENPGE_tabla")
    private String gENPGEtabla;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "GENPGE_codigo")
    private String gENPGEcodigo;

    public GenpParamgeneralesPK() {
    }

    public GenpParamgeneralesPK(String gENCIAcodigo, String gENPGEtabla, String gENPGEcodigo) {
        this.gENCIAcodigo = gENCIAcodigo;
        this.gENPGEtabla = gENPGEtabla;
        this.gENPGEcodigo = gENPGEcodigo;
    }

    public String getGENCIAcodigo() {
        return gENCIAcodigo;
    }

    public void setGENCIAcodigo(String gENCIAcodigo) {
        this.gENCIAcodigo = gENCIAcodigo;
    }

    public String getGENPGEtabla() {
        return gENPGEtabla;
    }

    public void setGENPGEtabla(String gENPGEtabla) {
        this.gENPGEtabla = gENPGEtabla;
    }

    public String getGENPGEcodigo() {
        return gENPGEcodigo;
    }

    public void setGENPGEcodigo(String gENPGEcodigo) {
        this.gENPGEcodigo = gENPGEcodigo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gENCIAcodigo != null ? gENCIAcodigo.hashCode() : 0);
        hash += (gENPGEtabla != null ? gENPGEtabla.hashCode() : 0);
        hash += (gENPGEcodigo != null ? gENPGEcodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GenpParamgeneralesPK)) {
            return false;
        }
        GenpParamgeneralesPK other = (GenpParamgeneralesPK) object;
        if ((this.gENCIAcodigo == null && other.gENCIAcodigo != null) || (this.gENCIAcodigo != null && !this.gENCIAcodigo.equals(other.gENCIAcodigo))) {
            return false;
        }
        if ((this.gENPGEtabla == null && other.gENPGEtabla != null) || (this.gENPGEtabla != null && !this.gENPGEtabla.equals(other.gENPGEtabla))) {
            return false;
        }
        if ((this.gENPGEcodigo == null && other.gENPGEcodigo != null) || (this.gENPGEcodigo != null && !this.gENPGEcodigo.equals(other.gENPGEcodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.GenpParamgeneralesPK[ gENCIAcodigo=" + gENCIAcodigo + ", gENPGEtabla=" + gENPGEtabla + ", gENPGEcodigo=" + gENPGEcodigo + " ]";
    }
    
}
