/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb6
 */
@Entity
@Table(name = "CXCR_RUTA")
@NamedQueries({
    @NamedQuery(name = "CxcrRuta.findAll", query = "SELECT c FROM CxcrRuta c")})
public class CxcrRuta implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CxcrRutaPK cxcrRutaPK;
    @Size(max = 6)
    @Column(name = "CXCN1_codigo")
    private String cXCN1codigo;
    @Size(max = 6)
    @Column(name = "CXCN2_codigo")
    private String cXCN2codigo;
    @Size(max = 6)
    @Column(name = "CXCN3_codigo")
    private String cXCN3codigo;
    @Size(max = 6)
    @Column(name = "CXCN4_codigo")
    private String cXCN4codigo;
    @Size(max = 6)
    @Column(name = "CXCN5_codigo")
    private String cXCN5codigo;
    @Size(max = 6)
    @Column(name = "CXCN6_codigo")
    private String cXCN6codigo;
    @Size(max = 6)
    @Column(name = "CXCN7_codigo")
    private String cXCN7codigo;
    @Size(max = 6)
    @Column(name = "CXCN8_codigo")
    private String cXCN8codigo;
    @Size(max = 6)
    @Column(name = "CXCN9_codigo")
    private String cXCN9codigo;
    @Column(name = "CXCRUT_tipo")
    private Character cXCRUTtipo;
    @Size(max = 100)
    @Column(name = "CXCRUT_descripcio")
    private String cXCRUTdescripcio;
    @Column(name = "CXCRUT_secuencial")
    private Integer cXCRUTsecuencial;
    @Column(name = "CXCRUT_secnotavta")
    private Integer cXCRUTsecnotavta;
    @Size(max = 10)
    @Column(name = "CXCRUT_rutadistri")
    private String cXCRUTrutadistri;
    @Size(max = 6)
    @Column(name = "CXCRUT_camion")
    private String cXCRUTcamion;
    @Column(name = "CXCRUT_estbloqueo")
    private Character cXCRUTestbloqueo;
    @Column(name = "CXCRUT_seccheques")
    private Integer cXCRUTseccheques;
    @Column(name = "CXCRUT_secanticip")
    private Integer cXCRUTsecanticip;
    @Column(name = "CXCRUT_seccobros")
    private Integer cXCRUTseccobros;
    @Column(name = "CXCRUT_secnotcred")
    private Integer cXCRUTsecnotcred;
    @Column(name = "CXCRUT_seccanjes")
    private Integer cXCRUTseccanjes;
    @Column(name = "CXCRUT_secrecenva")
    private Integer cXCRUTsecrecenva;
    @Column(name = "CXCRUT_secretorno")
    private Integer cXCRUTsecretorno;
    @Column(name = "CXCRUT_secdeposit")
    private Integer cXCRUTsecdeposit;
    @Size(max = 3)
    @Column(name = "CXCCOD_centrofac")
    private String cXCCODcentrofac;
    @Column(name = "CXCRUT_secreten")
    private Integer cXCRUTsecreten;
    @Column(name = "CXCRUT_usooficina")
    private Character cXCRUTusooficina;
    @Column(name = "CXCRUT_usarpedhh")
    private Character cXCRUTusarpedhh;
    @Size(max = 20)
    @Column(name = "CXCRUT_codctacont")
    private String cXCRUTcodctacont;
    @Column(name = "CXCRUT_ESTADO")
    private Character cxcrutEstado;
    @Column(name = "MobileSync")
    private Character mobileSync;
    @Column(name = "CXCRUT_fechaing")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cXCRUTfechaing;
    @Column(name = "CXCRUT_fechamod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cXCRUTfechamod;
    @Column(name = "Sync_estado")
    private Character syncestado;
    @Column(name = "Sync_fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date syncfecha;
    @Size(max = 4)
    @Column(name = "GENBOD_codigo")
    private String gENBODcodigo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CXCRUT_idcodigo")
    private int cXCRUTidcodigo;
    @Column(name = "CXCRUT_coddia")
    private Integer cXCRUTcoddia;
    @Size(max = 1)
    @Column(name = "CXCRUT_exclusivavend")
    private String cXCRUTexclusivavend;

    public CxcrRuta() {
    }

    public CxcrRuta(CxcrRutaPK cxcrRutaPK) {
        this.cxcrRutaPK = cxcrRutaPK;
    }

    public CxcrRuta(CxcrRutaPK cxcrRutaPK, int cXCRUTidcodigo) {
        this.cxcrRutaPK = cxcrRutaPK;
        this.cXCRUTidcodigo = cXCRUTidcodigo;
    }

    public CxcrRuta(String gENCIAcodigo, String cXCRUTcodigo) {
        this.cxcrRutaPK = new CxcrRutaPK(gENCIAcodigo, cXCRUTcodigo);
    }

    public CxcrRutaPK getCxcrRutaPK() {
        return cxcrRutaPK;
    }

    public void setCxcrRutaPK(CxcrRutaPK cxcrRutaPK) {
        this.cxcrRutaPK = cxcrRutaPK;
    }

    public String getCXCN1codigo() {
        return cXCN1codigo;
    }

    public void setCXCN1codigo(String cXCN1codigo) {
        this.cXCN1codigo = cXCN1codigo;
    }

    public String getCXCN2codigo() {
        return cXCN2codigo;
    }

    public void setCXCN2codigo(String cXCN2codigo) {
        this.cXCN2codigo = cXCN2codigo;
    }

    public String getCXCN3codigo() {
        return cXCN3codigo;
    }

    public void setCXCN3codigo(String cXCN3codigo) {
        this.cXCN3codigo = cXCN3codigo;
    }

    public String getCXCN4codigo() {
        return cXCN4codigo;
    }

    public void setCXCN4codigo(String cXCN4codigo) {
        this.cXCN4codigo = cXCN4codigo;
    }

    public String getCXCN5codigo() {
        return cXCN5codigo;
    }

    public void setCXCN5codigo(String cXCN5codigo) {
        this.cXCN5codigo = cXCN5codigo;
    }

    public String getCXCN6codigo() {
        return cXCN6codigo;
    }

    public void setCXCN6codigo(String cXCN6codigo) {
        this.cXCN6codigo = cXCN6codigo;
    }

    public String getCXCN7codigo() {
        return cXCN7codigo;
    }

    public void setCXCN7codigo(String cXCN7codigo) {
        this.cXCN7codigo = cXCN7codigo;
    }

    public String getCXCN8codigo() {
        return cXCN8codigo;
    }

    public void setCXCN8codigo(String cXCN8codigo) {
        this.cXCN8codigo = cXCN8codigo;
    }

    public String getCXCN9codigo() {
        return cXCN9codigo;
    }

    public void setCXCN9codigo(String cXCN9codigo) {
        this.cXCN9codigo = cXCN9codigo;
    }

    public Character getCXCRUTtipo() {
        return cXCRUTtipo;
    }

    public void setCXCRUTtipo(Character cXCRUTtipo) {
        this.cXCRUTtipo = cXCRUTtipo;
    }

    public String getCXCRUTdescripcio() {
        return cXCRUTdescripcio;
    }

    public void setCXCRUTdescripcio(String cXCRUTdescripcio) {
        this.cXCRUTdescripcio = cXCRUTdescripcio;
    }

    public Integer getCXCRUTsecuencial() {
        return cXCRUTsecuencial;
    }

    public void setCXCRUTsecuencial(Integer cXCRUTsecuencial) {
        this.cXCRUTsecuencial = cXCRUTsecuencial;
    }

    public Integer getCXCRUTsecnotavta() {
        return cXCRUTsecnotavta;
    }

    public void setCXCRUTsecnotavta(Integer cXCRUTsecnotavta) {
        this.cXCRUTsecnotavta = cXCRUTsecnotavta;
    }

    public String getCXCRUTrutadistri() {
        return cXCRUTrutadistri;
    }

    public void setCXCRUTrutadistri(String cXCRUTrutadistri) {
        this.cXCRUTrutadistri = cXCRUTrutadistri;
    }

    public String getCXCRUTcamion() {
        return cXCRUTcamion;
    }

    public void setCXCRUTcamion(String cXCRUTcamion) {
        this.cXCRUTcamion = cXCRUTcamion;
    }

    public Character getCXCRUTestbloqueo() {
        return cXCRUTestbloqueo;
    }

    public void setCXCRUTestbloqueo(Character cXCRUTestbloqueo) {
        this.cXCRUTestbloqueo = cXCRUTestbloqueo;
    }

    public Integer getCXCRUTseccheques() {
        return cXCRUTseccheques;
    }

    public void setCXCRUTseccheques(Integer cXCRUTseccheques) {
        this.cXCRUTseccheques = cXCRUTseccheques;
    }

    public Integer getCXCRUTsecanticip() {
        return cXCRUTsecanticip;
    }

    public void setCXCRUTsecanticip(Integer cXCRUTsecanticip) {
        this.cXCRUTsecanticip = cXCRUTsecanticip;
    }

    public Integer getCXCRUTseccobros() {
        return cXCRUTseccobros;
    }

    public void setCXCRUTseccobros(Integer cXCRUTseccobros) {
        this.cXCRUTseccobros = cXCRUTseccobros;
    }

    public Integer getCXCRUTsecnotcred() {
        return cXCRUTsecnotcred;
    }

    public void setCXCRUTsecnotcred(Integer cXCRUTsecnotcred) {
        this.cXCRUTsecnotcred = cXCRUTsecnotcred;
    }

    public Integer getCXCRUTseccanjes() {
        return cXCRUTseccanjes;
    }

    public void setCXCRUTseccanjes(Integer cXCRUTseccanjes) {
        this.cXCRUTseccanjes = cXCRUTseccanjes;
    }

    public Integer getCXCRUTsecrecenva() {
        return cXCRUTsecrecenva;
    }

    public void setCXCRUTsecrecenva(Integer cXCRUTsecrecenva) {
        this.cXCRUTsecrecenva = cXCRUTsecrecenva;
    }

    public Integer getCXCRUTsecretorno() {
        return cXCRUTsecretorno;
    }

    public void setCXCRUTsecretorno(Integer cXCRUTsecretorno) {
        this.cXCRUTsecretorno = cXCRUTsecretorno;
    }

    public Integer getCXCRUTsecdeposit() {
        return cXCRUTsecdeposit;
    }

    public void setCXCRUTsecdeposit(Integer cXCRUTsecdeposit) {
        this.cXCRUTsecdeposit = cXCRUTsecdeposit;
    }

    public String getCXCCODcentrofac() {
        return cXCCODcentrofac;
    }

    public void setCXCCODcentrofac(String cXCCODcentrofac) {
        this.cXCCODcentrofac = cXCCODcentrofac;
    }

    public Integer getCXCRUTsecreten() {
        return cXCRUTsecreten;
    }

    public void setCXCRUTsecreten(Integer cXCRUTsecreten) {
        this.cXCRUTsecreten = cXCRUTsecreten;
    }

    public Character getCXCRUTusooficina() {
        return cXCRUTusooficina;
    }

    public void setCXCRUTusooficina(Character cXCRUTusooficina) {
        this.cXCRUTusooficina = cXCRUTusooficina;
    }

    public Character getCXCRUTusarpedhh() {
        return cXCRUTusarpedhh;
    }

    public void setCXCRUTusarpedhh(Character cXCRUTusarpedhh) {
        this.cXCRUTusarpedhh = cXCRUTusarpedhh;
    }

    public String getCXCRUTcodctacont() {
        return cXCRUTcodctacont;
    }

    public void setCXCRUTcodctacont(String cXCRUTcodctacont) {
        this.cXCRUTcodctacont = cXCRUTcodctacont;
    }

    public Character getCxcrutEstado() {
        return cxcrutEstado;
    }

    public void setCxcrutEstado(Character cxcrutEstado) {
        this.cxcrutEstado = cxcrutEstado;
    }

    public Character getMobileSync() {
        return mobileSync;
    }

    public void setMobileSync(Character mobileSync) {
        this.mobileSync = mobileSync;
    }

    public Date getCXCRUTfechaing() {
        return cXCRUTfechaing;
    }

    public void setCXCRUTfechaing(Date cXCRUTfechaing) {
        this.cXCRUTfechaing = cXCRUTfechaing;
    }

    public Date getCXCRUTfechamod() {
        return cXCRUTfechamod;
    }

    public void setCXCRUTfechamod(Date cXCRUTfechamod) {
        this.cXCRUTfechamod = cXCRUTfechamod;
    }

    public Character getSyncestado() {
        return syncestado;
    }

    public void setSyncestado(Character syncestado) {
        this.syncestado = syncestado;
    }

    public Date getSyncfecha() {
        return syncfecha;
    }

    public void setSyncfecha(Date syncfecha) {
        this.syncfecha = syncfecha;
    }

    public String getGENBODcodigo() {
        return gENBODcodigo;
    }

    public void setGENBODcodigo(String gENBODcodigo) {
        this.gENBODcodigo = gENBODcodigo;
    }

    public int getCXCRUTidcodigo() {
        return cXCRUTidcodigo;
    }

    public void setCXCRUTidcodigo(int cXCRUTidcodigo) {
        this.cXCRUTidcodigo = cXCRUTidcodigo;
    }

    public Integer getCXCRUTcoddia() {
        return cXCRUTcoddia;
    }

    public void setCXCRUTcoddia(Integer cXCRUTcoddia) {
        this.cXCRUTcoddia = cXCRUTcoddia;
    }

    public String getCXCRUTexclusivavend() {
        return cXCRUTexclusivavend;
    }

    public void setCXCRUTexclusivavend(String cXCRUTexclusivavend) {
        this.cXCRUTexclusivavend = cXCRUTexclusivavend;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cxcrRutaPK != null ? cxcrRutaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CxcrRuta)) {
            return false;
        }
        CxcrRuta other = (CxcrRuta) object;
        if ((this.cxcrRutaPK == null && other.cxcrRutaPK != null) || (this.cxcrRutaPK != null && !this.cxcrRutaPK.equals(other.cxcrRutaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CxcrRuta[ cxcrRutaPK=" + cxcrRutaPK + " ]";
    }
    
}
