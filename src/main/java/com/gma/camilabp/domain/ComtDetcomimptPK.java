/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author userdb5
 */
@Embeddable
public class ComtDetcomimptPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "COMCCM_numsecucial")
    private int cOMCCMnumsecucial;
    @Basic(optional = false)
    @NotNull
    @Column(name = "COMDCM_numlinea")
    private int cOMDCMnumlinea;
    @Basic(optional = false)
    @NotNull
    @Column(name = "COMDCM_lineareg")
    private int cOMDCMlineareg;

    public ComtDetcomimptPK() {
    }

    public ComtDetcomimptPK(int cOMCCMnumsecucial, int cOMDCMnumlinea, int cOMDCMlineareg) {
        this.cOMCCMnumsecucial = cOMCCMnumsecucial;
        this.cOMDCMnumlinea = cOMDCMnumlinea;
        this.cOMDCMlineareg = cOMDCMlineareg;
    }

    public int getCOMCCMnumsecucial() {
        return cOMCCMnumsecucial;
    }

    public void setCOMCCMnumsecucial(int cOMCCMnumsecucial) {
        this.cOMCCMnumsecucial = cOMCCMnumsecucial;
    }

    public int getCOMDCMnumlinea() {
        return cOMDCMnumlinea;
    }

    public void setCOMDCMnumlinea(int cOMDCMnumlinea) {
        this.cOMDCMnumlinea = cOMDCMnumlinea;
    }

    public int getCOMDCMlineareg() {
        return cOMDCMlineareg;
    }

    public void setCOMDCMlineareg(int cOMDCMlineareg) {
        this.cOMDCMlineareg = cOMDCMlineareg;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) cOMCCMnumsecucial;
        hash += (int) cOMDCMnumlinea;
        hash += (int) cOMDCMlineareg;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ComtDetcomimptPK)) {
            return false;
        }
        ComtDetcomimptPK other = (ComtDetcomimptPK) object;
        if (this.cOMCCMnumsecucial != other.cOMCCMnumsecucial) {
            return false;
        }
        if (this.cOMDCMnumlinea != other.cOMDCMnumlinea) {
            return false;
        }
        if (this.cOMDCMlineareg != other.cOMDCMlineareg) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.ComtDetcomimptPK[ cOMCCMnumsecucial=" + cOMCCMnumsecucial + ", cOMDCMnumlinea=" + cOMDCMnumlinea + ", cOMDCMlineareg=" + cOMDCMlineareg + " ]";
    }
    
}
