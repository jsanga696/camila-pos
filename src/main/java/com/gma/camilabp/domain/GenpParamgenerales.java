/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb5
 */
@Entity
@Table(name = "GENP_PARAMGENERALES")
@NamedQueries({
    @NamedQuery(name = "GenpParamgenerales.findAll", query = "SELECT g FROM GenpParamgenerales g")})
public class GenpParamgenerales implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected GenpParamgeneralesPK genpParamgeneralesPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "GENPGE_nombre")
    private String gENPGEnombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "GENPGE_aplicacion")
    private String gENPGEaplicacion;

    public GenpParamgenerales() {
    }

    public GenpParamgenerales(GenpParamgeneralesPK genpParamgeneralesPK) {
        this.genpParamgeneralesPK = genpParamgeneralesPK;
    }

    public GenpParamgenerales(GenpParamgeneralesPK genpParamgeneralesPK, String gENPGEnombre, String gENPGEaplicacion) {
        this.genpParamgeneralesPK = genpParamgeneralesPK;
        this.gENPGEnombre = gENPGEnombre;
        this.gENPGEaplicacion = gENPGEaplicacion;
    }

    public GenpParamgenerales(String gENCIAcodigo, String gENPGEtabla, String gENPGEcodigo) {
        this.genpParamgeneralesPK = new GenpParamgeneralesPK(gENCIAcodigo, gENPGEtabla, gENPGEcodigo);
    }

    public GenpParamgeneralesPK getGenpParamgeneralesPK() {
        return genpParamgeneralesPK;
    }

    public void setGenpParamgeneralesPK(GenpParamgeneralesPK genpParamgeneralesPK) {
        this.genpParamgeneralesPK = genpParamgeneralesPK;
    }

    public String getGENPGEnombre() {
        return gENPGEnombre;
    }

    public void setGENPGEnombre(String gENPGEnombre) {
        this.gENPGEnombre = gENPGEnombre;
    }

    public String getGENPGEaplicacion() {
        return gENPGEaplicacion;
    }

    public void setGENPGEaplicacion(String gENPGEaplicacion) {
        this.gENPGEaplicacion = gENPGEaplicacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (genpParamgeneralesPK != null ? genpParamgeneralesPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GenpParamgenerales)) {
            return false;
        }
        GenpParamgenerales other = (GenpParamgenerales) object;
        if ((this.genpParamgeneralesPK == null && other.genpParamgeneralesPK != null) || (this.genpParamgeneralesPK != null && !this.genpParamgeneralesPK.equals(other.genpParamgeneralesPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.GenpParamgenerales[ genpParamgeneralesPK=" + genpParamgeneralesPK + " ]";
    }
    
}
