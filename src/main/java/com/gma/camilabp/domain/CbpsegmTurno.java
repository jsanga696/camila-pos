/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb6
 */
@Entity
@Table(name = "CBPSEGM_TURNO")
@NamedQueries({
    @NamedQuery(name = "CbpsegmTurno.findAll", query = "SELECT c FROM CbpsegmTurno c")})
public class CbpsegmTurno implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "segtur_codigo", nullable = false)
    private Long segturCodigo;
    @Basic(optional = false)
    @Column(name = "segtur_numero")
    private Long segturNumero;
    @Basic(optional = false)
    @NotNull
    @Column(name = "segtur_estado")
    private boolean segturEstado=true;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "segtur_maquina")
    private String segturMaquina;
    @Basic(optional = false)
    @NotNull
    @Column(name = "segtur_apertura")
    @Temporal(TemporalType.TIMESTAMP)
    private Date segturApertura= new Date();
    @Column(name = "segtur_cierre")
    @Temporal(TemporalType.TIMESTAMP)
    private Date segturCierre;
    @JoinColumn(name = "segusu_codigo", referencedColumnName = "segusu_codigo")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private CbpsegmUsuario segusuCodigo;
    @Column(name = "segtur_asiginicial")
    private BigDecimal segturAsiginicial=new BigDecimal("0.00");
    @Column(name = "segtur_adicional")
    private BigDecimal segturAdicional=new BigDecimal("0.00");
    @Column(name = "segtur_egreso")
    private BigDecimal segturEgreso=new BigDecimal("0.00");
    @Column(name = "segtur_valorcierre")
    private BigDecimal segturValorcierre=new BigDecimal("0.00");
    @Column(name = "segtur_efectivo")
    private BigDecimal segturEfectivo=new BigDecimal("0.00");
    @Column(name = "segtur_cheque")
    private BigDecimal segturCheque=new BigDecimal("0.00");
    @Column(name = "segtur_tarjeta")
    private BigDecimal segturTarjeta=new BigDecimal("0.00");
    @Column(name = "segtur_documento")
    private BigDecimal segturDocumento=new BigDecimal("0.00");
    @Column(name = "segtur_credito")
    private BigDecimal segturCredito=new BigDecimal("0.00");
    

    public CbpsegmTurno() {
    }

    public CbpsegmTurno(Long segturCodigo) {
        this.segturCodigo = segturCodigo;
    }

    public CbpsegmTurno(Long segturCodigo, Long segturNumero, boolean segturEstado, String segturMaquina, Date segturApertura) {
        this.segturCodigo = segturCodigo;
        this.segturNumero = segturNumero;
        this.segturEstado = segturEstado;
        this.segturMaquina = segturMaquina;
        this.segturApertura = segturApertura;
    }

    public Long getSegturCodigo() {
        return segturCodigo;
    }

    public void setSegturCodigo(Long segturCodigo) {
        this.segturCodigo = segturCodigo;
    }

    public Long getSegturNumero() {
        return segturNumero;
    }

    public void setSegturNumero(Long segturNumero) {
        this.segturNumero = segturNumero;
    }

    public boolean getSegturEstado() {
        return segturEstado;
    }

    public void setSegturEstado(boolean segturEstado) {
        this.segturEstado = segturEstado;
    }

    public String getSegturMaquina() {
        return segturMaquina;
    }

    public void setSegturMaquina(String segturMaquina) {
        this.segturMaquina = segturMaquina;
    }

    public Date getSegturApertura() {
        return segturApertura;
    }

    public void setSegturApertura(Date segturApertura) {
        this.segturApertura = segturApertura;
    }

    public Date getSegturCierre() {
        return segturCierre;
    }

    public void setSegturCierre(Date segturCierre) {
        this.segturCierre = segturCierre;
    }

    public CbpsegmUsuario getSegusuCodigo() {
        return segusuCodigo;
    }

    public void setSegusuCodigo(CbpsegmUsuario segusuCodigo) {
        this.segusuCodigo = segusuCodigo;
    }

    public BigDecimal getSegturAsiginicial() {
        return segturAsiginicial;
    }

    public void setSegturAsiginicial(BigDecimal segturAsiginicial) {
        this.segturAsiginicial = segturAsiginicial;
    }

    public BigDecimal getSegturAdicional() {
        return segturAdicional;
    }

    public void setSegturAdicional(BigDecimal segturAdicional) {
        this.segturAdicional = segturAdicional;
    }

    public BigDecimal getSegturEgreso() {
        return segturEgreso;
    }

    public void setSegturEgreso(BigDecimal segturEgreso) {
        this.segturEgreso = segturEgreso;
    }

    public BigDecimal getSegturValorcierre() {
        return segturValorcierre;
    }

    public void setSegturValorcierre(BigDecimal segturValorcierre) {
        this.segturValorcierre = segturValorcierre;
    }

    public BigDecimal getSegturEfectivo() {
        return segturEfectivo;
    }

    public void setSegturEfectivo(BigDecimal segturEfectivo) {
        this.segturEfectivo = segturEfectivo;
    }

    public BigDecimal getSegturCheque() {
        return segturCheque;
    }

    public void setSegturCheque(BigDecimal segturCheque) {
        this.segturCheque = segturCheque;
    }

    public BigDecimal getSegturTarjeta() {
        return segturTarjeta;
    }

    public void setSegturTarjeta(BigDecimal segturTarjeta) {
        this.segturTarjeta = segturTarjeta;
    }

    public BigDecimal getSegturDocumento() {
        return segturDocumento;
    }

    public void setSegturDocumento(BigDecimal segturDocumento) {
        this.segturDocumento = segturDocumento;
    }

    public BigDecimal getSegturCredito() {
        return segturCredito;
    }

    public void setSegturCredito(BigDecimal segturCredito) {
        this.segturCredito = segturCredito;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (segturCodigo != null ? segturCodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbpsegmTurno)) {
            return false;
        }
        CbpsegmTurno other = (CbpsegmTurno) object;
        if ((this.segturCodigo == null && other.segturCodigo != null) || (this.segturCodigo != null && !this.segturCodigo.equals(other.segturCodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CbpsegmTurno[ segturCodigo=" + segturCodigo + " ]";
    }
    
}
