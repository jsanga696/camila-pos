/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb6
 */
@Embeddable
public class INVRdetLISTAPRECIOPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "INVLPR_secuencia")
    private Long iNVLPRsecuencia;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "INVLPR_codigo")
    private String iNVLPRcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "INVITM_codigo")
    private String iNVITMcodigo;

    public INVRdetLISTAPRECIOPK() {
    }

    public INVRdetLISTAPRECIOPK(Long iNVLPRsecuencia, String iNVLPRcodigo, String iNVITMcodigo) {
        this.iNVLPRsecuencia = iNVLPRsecuencia;
        this.iNVLPRcodigo = iNVLPRcodigo;
        this.iNVITMcodigo = iNVITMcodigo;
    }

    public Long getINVLPRsecuencia() {
        return iNVLPRsecuencia;
    }

    public void setINVLPRsecuencia(Long iNVLPRsecuencia) {
        this.iNVLPRsecuencia = iNVLPRsecuencia;
    }

    public String getINVLPRcodigo() {
        return iNVLPRcodigo;
    }

    public void setINVLPRcodigo(String iNVLPRcodigo) {
        this.iNVLPRcodigo = iNVLPRcodigo;
    }

    public String getINVITMcodigo() {
        return iNVITMcodigo;
    }

    public void setINVITMcodigo(String iNVITMcodigo) {
        this.iNVITMcodigo = iNVITMcodigo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iNVLPRsecuencia != null ? iNVLPRsecuencia.hashCode() : 0);
        hash += (iNVLPRcodigo != null ? iNVLPRcodigo.hashCode() : 0);
        hash += (iNVITMcodigo != null ? iNVITMcodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof INVRdetLISTAPRECIOPK)) {
            return false;
        }
        INVRdetLISTAPRECIOPK other = (INVRdetLISTAPRECIOPK) object;
        if ((this.iNVLPRsecuencia == null && other.iNVLPRsecuencia != null) || (this.iNVLPRsecuencia != null && !this.iNVLPRsecuencia.equals(other.iNVLPRsecuencia))) {
            return false;
        }
        if ((this.iNVLPRcodigo == null && other.iNVLPRcodigo != null) || (this.iNVLPRcodigo != null && !this.iNVLPRcodigo.equals(other.iNVLPRcodigo))) {
            return false;
        }
        if ((this.iNVITMcodigo == null && other.iNVITMcodigo != null) || (this.iNVITMcodigo != null && !this.iNVITMcodigo.equals(other.iNVITMcodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.INVRdetLISTAPRECIOPK[ iNVLPRsecuencia=" + iNVLPRsecuencia + ", iNVLPRcodigo=" + iNVLPRcodigo + ", iNVITMcodigo=" + iNVITMcodigo + " ]";
    }
    
}
