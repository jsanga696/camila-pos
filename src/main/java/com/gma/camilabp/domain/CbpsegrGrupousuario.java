/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb
 */
@Entity
@Table(name = "CBPSEGR_GRUPOUSUARIO")
@NamedQueries({
    @NamedQuery(name = "CbpsegrGrupousuario.findAll", query = "SELECT c FROM CbpsegrGrupousuario c")})
public class CbpsegrGrupousuario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "seggpp_codigo", nullable = false)
    private Long seggppCodigo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "gencia_codigo")
    private long genciaCodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "seggpp_secuencia")
    private String seggppSecuencia;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "seggpp_descripcio")
    private String seggppDescripcio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "seggpp_estado")
    private boolean seggppEstado= true;
    @Basic(optional = false)
    @NotNull
    @Column(name = "seggpp_creafecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date seggppCreafecha;
    @Basic(optional = false)
    @NotNull
    @Column(name = "seggpp_creausuer")
    private long seggppCreausuer;

    public CbpsegrGrupousuario() {
    }

    public CbpsegrGrupousuario(Long seggppCodigo) {
        this.seggppCodigo = seggppCodigo;
    }

    public CbpsegrGrupousuario(Long seggppCodigo, long genciaCodigo, String seggppSecuencia, String seggppDescripcio, boolean seggppEstado, Date seggppCreafecha, long seggppCreausuer) {
        this.seggppCodigo = seggppCodigo;
        this.genciaCodigo = genciaCodigo;
        this.seggppSecuencia = seggppSecuencia;
        this.seggppDescripcio = seggppDescripcio;
        this.seggppEstado = seggppEstado;
        this.seggppCreafecha = seggppCreafecha;
        this.seggppCreausuer = seggppCreausuer;
    }

    public Long getSeggppCodigo() {
        return seggppCodigo;
    }

    public void setSeggppCodigo(Long seggppCodigo) {
        this.seggppCodigo = seggppCodigo;
    }

    public long getGenciaCodigo() {
        return genciaCodigo;
    }

    public void setGenciaCodigo(long genciaCodigo) {
        this.genciaCodigo = genciaCodigo;
    }

    public String getSeggppSecuencia() {
        return seggppSecuencia;
    }

    public void setSeggppSecuencia(String seggppSecuencia) {
        this.seggppSecuencia = seggppSecuencia;
    }

    public String getSeggppDescripcio() {
        return seggppDescripcio;
    }

    public void setSeggppDescripcio(String seggppDescripcio) {
        this.seggppDescripcio = seggppDescripcio;
    }

    public boolean getSeggppEstado() {
        return seggppEstado;
    }

    public void setSeggppEstado(boolean seggppEstado) {
        this.seggppEstado = seggppEstado;
    }

    public Date getSeggppCreafecha() {
        return seggppCreafecha;
    }

    public void setSeggppCreafecha(Date seggppCreafecha) {
        this.seggppCreafecha = seggppCreafecha;
    }

    public long getSeggppCreausuer() {
        return seggppCreausuer;
    }

    public void setSeggppCreausuer(long seggppCreausuer) {
        this.seggppCreausuer = seggppCreausuer;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (seggppCodigo != null ? seggppCodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbpsegrGrupousuario)) {
            return false;
        }
        CbpsegrGrupousuario other = (CbpsegrGrupousuario) object;
        if ((this.seggppCodigo == null && other.seggppCodigo != null) || (this.seggppCodigo != null && !this.seggppCodigo.equals(other.seggppCodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CbpsegrGrupousuario[ seggppCodigo=" + seggppCodigo + " ]";
    }
    
}
