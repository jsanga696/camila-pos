/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author userdb6
 */
@Entity
@Table(name = "CBPGENR_COBRO")
@NamedQueries({
    @NamedQuery(name = "CbpgenrCobro.findAll", query = "SELECT c FROM CbpgenrCobro c")})
public class CbpgenrCobro implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "gencob_codigo")
    private Long gencobCodigo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FACVTC_secuenvta")
    private Long facvtcSecuenvta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CXCDDE_numsecuen")
    private Long cxcddeNumsecuen;
    @Basic(optional = false)
    @NotNull
    @Column(name = "gencob_tipo")
    private long gencobTipo;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "gencob_cambio")
    private BigDecimal gencobCambio;
    @Column(name = "estado")
    private String estado="A";
    @Column(name = "gencob_valor")
    private BigDecimal gencobValor;
    @Column(name = "gencob_codent")
    private String gencobCodent;
    @Column(name = "gencob_numcuenta")
    private String gencobNumcuenta;
    @Column(name = "gencob_numcheque")
    private String gencobNumcheque;
    @Column(name = "cxcccr_numsecuen")
    private int cxcccrNumsecuen;
    @Column(name = "cxcccr_numdocumcr")
    private String cxcccrNumdocumcr;
    @Column(name = "faccvt_numdocumen")
    private int faccvtNumdocumen;
    

    public CbpgenrCobro() {
    }

    public CbpgenrCobro(Long gencobCodigo) {
        this.gencobCodigo = gencobCodigo;
    }

    public CbpgenrCobro(Long gencobCodigo, long gencobTipo, BigDecimal gencobCambio) {
        this.gencobCodigo = gencobCodigo;
        this.gencobTipo = gencobTipo;
        this.gencobCambio = gencobCambio;
    }

    public Long getGencobCodigo() {
        return gencobCodigo;
    }

    public void setGencobCodigo(Long gencobCodigo) {
        this.gencobCodigo = gencobCodigo;
    }

    public Long getFacvtcSecuenvta() {
        return facvtcSecuenvta;
    }

    public void setFacvtcSecuenvta(Long facvtcSecuenvta) {
        this.facvtcSecuenvta = facvtcSecuenvta;
    }

    public Long getCxcddeNumsecuen() {
        return cxcddeNumsecuen;
    }

    public void setCxcddeNumsecuen(Long cxcddeNumsecuen) {
        this.cxcddeNumsecuen = cxcddeNumsecuen;
    }

    public long getGencobTipo() {
        return gencobTipo;
    }

    public void setGencobTipo(long gencobTipo) {
        this.gencobTipo = gencobTipo;
    }

    public BigDecimal getGencobCambio() {
        return gencobCambio;
    }

    public void setGencobCambio(BigDecimal gencobCambio) {
        this.gencobCambio = gencobCambio;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public BigDecimal getGencobValor() {
        return gencobValor;
    }

    public void setGencobValor(BigDecimal gencobValor) {
        this.gencobValor = gencobValor;
    }

    public String getGencobCodent() {
        return gencobCodent;
    }

    public void setGencobCodent(String gencobCodent) {
        this.gencobCodent = gencobCodent;
    }

    public String getGencobNumcuenta() {
        return gencobNumcuenta;
    }

    public void setGencobNumcuenta(String gencobNumcuenta) {
        this.gencobNumcuenta = gencobNumcuenta;
    }

    public String getGencobNumcheque() {
        return gencobNumcheque;
    }

    public void setGencobNumcheque(String gencobNumcheque) {
        this.gencobNumcheque = gencobNumcheque;
    }

    public int getCxcccrNumsecuen() {
        return cxcccrNumsecuen;
    }

    public void setCxcccrNumsecuen(int cxcccrNumsecuen) {
        this.cxcccrNumsecuen = cxcccrNumsecuen;
    }

    public String getCxcccrNumdocumcr() {
        return cxcccrNumdocumcr;
    }

    public void setCxcccrNumdocumcr(String cxcccrNumdocumcr) {
        this.cxcccrNumdocumcr = cxcccrNumdocumcr;
    }

    public int getFaccvtNumdocumen() {
        return faccvtNumdocumen;
    }

    public void setFaccvtNumdocumen(int faccvtNumdocumen) {
        this.faccvtNumdocumen = faccvtNumdocumen;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gencobCodigo != null ? gencobCodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbpgenrCobro)) {
            return false;
        }
        CbpgenrCobro other = (CbpgenrCobro) object;
        if ((this.gencobCodigo == null && other.gencobCodigo != null) || (this.gencobCodigo != null && !this.gencobCodigo.equals(other.gencobCodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CbpgenrCobro[ gencobCodigo=" + gencobCodigo + " ]";
    }
    
}
