/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb
 */
@Entity
@Table(name = "CBPSEGM_PERFILRESTRICION")
@NamedQueries({
    @NamedQuery(name = "CbpsegmPerfilrestricion.findAll", query = "SELECT c FROM CbpsegmPerfilrestricion c")})
public class CbpsegmPerfilrestricion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "segprr_codigo")
    private Long segprrCodigo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "segprr_estado")
    private boolean segprrEstado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "seggpp_codigo")
    private Long seggppCodigo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "gencia_codigo")
    private long genciaCodigo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "segprr_creafecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date segprrCreafecha;
    @Basic(optional = false)
    @NotNull
    @Column(name = "segprr_creauser")
    private long segprrCreauser;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "segprr_secuencia")
    private String segprrSecuencia;
    @JoinColumn(name = "segacc_codigo", referencedColumnName = "segacc_codigo")
    @ManyToOne(optional = false)
    private CbpsegmAccion segaccCodigo;
    @JoinColumn(name = "segopc_codigo", referencedColumnName = "segopc_codigo")
    @ManyToOne(optional = false)
    private CbpsegmOpcion segopcCodigo;

    public CbpsegmPerfilrestricion() {
    }

    public CbpsegmPerfilrestricion(Long segprrCodigo) {
        this.segprrCodigo = segprrCodigo;
    }

    public CbpsegmPerfilrestricion(Long segprrCodigo, boolean segprrEstado, Long seggppCodigo, long genciaCodigo, Date segprrCreafecha, long segprrCreauser, String segprrSecuencia) {
        this.segprrCodigo = segprrCodigo;
        this.segprrEstado = segprrEstado;
        this.seggppCodigo = seggppCodigo;
        this.genciaCodigo = genciaCodigo;
        this.segprrCreafecha = segprrCreafecha;
        this.segprrCreauser = segprrCreauser;
        this.segprrSecuencia = segprrSecuencia;
    }

    public Long getSegprrCodigo() {
        return segprrCodigo;
    }

    public void setSegprrCodigo(Long segprrCodigo) {
        this.segprrCodigo = segprrCodigo;
    }

    public boolean getSegprrEstado() {
        return segprrEstado;
    }

    public void setSegprrEstado(boolean segprrEstado) {
        this.segprrEstado = segprrEstado;
    }

    public Long getSeggppCodigo() {
        return seggppCodigo;
    }

    public void setSeggppCodigo(long seggppCodigo) {
        this.seggppCodigo = seggppCodigo;
    }

    public long getGenciaCodigo() {
        return genciaCodigo;
    }

    public void setGenciaCodigo(long genciaCodigo) {
        this.genciaCodigo = genciaCodigo;
    }

    public Date getSegprrCreafecha() {
        return segprrCreafecha;
    }

    public void setSegprrCreafecha(Date segprrCreafecha) {
        this.segprrCreafecha = segprrCreafecha;
    }

    public long getSegprrCreauser() {
        return segprrCreauser;
    }

    public void setSegprrCreauser(long segprrCreauser) {
        this.segprrCreauser = segprrCreauser;
    }

    public String getSegprrSecuencia() {
        return segprrSecuencia;
    }

    public void setSegprrSecuencia(String segprrSecuencia) {
        this.segprrSecuencia = segprrSecuencia;
    }

    public CbpsegmAccion getSegaccCodigo() {
        return segaccCodigo;
    }

    public void setSegaccCodigo(CbpsegmAccion segaccCodigo) {
        this.segaccCodigo = segaccCodigo;
    }

    public CbpsegmOpcion getSegopcCodigo() {
        return segopcCodigo;
    }

    public void setSegopcCodigo(CbpsegmOpcion segopcCodigo) {
        this.segopcCodigo = segopcCodigo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (segprrCodigo != null ? segprrCodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbpsegmPerfilrestricion)) {
            return false;
        }
        CbpsegmPerfilrestricion other = (CbpsegmPerfilrestricion) object;
        if ((this.segprrCodigo == null && other.segprrCodigo != null) || (this.segprrCodigo != null && !this.segprrCodigo.equals(other.segprrCodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CbpsegmPerfilrestricion[ segprrCodigo=" + segprrCodigo + " ]";
    }
    
}
