/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author userdb2
 */
@Entity
@Table(name = "CXPR_RETENCIONES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CxprRetenciones.findAll", query = "SELECT c FROM CxprRetenciones c")})
public class CxprRetenciones implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CxprRetencionesPK cxprRetencionesPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CXPRET_tipo", nullable = false)
    private Character cXPRETtipo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "CXPRET_nombre", nullable = false, length = 100)
    private String cXPRETnombre;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CXPRET_Fechaini", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date cXPRETFechaini;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CXPRET_Fechafin", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date cXPRETFechafin;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "CXPRET_referenciacontablecobro", nullable = false, length = 20)
    private String cXPRETreferenciacontablecobro;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "CXPRET_referenciacontablepago", nullable = false, length = 20)
    private String cXPRETreferenciacontablepago;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "CXPRET_porcentaje", nullable = false, precision = 5, scale = 2)
    private BigDecimal cXPRETporcentaje;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CXPRET_porcentajedist", nullable = false, precision = 5, scale = 2)
    private BigDecimal cXPRETporcentajedist;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CXPRET_estado", nullable = false)
    private Character cXPRETestado;
    @Size(max = 5)
    @Column(name = "SRIRET_codigo", length = 5)
    private String sRIRETcodigo;

    public CxprRetenciones() {
    }

    public CxprRetenciones(CxprRetencionesPK cxprRetencionesPK) {
        this.cxprRetencionesPK = cxprRetencionesPK;
    }

    public CxprRetenciones(CxprRetencionesPK cxprRetencionesPK, Character cXPRETtipo, String cXPRETnombre, Date cXPRETFechaini, Date cXPRETFechafin, String cXPRETreferenciacontablecobro, String cXPRETreferenciacontablepago, BigDecimal cXPRETporcentaje, BigDecimal cXPRETporcentajedist, Character cXPRETestado) {
        this.cxprRetencionesPK = cxprRetencionesPK;
        this.cXPRETtipo = cXPRETtipo;
        this.cXPRETnombre = cXPRETnombre;
        this.cXPRETFechaini = cXPRETFechaini;
        this.cXPRETFechafin = cXPRETFechafin;
        this.cXPRETreferenciacontablecobro = cXPRETreferenciacontablecobro;
        this.cXPRETreferenciacontablepago = cXPRETreferenciacontablepago;
        this.cXPRETporcentaje = cXPRETporcentaje;
        this.cXPRETporcentajedist = cXPRETporcentajedist;
        this.cXPRETestado = cXPRETestado;
    }

    public CxprRetenciones(String gENCIAcodigo, String cXPRETcodigo) {
        this.cxprRetencionesPK = new CxprRetencionesPK(gENCIAcodigo, cXPRETcodigo);
    }

    public CxprRetencionesPK getCxprRetencionesPK() {
        return cxprRetencionesPK;
    }

    public void setCxprRetencionesPK(CxprRetencionesPK cxprRetencionesPK) {
        this.cxprRetencionesPK = cxprRetencionesPK;
    }

    public Character getCXPRETtipo() {
        return cXPRETtipo;
    }

    public void setCXPRETtipo(Character cXPRETtipo) {
        this.cXPRETtipo = cXPRETtipo;
    }

    public String getCXPRETnombre() {
        if(cXPRETnombre != null)
            cXPRETnombre = cXPRETnombre.toUpperCase();
        return cXPRETnombre;
    }

    public void setCXPRETnombre(String cXPRETnombre) {
        this.cXPRETnombre = cXPRETnombre;
    }

    public Date getCXPRETFechaini() {
        return cXPRETFechaini;
    }

    public void setCXPRETFechaini(Date cXPRETFechaini) {
        this.cXPRETFechaini = cXPRETFechaini;
    }

    public Date getCXPRETFechafin() {
        return cXPRETFechafin;
    }

    public void setCXPRETFechafin(Date cXPRETFechafin) {
        this.cXPRETFechafin = cXPRETFechafin;
    }

    public String getCXPRETreferenciacontablecobro() {
        return cXPRETreferenciacontablecobro;
    }

    public void setCXPRETreferenciacontablecobro(String cXPRETreferenciacontablecobro) {
        this.cXPRETreferenciacontablecobro = cXPRETreferenciacontablecobro;
    }

    public String getCXPRETreferenciacontablepago() {
        return cXPRETreferenciacontablepago;
    }

    public void setCXPRETreferenciacontablepago(String cXPRETreferenciacontablepago) {
        this.cXPRETreferenciacontablepago = cXPRETreferenciacontablepago;
    }

    public BigDecimal getCXPRETporcentaje() {
        return cXPRETporcentaje;
    }

    public void setCXPRETporcentaje(BigDecimal cXPRETporcentaje) {
        this.cXPRETporcentaje = cXPRETporcentaje;
    }

    public BigDecimal getCXPRETporcentajedist() {
        return cXPRETporcentajedist;
    }

    public void setCXPRETporcentajedist(BigDecimal cXPRETporcentajedist) {
        this.cXPRETporcentajedist = cXPRETporcentajedist;
    }

    public Character getCXPRETestado() {
        return cXPRETestado;
    }

    public void setCXPRETestado(Character cXPRETestado) {
        this.cXPRETestado = cXPRETestado;
    }

    public String getSRIRETcodigo() {
        return sRIRETcodigo;
    }

    public void setSRIRETcodigo(String sRIRETcodigo) {
        this.sRIRETcodigo = sRIRETcodigo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cxprRetencionesPK != null ? cxprRetencionesPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CxprRetenciones)) {
            return false;
        }
        CxprRetenciones other = (CxprRetenciones) object;
        if ((this.cxprRetencionesPK == null && other.cxprRetencionesPK != null) || (this.cxprRetencionesPK != null && !this.cxprRetencionesPK.equals(other.cxprRetencionesPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CxprRetenciones[ cxprRetencionesPK=" + cxprRetencionesPK + " ]";
    }
    
}
