/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb5
 */
@Entity
@Table(name = "CBPACT_TIPOLOGIA_CLIENTE")
@NamedQueries({
    @NamedQuery(name = "CbpactTipologiaCliente.findAll", query = "SELECT c FROM CbpactTipologiaCliente c")})
public class CbpactTipologiaCliente implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "tipologia")
    private Integer tipologia;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "cxcm_cliente_solicitante")
    private String cxcmClienteSolicitante;
    @Column(name = "fecha_ingreso")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaIngreso;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "compra")
    private BigDecimal compra;
    
    @JoinColumn(name = "equipo", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private CbpactEquipo equipo;

    public CbpactTipologiaCliente() {
    }

    public CbpactTipologiaCliente(Long id) {
        this.id = id;
    }

    public CbpactTipologiaCliente(Long id, String cxcmClienteSolicitante) {
        this.id = id;
        this.cxcmClienteSolicitante = cxcmClienteSolicitante;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getTipologia() {
        return tipologia;
    }

    public void setTipologia(Integer tipologia) {
        this.tipologia = tipologia;
    }

    public String getCxcmClienteSolicitante() {
        return cxcmClienteSolicitante;
    }

    public void setCxcmClienteSolicitante(String cxcmClienteSolicitante) {
        this.cxcmClienteSolicitante = cxcmClienteSolicitante;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public BigDecimal getCompra() {
        return compra;
    }

    public void setCompra(BigDecimal compra) {
        this.compra = compra;
    }

    public CbpactEquipo getEquipo() {
        return equipo;
    }

    public void setEquipo(CbpactEquipo equipo) {
        this.equipo = equipo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbpactTipologiaCliente)) {
            return false;
        }
        CbpactTipologiaCliente other = (CbpactTipologiaCliente) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CbpactTipologiaCliente[ id=" + id + " ]";
    }
    
}
