/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import com.gma.camilabp.web.util.Utilidades;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb5
 */
@Entity
@Table(name = "CBPACT_EQUIPO")
@NamedQueries({
    @NamedQuery(name = "CbpactEquipo.findAll", query = "SELECT c FROM CbpactEquipo c")})
public class CbpactEquipo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "fecha_ingreso")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaIngreso;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "capacidad_litros")
    private Double capacidadLitros;
    @Column(name = "fecha_llegada_equipo")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaLlegadaEquipo;
    @Column(name = "estado")
    private Integer estado;
    @Column(name = "placa")
    private String placa;
    @Column(name = "anio_fabricacion")
    private Integer anioFabricacion;
    @Column(name = "cxpm_proveedor")
    private String cxpmProveedor;
    @JoinColumn(name = "model_equipo", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private CbpactModelEquipo modelEquipo;    
    @JoinColumn(name = "genofi_creauser", referencedColumnName = "segusu_codigo")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private CbpsegmUsuario genofiCreauser;
    @JoinColumn(name = "genofi_codigo", referencedColumnName = "genofi_codigo")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private CbpgenrOficina genofiCodigo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "equipo", fetch = FetchType.LAZY)
    private List<CbpactSolicitudEquipo> solicitudesList;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "equipo", fetch = FetchType.LAZY)
    private List<CbpactTipologiaCliente> tipologiaClienteList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "equipo", fetch = FetchType.LAZY)
    private List<CbpactEquipoCategoria> equipoCategoriaList;
    
    @Transient
    private String fechaIngresoText;

    @Transient
    private String proveedorText;
    
    public CbpactEquipo() {
        this.capacidadLitros = 50.0;
        this.estado = 1;
        this.fechaIngreso = new Date();
        this.fechaLlegadaEquipo = new Date();
        this.anioFabricacion = Utilidades.getYear(new Date());
    }

    public CbpactEquipo(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFechaIngresoText() {
        if(fechaIngreso != null){
            SimpleDateFormat dt = new SimpleDateFormat("dd/MM/yyyy"); 
            return dt.format(fechaIngreso);
        }
        return "";
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public void setFechaIngresoText(String fechaIngresoText) {
        this.fechaIngresoText = fechaIngresoText;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public Integer getAnioFabricacion() {
        return anioFabricacion;
    }

    public void setAnioFabricacion(Integer anioFabricacion) {
        this.anioFabricacion = anioFabricacion;
    }

    public String getCxpmProveedor() {
        return cxpmProveedor;
    }

    public void setCxpmProveedor(String cxpmProveedor) {
        this.cxpmProveedor = cxpmProveedor;
    }

    public Date getFechaLlegadaEquipo() {
        return fechaLlegadaEquipo;
    }

    public void setFechaLlegadaEquipo(Date fechaLlegadaEquipo) {
        this.fechaLlegadaEquipo = fechaLlegadaEquipo;
    }

    public Double getCapacidadLitros() {
        return capacidadLitros;
    }

    public void setCapacidadLitros(Double capacidadLitros) {
        this.capacidadLitros = capacidadLitros;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public CbpactModelEquipo getModelEquipo() {
        return modelEquipo;
    }

    public void setModelEquipo(CbpactModelEquipo modelEquipo) {
        this.modelEquipo = modelEquipo;
    }

    public CbpsegmUsuario getGenofiCreauser() {
        return genofiCreauser;
    }

    public void setGenofiCreauser(CbpsegmUsuario genofiCreauser) {
        this.genofiCreauser = genofiCreauser;
    }

    public List<CbpactSolicitudEquipo> getSolicitudesList() {
        return solicitudesList;
    }

    public void setSolicitudesList(List<CbpactSolicitudEquipo> solicitudesList) {
        this.solicitudesList = solicitudesList;
    }

    public CbpgenrOficina getGenofiCodigo() {
        return genofiCodigo;
    }

    public void setGenofiCodigo(CbpgenrOficina genofiCodigo) {
        this.genofiCodigo = genofiCodigo;
    }

    public List<CbpactTipologiaCliente> getTipologiaClienteList() {
        return tipologiaClienteList;
    }

    public void setTipologiaClienteList(List<CbpactTipologiaCliente> tipologiaClienteList) {
        this.tipologiaClienteList = tipologiaClienteList;
    }

    public List<CbpactEquipoCategoria> getEquipoCategoriaList() {
        return equipoCategoriaList;
    }

    public void setEquipoCategoriaList(List<CbpactEquipoCategoria> equipoCategoriaList) {
        this.equipoCategoriaList = equipoCategoriaList;
    }

    public String getProveedorText() {
        return proveedorText;
    }

    public void setProveedorText(String proveedorText) {
        this.proveedorText = proveedorText;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbpactEquipo)) {
            return false;
        }
        CbpactEquipo other = (CbpactEquipo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CbpactEquipo[ id=" + id + " ]";
    }
}
