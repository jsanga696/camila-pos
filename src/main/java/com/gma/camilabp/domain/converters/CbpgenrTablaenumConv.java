/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain.converters;

import com.gma.camilabp.domain.CbpgenrCompania;
import com.gma.camilabp.domain.CbpgenrTablaenum;
import com.gma.camilabp.web.service.EntityManagerLocal;
import java.io.Serializable;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.naming.InitialContext;

/**
 *
 * @author dbriones
 */
public class CbpgenrTablaenumConv implements Converter, Serializable{
    @Override
    public Object getAsObject(
            FacesContext fc,
            UIComponent uic,
            String string) {
		
		if(string==null) return null;
		
        CbpgenrTablaenum objRes = null;
        Boolean transOk = true;
        try {

            //objRes = (CbpgenrTablaenum) sess.get(CbpgenrTablaenum.class, Long.parseLong(string));
            objRes = ((EntityManagerLocal) (new InitialContext().lookup("java:module/EntityManagerService"))).find(CbpgenrTablaenum.class, Long.parseLong(string));

        } catch (Exception e) {
            transOk = false;
            System.out.println("*** Error: " + e.getMessage());
            e.printStackTrace();
        } finally {
            /*  old close */
        }

        if(!transOk) return null;

        return (string == null)
                ? null
                : objRes;
    }

    @Override
    public String getAsString(
            FacesContext fc,
            UIComponent uic,
            Object o) {
        return (o == null)
                ? null
                : Long.toString(((CbpgenrTablaenum) o).getGentenCodigo());
    }
}
