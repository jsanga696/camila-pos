/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain.converters;

import com.gma.camilabp.domain.BanmEntidad;
import com.gma.camilabp.domain.BanmEntidadPK;
import com.gma.camilabp.web.service.EntityManagerLocal;
import java.io.Serializable;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.naming.InitialContext;
//import org.hibernate.Hibernate;
//import org.hibernate.HibernateException;

/**
 *
 * @author userdb6
 */
public class BanmEntidadConv implements Converter, Serializable{

    @Override
    public Object getAsObject(FacesContext fc,UIComponent uic,String string) {		
        if(string==null) return null;
		
        BanmEntidad objRes = null;
        Boolean transOk = true;
        //Session sess = HiberUtil.getSession();
        BanmEntidadPK pk = new BanmEntidadPK();
        System.out.println("/** "+string);
        if(string.equalsIgnoreCase("SELECIONAR"))
            return null;
        String[] lista = string.split(",");
        for (int i = 0; i < lista.length; i++) {
            String valor=lista[i].split("=")[1].split("]")[0];
            System.out.println("VALOR "+i+": "+valor);
            if(i==0)
                pk.setGenciaCodigo(valor);
            else if(i==1)
                pk.setBanentTipo(valor);
            else
                pk.setBanentCodigo(valor);
        }
        try {

            //objRes = (BanmEntidad) sess.get(BanmEntidad.class, pk);
            objRes = ((EntityManagerLocal) (new InitialContext().lookup("java:module/EntityManagerService"))).find(BanmEntidad.class, pk);
        }  catch (Exception e) {
            transOk = false;
            System.out.println("*** Error: " + e.getMessage());
            e.printStackTrace();
        } finally {
            /*  old close */
        }

        if(!transOk) return null;

        return (string == null)? null: objRes;
    }

    @Override
    public String getAsString(FacesContext fc,UIComponent uic,Object o) {
        return (o == null)? null: ((BanmEntidad) o).getBanmEntidadPK().toString();
    }
    
}
