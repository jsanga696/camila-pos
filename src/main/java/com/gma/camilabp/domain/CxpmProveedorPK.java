/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb5
 */
@Embeddable
public class CxpmProveedorPK implements Serializable {

    
    @Column(name = "GENCIA_codigo")
    private String gENCIAcodigo;
    
    @Column(name = "GENOFI_codigo")
    private String gENOFIcodigo;
    
    @Column(name = "CXPPRO_codigo")
    private String cXPPROcodigo;

    public CxpmProveedorPK() {
    }

    public CxpmProveedorPK(String gENCIAcodigo, String gENOFIcodigo, String cXPPROcodigo) {
        this.gENCIAcodigo = gENCIAcodigo;
        this.gENOFIcodigo = gENOFIcodigo;
        this.cXPPROcodigo = cXPPROcodigo;
    }

    public String getGENCIAcodigo() {
        return gENCIAcodigo;
    }

    public void setGENCIAcodigo(String gENCIAcodigo) {
        this.gENCIAcodigo = gENCIAcodigo;
    }

    public String getGENOFIcodigo() {
        return gENOFIcodigo;
    }

    public void setGENOFIcodigo(String gENOFIcodigo) {
        this.gENOFIcodigo = gENOFIcodigo;
    }

    public String getCXPPROcodigo() {
        return cXPPROcodigo;
    }

    public void setCXPPROcodigo(String cXPPROcodigo) {
        this.cXPPROcodigo = cXPPROcodigo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gENCIAcodigo != null ? gENCIAcodigo.hashCode() : 0);
        hash += (gENOFIcodigo != null ? gENOFIcodigo.hashCode() : 0);
        hash += (cXPPROcodigo != null ? cXPPROcodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CxpmProveedorPK)) {
            return false;
        }
        CxpmProveedorPK other = (CxpmProveedorPK) object;
        if ((this.gENCIAcodigo == null && other.gENCIAcodigo != null) || (this.gENCIAcodigo != null && !this.gENCIAcodigo.equals(other.gENCIAcodigo))) {
            return false;
        }
        if ((this.gENOFIcodigo == null && other.gENOFIcodigo != null) || (this.gENOFIcodigo != null && !this.gENOFIcodigo.equals(other.gENOFIcodigo))) {
            return false;
        }
        if ((this.cXPPROcodigo == null && other.cXPPROcodigo != null) || (this.cXPPROcodigo != null && !this.cXPPROcodigo.equals(other.cXPPROcodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CxpmProveedorPK[ gENCIAcodigo=" + gENCIAcodigo + ", gENOFIcodigo=" + gENOFIcodigo + ", cXPPROcodigo=" + cXPPROcodigo + " ]";
    }
    
}
