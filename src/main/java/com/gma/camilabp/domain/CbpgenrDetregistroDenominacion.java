/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author userdb6
 */
@Entity
@Table(name = "CBPGENR_DETREGISTRO_DENOMINACION")
@NamedQueries({
    @NamedQuery(name = "CbpgenrDetregistroDenominacion.findAll", query = "SELECT c FROM CbpgenrDetregistroDenominacion c")})
public class CbpgenrDetregistroDenominacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "gendre_codigo")
    private Long gendreCodigo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "gendre_cantidad")
    private Long gendreCantidad;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "gendre_valor")
    private BigDecimal gendreValor;
    @JoinColumn(name = "genden_codigo", referencedColumnName = "genden_codigo")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private CbpgenmDenominacion gendenCodigo;
    @JoinColumn(name = "genrde_codigo", referencedColumnName = "genrde_codigo")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private CbpgenrRegistroDenominacion genrdeCodigo;

    public CbpgenrDetregistroDenominacion() {
    }

    public CbpgenrDetregistroDenominacion(Long gendreCodigo) {
        this.gendreCodigo = gendreCodigo;
    }

    public CbpgenrDetregistroDenominacion(Long gendreCodigo, Long gendreCantidad, BigDecimal gendreValor) {
        this.gendreCodigo = gendreCodigo;
        this.gendreCantidad = gendreCantidad;
        this.gendreValor = gendreValor;
    }

    public CbpgenrDetregistroDenominacion(Long gendreCantidad, BigDecimal gendreValor, CbpgenmDenominacion gendenCodigo, CbpgenrRegistroDenominacion genrdeCodigo) {
        this.gendreCantidad = gendreCantidad;
        this.gendreValor = gendreValor;
        this.gendenCodigo = gendenCodigo;
        this.genrdeCodigo = genrdeCodigo;
    }

    public Long getGendreCodigo() {
        return gendreCodigo;
    }

    public void setGendreCodigo(Long gendreCodigo) {
        this.gendreCodigo = gendreCodigo;
    }

    public Long getGendreCantidad() {
        return gendreCantidad;
    }

    public void setGendreCantidad(Long gendreCantidad) {
        this.gendreCantidad = gendreCantidad;
    }

    public BigDecimal getGendreValor() {
        return gendreValor;
    }

    public void setGendreValor(BigDecimal gendreValor) {
        this.gendreValor = gendreValor;
    }

    public CbpgenmDenominacion getGendenCodigo() {
        return gendenCodigo;
    }

    public void setGendenCodigo(CbpgenmDenominacion gendenCodigo) {
        this.gendenCodigo = gendenCodigo;
    }

    public CbpgenrRegistroDenominacion getGenrdeCodigo() {
        return genrdeCodigo;
    }

    public void setGenrdeCodigo(CbpgenrRegistroDenominacion genrdeCodigo) {
        this.genrdeCodigo = genrdeCodigo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gendreCodigo != null ? gendreCodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbpgenrDetregistroDenominacion)) {
            return false;
        }
        CbpgenrDetregistroDenominacion other = (CbpgenrDetregistroDenominacion) object;
        if ((this.gendreCodigo == null && other.gendreCodigo != null) || (this.gendreCodigo != null && !this.gendreCodigo.equals(other.gendreCodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CbpgenrDetregistroDenominacion[ gendreCodigo=" + gendreCodigo + " ]";
    }
    
}
