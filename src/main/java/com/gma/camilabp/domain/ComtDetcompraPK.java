/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author userdb5
 */
@Embeddable
public class ComtDetcompraPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "COMCCM_numsecucial")
    private Long cOMCCMnumsecucial;
    @Basic(optional = false)
    @NotNull
    @Column(name = "COMDCM_numlinea")
    private int cOMDCMnumlinea;

    public ComtDetcompraPK() {
    }

    public ComtDetcompraPK(Long cOMCCMnumsecucial, int cOMDCMnumlinea) {
        this.cOMCCMnumsecucial = cOMCCMnumsecucial;
        this.cOMDCMnumlinea = cOMDCMnumlinea;
    }

    public Long getCOMCCMnumsecucial() {
        return cOMCCMnumsecucial;
    }

    public void setCOMCCMnumsecucial(Long cOMCCMnumsecucial) {
        this.cOMCCMnumsecucial = cOMCCMnumsecucial;
    }

    public int getCOMDCMnumlinea() {
        return cOMDCMnumlinea;
    }

    public void setCOMDCMnumlinea(int cOMDCMnumlinea) {
        this.cOMDCMnumlinea = cOMDCMnumlinea;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cOMCCMnumsecucial != null ? cOMCCMnumsecucial.hashCode() : 0);
        hash += (int) cOMDCMnumlinea;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ComtDetcompraPK)) {
            return false;
        }
        ComtDetcompraPK other = (ComtDetcompraPK) object;
        if (this.cOMCCMnumsecucial != other.cOMCCMnumsecucial) {
            return false;
        }
        if (this.cOMDCMnumlinea != other.cOMDCMnumlinea) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.ComtDetcompraPK[ cOMCCMnumsecucial=" + cOMCCMnumsecucial + ", cOMDCMnumlinea=" + cOMDCMnumlinea + " ]";
    }
    
}
