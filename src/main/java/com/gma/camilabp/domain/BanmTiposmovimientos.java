/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Acer
 */
@Entity
@Table(name = "BANM_TIPOSMOVIMIENTOS")
@NamedQueries({
    @NamedQuery(name = "BanmTiposmovimientos.findAll", query = "SELECT b FROM BanmTiposmovimientos b")})
public class BanmTiposmovimientos implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected BanmTiposmovimientosPK banmTiposmovimientosPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "BANTMV_nombre")
    private String bANTMVnombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "BANTMV_tipoentidad")
    private String bANTMVtipoentidad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BANTMV_ingresoegreso")
    private Character bANTMVingresoegreso;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "BANTMV_origenmov")
    private String bANTMVorigenmov;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "BANTMV_referenciacontable")
    private String bANTMVreferenciacontable;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "BANTMV_trancontable")
    private String bANTMVtrancontable;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "BANTMV_subtrancontable")
    private String bANTMVsubtrancontable;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BANTMV_flagdestinatario")
    private Character bANTMVflagdestinatario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BANTMV_flagcentrocosto")
    private Character bANTMVflagcentrocosto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BANTMV_flagcontabilizacion")
    private Character bANTMVflagcontabilizacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BANTMV_flagcambiacuenta")
    private Character bANTMVflagcambiacuenta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BANTMV_estado")
    private Character bANTMVestado;
    @Column(name = "BANTMV_flagcomprobante")
    private Character bANTMVflagcomprobante;

    public BanmTiposmovimientos() {
    }

    public BanmTiposmovimientos(BanmTiposmovimientosPK banmTiposmovimientosPK) {
        this.banmTiposmovimientosPK = banmTiposmovimientosPK;
    }

    public BanmTiposmovimientos(BanmTiposmovimientosPK banmTiposmovimientosPK, String bANTMVnombre, String bANTMVtipoentidad, Character bANTMVingresoegreso, String bANTMVorigenmov, String bANTMVreferenciacontable, String bANTMVtrancontable, String bANTMVsubtrancontable, Character bANTMVflagdestinatario, Character bANTMVflagcentrocosto, Character bANTMVflagcontabilizacion, Character bANTMVflagcambiacuenta, Character bANTMVestado) {
        this.banmTiposmovimientosPK = banmTiposmovimientosPK;
        this.bANTMVnombre = bANTMVnombre;
        this.bANTMVtipoentidad = bANTMVtipoentidad;
        this.bANTMVingresoegreso = bANTMVingresoegreso;
        this.bANTMVorigenmov = bANTMVorigenmov;
        this.bANTMVreferenciacontable = bANTMVreferenciacontable;
        this.bANTMVtrancontable = bANTMVtrancontable;
        this.bANTMVsubtrancontable = bANTMVsubtrancontable;
        this.bANTMVflagdestinatario = bANTMVflagdestinatario;
        this.bANTMVflagcentrocosto = bANTMVflagcentrocosto;
        this.bANTMVflagcontabilizacion = bANTMVflagcontabilizacion;
        this.bANTMVflagcambiacuenta = bANTMVflagcambiacuenta;
        this.bANTMVestado = bANTMVestado;
    }

    public BanmTiposmovimientos(String gENCIAcodigo, String bANTMVcodigo) {
        this.banmTiposmovimientosPK = new BanmTiposmovimientosPK(gENCIAcodigo, bANTMVcodigo);
    }

    public BanmTiposmovimientosPK getBanmTiposmovimientosPK() {
        return banmTiposmovimientosPK;
    }

    public void setBanmTiposmovimientosPK(BanmTiposmovimientosPK banmTiposmovimientosPK) {
        this.banmTiposmovimientosPK = banmTiposmovimientosPK;
    }

    public String getBANTMVnombre() {
        return bANTMVnombre;
    }

    public void setBANTMVnombre(String bANTMVnombre) {
        this.bANTMVnombre = bANTMVnombre;
    }

    public String getBANTMVtipoentidad() {
        return bANTMVtipoentidad;
    }

    public void setBANTMVtipoentidad(String bANTMVtipoentidad) {
        this.bANTMVtipoentidad = bANTMVtipoentidad;
    }

    public Character getBANTMVingresoegreso() {
        return bANTMVingresoegreso;
    }

    public void setBANTMVingresoegreso(Character bANTMVingresoegreso) {
        this.bANTMVingresoegreso = bANTMVingresoegreso;
    }

    public String getBANTMVorigenmov() {
        return bANTMVorigenmov;
    }

    public void setBANTMVorigenmov(String bANTMVorigenmov) {
        this.bANTMVorigenmov = bANTMVorigenmov;
    }

    public String getBANTMVreferenciacontable() {
        return bANTMVreferenciacontable;
    }

    public void setBANTMVreferenciacontable(String bANTMVreferenciacontable) {
        this.bANTMVreferenciacontable = bANTMVreferenciacontable;
    }

    public String getBANTMVtrancontable() {
        return bANTMVtrancontable;
    }

    public void setBANTMVtrancontable(String bANTMVtrancontable) {
        this.bANTMVtrancontable = bANTMVtrancontable;
    }

    public String getBANTMVsubtrancontable() {
        return bANTMVsubtrancontable;
    }

    public void setBANTMVsubtrancontable(String bANTMVsubtrancontable) {
        this.bANTMVsubtrancontable = bANTMVsubtrancontable;
    }

    public Character getBANTMVflagdestinatario() {
        return bANTMVflagdestinatario;
    }

    public void setBANTMVflagdestinatario(Character bANTMVflagdestinatario) {
        this.bANTMVflagdestinatario = bANTMVflagdestinatario;
    }

    public Character getBANTMVflagcentrocosto() {
        return bANTMVflagcentrocosto;
    }

    public void setBANTMVflagcentrocosto(Character bANTMVflagcentrocosto) {
        this.bANTMVflagcentrocosto = bANTMVflagcentrocosto;
    }

    public Character getBANTMVflagcontabilizacion() {
        return bANTMVflagcontabilizacion;
    }

    public void setBANTMVflagcontabilizacion(Character bANTMVflagcontabilizacion) {
        this.bANTMVflagcontabilizacion = bANTMVflagcontabilizacion;
    }

    public Character getBANTMVflagcambiacuenta() {
        return bANTMVflagcambiacuenta;
    }

    public void setBANTMVflagcambiacuenta(Character bANTMVflagcambiacuenta) {
        this.bANTMVflagcambiacuenta = bANTMVflagcambiacuenta;
    }

    public Character getBANTMVestado() {
        return bANTMVestado;
    }

    public void setBANTMVestado(Character bANTMVestado) {
        this.bANTMVestado = bANTMVestado;
    }

    public Character getBANTMVflagcomprobante() {
        return bANTMVflagcomprobante;
    }

    public void setBANTMVflagcomprobante(Character bANTMVflagcomprobante) {
        this.bANTMVflagcomprobante = bANTMVflagcomprobante;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (banmTiposmovimientosPK != null ? banmTiposmovimientosPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BanmTiposmovimientos)) {
            return false;
        }
        BanmTiposmovimientos other = (BanmTiposmovimientos) object;
        if ((this.banmTiposmovimientosPK == null && other.banmTiposmovimientosPK != null) || (this.banmTiposmovimientosPK != null && !this.banmTiposmovimientosPK.equals(other.banmTiposmovimientosPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.BanmTiposmovimientos[ banmTiposmovimientosPK=" + banmTiposmovimientosPK + " ]";
    }
    
}
