/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb
 */
@Entity
@Table(name = "CBPGENP_PARAMGENERALES")
@NamedQueries({
    @NamedQuery(name = "CbpgenpParamgenerales.findAll", query = "SELECT c FROM CbpgenpParamgenerales c")})
public class CbpgenpParamgenerales implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "gencia_codigo")
    private long genciaCodigo;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "genpge_tabla")
    private Long genpgeTabla;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "genpge_secuencia")
    private String genpgeSecuencia;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "genpge_codigo")
    private String genpgeCodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "genpge_nombre")
    private String genpgeNombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "genpge_aplicacion")
    private String genpgeAplicacion;
    @Column(name = "genpge_creauser")
    private BigInteger genpgeCreauser;
    @Column(name = "genpge_creafecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date genpgeCreafecha;

    public CbpgenpParamgenerales() {
    }

    public CbpgenpParamgenerales(Long genpgeTabla) {
        this.genpgeTabla = genpgeTabla;
    }

    public CbpgenpParamgenerales(Long genpgeTabla, long genciaCodigo, String genpgeSecuencia, String genpgeCodigo, String genpgeNombre, String genpgeAplicacion) {
        this.genpgeTabla = genpgeTabla;
        this.genciaCodigo = genciaCodigo;
        this.genpgeSecuencia = genpgeSecuencia;
        this.genpgeCodigo = genpgeCodigo;
        this.genpgeNombre = genpgeNombre;
        this.genpgeAplicacion = genpgeAplicacion;
    }

    public long getGenciaCodigo() {
        return genciaCodigo;
    }

    public void setGenciaCodigo(long genciaCodigo) {
        this.genciaCodigo = genciaCodigo;
    }

    public Long getGenpgeTabla() {
        return genpgeTabla;
    }

    public void setGenpgeTabla(Long genpgeTabla) {
        this.genpgeTabla = genpgeTabla;
    }

    public String getGenpgeSecuencia() {
        return genpgeSecuencia;
    }

    public void setGenpgeSecuencia(String genpgeSecuencia) {
        this.genpgeSecuencia = genpgeSecuencia;
    }

    public String getGenpgeCodigo() {
        return genpgeCodigo;
    }

    public void setGenpgeCodigo(String genpgeCodigo) {
        this.genpgeCodigo = genpgeCodigo;
    }

    public String getGenpgeNombre() {
        return genpgeNombre;
    }

    public void setGenpgeNombre(String genpgeNombre) {
        this.genpgeNombre = genpgeNombre;
    }

    public String getGenpgeAplicacion() {
        return genpgeAplicacion;
    }

    public void setGenpgeAplicacion(String genpgeAplicacion) {
        this.genpgeAplicacion = genpgeAplicacion;
    }

    public BigInteger getGenpgeCreauser() {
        return genpgeCreauser;
    }

    public void setGenpgeCreauser(BigInteger genpgeCreauser) {
        this.genpgeCreauser = genpgeCreauser;
    }

    public Date getGenpgeCreafecha() {
        return genpgeCreafecha;
    }

    public void setGenpgeCreafecha(Date genpgeCreafecha) {
        this.genpgeCreafecha = genpgeCreafecha;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (genpgeTabla != null ? genpgeTabla.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbpgenpParamgenerales)) {
            return false;
        }
        CbpgenpParamgenerales other = (CbpgenpParamgenerales) object;
        if ((this.genpgeTabla == null && other.genpgeTabla != null) || (this.genpgeTabla != null && !this.genpgeTabla.equals(other.genpgeTabla))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CbpgenpParamgenerales[ genpgeTabla=" + genpgeTabla + " ]";
    }
    
}
