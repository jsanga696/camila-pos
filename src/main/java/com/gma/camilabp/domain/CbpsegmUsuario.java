/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
//import org.hibernate.annotations.Formula;

/**
 *
 * @author userdb
 */
@Entity
@Table(name = "CBPSEGM_USUARIO")
@NamedQueries({
    @NamedQuery(name = "CbpsegmUsuario.findAll", query = "SELECT c FROM CbpsegmUsuario c")})
public class CbpsegmUsuario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "segusu_codigo")
    private Long segusuCodigo;
    @JoinColumn(name = "gencia_codigo", referencedColumnName = "gencia_codigo")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private CbpgenrCompania genciaCodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "segusu_login")
    private String segusuLogin;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "segusu_nombre")
    private String segusuNombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "segusu_apellido")
    private String segusuApellido;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "segusu_abreviado")
    private String segusuAbreviado;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "segusu_clave")
    private String segusuClave;
    @Basic(optional = false)
    @NotNull
    @Column(name = "segusu_estado")
    private boolean segusuEstado;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "segusu_usuariored")
    private String segusuUsuariored;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "segusu_usuariobas")
    private String segusuUsuariobas;
    @Basic(optional = false)
    @NotNull
    @Column(name = "segusu_tipo")
    private long segusuTipo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "segusu_tipocodigo")
    private int segusuTipocodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "segusu_email")
    private String segusuEmail;
    @Basic(optional = false)
    @NotNull
    @Column(name = "segusu_master")
    private int segusuMaster;
    @Basic(optional = false)
    @NotNull
    @Column(name = "segusu_cambioclave")
    private int segusuCambioclave;
    @Basic(optional = false)
    @NotNull
    @Column(name = "segusu_nomina")
    private long segusuNomina;
    @Basic(optional = false)
    @NotNull
    @Column(name = "segusu_oficredito")
    private long segusuOficredito;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "segusu_codigorela")
    private String segusuCodigorela;
    @Column(name = "segusu_creafecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date segusuCreafecha;
    @Column(name = "segusu_creauser")
    private BigInteger segusuCreauser;
    @JoinColumn(name = "seggpp_codigo", referencedColumnName = "seggpp_codigo")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private CbpsegrGrupousuario seggppCodigo;
    @Transient
    private boolean seleccionado=false;
    
    /*@OneToMany(cascade = CascadeType.ALL, mappedBy = "genofiCreauser", fetch = FetchType.LAZY)
    private List<CbpactEquipo> cbpactEquipoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "genofiCreauser", fetch = FetchType.LAZY)
    private List<CbpactModelEquipo> cbpactModelEquipoList;*/
    
    //@Formula(value = "(SELECT c.GENCAJ_codigo FROM GENM_CAJA c WHERE c.SEGUSU_codigo=segusu_login AND c.GENCAJ_estado='A')")
    @Transient
    private String codigoCaja;        

    public CbpsegmUsuario() {
    }

    public CbpsegmUsuario(Long segusuCodigo) {
        this.segusuCodigo = segusuCodigo;
    }

    public CbpsegmUsuario(Long segusuCodigo, String segusuLogin, String segusuNombre, String segusuApellido, String segusuAbreviado, String segusuClave, boolean segusuEstado, String segusuUsuariored, String segusuUsuariobas, long segusuTipo, int segusuTipocodigo, String segusuEmail, int segusuMaster, int segusuCambioclave, long segusuNomina, long segusuOficredito, String segusuCodigorela) {
        this.segusuCodigo = segusuCodigo;
        //this.segusuLogin = segusuLogin;
        this.segusuNombre = segusuNombre;
        this.segusuApellido = segusuApellido;
        this.segusuAbreviado = segusuAbreviado;
        this.segusuClave = segusuClave;
        this.segusuEstado = segusuEstado;
        this.segusuUsuariored = segusuUsuariored;
        this.segusuUsuariobas = segusuUsuariobas;
        this.segusuTipo = segusuTipo;
        this.segusuTipocodigo = segusuTipocodigo;
        this.segusuEmail = segusuEmail;
        this.segusuMaster = segusuMaster;
        this.segusuCambioclave = segusuCambioclave;
        this.segusuNomina = segusuNomina;
        this.segusuOficredito = segusuOficredito;
        this.segusuCodigorela = segusuCodigorela;
    }

    public CbpgenrCompania getGenciaCodigo() {
        return genciaCodigo;
    }

    public void setGenciaCodigo(CbpgenrCompania genciaCodigo) {
        this.genciaCodigo = genciaCodigo;
    }

    public Long getSegusuCodigo() {
        return segusuCodigo;
    }

    public void setSegusuCodigo(Long segusuCodigo) {
        this.segusuCodigo = segusuCodigo;
    }

    public String getSegusuLogin() {
        return segusuLogin;
    }

    public void setSegusuLogin(String segusuLogin) {
        this.segusuLogin = segusuLogin;
    }

    public String getSegusuNombre() {
        return segusuNombre;
    }

    public void setSegusuNombre(String segusuNombre) {
        this.segusuNombre = segusuNombre;
    }

    public String getSegusuApellido() {
        return segusuApellido;
    }

    public void setSegusuApellido(String segusuApellido) {
        this.segusuApellido = segusuApellido;
    }

    public String getSegusuAbreviado() {
        return segusuAbreviado;
    }

    public void setSegusuAbreviado(String segusuAbreviado) {
        this.segusuAbreviado = segusuAbreviado;
    }

    public String getSegusuClave() {
        return segusuClave;
    }

    public void setSegusuClave(String segusuClave) {
        this.segusuClave = segusuClave;
    }

    public boolean getSegusuEstado() {
        return segusuEstado;
    }

    public void setSegusuEstado(boolean segusuEstado) {
        this.segusuEstado = segusuEstado;
    }

    public String getSegusuUsuariored() {
        return segusuUsuariored;
    }

    public void setSegusuUsuariored(String segusuUsuariored) {
        this.segusuUsuariored = segusuUsuariored;
    }

    public String getSegusuUsuariobas() {
        return segusuUsuariobas;
    }

    public void setSegusuUsuariobas(String segusuUsuariobas) {
        this.segusuUsuariobas = segusuUsuariobas;
    }

    public long getSegusuTipo() {
        return segusuTipo;
    }

    public void setSegusuTipo(long segusuTipo) {
        this.segusuTipo = segusuTipo;
    }

    public int getSegusuTipocodigo() {
        return segusuTipocodigo;
    }

    public void setSegusuTipocodigo(int segusuTipocodigo) {
        this.segusuTipocodigo = segusuTipocodigo;
    }

    public String getSegusuEmail() {
        return segusuEmail;
    }

    public void setSegusuEmail(String segusuEmail) {
        this.segusuEmail = segusuEmail;
    }

    public int getSegusuMaster() {
        return segusuMaster;
    }

    public void setSegusuMaster(int segusuMaster) {
        this.segusuMaster = segusuMaster;
    }

    public int getSegusuCambioclave() {
        return segusuCambioclave;
    }

    public void setSegusuCambioclave(int segusuCambioclave) {
        this.segusuCambioclave = segusuCambioclave;
    }

    public long getSegusuNomina() {
        return segusuNomina;
    }

    public void setSegusuNomina(long segusuNomina) {
        this.segusuNomina = segusuNomina;
    }

    public long getSegusuOficredito() {
        return segusuOficredito;
    }

    public void setSegusuOficredito(long segusuOficredito) {
        this.segusuOficredito = segusuOficredito;
    }

    public String getSegusuCodigorela() {
        return segusuCodigorela;
    }

    public void setSegusuCodigorela(String segusuCodigorela) {
        this.segusuCodigorela = segusuCodigorela;
    }

    public Date getSegusuCreafecha() {
        return segusuCreafecha;
    }

    public void setSegusuCreafecha(Date segusuCreafecha) {
        this.segusuCreafecha = segusuCreafecha;
    }

    public BigInteger getSegusuCreauser() {
        return segusuCreauser;
    }

    public void setSegusuCreauser(BigInteger segusuCreauser) {
        this.segusuCreauser = segusuCreauser;
    }

    public CbpsegrGrupousuario getSeggppCodigo() {
        return seggppCodigo;
    }

    public void setSeggppCodigo(CbpsegrGrupousuario seggppCodigo) {
        this.seggppCodigo = seggppCodigo;
    }

    public boolean isSeleccionado() {
        return seleccionado;
    }

    public void setSeleccionado(boolean seleccionado) {
        this.seleccionado = seleccionado;
    }

    public String getCodigoCaja() {
        return codigoCaja;
    }

    public void setCodigoCaja(String codigoCaja) {
        this.codigoCaja = codigoCaja;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (segusuCodigo != null ? segusuCodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbpsegmUsuario)) {
            return false;
        }
        CbpsegmUsuario other = (CbpsegmUsuario) object;
        if ((this.segusuCodigo == null && other.segusuCodigo != null) || (this.segusuCodigo != null && !this.segusuCodigo.equals(other.segusuCodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CbpsegmUsuario[ segusuCodigo=" + segusuCodigo + " ]";
    }

    /*public List<CbpactEquipo> getCbpactEquipoList() {
        return cbpactEquipoList;
    }

    public void setCbpactEquipoList(List<CbpactEquipo> cbpactEquipoList) {
        this.cbpactEquipoList = cbpactEquipoList;
    }

    public List<CbpactModelEquipo> getCbpactModelEquipoList() {
        return cbpactModelEquipoList;
    }

    public void setCbpactModelEquipoList(List<CbpactModelEquipo> cbpactModelEquipoList) {
        this.cbpactModelEquipoList = cbpactModelEquipoList;
    }*/
    
}
