/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb6
 */
@Entity
@Table(name = "GENR_DOCUMENTO")
@NamedQueries({
    @NamedQuery(name = "GenrDocumento.findAll", query = "SELECT g FROM GenrDocumento g")})
public class GenrDocumento implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected GenrDocumentoPK genrDocumentoPK;
    @Size(max = 30)
    @Column(name = "GENTDO_nombre")
    private String gENTDOnombre;
    @Column(name = "GENTDO_contador")
    private Integer gENTDOcontador;
    @Size(max = 2)
    @Column(name = "GENTDO_tipo")
    private String gENTDOtipo;
    @Column(name = "GENTDO_estado")
    private Character gENTDOestado;
    @Size(max = 1)
    @Column(name = "GENTDO_generacosto")
    private String gENTDOgeneracosto;

    public GenrDocumento() {
    }

    public GenrDocumento(GenrDocumentoPK genrDocumentoPK) {
        this.genrDocumentoPK = genrDocumentoPK;
    }

    public GenrDocumento(String gENCIAcodigo, String gENOFIcodigo, String gENMODcodigo, String gENTDOcodigo) {
        this.genrDocumentoPK = new GenrDocumentoPK(gENCIAcodigo, gENOFIcodigo, gENMODcodigo, gENTDOcodigo);
    }

    public GenrDocumentoPK getGenrDocumentoPK() {
        return genrDocumentoPK;
    }

    public void setGenrDocumentoPK(GenrDocumentoPK genrDocumentoPK) {
        this.genrDocumentoPK = genrDocumentoPK;
    }

    public String getGENTDOnombre() {
        return gENTDOnombre;
    }

    public void setGENTDOnombre(String gENTDOnombre) {
        this.gENTDOnombre = gENTDOnombre;
    }

    public Integer getGENTDOcontador() {
        return gENTDOcontador;
    }

    public void setGENTDOcontador(Integer gENTDOcontador) {
        this.gENTDOcontador = gENTDOcontador;
    }

    public String getGENTDOtipo() {
        return gENTDOtipo;
    }

    public void setGENTDOtipo(String gENTDOtipo) {
        this.gENTDOtipo = gENTDOtipo;
    }

    public Character getGENTDOestado() {
        return gENTDOestado;
    }

    public void setGENTDOestado(Character gENTDOestado) {
        this.gENTDOestado = gENTDOestado;
    }

    public String getGENTDOgeneracosto() {
        return gENTDOgeneracosto;
    }

    public void setGENTDOgeneracosto(String gENTDOgeneracosto) {
        this.gENTDOgeneracosto = gENTDOgeneracosto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (genrDocumentoPK != null ? genrDocumentoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GenrDocumento)) {
            return false;
        }
        GenrDocumento other = (GenrDocumento) object;
        if ((this.genrDocumentoPK == null && other.genrDocumentoPK != null) || (this.genrDocumentoPK != null && !this.genrDocumentoPK.equals(other.genrDocumentoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.GenrDocumento[ genrDocumentoPK=" + genrDocumentoPK + " ]";
    }
    
}
