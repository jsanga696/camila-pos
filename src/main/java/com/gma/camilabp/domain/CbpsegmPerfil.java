/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author userdb
 */
@Entity
@Table(name = "CBPSEGM_PERFIL")
@NamedQueries({
    @NamedQuery(name = "CbpsegmPerfil.findAll", query = "SELECT c FROM CbpsegmPerfil c")})
public class CbpsegmPerfil implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)    
    @Column(name = "segper_codigo")
    private Long segperCodigo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "segper_estado")
    private boolean segperEstado=true;
    @Basic(optional = false)
    @NotNull
    @Column(name = "seggpp_codigo")
    private long seggppCodigo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "gencia_codigo")
    private long genciaCodigo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "segper_creafecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date segperCreafecha= new Date();
    @Basic(optional = false)
    @NotNull
    @Column(name = "segper_creauser")
    private long segperCreauser;
    @JoinColumn(name = "segacc_codigo", referencedColumnName = "segacc_codigo")
    @ManyToOne(optional = false)
    private CbpsegmAccion segaccCodigo;
    @JoinColumn(name = "segopc_codigo", referencedColumnName = "segopc_codigo")
    @ManyToOne(optional = false)
    private CbpsegmOpcion segopcCodigo;

    public CbpsegmPerfil() {
    }

    public CbpsegmPerfil(Long segperCodigo) {
        this.segperCodigo = segperCodigo;
    }

    public CbpsegmPerfil(Long segperCodigo, boolean segperEstado, long seggppCodigo, long genciaCodigo, Date segperCreafecha, long segperCreauser) {
        this.segperCodigo = segperCodigo;
        this.segperEstado = segperEstado;
        this.seggppCodigo = seggppCodigo;
        this.genciaCodigo = genciaCodigo;
        this.segperCreafecha = segperCreafecha;
        this.segperCreauser = segperCreauser;
    }

    public Long getSegperCodigo() {
        return segperCodigo;
    }

    public void setSegperCodigo(Long segperCodigo) {
        this.segperCodigo = segperCodigo;
    }

    public boolean getSegperEstado() {
        return segperEstado;
    }

    public void setSegperEstado(boolean segperEstado) {
        this.segperEstado = segperEstado;
    }

    public long getSeggppCodigo() {
        return seggppCodigo;
    }

    public void setSeggppCodigo(long seggppCodigo) {
        this.seggppCodigo = seggppCodigo;
    }

    public long getGenciaCodigo() {
        return genciaCodigo;
    }

    public void setGenciaCodigo(long genciaCodigo) {
        this.genciaCodigo = genciaCodigo;
    }

    public Date getSegperCreafecha() {
        return segperCreafecha;
    }

    public void setSegperCreafecha(Date segperCreafecha) {
        this.segperCreafecha = segperCreafecha;
    }

    public long getSegperCreauser() {
        return segperCreauser;
    }

    public void setSegperCreauser(long segperCreauser) {
        this.segperCreauser = segperCreauser;
    }

    public CbpsegmAccion getSegaccCodigo() {
        return segaccCodigo;
    }

    public void setSegaccCodigo(CbpsegmAccion segaccCodigo) {
        this.segaccCodigo = segaccCodigo;
    }

    public CbpsegmOpcion getSegopcCodigo() {
        return segopcCodigo;
    }

    public void setSegopcCodigo(CbpsegmOpcion segopcCodigo) {
        this.segopcCodigo = segopcCodigo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (segperCodigo != null ? segperCodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbpsegmPerfil)) {
            return false;
        }
        CbpsegmPerfil other = (CbpsegmPerfil) object;
        if ((this.segperCodigo == null && other.segperCodigo != null) || (this.segperCodigo != null && !this.segperCodigo.equals(other.segperCodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CbpsegmPerfil[ segperCodigo=" + segperCodigo + " ]";
    }
    
}
