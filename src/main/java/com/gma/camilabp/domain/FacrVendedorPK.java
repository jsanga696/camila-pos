/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb6
 */
@Embeddable
public class FacrVendedorPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENCIA_codigo")
    private String gENCIAcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "FACVDR_codigo")
    private String fACVDRcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENOFI_codigo")
    private String gENOFIcodigo;

    public FacrVendedorPK() {
    }

    public FacrVendedorPK(String gENCIAcodigo, String fACVDRcodigo, String gENOFIcodigo) {
        this.gENCIAcodigo = gENCIAcodigo;
        this.fACVDRcodigo = fACVDRcodigo;
        this.gENOFIcodigo = gENOFIcodigo;
    }

    public String getGENCIAcodigo() {
        return gENCIAcodigo;
    }

    public void setGENCIAcodigo(String gENCIAcodigo) {
        this.gENCIAcodigo = gENCIAcodigo;
    }

    public String getFACVDRcodigo() {
        return fACVDRcodigo;
    }

    public void setFACVDRcodigo(String fACVDRcodigo) {
        this.fACVDRcodigo = fACVDRcodigo;
    }

    public String getGENOFIcodigo() {
        return gENOFIcodigo;
    }

    public void setGENOFIcodigo(String gENOFIcodigo) {
        this.gENOFIcodigo = gENOFIcodigo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gENCIAcodigo != null ? gENCIAcodigo.hashCode() : 0);
        hash += (fACVDRcodigo != null ? fACVDRcodigo.hashCode() : 0);
        hash += (gENOFIcodigo != null ? gENOFIcodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FacrVendedorPK)) {
            return false;
        }
        FacrVendedorPK other = (FacrVendedorPK) object;
        if ((this.gENCIAcodigo == null && other.gENCIAcodigo != null) || (this.gENCIAcodigo != null && !this.gENCIAcodigo.equals(other.gENCIAcodigo))) {
            return false;
        }
        if ((this.fACVDRcodigo == null && other.fACVDRcodigo != null) || (this.fACVDRcodigo != null && !this.fACVDRcodigo.equals(other.fACVDRcodigo))) {
            return false;
        }
        if ((this.gENOFIcodigo == null && other.gENOFIcodigo != null) || (this.gENOFIcodigo != null && !this.gENOFIcodigo.equals(other.gENOFIcodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.FacrVendedorPK[ gENCIAcodigo=" + gENCIAcodigo + ", fACVDRcodigo=" + fACVDRcodigo + ", gENOFIcodigo=" + gENOFIcodigo + " ]";
    }
    
}
