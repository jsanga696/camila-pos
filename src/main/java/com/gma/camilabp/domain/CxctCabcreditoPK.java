/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb5
 */
@Embeddable
public class CxctCabcreditoPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENCIA_codigo")
    private String gENCIAcodigo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CXCCCR_numsecuen")
    private int cXCCCRnumsecuen;

    public CxctCabcreditoPK() {
    }

    public CxctCabcreditoPK(String gENCIAcodigo, int cXCCCRnumsecuen) {
        this.gENCIAcodigo = gENCIAcodigo;
        this.cXCCCRnumsecuen = cXCCCRnumsecuen;
    }

    public String getGENCIAcodigo() {
        return gENCIAcodigo;
    }

    public void setGENCIAcodigo(String gENCIAcodigo) {
        this.gENCIAcodigo = gENCIAcodigo;
    }

    public int getCXCCCRnumsecuen() {
        return cXCCCRnumsecuen;
    }

    public void setCXCCCRnumsecuen(int cXCCCRnumsecuen) {
        this.cXCCCRnumsecuen = cXCCCRnumsecuen;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gENCIAcodigo != null ? gENCIAcodigo.hashCode() : 0);
        hash += (int) cXCCCRnumsecuen;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CxctCabcreditoPK)) {
            return false;
        }
        CxctCabcreditoPK other = (CxctCabcreditoPK) object;
        if ((this.gENCIAcodigo == null && other.gENCIAcodigo != null) || (this.gENCIAcodigo != null && !this.gENCIAcodigo.equals(other.gENCIAcodigo))) {
            return false;
        }
        if (this.cXCCCRnumsecuen != other.cXCCCRnumsecuen) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CxctCabcreditoPK[ gENCIAcodigo=" + gENCIAcodigo + ", cXCCCRnumsecuen=" + cXCCCRnumsecuen + " ]";
    }
    
}
