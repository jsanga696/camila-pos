/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb6
 */
@Entity
@Table(name = "INVR_MOTVAJUSTE")
@NamedQueries({
    @NamedQuery(name = "InvrMotvajuste.findAll", query = "SELECT i FROM InvrMotvajuste i")})
public class InvrMotvajuste implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected InvrMotvajustePK invrMotvajustePK;
    @Size(max = 30)
    @Column(name = "INVMAJ_descripcio")
    private String iNVMAJdescripcio;
    @Column(name = "INVMAJ_tipo")
    private String iNVMAJtipo;
    @Column(name = "INVMAJ_tipouso")
    private Character iNVMAJtipouso;
    @Column(name = "INVMAJ_estcontab")
    private Character iNVMAJestcontab;
    @Size(max = 20)
    @Column(name = "INVMAJ_ctacontab")
    private String iNVMAJctacontab;
    @Column(name = "INVMAJ_estmodcta")
    private Character iNVMAJestmodcta;
    @Size(max = 5)
    @Column(name = "INVMAJ_trancontab")
    private String iNVMAJtrancontab;
    @Size(max = 3)
    @Column(name = "INVMAJ_subtrancont")
    private String iNVMAJsubtrancont;
    @Size(max = 3)
    @Column(name = "INVCCO_codigo")
    private String iNVCCOcodigo;
    @Size(max = 1)
    @Column(name = "INVMAJ_estbaja")
    private String iNVMAJestbaja;
    @Size(max = 1)
    @Column(name = "INVMAJ_afecgastcos")
    private String iNVMAJafecgastcos;
    @Size(max = 100)
    @Column(name = "INVMAJ_camporefere")
    private String iNVMAJcamporefere;
    @Size(max = 100)
    @Column(name = "INVMAJ_estdocumrel")
    private String iNVMAJestdocumrel;

    public InvrMotvajuste() {
    }

    public InvrMotvajuste(InvrMotvajustePK invrMotvajustePK) {
        this.invrMotvajustePK = invrMotvajustePK;
    }

    public InvrMotvajuste(String gENCIAcodigo, String iNVMAJcodigo) {
        this.invrMotvajustePK = new InvrMotvajustePK(gENCIAcodigo, iNVMAJcodigo);
    }

    public InvrMotvajustePK getInvrMotvajustePK() {
        return invrMotvajustePK;
    }

    public void setInvrMotvajustePK(InvrMotvajustePK invrMotvajustePK) {
        this.invrMotvajustePK = invrMotvajustePK;
    }

    public String getINVMAJdescripcio() {
        return iNVMAJdescripcio;
    }

    public void setINVMAJdescripcio(String iNVMAJdescripcio) {
        this.iNVMAJdescripcio = iNVMAJdescripcio;
    }

    public String getINVMAJtipo() {
        return iNVMAJtipo;
    }

    public void setINVMAJtipo(String iNVMAJtipo) {
        this.iNVMAJtipo = iNVMAJtipo;
    }

    public Character getINVMAJtipouso() {
        return iNVMAJtipouso;
    }

    public void setINVMAJtipouso(Character iNVMAJtipouso) {
        this.iNVMAJtipouso = iNVMAJtipouso;
    }

    public Character getINVMAJestcontab() {
        return iNVMAJestcontab;
    }

    public void setINVMAJestcontab(Character iNVMAJestcontab) {
        this.iNVMAJestcontab = iNVMAJestcontab;
    }

    public String getINVMAJctacontab() {
        return iNVMAJctacontab;
    }

    public void setINVMAJctacontab(String iNVMAJctacontab) {
        this.iNVMAJctacontab = iNVMAJctacontab;
    }

    public Character getINVMAJestmodcta() {
        return iNVMAJestmodcta;
    }

    public void setINVMAJestmodcta(Character iNVMAJestmodcta) {
        this.iNVMAJestmodcta = iNVMAJestmodcta;
    }

    public String getINVMAJtrancontab() {
        return iNVMAJtrancontab;
    }

    public void setINVMAJtrancontab(String iNVMAJtrancontab) {
        this.iNVMAJtrancontab = iNVMAJtrancontab;
    }

    public String getINVMAJsubtrancont() {
        return iNVMAJsubtrancont;
    }

    public void setINVMAJsubtrancont(String iNVMAJsubtrancont) {
        this.iNVMAJsubtrancont = iNVMAJsubtrancont;
    }

    public String getINVCCOcodigo() {
        return iNVCCOcodigo;
    }

    public void setINVCCOcodigo(String iNVCCOcodigo) {
        this.iNVCCOcodigo = iNVCCOcodigo;
    }

    public String getINVMAJestbaja() {
        return iNVMAJestbaja;
    }

    public void setINVMAJestbaja(String iNVMAJestbaja) {
        this.iNVMAJestbaja = iNVMAJestbaja;
    }

    public String getINVMAJafecgastcos() {
        return iNVMAJafecgastcos;
    }

    public void setINVMAJafecgastcos(String iNVMAJafecgastcos) {
        this.iNVMAJafecgastcos = iNVMAJafecgastcos;
    }

    public String getINVMAJcamporefere() {
        return iNVMAJcamporefere;
    }

    public void setINVMAJcamporefere(String iNVMAJcamporefere) {
        this.iNVMAJcamporefere = iNVMAJcamporefere;
    }

    public String getINVMAJestdocumrel() {
        return iNVMAJestdocumrel;
    }

    public void setINVMAJestdocumrel(String iNVMAJestdocumrel) {
        this.iNVMAJestdocumrel = iNVMAJestdocumrel;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (invrMotvajustePK != null ? invrMotvajustePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InvrMotvajuste)) {
            return false;
        }
        InvrMotvajuste other = (InvrMotvajuste) object;
        if ((this.invrMotvajustePK == null && other.invrMotvajustePK != null) || (this.invrMotvajustePK != null && !this.invrMotvajustePK.equals(other.invrMotvajustePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.InvrMotvajuste[ invrMotvajustePK=" + invrMotvajustePK + " ]";
    }
    
}
