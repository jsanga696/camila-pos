/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author userdb6
 */
@Entity
@Table(name = "FACT_SESIONCAJA")
@NamedQueries({
    @NamedQuery(name = "FactSesioncaja.findAll", query = "SELECT f FROM FactSesioncaja f")})
public class FactSesioncaja implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected FactSesioncajaPK factSesioncajaPK;
    @Column(name="GENMON_codigo")
    private String genmonCodigo;
    @Column(name = "FACSCA_feccierre")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fACSCAfeccierre;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "FACSCA_saldodia")
    private BigDecimal fACSCAsaldodia=BigDecimal.ZERO;
    @Column(name="SEGUSU_codigo")
    private String segusuCodigo;
    @Column(name = "FACSCA_horasalida")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fACSCAhorasalida;
    @Column(name = "FACSCA_salinimov")
    private BigDecimal fACSCAsalinimov=BigDecimal.ZERO;
    @Column(name = "FACSCA_horainicio")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fACSCAhorainicio;
    @Column(name = "FACSCA_salfinmov")
    private BigDecimal fACSCAsalfinmov=BigDecimal.ZERO;
    @Column(name = "FACSCA_saliniloc")
    private BigDecimal fACSCAsaliniloc=BigDecimal.ZERO;
    @Column(name = "FACSCA_salinibal")
    private BigDecimal fACSCAsalinibal=BigDecimal.ZERO;
    @Column(name = "FACSCA_salfinloc")
    private BigDecimal fACSCAsalfinloc=BigDecimal.ZERO;
    @Column(name = "FACSCA_salfinbal")
    private BigDecimal fACSCAsalfinbal=BigDecimal.ZERO;
    @Column(name = "FACSCA_totvenmov")
    private BigDecimal fACSCAtotvenmov=BigDecimal.ZERO;
    @Column(name = "FACSCA_totvenloc")
    private BigDecimal fACSCAtotvenloc=BigDecimal.ZERO;
    @Column(name = "FACSCA_totvenbal")
    private BigDecimal fACSCAtotvenbal=BigDecimal.ZERO;
    @Column(name = "FACSCA_totimpmov")
    private BigDecimal fACSCAtotimpmov=BigDecimal.ZERO;
    @Column(name = "FACSCA_totimploc")
    private BigDecimal fACSCAtotimploc=BigDecimal.ZERO;
    @Column(name = "FACSCA_totimpbal")
    private BigDecimal fACSCAtotimpbal=BigDecimal.ZERO;
    @Column(name = "FACSCA_totdescmov")
    private BigDecimal fACSCAtotdescmov=BigDecimal.ZERO;
    @Column(name = "FACSCA_totdescloc")
    private BigDecimal fACSCAtotdescloc=BigDecimal.ZERO;
    @Column(name = "FACSCA_totdescbal")
    private BigDecimal fACSCAtotdescbal=BigDecimal.ZERO;
    @Column(name = "FACSCA_totvacamov")
    private BigDecimal fACSCAtotvacamov=BigDecimal.ZERO;
    @Column(name = "FACSCA_totvacaloc")
    private BigDecimal fACSCAtotvacaloc=BigDecimal.ZERO;
    @Column(name = "FACSCA_totvacabal")
    private BigDecimal fACSCAtotvacabal=BigDecimal.ZERO;
    @Column(name = "FACSCA_estado")
    private Character fACSCAestado='A';
    @Column(name = "FACSCA_totrepomov")
    private BigDecimal fACSCAtotrepomov=BigDecimal.ZERO;
    @Column(name = "FACSCA_totrepoloc")
    private BigDecimal fACSCAtotrepoloc=BigDecimal.ZERO;
    @Column(name = "FACSCA_totrepobal")
    private BigDecimal fACSCAtotrepobal=BigDecimal.ZERO;
    @Column(name = "FACSCA_idsececaja")
    private Integer fACSCAidsececaja;

    public FactSesioncaja() {
    }

    public FactSesioncaja(FactSesioncajaPK factSesioncajaPK) {
        this.factSesioncajaPK = factSesioncajaPK;
    }

    public FactSesioncaja(String gENCIAcodigo, String gENOFIcodigo, String gENCAJcodigo, Date fACSCAfecapertur) {
        this.factSesioncajaPK = new FactSesioncajaPK(gENCIAcodigo, gENOFIcodigo, gENCAJcodigo, fACSCAfecapertur);
    }

    public FactSesioncaja(String gENCIAcodigo, String gENOFIcodigo, String gENCAJcodigo, Date fACSCAfecapertur, Integer fACSCAidsececaja) {
        this.factSesioncajaPK = new FactSesioncajaPK(gENCIAcodigo, gENOFIcodigo, gENCAJcodigo, fACSCAfecapertur);
        this.fACSCAidsececaja = fACSCAidsececaja;
    }

    public FactSesioncajaPK getFactSesioncajaPK() {
        return factSesioncajaPK;
    }

    public void setFactSesioncajaPK(FactSesioncajaPK factSesioncajaPK) {
        this.factSesioncajaPK = factSesioncajaPK;
    }

    public String getGenmonCodigo() {
        return genmonCodigo;
    }

    public void setGenmonCodigo(String genmonCodigo) {
        this.genmonCodigo = genmonCodigo;
    }

    public Date getFACSCAfeccierre() {
        return fACSCAfeccierre;
    }

    public void setFACSCAfeccierre(Date fACSCAfeccierre) {
        this.fACSCAfeccierre = fACSCAfeccierre;
    }

    public BigDecimal getFACSCAsaldodia() {
        return fACSCAsaldodia;
    }

    public void setFACSCAsaldodia(BigDecimal fACSCAsaldodia) {
        this.fACSCAsaldodia = fACSCAsaldodia;
    }

    public String getSegusuCodigo() {
        return segusuCodigo;
    }

    public void setSegusuCodigo(String segusuCodigo) {
        this.segusuCodigo = segusuCodigo;
    }

    public Date getFACSCAhorasalida() {
        return fACSCAhorasalida;
    }

    public void setFACSCAhorasalida(Date fACSCAhorasalida) {
        this.fACSCAhorasalida = fACSCAhorasalida;
    }

    public BigDecimal getFACSCAsalinimov() {
        return fACSCAsalinimov;
    }

    public void setFACSCAsalinimov(BigDecimal fACSCAsalinimov) {
        this.fACSCAsalinimov = fACSCAsalinimov;
    }

    public Date getFACSCAhorainicio() {
        return fACSCAhorainicio;
    }

    public void setFACSCAhorainicio(Date fACSCAhorainicio) {
        this.fACSCAhorainicio = fACSCAhorainicio;
    }

    public BigDecimal getFACSCAsalfinmov() {
        return fACSCAsalfinmov;
    }

    public void setFACSCAsalfinmov(BigDecimal fACSCAsalfinmov) {
        this.fACSCAsalfinmov = fACSCAsalfinmov;
    }

    public BigDecimal getFACSCAsaliniloc() {
        return fACSCAsaliniloc;
    }

    public void setFACSCAsaliniloc(BigDecimal fACSCAsaliniloc) {
        this.fACSCAsaliniloc = fACSCAsaliniloc;
    }

    public BigDecimal getFACSCAsalinibal() {
        return fACSCAsalinibal;
    }

    public void setFACSCAsalinibal(BigDecimal fACSCAsalinibal) {
        this.fACSCAsalinibal = fACSCAsalinibal;
    }

    public BigDecimal getFACSCAsalfinloc() {
        return fACSCAsalfinloc;
    }

    public void setFACSCAsalfinloc(BigDecimal fACSCAsalfinloc) {
        this.fACSCAsalfinloc = fACSCAsalfinloc;
    }

    public BigDecimal getFACSCAsalfinbal() {
        return fACSCAsalfinbal;
    }

    public void setFACSCAsalfinbal(BigDecimal fACSCAsalfinbal) {
        this.fACSCAsalfinbal = fACSCAsalfinbal;
    }

    public BigDecimal getFACSCAtotvenmov() {
        return fACSCAtotvenmov;
    }

    public void setFACSCAtotvenmov(BigDecimal fACSCAtotvenmov) {
        this.fACSCAtotvenmov = fACSCAtotvenmov;
    }

    public BigDecimal getFACSCAtotvenloc() {
        return fACSCAtotvenloc;
    }

    public void setFACSCAtotvenloc(BigDecimal fACSCAtotvenloc) {
        this.fACSCAtotvenloc = fACSCAtotvenloc;
    }

    public BigDecimal getFACSCAtotvenbal() {
        return fACSCAtotvenbal;
    }

    public void setFACSCAtotvenbal(BigDecimal fACSCAtotvenbal) {
        this.fACSCAtotvenbal = fACSCAtotvenbal;
    }

    public BigDecimal getFACSCAtotimpmov() {
        return fACSCAtotimpmov;
    }

    public void setFACSCAtotimpmov(BigDecimal fACSCAtotimpmov) {
        this.fACSCAtotimpmov = fACSCAtotimpmov;
    }

    public BigDecimal getFACSCAtotimploc() {
        return fACSCAtotimploc;
    }

    public void setFACSCAtotimploc(BigDecimal fACSCAtotimploc) {
        this.fACSCAtotimploc = fACSCAtotimploc;
    }

    public BigDecimal getFACSCAtotimpbal() {
        return fACSCAtotimpbal;
    }

    public void setFACSCAtotimpbal(BigDecimal fACSCAtotimpbal) {
        this.fACSCAtotimpbal = fACSCAtotimpbal;
    }

    public BigDecimal getFACSCAtotdescmov() {
        return fACSCAtotdescmov;
    }

    public void setFACSCAtotdescmov(BigDecimal fACSCAtotdescmov) {
        this.fACSCAtotdescmov = fACSCAtotdescmov;
    }

    public BigDecimal getFACSCAtotdescloc() {
        return fACSCAtotdescloc;
    }

    public void setFACSCAtotdescloc(BigDecimal fACSCAtotdescloc) {
        this.fACSCAtotdescloc = fACSCAtotdescloc;
    }

    public BigDecimal getFACSCAtotdescbal() {
        return fACSCAtotdescbal;
    }

    public void setFACSCAtotdescbal(BigDecimal fACSCAtotdescbal) {
        this.fACSCAtotdescbal = fACSCAtotdescbal;
    }

    public BigDecimal getFACSCAtotvacamov() {
        return fACSCAtotvacamov;
    }

    public void setFACSCAtotvacamov(BigDecimal fACSCAtotvacamov) {
        this.fACSCAtotvacamov = fACSCAtotvacamov;
    }

    public BigDecimal getFACSCAtotvacaloc() {
        return fACSCAtotvacaloc;
    }

    public void setFACSCAtotvacaloc(BigDecimal fACSCAtotvacaloc) {
        this.fACSCAtotvacaloc = fACSCAtotvacaloc;
    }

    public BigDecimal getFACSCAtotvacabal() {
        return fACSCAtotvacabal;
    }

    public void setFACSCAtotvacabal(BigDecimal fACSCAtotvacabal) {
        this.fACSCAtotvacabal = fACSCAtotvacabal;
    }

    public Character getFACSCAestado() {
        return fACSCAestado;
    }

    public void setFACSCAestado(Character fACSCAestado) {
        this.fACSCAestado = fACSCAestado;
    }

    public BigDecimal getFACSCAtotrepomov() {
        return fACSCAtotrepomov;
    }

    public void setFACSCAtotrepomov(BigDecimal fACSCAtotrepomov) {
        this.fACSCAtotrepomov = fACSCAtotrepomov;
    }

    public BigDecimal getFACSCAtotrepoloc() {
        return fACSCAtotrepoloc;
    }

    public void setFACSCAtotrepoloc(BigDecimal fACSCAtotrepoloc) {
        this.fACSCAtotrepoloc = fACSCAtotrepoloc;
    }

    public BigDecimal getFACSCAtotrepobal() {
        return fACSCAtotrepobal;
    }

    public void setFACSCAtotrepobal(BigDecimal fACSCAtotrepobal) {
        this.fACSCAtotrepobal = fACSCAtotrepobal;
    }

    public Integer getFACSCAidsececaja() {
        return fACSCAidsececaja;
    }

    public void setFACSCAidsececaja(Integer fACSCAidsececaja) {
        this.fACSCAidsececaja = fACSCAidsececaja;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (factSesioncajaPK != null ? factSesioncajaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FactSesioncaja)) {
            return false;
        }
        FactSesioncaja other = (FactSesioncaja) object;
        if ((this.factSesioncajaPK == null && other.factSesioncajaPK != null) || (this.factSesioncajaPK != null && !this.factSesioncajaPK.equals(other.factSesioncajaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.FactSesioncaja[ factSesioncajaPK=" + factSesioncajaPK + " ]";
    }
    
}
