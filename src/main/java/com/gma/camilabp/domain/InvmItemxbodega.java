/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
//import org.hibernate.annotations.Formula;

/**
 *
 * @author userdb6
 */
@Entity
@Table(name = "INVM_ITEMXBODEGA")
@NamedQueries({
    @NamedQuery(name = "InvmItemxbodega.findAll", query = "SELECT i FROM InvmItemxbodega i")})
public class InvmItemxbodega implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected InvmItemxbodegaPK invmItemxbodegaPK;
    @Size(max = 15)
    @Column(name = "INVIBO_ubicacion")
    private String iNVIBOubicacion;
    @Size(max = 15)
    @Column(name = "INVIBO_posicion")
    private String iNVIBOposicion;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "INVIBO_stock")
    private BigDecimal iNVIBOstock;
    @Basic(optional = false)
    @NotNull
    @Column(name = "INVIBO_stkpending")
    private BigDecimal iNVIBOstkpending;
    @Basic(optional = false)
    @NotNull
    @Column(name = "INVIBO_stkreserva")
    private BigDecimal iNVIBOstkreserva;
    @Basic(optional = false)
    @NotNull
    @Column(name = "INVIBO_stktransit")
    private BigDecimal iNVIBOstktransit;
    @Basic(optional = false)
    @NotNull
    @Column(name = "INVIBO_stkminimo")
    private BigDecimal iNVIBOstkminimo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "INVIBO_stkmaximo")
    private BigDecimal iNVIBOstkmaximo;
    @Column(name = "INVIBO_fecultiing")
    @Temporal(TemporalType.TIMESTAMP)
    private Date iNVIBOfecultiing;
    @Column(name = "INVIBO_fecultiegr")
    @Temporal(TemporalType.TIMESTAMP)
    private Date iNVIBOfecultiegr;
    @Column(name = "INVIBO_candisprod")
    private BigDecimal iNVIBOcandisprod;
    @Column(name = "INVIBO_canasiprod")
    private BigDecimal iNVIBOcanasiprod;
    @Column(name = "INVIBO_fecultcont")
    @Temporal(TemporalType.TIMESTAMP)
    private Date iNVIBOfecultcont;
    @Column(name = "INVIBO_fecexpira")
    @Temporal(TemporalType.TIMESTAMP)
    private Date iNVIBOfecexpira;
    @Column(name = "INVIBO_costobal")
    private BigDecimal iNVIBOcostobal;
    @Column(name = "INVIBO_costoloc")
    private BigDecimal iNVIBOcostoloc;
    @Size(max = 30)
    @Column(name = "INVIBO_usucrea")
    private String iNVIBOusucrea;
    @Size(max = 30)
    @Column(name = "INVIBO_usumodi")
    private String iNVIBOusumodi;
    @Column(name = "INVIBO_feccrea")
    @Temporal(TemporalType.TIMESTAMP)
    private Date iNVIBOfeccrea;
    @Column(name = "INVIBO_fecmodi")
    @Temporal(TemporalType.TIMESTAMP)
    private Date iNVIBOfecmodi;
    @Size(max = 8)
    @Column(name = "INVIBO_numerolote")
    private String iNVIBOnumerolote;
    @Size(max = 8)
    @Column(name = "INVIBO_numerefere")
    private String iNVIBOnumerefere;
    //@Formula(value = "(SELECT b.genbod_nombre FROM CBPGENR_BODEGA b WHERE b.genbod_secuencia=GENBOD_codigo)")
    @Transient
    private String bodega;
    //INVITM_esttipo=T,P,INVITM_estactivo=A,INVITM_estdispvta=S , INVITM_estaPromocion!=S
    //@Formula(value = "(SELECT i.INVITM_nombre FROM INVM_ITEM i WHERE i.INVITM_esttipo IN('T','P') AND i.INVITM_estactivo='A' AND i.INVITM_estdispvta='S' AND i.INVITM_estaPromocion!='S' AND i.INVITM_codigo=INVITM_codigo)")
    @Transient
    private String nombreItem;
    //@Formula(value = "(SELECT i.INVUNI_unidadexis FROM INVM_ITEM i WHERE i.INVITM_esttipo IN('T','P') AND i.INVITM_estactivo='A' AND i.INVITM_estdispvta='S' AND i.INVITM_estaPromocion!='S' AND i.INVITM_codigo=INVITM_codigo)")
    @Transient
    private String unidadItem;
    //@Formula(value = "(SELECT i.INVITM_codigbarra FROM INVM_ITEM i WHERE i.INVITM_esttipo IN('T','P') AND i.INVITM_estactivo='A' AND i.INVITM_estdispvta='S' AND i.INVITM_estaPromocion!='S' AND i.INVITM_codigo=INVITM_codigo)")
    @Transient
    private String codigoBarraItem;
    @Transient
    private BigDecimal disponible;
    @Transient
    private String codigoItem;
    @Transient
    private BigDecimal cantidadFuncional;

    public InvmItemxbodega() {
    }

    public InvmItemxbodega(InvmItemxbodegaPK invmItemxbodegaPK) {
        this.invmItemxbodegaPK = invmItemxbodegaPK;
    }

    public InvmItemxbodega(InvmItemxbodegaPK invmItemxbodegaPK, BigDecimal iNVIBOstock, BigDecimal iNVIBOstkpending, BigDecimal iNVIBOstkreserva, BigDecimal iNVIBOstktransit, BigDecimal iNVIBOstkminimo, BigDecimal iNVIBOstkmaximo) {
        this.invmItemxbodegaPK = invmItemxbodegaPK;
        this.iNVIBOstock = iNVIBOstock;
        this.iNVIBOstkpending = iNVIBOstkpending;
        this.iNVIBOstkreserva = iNVIBOstkreserva;
        this.iNVIBOstktransit = iNVIBOstktransit;
        this.iNVIBOstkminimo = iNVIBOstkminimo;
        this.iNVIBOstkmaximo = iNVIBOstkmaximo;
    }

    public InvmItemxbodega(String gENCIAcodigo, String gENOFIcodigo, String gENBODcodigo, String iNVITMcodigo) {
        this.invmItemxbodegaPK = new InvmItemxbodegaPK(gENCIAcodigo, gENOFIcodigo, gENBODcodigo, iNVITMcodigo);
    }

    public InvmItemxbodegaPK getInvmItemxbodegaPK() {
        return invmItemxbodegaPK;
    }

    public void setInvmItemxbodegaPK(InvmItemxbodegaPK invmItemxbodegaPK) {
        this.invmItemxbodegaPK = invmItemxbodegaPK;
    }

    public String getINVIBOubicacion() {
        return iNVIBOubicacion;
    }

    public void setINVIBOubicacion(String iNVIBOubicacion) {
        this.iNVIBOubicacion = iNVIBOubicacion;
    }

    public String getINVIBOposicion() {
        return iNVIBOposicion;
    }

    public void setINVIBOposicion(String iNVIBOposicion) {
        this.iNVIBOposicion = iNVIBOposicion;
    }

    public BigDecimal getINVIBOstock() {
        return iNVIBOstock;
    }

    public void setINVIBOstock(BigDecimal iNVIBOstock) {
        this.iNVIBOstock = iNVIBOstock;
    }

    public BigDecimal getINVIBOstkpending() {
        return iNVIBOstkpending;
    }

    public void setINVIBOstkpending(BigDecimal iNVIBOstkpending) {
        this.iNVIBOstkpending = iNVIBOstkpending;
    }

    public BigDecimal getINVIBOstkreserva() {
        return iNVIBOstkreserva;
    }

    public void setINVIBOstkreserva(BigDecimal iNVIBOstkreserva) {
        this.iNVIBOstkreserva = iNVIBOstkreserva;
    }

    public BigDecimal getINVIBOstktransit() {
        return iNVIBOstktransit;
    }

    public void setINVIBOstktransit(BigDecimal iNVIBOstktransit) {
        this.iNVIBOstktransit = iNVIBOstktransit;
    }

    public BigDecimal getINVIBOstkminimo() {
        return iNVIBOstkminimo;
    }

    public void setINVIBOstkminimo(BigDecimal iNVIBOstkminimo) {
        this.iNVIBOstkminimo = iNVIBOstkminimo;
    }

    public BigDecimal getINVIBOstkmaximo() {
        return iNVIBOstkmaximo;
    }

    public void setINVIBOstkmaximo(BigDecimal iNVIBOstkmaximo) {
        this.iNVIBOstkmaximo = iNVIBOstkmaximo;
    }

    public Date getINVIBOfecultiing() {
        return iNVIBOfecultiing;
    }

    public void setINVIBOfecultiing(Date iNVIBOfecultiing) {
        this.iNVIBOfecultiing = iNVIBOfecultiing;
    }

    public Date getINVIBOfecultiegr() {
        return iNVIBOfecultiegr;
    }

    public void setINVIBOfecultiegr(Date iNVIBOfecultiegr) {
        this.iNVIBOfecultiegr = iNVIBOfecultiegr;
    }

    public BigDecimal getINVIBOcandisprod() {
        return iNVIBOcandisprod;
    }

    public void setINVIBOcandisprod(BigDecimal iNVIBOcandisprod) {
        this.iNVIBOcandisprod = iNVIBOcandisprod;
    }

    public BigDecimal getINVIBOcanasiprod() {
        return iNVIBOcanasiprod;
    }

    public void setINVIBOcanasiprod(BigDecimal iNVIBOcanasiprod) {
        this.iNVIBOcanasiprod = iNVIBOcanasiprod;
    }

    public Date getINVIBOfecultcont() {
        return iNVIBOfecultcont;
    }

    public void setINVIBOfecultcont(Date iNVIBOfecultcont) {
        this.iNVIBOfecultcont = iNVIBOfecultcont;
    }

    public Date getINVIBOfecexpira() {
        return iNVIBOfecexpira;
    }

    public void setINVIBOfecexpira(Date iNVIBOfecexpira) {
        this.iNVIBOfecexpira = iNVIBOfecexpira;
    }

    public BigDecimal getINVIBOcostobal() {
        return iNVIBOcostobal;
    }

    public void setINVIBOcostobal(BigDecimal iNVIBOcostobal) {
        this.iNVIBOcostobal = iNVIBOcostobal;
    }

    public BigDecimal getINVIBOcostoloc() {
        return iNVIBOcostoloc;
    }

    public void setINVIBOcostoloc(BigDecimal iNVIBOcostoloc) {
        this.iNVIBOcostoloc = iNVIBOcostoloc;
    }

    public String getINVIBOusucrea() {
        return iNVIBOusucrea;
    }

    public void setINVIBOusucrea(String iNVIBOusucrea) {
        this.iNVIBOusucrea = iNVIBOusucrea;
    }

    public String getINVIBOusumodi() {
        return iNVIBOusumodi;
    }

    public void setINVIBOusumodi(String iNVIBOusumodi) {
        this.iNVIBOusumodi = iNVIBOusumodi;
    }

    public Date getINVIBOfeccrea() {
        return iNVIBOfeccrea;
    }

    public void setINVIBOfeccrea(Date iNVIBOfeccrea) {
        this.iNVIBOfeccrea = iNVIBOfeccrea;
    }

    public Date getINVIBOfecmodi() {
        return iNVIBOfecmodi;
    }

    public void setINVIBOfecmodi(Date iNVIBOfecmodi) {
        this.iNVIBOfecmodi = iNVIBOfecmodi;
    }

    public String getINVIBOnumerolote() {
        return iNVIBOnumerolote;
    }

    public void setINVIBOnumerolote(String iNVIBOnumerolote) {
        this.iNVIBOnumerolote = iNVIBOnumerolote;
    }

    public String getINVIBOnumerefere() {
        return iNVIBOnumerefere;
    }

    public void setINVIBOnumerefere(String iNVIBOnumerefere) {
        this.iNVIBOnumerefere = iNVIBOnumerefere;
    }

    public String getBodega() {
        return bodega;
    }

    public void setBodega(String bodega) {
        this.bodega = bodega;
    }

    public String getNombreItem() {
        return nombreItem;
    }

    public void setNombreItem(String nombreItem) {
        this.nombreItem = nombreItem;
    }

    public BigDecimal getDisponible() {
        disponible= iNVIBOstock.subtract(iNVIBOstkreserva);
        return disponible;
    }

    public void setDisponible(BigDecimal disponible) {
        this.disponible = disponible;
    }

    public String getUnidadItem() {
        return unidadItem;
    }

    public void setUnidadItem(String unidadItem) {
        this.unidadItem = unidadItem;
    }

    public String getCodigoItem() {
        return codigoItem;
    }

    public void setCodigoItem(String codigoItem) {
        this.codigoItem = codigoItem;
    }

    public BigDecimal getCantidadFuncional() {
        return cantidadFuncional;
    }

    public void setCantidadFuncional(BigDecimal cantidadFuncional) {
        this.cantidadFuncional = cantidadFuncional;
    }

    public String getCodigoBarraItem() {
        return codigoBarraItem;
    }

    public void setCodigoBarraItem(String codigoBarraItem) {
        this.codigoBarraItem = codigoBarraItem;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (invmItemxbodegaPK != null ? invmItemxbodegaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InvmItemxbodega)) {
            return false;
        }
        InvmItemxbodega other = (InvmItemxbodega) object;
        if ((this.invmItemxbodegaPK == null && other.invmItemxbodegaPK != null) || (this.invmItemxbodegaPK != null && !this.invmItemxbodegaPK.equals(other.invmItemxbodegaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.InvmItemxbodega[ invmItemxbodegaPK=" + invmItemxbodegaPK + " ]";
    }
    
}
