/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb
 */
@Entity
@Table(name = "CBPSEGM_PAQUETE")
@NamedQueries({
    @NamedQuery(name = "CbpsegmPaquete.findAll", query = "SELECT c FROM CbpsegmPaquete c")})
public class CbpsegmPaquete implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "segpaq_codigo")
    private Long segpaqCodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "segpaq_descripcio")
    private String segpaqDescripcio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "segpaq_estado")
    private boolean segpaqEstado= true;
    @Basic(optional = false)
    @NotNull
    @Column(name = "segpaq_creafecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date segpaqCreafecha= new Date();
    @Basic(optional = false)
    @NotNull
    @Column(name = "segpaq_creauser")
    private long segpaqCreauser;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "segpaqCodigo")
    private Collection<CbpsegmModulo> cbpsegmModuloCollection;

    public CbpsegmPaquete() {
    }

    public CbpsegmPaquete(Long segpaqCodigo) {
        this.segpaqCodigo = segpaqCodigo;
    }

    public CbpsegmPaquete(Long segpaqCodigo, String segpaqDescripcio, boolean segpaqEstado, Date segpaqCreafecha, long segpaqCreauser) {
        this.segpaqCodigo = segpaqCodigo;
        this.segpaqDescripcio = segpaqDescripcio;
        this.segpaqEstado = segpaqEstado;
        this.segpaqCreafecha = segpaqCreafecha;
        this.segpaqCreauser = segpaqCreauser;
    }

    public Long getSegpaqCodigo() {
        return segpaqCodigo;
    }

    public void setSegpaqCodigo(Long segpaqCodigo) {
        this.segpaqCodigo = segpaqCodigo;
    }

    public String getSegpaqDescripcio() {
        return segpaqDescripcio;
    }

    public void setSegpaqDescripcio(String segpaqDescripcio) {
        this.segpaqDescripcio = segpaqDescripcio;
    }

    public boolean getSegpaqEstado() {
        return segpaqEstado;
    }

    public void setSegpaqEstado(boolean segpaqEstado) {
        this.segpaqEstado = segpaqEstado;
    }

    public Date getSegpaqCreafecha() {
        return segpaqCreafecha;
    }

    public void setSegpaqCreafecha(Date segpaqCreafecha) {
        this.segpaqCreafecha = segpaqCreafecha;
    }

    public long getSegpaqCreauser() {
        return segpaqCreauser;
    }

    public void setSegpaqCreauser(long segpaqCreauser) {
        this.segpaqCreauser = segpaqCreauser;
    }

    public Collection<CbpsegmModulo> getCbpsegmModuloCollection() {
        return cbpsegmModuloCollection;
    }

    public void setCbpsegmModuloCollection(Collection<CbpsegmModulo> cbpsegmModuloCollection) {
        this.cbpsegmModuloCollection = cbpsegmModuloCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (segpaqCodigo != null ? segpaqCodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CbpsegmPaquete)) {
            return false;
        }
        CbpsegmPaquete other = (CbpsegmPaquete) object;
        if ((this.segpaqCodigo == null && other.segpaqCodigo != null) || (this.segpaqCodigo != null && !this.segpaqCodigo.equals(other.segpaqCodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.CbpsegmPaquete[ segpaqCodigo=" + segpaqCodigo + " ]";
    }
    
}
