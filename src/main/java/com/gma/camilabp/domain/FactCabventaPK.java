/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb2
 */
@Embeddable
public class FactCabventaPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENCIA_codigo", nullable = false, length = 4)
    private String gENCIAcodigo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FACCVT_numsecuenc", nullable = false)
    private int fACCVTnumsecuenc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENOFI_codigo", nullable = false, length = 4)
    private String gENOFIcodigo;

    public FactCabventaPK() {
    }

    public FactCabventaPK(String gENCIAcodigo, int fACCVTnumsecuenc, String gENOFIcodigo) {
        this.gENCIAcodigo = gENCIAcodigo;
        this.fACCVTnumsecuenc = fACCVTnumsecuenc;
        this.gENOFIcodigo = gENOFIcodigo;
    }

    public String getGENCIAcodigo() {
        return gENCIAcodigo;
    }

    public void setGENCIAcodigo(String gENCIAcodigo) {
        this.gENCIAcodigo = gENCIAcodigo;
    }

    public int getFACCVTnumsecuenc() {
        return fACCVTnumsecuenc;
    }

    public void setFACCVTnumsecuenc(int fACCVTnumsecuenc) {
        this.fACCVTnumsecuenc = fACCVTnumsecuenc;
    }

    public String getGENOFIcodigo() {
        return gENOFIcodigo;
    }

    public void setGENOFIcodigo(String gENOFIcodigo) {
        this.gENOFIcodigo = gENOFIcodigo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gENCIAcodigo != null ? gENCIAcodigo.hashCode() : 0);
        hash += (int) fACCVTnumsecuenc;
        hash += (gENOFIcodigo != null ? gENOFIcodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FactCabventaPK)) {
            return false;
        }
        FactCabventaPK other = (FactCabventaPK) object;
        if ((this.gENCIAcodigo == null && other.gENCIAcodigo != null) || (this.gENCIAcodigo != null && !this.gENCIAcodigo.equals(other.gENCIAcodigo))) {
            return false;
        }
        if (this.fACCVTnumsecuenc != other.fACCVTnumsecuenc) {
            return false;
        }
        if ((this.gENOFIcodigo == null && other.gENOFIcodigo != null) || (this.gENOFIcodigo != null && !this.gENOFIcodigo.equals(other.gENOFIcodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.FactCabventaPK[ gENCIAcodigo=" + gENCIAcodigo + ", fACCVTnumsecuenc=" + fACCVTnumsecuenc + ", gENOFIcodigo=" + gENOFIcodigo + " ]";
    }
    
}
