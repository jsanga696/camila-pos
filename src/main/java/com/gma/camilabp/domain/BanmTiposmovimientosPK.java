/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Acer
 */
@Embeddable
public class BanmTiposmovimientosPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "GENCIA_codigo")
    private String gENCIAcodigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "BANTMV_codigo")
    private String bANTMVcodigo;

    public BanmTiposmovimientosPK() {
    }

    public BanmTiposmovimientosPK(String gENCIAcodigo, String bANTMVcodigo) {
        this.gENCIAcodigo = gENCIAcodigo;
        this.bANTMVcodigo = bANTMVcodigo;
    }

    public String getGENCIAcodigo() {
        return gENCIAcodigo;
    }

    public void setGENCIAcodigo(String gENCIAcodigo) {
        this.gENCIAcodigo = gENCIAcodigo;
    }

    public String getBANTMVcodigo() {
        return bANTMVcodigo;
    }

    public void setBANTMVcodigo(String bANTMVcodigo) {
        this.bANTMVcodigo = bANTMVcodigo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gENCIAcodigo != null ? gENCIAcodigo.hashCode() : 0);
        hash += (bANTMVcodigo != null ? bANTMVcodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BanmTiposmovimientosPK)) {
            return false;
        }
        BanmTiposmovimientosPK other = (BanmTiposmovimientosPK) object;
        if ((this.gENCIAcodigo == null && other.gENCIAcodigo != null) || (this.gENCIAcodigo != null && !this.gENCIAcodigo.equals(other.gENCIAcodigo))) {
            return false;
        }
        if ((this.bANTMVcodigo == null && other.bANTMVcodigo != null) || (this.bANTMVcodigo != null && !this.bANTMVcodigo.equals(other.bANTMVcodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gma.camilabp.domain.BanmTiposmovimientosPK[ gENCIAcodigo=" + gENCIAcodigo + ", bANTMVcodigo=" + bANTMVcodigo + " ]";
    }
    
}
