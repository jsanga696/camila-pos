/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain.storedprocedure;

import java.io.Serializable;
import javax.persistence.Column;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb
 */
public class UsuarioPrueba{
    
    private String campoa;
    private String campob;

    public String getCampoa() {
        return campoa;
    }

    public void setCampoa(String campoa) {
        this.campoa = campoa;
    }

    public String getCampob() {
        return campob;
    }

    public void setCampob(String campob) {
        this.campob = campob;
    }

}
