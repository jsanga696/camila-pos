/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain.storedprocedure;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.persistence.Column;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb
 */
public class CarteraClasificacion implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private String clasificacion;
    private List<CarteraModelCliente> carteraModelClienteList = new ArrayList<>();
    
    //Temporales
    private List<CarteraModel> carteraModelTempList = new ArrayList<>();
    private Map<String, List<CarteraModel>> clasificacionClienteTempList = null;

    public Map<String, List<CarteraModel>> getClasificacionClienteTempList() {
        return clasificacionClienteTempList;
    }

    public void setClasificacionClienteTempList(Map<String, List<CarteraModel>> clasificacionClienteTempList) {
        this.clasificacionClienteTempList = clasificacionClienteTempList;
    }
    public CarteraClasificacion() {
    }

    public String getClasificacion() {
        return clasificacion;
    }

    public void setClasificacion(String clasificacion) {
        this.clasificacion = clasificacion;
    }

    public List<CarteraModelCliente> getCarteraModelClienteList() {
        return carteraModelClienteList;
    }

    public void setCarteraModelClienteList(List<CarteraModelCliente> carteraModelClienteList) {
        this.carteraModelClienteList = carteraModelClienteList;
    }

    public List<CarteraModel> getCarteraModelTempList() {
        return carteraModelTempList;
    }

    public void setCarteraModelTempList(List<CarteraModel> carteraModelTempList) {
        this.carteraModelTempList = carteraModelTempList;
    }
    
}
