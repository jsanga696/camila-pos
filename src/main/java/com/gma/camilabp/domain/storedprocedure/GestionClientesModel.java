/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain.storedprocedure;

import java.io.Serializable;
import java.math.BigInteger;

/**
 *
 * @author userdb6
 */
public class GestionClientesModel implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private BigInteger codigoGrupo1;
    private String grupo1;
    private BigInteger codigoGrupo2;
    private String grupo2;
    private Integer clientes;
    private Integer cantFacturas;
    private Integer clientesVentas;

    public GestionClientesModel() {
    }

    public BigInteger getCodigoGrupo1() {
        return codigoGrupo1;
    }

    public void setCodigoGrupo1(BigInteger codigoGrupo1) {
        this.codigoGrupo1 = codigoGrupo1;
    }

    public String getGrupo1() {
        return grupo1;
    }

    public void setGrupo1(String grupo1) {
        this.grupo1 = grupo1;
    }

    public BigInteger getCodigoGrupo2() {
        return codigoGrupo2;
    }

    public void setCodigoGrupo2(BigInteger codigoGrupo2) {
        this.codigoGrupo2 = codigoGrupo2;
    }

    public String getGrupo2() {
        return grupo2;
    }

    public void setGrupo2(String grupo2) {
        this.grupo2 = grupo2;
    }

    public Integer getClientes() {
        return clientes;
    }

    public void setClientes(Integer clientes) {
        this.clientes = clientes;
    }

    public Integer getCantFacturas() {
        return cantFacturas;
    }

    public void setCantFacturas(Integer cantFacturas) {
        this.cantFacturas = cantFacturas;
    }

    public Integer getClientesVentas() {
        return clientesVentas;
    }

    public void setClientesVentas(Integer clientesVentas) {
        this.clientesVentas = clientesVentas;
    }
    
}
