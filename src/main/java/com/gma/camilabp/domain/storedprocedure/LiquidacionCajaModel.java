/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain.storedprocedure;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author userdb6
 */
public class LiquidacionCajaModel implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private String cxcfpgSecuencia;
    private String  cxcfpgDescripcio;
    private String gentdoSecuencia;
    private String gentdoNombre;
    private BigDecimal total;
    private String numCuenta;
    private String numCheque;
    private String numPago;
    private Date fecha;
    private Integer secuencial;

    public LiquidacionCajaModel() {
    }

    public String getCxcfpgSecuencia() {
        return cxcfpgSecuencia;
    }

    public void setCxcfpgSecuencia(String cxcfpgSecuencia) {
        this.cxcfpgSecuencia = cxcfpgSecuencia;
    }

    public String getCxcfpgDescripcio() {
        return cxcfpgDescripcio;
    }

    public void setCxcfpgDescripcio(String cxcfpgDescripcio) {
        this.cxcfpgDescripcio = cxcfpgDescripcio;
    }

    public String getGentdoSecuencia() {
        return gentdoSecuencia;
    }

    public void setGentdoSecuencia(String gentdoSecuencia) {
        this.gentdoSecuencia = gentdoSecuencia;
    }

    public String getGentdoNombre() {
        return gentdoNombre;
    }

    public void setGentdoNombre(String gentdoNombre) {
        this.gentdoNombre = gentdoNombre;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public String getNumCuenta() {
        return numCuenta;
    }

    public void setNumCuenta(String numCuenta) {
        this.numCuenta = numCuenta;
    }

    public String getNumCheque() {
        return numCheque;
    }

    public void setNumCheque(String numCheque) {
        this.numCheque = numCheque;
    }

    public String getNumPago() {
        return numPago;
    }

    public void setNumPago(String numPago) {
        this.numPago = numPago;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Integer getSecuencial() {
        return secuencial;
    }

    public void setSecuencial(Integer secuencial) {
        this.secuencial = secuencial;
    }
    
}
