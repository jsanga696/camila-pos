/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain.storedprocedure;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb
 */
public class CarteraModel implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private String cliente;
    private String vendedor;
    private String supervisor;
    private String canalsubcanal;
    private String mercaderista;
    private String tipodoc;
    private String numdoc;
    private Date fechaemision;
    private Date fechavencimiento;
    private Integer diavencido;
    private BigDecimal valormovi;
    private BigDecimal saldo;

    public CarteraModel() {
    }

    public Integer getDiavencido() {
        return diavencido;
    }

    public void setDiavencido(Integer diavencido) {
        this.diavencido = diavencido;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    

    
    public String getTipodoc() {
        return tipodoc;
    }

    public void setTipodoc(String tipodoc) {
        this.tipodoc = tipodoc;
    }

    public String getNumdoc() {
        return numdoc;
    }

    public void setNumdoc(String numdoc) {
        this.numdoc = numdoc;
    }

    public Date getFechaemision() {
        return fechaemision;
    }

    public void setFechaemision(Date fechaemision) {
        this.fechaemision = fechaemision;
    }

    public Date getFechavencimiento() {
        return fechavencimiento;
    }

    public void setFechavencimiento(Date fechavencimiento) {
        this.fechavencimiento = fechavencimiento;
    }

    public BigDecimal getValormovi() {
        return valormovi;
    }

    public void setValormovi(BigDecimal valormovi) {
        this.valormovi = valormovi;
    }

    public BigDecimal getSaldo() {
        return saldo;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }

    public String getVendedor() {
        return vendedor;
    }

    public void setVendedor(String vendedor) {
        this.vendedor = vendedor;
    }

    public String getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(String supervisor) {
        this.supervisor = supervisor;
    }

    public String getCanalsubcanal() {
        return canalsubcanal;
    }

    public void setCanalsubcanal(String canalsubcanal) {
        this.canalsubcanal = canalsubcanal;
    }

    public String getMercaderista() {
        return mercaderista;
    }

    public void setMercaderista(String mercaderista) {
        this.mercaderista = mercaderista;
    }

    



    
}
