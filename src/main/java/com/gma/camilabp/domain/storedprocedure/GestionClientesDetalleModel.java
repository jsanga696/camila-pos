/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain.storedprocedure;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 *
 * @author userdb6
 */
public class GestionClientesDetalleModel implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private BigInteger codigoGrupo1;
    private String grupo1;
    private BigInteger codigoGrupo;
    private String grupo;
    private BigInteger codigoCliente;
    private String cliente;
    private Integer cantFacturas;
    private BigDecimal subTotal;
    private BigDecimal descuento;
    private BigDecimal impuesto;
    private BigDecimal neto;
    private BigDecimal ingreso;
    private BigDecimal netoSinImp;
    private BigDecimal utilidad;
    private BigDecimal ingresoS1;
    private BigDecimal ingresoS2;
    private BigDecimal ingresoS3;
    private BigDecimal ingresoS4;
    private BigDecimal ingresoS5;
    private BigDecimal ingresoMv;

    public GestionClientesDetalleModel() {
    }

    public BigInteger getCodigoGrupo1() {
        return codigoGrupo1;
    }

    public void setCodigoGrupo1(BigInteger codigoGrupo1) {
        this.codigoGrupo1 = codigoGrupo1;
    }

    public String getGrupo1() {
        return grupo1;
    }

    public void setGrupo1(String grupo1) {
        this.grupo1 = grupo1;
    }    

    public BigInteger getCodigoGrupo() {
        return codigoGrupo;
    }

    public void setCodigoGrupo(BigInteger codigoGrupo) {
        this.codigoGrupo = codigoGrupo;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public BigInteger getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(BigInteger codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public Integer getCantFacturas() {
        return cantFacturas;
    }

    public void setCantFacturas(Integer cantFacturas) {
        this.cantFacturas = cantFacturas;
    }

    public BigDecimal getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(BigDecimal subTotal) {
        this.subTotal = subTotal;
    }

    public BigDecimal getDescuento() {
        return descuento;
    }

    public void setDescuento(BigDecimal descuento) {
        this.descuento = descuento;
    }

    public BigDecimal getImpuesto() {
        return impuesto;
    }

    public void setImpuesto(BigDecimal impuesto) {
        this.impuesto = impuesto;
    }

    public BigDecimal getNeto() {
        return neto;
    }

    public void setNeto(BigDecimal neto) {
        this.neto = neto;
    }

    public BigDecimal getIngreso() {
        return ingreso;
    }

    public void setIngreso(BigDecimal ingreso) {
        this.ingreso = ingreso;
    }

    public BigDecimal getNetoSinImp() {
        return netoSinImp;
    }

    public void setNetoSinImp(BigDecimal netoSinImp) {
        this.netoSinImp = netoSinImp;
    }

    public BigDecimal getUtilidad() {
        return utilidad;
    }

    public void setUtilidad(BigDecimal utilidad) {
        this.utilidad = utilidad;
    }

    public BigDecimal getIngresoS1() {
        return ingresoS1;
    }

    public void setIngresoS1(BigDecimal ingresoS1) {
        this.ingresoS1 = ingresoS1;
    }

    public BigDecimal getIngresoS2() {
        return ingresoS2;
    }

    public void setIngresoS2(BigDecimal ingresoS2) {
        this.ingresoS2 = ingresoS2;
    }

    public BigDecimal getIngresoS3() {
        return ingresoS3;
    }

    public void setIngresoS3(BigDecimal ingresoS3) {
        this.ingresoS3 = ingresoS3;
    }

    public BigDecimal getIngresoS4() {
        return ingresoS4;
    }

    public void setIngresoS4(BigDecimal ingresoS4) {
        this.ingresoS4 = ingresoS4;
    }

    public BigDecimal getIngresoS5() {
        return ingresoS5;
    }

    public void setIngresoS5(BigDecimal ingresoS5) {
        this.ingresoS5 = ingresoS5;
    }

    public BigDecimal getIngresoMv() {
        return ingresoMv;
    }

    public void setIngresoMv(BigDecimal ingresoMv) {
        this.ingresoMv = ingresoMv;
    }
    
}
