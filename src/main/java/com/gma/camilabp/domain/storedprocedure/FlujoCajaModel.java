/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain.storedprocedure;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

/**
 *
 * @author userdb6
 */
public class FlujoCajaModel implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private String genofiSecuencia;
    private String genofiNombre;
    private BigInteger cxccliCodigo;
    private String cxccliSecuencia;
    private String cxccliRazonsocia;
    private BigInteger facvdrCodigo;
    private String facvdrSecuencia;
    private String facvdrNombre;
    private BigDecimal efectivo;
    private BigDecimal cheque;
    private BigDecimal total;
    private String numCuenta;
    private String numCheque;
    private String numPago;
    private Date fecha;
    private Integer secuencial;

    public FlujoCajaModel() {
    }

    public String getGenofiSecuencia() {
        return genofiSecuencia;
    }

    public void setGenofiSecuencia(String genofiSecuencia) {
        this.genofiSecuencia = genofiSecuencia;
    }

    public String getGenofiNombre() {
        return genofiNombre;
    }

    public void setGenofiNombre(String genofiNombre) {
        this.genofiNombre = genofiNombre;
    }

    public BigInteger getCxccliCodigo() {
        return cxccliCodigo;
    }

    public void setCxccliCodigo(BigInteger cxccliCodigo) {
        this.cxccliCodigo = cxccliCodigo;
    }

    public String getCxccliSecuencia() {
        return cxccliSecuencia;
    }

    public void setCxccliSecuencia(String cxccliSecuencia) {
        this.cxccliSecuencia = cxccliSecuencia;
    }

    public String getCxccliRazonsocia() {
        return cxccliRazonsocia;
    }

    public void setCxccliRazonsocia(String cxccliRazonsocia) {
        this.cxccliRazonsocia = cxccliRazonsocia;
    }

    public BigInteger getFacvdrCodigo() {
        return facvdrCodigo;
    }

    public void setFacvdrCodigo(BigInteger facvdrCodigo) {
        this.facvdrCodigo = facvdrCodigo;
    }

    public String getFacvdrSecuencia() {
        return facvdrSecuencia;
    }

    public void setFacvdrSecuencia(String facvdrSecuencia) {
        this.facvdrSecuencia = facvdrSecuencia;
    }

    public String getFacvdrNombre() {
        return facvdrNombre;
    }

    public void setFacvdrNombre(String facvdrNombre) {
        this.facvdrNombre = facvdrNombre;
    }

    public BigDecimal getEfectivo() {
        return efectivo;
    }

    public void setEfectivo(BigDecimal efectivo) {
        this.efectivo = efectivo;
    }

    public BigDecimal getCheque() {
        return cheque;
    }

    public void setCheque(BigDecimal cheque) {
        this.cheque = cheque;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public String getNumCuenta() {
        return numCuenta;
    }

    public void setNumCuenta(String numCuenta) {
        this.numCuenta = numCuenta;
    }

    public String getNumCheque() {
        return numCheque;
    }

    public void setNumCheque(String numCheque) {
        this.numCheque = numCheque;
    }

    public String getNumPago() {
        return numPago;
    }

    public void setNumPago(String numPago) {
        this.numPago = numPago;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Integer getSecuencial() {
        return secuencial;
    }

    public void setSecuencial(Integer secuencial) {
        this.secuencial = secuencial;
    }
    
}
