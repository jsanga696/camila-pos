/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain.storedprocedure;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author userdb6
 */
public class Cabecera implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private String nombre;
    private List<Detalle> listDetalle;
    private BigDecimal efectivo;
    private BigDecimal cheque;
    private BigDecimal total;
    private BigDecimal debito;
    private BigDecimal credito;
    private Integer valor1=0;
    private Integer valor2=0;
    private Integer valor3=0;

    public Cabecera() {
        listDetalle= new ArrayList<>();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Detalle> getListDetalle() {
        return listDetalle;
    }

    public void setListDetalle(List<Detalle> listDetalle) {
        this.listDetalle = listDetalle;
    }

    public BigDecimal getEfectivo() {
        return efectivo;
    }

    public void setEfectivo(BigDecimal efectivo) {
        this.efectivo = efectivo;
    }

    public BigDecimal getCheque() {
        return cheque;
    }

    public void setCheque(BigDecimal cheque) {
        this.cheque = cheque;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public BigDecimal getDebito() {
        return debito;
    }

    public void setDebito(BigDecimal debito) {
        this.debito = debito;
    }

    public BigDecimal getCredito() {
        return credito;
    }

    public void setCredito(BigDecimal credito) {
        this.credito = credito;
    }

    public Integer getValor1() {
        return valor1;
    }

    public void setValor1(Integer valor1) {
        this.valor1 = valor1;
    }

    public Integer getValor2() {
        return valor2;
    }

    public void setValor2(Integer valor2) {
        this.valor2 = valor2;
    }

    public Integer getValor3() {
        return valor3;
    }

    public void setValor3(Integer valor3) {
        this.valor3 = valor3;
    }

    
    public void generarValores(){
        this.efectivo= new BigDecimal("0.00");
        this.cheque= new BigDecimal("0.00");
        this.total= new BigDecimal("0.00");
        this.valor1= 0;
        this.valor2= 0;
        this.valor3= 0;
        this.listDetalle.forEach(d->{
            this.efectivo=this.efectivo.add(d.getEfectivo()==null?BigDecimal.ZERO:d.getEfectivo());
            this.cheque=this.cheque.add(d.getCheque()==null?BigDecimal.ZERO:d.getCheque());
            this.total=this.total.add(d.getTotal()==null?BigDecimal.ZERO:d.getTotal());
            if(d.getGc()!=null){
                this.valor1=this.valor1+d.getGc().getClientes();
                this.valor2=this.valor2+d.getGc().getClientesVentas();
                this.valor3=this.valor3+d.getGc().getCantFacturas();
            }
        });
        this.efectivo=this.efectivo.setScale(2, RoundingMode.HALF_UP);
        this.cheque=this.cheque.setScale(2, RoundingMode.HALF_UP);
        this.total=this.total.setScale(2, RoundingMode.HALF_UP);
    }
    
    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Cabecera)) {
            return false;
        }
        Cabecera other = (Cabecera) object;
        return !((this.nombre == null && other.nombre != null) || (this.nombre != null && !this.nombre.equals(other.nombre)));
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 11 * hash + Objects.hashCode(this.nombre);
        return hash;
    }
}
