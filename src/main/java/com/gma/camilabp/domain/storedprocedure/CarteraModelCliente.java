/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain.storedprocedure;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.Column;
import javax.validation.constraints.Size;

/**
 *
 * @author userdb
 */
public class CarteraModelCliente implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private String cliente;
    private List<CarteraModel> carteraModelList = new ArrayList<>();

    public CarteraModelCliente() {
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    
    

    public List<CarteraModel> getCarteraModelList() {
        return carteraModelList;
    }

    public void setCarteraModelList(List<CarteraModel> carteraModelList) {
        this.carteraModelList = carteraModelList;
    }
    
    public BigDecimal getMontoVencido()
    {
        return   carteraModelList.stream()
                .filter(Objects::nonNull)
                .filter(x->x.getValormovi() != null)
                .map(CarteraModel::getValormovi)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
       
    }

    public BigDecimal getSaldo()
    {
        return   carteraModelList.stream()
                .filter(Objects::nonNull)
                .filter(x->x.getSaldo()!= null)
                .map(CarteraModel::getSaldo)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }
    
}
