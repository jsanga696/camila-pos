/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.domain.storedprocedure;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author userdb6
 */
public class Detalle implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private String nombre;
    private BigDecimal efectivo;
    private BigDecimal cheque;
    private BigDecimal total;
    private BigDecimal debito;
    private BigDecimal cretito;
    private GestionClientesModel gc;

    public Detalle() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public BigDecimal getEfectivo() {
        return efectivo;
    }

    public void setEfectivo(BigDecimal efectivo) {
        this.efectivo = efectivo;
    }

    public BigDecimal getCheque() {
        return cheque;
    }

    public void setCheque(BigDecimal cheque) {
        this.cheque = cheque;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public BigDecimal getDebito() {
        return debito;
    }

    public void setDebito(BigDecimal debito) {
        this.debito = debito;
    }

    public BigDecimal getCretito() {
        return cretito;
    }

    public void setCretito(BigDecimal cretito) {
        this.cretito = cretito;
    }

    public GestionClientesModel getGc() {
        return gc;
    }

    public void setGc(GestionClientesModel gc) {
        this.gc = gc;
    }
    
}
