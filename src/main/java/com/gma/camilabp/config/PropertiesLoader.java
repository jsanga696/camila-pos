/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.config;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import jodd.props.Props;

/**
 *
 * @author userdb6
 */
public class PropertiesLoader {
    protected ServletContext servletContext;

    public PropertiesLoader(ServletContext servletContext) {
        this.servletContext = servletContext;
    }
        
    public void load() {
        Props prop = new Props();
        try{
            prop.load(servletContext.getResourceAsStream("/WEB-INF/settings.properties"));
            SisVars.driverClassPos = prop.getValue("pos.driverClass");
            SisVars.urlPos = prop.getValue("pos.url");
            SisVars.usernamePos = prop.getValue("pos.username");
            SisVars.passwordPos = prop.getValue("pos.password");
        } catch (IOException ex) {
            Logger.getLogger(PropertiesLoader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
