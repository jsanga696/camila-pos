/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.config;

/**
 *
 * @author dbriones
 */
public class SisVars {

    public static String urlbase;
    public static String facesUrl = "/faces";
    public static String urlbaseFaces;
    public static String urlbaseFacesSinBarra;
    public static String ejbRuta="java:global/camilaBPReport/";// = "java:global/JavaServerFaces/";
    public static String formatoArchivos;// ="/(\\.|\\/)(gif|jpe?g|png|pdf|xlsx|docx|xlsm|dwg|shp|doc|xls|ppt|pptx|tif|txt)$/";
    
    public static String driverClassPos;
    public static String urlPos;
    public static String usernamePos;
    public static String passwordPos;
}
