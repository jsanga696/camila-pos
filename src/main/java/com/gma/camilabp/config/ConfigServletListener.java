/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.config;

import java.util.Locale;
import java.util.TimeZone;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import jodd.datetime.JDateTimeDefault;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.converters.DateConverter;

/**
 * Web application lifecycle listener.
 *
 * @author dbriones
 */
@WebListener()
public class ConfigServletListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {

        System.setProperty("org.apache.el.parser.COERCE_TO_ZERO", "false");
        // timezone y locale default para el sistema
        TimeZone.setDefault(TimeZone.getTimeZone("America/Guayaquil"));
        JDateTimeDefault.timeZone = TimeZone.getTimeZone("America/Guayaquil");
        Locale.setDefault(new Locale("es", "EC"));

        SisVars.urlbase = sce.getServletContext().getContextPath() + "/";
        SisVars.urlbaseFaces = sce.getServletContext().getContextPath() + SisVars.facesUrl + "/";
        SisVars.urlbaseFacesSinBarra = sce.getServletContext().getContextPath() + SisVars.facesUrl;

        DateConverter dateConverter = new DateConverter();
        dateConverter.setPattern("dd MM yyyy HH mm ss");
        ConvertUtils.register(dateConverter, java.util.Date.class);
        System.out.println("DATE CONVERTER REGISTERED");

        // carga de propiedades
        PropertiesLoader props1 = new PropertiesLoader(sce.getServletContext());
        props1.load();

        //Notificador not = new Notificador();
        //not.notificar();

    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        //
        System.out.println("cerrando kiosko!!!");
    }
}
