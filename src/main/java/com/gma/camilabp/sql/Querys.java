/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.sql;


/**
 *
 * @author userdb6
 */
public class Querys {
    //CONSULTA CLIENTES 
    public static String clientesActivosByCompaniaOficinaCanalesCount="SELECT COUNT(DISTINCT c) "
            + "FROM CxcmCliente c, RtkrVenrutav vr "
            + "WHERE c.cxcrutCodigo.cxcrutCodigo=vr.cxcrutCodigo AND c.cxccliEstactivo=1 AND c.genciaCodigo=:genciaCodigo "
            + "AND c.genofiCodigo IN (:genofiCodigo) AND c.cxcrutCodigo IN (:cxcrutCodigo) "
            + "AND c.cxcmcrCodigo IN (:cxcmcrCodigo) "
            + "AND c.cxcznaCodigo IN (:cxcznaCodigo) "
            + "AND c.cxcmzaCodigo IN (:cxcmzaCodigo) "
            + "AND c.cxccdiCodigo IN (:cxccdiCodigo) "
            + "AND vr.facvdrCodigo.facjveCodigo IN (:facjveCodigo) "
            + "AND vr.facvdrCodigo.facsupCodigo IN (:facsupCodigo) "
            + "AND vr.facvdrCodigo IN (:facvdrCodigo)";
    
    public static String clientesActivosByCompaniaOficinaCanalesVendedoresSupervisoresGrupo="SELECT DISTINCT c "
            + "FROM CxcmCliente c, RtkrVenrutav vr "
            + "WHERE c.cxcrutCodigo.cxcrutCodigo=vr.cxcrutCodigo AND c.cxccliEstactivo=1 AND c.genciaCodigo=:genciaCodigo "
            + "AND c.genofiCodigo IN (:genofiCodigo) AND c.cxcrutCodigo IN (:cxcrutCodigo) "
            + "AND c.cxcmcrCodigo IN (:cxcmcrCodigo) "
            + "AND c.cxcznaCodigo IN (:cxcznaCodigo) "
            + "AND c.cxcmzaCodigo IN (:cxcmzaCodigo) "
            + "AND c.cxccdiCodigo IN (:cxccdiCodigo) "
            + "AND vr.facvdrCodigo.facjveCodigo IN (:facjveCodigo) AND vr.facvdrCodigo.facsupCodigo IN (:facsupCodigo) "
            + "AND vr.facvdrCodigo IN (:facvdrCodigo) ";
    
    //CONSULTA RUTAS
    public static String rutas="SELECT r FROM CxcrRuta r";
    
    //CONSULTA MACROZONAS
    public static String macrozonas="SELECT mac FROM CxcrMacrozona mac";
    
    //CONSULTA ZONAS
    public static String zonas="SELECT z FROM CxcrZonanifica z";
    
    //CONSULTA MICROZONAS
    public static String microzonas="SELECT mic FROM CxcrMicrozona mic";
    
    //CONSULTA CANALES
    public static String canales="SELECT c.cxccdiCodigo FROM CxcrCanaldistr c";
    
    //CONSULTA OFICINAS
    public static String oficinas="SELECT o.genofiCodigo FROM GenrOficina o";
    
    //CONSULTA JEFES
    public static String jefes="SELECT j.facjveCodigo FROM FacrJefeventa j";
    
    //CONSULTA SUPERVISORES
    public static String supervisores="SELECT s FROM FacrSupervisor s";
    
    //CONSULTA VENDEDORES
    public static String vendedores="SELECT v FROM FacrVendedor v";
    
    //CONSULTA VENDEDORES POR RUTA
    public static String vendedoresByRuta="SELECT vr.facvdrCodigo.facvdrCodigo FROM RtkrVenrutav vr WHERE vr.cxcrutCodigo IN (:cxcrutCodigo)";
    
    //CONSULTA RUTAS POR VENDEDOR
    public static String rutasByVendedor="SELECT vr.cxcrutCodigo FROM RtkrVenrutav vr WHERE vr.facvdrCodigo.facvdrCodigo IN (:facvdrCodigo)";
    
    //CONSULTA FACTURAS
    public static String clientesEnFacturasByCompaniaOficinaCanalMesAnioCount="SELECT COUNT(DISTINCT df.cxccliCodigo) "
            + "FROM FacttDetllefacturas df, RtkrVenrutav vr "
            + "WHERE df.cxccliCodigo.cxcrutCodigo.cxcrutCodigo=vr.cxcrutCodigo AND vr.facvdrCodigo.facvdrCodigo=df.facvrdCodigo AND "
            + "df.genciaCodigo=:genciaCodigo AND df.genofiCodigo IN (:genofiCodigo) "
            + "AND df.cxccliCodigo.cxcrutCodigo IN (:cxcrutCodigo) "
            + "AND df.cxccliCodigo.cxcmcrCodigo IN (:cxcmcrCodigo) "
            + "AND df.cxccliCodigo.cxcznaCodigo IN (:cxcznaCodigo) "
            + "AND df.cxccliCodigo.cxcmzaCodigo IN (:cxcmzaCodigo) "
            + "AND df.cxccliCodigo.cxccliEstactivo=1 AND df.cxccliCodigo.cxccdiCodigo IN (:cxccdiCodigo) "
            + "AND vr.facvdrCodigo.facjveCodigo IN (:facjveCodigo) AND vr.facvdrCodigo.facsupCodigo IN (:facsupCodigo) "
            + "AND vr.facvdrCodigo IN (:facvdrCodigo) "
            + "AND YEAR(df.cxcdeuFechaemisi)=:anio AND MONTH(df.cxcdeuFechaemisi)=:mes";
    
    public static String aniosEnFacturas ="SELECT DISTINCT YEAR(df.cxcdeuFechaemisi) FROM FacttDetllefacturas df ORDER BY 1";
    
    public static String facturasByCompaniaOficinaCanalJefSupVendedorClienteMesAnio="SELECT df FROM FacttDetllefacturas df, RtkrVenrutav vr "
            + "WHERE df.cxccliCodigo.cxcrutCodigo.cxcrutCodigo=vr.cxcrutCodigo AND vr.facvdrCodigo.facvdrCodigo=df.facvrdCodigo AND "
            + "df.genciaCodigo=:genciaCodigo AND df.genofiCodigo IN (:genofiCodigo) "
            + "AND df.cxccliCodigo.cxcrutCodigo IN (:cxcrutCodigo) "
            + "AND df.cxccliCodigo.cxcmcrCodigo IN (:cxcmcrCodigo) "
            + "AND df.cxccliCodigo.cxcznaCodigo IN (:cxcznaCodigo) "
            + "AND df.cxccliCodigo.cxcmzaCodigo IN (:cxcmzaCodigo) "
            + "AND df.cxccliCodigo.cxccliEstactivo=1 AND df.cxccliCodigo.cxccdiCodigo IN (:cxccdiCodigo) "
            + "AND vr.facvdrCodigo.facjveCodigo IN (:facjveCodigo) AND vr.facvdrCodigo.facsupCodigo IN (:facsupCodigo) "
            + "AND vr.facvdrCodigo IN (:facvdrCodigo) "
            + "AND df.cxccliCodigo.cxccliCodigo=:cxccliCodigo "
            + "AND YEAR(df.cxcdeuFechaemisi)=:anio AND MONTH(df.cxcdeuFechaemisi)=:mes ";
    
    public static String cantidadClientesAgrupadoFrom="FROM CxcmCliente c, RtkrVenrutav vr "
            + "WHERE c.cxcrutCodigo.cxcrutCodigo=vr.cxcrutCodigo AND c.cxccliEstactivo=1 AND c.genciaCodigo=:genciaCodigo "
            + "AND c.genofiCodigo IN (:genofiCodigo) AND c.cxcrutCodigo IN (:cxcrutCodigo) "
            + "AND c.cxcmcrCodigo IN (:cxcmcrCodigo) "
            + "AND c.cxcznaCodigo IN (:cxcznaCodigo) "
            + "AND c.cxcmzaCodigo IN (:cxcmzaCodigo) "
            + "AND c.cxccdiCodigo IN (:cxccdiCodigo) "
            + "AND vr.facvdrCodigo.facjveCodigo IN (:facjveCodigo) AND vr.facvdrCodigo.facsupCodigo IN (:facsupCodigo) "
            + "AND vr.facvdrCodigo IN (:facvdrCodigo) ";
    
    public static String cantidadClientesEnFacturasAgrupadoFrom="FROM FacttDetllefacturas df, RtkrVenrutav vr "
            + "WHERE df.cxccliCodigo.cxcrutCodigo.cxcrutCodigo=vr.cxcrutCodigo AND df.genciaCodigo=:genciaCodigo AND vr.facvdrCodigo.facvdrCodigo=df.facvrdCodigo AND df.genofiCodigo IN (:genofiCodigo) "
            + "AND df.cxccliCodigo.cxcrutCodigo IN (:cxcrutCodigo) "
            + "AND df.cxccliCodigo.cxcmcrCodigo IN (:cxcmcrCodigo) "
            + "AND df.cxccliCodigo.cxcznaCodigo IN (:cxcznaCodigo) "
            + "AND df.cxccliCodigo.cxcmzaCodigo IN (:cxcmzaCodigo) "
            + "AND df.cxccliCodigo.cxccliEstactivo=1 AND df.cxccliCodigo.cxccdiCodigo IN (:cxccdiCodigo) "
            + "AND vr.facvdrCodigo.facjveCodigo IN (:facjveCodigo) AND vr.facvdrCodigo.facsupCodigo IN (:facsupCodigo) "
            + "AND vr.facvdrCodigo IN (:facvdrCodigo) "
            + "AND YEAR(df.cxcdeuFechaemisi)=:anio AND MONTH(df.cxcdeuFechaemisi)=:mes ";
        
}
