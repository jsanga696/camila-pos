/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.sql;

/**
 *
 * @author userdb5
 */
public class QueryPosCxc {
    
    public static String getPagosTC = "SELECT det.* FROM FACT_CABVENTA cab INNER JOIN CXCT_DOCUAPLICA doc ON cab.FACCVT_numdocumen = doc.CXCDEU_numdocumen" +
        " INNER JOIN FACT_CABVTACAJA caja ON cab.FACCVT_numsecuenc = caja.FACVTC_secuenvta INNER JOIN GENM_CAJA genc ON caja.GENCAJ_codigo = genc.GENCAJ_codigo" +
        " INNER JOIN CBPSEGM_TURNO t ON genc.FACSCA_idsececaja = t.segtur_codigo INNER JOIN FACT_SESIONCAJA sc ON t.segtur_codigo = sc.FACSCA_idsececaja " +
        " INNER JOIN cxct_Detdocumcr det ON doc.CXCCCR_numsecuen = det.CXCDPG_numerosec WHERE det.CXCFPG_codigo ='TAR' ";
    
    public static String getPagosTCTemp = "SELECT det.* FROM cxct_Detdocumcr det INNER JOIN -nombre-tabla-temp- t ON "
            + "det.CXCCCR_numdocumcr = t.CXCCCR_numsecuen AND det.CXCDPG_numlinea = t.CXCDPG_numlinea AND "
            + "det.CXCCLI_codigo = t.CXCCLI_nombre AND det.CXCDPG_numerosec = t.CXCDPG_secubanco";
    
    public static String borrarDataTempTb = "DELETE FROM -nombre-tabla-temp-";
    
    //COMPRA
    public static String sugeridoPorNumeroFactura="SELECT s FROM ComtCabpedsugerido s WHERE CAST(s.cOMPDSnumero AS VARCHAR(9)) IN (SELECT c.cOMCCMnumdocurel FROM ComtCabcompra c WHERE c.gENOFIcodigo=:secuenciaOficina AND c.cXPPROcodigo=:codigoProveedor AND c.cOMCCMnumfacprov=:numFactura)";
    public static String compraPorNumeroFactura="SELECT c FROM ComtCabcompra c WHERE c.gENOFIcodigo=:secuenciaOficina AND c.cXPPROcodigo=:codigoProveedor AND c.cOMCCMnumfacprov=:numFactura ";
    public static String grupoSugeridoPorNumeroFactura="SELECT A.codigo,A.dato, A.cajasugerido, A.cajarecibido, B.cajacompra, B.unidadcompra " +
            "FROM ( SELECT ds.INVITM_codigo AS codigo,i.INVITM_nombre AS dato, SUM(ds.COMPDS_cantisugeridas) AS cajasugerido,SUM(ds.COMPDS_cantirecibidas) AS cajarecibido " +
                    "FROM COMT_CABPEDSUGERIDO cs " +
                    "INNER JOIN COMT_DETPEDSUGERIDO ds ON (cs.COMPDS_secuencia=ds.COMPDS_secuencia) " +
                    "INNER JOIN INVM_ITEM i ON (ds.INVITM_codigo=i.INVITM_codigo AND cs.GENCIA_codigo=i.GENCIA_codigo) " +
                    "WHERE cs.GENOFI_codigo=? AND cs.COMPDS_numero IN ( SELECT c.COMCCM_numdocurel FROM COMT_CABCOMPRA c WHERE c.GENOFI_codigo=? AND c.COMTDO_codigorel='PDS' AND c.CXPPRO_codigo=? AND c.COMCCM_numfacprov=? posteo ) " +
                    "GROUP BY ds.INVITM_codigo,i.INVITM_nombre ) AS A " +
            "INNER JOIN ( SELECT d.INVITM_codigo AS codigo, SUM(d.COMDCM_canticaja) AS cajacompra, SUM(d.COMDCM_cantiunida) AS unidadcompra " +
            "FROM COMT_CABCOMPRA c " +
            "INNER JOIN COMT_DETCOMPRA  d ON (c.COMCCM_numsecucial=d.COMCCM_numsecucial) " +
            "WHERE c.COMTDO_codigorel='PDS' AND d.GENOFI_codigo=? AND c.CXPPRO_codigo=? AND c.COMCCM_numfacprov=? posteo " +
            "GROUP BY d.INVITM_codigo ) AS B ON (A.codigo=B.codigo)";
}
