/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.sql;

/**
 *
 * @author userdb6
 */
public class QuerysPos {

    public static String usuariosActivosByOficina = "SELECT uo FROM CbpgenrUsuarioxofi uo WHERE uo.genofiCodigo=:oficina AND uo.segusuCodigo.segusuEstado=:estado";
    public static String countTransaciones = "SELECT COUNT(t.facvtcSecuenvta) FROM FactCabvtacaja t WHERE t.fACSCAidsececaja=:turno";
    public static String oficinasByUser = "SELECT uo.genofiCodigo FROM CbpgenrUsuarioxofi uo WHERE uo.segusuCodigo=:segusuCodigo AND uo.segusuCodigo.segusuEstado=:estado";
    public static String filtroNivel1 = "SELECT INVIN1_codigo AS codigo, INVIN1_descripcio AS dato FROM invr_nivel1 WHERE gencia_codigo=? and INVIN1_estadoUni = 'S' ORDER BY INVIN1_codigo";
    public static String filtroNivel2 = "SELECT INVIN2_codigo AS codigo, INVIN2_descripcio AS dato FROM invr_nivel2 WHERE gencia_codigo=? and INVIN2_estado = 'S'";
    public static String filtroNivel3 = "SELECT INVIN3_codigo AS codigo, INVIN3_descripcio AS dato FROM invr_nivel3 WHERE gencia_codigo=? and INVIN3_estado = 'S'";
    public static String filtroNivel4 = "SELECT INVIN4_codigo AS codigo, INVIN4_descripcio AS dato FROM invr_nivel4 WHERE gencia_codigo=? and INVIN4_estado = 'S'";
    public static String itemFiltroNivel1 = "SELECT INVIN1_codigo AS codigo, INVIN1_descripcio AS dato FROM invr_nivel1 WHERE gencia_codigo=? and INVIN1_codigo=? and INVIN1_estadoUni = 'S'";
    public static String itemFiltroNivel2 = "SELECT INVIN2_codigo AS codigo, INVIN2_descripcio AS dato FROM invr_nivel2 WHERE gencia_codigo=? and INVIN2_codigo=? and INVIN2_estado = 'S'";
    public static String itemFiltroNivel3 = "SELECT INVIN3_codigo AS codigo, INVIN3_descripcio AS dato FROM invr_nivel3 WHERE gencia_codigo=? and INVIN3_codigo=? and INVIN3_estado = 'S'";
    public static String itemFiltroNivel4 = "SELECT INVIN4_codigo AS codigo, INVIN4_descripcio AS dato FROM invr_nivel4 WHERE gencia_codigo=? and INVIN4_codigo=? and INVIN4_estado = 'S'";
    public static String filtroNivel2ByN1 = "SELECT INVIN2_codigo AS codigo, INVIN2_descripcio AS dato FROM invr_nivel2 WHERE gencia_codigo=? and INVIN1_codigo=? and INVIN2_estado = 'S'";
    public static String filtroNivel3ByN2 = "SELECT INVIN3_codigo AS codigo, INVIN3_descripcio AS dato FROM invr_nivel3 WHERE gencia_codigo=? and INVIN1_codigo=? and INVIN2_codigo=? and INVIN3_estado = 'S'";
    public static String filtroNivel4ByN3 = "SELECT INVIN4_codigo AS codigo, INVIN4_descripcio AS dato FROM invr_nivel4 WHERE gencia_codigo=? and INVIN1_codigo=? and INVIN2_codigo=? and INVIN3_codigo=? and INVIN4_estado = 'S'";
    public static String filtroSugerido = "SELECT n.INVIN2_codigo AS codigo, n.INVIN2_descripcio AS dato FROM INVR_nivel2 n INNER JOIN GENR_PROVNIVINV pn ON (n.GENCIA_codigo=pn.GENCIA_codigo AND n.INVIN1_codigo=pn.INVIN1_codigo AND n.INVIN2_codigo=pn.INVIN2_codigo) WHERE pn.GENCIA_codigo=? AND pn.GENOFI_codigo=? AND pn.CXPPRO_codigo=?";
    public static String inveRepdiasInventarioSelect = "SELECT * FROM INVE_REPDIASINVENTARIO (NOLOCK) ";                        
    public static String inveRepdiasInventarioWhere ="WHERE SEGUSU_codigo=? AND SEGOPC_codigo=? AND SEGMAQ_codigo=? "
            + "AND INVIN1_codin1item IS NOT NULL ";
    public static String inveRepdiasInventarioOrder =" ORDER BY INVIN1_descripcio ASC, INVIN1_codin1item ASC, INVIN2_descripcio ASC, INVIN2_codin2item ASC, INVIN3_descripcio ASC, INVIN3_codin3item ASC, INVIN4_descripcio ASC, INVITM_nombre ASC, INVIN4_codin4item ASC";
    
    //public static String claveSupervisor="SELECT CAST(u.segusu_clave AS VARCHAR(100)) FROM CBPSEGM_USUARIO u INNER JOIN FACR_SUPERVISOR s ON (u.segusu_login=s.SEGUSU_codigo) WHERE s.FACSUP_estactivo='A' AND u.segusu_estado=1 AND s.FACSUP_codigo=?";
    public static String claveSupervisor="SELECT FACSUP_clave FROM FACR_SUPERVISOR WHERE FACSUP_estactivo='A' AND FACSUP_codigo=?";
    public static String codigoSupervisor="SELECT FACSUP_codigo FROM FACR_SUPERVISOR WHERE SEGUSU_codigo=?";
    public static String fechaClaveSupervisor="SELECT FACSUP_editclave FROM FACR_SUPERVISOR WHERE FACSUP_estactivo='A' AND FACSUP_codigo=?";
    public static String tipoGeneracionCodigoCliente="SELECT CAST(GENCIA_codautocli AS VARCHAR(1)) FROM GENR_COMPANIA WHERE GENCIA_codigo=?";
    /**
     * PARA EL PROCESO DE RETENCIONES
     */
    
    public static String datosRetencion = "SELECT * FROM X_POS_RETENCION_FACTURA WHERE usuario_reporte = ? AND num_factura = ? AND estado = 'T';";
    public static String datosRetencionP = "SELECT * FROM X_POS_RETENCION_FACTURA WHERE num_factura = ? AND estado = ?;";
    public static String datosRetencionDetalle = "SELECT * FROM X_POS_RETENCION_FACTURA_DET WHERE usuario_reporte = ? AND num_factura = ?;";
    public static String datosRetencionTempFact = "SELECT * FROM X_POS_RETENCION_FACTURA WHERE usuario_reporte = ? AND estado = 'X';";
    public static String datosRetencionDetalleTempFact = "SELECT * FROM X_POS_RETENCION_FACTURA_DET WHERE usuario_reporte = ? AND num_factura = ?;";
    public static String datosRetencionUser = "SELECT * FROM X_POS_RETENCION_FACTURA WHERE usuario_reporte = ? AND estado = ?;";
    public static String datosRetencionUserDetalle = "SELECT * FROM X_POS_RETENCION_FACTURA_DET WHERE usuario_reporte = ? AND num_factura=?;";
    public static String eliminarRetencionUser = "DELETE FROM X_POS_RETENCION_FACTURA WHERE usuario_reporte = ? AND estado = ?;";
    public static String eliminarRetencionUserDetalle = "DELETE FROM X_POS_RETENCION_FACTURA_DET WHERE usuario_reporte = ? AND num_factura=?;";
    public static String actualizarRetencionTemp = "UPDATE X_POS_RETENCION_FACTURA SET sRIRETcodigo = ?, tipo_retencion = ?, num_autorizacion = ?, turno = ? WHERE usuario_reporte = ? AND num_factura = ?;";
    public static String actualizarRetencionTempP = "UPDATE X_POS_RETENCION_FACTURA SET sRIRETcodigo = ?, tipo_retencion = ?, num_autorizacion = ?, turno = ?, estado = ? WHERE usuario_reporte = ? AND num_factura = ?;";
    public static String registroRetencionTablasCamila = "EXEC SP_CXCM_DOCUMENTO @GENCIA_codigo = ?,@CXCDEU_numesecuen= ?,@GENOFI_codigo=?,@CXCCLI_codigo=?,@CXCDEU_fechaemisi=?,@CXCDEU_fechavenci=?,@FACVDR_codigo=?,@CXCFPG_codigo=?,@CXCDEU_terminopag=?,@GENMOD_codigo=?, "
            + "@GENTDO_codigo=?,@CXCRUT_codigodocu=?,@CXCDEU_numdocumen=?,@GENCAU_codcausadb=?,@GENCIA_coddocrela=?,@GENOFI_coddocrela=?,@GENMOD_coddocrela=?,@GENTDO_coddocrel=?,@CXCRUT_coddocrela=?,@CXCDEU_numdocrela=?, "
            + "@CXCDEU_montomov=?,@CXCDEU_saldomov=?,@CXCDEU_subtotalmov=?,@CXCDEU_descuentomov=?,@CXCDEU_imptomov=?,@CXCDEU_concepto=?,@CXCDEU_detalle=?,@CXCDEU_tipoban=?,@CXCDEU_codbanco=?,@CXCDEU_numchq=?, "
            + "@CXCDEU_cuentacte=?,@CXCDEU_usuaingreg=?,@CXCDEU_ctacontable=?,@CXCDEU_secfactrela=?,@CXCDEU_motdevolu=?,@CXCDEU_codautoriza=?,@CXCDEU_codifiscoret=?,@GENTAL_secuencia=?,@CXCDEU_baseimponible=?,@CXCDEU_basecero=?, "
            + "@CXCDEU_imptoservicio=?,@CXCDEU_imptoICE=?,@ACCION=?";
    /**
     * FIN PROCESO DE RETENCIONES
     */
    public static String documentosCreditosByCliente = "SELECT dc FROM CxctDocumento dc WHERE dc.cXCDEUsaldomov<0 AND dc.gENTDOcodigo IN ('NCR','NCS','RET','ANT') AND dc.gENOFIcodigo=:oficina AND dc.cXCCLIcodigo=:cliente AND dc.cXCDEUestado NOT IN ('A')";
    public static String documentosCreditosByClienteSeleccionados = "SELECT dc FROM CxctDocumento dc WHERE dc.cXCDEUsaldomov<0 AND dc.gENTDOcodigo IN ('NCR','NCS','RET','ANT') AND dc.gENOFIcodigo=:oficina AND dc.cXCCLIcodigo=:cliente AND dc.cXCDEUestado NOT IN ('A') AND dc.cxctDocumentoPK.cXCDEUnumesecuen IN (:secuencias)";
    /**
     * DETALLE FACTURA*
     */
    public static String consultaDetalleFactura = "SELECT 0 ID_secuencia,df.FACDTC_secuelinea, df.GENCIA_codigo GENCIA_codigo, df.GENOFI_codigo GENOFI_codigo, "
            + "i.INVITM_codbodega GENBOD_codigo, df.FACDTC_linearela idLineaPed, v.FACVDR_codigo, v.CXCCLI_codigo, v.CXCRUT_codigo, df.INVITM_codigo, '' INVITM_estaitem, "
            + "df.FACDTC_secuelinea ID_lineaFAC, df.FACDVC_cantunidad + (df.FACDVC_cantcajas*FACDVC_unidademba) FACDPD_cantfuncional,  "
            + "df.FACDVC_preciomov FACDVT_precio, df.FACDVC_subtotmov FACDVT_subtotal, df.FACDVC_desctomov FACDVT_descuento, df.FACDVC_impuesmov FACDVT_impuesto, "
            + "df.FACDVC_netomov FACDVT_neto, v.FACDVT_listaprecio FACDVT_listaprecio,i.INVITM_nombre nombreItem, df.FACDVT_cantfundev "
            + "FROM FACT_DETVTACAJA df INNER JOIN FACT_CABVENTA v ON v.FACCVT_numsecuenc=df.FACVTC_secuenvta "
            + "INNER JOIN INVM_ITEM i ON (df.GENCIA_codigo=i.GENCIA_codigo AND df.INVITM_codigo=i.INVITM_codigo) WHERE df.FACVTC_secuenvta = ?";
    public static String devolucionTemp = "EXEC SP_FACP_LLENADOTEMPSAVEDEVOLUCION @NAME_table=?,@GENCIA_codigo=?,@GENOFI_codigo=?,@GENBOD_codigo=?,@ID_lineaFAC=?,"
            + "@FACCPD_fechaemisi=?,@CXCCLI_codigo=?,@FACVDR_codigo=?,@CXCRUT_codigo=?,@INVITM_codigo=? ,@INVITM_estaitem=?,@FACDPD_cantfuncional=?,@FACDPD_precio=?,"
            + "@FACDPD_subtotal=?,@FACDPD_descuento=?,@FACDPD_impuesto=?,@FACDPD_neto=?,@FACDPD_listaprecio=?";
    public static String registrarDevolucion = "EXEC SP_FACP_GENRARADEVOLUCIONVTA @NAME_table=?,@GENCIA_codigo=?,@GENOFI_codigo=?,@GENUS_codigo=?,@FECHA_PROCESO=?,@FACCVT_numsecuenc=?,"
            + "@FACCVT_motvdevolu=?,@OBSERBACION=?,@GENTAL_secuencia=?,@FACCVT_numeroNCR=?";    
    public static String registrarDevolucion1 = "EXEC SP_FACP_GENRARADEVOLUCIONVTA @NAME_table=?,@GENCIA_codigo=?,@GENOFI_codigo=?,@GENUS_codigo=?,@FECHA_PROCESO=?,@FACCVT_numsecuenc=?,"
            + "@FACCVT_motvdevolu=?,@OBSERBACION=?,@GENTAL_secuencia=?";
    public static String detallePagoFact = "SELECT GENTDO_codigocr, CXCCCR_numdocumcr, CXCFPG_codigo, GENMON_codigo, CXCDPG_valormov FROM CXCT_DETDOCUMCR ";
    
    public static String obtenerNotaVenta = "SELECT FACCVT_numsecuenc secuencia, FACCVT_numdocumen secuenciaCxc FROM FACT_CABVENTA WHERE FACCVT_numdocumen = ? AND GENTDO_codigo = 'DFA' AND FACCVT_secdocurel = ?";
    
    public static String logBitacora = "SELECT CAST(u.segusu_login AS VARCHAR(120)) AS segusu_login, CAST(u.segusu_abreviado AS VARCHAR(120)) AS segusu_abreviado, t.segtur_numero, t.segtur_apertura, t.segtur_maquina,"
            + "b.cbpbit_fecha, b.cbpbit_accion, b.gencaj_codigo, b.cbpbit_detalle FROM CBPLOG_BITACORA b INNER JOIN CBPSEGM_USUARIO u "
            + "ON (b.segusu_codigo=u.segusu_codigo) INNER JOIN CBPSEGM_TURNO t ON (t.segtur_numero=b.segtur_numero) WHERE b.segtur_numero=?";
        
    public static String importe="SELECT venta, devolucion, impuesto, descuento, ingreso, gasto, total, ingreso_adicional, cierre FROM RPTIMPORTE WHERE turno=? AND usuario_reporte=?";
    
    public static String estadistica="SELECT venta, venta_cliente, anulada, sesion, apertura, transaciones, valor_venta, items_eliminados,colaboradores, registrados, clientes FROM RPTESTADISTICA WHERE turno=? AND usuario_reporte=?";
    
    public static String formaPago="SELECT efectivo, cheque, tarjeta, documento, credito FROM RPTPAGO WHERE turno=? AND usuario_reporte=?";
    
    public static String porcentajeImpuesto = "SELECT ISNULL(SUM(ip.INVIPO_porcenta),0.00) FROM INVR_IMPTOXPROD ip " +
                        "INNER JOIN GENR_IMPUESTO i ON (ip.GENIMP_codigo = i.GENIMP_codigo AND ip.GENCIA_codigo = i.GENCIA_codigo) " +
                        "WHERE i.GENIMP_estado='A' AND ip.INVIPO_estactivo='A' AND ip.INVITM_codigo=? " +
                        "AND ip.GENCIA_codigo=? AND ? BETWEEN i.GENIMP_fechadesde AND i.GENIMP_fechahasta";
    public static String itemsPolitica = "SELECT i FROM InvmItem i WHERE i.invmItemPK.gENCIAcodigo=:genciaSecuencia AND i.invmItemPK.iNVITMcodigo IN (SELECT pi.iNVITMcodigo FROM CbpgenrPoliticaitem pi WHERE pi.genpolCodigo=:politica AND pi.genpitEstado=1)";
    public static String bodegasPolitica = "SELECT pb.genbodCodigo FROM CbpgenrPoliticabodega pb WHERE pb.genpolCodigo=:politica AND pb.genpboEstado=1";
    public static String itemsPoliticaPorCantidad = "SELECT i FROM InvmItem i WHERE i.invmItemPK.gENCIAcodigo=:genciaSecuencia AND i.invmItemPK.iNVITMcodigo IN (SELECT pi.iNVITMcodigo FROM CbpgenrPoliticaitem pi WHERE pi.genpolCodigo.genpolCantidad=:cantidad AND pi.genpolCodigo.genpolEstado=1 AND pi.genpitEstado=1)";
    public static String politicaItems = "SELECT pi FROM CbpgenrPoliticaitem pi WHERE pi.genpolCodigo=:politica AND pi.genpitEstado=1";
    public static String politicasItem = "SELECT pi.genpolCodigo FROM CbpgenrPoliticaitem pi WHERE pi.iNVITMcodigo=:item AND pi.genpolCodigo.genciaCodigo.genciaSecuencia=:secuenciaCompania AND pi.genpitEstado=1 AND pi.genpolCodigo.genpolEstado=1 ORDER BY pi.genpolCodigo.genpolCantidad ASC";
    public static String politicasItemBodega = "SELECT pi.genpolCodigo FROM CbpgenrPoliticaitem pi WHERE pi.iNVITMcodigo=:item AND pi.genpolCodigo.genciaCodigo.genciaSecuencia=:secuenciaCompania AND pi.genpolCodigo IN (SELECT pb.genpolCodigo FROM CbpgenrPoliticabodega pb WHERE pb.genbodCodigo.genbodSecuencia=:bodega) AND pi.genpitEstado=1 AND pi.genpolCodigo.genpolEstado=1 ORDER BY pi.genpolCodigo.genpolCantidad ASC";
    
    public static String secuenciaModulo = "SELECT GENSCM_numsecuen FROM GENR_SECCOMPMOD WHERE GENMOD_codigo=? AND GENCIA_codigo=?";
    public static String numeroDocumentoModulo = "SELECT GENTDO_contador FROM GENR_DOCUMENTO WHERE GENCIA_codigo=? AND GENOFI_codigo=? AND GENMOD_codigo =? AND GENTDO_codigo =? ";
    public static String numeroDocumentoCompra = "SELECT right(substring(CXPSEC_secuencia, 1, 12) + 1, 12) FROM CXPP_SECUENCIACOMP WITH (TABLOCK HOLDLOCK) WHERE GENCIA_codigo=? AND GENOFI_codigo=? AND CXPSEC_etiqueta=?";
    
    // INVENTARIO AJUSTE TRANSACCION
    public static String registrosTablaTemporal = "SELECT * FROM ";
    public static String campoConsultaTablaTemporal ="WHERE ID_SECUENCIA=?";
    public static String selectDistinct="SELECT DISTINCT column FROM table";
    
    //CANTIDAD SUGERIDOS PENDIENTES
    public static String pendientesSugeridos= "SELECT COUNT(COMPDS_linea) FROM COMT_CABPEDSUGERIDO C INNER JOIN COMT_DETPEDSUGERIDO D ON (C.COMPDS_secuencia=D.COMPDS_secuencia) "
            + "WHERE C.COMPDS_estado='G' AND D.COMPDS_cantfuncionalsugeridas>0 AND D.COMPDS_cantfuncionalsugeridas>D.COMPDS_cantfuncionalrecibidas AND C.COMPDS_secuencia=?";
    
    //COMPRA
    public static String precioListaItem ="SELECT TOP 1 INVITM_prelista FROM INVR_CABLISTAPRECIO t1 INNER JOIN INVR_detLISTAPRECIO t2 ON t1.INVLPR_secuencia = t2.INVLPR_secuencia WHERE t1.INVLPR_tipoLista=? AND t2.INVITM_codigo=?";
    
    //INSERT
    public static String insertItem="INSERT INTO INVM_ITEM (GENCIA_codigo,INVITM_codigo,INVITM_nombre,INVITM_nombrecor,INVITM_porceutili,INVITM_porcutimin,INVITM_grupodscto,INVITM_peso,"
            + "INVITM_volumen,INVITM_unidembala,INVUNI_unidadgrup,INVUNI_unidadexis,INVITM_stock,INVITM_stkpending,INVITM_stkreserva,INVITM_stktransit,INVITM_stkminimo,INVITM_preciopvp,"
            + "INVITM_stkmaximo,INVITM_costoprom1,INVITM_precilista,INVITM_costoprom2,INVITM_costomerc1,INVITM_costomerc2,INVITM_fechaingre,INVITM_observacio,INVITM_esttipo,INVITM_estactivo,"
            + "INVTIM_tipoitem,INVITM_estretorna,INVITM_estensamb,INVITM_estproduce,INVITM_estafecinv,INVITM_estdispvta,INVITM_estvtafrac,INVITM_estmovimie,INVITM_estadolote,INVITM_estadserie,"
            + "INVITM_estadescar,INVITM_estactpubl,INVITM_estmovfrac,INVITM_unimedvol,INVITM_tipocombo,INVITM_codrefprov,INVITM_estcompe,INVITM_politidesc,INVITM_asumereten,INVITM_manedecimales,"
            + "INVITM_manedisplay,GRPPRO_codigo,INVITM_impplastico,INVITM_tipolistaprecio,INVITM_uniAsignada,INVITM_estfecha,INVITM_unidmedbase,INVITM_estamultunid,"
            + "INVIN1_codin1item,INVIN2_codin2item,INVIN3_codin3item,INVIN4_codin4item) "
            + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    
    public static String insertItemBodega="INSERT INTO INVM_ITEMXBODEGA(GENCIA_codigo,GENOFI_codigo,GENBOD_codigo,INVITM_codigo,INVIBO_posicion,INVIBO_stock,"
            + "INVIBO_stkpending,INVIBO_stkreserva,INVIBO_stktransit,INVIBO_stkminimo,INVIBO_stkmaximo,INVIBO_usucrea,INVIBO_feccrea) "
            + "SELECT i.GENCIA_codigo,b.GENOFI_codigo,b.GENBOD_codigo,i.INVITM_codigo,NULL,0,0,0,0,0,0,?,? "
            + "FROM INVM_ITEM i (NOLOCK),GENR_BODEGA b (NOLOCK) WHERE i.INVITM_codigo=? ";
    
    public static String insertItemImpuesto="INSERT INTO INVR_IMPTOXPROD (GENCIA_codigo,INVITM_codigo,GENIMP_codigo,INVIPO_estactivo,INVIPO_porcenta)"
            + "SELECT A.GENCIA_codigo,A.INVITM_codigo,B.GENIMP_codigo,'A',B.GENIMP_porcentaje FROM INVM_ITEM A (NOLOCK), GENR_IMPUESTO B (NOLOCK) "
            + "WHERE A.GENCIA_codigo=B.GENCIA_codigo AND B.GENIMP_tipoimpuesto='IVA' AND A.INVITM_codigo=? AND B.GENIMP_porcentaje>0 ";            
    
    public static String insertItemCosto="INSERT INTO INVR_DETLISTAPRECIO (INVLPR_secuencia,INVLPR_codigo,INVITM_codigo,INVITM_prelista,INVITM_preciopvp,INVLPR_pocentajedscto) "
            + "SELECT INVLPR_secuencia,INVLPR_codigo,?,?,0.00,0.00 FROM INVR_CABLISTAPRECIO WHERE GENCIA_codigo=? AND INVLPR_tipoLista='C'";
}
