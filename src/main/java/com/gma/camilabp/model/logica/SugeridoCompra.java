/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.model.logica;

import com.gma.camilabp.domain.CbpgenrBodega;
import com.gma.camilabp.domain.CbpgenrOficina;
import com.gma.camilabp.domain.CxpmProveedor;
import com.gma.camilabp.model.consultas.TipoFiltro;
import java.io.Serializable;

/**
 *
 * @author userdb6
 */
public class SugeridoCompra implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private CbpgenrOficina oficina;
    private String secuenciaOficina;
    private CbpgenrBodega bodega;
    private CxpmProveedor proveedor;
    private String secuenciaProveedor;
    private String secuenciaFiltro;
    private String datoFiltro;
    private Integer dias=0;
    private Integer numero;
    private TipoFiltro nivel;
    private String filtroNivel="N1";
    private String agrupadoPor = "N1";

    public SugeridoCompra() {
    }

    public SugeridoCompra(Integer numero) {
        this.numero = numero;
    }

    public CbpgenrOficina getOficina() {
        return oficina;
    }

    public void setOficina(CbpgenrOficina oficina) {
        this.oficina = oficina;
    }

    public String getSecuenciaOficina() {
        return secuenciaOficina;
    }

    public void setSecuenciaOficina(String secuenciaOficina) {
        this.secuenciaOficina = secuenciaOficina;
    }

    public CbpgenrBodega getBodega() {
        return bodega;
    }

    public void setBodega(CbpgenrBodega bodega) {
        this.bodega = bodega;
    }    

    public CxpmProveedor getProveedor() {
        return proveedor;
    }

    public void setProveedor(CxpmProveedor proveedor) {
        this.proveedor = proveedor;
    }

    public String getSecuenciaProveedor() {
        return secuenciaProveedor;
    }

    public void setSecuenciaProveedor(String secuenciaProveedor) {
        this.secuenciaProveedor = secuenciaProveedor;
    }

    public String getSecuenciaFiltro() {
        return secuenciaFiltro;
    }

    public void setSecuenciaFiltro(String secuenciaFiltro) {
        this.secuenciaFiltro = secuenciaFiltro;
    }

    public String getDatoFiltro() {
        return datoFiltro;
    }

    public void setDatoFiltro(String datoFiltro) {
        this.datoFiltro = datoFiltro;
    }

    public Integer getDias() {
        return dias;
    }

    public void setDias(Integer dias) {
        this.dias = dias;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public TipoFiltro getNivel() {
        return nivel;
    }

    public void setNivel(TipoFiltro nivel) {
        this.nivel = nivel;
    }

    public String getFiltroNivel() {
        return filtroNivel;
    }

    public void setFiltroNivel(String filtroNivel) {
        this.filtroNivel = filtroNivel;
    }

    public String getAgrupadoPor() {
        return agrupadoPor;
    }

    public void setAgrupadoPor(String agrupadoPor) {
        this.agrupadoPor = agrupadoPor;
    }
    
}
