/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.model.logica;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

/**
 *
 * @author userdb6
 */
public class TipoPago implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private Integer idSecuencia;
    private int tipo;
    private BigDecimal valor;
    private String numeroCuenta;
    private String numeroDocumento;
    private String entidad;
    private String fechaValidez;
    private Date fechaVencimiento= new Date();
    private String titular;

    public TipoPago() {
    }

    public TipoPago(Integer idSecuencia, int tipo, BigDecimal valor, String numeroCuenta, String numeroDocumento, String entidad, String fechaValidez, Date fechaVencimiento, String titular) {
        this.idSecuencia= idSecuencia;
        this.tipo = tipo;
        this.valor = valor;
        this.numeroCuenta = numeroCuenta;
        this.numeroDocumento = numeroDocumento;
        this.entidad = entidad;
        this.fechaValidez = fechaValidez;
        this.fechaVencimiento = fechaVencimiento;
        this.titular = titular!=null?titular.toUpperCase():null;
    }    

    public TipoPago(int tipo) {
        this.tipo = tipo;
    }

    public Integer getIdSecuencia() {
        return idSecuencia;
    }

    public void setIdSecuencia(Integer idSecuencia) {
        this.idSecuencia = idSecuencia;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public String getFechaValidez() {
        return fechaValidez;
    }

    public void setFechaValidez(String fechaValidez) {
        this.fechaValidez = fechaValidez;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public String getTitular() {
        return titular;
    }

    public void setTitular(String titular) {
        this.titular = titular;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.idSecuencia);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TipoPago other = (TipoPago) obj;
        if (this.tipo != other.tipo) {
            return false;
        }
        if (!Objects.equals(this.numeroCuenta, other.numeroCuenta)) {
            return false;
        }
        if (!Objects.equals(this.numeroDocumento, other.numeroDocumento)) {
            return false;
        }
        if (!Objects.equals(this.entidad, other.entidad)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "TipoPago{" + "idSecuencia=" + idSecuencia + ", tipo=" + tipo + ", valor=" + valor + ", numeroCuenta=" + numeroCuenta + ", numeroDocumento=" + numeroDocumento + ", entidad=" + entidad + '}';
    }
    
}
