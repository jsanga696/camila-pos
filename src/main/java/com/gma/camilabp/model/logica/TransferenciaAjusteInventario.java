/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.model.logica;

import com.gma.camilabp.domain.CbpgenrBodega;
import com.gma.camilabp.domain.CbpgenrOficina;
import com.gma.camilabp.domain.GenrDocumento;
import com.gma.camilabp.domain.InvtCabotros;
import java.io.Serializable;

/**
 *
 * @author Acer
 */
public class TransferenciaAjusteInventario implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private CbpgenrOficina oficina;
    private CbpgenrBodega bodegaOrigen;
    private CbpgenrBodega bodegaDestino;
    private String secuenciaOficina;
    private GenrDocumento tipoMovimiento;
    private InvtCabotros cabOtros;
    private Long secuenciaCabOtros;

    public TransferenciaAjusteInventario() {
    }

    public CbpgenrOficina getOficina() {
        return oficina;
    }

    public void setOficina(CbpgenrOficina oficina) {
        this.oficina = oficina;
    }

    public CbpgenrBodega getBodegaOrigen() {
        return bodegaOrigen;
    }

    public void setBodegaOrigen(CbpgenrBodega bodegaOrigen) {
        this.bodegaOrigen = bodegaOrigen;
    }

    public CbpgenrBodega getBodegaDestino() {
        return bodegaDestino;
    }

    public void setBodegaDestino(CbpgenrBodega bodegaDestino) {
        this.bodegaDestino = bodegaDestino;
    }

    public String getSecuenciaOficina() {
        return secuenciaOficina;
    }

    public void setSecuenciaOficina(String secuenciaOficina) {
        this.secuenciaOficina = secuenciaOficina;
    }

    public GenrDocumento getTipoMovimiento() {
        return tipoMovimiento;
    }

    public void setTipoMovimiento(GenrDocumento tipoMovimiento) {
        this.tipoMovimiento = tipoMovimiento;
    }

    public InvtCabotros getCabOtros() {
        return cabOtros;
    }

    public void setCabOtros(InvtCabotros cabOtros) {
        this.cabOtros = cabOtros;
    }

    public Long getSecuenciaCabOtros() {
        return secuenciaCabOtros;
    }

    public void setSecuenciaCabOtros(Long secuenciaCabOtros) {
        this.secuenciaCabOtros = secuenciaCabOtros;
    }

}
