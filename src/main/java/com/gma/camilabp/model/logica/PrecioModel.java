/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.model.logica;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import javax.persistence.Transient;

/**
 *
 * @author userdb5
 */
public class PrecioModel implements Serializable{
    
    private static final long serialVersionUID = 1L;
    
    private String item_cod;
    private String item_nombre;
    private BigDecimal valor;
    private BigDecimal imp;
    private BigDecimal valorConImp;
    
    public PrecioModel(){
    
    }
    
    public String getItem_cod() {
        return item_cod;
    }

    public void setItem_cod(String item_cod) {
        this.item_cod = item_cod;
    }

    public String getItem_nombre() {
        return item_nombre;
    }

    public void setItem_nombre(String item_nombre) {
        this.item_nombre = item_nombre;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public BigDecimal getImp() {
        return imp;
    }

    public void setImp(BigDecimal imp) {
        this.imp = imp;
    }

    /*public Double getValorConImp() {
        valorConImp = this.valor.add(this.valor.multiply(this.imp)).setScale(2, RoundingMode.HALF_UP).doubleValue();
        return valorConImp;
    }

    public void setValorConImp(Double valorConImp) {
        this.valorConImp = valorConImp;
    }*/

    public BigDecimal getValorConImp() {
        return valorConImp;
    }

    public void setValorConImp(BigDecimal valorConImp) {
        this.valorConImp = valorConImp;
    }
    
}
