/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.model.logica;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author userdb2
 */
public class CxprRetencionesTemp implements Serializable {

    private static final long serialVersionUI = 1l;

    private String tipo_retencion = "2";
    private String cXPRETnombre;
    private Date fecha_emision = new Date();
    private Date fecha_vigencia = new Date();
    private String cXPRETreferenciacontablecobro;
    private String CXCFPG_codigo;
    private BigDecimal cXPRETporcentajedist;
    private String cXPRETestado;
    private String sRIRETcodigo;
    private String num_autorizacion;
    private String usuario_reporte;
    private String num_factura;
    private String FACVTC_clienombre;
    private String FACVTC_clierucci;
    private String esContribuyenteEspecial;
    private Integer secuenciafactura;
    private String estado;
    private Integer turno;

    private List<CxprRetencionesDetalleTemp> detalle;

    public CxprRetencionesTemp() {
    }

    public String getTipo_retencion() {
        if (tipo_retencion == null) {
            tipo_retencion = "2";
        }
        return tipo_retencion;
    }

    public void setTipo_retencion(String tipo_retencion) {
        this.tipo_retencion = tipo_retencion;
    }

    public String getcXPRETnombre() {
        return cXPRETnombre;
    }

    public void setcXPRETnombre(String cXPRETnombre) {
        this.cXPRETnombre = cXPRETnombre;
    }

    public Date getFecha_emision() {
        return fecha_emision;
    }

    public void setFecha_emision(Date fecha_emision) {
        this.fecha_emision = fecha_emision;
    }

    public Date getFecha_vigencia() {
        return fecha_vigencia;
    }

    public void setFecha_vigencia(Date fecha_vigencia) {
        this.fecha_vigencia = fecha_vigencia;
    }

    public String getcXPRETreferenciacontablecobro() {
        return cXPRETreferenciacontablecobro;
    }

    public void setcXPRETreferenciacontablecobro(String cXPRETreferenciacontablecobro) {
        this.cXPRETreferenciacontablecobro = cXPRETreferenciacontablecobro;
    }

    /**
     * Forma de Pago
     *
     * @return
     */
    public String getCXCFPG_codigo() {
        return CXCFPG_codigo;
    }

    public void setCXCFPG_codigo(String CXCFPG_codigo) {
        this.CXCFPG_codigo = CXCFPG_codigo;
    }

    public BigDecimal getcXPRETporcentajedist() {
        return cXPRETporcentajedist;
    }

    public void setcXPRETporcentajedist(BigDecimal cXPRETporcentajedist) {
        this.cXPRETporcentajedist = cXPRETporcentajedist;
    }

    public String getcXPRETestado() {
        return cXPRETestado;
    }

    public void setcXPRETestado(String cXPRETestado) {
        this.cXPRETestado = cXPRETestado;
    }

    public String getSRIRETcodigo() {
        return sRIRETcodigo;
    }

    public void setSRIRETcodigo(String sRIRETcodigo) {
        this.sRIRETcodigo = sRIRETcodigo;
    }

    public String getNum_autorizacion() {
        return num_autorizacion;
    }

    public void setNum_autorizacion(String num_autorizacion) {
        this.num_autorizacion = num_autorizacion;
    }

    public String getUsuario_reporte() {
        return usuario_reporte;
    }

    public void setUsuario_reporte(String usuario_reporte) {
        this.usuario_reporte = usuario_reporte;
    }

    public String getNum_factura() {
        return num_factura;
    }

    public void setNum_factura(String num_factura) {
        this.num_factura = num_factura;
    }

    public String getFACVTC_clienombre() {
        return FACVTC_clienombre;
    }

    public void setFACVTC_clienombre(String FACVTC_clienombre) {
        this.FACVTC_clienombre = FACVTC_clienombre;
    }

    public String getFACVTC_clierucci() {
        return FACVTC_clierucci;
    }

    public void setFACVTC_clierucci(String FACVTC_clierucci) {
        this.FACVTC_clierucci = FACVTC_clierucci;
    }

    public String getEsContribuyenteEspecial() {
        return esContribuyenteEspecial;
    }

    public void setEsContribuyenteEspecial(String esContribuyenteEspecial) {
        this.esContribuyenteEspecial = esContribuyenteEspecial;
    }

    public List<CxprRetencionesDetalleTemp> getDetalle() {
        return detalle;
    }

    public void setDetalle(List<CxprRetencionesDetalleTemp> detalle) {
        this.detalle = detalle;
    }

    public Integer getSecuenciafactura() {
        return secuenciafactura;
    }

    public void setSecuenciafactura(Integer secuenciafactura) {
        this.secuenciafactura = secuenciafactura;
    }

    public List<CxprRetencionesDetalleTemp> getDetalleFuente() {
        if (detalle != null) {
            List<CxprRetencionesDetalleTemp> temp = new ArrayList<>();
            for (CxprRetencionesDetalleTemp fuen : detalle) {
                if (fuen.getRetencionTipo().contains("F")) {
                    temp.add(fuen);
                }
            }
            return temp;
        }
        return null;
    }

    public List<CxprRetencionesDetalleTemp> getDetalleIva() {
        if (detalle != null) {
            List<CxprRetencionesDetalleTemp> temp = new ArrayList<>();
            for (CxprRetencionesDetalleTemp fuen : detalle) {
                if (fuen.getRetencionTipo().contains("I")) {
                    temp.add(fuen);
                }
            }
            return temp;
        }
        return null;
    }

    public BigDecimal getTotal() {
        if (detalle != null) {
            BigDecimal temp = BigDecimal.ZERO;
            for (CxprRetencionesDetalleTemp fuen : detalle) {
                temp = temp.add(fuen.getValorRetenido());
            }
            return temp;
        }
        return BigDecimal.ZERO;
    }

    @Override
    public String toString() {
        return "CxprRetencionesTemp{" + "cXPRETtipo=" + tipo_retencion + ", cXPRETnombre=" + cXPRETnombre + ", cXPRETFechaini=" + fecha_emision + ", cXPRETFechafin=" + fecha_vigencia + ", cXPRETreferenciacontablecobro=" + cXPRETreferenciacontablecobro + ", cXPRETreferenciacontablepago=" + CXCFPG_codigo + ", cXPRETporcentajedist=" + cXPRETporcentajedist + ", cXPRETestado=" + cXPRETestado + ", sRIRETcodigo=" + sRIRETcodigo + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.usuario_reporte);
        hash = 97 * hash + Objects.hashCode(this.num_factura);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CxprRetencionesTemp other = (CxprRetencionesTemp) obj;
        if (!Objects.equals(this.usuario_reporte, other.usuario_reporte)) {
            return false;
        }
        if (!Objects.equals(this.num_factura, other.num_factura)) {
            return false;
        }
        return true;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Integer getTurno() {
        return turno;
    }

    public void setTurno(Integer turno) {
        this.turno = turno;
    }

}
