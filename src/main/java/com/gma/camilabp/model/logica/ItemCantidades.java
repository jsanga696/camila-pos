/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.model.logica;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author Acer
 */
public class ItemCantidades implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private BigDecimal unidad;
    private BigDecimal factor;
    private BigDecimal caja;
    private BigDecimal funcional;

    public ItemCantidades() {
    }

    public BigDecimal getUnidad() {
        return unidad;
    }

    public void setUnidad(BigDecimal unidad) {
        this.unidad = unidad;
    }

    public BigDecimal getFactor() {
        return factor;
    }

    public void setFactor(BigDecimal factor) {
        this.factor = factor;
    }

    public BigDecimal getCaja() {
        return caja;
    }

    public void setCaja(BigDecimal caja) {
        this.caja = caja;
    }

    public BigDecimal getFuncional() {
        return funcional;
    }

    public void setFuncional(BigDecimal funcional) {
        this.funcional = funcional;
    }
    
}
