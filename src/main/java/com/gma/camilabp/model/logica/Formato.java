/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.model.logica;

import java.io.Serializable;

/**
 *
 * @author Acer
 */
public class Formato implements Serializable {
    private static final long serialVersionUID = 1L;

    private String etiquetaInicial="";
    private String etiquetaFinal="";
    private Boolean aplicaEtiInicial=false;
    private Boolean aplicaEtiFinal=false;
    private char caracter='.';
    private int longitud=0;

    public Formato() {
    }

    public Formato(String etiquetaInicial, String etiquetaFinal, Boolean aplicaEtiInicial, Boolean aplicaEtiFinal, char caracter, int longitud) {
        this.etiquetaInicial = etiquetaInicial;
        this.etiquetaFinal = etiquetaFinal;
        this.aplicaEtiInicial = aplicaEtiInicial;
        this.aplicaEtiFinal = aplicaEtiFinal;
        this.caracter = caracter;
        this.longitud = longitud;
    }
    
    public void getObtenrFormato(String cadena){
        String[] datos;
        if(cadena!=null){
            datos = cadena.split(";");
            this.etiquetaInicial= datos[0];
            this.aplicaEtiInicial= "S".equalsIgnoreCase(datos[1]);
            this.etiquetaFinal= datos[2];
            this.aplicaEtiFinal= "S".equalsIgnoreCase(datos[3]);
            this.caracter=datos[4].charAt(0);
            this.longitud = Integer.valueOf(datos[5]);
        }
    }
    
    public String getEtiquetaInicial() {
        return etiquetaInicial;
    }

    public void setEtiquetaInicial(String etiquetaInicial) {
        this.etiquetaInicial = etiquetaInicial;
    }

    public String getEtiquetaFinal() {
        return etiquetaFinal;
    }

    public void setEtiquetaFinal(String etiquetaFinal) {
        this.etiquetaFinal = etiquetaFinal;
    }

    public Boolean getAplicaEtiInicial() {
        return aplicaEtiInicial;
    }

    public void setAplicaEtiInicial(Boolean aplicaEtiInicial) {
        this.aplicaEtiInicial = aplicaEtiInicial;
    }

    public Boolean getAplicaEtiFinal() {
        return aplicaEtiFinal;
    }

    public void setAplicaEtiFinal(Boolean aplicaEtiFinal) {
        this.aplicaEtiFinal = aplicaEtiFinal;
    }

    public char getCaracter() {
        return caracter;
    }

    public void setCaracter(char caracter) {
        this.caracter = caracter;
    }

    public int getLongitud() {
        return longitud;
    }

    public void setLongitud(int longitud) {
        this.longitud = longitud;
    }
    
    
}
