/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.model.logica;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;

/**
 *
 * @author userdb2
 */
public class CxprRetencionesDetalleTemp implements Serializable {

    private String usuario_reporte;
    private String num_factura;
    private String codigo;
    private Integer codigoImpuesto;
    private String retencionTipo;
    private String nombreRetencion;
    private BigDecimal porcentaje;
    private BigDecimal baseImponible;
    private BigDecimal valorRetenido;

    public CxprRetencionesDetalleTemp() {
    }

    public CxprRetencionesDetalleTemp(String usuario_reporte, String num_factura, String codigo, Integer codigoImpuesto, String retencionTipo, String nombreRetencion, BigDecimal porcentaje, BigDecimal baseImponible, BigDecimal valorRetenido) {
        this.usuario_reporte = usuario_reporte;
        this.num_factura = num_factura;
        this.codigo = codigo;
        this.codigoImpuesto = codigoImpuesto;
        this.retencionTipo = retencionTipo;
        this.nombreRetencion = nombreRetencion;
        this.porcentaje = porcentaje;
        this.baseImponible = baseImponible;
        this.valorRetenido = valorRetenido;
    }

    public String getUsuario_reporte() {
        return usuario_reporte;
    }

    public void setUsuario_reporte(String usuario_reporte) {
        this.usuario_reporte = usuario_reporte;
    }

    public String getNum_factura() {
        return num_factura;
    }

    public void setNum_factura(String num_factura) {
        this.num_factura = num_factura;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Integer getCodigoImpuesto() {
        return codigoImpuesto;
    }

    public void setCodigoImpuesto(Integer codigoImpuesto) {
        this.codigoImpuesto = codigoImpuesto;
    }

    public String getRetencionTipo() {
        return retencionTipo;
    }

    public void setRetencionTipo(String retencionTipo) {
        this.retencionTipo = retencionTipo;
    }

    public String getNombreRetencion() {
        return nombreRetencion;
    }

    public void setNombreRetencion(String nombreRetencion) {
        this.nombreRetencion = nombreRetencion;
    }

    public BigDecimal getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(BigDecimal porcentaje) {
        this.porcentaje = porcentaje;
    }

    public BigDecimal getBaseImponible() {
        return baseImponible;
    }

    public void setBaseImponible(BigDecimal baseImponible) {
        this.baseImponible = baseImponible;
    }

    public BigDecimal getValorRetenido() {
        if (valorRetenido == null || valorRetenido.compareTo(BigDecimal.ZERO) == 0) {
            this.calcularRetencion();
        }
        return valorRetenido;
    }

    public void setValorRetenido(BigDecimal valorRetenido) {
        this.valorRetenido = valorRetenido;
    }

    public void calcularRetencion() {
        if (porcentaje == null) {
            porcentaje = BigDecimal.ZERO;
        }
        if (baseImponible == null) {
            baseImponible = BigDecimal.ZERO;
        }
        BigDecimal tempPorcentaje = porcentaje.divide(BigDecimal.valueOf(100.00), RoundingMode.HALF_UP);
        this.valorRetenido = tempPorcentaje.multiply(baseImponible);
    }

    @Override
    public String toString() {
        return "CxprRetencionesDetalleTemp{" + "usuario_reporte=" + usuario_reporte + ", num_factura=" + num_factura + ", codigo=" + codigo + ", codigoImpuesto=" + codigoImpuesto + ", retencionTipo=" + retencionTipo + ", nombreRetencion=" + nombreRetencion + ", porcentaje=" + porcentaje + ", baseImponible=" + baseImponible + ", valorRetenido=" + valorRetenido + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.usuario_reporte);
        hash = 29 * hash + Objects.hashCode(this.num_factura);
        hash = 29 * hash + Objects.hashCode(this.codigo);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CxprRetencionesDetalleTemp other = (CxprRetencionesDetalleTemp) obj;
        if (!Objects.equals(this.usuario_reporte, other.usuario_reporte)) {
            return false;
        }
        if (!Objects.equals(this.num_factura, other.num_factura)) {
            return false;
        }
        if (!Objects.equals(this.codigo, other.codigo)) {
            return false;
        }
        return true;
    }

}
