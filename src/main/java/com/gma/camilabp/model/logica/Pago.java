/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.model.logica;

import com.gma.camilabp.domain.CxctDocumento;
import com.gma.camilabp.model.consultas.TempCbpCobUsuarioOpcion;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author userdb6
 */
public class Pago implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private Integer idSecuencia;
    private BigDecimal valorCobro= BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP);
    private BigDecimal valorEfectivoAnterior= BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP);
    private BigDecimal valorEfectivo= BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP);
    private BigDecimal valorCheque= BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP);
    private BigDecimal valorTarjeta= BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP);
    private BigDecimal valorDocumento= BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP);
    private BigDecimal valorCredito= BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP);
    private BigDecimal valorTotal= BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP);
    private BigDecimal valorDiferencia= BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP);
    private BigDecimal valorCambioEfectivo= BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP);
    private BigDecimal valorDisponibleCliente= BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP);
    private TipoPago pagoCheque= new TipoPago(2);
    private TipoPago pagoTarjeta= new TipoPago(3);
    //private TipoPago pagoRetencion= new TipoPago();
    private List<TipoPago> pagosCheque = new ArrayList<>();
    private List<TipoPago> pagosTarjeta = new ArrayList<>();
    //private List<TipoPago> pagosRetencion = new ArrayList<>();
    private List<Long> secuencias= new ArrayList<>();
    private List<CxctDocumento> pagosDocumento= new ArrayList<>();
    private Boolean habilitarCredito=Boolean.FALSE;
    private Boolean credito=Boolean.FALSE;
    private Boolean retencion=Boolean.FALSE;
    private Integer idSecuenciaRet;
    private Boolean excedente=Boolean.FALSE;

    public Pago() {
    }

    public Pago(BigDecimal valorCobro) {
        this.valorCobro = valorCobro;        
    }
    
    public void calcularValores(){
        valorCheque= BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP);
        valorTarjeta= BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP);
        valorDocumento= BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP);
        valorCredito= BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP);
        pagosCheque.forEach((pch) -> {this.valorCheque=this.valorCheque.add(pch.getValor());});
        pagosTarjeta.forEach((ptr) -> {this.valorTarjeta=this.valorTarjeta.add(ptr.getValor());});
        pagosDocumento.forEach((d) -> {this.valorDocumento=this.valorDocumento.add(d.getCXCDEUsaldomov().abs().setScale(2, RoundingMode.HALF_UP));});
        //pagosRetencion.forEach((pre) -> {this.valorDocumento=this.valorDocumento.add(pre.getValor());});
    }
    
    public void calcularValoresTotales(){
        this.valorTotal=this.valorEfectivo.add(this.valorCheque).add(this.valorTarjeta).add(this.valorDocumento).add(this.valorCredito);
        this.valorDiferencia=this.valorTotal.subtract(this.valorCobro);
        if(this.valorEfectivo.compareTo(this.valorCobro)>0)
            this.valorCambioEfectivo=this.valorEfectivo.subtract(this.valorCobro);
        else
            this.valorCambioEfectivo= BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP);
        if(this.habilitarCredito && this.credito){
            if(this.valorDiferencia.compareTo(BigDecimal.ZERO)<0)
                this.valorCredito=this.valorDisponibleCliente.compareTo(this.valorDiferencia.abs())>=0?this.valorDiferencia.abs():this.valorDisponibleCliente.setScale(2, RoundingMode.HALF_UP);
            else
                this.valorCredito=BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP);
            this.valorTotal=this.valorEfectivo.add(this.valorCheque).add(this.valorTarjeta).add(this.valorDocumento).add(this.valorCredito);
            this.valorDiferencia=this.valorTotal.subtract(this.valorCobro);
            if(this.valorEfectivo.compareTo(this.valorCobro)>0)
                this.valorCambioEfectivo=this.valorEfectivo.subtract(this.valorCobro);
            else
                this.valorCambioEfectivo= BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP);
        }else{
            valorCredito= BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP);
        }
    }
    
    public void agregarTipoPago(int tp){
        try{
            switch (tp){
                case 2: //CHEQUE
                    if(this.pagoCheque!=null && this.pagoCheque.getValor()!=null && this.pagoCheque.getValor().compareTo(BigDecimal.ZERO)>0)
                        this.pagosCheque.add(new TipoPago(this.pagoCheque.getIdSecuencia(), tp, this.pagoCheque.getValor().setScale(2, RoundingMode.HALF_UP), this.pagoCheque.getNumeroCuenta(), this.pagoCheque.getNumeroDocumento(), this.pagoCheque.getEntidad(), this.pagoCheque.getFechaValidez(), this.pagoCheque.getFechaVencimiento(), null));
                    break;
                case 3://TARJETA
                    if(this.pagoTarjeta!=null && this.pagoTarjeta.getValor()!=null && this.pagoTarjeta.getValor().compareTo(BigDecimal.ZERO)>0)
                        this.pagosTarjeta.add(new TipoPago(this.pagoTarjeta.getIdSecuencia(), tp, this.pagoTarjeta.getValor().setScale(2, RoundingMode.HALF_UP), this.pagoTarjeta.getNumeroCuenta(), this.pagoTarjeta.getNumeroDocumento(), this.pagoTarjeta.getEntidad(), this.pagoTarjeta.getFechaValidez(), this.pagoTarjeta.getFechaVencimiento(), this.pagoTarjeta.getTitular()));
                    break;
                case 4://RETENCION
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            Logger.getLogger(Pago.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void eliminarTipoPago(int tp, TipoPago p){
        try{
            switch (tp){
                case 2: //CHEQUE
                    this.pagosCheque.remove(p);
                    break;
                case 3://TARJETA
                    this.pagosTarjeta.remove(p);
                    break;
                case 4://RETENCION
                    //this.pagosRetencion.remove(p);
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            Logger.getLogger(Pago.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void eliminarRetencion(){
        for (CxctDocumento cxctDocumento : this.pagosDocumento) {
            if(cxctDocumento.getCXCDEUnumdocumen()==null || cxctDocumento.getCXCDEUnumdocumen().trim().equalsIgnoreCase("")){
                this.pagosDocumento.remove(cxctDocumento);
                break;
            }
        }
    }
    
    public void tablaClase(List<TempCbpCobUsuarioOpcion> lc){
        try{
            for (TempCbpCobUsuarioOpcion i : lc) {
                switch(i.getTipoPago()){
                    case 1:
                        this.setIdSecuencia(i.getIdSecuencia());
                        this.setValorEfectivo(i.getCxcdeuValorPago().setScale(2, RoundingMode.HALF_UP));
                        this.setValorEfectivoAnterior(i.getCxcdeuValorPago().setScale(2, RoundingMode.HALF_UP));
                        break;
                    case 2:
                        this.pagosCheque.add(new TipoPago(i.getIdSecuencia(),i.getTipoPago(), i.getCxcdeuValorPago().setScale(2, RoundingMode.HALF_UP), i.getCxcdeuNumcuenta(), i.getCxcdeuNumcheque(), i.getCxcdeuCodent(),null, i.getCxcdeuFechavenci(),null));
                        break;
                    case 3:
                        this.pagosTarjeta.add(new TipoPago(i.getIdSecuencia(),i.getTipoPago(), i.getCxcdeuValorPago().setScale(2, RoundingMode.HALF_UP), i.getCxcdeuNumcuenta(), i.getCxcdeuNumcheque(), i.getCxcdeuCodent(), i.getFaccobFechavalidez(), null, i.getTitular()));
                        break;
                    case 4:
                        this.secuencias.add(new Long(i.getCxcdeuNumcuenta()));
                        break;
                    case 5:
                        this.setValorCredito(i.getCxcdeuValorPago());
                        break;
                    default:
                        break;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(Pago.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public Integer getIdSecuencia() {
        return idSecuencia;
    }

    public void setIdSecuencia(Integer idSecuencia) {
        this.idSecuencia = idSecuencia;
    }

    public BigDecimal getValorCobro() {
        return valorCobro;
    }

    public void setValorCobro(BigDecimal valorCobro) {
        this.valorCobro = valorCobro;
    }

    public BigDecimal getValorEfectivoAnterior() {
        return valorEfectivoAnterior;
    }

    public void setValorEfectivoAnterior(BigDecimal valorEfectivoAnterior) {
        this.valorEfectivoAnterior = valorEfectivoAnterior;
    }

    public BigDecimal getValorEfectivo() {
        return valorEfectivo;
    }

    public void setValorEfectivo(BigDecimal valorEfectivo) {
        this.valorEfectivo = valorEfectivo;
    }

    public BigDecimal getValorCheque() {
        return valorCheque;
    }

    public void setValorCheque(BigDecimal valorCheque) {
        this.valorCheque = valorCheque;
    }

    public BigDecimal getValorTarjeta() {
        return valorTarjeta;
    }

    public void setValorTarjeta(BigDecimal valorTarjeta) {
        this.valorTarjeta = valorTarjeta;
    }

    public BigDecimal getValorDocumento() {
        return valorDocumento;
    }

    public void setValorDocumento(BigDecimal valorDocumento) {
        this.valorDocumento = valorDocumento;
    }

    public BigDecimal getValorCredito() {
        return valorCredito;
    }

    public void setValorCredito(BigDecimal valorCredito) {
        this.valorCredito = valorCredito;
    }

    public BigDecimal getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(BigDecimal valorTotal) {
        this.valorTotal = valorTotal;
    }

    public BigDecimal getValorDiferencia() {
        return valorDiferencia;
    }

    public void setValorDiferencia(BigDecimal valorDiferencia) {
        this.valorDiferencia = valorDiferencia;
    }

    public BigDecimal getValorCambioEfectivo() {
        return valorCambioEfectivo;
    }

    public void setValorCambioEfectivo(BigDecimal valorCambioEfectivo) {
        this.valorCambioEfectivo = valorCambioEfectivo;
    }

    public List<CxctDocumento> getPagosDocumento() {
        return pagosDocumento;
    }

    public void setPagosDocumento(List<CxctDocumento> pagosDocumento) {
        this.pagosDocumento = pagosDocumento;
    }

    public List<TipoPago> getPagosCheque() {
        return pagosCheque;
    }

    public void setPagosCheque(List<TipoPago> pagosCheque) {
        this.pagosCheque = pagosCheque;
    }

    public List<TipoPago> getPagosTarjeta() {
        return pagosTarjeta;
    }

    public void setPagosTarjeta(List<TipoPago> pagosTarjeta) {
        this.pagosTarjeta = pagosTarjeta;
    }

    public TipoPago getPagoCheque() {
        return pagoCheque;
    }

    public void setPagoCheque(TipoPago pagoCheque) {
        this.pagoCheque = pagoCheque;
    }

    public TipoPago getPagoTarjeta() {
        return pagoTarjeta;
    }

    public void setPagoTarjeta(TipoPago pagoTarjeta) {
        this.pagoTarjeta = pagoTarjeta;
    }

    public Boolean getHabilitarCredito() {
        return habilitarCredito;
    }

    public void setHabilitarCredito(Boolean habilitarCredito) {
        this.habilitarCredito = habilitarCredito;
    }

    public Boolean getCredito() {
        return credito;
    }

    public void setCredito(Boolean credito) {
        this.credito = credito;
    }

    public BigDecimal getValorDisponibleCliente() {
        return valorDisponibleCliente;
    }

    public void setValorDisponibleCliente(BigDecimal valorDisponibleCliente) {
        this.valorDisponibleCliente = valorDisponibleCliente;
    }

    public List<Long> getSecuencias() {
        return secuencias;
    }

    public void setSecuencias(List<Long> secuencias) {
        this.secuencias = secuencias;
    }

    public Boolean getRetencion() {
        return retencion;
    }

    public void setRetencion(Boolean retencion) {
        this.retencion = retencion;
    }

    public Integer getIdSecuenciaRet() {
        return idSecuenciaRet;
    }

    public void setIdSecuenciaRet(Integer idSecuenciaRet) {
        this.idSecuenciaRet = idSecuenciaRet;
    }

    public Boolean getExcedente() {
        return excedente;
    }

    public void setExcedente(Boolean excedente) {
        this.excedente = excedente;
    }

}
