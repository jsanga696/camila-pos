/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.model.logica;

import com.gma.camilabp.domain.CbpsegmPerfilrestricion;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author userdb6
 */
public class Render implements Serializable{
    
    private Boolean standby=Boolean.TRUE;

    public Render() {
    }
    
    public void registrarRestricciones(List<CbpsegmPerfilrestricion> perfilrestricions) {
        try{
            Field fiel[] = this.getClass().getDeclaredFields();
            for (Field f : fiel) {
                for (CbpsegmPerfilrestricion pr : perfilrestricions) {
                    if(f.getName().equalsIgnoreCase(pr.getSegprrSecuencia())){
                        f.setAccessible(true);
                        f.set(this, Boolean.FALSE);
                        break;
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(Render.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public Boolean getStandby() {
        return standby;
    }

    public void setStandby(Boolean standby) {
        this.standby = standby;
    }
        
}
