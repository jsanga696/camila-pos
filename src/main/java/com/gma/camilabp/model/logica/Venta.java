/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.model.logica;

import com.gma.camilabp.domain.CxcmCliente;
import com.gma.camilabp.domain.GenpTalonarios;
import java.io.Serializable;

/**
 *
 * @author Acer
 */
public class Venta implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private CxcmCliente cliente;
    private GenpTalonarios talonario;
    private String vendedor;
    private String numDocumento;
    private Boolean consumidorFinal;

    public Venta() {
        this.cliente=new CxcmCliente();
        this.consumidorFinal=Boolean.FALSE;
    }

    public CxcmCliente getCliente() {
        return cliente;
    }

    public void setCliente(CxcmCliente cliente) {
        this.cliente = cliente;
    }

    public GenpTalonarios getTalonario() {
        return talonario;
    }

    public void setTalonario(GenpTalonarios talonario) {
        this.talonario = talonario;
    }

    public String getVendedor() {
        return vendedor;
    }

    public void setVendedor(String vendedor) {
        this.vendedor = vendedor;
    }

    public String getNumDocumento() {
        return numDocumento;
    }

    public void setNumDocumento(String numDocumento) {
        this.numDocumento = numDocumento;
    }

    public Boolean getConsumidorFinal() {
        return consumidorFinal;
    }

    public void setConsumidorFinal(Boolean consumidorFinal) {
        this.consumidorFinal = consumidorFinal;
    }
    
}
