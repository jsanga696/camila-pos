/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.model.logica;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author userdb5
 */
public class ProductividadModel implements Serializable{
    
    private static final long serialVersionUID = 1L;
    
    private String clase_cliente;
    private String cliente;
    private BigDecimal suma;
    
    public ProductividadModel(){
    
    }

    public String getClase_cliente() {
        return clase_cliente;
    }

    public void setClase_cliente(String clase_cliente) {
        this.clase_cliente = clase_cliente;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public BigDecimal getSuma() {
        return suma;
    }

    public void setSuma(BigDecimal suma) {
        this.suma = suma;
    }

}
