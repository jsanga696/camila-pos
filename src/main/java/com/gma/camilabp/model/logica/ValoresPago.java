/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.model.logica;

import com.gma.camilabp.model.consultas.TempCbpPcUsuarioOpcion;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 *
 * @author userdb6
 */
public class ValoresPago implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private BigDecimal subtotal= BigDecimal.ZERO.setScale(4, RoundingMode.HALF_UP);
    private BigDecimal subtotalImponible= BigDecimal.ZERO.setScale(4, RoundingMode.HALF_UP);
    private BigDecimal subtotalExenta= BigDecimal.ZERO.setScale(4, RoundingMode.HALF_UP);
    private BigDecimal descuento= BigDecimal.ZERO.setScale(4, RoundingMode.HALF_UP);
    private BigDecimal descuentoImponible= BigDecimal.ZERO.setScale(4, RoundingMode.HALF_UP);
    private BigDecimal descuentoExenta= BigDecimal.ZERO.setScale(4, RoundingMode.HALF_UP);
    private BigDecimal impuesto= BigDecimal.ZERO.setScale(4, RoundingMode.HALF_UP);
    private BigDecimal total= BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP);

    public ValoresPago() {
    }
    /**
     * 
     * @param item Objeto
     * @param tipo 1:Agegar 2:Eliminar
     */
    public void actualizarValores(TempCbpPcUsuarioOpcion item, int tipo){
        if(tipo==1){
            if("S".equalsIgnoreCase(item.getImpuesto())){
                this.setSubtotalImponible(this.getSubtotalImponible().add(item.getFacdpdSubtotal()));
                this.setDescuentoImponible(this.getDescuentoImponible().add(item.getFacdpdDescuento()));
                this.setImpuesto(this.getImpuesto().add(item.getFacdpdImpuesto()));
            }else{
                this.setSubtotalExenta(this.getSubtotalExenta().add(item.getFacdpdSubtotal()));
                this.setDescuentoExenta(this.getDescuentoExenta().add(item.getFacdpdDescuento()));
            }
        }else{
            if("S".equalsIgnoreCase(item.getImpuesto())){
                this.setSubtotalImponible(this.getSubtotalImponible().subtract(item.getFacdpdSubtotal()));
                this.setDescuentoImponible(this.getDescuentoImponible().subtract(item.getFacdpdDescuento()));
                this.setImpuesto(this.getImpuesto().subtract(item.getFacdpdImpuesto()));
            }else{
                this.setSubtotalExenta(this.getSubtotalExenta().subtract(item.getFacdpdSubtotal()));
                this.setDescuentoExenta(this.getDescuentoExenta().subtract(item.getFacdpdDescuento()));
            }
        }
    }
    
    public void actualizarValoresTotales(){
        this.calcularParcial();
        this.calcularTotal();
    }
    
    public void calcularParcial(){
        this.subtotal= this.subtotalExenta.add(this.subtotalImponible);
        this.descuento= this.descuentoExenta.add(this.descuentoImponible);
    }
    
    public void calcularTotal(){
        this.total= this.subtotal.add(this.impuesto).subtract(this.descuento).setScale(2, RoundingMode.HALF_UP);
    }

    public BigDecimal getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(BigDecimal subtotal) {
        this.subtotal = subtotal;
    }

    public BigDecimal getSubtotalImponible() {
        return subtotalImponible;
    }

    public void setSubtotalImponible(BigDecimal subtotalImponible) {
        this.subtotalImponible = subtotalImponible;
    }

    public BigDecimal getSubtotalExenta() {
        return subtotalExenta;
    }

    public void setSubtotalExenta(BigDecimal subtotalExenta) {
        this.subtotalExenta = subtotalExenta;
    }

    public BigDecimal getDescuento() {
        return descuento;
    }

    public void setDescuento(BigDecimal descuento) {
        this.descuento = descuento;
    }

    public BigDecimal getDescuentoImponible() {
        return descuentoImponible;
    }

    public void setDescuentoImponible(BigDecimal descuentoImponible) {
        this.descuentoImponible = descuentoImponible;
    }

    public BigDecimal getDescuentoExenta() {
        return descuentoExenta;
    }

    public void setDescuentoExenta(BigDecimal descuentoExenta) {
        this.descuentoExenta = descuentoExenta;
    }

    public BigDecimal getImpuesto() {
        return impuesto;
    }

    public void setImpuesto(BigDecimal impuesto) {
        this.impuesto = impuesto;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }
    
}
