/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.model.consultas;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author userdb6
 */
public class InveRepdiasInventario implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private String GENCIA_codigo;
    private String GENCIA_nombre;
    private String GENOFI_codigo;
    private String GENOFI_nombre;
    private String INVITM_codigo;
    private String INVITM_codalterno;
    private String INVITM_nombre;
    private String INVIN1_codin1item;
    private String INVIN2_codin2item;
    private String INVIN3_codin3item;
    private String INVIN4_codin4item;
    private String INVIN1_descripcio;
    private String INVIN2_descripcio;
    private String INVIN3_descripcio;
    private String INVIN4_descripcio;
    private BigDecimal INVITM_peso;
    private BigDecimal INVITM_volumen;
    private Date INVITM_fecultvent;
    private Date INVITM_fecultcomp;
    private Date INVITM_fechaingre;
    private Date ESTIDE_Fecha;
    private Integer INVITM_unidembala;
    private String INVITM_esttipo;
    private String INVITM_estactivo;
    private String INVITM_estensamb;
    private String INVITM_estafecinv;
    private String INVITM_estdispvta;
    private String INVITM_estvtafrac;
    private Integer INVITM_stockreal;
    private Integer INVITM_cajas;
    private Integer INVITM_unidades;
    private BigDecimal INVITM_costoprom1;
    private BigDecimal INVITM_costoprom1IVA;
    private BigDecimal INVITM_impuestoIVA;
    private BigDecimal INVITM_preciopvp;
    private BigDecimal INVITM_precilista;
    private BigDecimal INVITM_precilistaIVA;
    private Integer INVITM_cajas1;
    private Integer INVITM_unidad1;
    private Integer INVITM_canfunci1;
    private Integer INVITM_cajas2;
    private Integer INVITM_unidad2;
    private Integer INVITM_canfunci2;
    private Integer INVITM_cajas3;
    private Integer INVITM_unidad3;
    private Integer INVITM_canfunci3;
    private Integer INVITM_cajas4;
    private Integer INVITM_unidad4;
    private Integer INVITM_canfunci4;
    private Integer INVITM_cajas5;
    private Integer INVITM_unidad5;
    private Integer INVITM_canfunci5;
    private Integer INVITM_cajas6;
    private Integer INVITM_unidad6;
    private Integer INVITM_canfunci6;
    private BigDecimal INVITM_ventatotalQ;
    private BigDecimal INVITM_ventapromedioQ;
    private BigDecimal INVITM_totalinventario;
    private BigDecimal INVITM_diasinventario;
    private Character INVITM_estadoIVA;
    private String INVITM_codigoZAP;
    private Integer INVITM_diasventa;
    private Integer INVITM_diascreado;
    private String SEGUSU_codigo;
    private String SEGOPC_codigo;
    private String SEGMAQ_codigo;
    private String INVITM_manedecimales;
    private BigDecimal INVITM_totalbasecero;
    private BigDecimal INVITM_totalImponible;
    private BigDecimal INVITM_ImpuestoGrabado;
    private BigDecimal INVITM_inventarioAdd;
    private BigDecimal divsug;
    private Integer INVITM_CajasReales;
    private Integer INVITM_CajasSugeridad;
    private BigDecimal INVITM_ventaTotalSugerida;
    private Integer INVITM_UniReales;
    private Integer INVITM_CantFunciReal;
    private BigDecimal INVITM_costoventanetaQ;
    private BigDecimal COMPDS_stockproveedor;
    private String INVIN1_codin1item_1;
    private String INVIN2_codin2item_1;
    

    public InveRepdiasInventario() {
    }

    public String getGENCIA_codigo() {
        return GENCIA_codigo;
    }

    public void setGENCIA_codigo(String GENCIA_codigo) {
        this.GENCIA_codigo = GENCIA_codigo;
    }

    public String getGENCIA_nombre() {
        return GENCIA_nombre;
    }

    public void setGENCIA_nombre(String GENCIA_nombre) {
        this.GENCIA_nombre = GENCIA_nombre;
    }

    public String getGENOFI_codigo() {
        return GENOFI_codigo;
    }

    public void setGENOFI_codigo(String GENOFI_codigo) {
        this.GENOFI_codigo = GENOFI_codigo;
    }

    public String getGENOFI_nombre() {
        return GENOFI_nombre;
    }

    public void setGENOFI_nombre(String GENOFI_nombre) {
        this.GENOFI_nombre = GENOFI_nombre;
    }

    public String getINVITM_codigo() {
        return INVITM_codigo;
    }

    public void setINVITM_codigo(String INVITM_codigo) {
        this.INVITM_codigo = INVITM_codigo;
    }

    public String getINVITM_codalterno() {
        return INVITM_codalterno;
    }

    public void setINVITM_codalterno(String INVITM_codalterno) {
        this.INVITM_codalterno = INVITM_codalterno;
    }

    public String getINVITM_nombre() {
        return INVITM_nombre;
    }

    public void setINVITM_nombre(String INVITM_nombre) {
        this.INVITM_nombre = INVITM_nombre;
    }

    public String getINVIN1_codin1item() {
        return INVIN1_codin1item;
    }

    public void setINVIN1_codin1item(String INVIN1_codin1item) {
        this.INVIN1_codin1item = INVIN1_codin1item;
    }

    public String getINVIN2_codin2item() {
        return INVIN2_codin2item;
    }

    public void setINVIN2_codin2item(String INVIN2_codin2item) {
        this.INVIN2_codin2item = INVIN2_codin2item;
    }

    public String getINVIN3_codin3item() {
        return INVIN3_codin3item;
    }

    public void setINVIN3_codin3item(String INVIN3_codin3item) {
        this.INVIN3_codin3item = INVIN3_codin3item;
    }

    public String getINVIN4_codin4item() {
        return INVIN4_codin4item;
    }

    public void setINVIN4_codin4item(String INVIN4_codin4item) {
        this.INVIN4_codin4item = INVIN4_codin4item;
    }

    public String getINVIN1_descripcio() {
        return INVIN1_descripcio;
    }

    public void setINVIN1_descripcio(String INVIN1_descripcio) {
        this.INVIN1_descripcio = INVIN1_descripcio;
    }

    public String getINVIN2_descripcio() {
        return INVIN2_descripcio;
    }

    public void setINVIN2_descripcio(String INVIN2_descripcio) {
        this.INVIN2_descripcio = INVIN2_descripcio;
    }

    public String getINVIN3_descripcio() {
        return INVIN3_descripcio;
    }

    public void setINVIN3_descripcio(String INVIN3_descripcio) {
        this.INVIN3_descripcio = INVIN3_descripcio;
    }

    public String getINVIN4_descripcio() {
        return INVIN4_descripcio;
    }

    public void setINVIN4_descripcio(String INVIN4_descripcio) {
        this.INVIN4_descripcio = INVIN4_descripcio;
    }

    public BigDecimal getINVITM_peso() {
        return INVITM_peso;
    }

    public void setINVITM_peso(BigDecimal INVITM_peso) {
        this.INVITM_peso = INVITM_peso;
    }

    public BigDecimal getINVITM_volumen() {
        return INVITM_volumen;
    }

    public void setINVITM_volumen(BigDecimal INVITM_volumen) {
        this.INVITM_volumen = INVITM_volumen;
    }

    public Date getINVITM_fecultvent() {
        return INVITM_fecultvent;
    }

    public void setINVITM_fecultvent(Date INVITM_fecultvent) {
        this.INVITM_fecultvent = INVITM_fecultvent;
    }

    public Date getINVITM_fecultcomp() {
        return INVITM_fecultcomp;
    }

    public void setINVITM_fecultcomp(Date INVITM_fecultcomp) {
        this.INVITM_fecultcomp = INVITM_fecultcomp;
    }

    public Date getINVITM_fechaingre() {
        return INVITM_fechaingre;
    }

    public void setINVITM_fechaingre(Date INVITM_fechaingre) {
        this.INVITM_fechaingre = INVITM_fechaingre;
    }

    public Date getESTIDE_Fecha() {
        return ESTIDE_Fecha;
    }

    public void setESTIDE_Fecha(Date ESTIDE_Fecha) {
        this.ESTIDE_Fecha = ESTIDE_Fecha;
    }

    public Integer getINVITM_unidembala() {
        return INVITM_unidembala;
    }

    public void setINVITM_unidembala(Integer INVITM_unidembala) {
        this.INVITM_unidembala = INVITM_unidembala;
    }

    public String getINVITM_esttipo() {
        return INVITM_esttipo;
    }

    public void setINVITM_esttipo(String INVITM_esttipo) {
        this.INVITM_esttipo = INVITM_esttipo;
    }

    public String getINVITM_estactivo() {
        return INVITM_estactivo;
    }

    public void setINVITM_estactivo(String INVITM_estactivo) {
        this.INVITM_estactivo = INVITM_estactivo;
    }

    public String getINVITM_estensamb() {
        return INVITM_estensamb;
    }

    public void setINVITM_estensamb(String INVITM_estensamb) {
        this.INVITM_estensamb = INVITM_estensamb;
    }

    public String getINVITM_estafecinv() {
        return INVITM_estafecinv;
    }

    public void setINVITM_estafecinv(String INVITM_estafecinv) {
        this.INVITM_estafecinv = INVITM_estafecinv;
    }

    public String getINVITM_estdispvta() {
        return INVITM_estdispvta;
    }

    public void setINVITM_estdispvta(String INVITM_estdispvta) {
        this.INVITM_estdispvta = INVITM_estdispvta;
    }

    public String getINVITM_estvtafrac() {
        return INVITM_estvtafrac;
    }

    public void setINVITM_estvtafrac(String INVITM_estvtafrac) {
        this.INVITM_estvtafrac = INVITM_estvtafrac;
    }

    public Integer getINVITM_stockreal() {
        return INVITM_stockreal;
    }

    public void setINVITM_stockreal(Integer INVITM_stockreal) {
        this.INVITM_stockreal = INVITM_stockreal;
    }

    public Integer getINVITM_cajas() {
        return INVITM_cajas;
    }

    public void setINVITM_cajas(Integer INVITM_cajas) {
        this.INVITM_cajas = INVITM_cajas;
    }

    public Integer getINVITM_unidades() {
        return INVITM_unidades;
    }

    public void setINVITM_unidades(Integer INVITM_unidades) {
        this.INVITM_unidades = INVITM_unidades;
    }

    public BigDecimal getINVITM_costoprom1() {
        return INVITM_costoprom1;
    }

    public void setINVITM_costoprom1(BigDecimal INVITM_costoprom1) {
        this.INVITM_costoprom1 = INVITM_costoprom1;
    }

    public BigDecimal getINVITM_costoprom1IVA() {
        return INVITM_costoprom1IVA;
    }

    public void setINVITM_costoprom1IVA(BigDecimal INVITM_costoprom1IVA) {
        this.INVITM_costoprom1IVA = INVITM_costoprom1IVA;
    }

    public BigDecimal getINVITM_impuestoIVA() {
        return INVITM_impuestoIVA;
    }

    public void setINVITM_impuestoIVA(BigDecimal INVITM_impuestoIVA) {
        this.INVITM_impuestoIVA = INVITM_impuestoIVA;
    }

    public BigDecimal getINVITM_preciopvp() {
        return INVITM_preciopvp;
    }

    public void setINVITM_preciopvp(BigDecimal INVITM_preciopvp) {
        this.INVITM_preciopvp = INVITM_preciopvp;
    }

    public BigDecimal getINVITM_precilista() {
        return INVITM_precilista;
    }

    public void setINVITM_precilista(BigDecimal INVITM_precilista) {
        this.INVITM_precilista = INVITM_precilista;
    }

    public BigDecimal getINVITM_precilistaIVA() {
        return INVITM_precilistaIVA;
    }

    public void setINVITM_precilistaIVA(BigDecimal INVITM_precilistaIVA) {
        this.INVITM_precilistaIVA = INVITM_precilistaIVA;
    }

    public Integer getINVITM_cajas1() {
        return INVITM_cajas1;
    }

    public void setINVITM_cajas1(Integer INVITM_cajas1) {
        this.INVITM_cajas1 = INVITM_cajas1;
    }

    public Integer getINVITM_unidad1() {
        return INVITM_unidad1;
    }

    public void setINVITM_unidad1(Integer INVITM_unidad1) {
        this.INVITM_unidad1 = INVITM_unidad1;
    }

    public Integer getINVITM_canfunci1() {
        return INVITM_canfunci1;
    }

    public void setINVITM_canfunci1(Integer INVITM_canfunci1) {
        this.INVITM_canfunci1 = INVITM_canfunci1;
    }

    public Integer getINVITM_cajas2() {
        return INVITM_cajas2;
    }

    public void setINVITM_cajas2(Integer INVITM_cajas2) {
        this.INVITM_cajas2 = INVITM_cajas2;
    }

    public Integer getINVITM_unidad2() {
        return INVITM_unidad2;
    }

    public void setINVITM_unidad2(Integer INVITM_unidad2) {
        this.INVITM_unidad2 = INVITM_unidad2;
    }

    public Integer getINVITM_canfunci2() {
        return INVITM_canfunci2;
    }

    public void setINVITM_canfunci2(Integer INVITM_canfunci2) {
        this.INVITM_canfunci2 = INVITM_canfunci2;
    }

    public Integer getINVITM_cajas3() {
        return INVITM_cajas3;
    }

    public void setINVITM_cajas3(Integer INVITM_cajas3) {
        this.INVITM_cajas3 = INVITM_cajas3;
    }

    public Integer getINVITM_unidad3() {
        return INVITM_unidad3;
    }

    public void setINVITM_unidad3(Integer INVITM_unidad3) {
        this.INVITM_unidad3 = INVITM_unidad3;
    }

    public Integer getINVITM_canfunci3() {
        return INVITM_canfunci3;
    }

    public void setINVITM_canfunci3(Integer INVITM_canfunci3) {
        this.INVITM_canfunci3 = INVITM_canfunci3;
    }

    public Integer getINVITM_cajas4() {
        return INVITM_cajas4;
    }

    public void setINVITM_cajas4(Integer INVITM_cajas4) {
        this.INVITM_cajas4 = INVITM_cajas4;
    }

    public Integer getINVITM_unidad4() {
        return INVITM_unidad4;
    }

    public void setINVITM_unidad4(Integer INVITM_unidad4) {
        this.INVITM_unidad4 = INVITM_unidad4;
    }

    public Integer getINVITM_canfunci4() {
        return INVITM_canfunci4;
    }

    public void setINVITM_canfunci4(Integer INVITM_canfunci4) {
        this.INVITM_canfunci4 = INVITM_canfunci4;
    }

    public Integer getINVITM_cajas5() {
        return INVITM_cajas5;
    }

    public void setINVITM_cajas5(Integer INVITM_cajas5) {
        this.INVITM_cajas5 = INVITM_cajas5;
    }

    public Integer getINVITM_unidad5() {
        return INVITM_unidad5;
    }

    public void setINVITM_unidad5(Integer INVITM_unidad5) {
        this.INVITM_unidad5 = INVITM_unidad5;
    }

    public Integer getINVITM_canfunci5() {
        return INVITM_canfunci5;
    }

    public void setINVITM_canfunci5(Integer INVITM_canfunci5) {
        this.INVITM_canfunci5 = INVITM_canfunci5;
    }

    public Integer getINVITM_cajas6() {
        return INVITM_cajas6;
    }

    public void setINVITM_cajas6(Integer INVITM_cajas6) {
        this.INVITM_cajas6 = INVITM_cajas6;
    }

    public Integer getINVITM_unidad6() {
        return INVITM_unidad6;
    }

    public void setINVITM_unidad6(Integer INVITM_unidad6) {
        this.INVITM_unidad6 = INVITM_unidad6;
    }

    public Integer getINVITM_canfunci6() {
        return INVITM_canfunci6;
    }

    public void setINVITM_canfunci6(Integer INVITM_canfunci6) {
        this.INVITM_canfunci6 = INVITM_canfunci6;
    }

    public BigDecimal getINVITM_ventatotalQ() {
        return INVITM_ventatotalQ;
    }

    public void setINVITM_ventatotalQ(BigDecimal INVITM_ventatotalQ) {
        this.INVITM_ventatotalQ = INVITM_ventatotalQ;
    }

    public BigDecimal getINVITM_ventapromedioQ() {
        return INVITM_ventapromedioQ;
    }

    public void setINVITM_ventapromedioQ(BigDecimal INVITM_ventapromedioQ) {
        this.INVITM_ventapromedioQ = INVITM_ventapromedioQ;
    }

    public BigDecimal getINVITM_totalinventario() {
        return INVITM_totalinventario;
    }

    public void setINVITM_totalinventario(BigDecimal INVITM_totalinventario) {
        this.INVITM_totalinventario = INVITM_totalinventario;
    }

    public BigDecimal getINVITM_diasinventario() {
        return INVITM_diasinventario;
    }

    public void setINVITM_diasinventario(BigDecimal INVITM_diasinventario) {
        this.INVITM_diasinventario = INVITM_diasinventario;
    }

    public Character getINVITM_estadoIVA() {
        return INVITM_estadoIVA;
    }

    public void setINVITM_estadoIVA(Character INVITM_estadoIVA) {
        this.INVITM_estadoIVA = INVITM_estadoIVA;
    }

    public String getINVITM_codigoZAP() {
        return INVITM_codigoZAP;
    }

    public void setINVITM_codigoZAP(String INVITM_codigoZAP) {
        this.INVITM_codigoZAP = INVITM_codigoZAP;
    }

    public Integer getINVITM_diasventa() {
        return INVITM_diasventa;
    }

    public void setINVITM_diasventa(Integer INVITM_diasventa) {
        this.INVITM_diasventa = INVITM_diasventa;
    }

    public Integer getINVITM_diascreado() {
        return INVITM_diascreado;
    }

    public void setINVITM_diascreado(Integer INVITM_diascreado) {
        this.INVITM_diascreado = INVITM_diascreado;
    }

    public String getSEGUSU_codigo() {
        return SEGUSU_codigo;
    }

    public void setSEGUSU_codigo(String SEGUSU_codigo) {
        this.SEGUSU_codigo = SEGUSU_codigo;
    }

    public String getSEGOPC_codigo() {
        return SEGOPC_codigo;
    }

    public void setSEGOPC_codigo(String SEGOPC_codigo) {
        this.SEGOPC_codigo = SEGOPC_codigo;
    }

    public String getSEGMAQ_codigo() {
        return SEGMAQ_codigo;
    }

    public void setSEGMAQ_codigo(String SEGMAQ_codigo) {
        this.SEGMAQ_codigo = SEGMAQ_codigo;
    }

    public String getINVITM_manedecimales() {
        return INVITM_manedecimales;
    }

    public void setINVITM_manedecimales(String INVITM_manedecimales) {
        this.INVITM_manedecimales = INVITM_manedecimales;
    }

    public BigDecimal getINVITM_totalbasecero() {
        return INVITM_totalbasecero;
    }

    public void setINVITM_totalbasecero(BigDecimal INVITM_totalbasecero) {
        this.INVITM_totalbasecero = INVITM_totalbasecero;
    }

    public BigDecimal getINVITM_totalImponible() {
        return INVITM_totalImponible;
    }

    public void setINVITM_totalImponible(BigDecimal INVITM_totalImponible) {
        this.INVITM_totalImponible = INVITM_totalImponible;
    }

    public BigDecimal getINVITM_ImpuestoGrabado() {
        return INVITM_ImpuestoGrabado;
    }

    public void setINVITM_ImpuestoGrabado(BigDecimal INVITM_ImpuestoGrabado) {
        this.INVITM_ImpuestoGrabado = INVITM_ImpuestoGrabado;
    }

    public BigDecimal getINVITM_inventarioAdd() {
        return INVITM_inventarioAdd;
    }

    public void setINVITM_inventarioAdd(BigDecimal INVITM_inventarioAdd) {
        this.INVITM_inventarioAdd = INVITM_inventarioAdd;
    }

    public BigDecimal getDivsug() {
        return divsug;
    }

    public void setDivsug(BigDecimal divsug) {
        this.divsug = divsug;
    }

    public Integer getINVITM_CajasReales() {
        return INVITM_CajasReales;
    }

    public void setINVITM_CajasReales(Integer INVITM_CajasReales) {
        this.INVITM_CajasReales = INVITM_CajasReales;
    }

    public Integer getINVITM_CajasSugeridad() {
        return INVITM_CajasSugeridad;
    }

    public void setINVITM_CajasSugeridad(Integer INVITM_CajasSugeridad) {
        this.INVITM_CajasSugeridad = INVITM_CajasSugeridad;
    }

    public BigDecimal getINVITM_ventaTotalSugerida() {
        return INVITM_ventaTotalSugerida;
    }

    public void setINVITM_ventaTotalSugerida(BigDecimal INVITM_ventaTotalSugerida) {
        this.INVITM_ventaTotalSugerida = INVITM_ventaTotalSugerida;
    }

    public Integer getINVITM_UniReales() {
        return INVITM_UniReales;
    }

    public void setINVITM_UniReales(Integer INVITM_UniReales) {
        this.INVITM_UniReales = INVITM_UniReales;
    }

    public Integer getINVITM_CantFunciReal() {
        return INVITM_CantFunciReal;
    }

    public void setINVITM_CantFunciReal(Integer INVITM_CantFunciReal) {
        this.INVITM_CantFunciReal = INVITM_CantFunciReal;
    }

    public BigDecimal getINVITM_costoventanetaQ() {
        return INVITM_costoventanetaQ;
    }

    public void setINVITM_costoventanetaQ(BigDecimal INVITM_costoventanetaQ) {
        this.INVITM_costoventanetaQ = INVITM_costoventanetaQ;
    }

    public BigDecimal getCOMPDS_stockproveedor() {
        return COMPDS_stockproveedor;
    }

    public void setCOMPDS_stockproveedor(BigDecimal COMPDS_stockproveedor) {
        this.COMPDS_stockproveedor = COMPDS_stockproveedor;
    }

    public String getINVIN1_codin1item_1() {
        return INVIN1_codin1item_1;
    }

    public void setINVIN1_codin1item_1(String INVIN1_codin1item_1) {
        this.INVIN1_codin1item_1 = INVIN1_codin1item_1;
    }

    public String getINVIN2_codin2item_1() {
        return INVIN2_codin2item_1;
    }

    public void setINVIN2_codin2item_1(String INVIN2_codin2item_1) {
        this.INVIN2_codin2item_1 = INVIN2_codin2item_1;
    }

    @Override
    public String toString() {
        return "InveRepdiasInventario{" + "INVITM_codigo=" + INVITM_codigo + ", INVITM_nombre=" + INVITM_nombre + ", INVIN1_codin1item=" + INVIN1_codin1item + ", INVIN2_codin2item=" + INVIN2_codin2item + ", INVIN3_codin3item=" + INVIN3_codin3item + ", INVIN4_codin4item=" + INVIN4_codin4item + ", INVIN1_descripcio=" + INVIN1_descripcio + ", INVIN2_descripcio=" + INVIN2_descripcio + ", INVIN3_descripcio=" + INVIN3_descripcio + ", INVIN4_descripcio=" + INVIN4_descripcio + '}';
    }
    
    public void getTotalSug(){
        INVITM_ventapromedioQ = INVITM_costoprom1.multiply(BigDecimal.valueOf(INVITM_CajasReales==null?0:INVITM_CajasReales));
    }
    
}
