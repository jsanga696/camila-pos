/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.model.consultas;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author userdb6
 */
public class RtpFactura implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private String usuario_reporte ;
    private int secuencia;
    private String factura;
    private String cliente_nombre;
    private String identificacion;
    private String direccion;
    private String telefono;
    private Date fecha;
    private BigDecimal subtotamovimp;
    private BigDecimal subtotamovexe;
    private BigDecimal subtotamov;
    private BigDecimal totimpumov;
    private BigDecimal desctoimp;
    private BigDecimal desctoexe;
    private BigDecimal desctomov;
    private BigDecimal netomov;
    private BigDecimal totunidpedi;
    private String invitm_codigo;
    private String invitm_nombre;
    private BigDecimal facdvc_cantunidad;
    private BigDecimal facdvc_preciomov;
    private BigDecimal facdvc_desctomov;
    private BigDecimal facdvc_subtotmov;
    private String cajero;
    private BigDecimal fcdvc_cantcajas;
    private BigDecimal facdvc_cambio;
    private String codigo_cliente;

    public RtpFactura() {
    }

    public String getUsuario_reporte() {
        return usuario_reporte;
    }

    public void setUsuario_reporte(String usuario_reporte) {
        this.usuario_reporte = usuario_reporte;
    }

    public int getSecuencia() {
        return secuencia;
    }

    public void setSecuencia(int secuencia) {
        this.secuencia = secuencia;
    }

    public String getFactura() {
        return factura;
    }

    public void setFactura(String factura) {
        this.factura = factura;
    }

    public String getCliente_nombre() {
        return cliente_nombre;
    }

    public void setCliente_nombre(String cliente_nombre) {
        this.cliente_nombre = cliente_nombre;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public BigDecimal getSubtotamovimp() {
        return subtotamovimp;
    }

    public void setSubtotamovimp(BigDecimal subtotamovimp) {
        this.subtotamovimp = subtotamovimp;
    }

    public BigDecimal getSubtotamovexe() {
        return subtotamovexe;
    }

    public void setSubtotamovexe(BigDecimal subtotamovexe) {
        this.subtotamovexe = subtotamovexe;
    }

    public BigDecimal getSubtotamov() {
        return subtotamov;
    }

    public void setSubtotamov(BigDecimal subtotamov) {
        this.subtotamov = subtotamov;
    }

    public BigDecimal getTotimpumov() {
        return totimpumov;
    }

    public void setTotimpumov(BigDecimal totimpumov) {
        this.totimpumov = totimpumov;
    }

    public BigDecimal getDesctoimp() {
        return desctoimp;
    }

    public void setDesctoimp(BigDecimal desctoimp) {
        this.desctoimp = desctoimp;
    }

    public BigDecimal getDesctoexe() {
        return desctoexe;
    }

    public void setDesctoexe(BigDecimal desctoexe) {
        this.desctoexe = desctoexe;
    }

    public BigDecimal getDesctomov() {
        return desctomov;
    }

    public void setDesctomov(BigDecimal desctomov) {
        this.desctomov = desctomov;
    }

    public BigDecimal getNetomov() {
        return netomov;
    }

    public void setNetomov(BigDecimal netomov) {
        this.netomov = netomov;
    }

    public BigDecimal getTotunidpedi() {
        return totunidpedi;
    }

    public void setTotunidpedi(BigDecimal totunidpedi) {
        this.totunidpedi = totunidpedi;
    }

    public String getInvitm_codigo() {
        return invitm_codigo;
    }

    public void setInvitm_codigo(String invitm_codigo) {
        this.invitm_codigo = invitm_codigo;
    }

    public String getInvitm_nombre() {
        return invitm_nombre;
    }

    public void setInvitm_nombre(String invitm_nombre) {
        this.invitm_nombre = invitm_nombre;
    }

    public BigDecimal getFacdvc_cantunidad() {
        return facdvc_cantunidad;
    }

    public void setFacdvc_cantunidad(BigDecimal facdvc_cantunidad) {
        this.facdvc_cantunidad = facdvc_cantunidad;
    }

    public BigDecimal getFacdvc_preciomov() {
        return facdvc_preciomov;
    }

    public void setFacdvc_preciomov(BigDecimal facdvc_preciomov) {
        this.facdvc_preciomov = facdvc_preciomov;
    }

    public BigDecimal getFacdvc_desctomov() {
        return facdvc_desctomov;
    }

    public void setFacdvc_desctomov(BigDecimal facdvc_desctomov) {
        this.facdvc_desctomov = facdvc_desctomov;
    }

    public BigDecimal getFacdvc_subtotmov() {
        return facdvc_subtotmov;
    }

    public void setFacdvc_subtotmov(BigDecimal facdvc_subtotmov) {
        this.facdvc_subtotmov = facdvc_subtotmov;
    }

    public String getCajero() {
        return cajero;
    }

    public void setCajero(String cajero) {
        this.cajero = cajero;
    }

    public BigDecimal getFcdvc_cantcajas() {
        return fcdvc_cantcajas;
    }

    public void setFcdvc_cantcajas(BigDecimal fcdvc_cantcajas) {
        this.fcdvc_cantcajas = fcdvc_cantcajas;
    }

    public BigDecimal getFacdvc_cambio() {
        return facdvc_cambio;
    }

    public void setFacdvc_cambio(BigDecimal facdvc_cambio) {
        this.facdvc_cambio = facdvc_cambio;
    }

    public String getCodigo_cliente() {
        return codigo_cliente;
    }

    public void setCodigo_cliente(String codigo_cliente) {
        this.codigo_cliente = codigo_cliente;
    }
    
}
