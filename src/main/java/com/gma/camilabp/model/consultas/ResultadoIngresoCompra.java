/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.model.consultas;

import java.io.Serializable;

/**
 *
 * @author userdb5
 */
public class ResultadoIngresoCompra implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private Integer COMCCM_numsecucial;
    private Integer COMCCM_numdocumen;
    
    public ResultadoIngresoCompra(){
    
    }

    public Integer getCOMCCM_numsecucial() {
        return COMCCM_numsecucial;
    }

    public void setCOMCCM_numsecucial(Integer COMCCM_numsecucial) {
        this.COMCCM_numsecucial = COMCCM_numsecucial;
    }

    public Integer getCOMCCM_numdocumen() {
        return COMCCM_numdocumen;
    }

    public void setCOMCCM_numdocumen(Integer COMCCM_numdocumen) {
        this.COMCCM_numdocumen = COMCCM_numdocumen;
    }
    
}
