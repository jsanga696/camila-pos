/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.model.consultas;

import com.gma.camilabp.domain.ComtCabcompra;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author userdb5
 */
public class IngresoBodegaModel implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private Integer COMCCM_numsecucial;
    private Integer COMCCM_numdocumen;
    private String GENCIA_codigo;
    private String GENOFI_codigo;
    private String GENMOD_codigo="COM";
    private String GENTDO_codigo="COM";
    private String CXPPRO_codigo;
    private Date COMCCM_fecrecepci;
    private Date CXPDOC_fechafactura;
    private String COMCCM_descricort;
    private String COMCCM_descrilarg;
    private String COMCCM_numfacprov;
    private String COMCCM_estado;
    private String GENMON_codigo;
    private String COMTDO_codigorel;
    private String COMCCM_numdocurel;
    private Long COMCCM_numsecrel;
    private BigDecimal COMCCM_subtotamov;
    private BigDecimal COMCCM_desctomov;
    private BigDecimal COMCCM_totimpumov;
    private BigDecimal COMCCM_netomov;
    private BigDecimal COMCCM_valimpICE;
    private String COMCCM_estuso;
    private BigDecimal COMCCM_totalvolum;
    private BigDecimal COMCCM_totalpeso;
    private String COMCCM_estelimina;
    private String SEGUSU_codigo;
    private Boolean COMCCM_prepostea;
    private String ACCION;
    private String codigoTalonario;
    private String codigoBodega;

    public IngresoBodegaModel(){
        this.COMCCM_numsecucial = null;
        this.COMCCM_numdocumen = null;
        this.codigoTalonario = null;
        this.COMCCM_numfacprov = null;
        this.COMTDO_codigorel = "";
        this.COMCCM_numdocurel = "";
        this.COMCCM_numsecrel = null;
        this.COMCCM_desctomov = BigDecimal.valueOf(0.0);
        this.COMCCM_totimpumov = BigDecimal.valueOf(0.0);
        this.COMCCM_valimpICE = BigDecimal.valueOf(0.0);
        this.COMCCM_estuso = "N";
        this.COMCCM_totalvolum = BigDecimal.valueOf(0.0);
        this.COMCCM_totalpeso = BigDecimal.valueOf(0.0);
        this.COMCCM_estelimina = "N";
        this.COMCCM_prepostea = Boolean.FALSE;
        this.COMCCM_fecrecepci = new Date();
        this.CXPDOC_fechafactura = new Date();
    }
    
    public void cargarDatosCompra(ComtCabcompra compra){
        this.setGENOFI_codigo(compra.getGENOFIcodigo());
        this.setGENMON_codigo(compra.getGENMONcodigo());
        this.setCOMCCM_numdocumen(compra.getCOMCCMnumdocumen());
        this.setCOMCCM_numsecucial(compra.getCOMCCMnumsecucial().intValue());
        this.setCOMCCM_fecrecepci(compra.getCOMCCMfecrecepci());
        this.setCXPDOC_fechafactura(compra.getCXPDOCfechafactura());
        this.setCOMCCM_descrilarg(compra.getCOMCCMdescrilarg());
        this.setCodigoTalonario(compra.getCOMCCMnumfacprov().split("-")[0]);
        this.setCOMCCM_numfacprov(compra.getCOMCCMnumfacprov().split("-")[1]);
        this.setCodigoBodega(compra.getBodega());
        this.setCXPPRO_codigo(compra.getCXPPROcodigo());
        this.setCOMCCM_numsecrel(compra.getCOMCCMnumsecrel().longValue());
        this.setCOMCCM_numdocurel(compra.getCOMCCMnumdocurel());
    }
    
    public Integer getCOMCCM_numsecucial() {
        return COMCCM_numsecucial;
    }

    public void setCOMCCM_numsecucial(Integer COMCCM_numsecucial) {
        this.COMCCM_numsecucial = COMCCM_numsecucial;
    }

    public Integer getCOMCCM_numdocumen() {
        return COMCCM_numdocumen;
    }

    public void setCOMCCM_numdocumen(Integer COMCCM_numdocumen) {
        this.COMCCM_numdocumen = COMCCM_numdocumen;
    }

    public String getGENCIA_codigo() {
        return GENCIA_codigo;
    }

    public void setGENCIA_codigo(String GENCIA_codigo) {
        this.GENCIA_codigo = GENCIA_codigo;
    }

    public String getGENOFI_codigo() {
        return GENOFI_codigo;
    }

    public void setGENOFI_codigo(String GENOFI_codigo) {
        this.GENOFI_codigo = GENOFI_codigo;
    }

    public String getGENMOD_codigo() {
        return GENMOD_codigo;
    }

    public void setGENMOD_codigo(String GENMOD_codigo) {
        this.GENMOD_codigo = GENMOD_codigo;
    }

    public String getGENTDO_codigo() {
        return GENTDO_codigo;
    }

    public void setGENTDO_codigo(String GENTDO_codigo) {
        this.GENTDO_codigo = GENTDO_codigo;
    }

    public String getCXPPRO_codigo() {
        return CXPPRO_codigo;
    }

    public void setCXPPRO_codigo(String CXPPRO_codigo) {
        this.CXPPRO_codigo = CXPPRO_codigo;
    }

    public Date getCOMCCM_fecrecepci() {
        return COMCCM_fecrecepci;
    }

    public void setCOMCCM_fecrecepci(Date COMCCM_fecrecepci) {
        this.COMCCM_fecrecepci = COMCCM_fecrecepci;
    }

    public Date getCXPDOC_fechafactura() {
        return CXPDOC_fechafactura;
    }

    public void setCXPDOC_fechafactura(Date CXPDOC_fechafactura) {
        this.CXPDOC_fechafactura = CXPDOC_fechafactura;
    }

    public String getCOMCCM_descricort() {
        return COMCCM_descricort;
    }

    public void setCOMCCM_descricort(String COMCCM_descricort) {
        this.COMCCM_descricort = COMCCM_descricort;
    }

    public String getCOMCCM_descrilarg() {
        return COMCCM_descrilarg;
    }

    public void setCOMCCM_descrilarg(String COMCCM_descrilarg) {
        this.COMCCM_descrilarg = COMCCM_descrilarg;
    }

    public String getCOMCCM_numfacprov() {
        return COMCCM_numfacprov;
    }

    public void setCOMCCM_numfacprov(String COMCCM_numfacprov) {
        this.COMCCM_numfacprov = COMCCM_numfacprov;
    }

    public String getCOMCCM_estado() {
        return COMCCM_estado;
    }

    public void setCOMCCM_estado(String COMCCM_estado) {
        this.COMCCM_estado = COMCCM_estado;
    }

    public String getGENMON_codigo() {
        return GENMON_codigo;
    }

    public void setGENMON_codigo(String GENMON_codigo) {
        this.GENMON_codigo = GENMON_codigo;
    }

    public String getCOMTDO_codigorel() {
        return COMTDO_codigorel;
    }

    public void setCOMTDO_codigorel(String COMTDO_codigorel) {
        this.COMTDO_codigorel = COMTDO_codigorel;
    }

    public String getCOMCCM_numdocurel() {
        return COMCCM_numdocurel;
    }

    public void setCOMCCM_numdocurel(String COMCCM_numdocurel) {
        this.COMCCM_numdocurel = COMCCM_numdocurel;
    }

    public Long getCOMCCM_numsecrel() {
        return COMCCM_numsecrel;
    }

    public void setCOMCCM_numsecrel(Long COMCCM_numsecrel) {
        this.COMCCM_numsecrel = COMCCM_numsecrel;
    }

    public BigDecimal getCOMCCM_subtotamov() {
        return COMCCM_subtotamov;
    }

    public void setCOMCCM_subtotamov(BigDecimal COMCCM_subtotamov) {
        this.COMCCM_subtotamov = COMCCM_subtotamov;
    }

    public BigDecimal getCOMCCM_desctomov() {
        return COMCCM_desctomov;
    }

    public void setCOMCCM_desctomov(BigDecimal COMCCM_desctomov) {
        this.COMCCM_desctomov = COMCCM_desctomov;
    }

    public BigDecimal getCOMCCM_totimpumov() {
        return COMCCM_totimpumov;
    }

    public void setCOMCCM_totimpumov(BigDecimal COMCCM_totimpumov) {
        this.COMCCM_totimpumov = COMCCM_totimpumov;
    }

    public BigDecimal getCOMCCM_netomov() {
        return COMCCM_netomov;
    }

    public void setCOMCCM_netomov(BigDecimal COMCCM_netomov) {
        this.COMCCM_netomov = COMCCM_netomov;
    }

    public BigDecimal getCOMCCM_valimpICE() {
        return COMCCM_valimpICE;
    }

    public void setCOMCCM_valimpICE(BigDecimal COMCCM_valimpICE) {
        this.COMCCM_valimpICE = COMCCM_valimpICE;
    }

    public String getCOMCCM_estuso() {
        return COMCCM_estuso;
    }

    public void setCOMCCM_estuso(String COMCCM_estuso) {
        this.COMCCM_estuso = COMCCM_estuso;
    }

    public BigDecimal getCOMCCM_totalvolum() {
        return COMCCM_totalvolum;
    }

    public void setCOMCCM_totalvolum(BigDecimal COMCCM_totalvolum) {
        this.COMCCM_totalvolum = COMCCM_totalvolum;
    }

    public BigDecimal getCOMCCM_totalpeso() {
        return COMCCM_totalpeso;
    }

    public void setCOMCCM_totalpeso(BigDecimal COMCCM_totalpeso) {
        this.COMCCM_totalpeso = COMCCM_totalpeso;
    }

    public String getCOMCCM_estelimina() {
        return COMCCM_estelimina;
    }

    public void setCOMCCM_estelimina(String COMCCM_estelimina) {
        this.COMCCM_estelimina = COMCCM_estelimina;
    }

    public String getSEGUSU_codigo() {
        return SEGUSU_codigo;
    }

    public void setSEGUSU_codigo(String SEGUSU_codigo) {
        this.SEGUSU_codigo = SEGUSU_codigo;
    }

    public Boolean getCOMCCM_prepostea() {
        return COMCCM_prepostea;
    }

    public void setCOMCCM_prepostea(Boolean COMCCM_prepostea) {
        this.COMCCM_prepostea = COMCCM_prepostea;
    }

    public String getACCION() {
        return ACCION;
    }

    public void setACCION(String ACCION) {
        this.ACCION = ACCION;
    }

    public String getCodigoTalonario() {
        return codigoTalonario;
    }

    public void setCodigoTalonario(String codigoTalonario) {
        this.codigoTalonario = codigoTalonario;
    }    

    public String getCodigoBodega() {
        return codigoBodega;
    }

    public void setCodigoBodega(String codigoBodega) {
        this.codigoBodega = codigoBodega;
    }
    
}
