/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.model.consultas;

import java.io.Serializable;

/**
 *
 * @author ANGEL NAVARRO
 */
public class SugerenciaCompraSecuencia implements Serializable {

    private Integer secuencia;
    private Integer numero;

    public SugerenciaCompraSecuencia() {
    }

    public SugerenciaCompraSecuencia(Integer COMPDS_secuencia, Integer COMPDS_numero) {
        this.secuencia = COMPDS_secuencia;
        this.numero = COMPDS_numero;
    }

    public Integer getSecuencia() {
        return secuencia;
    }

    public void setSecuencia(Integer secuencia) {
        this.secuencia = secuencia;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    @Override
    public String toString() {
        return "SugerenciaCompraSecuencia{" + "COMPDS_secuencia=" + secuencia + ", COMPDS_numero=" + numero + '}';
    }

}
