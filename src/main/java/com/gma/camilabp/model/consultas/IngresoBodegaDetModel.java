/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.model.consultas;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author userdb5
 */
public class IngresoBodegaDetModel implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private Integer COMCCM_numsecucial;
    private Integer COMDCM_numlinea;
    private String COMCCM_numdocumen;
    private String GENCIA_codigo;
    private String GENOFI_codigo;
    private String GENMOD_codigo;
    private String GENTDO_codigo;
    private String COMDCM_codbodega;
    private String INVITM_codigo;
    private Integer COMDCM_numlinrela;
    private BigDecimal COMDCM_canticaja;
    private BigDecimal COMDCM_cantiunida;
    private BigDecimal COMDCM_cantifunci;
    private BigDecimal COMDCM_cantfunpen;
    private BigDecimal COMDCM_cantfundev;
    private BigDecimal COMDCM_cantegresa;
    private BigDecimal COMDCM_cantfactu;
    private String INVUPR_unibascomp;
    private String INVUPR_unirelcomp;
    private Integer COMDCM_unidembala;
    private BigDecimal COMDCM_peso;
    private BigDecimal COMDCM_costomov;
    private BigDecimal COMDCM_preciomov;
    private BigDecimal COMDCM_porcdescto;
    private BigDecimal COMDCM_subtotamov;
    private BigDecimal COMDCM_desctomov;
    private BigDecimal COMDCM_totimpumov;
    private BigDecimal COMDCM_otrosimpuestos;
    private BigDecimal COMDCM_netomov;
    private BigDecimal COMDCM_volumen;
    private Integer CXPICA_codclase;
    private String COMCCM_prepostea;
    private String COMDCM_codimpICE;
    private BigDecimal COMDCM_valimpICE;
    
    public IngresoBodegaDetModel(){
        this.COMDCM_netomov = BigDecimal.ZERO;
        this.COMDCM_volumen = BigDecimal.ZERO;
        this.COMDCM_numlinrela = 0;
        this.COMDCM_cantfundev = BigDecimal.ZERO;
    }
    
    public IngresoBodegaDetModel(String GENCIA_codigo, String GENOFI_codigo, String GENMOD_codigo, String GENTDO_codigo,
            String COMCCM_numdocumen, Integer COMDCM_numlinea, String INVITM_codigo){
        this.GENCIA_codigo = GENCIA_codigo;
        this.GENOFI_codigo = GENOFI_codigo;
        this.GENMOD_codigo = GENMOD_codigo;
        this.GENTDO_codigo = GENTDO_codigo;
        this.COMCCM_numdocumen = COMCCM_numdocumen;
        this.COMDCM_numlinea = COMDCM_numlinea;
        this.INVITM_codigo = INVITM_codigo;
        this.COMDCM_netomov = BigDecimal.ZERO;
        this.COMDCM_volumen = BigDecimal.ZERO;
        this.COMDCM_numlinrela = 0;
        this.COMDCM_cantfundev = BigDecimal.ZERO;
        this.COMDCM_cantegresa = BigDecimal.ZERO;
        this.COMDCM_cantfactu = BigDecimal.ZERO;
        this.COMDCM_costomov = BigDecimal.ZERO;
    }

    public String getGENCIA_codigo() {
        return GENCIA_codigo;
    }

    public void setGENCIA_codigo(String GENCIA_codigo) {
        this.GENCIA_codigo = GENCIA_codigo;
    }

    public String getGENOFI_codigo() {
        return GENOFI_codigo;
    }

    public void setGENOFI_codigo(String GENOFI_codigo) {
        this.GENOFI_codigo = GENOFI_codigo;
    }

    public String getGENMOD_codigo() {
        return GENMOD_codigo;
    }

    public void setGENMOD_codigo(String GENMOD_codigo) {
        this.GENMOD_codigo = GENMOD_codigo;
    }

    public String getGENTDO_codigo() {
        return GENTDO_codigo;
    }

    public void setGENTDO_codigo(String GENTDO_codigo) {
        this.GENTDO_codigo = GENTDO_codigo;
    }

    public String getCOMCCM_numdocumen() {
        return COMCCM_numdocumen;
    }

    public void setCOMCCM_numdocumen(String COMCCM_numdocumen) {
        this.COMCCM_numdocumen = COMCCM_numdocumen;
    }

    public Integer getCOMDCM_numlinea() {
        return COMDCM_numlinea;
    }

    public void setCOMDCM_numlinea(Integer COMDCM_numlinea) {
        this.COMDCM_numlinea = COMDCM_numlinea;
    }

    public String getINVITM_codigo() {
        return INVITM_codigo;
    }

    public void setINVITM_codigo(String INVITM_codigo) {
        this.INVITM_codigo = INVITM_codigo;
    }

    public Integer getCOMCCM_numsecucial() {
        return COMCCM_numsecucial;
    }

    public void setCOMCCM_numsecucial(Integer COMCCM_numsecucial) {
        this.COMCCM_numsecucial = COMCCM_numsecucial;
    }

    public String getCOMDCM_codbodega() {
        return COMDCM_codbodega;
    }

    public void setCOMDCM_codbodega(String COMDCM_codbodega) {
        this.COMDCM_codbodega = COMDCM_codbodega;
    }

    public Integer getCOMDCM_numlinrela() {
        return COMDCM_numlinrela;
    }

    public void setCOMDCM_numlinrela(Integer COMDCM_numlinrela) {
        this.COMDCM_numlinrela = COMDCM_numlinrela;
    }

    public BigDecimal getCOMDCM_canticaja() {
        return COMDCM_canticaja;
    }

    public void setCOMDCM_canticaja(BigDecimal COMDCM_canticaja) {
        this.COMDCM_canticaja = COMDCM_canticaja;
    }

    public BigDecimal getCOMDCM_cantiunida() {
        return COMDCM_cantiunida;
    }

    public void setCOMDCM_cantiunida(BigDecimal COMDCM_cantiunida) {
        this.COMDCM_cantiunida = COMDCM_cantiunida;
    }

    public BigDecimal getCOMDCM_cantifunci() {
        return COMDCM_cantifunci;
    }

    public void setCOMDCM_cantifunci(BigDecimal COMDCM_cantifunci) {
        this.COMDCM_cantifunci = COMDCM_cantifunci;
    }

    public BigDecimal getCOMDCM_cantfunpen() {
        return COMDCM_cantfunpen;
    }

    public void setCOMDCM_cantfunpen(BigDecimal COMDCM_cantfunpen) {
        this.COMDCM_cantfunpen = COMDCM_cantfunpen;
    }

    public BigDecimal getCOMDCM_cantfundev() {
        return COMDCM_cantfundev;
    }

    public void setCOMDCM_cantfundev(BigDecimal COMDCM_cantfundev) {
        this.COMDCM_cantfundev = COMDCM_cantfundev;
    }

    public BigDecimal getCOMDCM_cantegresa() {
        return COMDCM_cantegresa;
    }

    public void setCOMDCM_cantegresa(BigDecimal COMDCM_cantegresa) {
        this.COMDCM_cantegresa = COMDCM_cantegresa;
    }

    public BigDecimal getCOMDCM_cantfactu() {
        return COMDCM_cantfactu;
    }

    public void setCOMDCM_cantfactu(BigDecimal COMDCM_cantfactu) {
        this.COMDCM_cantfactu = COMDCM_cantfactu;
    }

    public String getINVUPR_unibascomp() {
        return INVUPR_unibascomp;
    }

    public void setINVUPR_unibascomp(String INVUPR_unibascomp) {
        this.INVUPR_unibascomp = INVUPR_unibascomp;
    }

    public String getINVUPR_unirelcomp() {
        return INVUPR_unirelcomp;
    }

    public void setINVUPR_unirelcomp(String INVUPR_unirelcomp) {
        this.INVUPR_unirelcomp = INVUPR_unirelcomp;
    }

    public Integer getCOMDCM_unidembala() {
        return COMDCM_unidembala;
    }

    public void setCOMDCM_unidembala(Integer COMDCM_unidembala) {
        this.COMDCM_unidembala = COMDCM_unidembala;
    }

    public BigDecimal getCOMDCM_peso() {
        return COMDCM_peso;
    }

    public void setCOMDCM_peso(BigDecimal COMDCM_peso) {
        this.COMDCM_peso = COMDCM_peso;
    }

    public BigDecimal getCOMDCM_costomov() {
        return COMDCM_costomov;
    }

    public void setCOMDCM_costomov(BigDecimal COMDCM_costomov) {
        this.COMDCM_costomov = COMDCM_costomov;
    }

    public BigDecimal getCOMDCM_preciomov() {
        return COMDCM_preciomov;
    }

    public void setCOMDCM_preciomov(BigDecimal COMDCM_preciomov) {
        this.COMDCM_preciomov = COMDCM_preciomov;
    }

    public BigDecimal getCOMDCM_porcdescto() {
        return COMDCM_porcdescto;
    }

    public void setCOMDCM_porcdescto(BigDecimal COMDCM_porcdescto) {
        this.COMDCM_porcdescto = COMDCM_porcdescto;
    }

    public BigDecimal getCOMDCM_subtotamov() {
        return COMDCM_subtotamov;
    }

    public void setCOMDCM_subtotamov(BigDecimal COMDCM_subtotamov) {
        this.COMDCM_subtotamov = COMDCM_subtotamov;
    }

    public BigDecimal getCOMDCM_desctomov() {
        return COMDCM_desctomov;
    }

    public void setCOMDCM_desctomov(BigDecimal COMDCM_desctomov) {
        this.COMDCM_desctomov = COMDCM_desctomov;
    }

    public BigDecimal getCOMDCM_totimpumov() {
        return COMDCM_totimpumov;
    }

    public void setCOMDCM_totimpumov(BigDecimal COMDCM_totimpumov) {
        this.COMDCM_totimpumov = COMDCM_totimpumov;
    }

    public BigDecimal getCOMDCM_otrosimpuestos() {
        return COMDCM_otrosimpuestos;
    }

    public void setCOMDCM_otrosimpuestos(BigDecimal COMDCM_otrosimpuestos) {
        this.COMDCM_otrosimpuestos = COMDCM_otrosimpuestos;
    }

    public BigDecimal getCOMDCM_netomov() {
        return COMDCM_netomov;
    }

    public void setCOMDCM_netomov(BigDecimal COMDCM_netomov) {
        this.COMDCM_netomov = COMDCM_netomov;
    }

    public BigDecimal getCOMDCM_volumen() {
        return COMDCM_volumen;
    }

    public void setCOMDCM_volumen(BigDecimal COMDCM_volumen) {
        this.COMDCM_volumen = COMDCM_volumen;
    }

    public Integer getCXPICA_codclase() {
        return CXPICA_codclase;
    }

    public void setCXPICA_codclase(Integer CXPICA_codclase) {
        this.CXPICA_codclase = CXPICA_codclase;
    }

    public String getCOMCCM_prepostea() {
        return COMCCM_prepostea;
    }

    public void setCOMCCM_prepostea(String COMCCM_prepostea) {
        this.COMCCM_prepostea = COMCCM_prepostea;
    }

    public String getCOMDCM_codimpICE() {
        return COMDCM_codimpICE;
    }

    public void setCOMDCM_codimpICE(String COMDCM_codimpICE) {
        this.COMDCM_codimpICE = COMDCM_codimpICE;
    }

    public BigDecimal getCOMDCM_valimpICE() {
        return COMDCM_valimpICE;
    }

    public void setCOMDCM_valimpICE(BigDecimal COMDCM_valimpICE) {
        this.COMDCM_valimpICE = COMDCM_valimpICE;
    }
    
}
