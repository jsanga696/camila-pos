/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.model.consultas;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 *
 * @author userdb6
 */
public class Estadistica implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private int venta;
    private int venta_cliente;
    private int anulada;
    private int sesion;
    private int apertura;
    private BigInteger transaciones;
    private BigDecimal valor_venta;
    private BigInteger items_eliminados;
    private int colaboradores;
    private int registrados;
    private int clientes;

    public Estadistica() {
    }

    public int getVenta() {
        return venta;
    }

    public void setVenta(int venta) {
        this.venta = venta;
    }

    public int getVenta_cliente() {
        return venta_cliente;
    }

    public void setVenta_cliente(int venta_cliente) {
        this.venta_cliente = venta_cliente;
    }

    public int getAnulada() {
        return anulada;
    }

    public void setAnulada(int anulada) {
        this.anulada = anulada;
    }

    public int getSesion() {
        return sesion;
    }

    public void setSesion(int sesion) {
        this.sesion = sesion;
    }

    public int getApertura() {
        return apertura;
    }

    public void setApertura(int apertura) {
        this.apertura = apertura;
    }

    public BigInteger getTransaciones() {
        return transaciones;
    }

    public void setTransaciones(BigInteger transaciones) {
        this.transaciones = transaciones;
    }

    public BigDecimal getValor_venta() {
        return valor_venta;
    }

    public void setValor_venta(BigDecimal valor_venta) {
        this.valor_venta = valor_venta;
    }

    public BigInteger getItems_eliminados() {
        return items_eliminados;
    }

    public void setItems_eliminados(BigInteger items_eliminados) {
        this.items_eliminados = items_eliminados;
    }

    public int getColaboradores() {
        return colaboradores;
    }

    public void setColaboradores(int colaboradores) {
        this.colaboradores = colaboradores;
    }

    public int getRegistrados() {
        return registrados;
    }

    public void setRegistrados(int registrados) {
        this.registrados = registrados;
    }

    public int getClientes() {
        return clientes;
    }

    public void setClientes(int clientes) {
        this.clientes = clientes;
    }

    
    
}
