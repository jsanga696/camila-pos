/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.model.consultas;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author userdb6
 */
public class PieModel implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private String categoria;
    private Long dato;

    public PieModel() {
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public Long getDato() {
        return dato;
    }

    public void setDato(Long dato) {
        this.dato = dato;
    }

    @Override
    public String toString() {
        return "PieModel{" + "categoria=" + categoria + ", dato=" + dato + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.categoria);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PieModel other = (PieModel) obj;
        if (!Objects.equals(this.categoria, other.categoria)) {
            return false;
        }
        return true;
    }
    
}
