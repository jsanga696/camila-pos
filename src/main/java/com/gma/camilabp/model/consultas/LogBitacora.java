/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.model.consultas;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

/**
 *
 * @author userdb6
 */
public class LogBitacora implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private String segusu_login;
    private String segusu_abreviado;
    private BigInteger segtur_numero;
    private Date segtur_apertura;
    private String segtur_maquina;
    private Date cbpbit_fecha;
    private String cbpbit_accion;
    private String gencaj_codigo;
    private String cbpbit_detalle;

    public LogBitacora() {
    }

    public String getSegusu_login() {
        return segusu_login;
    }

    public void setSegusu_login(String segusu_login) {
        this.segusu_login = segusu_login;
    }

    public String getSegusu_abreviado() {
        return segusu_abreviado;
    }

    public void setSegusu_abreviado(String segusu_abreviado) {
        this.segusu_abreviado = segusu_abreviado;
    }

    public BigInteger getSegtur_numero() {
        return segtur_numero;
    }

    public void setSegtur_numero(BigInteger segtur_numero) {
        this.segtur_numero = segtur_numero;
    }

    public Date getSegtur_apertura() {
        return segtur_apertura;
    }

    public void setSegtur_apertura(Date segtur_apertura) {
        this.segtur_apertura = segtur_apertura;
    }

    public String getSegtur_maquina() {
        return segtur_maquina;
    }

    public void setSegtur_maquina(String segtur_maquina) {
        this.segtur_maquina = segtur_maquina;
    }

    public Date getCbpbit_fecha() {
        return cbpbit_fecha;
    }

    public void setCbpbit_fecha(Date cbpbit_fecha) {
        this.cbpbit_fecha = cbpbit_fecha;
    }

    public String getCbpbit_accion() {
        return cbpbit_accion;
    }

    public void setCbpbit_accion(String cbpbit_accion) {
        this.cbpbit_accion = cbpbit_accion;
    }

    public String getGencaj_codigo() {
        return gencaj_codigo;
    }

    public void setGencaj_codigo(String gencaj_codigo) {
        this.gencaj_codigo = gencaj_codigo;
    }

    public String getCbpbit_detalle() {
        return cbpbit_detalle;
    }

    public void setCbpbit_detalle(String cbpbit_detalle) {
        this.cbpbit_detalle = cbpbit_detalle;
    }
    
}
