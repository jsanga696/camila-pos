/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.model.consultas;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author userdb6
 */
public class RptCobro implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private String usuario_reporte;
    private int fac_secuencia;
    private int cob_secuencia;
    private String tipo;
    private String entidad;
    private String num_cuenta;
    private String num_documento;
    private BigDecimal valor;
    private int rpt_secuencia;

    public RptCobro() {
    }

    public String getUsuario_reporte() {
        return usuario_reporte;
    }

    public void setUsuario_reporte(String usuario_reporte) {
        this.usuario_reporte = usuario_reporte;
    }

    public int getFac_secuencia() {
        return fac_secuencia;
    }

    public void setFac_secuencia(int fac_secuencia) {
        this.fac_secuencia = fac_secuencia;
    }

    public int getCob_secuencia() {
        return cob_secuencia;
    }

    public void setCob_secuencia(int cob_secuencia) {
        this.cob_secuencia = cob_secuencia;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public String getNum_cuenta() {
        return num_cuenta;
    }

    public void setNum_cuenta(String num_cuenta) {
        this.num_cuenta = num_cuenta;
    }

    public String getNum_documento() {
        return num_documento;
    }

    public void setNum_documento(String num_documento) {
        this.num_documento = num_documento;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public int getRpt_secuencia() {
        return rpt_secuencia;
    }

    public void setRpt_secuencia(int rpt_secuencia) {
        this.rpt_secuencia = rpt_secuencia;
    }
    
}
