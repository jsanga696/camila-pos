/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.model.consultas;

import java.io.Serializable;

/**
 *
 * @author userdb6
 */
public class ResultadoFacturacion implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private String numero;
    private Integer secuencia;
    private Integer secuenciaCxc;

    public ResultadoFacturacion() {
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Integer getSecuencia() {
        return secuencia;
    }

    public void setSecuencia(Integer secuencia) {
        this.secuencia = secuencia;
    }

    public Integer getSecuenciaCxc() {
        return secuenciaCxc;
    }

    public void setSecuenciaCxc(Integer secuenciaCxc) {
        this.secuenciaCxc = secuenciaCxc;
    }

}
