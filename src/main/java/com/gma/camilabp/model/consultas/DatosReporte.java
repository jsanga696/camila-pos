/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.model.consultas;

import java.io.Serializable;
import java.util.Map;

/**
 *
 * @author userdb6
 */
public class DatosReporte implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private String nombreReporte;
    private Map parametros;

    public DatosReporte() {
    }

    public String getNombreReporte() {
        return nombreReporte;
    }

    public void setNombreReporte(String nombreReporte) {
        this.nombreReporte = nombreReporte;
    }

    public Map getParametros() {
        return parametros;
    }

    public void setParametros(Map parametros) {
        this.parametros = parametros;
    }
    
    
}
