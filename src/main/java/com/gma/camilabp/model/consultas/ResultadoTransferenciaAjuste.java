/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.model.consultas;

import java.io.Serializable;

/**
 *
 * @author Acer
 */
public class ResultadoTransferenciaAjuste implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private Integer invcotNumsecuenc;
    private Integer invcotNumdocumen;
    private Integer invNumbodega;

    public ResultadoTransferenciaAjuste() {
    }

    public Integer getInvcotNumsecuenc() {
        return invcotNumsecuenc;
    }

    public void setInvcotNumsecuenc(Integer invcotNumsecuenc) {
        this.invcotNumsecuenc = invcotNumsecuenc;
    }

    public Integer getInvcotNumdocumen() {
        return invcotNumdocumen;
    }

    public void setInvcotNumdocumen(Integer invcotNumdocumen) {
        this.invcotNumdocumen = invcotNumdocumen;
    }

    public Integer getInvNumbodega() {
        return invNumbodega;
    }

    public void setInvNumbodega(Integer invNumbodega) {
        this.invNumbodega = invNumbodega;
    }
    
}
