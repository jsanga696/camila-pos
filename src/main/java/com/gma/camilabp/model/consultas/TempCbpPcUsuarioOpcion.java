/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.model.consultas;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author userdb6
 */
public class TempCbpPcUsuarioOpcion implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private Integer idSecuencia;
    private Integer faccpdNumdocumen=0;
    private Integer faccpdNumsectrans=0;
    private Integer idPedidos=0;
    private Integer idLineaPed=0;
    private String genciaCodigo;
    private String genofiCodigo;
    private Date faccpdFechaemisi;
    private String facvdrTiporeg="P";
    private String cxccliCodigo;
    private String facvdrCodigo;
    private String cxcrutCodigo;
    private String faccpdEtipoped="PTO";
    private String inviniCodigo;
    private String invitmCodigo;
    private String facdvtDetallefactserv;
    private BigDecimal facdpdCantfuncional;
    private BigDecimal facdpdPorcendscto;
    private BigDecimal facdpdPrecio;
    private BigDecimal facdpdSubtotal;
    private BigDecimal facdpdDescuento= BigDecimal.ZERO;
    private BigDecimal facdpdImpuesto;
    private BigDecimal facdpdNeto;
    private String facdpdProveedor;
    private String facdpdListaprecio;
    private String conplcCodigo;
    private String cxcfpgCodigo;
    private String cxctpgCodigo;
    private String impuesto;
    private BigDecimal invitmPeso;
    private BigDecimal invitmVolumen;
    private BigDecimal invitmUnidembala;
    private String nombreItem;

    public TempCbpPcUsuarioOpcion() {
    }

    public TempCbpPcUsuarioOpcion(Integer idSecuencia) {
        this.idSecuencia = idSecuencia;
    }

    public TempCbpPcUsuarioOpcion(Integer idSecuencia, String genciaCodigo, String genofiCodigo, Date faccpdFechaemisi, 
                            String cxccliCodigo, String facvdrCodigo, String invitmCodigo, BigDecimal facdpdCantfuncional, 
                            BigDecimal facdpdPrecio, String nombreItem, String facdpdListaprecio, String cxcrutCodigo,
                            BigDecimal invitmPeso,BigDecimal invitmVolumen,BigDecimal invitmUnidembala) {
        this.idSecuencia = idSecuencia;
        this.genciaCodigo = genciaCodigo;
        this.genofiCodigo = genofiCodigo;
        this.faccpdFechaemisi = faccpdFechaemisi;
        this.cxccliCodigo = cxccliCodigo;
        this.facvdrCodigo = facvdrCodigo;
        this.invitmCodigo = invitmCodigo;
        this.facdpdCantfuncional = facdpdCantfuncional;
        this.facdpdPrecio = facdpdPrecio;
        this.nombreItem= nombreItem;
        this.facdpdListaprecio= facdpdListaprecio;
        this.cxcrutCodigo= cxcrutCodigo;
        this.invitmPeso = invitmPeso;
        this.invitmVolumen = invitmVolumen;
        this.invitmUnidembala = invitmUnidembala;
    }        

    public Integer getIdSecuencia() {
        return idSecuencia;
    }

    public void setIdSecuencia(Integer idSecuencia) {
        this.idSecuencia = idSecuencia;
    }

    public Integer getFaccpdNumdocumen() {
        return faccpdNumdocumen;
    }

    public void setFaccpdNumdocumen(Integer faccpdNumdocumen) {
        this.faccpdNumdocumen = faccpdNumdocumen;
    }

    public Integer getFaccpdNumsectrans() {
        return faccpdNumsectrans;
    }

    public void setFaccpdNumsectrans(Integer faccpdNumsectrans) {
        this.faccpdNumsectrans = faccpdNumsectrans;
    }

    public Integer getIdPedidos() {
        return idPedidos;
    }

    public void setIdPedidos(Integer idPedidos) {
        this.idPedidos = idPedidos;
    }

    public Integer getIdLineaPed() {
        return idLineaPed;
    }

    public void setIdLineaPed(Integer idLineaPed) {
        this.idLineaPed = idLineaPed;
    }

    public String getGenciaCodigo() {
        return genciaCodigo;
    }

    public void setGenciaCodigo(String genciaCodigo) {
        this.genciaCodigo = genciaCodigo;
    }

    public String getGenofiCodigo() {
        return genofiCodigo;
    }

    public void setGenofiCodigo(String genofiCodigo) {
        this.genofiCodigo = genofiCodigo;
    }

    public Date getFaccpdFechaemisi() {
        return faccpdFechaemisi;
    }

    public void setFaccpdFechaemisi(Date faccpdFechaemisi) {
        this.faccpdFechaemisi = faccpdFechaemisi;
    }

    public String getFacvdrTiporeg() {
        return facvdrTiporeg;
    }

    public void setFacvdrTiporeg(String facvdrTiporeg) {
        this.facvdrTiporeg = facvdrTiporeg;
    }

    public String getCxccliCodigo() {
        return cxccliCodigo;
    }

    public void setCxccliCodigo(String cxccliCodigo) {
        this.cxccliCodigo = cxccliCodigo;
    }

    public String getFacvdrCodigo() {
        return facvdrCodigo;
    }

    public void setFacvdrCodigo(String facvdrCodigo) {
        this.facvdrCodigo = facvdrCodigo;
    }

    public String getCxcrutCodigo() {
        return cxcrutCodigo;
    }

    public void setCxcrutCodigo(String cxcrutCodigo) {
        this.cxcrutCodigo = cxcrutCodigo;
    }

    public String getFaccpdEtipoped() {
        return faccpdEtipoped;
    }

    public void setFaccpdEtipoped(String faccpdEtipoped) {
        this.faccpdEtipoped = faccpdEtipoped;
    }

    public String getInviniCodigo() {
        return inviniCodigo;
    }

    public void setInviniCodigo(String inviniCodigo) {
        this.inviniCodigo = inviniCodigo;
    }

    public String getInvitmCodigo() {
        return invitmCodigo;
    }

    public void setInvitmCodigo(String invitmCodigo) {
        this.invitmCodigo = invitmCodigo;
    }

    public String getFacdvtDetallefactserv() {
        return facdvtDetallefactserv;
    }

    public void setFacdvtDetallefactserv(String facdvtDetallefactserv) {
        this.facdvtDetallefactserv = facdvtDetallefactserv;
    }

    public BigDecimal getFacdpdCantfuncional() {
        return facdpdCantfuncional;
    }

    public void setFacdpdCantfuncional(BigDecimal facdpdCantfuncional) {
        this.facdpdCantfuncional = facdpdCantfuncional;
    }

    public BigDecimal getFacdpdPorcendscto() {
        return facdpdPorcendscto;
    }

    public void setFacdpdPorcendscto(BigDecimal facdpdPorcendscto) {
        this.facdpdPorcendscto = facdpdPorcendscto;
    }

    public BigDecimal getFacdpdPrecio() {
        return facdpdPrecio;
    }

    public void setFacdpdPrecio(BigDecimal facdpdPrecio) {
        this.facdpdPrecio = facdpdPrecio;
    }

    public BigDecimal getFacdpdSubtotal() {
        return facdpdSubtotal;
    }

    public void setFacdpdSubtotal(BigDecimal facdpdSubtotal) {
        this.facdpdSubtotal = facdpdSubtotal;
    }

    public BigDecimal getFacdpdDescuento() {
        return facdpdDescuento;
    }

    public void setFacdpdDescuento(BigDecimal facdpdDescuento) {
        this.facdpdDescuento = facdpdDescuento;
    }

    public BigDecimal getFacdpdImpuesto() {
        return facdpdImpuesto;
    }

    public void setFacdpdImpuesto(BigDecimal facdpdImpuesto) {
        this.facdpdImpuesto = facdpdImpuesto;
    }

    public BigDecimal getFacdpdNeto() {
        return facdpdNeto;
    }

    public void setFacdpdNeto(BigDecimal facdpdNeto) {
        this.facdpdNeto = facdpdNeto;
    }

    public String getFacdpdProveedor() {
        return facdpdProveedor;
    }

    public void setFacdpdProveedor(String facdpdProveedor) {
        this.facdpdProveedor = facdpdProveedor;
    }

    public String getFacdpdListaprecio() {
        return facdpdListaprecio;
    }

    public void setFacdpdListaprecio(String facdpdListaprecio) {
        this.facdpdListaprecio = facdpdListaprecio;
    }

    public String getConplcCodigo() {
        return conplcCodigo;
    }

    public void setConplcCodigo(String conplcCodigo) {
        this.conplcCodigo = conplcCodigo;
    }

    public String getCxcfpgCodigo() {
        return cxcfpgCodigo;
    }

    public void setCxcfpgCodigo(String cxcfpgCodigo) {
        this.cxcfpgCodigo = cxcfpgCodigo;
    }

    public String getCxctpgCodigo() {
        return cxctpgCodigo;
    }

    public void setCxctpgCodigo(String cxctpgCodigo) {
        this.cxctpgCodigo = cxctpgCodigo;
    }

    public String getImpuesto() {
        return impuesto;
    }

    public void setImpuesto(String impuesto) {
        this.impuesto = impuesto;
    }

    public BigDecimal getInvitmPeso() {
        return invitmPeso;
    }

    public void setInvitmPeso(BigDecimal invitmPeso) {
        this.invitmPeso = invitmPeso;
    }

    public BigDecimal getInvitmVolumen() {
        return invitmVolumen;
    }

    public void setInvitmVolumen(BigDecimal invitmVolumen) {
        this.invitmVolumen = invitmVolumen;
    }

    public BigDecimal getInvitmUnidembala() {
        return invitmUnidembala;
    }

    public void setInvitmUnidembala(BigDecimal invitmUnidembala) {
        this.invitmUnidembala = invitmUnidembala;
    }

    public String getNombreItem() {
        return nombreItem;
    }

    public void setNombreItem(String nombreItem) {
        this.nombreItem = nombreItem;
    }

    @Override
    public String toString() {
        return "TempCbpPcUsuarioOpcion{" + "idSecuencia=" + idSecuencia + '}';
    }
    
}
