/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.model.consultas;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.Transient;

/**
 *
 * @author userdb5
 */
public class TarjCredModel implements Serializable{
    
    private static final long serialVersionUID = 1L;
    
    private String GENCIA_codigo;
    private Integer CXCDPG_numerosec;
    private Integer CXCDPG_numlinea;
    private String GENOFI_codigo;
    private String GENMOD_codigocr;
    private String GENTDO_codigocr;
    private String CXCCCR_numdocumcr;
    private String GENBAN_tipo;
    private String GENBAN_codigo;
    private String CXCCLI_codigo;
    private Date CXCDPG_fecha;
    private String CXCFPG_codigo;
    private String GENMON_codigo;
    private BigDecimal CXCDPG_cotiza; 
    private Date CXCDPG_fechavcto;
    private BigDecimal CXCDPG_valormov;
    private String CXCDPG_cuentacte;
    private String CXCDPG_numerochq;
    private Date CXCDPG_fechadepos;
    private String CXCDPG_estcheque;
    private String CXCDPG_secubanco;
    private String CXCDPG_secubancodep;
    
    @Transient
    private String nombreBanco;
    
    @Transient
    private String nombreCliente;
    
    @Transient
    private BigDecimal valorTotalAlDeposito;
    
    @Transient
    private BigDecimal valorComision;
    
    public TarjCredModel(){
        valorTotalAlDeposito = valorComision = BigDecimal.ZERO;
    }

    public String getGENCIA_codigo() {
        return GENCIA_codigo;
    }

    public void setGENCIA_codigo(String GENCIA_codigo) {
        this.GENCIA_codigo = GENCIA_codigo;
    }

    public Integer getCXCDPG_numerosec() {
        return CXCDPG_numerosec;
    }

    public void setCXCDPG_numerosec(Integer CXCDPG_numerosec) {
        this.CXCDPG_numerosec = CXCDPG_numerosec;
    }

    public Integer getCXCDPG_numlinea() {
        return CXCDPG_numlinea;
    }

    public void setCXCDPG_numlinea(Integer CXCDPG_numlinea) {
        this.CXCDPG_numlinea = CXCDPG_numlinea;
    }

    public String getGENOFI_codigo() {
        return GENOFI_codigo;
    }

    public void setGENOFI_codigo(String GENOFI_codigo) {
        this.GENOFI_codigo = GENOFI_codigo;
    }

    public String getGENMOD_codigocr() {
        return GENMOD_codigocr;
    }

    public void setGENMOD_codigocr(String GENMOD_codigocr) {
        this.GENMOD_codigocr = GENMOD_codigocr;
    }

    public String getGENTDO_codigocr() {
        return GENTDO_codigocr;
    }

    public void setGENTDO_codigocr(String GENTDO_codigocr) {
        this.GENTDO_codigocr = GENTDO_codigocr;
    }

    public String getCXCCCR_numdocumcr() {
        return CXCCCR_numdocumcr;
    }

    public void setCXCCCR_numdocumcr(String CXCCCR_numdocumcr) {
        this.CXCCCR_numdocumcr = CXCCCR_numdocumcr;
    }

    public String getGENBAN_tipo() {
        return GENBAN_tipo;
    }

    public void setGENBAN_tipo(String GENBAN_tipo) {
        this.GENBAN_tipo = GENBAN_tipo;
    }

    public String getGENBAN_codigo() {
        return GENBAN_codigo;
    }

    public void setGENBAN_codigo(String GENBAN_codigo) {
        this.GENBAN_codigo = GENBAN_codigo;
    }

    public String getCXCCLI_codigo() {
        return CXCCLI_codigo;
    }

    public void setCXCCLI_codigo(String CXCCLI_codigo) {
        this.CXCCLI_codigo = CXCCLI_codigo;
    }

    public String getCXCDPG_fecha() {
        if(CXCDPG_fecha != null){
            SimpleDateFormat dt = new SimpleDateFormat("dd/MM/yyyy"); 
            return dt.format(CXCDPG_fecha);
        }
        return "";
    }

    public void setCXCDPG_fecha(Date CXCDPG_fecha) {
        this.CXCDPG_fecha = CXCDPG_fecha;
    }

    public String getCXCFPG_codigo() {
        return CXCFPG_codigo;
    }

    public void setCXCFPG_codigo(String CXCFPG_codigo) {
        this.CXCFPG_codigo = CXCFPG_codigo;
    }

    public String getGENMON_codigo() {
        return GENMON_codigo;
    }

    public void setGENMON_codigo(String GENMON_codigo) {
        this.GENMON_codigo = GENMON_codigo;
    }

    public BigDecimal getCXCDPG_cotiza() {
        return CXCDPG_cotiza;
    }

    public void setCXCDPG_cotiza(BigDecimal CXCDPG_cotiza) {
        this.CXCDPG_cotiza = CXCDPG_cotiza;
    }

    public Date getCXCDPG_fechavcto() {
        return CXCDPG_fechavcto;
    }

    public void setCXCDPG_fechavcto(Date CXCDPG_fechavcto) {
        this.CXCDPG_fechavcto = CXCDPG_fechavcto;
    }

    public BigDecimal getCXCDPG_valormov() {
        return CXCDPG_valormov;
    }

    public void setCXCDPG_valormov(BigDecimal CXCDPG_valormov) {
        this.CXCDPG_valormov = CXCDPG_valormov;
    }

    public String getCXCDPG_cuentacte() {
        return CXCDPG_cuentacte;
    }

    public void setCXCDPG_cuentacte(String CXCDPG_cuentacte) {
        this.CXCDPG_cuentacte = CXCDPG_cuentacte;
    }

    public String getCXCDPG_numerochq() {
        return CXCDPG_numerochq;
    }

    public void setCXCDPG_numerochq(String CXCDPG_numerochq) {
        this.CXCDPG_numerochq = CXCDPG_numerochq;
    }

    public Date getCXCDPG_fechadepos() {
        return CXCDPG_fechadepos;
    }

    public void setCXCDPG_fechadepos(Date CXCDPG_fechadepos) {
        this.CXCDPG_fechadepos = CXCDPG_fechadepos;
    }

    public String getCXCDPG_estcheque() {
        return CXCDPG_estcheque;
    }

    public void setCXCDPG_estcheque(String CXCDPG_estcheque) {
        this.CXCDPG_estcheque = CXCDPG_estcheque;
    }

    public String getCXCDPG_secubanco() {
        return CXCDPG_secubanco;
    }

    public void setCXCDPG_secubanco(String CXCDPG_secubanco) {
        this.CXCDPG_secubanco = CXCDPG_secubanco;
    }

    public String getCXCDPG_secubancodep() {
        return CXCDPG_secubancodep;
    }

    public void setCXCDPG_secubancodep(String CXCDPG_secubancodep) {
        this.CXCDPG_secubancodep = CXCDPG_secubancodep;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public BigDecimal getValorTotalAlDeposito() {
        valorTotalAlDeposito = this.CXCDPG_valormov.subtract(valorComision);
        return valorTotalAlDeposito;
    }

    public void setValorTotalAlDeposito(BigDecimal valorTotalAlDeposito) {
        this.valorTotalAlDeposito = valorTotalAlDeposito;
    }

    public BigDecimal getValorComision() {
        return valorComision;
    }

    public void setValorComision(BigDecimal valorComision) {
        this.valorComision = valorComision;
    }

    public String getNombreBanco() {
        return nombreBanco;
    }

    public void setNombreBanco(String nombreBanco) {
        this.nombreBanco = nombreBanco;
    }
    
}
