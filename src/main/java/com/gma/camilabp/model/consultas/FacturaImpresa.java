/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.model.consultas;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Acer
 */
public class FacturaImpresa implements Serializable {

    private static final long serialVersionUID = 1L;

    private String GENOFI_codigo;
    private int FACCVT_numdocumen;
    private int FACCVT_numrelplan;
    private Date FACCVT_fechaemisi;
    private String FACVDR_codigo;
    private String CXCCLI_codigo;
    private String CXCTPG_codigo;
    private String CXCCLI_contacto;
    private String CXCCLI_razonsocia;
    private String CXCCLI_ruc_ci;
    private String CXCCLI_direccion1;
    private String CXCCLI_telefono;
    private String CXCRUT_codigo;
    private String FACVDR_nombre;
    private String FACCVT_codrutavta;
    private Date FECHA_VENCIMIENTO;
    private int DIAS_PLAZO;
    private String PROVINCIA;
    private String CIUDAD;
    private String INVITM_codigo;
    private String INVITM_nombre;
    private int FACDVT_cantifunci;
    private int FACDVT_cajas;
    private int FACDVT_unidades;
    private String INVUPR_unibasvent;
    private String INVUPR_unirelvent;
    private BigDecimal FACDVT_subtotamov;
    private BigDecimal FACDVT_desctomov;
    private BigDecimal FACDVT_porc_iva;
    private BigDecimal FACDVT_totimpumov;
    private BigDecimal FACDVT_netomov;
    private BigDecimal INVITM_preciopvp;
    private int FACDVT_unidembala;
    private String FACDVT_tipodetall;
    private BigDecimal FACDVT_totalpeso;
    private int FACDVT_linea;
    private BigDecimal INVITM_preciolista;
    private BigDecimal INVITM_precioneto;
    private String INVCHO_nombre;
    private String INVCHO_rucci;
    private String INVCAM_placa;
    private int FACCVT_numpedido;
    private String CXCCDI_codigo;
    private String CXCCDI_nombre;
    private String FACSUP_nombre;
    private String CXCN6_nombre;
    private String CXCN7_nombre;
    private Date FACCPD_fechaemisi;
    private BigDecimal FACDVT_BaseGrabada;
    private BigDecimal FACDVT_BaseExcenta;
    private int FACDVT_cajasvta;
    private String FACDVT_unidadvta;
    private int FACCVT_numsecuenc;
    private String GENUSU_codigo;
    private String GENMAQ_codigo;
    private String FACDVT_nombrecorto;
    private BigDecimal FACDVT_desctoporcen;
    private BigDecimal FACDVT_dsctopromocional;
    private BigDecimal FACDVT_netolineal;
    private BigDecimal FACDVT_cantimpri;
    private String FACDVT_unidimpri;
    private String GENOFI_nombre;
    private String GENOFI_direccionMatriz;
    private String GENOFI_direccionSucursal;
    private String GENOFI_resulusion;
    private String GENOFI_llevacontabilidad;
    private String FACCVT_numeroFactura;
    private String GENOFI_gloza;
    private String GENOFI_gloza2;
    private BigDecimal FACDVT_BaseImponible;
    private String GENOFI_nombreComercial;
    private String FACDVT_LLevaContabilidad;
    private String FACDVT_portalweb;
    private String GENCIA_ruc;
    private String FACCVT_autorizacion;
    private String FACCVT_fechaautoriza;
    private String FACCVT_claveacceso;
    private String CXCFPG_codigo;
    private String FACCVT_horaautoriza;
    private BigDecimal FACDVT_dsctoprecio;
    private String FACCPD_ordencompra;
    private String FACCPD_codalmacen;
    private int FACCPD_secuenciacli;
    private String FACCPD_montoletra;
    private String GENOFI_telefono;

    public FacturaImpresa() {
    }

    public String getGENOFI_codigo() {
        return GENOFI_codigo;
    }

    public void setGENOFI_codigo(String GENOFI_codigo) {
        this.GENOFI_codigo = GENOFI_codigo;
    }

    public int getFACCVT_numdocumen() {
        return FACCVT_numdocumen;
    }

    public void setFACCVT_numdocumen(int FACCVT_numdocumen) {
        this.FACCVT_numdocumen = FACCVT_numdocumen;
    }

    public int getFACCVT_numrelplan() {
        return FACCVT_numrelplan;
    }

    public void setFACCVT_numrelplan(int FACCVT_numrelplan) {
        this.FACCVT_numrelplan = FACCVT_numrelplan;
    }

    public Date getFACCVT_fechaemisi() {
        return FACCVT_fechaemisi;
    }

    public void setFACCVT_fechaemisi(Date FACCVT_fechaemisi) {
        this.FACCVT_fechaemisi = FACCVT_fechaemisi;
    }

    public String getFACVDR_codigo() {
        return FACVDR_codigo;
    }

    public void setFACVDR_codigo(String FACVDR_codigo) {
        this.FACVDR_codigo = FACVDR_codigo;
    }

    public String getCXCCLI_codigo() {
        return CXCCLI_codigo;
    }

    public void setCXCCLI_codigo(String CXCCLI_codigo) {
        this.CXCCLI_codigo = CXCCLI_codigo;
    }

    public String getCXCTPG_codigo() {
        return CXCTPG_codigo;
    }

    public void setCXCTPG_codigo(String CXCTPG_codigo) {
        this.CXCTPG_codigo = CXCTPG_codigo;
    }

    public String getCXCCLI_contacto() {
        return CXCCLI_contacto;
    }

    public void setCXCCLI_contacto(String CXCCLI_contacto) {
        this.CXCCLI_contacto = CXCCLI_contacto;
    }

    public String getCXCCLI_razonsocia() {
        return CXCCLI_razonsocia;
    }

    public void setCXCCLI_razonsocia(String CXCCLI_razonsocia) {
        this.CXCCLI_razonsocia = CXCCLI_razonsocia;
    }

    public String getCXCCLI_ruc_ci() {
        return CXCCLI_ruc_ci;
    }

    public void setCXCCLI_ruc_ci(String CXCCLI_ruc_ci) {
        this.CXCCLI_ruc_ci = CXCCLI_ruc_ci;
    }

    public String getCXCCLI_direccion1() {
        return CXCCLI_direccion1;
    }

    public void setCXCCLI_direccion1(String CXCCLI_direccion1) {
        this.CXCCLI_direccion1 = CXCCLI_direccion1;
    }

    public String getCXCCLI_telefono() {
        return CXCCLI_telefono;
    }

    public void setCXCCLI_telefono(String CXCCLI_telefono) {
        this.CXCCLI_telefono = CXCCLI_telefono;
    }

    public String getCXCRUT_codigo() {
        return CXCRUT_codigo;
    }

    public void setCXCRUT_codigo(String CXCRUT_codigo) {
        this.CXCRUT_codigo = CXCRUT_codigo;
    }

    public String getFACVDR_nombre() {
        return FACVDR_nombre;
    }

    public void setFACVDR_nombre(String FACVDR_nombre) {
        this.FACVDR_nombre = FACVDR_nombre;
    }

    public String getFACCVT_codrutavta() {
        return FACCVT_codrutavta;
    }

    public void setFACCVT_codrutavta(String FACCVT_codrutavta) {
        this.FACCVT_codrutavta = FACCVT_codrutavta;
    }

    public Date getFECHA_VENCIMIENTO() {
        return FECHA_VENCIMIENTO;
    }

    public void setFECHA_VENCIMIENTO(Date FECHA_VENCIMIENTO) {
        this.FECHA_VENCIMIENTO = FECHA_VENCIMIENTO;
    }

    public int getDIAS_PLAZO() {
        return DIAS_PLAZO;
    }

    public void setDIAS_PLAZO(int DIAS_PLAZO) {
        this.DIAS_PLAZO = DIAS_PLAZO;
    }

    public String getPROVINCIA() {
        return PROVINCIA;
    }

    public void setPROVINCIA(String PROVINCIA) {
        this.PROVINCIA = PROVINCIA;
    }

    public String getCIUDAD() {
        return CIUDAD;
    }

    public void setCIUDAD(String CIUDAD) {
        this.CIUDAD = CIUDAD;
    }

    public String getINVITM_codigo() {
        return INVITM_codigo;
    }

    public void setINVITM_codigo(String INVITM_codigo) {
        this.INVITM_codigo = INVITM_codigo;
    }

    public String getINVITM_nombre() {
        return INVITM_nombre;
    }

    public void setINVITM_nombre(String INVITM_nombre) {
        this.INVITM_nombre = INVITM_nombre;
    }

    public int getFACDVT_cantifunci() {
        return FACDVT_cantifunci;
    }

    public void setFACDVT_cantifunci(int FACDVT_cantifunci) {
        this.FACDVT_cantifunci = FACDVT_cantifunci;
    }

    public int getFACDVT_cajas() {
        return FACDVT_cajas;
    }

    public void setFACDVT_cajas(int FACDVT_cajas) {
        this.FACDVT_cajas = FACDVT_cajas;
    }

    public int getFACDVT_unidades() {
        return FACDVT_unidades;
    }

    public void setFACDVT_unidades(int FACDVT_unidades) {
        this.FACDVT_unidades = FACDVT_unidades;
    }

    public String getINVUPR_unibasvent() {
        return INVUPR_unibasvent;
    }

    public void setINVUPR_unibasvent(String INVUPR_unibasvent) {
        this.INVUPR_unibasvent = INVUPR_unibasvent;
    }

    public String getINVUPR_unirelvent() {
        return INVUPR_unirelvent;
    }

    public void setINVUPR_unirelvent(String INVUPR_unirelvent) {
        this.INVUPR_unirelvent = INVUPR_unirelvent;
    }

    public BigDecimal getFACDVT_subtotamov() {
        return FACDVT_subtotamov;
    }

    public void setFACDVT_subtotamov(BigDecimal FACDVT_subtotamov) {
        this.FACDVT_subtotamov = FACDVT_subtotamov;
    }

    public BigDecimal getFACDVT_desctomov() {
        return FACDVT_desctomov;
    }

    public void setFACDVT_desctomov(BigDecimal FACDVT_desctomov) {
        this.FACDVT_desctomov = FACDVT_desctomov;
    }

    public BigDecimal getFACDVT_porc_iva() {
        return FACDVT_porc_iva;
    }

    public void setFACDVT_porc_iva(BigDecimal FACDVT_porc_iva) {
        this.FACDVT_porc_iva = FACDVT_porc_iva;
    }

    public BigDecimal getFACDVT_totimpumov() {
        return FACDVT_totimpumov;
    }

    public void setFACDVT_totimpumov(BigDecimal FACDVT_totimpumov) {
        this.FACDVT_totimpumov = FACDVT_totimpumov;
    }

    public BigDecimal getFACDVT_netomov() {
        return FACDVT_netomov;
    }

    public void setFACDVT_netomov(BigDecimal FACDVT_netomov) {
        this.FACDVT_netomov = FACDVT_netomov;
    }

    public BigDecimal getINVITM_preciopvp() {
        return INVITM_preciopvp;
    }

    public void setINVITM_preciopvp(BigDecimal INVITM_preciopvp) {
        this.INVITM_preciopvp = INVITM_preciopvp;
    }

    public int getFACDVT_unidembala() {
        return FACDVT_unidembala;
    }

    public void setFACDVT_unidembala(int FACDVT_unidembala) {
        this.FACDVT_unidembala = FACDVT_unidembala;
    }

    public String getFACDVT_tipodetall() {
        return FACDVT_tipodetall;
    }

    public void setFACDVT_tipodetall(String FACDVT_tipodetall) {
        this.FACDVT_tipodetall = FACDVT_tipodetall;
    }

    public BigDecimal getFACDVT_totalpeso() {
        return FACDVT_totalpeso;
    }

    public void setFACDVT_totalpeso(BigDecimal FACDVT_totalpeso) {
        this.FACDVT_totalpeso = FACDVT_totalpeso;
    }

    public int getFACDVT_linea() {
        return FACDVT_linea;
    }

    public void setFACDVT_linea(int FACDVT_linea) {
        this.FACDVT_linea = FACDVT_linea;
    }

    public BigDecimal getINVITM_preciolista() {
        return INVITM_preciolista;
    }

    public void setINVITM_preciolista(BigDecimal INVITM_preciolista) {
        this.INVITM_preciolista = INVITM_preciolista;
    }

    public BigDecimal getINVITM_precioneto() {
        return INVITM_precioneto;
    }

    public void setINVITM_precioneto(BigDecimal INVITM_precioneto) {
        this.INVITM_precioneto = INVITM_precioneto;
    }

    public String getINVCHO_nombre() {
        return INVCHO_nombre;
    }

    public void setINVCHO_nombre(String INVCHO_nombre) {
        this.INVCHO_nombre = INVCHO_nombre;
    }

    public String getINVCHO_rucci() {
        return INVCHO_rucci;
    }

    public void setINVCHO_rucci(String INVCHO_rucci) {
        this.INVCHO_rucci = INVCHO_rucci;
    }

    public String getINVCAM_placa() {
        return INVCAM_placa;
    }

    public void setINVCAM_placa(String INVCAM_placa) {
        this.INVCAM_placa = INVCAM_placa;
    }

    public int getFACCVT_numpedido() {
        return FACCVT_numpedido;
    }

    public void setFACCVT_numpedido(int FACCVT_numpedido) {
        this.FACCVT_numpedido = FACCVT_numpedido;
    }

    public String getCXCCDI_codigo() {
        return CXCCDI_codigo;
    }

    public void setCXCCDI_codigo(String CXCCDI_codigo) {
        this.CXCCDI_codigo = CXCCDI_codigo;
    }

    public String getCXCCDI_nombre() {
        return CXCCDI_nombre;
    }

    public void setCXCCDI_nombre(String CXCCDI_nombre) {
        this.CXCCDI_nombre = CXCCDI_nombre;
    }

    public String getFACSUP_nombre() {
        return FACSUP_nombre;
    }

    public void setFACSUP_nombre(String FACSUP_nombre) {
        this.FACSUP_nombre = FACSUP_nombre;
    }

    public String getCXCN6_nombre() {
        return CXCN6_nombre;
    }

    public void setCXCN6_nombre(String CXCN6_nombre) {
        this.CXCN6_nombre = CXCN6_nombre;
    }

    public String getCXCN7_nombre() {
        return CXCN7_nombre;
    }

    public void setCXCN7_nombre(String CXCN7_nombre) {
        this.CXCN7_nombre = CXCN7_nombre;
    }

    public Date getFACCPD_fechaemisi() {
        return FACCPD_fechaemisi;
    }

    public void setFACCPD_fechaemisi(Date FACCPD_fechaemisi) {
        this.FACCPD_fechaemisi = FACCPD_fechaemisi;
    }

    public BigDecimal getFACDVT_BaseGrabada() {
        return FACDVT_BaseGrabada;
    }

    public void setFACDVT_BaseGrabada(BigDecimal FACDVT_BaseGrabada) {
        this.FACDVT_BaseGrabada = FACDVT_BaseGrabada;
    }

    public BigDecimal getFACDVT_BaseExcenta() {
        return FACDVT_BaseExcenta;
    }

    public void setFACDVT_BaseExcenta(BigDecimal FACDVT_BaseExcenta) {
        this.FACDVT_BaseExcenta = FACDVT_BaseExcenta;
    }

    public int getFACDVT_cajasvta() {
        return FACDVT_cajasvta;
    }

    public void setFACDVT_cajasvta(int FACDVT_cajasvta) {
        this.FACDVT_cajasvta = FACDVT_cajasvta;
    }

    public String getFACDVT_unidadvta() {
        return FACDVT_unidadvta;
    }

    public void setFACDVT_unidadvta(String FACDVT_unidadvta) {
        this.FACDVT_unidadvta = FACDVT_unidadvta;
    }

    public int getFACCVT_numsecuenc() {
        return FACCVT_numsecuenc;
    }

    public void setFACCVT_numsecuenc(int FACCVT_numsecuenc) {
        this.FACCVT_numsecuenc = FACCVT_numsecuenc;
    }

    public String getGENUSU_codigo() {
        return GENUSU_codigo;
    }

    public void setGENUSU_codigo(String GENUSU_codigo) {
        this.GENUSU_codigo = GENUSU_codigo;
    }

    public String getGENMAQ_codigo() {
        return GENMAQ_codigo;
    }

    public void setGENMAQ_codigo(String GENMAQ_codigo) {
        this.GENMAQ_codigo = GENMAQ_codigo;
    }

    public String getFACDVT_nombrecorto() {
        return FACDVT_nombrecorto;
    }

    public void setFACDVT_nombrecorto(String FACDVT_nombrecorto) {
        this.FACDVT_nombrecorto = FACDVT_nombrecorto;
    }

    public BigDecimal getFACDVT_desctoporcen() {
        return FACDVT_desctoporcen;
    }

    public void setFACDVT_desctoporcen(BigDecimal FACDVT_desctoporcen) {
        this.FACDVT_desctoporcen = FACDVT_desctoporcen;
    }

    public BigDecimal getFACDVT_dsctopromocional() {
        return FACDVT_dsctopromocional;
    }

    public void setFACDVT_dsctopromocional(BigDecimal FACDVT_dsctopromocional) {
        this.FACDVT_dsctopromocional = FACDVT_dsctopromocional;
    }

    public BigDecimal getFACDVT_netolineal() {
        return FACDVT_netolineal;
    }

    public void setFACDVT_netolineal(BigDecimal FACDVT_netolineal) {
        this.FACDVT_netolineal = FACDVT_netolineal;
    }

    public BigDecimal getFACDVT_cantimpri() {
        return FACDVT_cantimpri;
    }

    public void setFACDVT_cantimpri(BigDecimal FACDVT_cantimpri) {
        this.FACDVT_cantimpri = FACDVT_cantimpri;
    }

    public String getFACDVT_unidimpri() {
        return FACDVT_unidimpri;
    }

    public void setFACDVT_unidimpri(String FACDVT_unidimpri) {
        this.FACDVT_unidimpri = FACDVT_unidimpri;
    }

    public String getGENOFI_nombre() {
        return GENOFI_nombre;
    }

    public void setGENOFI_nombre(String GENOFI_nombre) {
        this.GENOFI_nombre = GENOFI_nombre;
    }

    public String getGENOFI_direccionMatriz() {
        return GENOFI_direccionMatriz;
    }

    public void setGENOFI_direccionMatriz(String GENOFI_direccionMatriz) {
        this.GENOFI_direccionMatriz = GENOFI_direccionMatriz;
    }

    public String getGENOFI_direccionSucursal() {
        return GENOFI_direccionSucursal;
    }

    public void setGENOFI_direccionSucursal(String GENOFI_direccionSucursal) {
        this.GENOFI_direccionSucursal = GENOFI_direccionSucursal;
    }

    public String getGENOFI_resulusion() {
        return GENOFI_resulusion;
    }

    public void setGENOFI_resulusion(String GENOFI_resulusion) {
        this.GENOFI_resulusion = GENOFI_resulusion;
    }

    public String getGENOFI_llevacontabilidad() {
        return GENOFI_llevacontabilidad;
    }

    public void setGENOFI_llevacontabilidad(String GENOFI_llevacontabilidad) {
        this.GENOFI_llevacontabilidad = GENOFI_llevacontabilidad;
    }

    public String getFACCVT_numeroFactura() {
        return FACCVT_numeroFactura;
    }

    public void setFACCVT_numeroFactura(String FACCVT_numeroFactura) {
        this.FACCVT_numeroFactura = FACCVT_numeroFactura;
    }

    public String getGENOFI_gloza() {
        return GENOFI_gloza;
    }

    public void setGENOFI_gloza(String GENOFI_gloza) {
        this.GENOFI_gloza = GENOFI_gloza;
    }

    public String getGENOFI_gloza2() {
        return GENOFI_gloza2;
    }

    public void setGENOFI_gloza2(String GENOFI_gloza2) {
        this.GENOFI_gloza2 = GENOFI_gloza2;
    }

    public BigDecimal getFACDVT_BaseImponible() {
        return FACDVT_BaseImponible;
    }

    public void setFACDVT_BaseImponible(BigDecimal FACDVT_BaseImponible) {
        this.FACDVT_BaseImponible = FACDVT_BaseImponible;
    }

    public String getGENOFI_nombreComercial() {
        return GENOFI_nombreComercial;
    }

    public void setGENOFI_nombreComercial(String GENOFI_nombreComercial) {
        this.GENOFI_nombreComercial = GENOFI_nombreComercial;
    }

    public String getFACDVT_LLevaContabilidad() {
        return FACDVT_LLevaContabilidad;
    }

    public void setFACDVT_LLevaContabilidad(String FACDVT_LLevaContabilidad) {
        this.FACDVT_LLevaContabilidad = FACDVT_LLevaContabilidad;
    }

    public String getFACDVT_portalweb() {
        return FACDVT_portalweb;
    }

    public void setFACDVT_portalweb(String FACDVT_portalweb) {
        this.FACDVT_portalweb = FACDVT_portalweb;
    }

    public String getGENCIA_ruc() {
        return GENCIA_ruc;
    }

    public void setGENCIA_ruc(String GENCIA_ruc) {
        this.GENCIA_ruc = GENCIA_ruc;
    }

    public String getFACCVT_autorizacion() {
        return FACCVT_autorizacion;
    }

    public void setFACCVT_autorizacion(String FACCVT_autorizacion) {
        this.FACCVT_autorizacion = FACCVT_autorizacion;
    }

    public String getFACCVT_fechaautoriza() {
        return FACCVT_fechaautoriza;
    }

    public void setFACCVT_fechaautoriza(String FACCVT_fechaautoriza) {
        this.FACCVT_fechaautoriza = FACCVT_fechaautoriza;
    }

    public String getFACCVT_claveacceso() {
        return FACCVT_claveacceso;
    }

    public void setFACCVT_claveacceso(String FACCVT_claveacceso) {
        this.FACCVT_claveacceso = FACCVT_claveacceso;
    }

    public String getCXCFPG_codigo() {
        return CXCFPG_codigo;
    }

    public void setCXCFPG_codigo(String CXCFPG_codigo) {
        this.CXCFPG_codigo = CXCFPG_codigo;
    }

    public String getFACCVT_horaautoriza() {
        return FACCVT_horaautoriza;
    }

    public void setFACCVT_horaautoriza(String FACCVT_horaautoriza) {
        this.FACCVT_horaautoriza = FACCVT_horaautoriza;
    }

    public BigDecimal getFACDVT_dsctoprecio() {
        return FACDVT_dsctoprecio;
    }

    public void setFACDVT_dsctoprecio(BigDecimal FACDVT_dsctoprecio) {
        this.FACDVT_dsctoprecio = FACDVT_dsctoprecio;
    }

    public String getFACCPD_ordencompra() {
        return FACCPD_ordencompra;
    }

    public void setFACCPD_ordencompra(String FACCPD_ordencompra) {
        this.FACCPD_ordencompra = FACCPD_ordencompra;
    }

    public String getFACCPD_codalmacen() {
        return FACCPD_codalmacen;
    }

    public void setFACCPD_codalmacen(String FACCPD_codalmacen) {
        this.FACCPD_codalmacen = FACCPD_codalmacen;
    }

    public int getFACCPD_secuenciacli() {
        return FACCPD_secuenciacli;
    }

    public void setFACCPD_secuenciacli(int FACCPD_secuenciacli) {
        this.FACCPD_secuenciacli = FACCPD_secuenciacli;
    }

    public String getFACCPD_montoletra() {
        return FACCPD_montoletra;
    }

    public void setFACCPD_montoletra(String FACCPD_montoletra) {
        this.FACCPD_montoletra = FACCPD_montoletra;
    }

    public String getGENOFI_telefono() {
        return GENOFI_telefono;
    }

    public void setGENOFI_telefono(String GENOFI_telefono) {
        this.GENOFI_telefono = GENOFI_telefono;
    }

}
