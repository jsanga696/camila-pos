/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.model.consultas;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author ANGEL NAVARRO
 */
public class AgrupacionInventario implements Serializable {

    private String INVIN1_descripcio;
    private String INVIN2_descripcio;
    private String INVIN3_descripcio;
    private String INVIN4_descripcio;
    private String INVITM_nombre;
    private String INVIN1_codin1item;
    private String INVIN2_codin2item;
    private String INVIN3_codin3item;
    private String INVIN4_codin4item;
    private String agrupacion;
    
    private Integer INVITM_CajasSugeridad = 0;
    private BigDecimal INVITM_ventapromedioQ;

    private List<InveRepdiasInventario> listadoItems;

    public AgrupacionInventario() {
    }

    public AgrupacionInventario(String INVIN1_descripcio) {
        this.INVIN1_descripcio = INVIN1_descripcio;
    }
    
    public AgrupacionInventario(String INVIN1_codin1item,String INVIN1_descripcio) {
        this.INVIN1_codin1item = INVIN1_codin1item;
        this.INVIN1_descripcio = INVIN1_descripcio;
    }

    public String getINVIN1_descripcio() {
        return INVIN1_descripcio;
    }

    public void setINVIN1_descripcio(String INVIN1_descripcio) {
        this.INVIN1_descripcio = INVIN1_descripcio;
    }

    public String getINVIN2_descripcio() {
        return INVIN2_descripcio;
    }

    public void setINVIN2_descripcio(String INVIN2_descripcio) {
        this.INVIN2_descripcio = INVIN2_descripcio;
    }

    public String getINVIN3_descripcio() {
        return INVIN3_descripcio;
    }

    public void setINVIN3_descripcio(String INVIN3_descripcio) {
        this.INVIN3_descripcio = INVIN3_descripcio;
    }

    public String getINVIN4_descripcio() {
        return INVIN4_descripcio;
    }

    public void setINVIN4_descripcio(String INVIN4_descripcio) {
        this.INVIN4_descripcio = INVIN4_descripcio;
    }

    public String getINVITM_nombre() {
        return INVITM_nombre;
    }

    public void setINVITM_nombre(String INVITM_nombre) {
        this.INVITM_nombre = INVITM_nombre;
    }

    public String getINVIN1_codin1item() {
        return INVIN1_codin1item;
    }

    public void setINVIN1_codin1item(String INVIN1_codin1item) {
        this.INVIN1_codin1item = INVIN1_codin1item;
    }

    public String getINVIN2_codin2item() {
        return INVIN2_codin2item;
    }

    public void setINVIN2_codin2item(String INVIN2_codin2item) {
        this.INVIN2_codin2item = INVIN2_codin2item;
    }

    public String getINVIN3_codin3item() {
        return INVIN3_codin3item;
    }

    public void setINVIN3_codin3item(String INVIN3_codin3item) {
        this.INVIN3_codin3item = INVIN3_codin3item;
    }

    public String getINVIN4_codin4item() {
        return INVIN4_codin4item;
    }

    public void setINVIN4_codin4item(String INVIN4_codin4item) {
        this.INVIN4_codin4item = INVIN4_codin4item;
    }

    public List<InveRepdiasInventario> getListadoItems() {
        return listadoItems;
    }

    public void setListadoItems(List<InveRepdiasInventario> listadoItems) {
        this.listadoItems = listadoItems;
    }

    public Integer getINVITM_CajasSugeridad() {
        return INVITM_CajasSugeridad;
    }

    public void setINVITM_CajasSugeridad(Integer INVITM_CajasSugeridad) {
        this.INVITM_CajasSugeridad = INVITM_CajasSugeridad;
    }

    public BigDecimal getINVITM_ventapromedioQ() {
        return INVITM_ventapromedioQ;
    }

    public void setINVITM_ventapromedioQ(BigDecimal INVITM_ventapromedioQ) {
        this.INVITM_ventapromedioQ = INVITM_ventapromedioQ;
    }

    public String getAgrupacion() {
        return agrupacion;
    }

    public void setAgrupacion(String agrupacion) {
        this.agrupacion = agrupacion;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.INVIN1_descripcio);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AgrupacionInventario other = (AgrupacionInventario) obj;
        if (!Objects.equals(this.INVIN1_descripcio, other.INVIN1_descripcio)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "AgrupacionInventario{" + "INVIN1_descripcio=" + INVIN1_descripcio + ", INVIN2_descripcio=" + INVIN2_descripcio + ", INVIN3_descripcio=" + INVIN3_descripcio + ", INVIN4_descripcio=" + INVIN4_descripcio + ", INVITM_nombre=" + INVITM_nombre + ", INVIN1_codin1item=" + INVIN1_codin1item + ", INVIN2_codin2item=" + INVIN2_codin2item + ", INVIN3_codin3item=" + INVIN3_codin3item + ", INVIN4_codin4item=" + INVIN4_codin4item + '}';
    }
    
    public Integer sumarSug(){
        INVITM_ventapromedioQ = BigDecimal.ZERO;
        INVITM_CajasSugeridad = 0;
        if(this.listadoItems != null){
            for (InveRepdiasInventario item : listadoItems) {
                if(item.getINVITM_CajasReales() == null){
                    item.setINVITM_CajasReales(0);
                }
                INVITM_CajasSugeridad += item.getINVITM_CajasReales();
                if(item.getINVITM_ventapromedioQ() == null){
                    item.setINVITM_ventapromedioQ(BigDecimal.ZERO);
                }
                INVITM_ventapromedioQ = INVITM_ventapromedioQ.add(item.getINVITM_ventapromedioQ());
            }
        }
        return INVITM_CajasSugeridad;
    }
    
    public String getNombreAgrupacion(){
        switch (this.agrupacion) {
            case "N1":
                return this.INVIN1_descripcio;
            case "N2":
                return this.INVIN2_descripcio;
            case "N3":
                return this.INVIN3_descripcio;
            default:
                return this.INVIN4_descripcio;
        }
    }

}
