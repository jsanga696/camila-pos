/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.model.consultas;

import java.io.Serializable;

/**
 *
 * @author Acer
 */
public class ResultadoMovimiento implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private String pscodigo;
    private String vosMsgErrorCorto;
    private String vosMsgErrorLargo;

    public ResultadoMovimiento() {
    }

    public String getPscodigo() {
        return pscodigo;
    }

    public void setPscodigo(String pscodigo) {
        this.pscodigo = pscodigo;
    }

    public String getVosMsgErrorCorto() {
        return vosMsgErrorCorto;
    }

    public void setVosMsgErrorCorto(String vosMsgErrorCorto) {
        this.vosMsgErrorCorto = vosMsgErrorCorto;
    }

    public String getVosMsgErrorLargo() {
        return vosMsgErrorLargo;
    }

    public void setVosMsgErrorLargo(String vosMsgErrorLargo) {
        this.vosMsgErrorLargo = vosMsgErrorLargo;
    }
    
    
}
