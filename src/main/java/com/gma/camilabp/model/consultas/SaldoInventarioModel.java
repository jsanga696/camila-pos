/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.model.consultas;

import com.gma.camilabo.enums.TiposItem;
import com.gma.camilabp.domain.CbpgenrBodega;
import com.gma.camilabp.domain.CbpgenrOficina;
import com.gma.camilabp.domain.CxpmProveedor;
import com.gma.camilabp.domain.InvmItem;
import com.gma.camilabp.domain.InvrCablistaprecio;
import com.gma.camilabp.web.util.Utilidades;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author userdb6
 */
public class SaldoInventarioModel implements Serializable{
    private static final long serialVersionUID = 1L;
    private String codigoCompania;
    private CbpgenrOficina oficina;
    private String secuenciaOficina;
    private String codigoTipoSaldoInventario;
    private String codigoTipoItem= TiposItem.TERMINADO.getCodigo();
    private List<String> estadosItem= new ArrayList<>();
    private String codigoStock="D";
    private List<CbpgenrBodega> bodegas;
    private Boolean filtro= Boolean.FALSE;
    private Boolean corte= Boolean.FALSE;
    private String codigoFiltro;
    private String codigoBusquedaFiltro;
    private String datoBusquedaFiltro;
    private CxpmProveedor provedor;
    private InvmItem item;
    private TipoFiltro nivel;
    private InvrCablistaprecio cablistaprecio;
    private String tipoCosto;
    private Date fechaCorte= new Date();
    private String usuario;
    private String opcion;
    private String maquina;
    
    public SaldoInventarioModel() {
        this.estadosItem.add("INVITM_estactivo");
    }
    
    public String getCadenaBodega(){
        String cadenaBod="";
        for (CbpgenrBodega bodega : this.bodegas) {
            cadenaBod=cadenaBod+"*"+bodega.getGenbodSecuencia()+"*,";
        }
        return Utilidades.getSubstring(cadenaBod);
    }
    
    public String getTipoCostoReporte(){
        return this.tipoCosto.equalsIgnoreCase("P")?"Costo Promedio":"Ultimo Costo";
    }
    
    public void limpiarDatosFiltro(){
        this.provedor = null;
        this.item = null;
        this.datoBusquedaFiltro = null;
    }
    
    public String getCodigoCompania() {
        return codigoCompania;
    }

    public void setCodigoCompania(String codigoCompania) {
        this.codigoCompania = codigoCompania;
    }

    public CbpgenrOficina getOficina() {
        return oficina;
    }

    public void setOficina(CbpgenrOficina oficina) {
        this.oficina = oficina;
    }

    public String getSecuenciaOficina() {
        return secuenciaOficina;
    }

    public void setSecuenciaOficina(String secuenciaOficina) {
        this.secuenciaOficina = secuenciaOficina;
    }

    public String getCodigoTipoSaldoInventario() {
        return codigoTipoSaldoInventario;
    }

    public void setCodigoTipoSaldoInventario(String codigoTipoSaldoInventario) {
        this.codigoTipoSaldoInventario = codigoTipoSaldoInventario;
    }

    public String getCodigoTipoItem() {
        return codigoTipoItem;
    }

    public void setCodigoTipoItem(String codigoTipoItem) {
        this.codigoTipoItem = codigoTipoItem;
    }

    public List<String> getEstadosItem() {
        return estadosItem;
    }

    public void setEstadosItem(List<String> estadosItem) {
        this.estadosItem = estadosItem;
    }

    public String getCodigoStock() {
        return codigoStock;
    }

    public void setCodigoStock(String codigoStock) {
        this.codigoStock = codigoStock;
    }

    public List<CbpgenrBodega> getBodegas() {
        return bodegas;
    }

    public void setBodegas(List<CbpgenrBodega> bodegas) {
        this.bodegas = bodegas;
    }

    public Boolean getFiltro() {
        return filtro;
    }

    public void setFiltro(Boolean filtro) {
        this.filtro = filtro;
    }

    public Boolean getCorte() {
        return corte;
    }

    public void setCorte(Boolean corte) {
        this.corte = corte;
    }

    public String getCodigoFiltro() {
        return codigoFiltro;
    }

    public void setCodigoFiltro(String codigoFiltro) {
        this.codigoFiltro = codigoFiltro;
    }

    public String getCodigoBusquedaFiltro() {
        return codigoBusquedaFiltro;
    }

    public void setCodigoBusquedaFiltro(String codigoBusquedaFiltro) {
        this.codigoBusquedaFiltro = codigoBusquedaFiltro;
    }

    public String getDatoBusquedaFiltro() {
        return datoBusquedaFiltro;
    }

    public void setDatoBusquedaFiltro(String datoBusquedaFiltro) {
        this.datoBusquedaFiltro = datoBusquedaFiltro;
    }

    public CxpmProveedor getProvedor() {
        return provedor;
    }

    public void setProvedor(CxpmProveedor provedor) {
        this.provedor = provedor;
    }

    public InvmItem getItem() {
        return item;
    }

    public void setItem(InvmItem item) {
        this.item = item;
    }

    public TipoFiltro getNivel() {
        return nivel;
    }

    public void setNivel(TipoFiltro nivel) {
        this.nivel = nivel;
    }

    public InvrCablistaprecio getCablistaprecio() {
        return cablistaprecio;
    }

    public void setCablistaprecio(InvrCablistaprecio cablistaprecio) {
        this.cablistaprecio = cablistaprecio;
    }

    public String getTipoCosto() {
        return tipoCosto;
    }

    public void setTipoCosto(String tipoCosto) {
        this.tipoCosto = tipoCosto;
    }

    public Date getFechaCorte() {
        return fechaCorte;
    }

    public void setFechaCorte(Date fechaCorte) {
        this.fechaCorte = fechaCorte;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getOpcion() {
        return opcion;
    }

    public void setOpcion(String opcion) {
        this.opcion = opcion;
    }

    public String getMaquina() {
        return maquina;
    }

    public void setMaquina(String maquina) {
        this.maquina = maquina;
    }
    
}
