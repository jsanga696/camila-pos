/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.model.consultas;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Acer
 */
public class TempAjusteTransferenciaInv implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer ID_SECUENCIA;
    private Integer INVCOT_numsecuenc;
    private Integer INVCOT_numdocumen;
    private String GENCIA_codigo;
    private String GENOFI_codigo;
    private String GENMOD_codigo;
    private String GENTDO_codigo;
    private String INVCOT_motvajuste;
    private String GENBOD_codigo;
    private String INVITM_codigo;
    private String INVLOT_codigo;
    private Date INVLOT_fechavenci;
    private BigDecimal INVITM_uniembala;
    private BigDecimal INVDOT_precio;
    private BigDecimal INVDOT_cantifunci= BigDecimal.ZERO;
    private BigDecimal INVDOT_cantcajas= BigDecimal.ZERO;
    private BigDecimal INVDOT_cantunida= BigDecimal.ZERO;
    private String INVDOT_estMultUnidad;
    private String INVDOT_MultUniCod;
    private BigDecimal INVDOT_subtotamov;
    private String item_nombre;
    private String GENBODDES_codigo;
    private BigDecimal INVDOT_faltantecantifunci= BigDecimal.ZERO;
    private BigDecimal INVDOT_faltantecantcajas= BigDecimal.ZERO;
    private BigDecimal INVDOT_faltantecantunida= BigDecimal.ZERO;

    public TempAjusteTransferenciaInv() {
    }

    public TempAjusteTransferenciaInv(Integer ID_SECUENCIA) {
        this.ID_SECUENCIA = ID_SECUENCIA;
    }

    public TempAjusteTransferenciaInv(Integer ID_SECUENCIA, Integer INVCOT_numsecuenc, Integer INVCOT_numdocumen, String GENCIA_codigo, String GENOFI_codigo, String GENMOD_codigo, String GENTDO_codigo, String INVCOT_motvajuste, String GENBOD_codigo, String INVITM_codigo, String INVLOT_codigo, Date INVLOT_fechavenci, BigDecimal INVITM_uniembala, BigDecimal INVDOT_precio, BigDecimal INVDOT_cantifunci, BigDecimal INVDOT_cantcajas, BigDecimal INVDOT_cantunida, String INVDOT_estMultUnidad, String INVDOT_MultUniCod, BigDecimal INVDOT_subtotamov, String item_nombre, String GENBODDES_codigo) {
        this.ID_SECUENCIA = ID_SECUENCIA;
        this.INVCOT_numsecuenc = INVCOT_numsecuenc;
        this.INVCOT_numdocumen = INVCOT_numdocumen;
        this.GENCIA_codigo = GENCIA_codigo;
        this.GENOFI_codigo = GENOFI_codigo;
        this.GENMOD_codigo = GENMOD_codigo;
        this.GENTDO_codigo = GENTDO_codigo;
        this.INVCOT_motvajuste = INVCOT_motvajuste;
        this.GENBOD_codigo = GENBOD_codigo;
        this.INVITM_codigo = INVITM_codigo;
        this.INVLOT_codigo = INVLOT_codigo;
        this.INVLOT_fechavenci = INVLOT_fechavenci;
        this.INVITM_uniembala = INVITM_uniembala;
        this.INVDOT_precio = INVDOT_precio;
        this.INVDOT_cantifunci = INVDOT_cantifunci;
        this.INVDOT_cantcajas = INVDOT_cantcajas;
        this.INVDOT_cantunida = INVDOT_cantunida;
        this.INVDOT_estMultUnidad = INVDOT_estMultUnidad;
        this.INVDOT_MultUniCod = INVDOT_MultUniCod;
        this.INVDOT_subtotamov = INVDOT_subtotamov;
        this.item_nombre = item_nombre;
        this.GENBODDES_codigo = GENBODDES_codigo;
    }

    public Integer getID_SECUENCIA() {
        return ID_SECUENCIA;
    }

    public void setID_SECUENCIA(Integer ID_SECUENCIA) {
        this.ID_SECUENCIA = ID_SECUENCIA;
    }

    public Integer getINVCOT_numsecuenc() {
        return INVCOT_numsecuenc;
    }

    public void setINVCOT_numsecuenc(Integer INVCOT_numsecuenc) {
        this.INVCOT_numsecuenc = INVCOT_numsecuenc;
    }

    public Integer getINVCOT_numdocumen() {
        return INVCOT_numdocumen;
    }

    public void setINVCOT_numdocumen(Integer INVCOT_numdocumen) {
        this.INVCOT_numdocumen = INVCOT_numdocumen;
    }

    public String getGENCIA_codigo() {
        return GENCIA_codigo;
    }

    public void setGENCIA_codigo(String GENCIA_codigo) {
        this.GENCIA_codigo = GENCIA_codigo;
    }

    public String getGENOFI_codigo() {
        return GENOFI_codigo;
    }

    public void setGENOFI_codigo(String GENOFI_codigo) {
        this.GENOFI_codigo = GENOFI_codigo;
    }

    public String getGENMOD_codigo() {
        return GENMOD_codigo;
    }

    public void setGENMOD_codigo(String GENMOD_codigo) {
        this.GENMOD_codigo = GENMOD_codigo;
    }

    public String getGENTDO_codigo() {
        return GENTDO_codigo;
    }

    public void setGENTDO_codigo(String GENTDO_codigo) {
        this.GENTDO_codigo = GENTDO_codigo;
    }

    public String getINVCOT_motvajuste() {
        return INVCOT_motvajuste;
    }

    public void setINVCOT_motvajuste(String INVCOT_motvajuste) {
        this.INVCOT_motvajuste = INVCOT_motvajuste;
    }

    public String getGENBOD_codigo() {
        return GENBOD_codigo;
    }

    public void setGENBOD_codigo(String GENBOD_codigo) {
        this.GENBOD_codigo = GENBOD_codigo;
    }

    public String getINVITM_codigo() {
        return INVITM_codigo;
    }

    public void setINVITM_codigo(String INVITM_codigo) {
        this.INVITM_codigo = INVITM_codigo;
    }

    public String getINVLOT_codigo() {
        return INVLOT_codigo;
    }

    public void setINVLOT_codigo(String INVLOT_codigo) {
        this.INVLOT_codigo = INVLOT_codigo;
    }

    public Date getINVLOT_fechavenci() {
        return INVLOT_fechavenci;
    }

    public void setINVLOT_fechavenci(Date INVLOT_fechavenci) {
        this.INVLOT_fechavenci = INVLOT_fechavenci;
    }

    public BigDecimal getINVITM_uniembala() {
        return INVITM_uniembala;
    }

    public void setINVITM_uniembala(BigDecimal INVITM_uniembala) {
        this.INVITM_uniembala = INVITM_uniembala;
    }

    public BigDecimal getINVDOT_precio() {
        return INVDOT_precio;
    }

    public void setINVDOT_precio(BigDecimal INVDOT_precio) {
        this.INVDOT_precio = INVDOT_precio;
    }

    public BigDecimal getINVDOT_cantifunci() {
        return INVDOT_cantifunci;
    }

    public void setINVDOT_cantifunci(BigDecimal INVDOT_cantifunci) {
        this.INVDOT_cantifunci = INVDOT_cantifunci;
    }

    public BigDecimal getINVDOT_cantcajas() {
        return INVDOT_cantcajas;
    }

    public void setINVDOT_cantcajas(BigDecimal INVDOT_cantcajas) {
        this.INVDOT_cantcajas = INVDOT_cantcajas;
    }

    public BigDecimal getINVDOT_cantunida() {
        return INVDOT_cantunida;
    }

    public void setINVDOT_cantunida(BigDecimal INVDOT_cantunida) {
        this.INVDOT_cantunida = INVDOT_cantunida;
    }

    public String getINVDOT_estMultUnidad() {
        return INVDOT_estMultUnidad;
    }

    public void setINVDOT_estMultUnidad(String INVDOT_estMultUnidad) {
        this.INVDOT_estMultUnidad = INVDOT_estMultUnidad;
    }

    public String getINVDOT_MultUniCod() {
        return INVDOT_MultUniCod;
    }

    public void setINVDOT_MultUniCod(String INVDOT_MultUniCod) {
        this.INVDOT_MultUniCod = INVDOT_MultUniCod;
    }

    public BigDecimal getINVDOT_subtotamov() {
        return INVDOT_subtotamov;
    }

    public void setINVDOT_subtotamov(BigDecimal INVDOT_subtotamov) {
        this.INVDOT_subtotamov = INVDOT_subtotamov;
    }

    public String getItem_nombre() {
        return item_nombre;
    }

    public void setItem_nombre(String item_nombre) {
        this.item_nombre = item_nombre;
    }

    public String getGENBODDES_codigo() {
        return GENBODDES_codigo;
    }

    public void setGENBODDES_codigo(String GENBODDES_codigo) {
        this.GENBODDES_codigo = GENBODDES_codigo;
    }

    public BigDecimal getINVDOT_faltantecantifunci() {
        return INVDOT_faltantecantifunci;
    }

    public void setINVDOT_faltantecantifunci(BigDecimal INVDOT_faltantecantifunci) {
        this.INVDOT_faltantecantifunci = INVDOT_faltantecantifunci;
    }

    public BigDecimal getINVDOT_faltantecantcajas() {
        return INVDOT_faltantecantcajas;
    }

    public void setINVDOT_faltantecantcajas(BigDecimal INVDOT_faltantecantcajas) {
        this.INVDOT_faltantecantcajas = INVDOT_faltantecantcajas;
    }

    public BigDecimal getINVDOT_faltantecantunida() {
        return INVDOT_faltantecantunida;
    }

    public void setINVDOT_faltantecantunida(BigDecimal INVDOT_faltantecantunida) {
        this.INVDOT_faltantecantunida = INVDOT_faltantecantunida;
    }

    
}
