/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.model.consultas;

import java.io.Serializable;

/**
 *
 * @author Acer
 */
public class Facturacion implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private ResultadoFacturacion factura;
    private ResultadoCobro cobro;

    public Facturacion() {
    }

    public Facturacion(ResultadoFacturacion factura, ResultadoCobro cobro) {
        this.factura = factura;
        this.cobro = cobro;
    }

    public ResultadoFacturacion getFactura() {
        return factura;
    }

    public void setFactura(ResultadoFacturacion factura) {
        this.factura = factura;
    }

    public ResultadoCobro getCobro() {
        return cobro;
    }

    public void setCobro(ResultadoCobro cobro) {
        this.cobro = cobro;
    }
    
}
