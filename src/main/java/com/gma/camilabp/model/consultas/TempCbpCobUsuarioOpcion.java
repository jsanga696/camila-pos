/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.model.consultas;

import com.gma.camilabp.domain.CbpgenrUsuarioxofi;
import com.gma.camilabp.domain.GenmCaja;
import com.gma.camilabp.model.logica.TipoPago;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author userdb6
 */
public class TempCbpCobUsuarioOpcion implements Serializable{
    private static final long serialVersionUID = 1L;
    private Integer idSecuencia;
    /*private String genciaCodigo;
    private String genofiCodigo;
    private BigDecimal faccobSubtotimp;
    private BigDecimal faccobSubtotexe;
    private BigDecimal faccobSubtotal;
    private BigDecimal faccobDescimp;
    private BigDecimal faccobDescexe;
    private BigDecimal faccobDescuento;
    private BigDecimal faccobImpuesto;
    private BigDecimal faccobNeto;*/
    
    //private BigDecimal faccobRecibido;
    private BigDecimal cxcdeuValorPago;
    //private BigDecimal faccobCambio;
    private BigDecimal cxcdeuVuelto= BigDecimal.ZERO;
    /*private Date faccobFechaemisi;
    private String facusuCodigo;
    private String facvdrCodigo;*/
    //private String faccobTipopago;
    private String cxcdeuTipopag;
    private String cxcdeuTipoent;
    /*private BigDecimal faccobValor;*/
    //private String banentCodigo;
    private String cxcdeuCodent;
    //private String faccobNumcuenta;
    private String cxcdeuNumcuenta;
    //private String faccobNumdocumento;
    private String cxcdeuNumcheque;
    private Date cxcdeuFechavenci;
    private String faccobFechavalidez;
    private String titular;
    private Integer tipoPago;

    public TempCbpCobUsuarioOpcion() {
    }

    public TempCbpCobUsuarioOpcion(Integer idSecuencia) {
        this.idSecuencia = idSecuencia;
    }

    /*public TempCbpCobUsuarioOpcion(Integer idSecuencia, String genciaCodigo, String genofiCodigo, BigDecimal faccobSubtotimp, BigDecimal faccobSubtotexe, BigDecimal faccobSubtotal, BigDecimal faccobDescimp, BigDecimal faccobDescexe, BigDecimal faccobDescuento, BigDecimal faccobImpuesto, BigDecimal faccobNeto, BigDecimal faccobRecibido, BigDecimal faccobCambio, Date faccobFechaemisi, String facusuCodigo, String facvdrCodigo, String faccobTipopago, BigDecimal faccobValor, String banentCodigo, String faccobNumcuenta, String faccobNumdocumento, String faccobFechavalidez) {
        this.idSecuencia = idSecuencia;
        this.genciaCodigo = genciaCodigo;
        this.genofiCodigo = genofiCodigo;
        this.faccobSubtotimp = faccobSubtotimp;
        this.faccobSubtotexe = faccobSubtotexe;
        this.faccobSubtotal = faccobSubtotal;
        this.faccobDescimp = faccobDescimp;
        this.faccobDescexe = faccobDescexe;
        this.faccobDescuento = faccobDescuento;
        this.faccobImpuesto = faccobImpuesto;
        this.faccobNeto = faccobNeto;
        this.faccobRecibido = faccobRecibido;
        this.faccobCambio = faccobCambio;
        this.faccobFechaemisi = faccobFechaemisi;
        this.facusuCodigo = facusuCodigo;
        this.facvdrCodigo = facvdrCodigo;
        this.faccobTipopago = faccobTipopago;
        this.faccobValor = faccobValor;
        this.banentCodigo = banentCodigo;
        this.faccobNumcuenta = faccobNumcuenta;
        this.faccobNumdocumento = faccobNumdocumento;
        this.faccobFechavalidez = faccobFechavalidez;
    }*/
    
    public TempCbpCobUsuarioOpcion(Integer idSecuencia, String cxcdeuTipopag, BigDecimal cxcdeuValorPago, String faccobFechavalidez, Integer tipoPago) {
        this.idSecuencia = idSecuencia;
        this.cxcdeuTipopag = cxcdeuTipopag;
        this.tipoPago = tipoPago;
        this.cxcdeuTipoent = tipoPago==1L?"EF":tipoPago==2L?"BC":tipoPago==3L?"TC":tipoPago==4L?"DC":tipoPago==5L?"CR":"";
        this.cxcdeuValorPago = cxcdeuValorPago;
        this.faccobFechavalidez = faccobFechavalidez;
    }
    
    public TempCbpCobUsuarioOpcion(TipoPago tipoPago, GenmCaja caja){
        /*this.genciaCodigo = usuario.getGenciaCodigo().getGenciaSecuencia();
        this.genofiCodigo = usuario.getGenofiCodigo().getGenofiSecuencia();
        this.faccobFechaemisi = new Date();
        this.facusuCodigo = usuario.getSegusuLogin();
        this.facvdrCodigo = usuario.getFacvdrCodigo();
        this.faccobTipopago = tipoPago.getTipo()+"";
        this.faccobValor = tipoPago.getValor();
        this.banentCodigo = tipoPago.getEntidad().substring(0, 2);
        this.faccobNumcuenta = tipoPago.getNumeroCuenta();
        this.faccobNumdocumento = tipoPago.getNumeroDocumento();
        this.faccobFechavalidez = tipoPago.getFechaValidez();*/
        this.cxcdeuTipopag = tipoPago.getTipo()+"";
        this.tipoPago= tipoPago.getTipo();
        this.cxcdeuTipoent = tipoPago.getTipo()==1?"EF":tipoPago.getTipo()==2?"BC":tipoPago.getTipo()==3?"TC":tipoPago.getTipo()==4?"DC":tipoPago.getTipo()==5?"CR":"";
        this.cxcdeuValorPago = tipoPago.getValor();
        this.cxcdeuCodent = tipoPago.getEntidad().substring(0, 2);
        this.cxcdeuNumcuenta = tipoPago.getNumeroCuenta();
        this.cxcdeuNumcheque = tipoPago.getNumeroDocumento();
        this.faccobFechavalidez = tipoPago.getFechaValidez();
        this.cxcdeuFechavenci = tipoPago.getFechaVencimiento();
        this.titular = tipoPago.getTitular()!=null?tipoPago.getTitular().toUpperCase():null;
    }

    public Integer getIdSecuencia() {
        return idSecuencia;
    }

    public void setIdSecuencia(Integer idSecuencia) {
        this.idSecuencia = idSecuencia;
    }

    /*public String getGenciaCodigo() {
        return genciaCodigo;
    }

    public void setGenciaCodigo(String genciaCodigo) {
        this.genciaCodigo = genciaCodigo;
    }

    public String getGenofiCodigo() {
        return genofiCodigo;
    }

    public void setGenofiCodigo(String genofiCodigo) {
        this.genofiCodigo = genofiCodigo;
    }

    public BigDecimal getFaccobSubtotimp() {
        return faccobSubtotimp;
    }

    public void setFaccobSubtotimp(BigDecimal faccobSubtotimp) {
        this.faccobSubtotimp = faccobSubtotimp;
    }

    public BigDecimal getFaccobSubtotexe() {
        return faccobSubtotexe;
    }

    public void setFaccobSubtotexe(BigDecimal faccobSubtotexe) {
        this.faccobSubtotexe = faccobSubtotexe;
    }

    public BigDecimal getFaccobSubtotal() {
        return faccobSubtotal;
    }

    public void setFaccobSubtotal(BigDecimal faccobSubtotal) {
        this.faccobSubtotal = faccobSubtotal;
    }

    public BigDecimal getFaccobDescimp() {
        return faccobDescimp;
    }

    public void setFaccobDescimp(BigDecimal faccobDescimp) {
        this.faccobDescimp = faccobDescimp;
    }

    public BigDecimal getFaccobDescexe() {
        return faccobDescexe;
    }

    public void setFaccobDescexe(BigDecimal faccobDescexe) {
        this.faccobDescexe = faccobDescexe;
    }

    public BigDecimal getFaccobDescuento() {
        return faccobDescuento;
    }

    public void setFaccobDescuento(BigDecimal faccobDescuento) {
        this.faccobDescuento = faccobDescuento;
    }

    public BigDecimal getFaccobImpuesto() {
        return faccobImpuesto;
    }

    public void setFaccobImpuesto(BigDecimal faccobImpuesto) {
        this.faccobImpuesto = faccobImpuesto;
    }

    public BigDecimal getFaccobNeto() {
        return faccobNeto;
    }

    public void setFaccobNeto(BigDecimal faccobNeto) {
        this.faccobNeto = faccobNeto;
    }

    public BigDecimal getFaccobRecibido() {
        return faccobRecibido;
    }

    public void setFaccobRecibido(BigDecimal faccobRecibido) {
        this.faccobRecibido = faccobRecibido;
    }

    public BigDecimal getFaccobCambio() {
        return faccobCambio;
    }

    public void setFaccobCambio(BigDecimal faccobCambio) {
        this.faccobCambio = faccobCambio;
    }

    public Date getFaccobFechaemisi() {
        return faccobFechaemisi;
    }

    public void setFaccobFechaemisi(Date faccobFechaemisi) {
        this.faccobFechaemisi = faccobFechaemisi;
    }

    public String getFacusuCodigo() {
        return facusuCodigo;
    }

    public void setFacusuCodigo(String facusuCodigo) {
        this.facusuCodigo = facusuCodigo;
    }

    public String getFacvdrCodigo() {
        return facvdrCodigo;
    }

    public void setFacvdrCodigo(String facvdrCodigo) {
        this.facvdrCodigo = facvdrCodigo;
    }

    public String getFaccobTipopago() {
        return faccobTipopago;
    }

    public void setFaccobTipopago(String faccobTipopago) {
        this.faccobTipopago = faccobTipopago;
    }

    public BigDecimal getFaccobValor() {
        return faccobValor;
    }

    public void setFaccobValor(BigDecimal faccobValor) {
        this.faccobValor = faccobValor;
    }

    public String getBanentCodigo() {
        return banentCodigo;
    }

    public void setBanentCodigo(String banentCodigo) {
        this.banentCodigo = banentCodigo;
    }

    public String getFaccobNumcuenta() {
        return faccobNumcuenta;
    }

    public void setFaccobNumcuenta(String faccobNumcuenta) {
        this.faccobNumcuenta = faccobNumcuenta;
    }

    public String getFaccobNumdocumento() {
        return faccobNumdocumento;
    }

    public void setFaccobNumdocumento(String faccobNumdocumento) {
        this.faccobNumdocumento = faccobNumdocumento;
    }*/
    
    public BigDecimal getCxcdeuValorPago() {
        return cxcdeuValorPago;
    }

    public void setCxcdeuValorPago(BigDecimal cxcdeuValorPago) {
        this.cxcdeuValorPago = cxcdeuValorPago;
    }

    public BigDecimal getCxcdeuVuelto() {
        return cxcdeuVuelto;
    }

    public void setCxcdeuVuelto(BigDecimal cxcdeuVuelto) {
        this.cxcdeuVuelto = cxcdeuVuelto;
    }

    public String getCxcdeuTipopag() {
        return cxcdeuTipopag;
    }

    public void setCxcdeuTipopag(String cxcdeuTipopag) {
        this.cxcdeuTipopag = cxcdeuTipopag;
    }

    public String getCxcdeuTipoent() {
        return cxcdeuTipoent;
    }

    public void setCxcdeuTipoent(String cxcdeuTipoent) {
        this.cxcdeuTipoent = cxcdeuTipoent;
    }

    public String getCxcdeuCodent() {
        return cxcdeuCodent;
    }

    public void setCxcdeuCodent(String cxcdeuCodent) {
        this.cxcdeuCodent = cxcdeuCodent;
    }

    public String getCxcdeuNumcuenta() {
        return cxcdeuNumcuenta;
    }

    public void setCxcdeuNumcuenta(String cxcdeuNumcuenta) {
        this.cxcdeuNumcuenta = cxcdeuNumcuenta;
    }

    public String getCxcdeuNumcheque() {
        return cxcdeuNumcheque;
    }

    public void setCxcdeuNumcheque(String cxcdeuNumcheque) {
        this.cxcdeuNumcheque = cxcdeuNumcheque;
    }

    public Date getCxcdeuFechavenci() {
        return cxcdeuFechavenci;
    }

    public void setCxcdeuFechavenci(Date cxcdeuFechavenci) {
        this.cxcdeuFechavenci = cxcdeuFechavenci;
    }

    

    public String getFaccobFechavalidez() {
        return faccobFechavalidez;
    }

    public void setFaccobFechavalidez(String faccobFechavalidez) {
        this.faccobFechavalidez = faccobFechavalidez;
    }

    public String getTitular() {
        return titular;
    }

    public void setTitular(String titular) {
        this.titular = titular;
    }

    public Integer getTipoPago() {
        return tipoPago;
    }

    public void setTipoPago(Integer tipoPago) {
        this.tipoPago = tipoPago;
    }

    @Override
    public String toString() {
        return "TempCbpCobUsuarioOpcion{" + "idSecuencia=" + idSecuencia + '}';
    }
    
}
