/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.model.consultas;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.Objects;

/**
 *
 * @author userdb2
 */
public class TemDevolucionFactura implements Serializable {

    private Integer ID_secuencia;
    private Integer ID_Pedidos;
    private Integer ID_lineaFAC;
    private Integer idLineaPed;
    private Integer FACDTC_secuelinea;
    private String GENCIA_codigo;
    private String GENOFI_codigo;
    private String GENBOD_codigo;
    private Date FACCVT_fechaemisi;
    private String FACCVT_tiporeg;
    private String CXCCLI_codigo;
    private String FACVDR_codigo;
    private String CXCRUT_codigo;
    private String INVITM_codigo;
    private String INVITM_estaitem;
    private BigDecimal FACDVT_cantfuncional = BigDecimal.ZERO;
    private BigDecimal FACDPD_cantfuncional;
    private BigDecimal FACDVT_precio;
    private BigDecimal FACDVT_subtotal;
    private BigDecimal FACDVT_descuento;
    private BigDecimal FACDVT_impuesto;
    private BigDecimal FACDVT_neto;
    private String FACDVT_listaprecio;
    private String nombreItem;
    private BigDecimal FACDVT_cantfundev;

    public TemDevolucionFactura() {
    }

    public TemDevolucionFactura(Integer ID_lineaFAC, String GENCIA_codigo, String GENOFI_codigo, String GENBOD_codigo, Date FACCVT_fechaemisi, String CXCCLI_codigo,
            String FACVDR_codigo, String CXCRUT_codigo, String INVITM_codigo, String INVITM_estaitem, BigDecimal FACDPD_cantfuncional, BigDecimal FACDVT_precio, 
            BigDecimal FACDVT_subtotal, BigDecimal FACDVT_descuento, BigDecimal FACDVT_impuesto, BigDecimal FACDVT_neto, String FACDVT_listaprecio) {
        this.ID_lineaFAC = ID_lineaFAC;
        this.GENCIA_codigo = GENCIA_codigo;
        this.GENOFI_codigo = GENOFI_codigo;
        this.GENBOD_codigo = GENBOD_codigo;
        this.FACCVT_fechaemisi = FACCVT_fechaemisi;
        this.CXCCLI_codigo = CXCCLI_codigo;
        this.FACVDR_codigo = FACVDR_codigo;
        this.CXCRUT_codigo = CXCRUT_codigo;
        this.INVITM_codigo = INVITM_codigo;
        this.INVITM_estaitem = INVITM_estaitem;
        this.FACDPD_cantfuncional = FACDPD_cantfuncional;
        this.FACDVT_precio = FACDVT_precio;
        this.FACDVT_subtotal = FACDVT_subtotal;
        this.FACDVT_descuento = FACDVT_descuento;
        this.FACDVT_impuesto = FACDVT_impuesto;
        this.FACDVT_neto = FACDVT_neto;
        this.FACDVT_listaprecio = FACDVT_listaprecio;
    }

    public Integer getID_secuencia() {
        return ID_secuencia;
    }

    public void setID_secuencia(Integer ID_secuencia) {
        this.ID_secuencia = ID_secuencia;
    }

    public Integer getID_Pedidos() {
        return ID_Pedidos;
    }

    public void setID_Pedidos(Integer ID_Pedidos) {
        this.ID_Pedidos = ID_Pedidos;
    }

    public Integer getFACDTC_secuelinea() {
        return FACDTC_secuelinea;
    }

    public void setFACDTC_secuelinea(Integer FACDTC_secuelinea) {
        this.FACDTC_secuelinea = FACDTC_secuelinea;
    }

    public BigDecimal getFACDVT_cantfuncional() {
        return FACDVT_cantfuncional;
    }

    public void setFACDVT_cantfuncional(BigDecimal FACDVT_cantfuncional) {
        this.FACDVT_cantfuncional = FACDVT_cantfuncional;
    }

    public Integer getIdLineaPed() {
        return idLineaPed;
    }

    public void setIdLineaPed(Integer idLineaPed) {
        this.idLineaPed = idLineaPed;
    }

    public Integer getID_lineaFAC() {
        return ID_lineaFAC;
    }

    public void setID_lineaFAC(Integer ID_lineaFAC) {
        this.ID_lineaFAC = ID_lineaFAC;
    }

    public String getGENCIA_codigo() {
        return GENCIA_codigo;
    }

    public void setGENCIA_codigo(String GENCIA_codigo) {
        this.GENCIA_codigo = GENCIA_codigo;
    }

    public String getGENOFI_codigo() {
        return GENOFI_codigo;
    }

    public void setGENOFI_codigo(String GENOFI_codigo) {
        this.GENOFI_codigo = GENOFI_codigo;
    }

    public String getGENBOD_codigo() {
        return GENBOD_codigo;
    }

    public void setGENBOD_codigo(String GENBOD_codigo) {
        this.GENBOD_codigo = GENBOD_codigo;
    }

    public Date getFACCVT_fechaemisi() {
        return FACCVT_fechaemisi;
    }

    public void setFACCVT_fechaemisi(Date FACCVT_fechaemisi) {
        this.FACCVT_fechaemisi = FACCVT_fechaemisi;
    }

    public String getFACCVT_tiporeg() {
        return FACCVT_tiporeg;
    }

    public void setFACCVT_tiporeg(String FACCVT_tiporeg) {
        this.FACCVT_tiporeg = FACCVT_tiporeg;
    }

    public String getCXCCLI_codigo() {
        return CXCCLI_codigo;
    }

    public void setCXCCLI_codigo(String CXCCLI_codigo) {
        this.CXCCLI_codigo = CXCCLI_codigo;
    }

    public String getFACVDR_codigo() {
        return FACVDR_codigo;
    }

    public void setFACVDR_codigo(String FACVDR_codigo) {
        this.FACVDR_codigo = FACVDR_codigo;
    }

    public String getCXCRUT_codigo() {
        return CXCRUT_codigo;
    }

    public void setCXCRUT_codigo(String CXCRUT_codigo) {
        this.CXCRUT_codigo = CXCRUT_codigo;
    }

    public String getINVITM_codigo() {
        return INVITM_codigo;
    }

    public void setINVITM_codigo(String INVITM_codigo) {
        this.INVITM_codigo = INVITM_codigo;
    }

    public String getINVITM_estaitem() {
        return INVITM_estaitem;
    }

    public void setINVITM_estaitem(String INVITM_estaitem) {
        this.INVITM_estaitem = INVITM_estaitem;
    }

    public BigDecimal getFACDPD_cantfuncional() {
        return FACDPD_cantfuncional;
    }

    public void setFACDPD_cantfuncional(BigDecimal FACDPD_cantfuncional) {
        this.FACDPD_cantfuncional = FACDPD_cantfuncional;
    }

    public BigDecimal getFACDVT_precio() {
        return FACDVT_precio;
    }

    public void setFACDVT_precio(BigDecimal FACDVT_precio) {
        this.FACDVT_precio = FACDVT_precio;
    }

    public BigDecimal getFACDVT_subtotal() {
        return FACDVT_subtotal;
    }

    public void setFACDVT_subtotal(BigDecimal FACDVT_subtotal) {
        this.FACDVT_subtotal = FACDVT_subtotal;
    }

    public BigDecimal getFACDVT_descuento() {
        return FACDVT_descuento;
    }

    public void setFACDVT_descuento(BigDecimal FACDVT_descuento) {
        this.FACDVT_descuento = FACDVT_descuento;
    }

    public BigDecimal getFACDVT_impuesto() {
        return FACDVT_impuesto;
    }

    public void setFACDVT_impuesto(BigDecimal FACDVT_impuesto) {
        this.FACDVT_impuesto = FACDVT_impuesto;
    }

    public BigDecimal getFACDVT_neto() {
        return FACDVT_neto;
    }

    public void setFACDVT_neto(BigDecimal FACDVT_neto) {
        this.FACDVT_neto = FACDVT_neto;
    }

    public String getFACDVT_listaprecio() {
        return FACDVT_listaprecio;
    }

    public void setFACDVT_listaprecio(String FACDVT_listaprecio) {
        this.FACDVT_listaprecio = FACDVT_listaprecio;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + Objects.hashCode(this.GENCIA_codigo);
        hash = 61 * hash + Objects.hashCode(this.GENOFI_codigo);
        hash = 61 * hash + Objects.hashCode(this.GENBOD_codigo);
        hash = 61 * hash + Objects.hashCode(this.INVITM_codigo);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TemDevolucionFactura other = (TemDevolucionFactura) obj;
        if (!Objects.equals(this.GENCIA_codigo, other.GENCIA_codigo)) {
            return false;
        }
        if (!Objects.equals(this.GENOFI_codigo, other.GENOFI_codigo)) {
            return false;
        }
        if (!Objects.equals(this.GENBOD_codigo, other.GENBOD_codigo)) {
            return false;
        }
        if (!Objects.equals(this.INVITM_codigo, other.INVITM_codigo)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "TemDevolucionFactura{" + "GENCIA_codigo=" + GENCIA_codigo + ", GENOFI_codigo=" + GENOFI_codigo + ", GENBOD_codigo=" + GENBOD_codigo + ", INVITM_codigo=" + INVITM_codigo + ",facdpdCantfuncionalItem " + FACDVT_cantfuncional + '}';
    }

    public String getNombreItem() {
        return nombreItem;
    }

    public void setNombreItem(String nombreItem) {
        this.nombreItem = nombreItem;
    }

    public BigDecimal getPrecioImporte() {
        return (this.FACDVT_precio.divide((this.FACDPD_cantfuncional==null||this.FACDPD_cantfuncional.equals(BigDecimal.ZERO))?BigDecimal.ONE:this.FACDPD_cantfuncional)).multiply(this.FACDVT_cantfuncional).setScale(3, RoundingMode.HALF_UP);
    }

    public BigDecimal getsubTotalImporte() {
        return (this.FACDVT_subtotal.divide((this.FACDPD_cantfuncional==null||this.FACDPD_cantfuncional.equals(BigDecimal.ZERO))?BigDecimal.ONE:this.FACDPD_cantfuncional)).multiply(this.FACDVT_cantfuncional).setScale(3, RoundingMode.HALF_UP);
    }

    public BigDecimal getDescuentoImporte() {
        return (this.FACDVT_descuento.divide((this.FACDPD_cantfuncional==null||this.FACDPD_cantfuncional.equals(BigDecimal.ZERO))?BigDecimal.ONE:this.FACDPD_cantfuncional)).multiply(this.FACDVT_cantfuncional).setScale(3, RoundingMode.HALF_UP);
    }

    public BigDecimal getImpuestoImporte() {
        return (this.FACDVT_impuesto.divide((this.FACDPD_cantfuncional==null||this.FACDPD_cantfuncional.equals(BigDecimal.ZERO))?BigDecimal.ONE:this.FACDPD_cantfuncional)).multiply(this.FACDVT_cantfuncional).setScale(3, RoundingMode.HALF_UP);
    }

    public BigDecimal getNetoImporte() {
        return (this.FACDVT_neto.divide((this.FACDPD_cantfuncional==null||this.FACDPD_cantfuncional.equals(BigDecimal.ZERO))?BigDecimal.ONE:this.FACDPD_cantfuncional)).multiply(this.FACDVT_cantfuncional).setScale(3, RoundingMode.HALF_UP);
    }

    public BigDecimal getFACDVT_cantfundev() {
        return FACDVT_cantfundev;
    }

    public void setFACDVT_cantfundev(BigDecimal FACDVT_cantfundev) {
        this.FACDVT_cantfundev = FACDVT_cantfundev;
    }

    public BigDecimal getCantidadFuncional() {
        return (this.FACDPD_cantfuncional==null?BigDecimal.ZERO:this.FACDPD_cantfuncional).subtract(this.FACDVT_cantfundev == null?BigDecimal.ZERO:this.FACDVT_cantfundev);
    }
}
