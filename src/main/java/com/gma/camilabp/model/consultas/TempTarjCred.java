/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.model.consultas;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author userdb5
 */
public class TempTarjCred implements Serializable{
    
    private static final long serialVersionUID = 1L;
    
    private Integer ID_secuencia;
    private Integer CXCDEU_numesecuen;
    private Integer CXCCCR_numsecuen;
    private Integer CXCDPG_numlinea;
    private BigDecimal CXCDEU_valorpago;
    private String CXCCLI_nombre;
    private String CXCDPG_secubanco;

    public Integer getID_secuencia() {
        return ID_secuencia;
    }

    public void setID_secuencia(Integer ID_secuencia) {
        this.ID_secuencia = ID_secuencia;
    }

    public Integer getCXCDEU_numesecuen() {
        return CXCDEU_numesecuen;
    }

    public void setCXCDEU_numesecuen(Integer CXCDEU_numesecuen) {
        this.CXCDEU_numesecuen = CXCDEU_numesecuen;
    }

    public Integer getCXCCCR_numsecuen() {
        return CXCCCR_numsecuen;
    }

    public void setCXCCCR_numsecuen(Integer CXCCCR_numsecuen) {
        this.CXCCCR_numsecuen = CXCCCR_numsecuen;
    }

    public Integer getCXCDPG_numlinea() {
        return CXCDPG_numlinea;
    }

    public void setCXCDPG_numlinea(Integer CXCDPG_numlinea) {
        this.CXCDPG_numlinea = CXCDPG_numlinea;
    }

    public BigDecimal getCXCDEU_valorpago() {
        return CXCDEU_valorpago;
    }

    public void setCXCDEU_valorpago(BigDecimal CXCDEU_valorpago) {
        this.CXCDEU_valorpago = CXCDEU_valorpago;
    }

    public String getCXCCLI_nombre() {
        return CXCCLI_nombre;
    }

    public void setCXCCLI_nombre(String CXCCLI_nombre) {
        this.CXCCLI_nombre = CXCCLI_nombre;
    }

    public String getCXCDPG_secubanco() {
        return CXCDPG_secubanco;
    }

    public void setCXCDPG_secubanco(String CXCDPG_secubanco) {
        this.CXCDPG_secubanco = CXCDPG_secubanco;
    }
    
}
