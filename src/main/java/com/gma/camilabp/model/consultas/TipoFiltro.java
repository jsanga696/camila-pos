/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.model.consultas;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author userdb6
 */
public class TipoFiltro implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private String codigo;
    private String dato;
    private List<TipoFiltro> lista= new ArrayList<>();
    private List<InveRepdiasInventario> listaItem;
    private BigDecimal caja= BigDecimal.ZERO;
    private BigDecimal unidades= BigDecimal.ZERO;
    private BigDecimal funcional= BigDecimal.ZERO;
    private BigDecimal cajasugerido= BigDecimal.ZERO;
    private BigDecimal cajarecibido= BigDecimal.ZERO;
    private BigDecimal cajacompra= BigDecimal.ZERO;
    private BigDecimal unidadcompra= BigDecimal.ZERO;

    public TipoFiltro() {
    }

    public TipoFiltro(String codigo, String dato) {
        this.codigo = codigo;
        this.dato = dato;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDato() {
        return dato;
    }

    public void setDato(String dato) {
        this.dato = dato;
    }

    public List<TipoFiltro> getLista() {
        return lista;
    }

    public void setLista(List<TipoFiltro> lista) {
        this.lista = lista;
    }

    public List<InveRepdiasInventario> getListaItem() {
        return listaItem;
    }

    public void setListaItem(List<InveRepdiasInventario> listaItem) {
        this.listaItem = listaItem;
    }

    public BigDecimal getCaja() {
        return caja;
    }

    public void setCaja(BigDecimal caja) {
        this.caja = caja;
    }

    public BigDecimal getUnidades() {
        return unidades;
    }

    public void setUnidades(BigDecimal unidades) {
        this.unidades = unidades;
    }

    public BigDecimal getFuncional() {
        return funcional;
    }

    public void setFuncional(BigDecimal funcional) {
        this.funcional = funcional;
    }

    public BigDecimal getCajasugerido() {
        return cajasugerido;
    }

    public void setCajasugerido(BigDecimal cajasugerido) {
        this.cajasugerido = cajasugerido;
    }

    public BigDecimal getCajarecibido() {
        return cajarecibido;
    }

    public void setCajarecibido(BigDecimal cajarecibido) {
        this.cajarecibido = cajarecibido;
    }

    public BigDecimal getCajacompra() {
        return cajacompra;
    }

    public void setCajacompra(BigDecimal cajacompra) {
        this.cajacompra = cajacompra;
    }

    public BigDecimal getUnidadcompra() {
        return unidadcompra;
    }

    public void setUnidadcompra(BigDecimal unidadcompra) {
        this.unidadcompra = unidadcompra;
    }
    
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + Objects.hashCode(this.codigo);
        hash = 71 * hash + Objects.hashCode(this.dato);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TipoFiltro other = (TipoFiltro) obj;
        if (!Objects.equals(this.codigo.trim(), other.codigo.trim())) {
            return false;
        }
        return true;
    }
    
}
