/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.model.consultas;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author userdb6
 */
public class Importe implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private BigDecimal venta;
    private BigDecimal devolucion;
    private BigDecimal impuesto;
    private BigDecimal descuento;
    private BigDecimal ingreso;
    private BigDecimal gasto;
    private BigDecimal total;
    private BigDecimal ingreso_adicional;
    private BigDecimal cierre;

    public Importe() {
    }

    public BigDecimal getVenta() {
        return venta;
    }

    public void setVenta(BigDecimal venta) {
        this.venta = venta;
    }

    public BigDecimal getDevolucion() {
        return devolucion;
    }

    public void setDevolucion(BigDecimal devolucion) {
        this.devolucion = devolucion;
    }

    public BigDecimal getImpuesto() {
        return impuesto;
    }

    public void setImpuesto(BigDecimal impuesto) {
        this.impuesto = impuesto;
    }

    public BigDecimal getDescuento() {
        return descuento;
    }

    public void setDescuento(BigDecimal descuento) {
        this.descuento = descuento;
    }

    public BigDecimal getIngreso() {
        return ingreso;
    }

    public void setIngreso(BigDecimal ingreso) {
        this.ingreso = ingreso;
    }

    public BigDecimal getGasto() {
        return gasto;
    }

    public void setGasto(BigDecimal gasto) {
        this.gasto = gasto;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public BigDecimal getIngreso_adicional() {
        return ingreso_adicional;
    }

    public void setIngreso_adicional(BigDecimal ingreso_adicional) {
        this.ingreso_adicional = ingreso_adicional;
    }

    public BigDecimal getCierre() {
        return cierre;
    }

    public void setCierre(BigDecimal cierre) {
        this.cierre = cierre;
    }
    
}
