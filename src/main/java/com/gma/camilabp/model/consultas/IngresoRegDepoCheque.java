/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.model.consultas;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author userdb5
 */
public class IngresoRegDepoCheque implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private String GENCIA_codigo;
    private String GENOFI_codigo;
    private Date FECHA_REGISTRO;
    private String SEGUSU_codigo;
    private String SEGNAQ_codigo;
    private String BANENT_codigo;
    private String BANCTA_codigo;
    private String NUM_PAPELETA;
    private String NAMETABLE;

    public String getGENCIA_codigo() {
        return GENCIA_codigo;
    }

    public void setGENCIA_codigo(String GENCIA_codigo) {
        this.GENCIA_codigo = GENCIA_codigo;
    }

    public String getGENOFI_codigo() {
        return GENOFI_codigo;
    }

    public void setGENOFI_codigo(String GENOFI_codigo) {
        this.GENOFI_codigo = GENOFI_codigo;
    }

    public Date getFECHA_REGISTRO() {
        return FECHA_REGISTRO;
    }

    public void setFECHA_REGISTRO(Date FECHA_REGISTRO) {
        this.FECHA_REGISTRO = FECHA_REGISTRO;
    }

    public String getSEGUSU_codigo() {
        return SEGUSU_codigo;
    }

    public void setSEGUSU_codigo(String SEGUSU_codigo) {
        this.SEGUSU_codigo = SEGUSU_codigo;
    }

    public String getSEGNAQ_codigo() {
        return SEGNAQ_codigo;
    }

    public void setSEGNAQ_codigo(String SEGNAQ_codigo) {
        this.SEGNAQ_codigo = SEGNAQ_codigo;
    }

    public String getBANENT_codigo() {
        return BANENT_codigo;
    }

    public void setBANENT_codigo(String BANENT_codigo) {
        this.BANENT_codigo = BANENT_codigo;
    }

    public String getBANCTA_codigo() {
        return BANCTA_codigo;
    }

    public void setBANCTA_codigo(String BANCTA_codigo) {
        this.BANCTA_codigo = BANCTA_codigo;
    }

    public String getNUM_PAPELETA() {
        return NUM_PAPELETA;
    }

    public void setNUM_PAPELETA(String NUM_PAPELETA) {
        this.NUM_PAPELETA = NUM_PAPELETA;
    }

    public String getNAMETABLE() {
        return NAMETABLE;
    }

    public void setNAMETABLE(String NAMETABLE) {
        this.NAMETABLE = NAMETABLE;
    }
    
}
