/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.activos.reportes;

import com.gma.camilabp.domain.CbpactSolicitudEquipo;
import com.gma.camilabp.model.logica.ProductividadModel;
import com.gma.camilabp.web.lazy.CbpactSolicitudEquipoLazy;
import com.gma.camilabp.web.lazy.CbpgenrOficinaLazyTest;
import com.gma.camilabp.web.lazy.CxcmClienteLazy;
import com.gma.camilabp.web.service.ActivosEquiposLocal;
import com.gma.camilabp.web.service.BitacoraLocal;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.service.GeneralLocal;
import com.gma.camilabp.web.session.ServletSession;
import com.gma.camilabp.web.session.UserSession;
import com.gma.camilabp.web.util.JsfUtil;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author userdb5
 */
@Named(value = "productividadView")
@ViewScoped
public class ProductividadView implements Serializable {

    @Inject
    private UserSession session;

    @EJB
    private EntityManagerLocal entity;
    @EJB
    private ActivosEquiposLocal activosServices;
    @EJB
    private BitacoraLocal bitacora;
    @EJB
    private GeneralLocal generalEjb;

    @Inject
    private ServletSession datosSession;

    private HashMap dataIngreso;
    private CbpgenrOficinaLazyTest oficinasLazy;
    private CxcmClienteLazy clientes;
    private HashMap m;
    private CbpactSolicitudEquipoLazy solicitudes;
    private List<CbpactSolicitudEquipo> consignaciones;
    private List<Integer> tipos, estados;
    private List<ProductividadModel> l;

    @PostConstruct
    public void initView() {
        try {
            dataIngreso = new HashMap();
            dataIngreso.put("fechaInicio", new Date());
            dataIngreso.put("fechaFin", new Date());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void verDatos() {
        try {
            HashMap m = new HashMap();
            m.put("0", (Date)dataIngreso.get("fechaInicio"));
            m.put("1", (Date)dataIngreso.get("fechaFin"));
            
            l = (List<ProductividadModel>) entity.findAllSQL("SELECT rango.clase_cliente, A.* FROM ( \n"
                    + "SELECT cliente.CXCCLI_razonsocia as cliente, ROUND(SUM(venta.FACCVT_netomov),2) as suma FROM CBPACT_EQUIPO equipo \n"
                    + "INNER JOIN CBPACT_SOLICITUD_EQUIPO solicitud ON equipo.id = solicitud.equipo \n"
                    + "INNER JOIN CBPGENR_OFICINA oficina ON equipo.genofi_codigo = oficina.genofi_codigo \n"
                    + "INNER JOIN CBPGENR_COMPANIA compania ON compania.gencia_codigo = oficina.gencia_codigo \n"
                    + "INNER JOIN cxcm_cliente cliente ON solicitud.cxcm_cliente_solicitante = cliente.CXCCLI_codigo \n"
                    + "AND cliente.GENCIA_codigo = compania.gencia_secuencia \n"
                    + "AND cliente.GENOFI_codigo = oficina.genofi_secuencia \n"
                    + "INNER JOIN FACT_CABVENTA venta ON venta.GENCIA_codigo = compania.gencia_secuencia AND venta.GENOFI_codigo = oficina.genofi_secuencia \n"
                    + "AND venta.CXCCLI_codigo = cliente.CXCCLI_codigo \n"
                    + "WHERE venta.GENTDO_codigo = 'FAC' AND venta.FACCVT_fechaemisi BETWEEN ? AND ? \n"
                    + "GROUP BY CXCCLI_razonsocia) AS A \n"
                    + "INNER JOIN CBPACT_RANGOS_CLIENTE rango ON A.suma BETWEEN rango.valor_min AND rango.valor_max \n"
                    + "ORDER BY 1,3 DESC", m, ProductividadModel.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void generarReporte() {
        datosSession.borrarDatos();
        datosSession.setTieneDatasource(Boolean.FALSE);
        datosSession.setNombreReporte("pos/activos/productividad");

        datosSession.agregarParametro("FECHA_INICIO", (Date) this.dataIngreso.get("fechaInicio"));
        datosSession.agregarParametro("FECHA_FIN", (Date) this.dataIngreso.get("fechaFin"));
        JsfUtil.redirectNewTab("/CamilaPOS/DocumentoXls");
    }

    public HashMap getDataIngreso() {
        return dataIngreso;
    }

    public void setDataIngreso(HashMap dataIngreso) {
        this.dataIngreso = dataIngreso;
    }

    public List<ProductividadModel> getL() {
        return l;
    }

    public void setL(List<ProductividadModel> l) {
        this.l = l;
    }

}
