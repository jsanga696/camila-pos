/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.activos.equipos;

import com.gma.camilabo.enums.ActionsTransaccion;
import com.gma.camilabp.domain.CbpactEquipo;
import com.gma.camilabp.domain.CbpactSolicitudEquipo;
import com.gma.camilabp.domain.CbpgenrOficina;
import com.gma.camilabp.domain.CxcmCliente;
import com.gma.camilabp.domain.CxpmProveedor;
import com.gma.camilabp.domain.CxpmProveedorPK;
import com.gma.camilabp.web.comercial.compras.IngresoBodegaView;
import com.gma.camilabp.web.lazy.CbpactEquipoLazy;
import com.gma.camilabp.web.lazy.CbpactSolicitudEquipoLazy;
import com.gma.camilabp.web.lazy.CbpgenrOficinaLazyTest;
import com.gma.camilabp.web.lazy.CxcmClienteLazy;
import com.gma.camilabp.web.service.ActivosEquiposLocal;
import com.gma.camilabp.web.service.BitacoraLocal;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.service.GeneralLocal;
import com.gma.camilabp.web.session.UserSession;
import com.gma.camilabp.web.util.JsfUtil;
import com.gma.camilabp.web.util.Utilidades;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author userdb5
 */
@Named(value = "solicitudEquipoView")
@ViewScoped
public class SolicitudEquipoView implements Serializable {

    @Inject
    private UserSession session;
    @EJB
    private EntityManagerLocal entity;
    @EJB
    private ActivosEquiposLocal activosServices;
    @EJB
    private GeneralLocal generalEjb;
    @EJB
    private BitacoraLocal bitacora;

    private HashMap m;
    private CbpactEquipoLazy equiposLazy;
    private HashMap dataIngreso;
    private CbpgenrOficinaLazyTest oficinasLazy;
    private CbpactSolicitudEquipo solicitud;
    private CxcmClienteLazy clientes;
    private CbpactSolicitudEquipoLazy solicitudes;
    private List<Integer> tipos, estados;

    @PostConstruct
    public void initView() {
        m = new HashMap();
        dataIngreso = new HashMap();
        this.oficinasLazy = new CbpgenrOficinaLazyTest(Utilidades.getIdsOfList(this.session.getOficinas()));
        dataIngreso.put("oficina", (CbpgenrOficina) Utilidades.getFirstElement(this.session.getOficinas()));
        dataIngreso.put("proveedor", new CxpmProveedor());
        dataIngreso.put("valOfi", ((CbpgenrOficina) dataIngreso.get("oficina")).getGenofiSecuencia());
        dataIngreso.put("valProv", "");
        dataIngreso.put("valEq", "");
        dataIngreso.put("valSol", "");
        dataIngreso.put("solicitudInfo", new CbpactSolicitudEquipo());
        dataIngreso.put("cliente", new CxcmCliente());
        dataIngreso.put("equipo", new CbpactEquipo());
        this.equiposLazy = new CbpactEquipoLazy();
        tipos = new ArrayList();
        tipos.add(1);
        tipos.add(2);
        tipos.add(3);
        estados = new ArrayList();
        estados.add(1);
        estados.add(2);
        estados.add(3);
        estados.add(4);
        this.solicitudes = new CbpactSolicitudEquipoLazy(estados, tipos, Utilidades.getIdsOfList(this.session.getOficinas()));
        m.put("1", ((CbpgenrOficina) dataIngreso.get("oficina")).getGenofiSecuencia());
        m.put("0", session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia());
        solicitud = new CbpactSolicitudEquipo();
        clientes = new CxcmClienteLazy(((CbpgenrOficina) dataIngreso.get("oficina")).getGenofiSecuencia(),
                session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia());
    }

    public void ingresar() {
        if (((CbpactEquipo) dataIngreso.get("equipo")).getId() == null || ((CxcmCliente) dataIngreso.get("cliente")).getCxcmClientePK().getCXCCLIcodigo() == null) {
            JsfUtil.messageInfo(null, "Debse seleccionar un cliente y un equipo antes de proceder.", "");
            return;
        } else {
            HashMap m = new HashMap();
            CbpactEquipo eq = (CbpactEquipo) dataIngreso.get("equipo");

            switch (solicitud.getTipoSolicitud()) {
                case 1:
                    List<Integer> estado = new ArrayList();
                    estado.add(1);
                    estado.add(2);
                    m.put("tipoSolicitud", 1);
                    m.put("estado", estado);
                    m.put("equipo", dataIngreso.get("equipo"));
                    if (entity.findObject("HQL", "SELECT e FROM CbpactSolicitudEquipo e WHERE "
                            + "e.tipoSolicitud = :tipoSolicitud AND e.estado IN :estado AND e.equipo = :equipo", m, CbpactSolicitudEquipo.class, Boolean.TRUE) != null) {
                        JsfUtil.messageInfo(null, "El equipo se encuentra en proceso de consignacion o consignado.", "");
                        return;
                    }
                    break;

                case 2:
                    eq.setEstado(5);
                    break;
                    
                case 3:
                    m.put("estado", 4);
                    m.put("placa", dataIngreso.get("valEq"));
                    if (entity.findObject(CbpactEquipo.class, m) == null) {
                        JsfUtil.messageInfo(null, "El equipo no se encuentra consignado a ningún cliente.", "");
                        return;
                    }
                    eq.setEstado(3);
                    break;
            }

            solicitud.setFechaIngreso(new Date());
            solicitud.setEquipo((CbpactEquipo) dataIngreso.get("equipo"));
            solicitud.setCxcmClienteSolicitante(((CxcmCliente) dataIngreso.get("cliente")).getCxcmClientePK().getCXCCLIcodigo());
            solicitud = (CbpactSolicitudEquipo) entity.merge(solicitud);
            entity.merge(eq);
            solicitud = new CbpactSolicitudEquipo();
            dataIngreso.put("valEq", "");
            dataIngreso.put("valSol", "");
            dataIngreso.put("cliente", new CxcmCliente());
            dataIngreso.put("equipo", new CbpactEquipo());
            JsfUtil.update("mainForm");
            JsfUtil.executeJS("PF('dlgSolicitud').hide()");
            JsfUtil.messageInfo(null, "Solicitud ingresada correctamente", "");
            bitacora.saveActionLog(ActionsTransaccion.INGRESO_SOLICITUD_EQUIPO, "" + solicitud.getId());
        }
    }

    public void setLazy() {
        if (this.dataIngreso.get("oficina") != null && !this.dataIngreso.get("valProv").equals("")) {

        }
    }

    public String getNombreProveedor(String cod) {
        m.put("2", cod);
        return "" + entity.findObject("SQL", "SELECT s.CXPPRO_razonsocia FROM CXPM_PROVEEDOR s WHERE s.GENCIA_codigo = ? AND "
                + "GENOFI_codigo = ? AND CXPPRO_codigo = ?", m, null, false);

    }

    public void selectOficina(SelectEvent event) {
        dataIngreso.put("valOfi", ((CbpgenrOficina) dataIngreso.get("oficina")).getGenofiSecuencia());
        JsfUtil.update("mainForm:secuenciaOficina");
        m.put("1", ((CbpgenrOficina) dataIngreso.get("oficina")).getGenofiSecuencia());
        clientes = new CxcmClienteLazy(Long.parseLong(((CbpgenrOficina) dataIngreso.get("oficina")).getGenofiSecuencia() + ""),
                Long.parseLong("" + session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia()));
    }

    public void busquedaOficina() {
        try {
            Object o = this.generalEjb.consultarOficina((String) this.dataIngreso.get("valOfi"));
            if (o != null) {
                this.dataIngreso.put("oficina", (CbpgenrOficina) o);
                m.put("1", ((CbpgenrOficina) dataIngreso.get("oficina")).getGenofiSecuencia());
                clientes = new CxcmClienteLazy(Long.parseLong(((CbpgenrOficina) dataIngreso.get("oficina")).getGenofiSecuencia() + ""),
                        Long.parseLong("" + session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia()));
            } else {
                JsfUtil.update("frmdlgOficina");
                JsfUtil.executeJS("PF('dlgOficina').show();");
            }
        } catch (Exception e) {
            Logger.getLogger(IngresoBodegaView.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public void setValEq() {
        HashMap m = new HashMap();
        m.put("estado", 1);
        m.put("equipo", dataIngreso.get("equipo"));
        List<CbpactSolicitudEquipo> solicitudes = entity.findAllByParameter(CbpactSolicitudEquipo.class, m, "id");

        if (solicitudes != null && !solicitudes.isEmpty()) {
            dataIngreso.put("equipo", new CbpactEquipo());
            JsfUtil.messageInfo(null, "El equipo tiene solicitudes pendientes de revision", "");
            return;
        }
        dataIngreso.put("valEq", ((CbpactEquipo) dataIngreso.get("equipo")).getPlaca());
    }

    public void setValSol() {
        dataIngreso.put("valSol", ((CxcmCliente) dataIngreso.get("cliente")).getCxcmClientePK().getCXCCLIcodigo());
    }

    public void busquedaSolicitante() {
        HashMap m = new HashMap();
        m.put("CXCCLIcodigo", dataIngreso.get("valSol"));

        CxcmCliente eq = (CxcmCliente) entity.findObject("HQL", "SELECT m FROM CxcmCliente m WHERE m.cxcmClientePK.cXCCLIcodigo = :CXCCLIcodigo",
                m, CxcmCliente.class, Boolean.TRUE);

        if (eq != null) {
            dataIngreso.put("cliente", eq);
        } else {
            JsfUtil.update("frmdlgSolicitante");
            JsfUtil.executeJS("PF('dlgSolicitante').show();");
        }
    }

    public void busquedaEquipo() {

        HashMap m = new HashMap();
        m.put("placa", dataIngreso.get("valEq"));

        CbpactEquipo eq = (CbpactEquipo) entity.findObject("HQL", "SELECT m FROM CbpactEquipo m WHERE m.placa = :placa",
                m, CbpactEquipo.class, Boolean.TRUE);

        if (eq != null) {
            m.put("estado", 4);
            m.put("placa", eq.getPlaca());
            if (entity.findObject(CbpactEquipo.class, m) != null) {
                JsfUtil.messageInfo(null, "El equipo se encuentra consignado.", "");
                return;
            }
            m = new HashMap();
            m.put("estado", 1);
            m.put("equipo", eq);
            List<CbpactSolicitudEquipo> solicitudes = entity.findAllByParameter(CbpactSolicitudEquipo.class, m, "id");

            if (solicitudes != null && !solicitudes.isEmpty()) {
                JsfUtil.messageInfo(null, "El equipo tiene solicitudes de consignacion pendientes de revision", "");
                return;
            }

            dataIngreso.put("equipo", eq);
        } else {
            JsfUtil.update("frmdlgEquipo");
            JsfUtil.executeJS("PF('wdEquipos').show();");
        }
    }

    public void busquedaProveedor() {
        CxpmProveedorPK pk = new CxpmProveedorPK(session.getCaja().getGENCIAcodigo(), ((CbpgenrOficina) dataIngreso.get("oficina")).getGenofiSecuencia(), (String) dataIngreso.get("valProv"));
        CxpmProveedor cp = entity.find(CxpmProveedor.class, pk);
        if (cp != null) {
            dataIngreso.put("valProv", cp.getCxpmProveedorPK().getCXPPROcodigo());
            dataIngreso.put("proveedor", cp);
        } else {
            JsfUtil.update("proveedoresFrm");
            JsfUtil.executeJS("PF('wdProveedores').show();");
        }
    }

    public CbpactEquipoLazy getEquiposLazy() {
        return equiposLazy;
    }

    public void setEquiposLazy(CbpactEquipoLazy equiposLazy) {
        this.equiposLazy = equiposLazy;
    }

    public HashMap getDataIngreso() {
        return dataIngreso;
    }

    public void setDataIngreso(HashMap dataIngreso) {
        this.dataIngreso = dataIngreso;
    }

    public CbpgenrOficinaLazyTest getOficinasLazy() {
        return oficinasLazy;
    }

    public void setOficinasLazy(CbpgenrOficinaLazyTest oficinasLazy) {
        this.oficinasLazy = oficinasLazy;
    }

    public CbpactSolicitudEquipo getSolicitud() {
        return solicitud;
    }

    public void setSolicitud(CbpactSolicitudEquipo solicitud) {
        this.solicitud = solicitud;
    }

    public CxcmClienteLazy getClientes() {
        return clientes;
    }

    public void setClientes(CxcmClienteLazy clientes) {
        this.clientes = clientes;
    }

    public CbpactSolicitudEquipoLazy getSolicitudes() {
        return solicitudes;
    }

    public void setSolicitudes(CbpactSolicitudEquipoLazy solicitudes) {
        this.solicitudes = solicitudes;
    }

}
