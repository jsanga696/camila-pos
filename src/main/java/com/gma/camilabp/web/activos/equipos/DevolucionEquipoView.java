/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.activos.equipos;

import com.gma.camilabo.enums.ActionsTransaccion;
import com.gma.camilabp.domain.CbpactConsignacion;
import com.gma.camilabp.domain.CbpactDevolucion;
import com.gma.camilabp.domain.CbpactEquipo;
import com.gma.camilabp.domain.CbpactMantenimiento;
import com.gma.camilabp.domain.CbpactSolicitudEquipo;
import com.gma.camilabp.domain.CbpgenrOficina;
import com.gma.camilabp.domain.CbpsegmUsuario;
import com.gma.camilabp.domain.CxpmProveedor;
import com.gma.camilabp.domain.CxpmProveedorPK;
import com.gma.camilabp.domain.MEmpleados;
import com.gma.camilabp.web.comercial.compras.IngresoBodegaView;
import com.gma.camilabp.web.lazy.CbpactSolicitudEquipoLazy;
import com.gma.camilabp.web.lazy.CbpgenrOficinaLazyTest;
import com.gma.camilabp.web.lazy.CxcmClienteLazy;
import com.gma.camilabp.web.lazy.MEmpleadosLazy;
import com.gma.camilabp.web.service.ActivosEquiposLocal;
import com.gma.camilabp.web.service.BitacoraLocal;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.service.GeneralLocal;
import com.gma.camilabp.web.session.ServletSession;
import com.gma.camilabp.web.session.UserSession;
import com.gma.camilabp.web.util.JsfUtil;
import com.gma.camilabp.web.util.Utilidades;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author userdb5
 */
@Named(value = "devolucionEquipoView")
@ViewScoped
public class DevolucionEquipoView implements Serializable {

    @Inject
    private UserSession session;
    @Inject
    private ServletSession datosSession;

    @EJB
    private EntityManagerLocal entity;
    @EJB
    private ActivosEquiposLocal activosServices;
    @EJB
    private GeneralLocal generalEjb;
    @EJB
    private BitacoraLocal bitacora;

    private HashMap m, dataIngreso;
    private CbpgenrOficinaLazyTest oficinasLazy;
    private CxcmClienteLazy clientes;
    private CbpactSolicitudEquipoLazy solicitudes, solicitudesConResolucion;
    private List<Integer> tipos, estados, estados2;
    private CbpactDevolucion devolucion, devolucionInfo;
    private MEmpleadosLazy empleadosLazy;

    @PostConstruct
    public void initView() {
        m = new HashMap();
        dataIngreso = new HashMap();
        dataIngreso.put("oficina", (CbpgenrOficina) Utilidades.getFirstElement(this.session.getOficinas()));
        dataIngreso.put("proveedor", new CxpmProveedor());
        dataIngreso.put("valOfi", ((CbpgenrOficina) dataIngreso.get("oficina")).getGenofiSecuencia());
        dataIngreso.put("valProv", "");
        dataIngreso.put("empleadoResolucion", new MEmpleados());
        dataIngreso.put("solicitud", new CbpactSolicitudEquipo());
        dataIngreso.put("solicitudInfo", new CbpactSolicitudEquipo());
        dataIngreso.put("equipo", new CbpactEquipo());
        this.oficinasLazy = new CbpgenrOficinaLazyTest(Utilidades.getIdsOfList(this.session.getOficinas()));
        clientes = new CxcmClienteLazy(((CbpgenrOficina) dataIngreso.get("oficina")).getGenofiSecuencia(),
                session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia());

        tipos = new ArrayList();
        tipos.add(3);
        estados = new ArrayList();
        estados.add(1);
        this.solicitudes = new CbpactSolicitudEquipoLazy(estados, tipos, Utilidades.getIdsOfList(this.session.getOficinas()));

        estados2 = new ArrayList();
        estados2.add(2);
        estados2.add(3);
        estados2.add(4);
        estados2.add(5);
        estados2.add(6);
        empleadosLazy = new MEmpleadosLazy(((CbpgenrOficina) dataIngreso.get("oficina")).getGenofiSecuencia(),
                session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia());
        solicitudesConResolucion = new CbpactSolicitudEquipoLazy(estados2, tipos, Utilidades.getIdsOfList(this.session.getOficinas()));
        devolucion = new CbpactDevolucion();
    }

    public void initSolicitud() {
        devolucion = new CbpactDevolucion();
        this.dataIngreso.put("solicitud", new CbpactSolicitudEquipo());
        this.solicitudes = new CbpactSolicitudEquipoLazy(estados, tipos, Utilidades.getIdsOfList(this.session.getOficinas()));
        dataIngreso.put("empleadoResolucion", new MEmpleados());
        dataIngreso.put("proveedor", new CxpmProveedor());
        JsfUtil.update("mainForm");
        JsfUtil.messageInfo(null, "Mantenimiento guardado correctamente", "");
    }

    public void aceptarSolicitud() {

        if (this.devolucion.getObservacion() == null) {
            JsfUtil.messageInfo(null, "Debe ingresar una observacion", "");
            return;
        }

        this.devolucion.setObservacion(this.devolucion.getObservacion().toUpperCase());
        this.devolucion.setSolicitud((CbpactSolicitudEquipo) this.dataIngreso.get("solicitud"));
        this.devolucion.setFechaIngreso(new Date());
        this.devolucion.setUsuarioIngreso(session.getCbpsegmUsuario().getSegusuCodigo());
        this.devolucion.setEstado(1);
        this.devolucion.setUsuarioResolucion(Long.parseLong("" + ((MEmpleados) this.dataIngreso.get("empleadoResolucion")).getMEmpleadosPK().getNOMEMPcodigo()));

        CbpactSolicitudEquipo sol = (CbpactSolicitudEquipo) dataIngreso.get("solicitud");
        sol.setUsuarioAprobacion(Long.parseLong("" + ((MEmpleados) this.dataIngreso.get("empleadoResolucion")).getMEmpleadosPK().getNOMEMPcodigo()));
        sol.setEstado(2);
        sol.setFechaAprobacion(new Date());
        this.entity.merge(sol);

        List<CbpactSolicitudEquipo> consignaciones = entity.findListByParameters("SELECT m FROM CbpactSolicitudEquipo m WHERE m.equipo = :equipo AND m.estado = :estado AND m.tipoSolicitud = :tipoSolicitud",
                 new String[]{"equipo", "estado", "tipoSolicitud"}, new Object[]{sol.getEquipo(), 2, 1});

        for (CbpactSolicitudEquipo t : consignaciones) {
            t.setEstado(4);
            this.entity.merge(t);
        }

        CbpactEquipo temp = (CbpactEquipo) sol.getEquipo();
        temp.setEstado(1);
        this.entity.merge(temp);

        this.devolucion = (CbpactDevolucion) this.entity.merge(this.devolucion);
        bitacora.saveActionLog(ActionsTransaccion.ACEPTAR_SOLICITUD, "" + sol.getId());
        initSolicitud();
    }

    public void setearSolicitudAnterior() {
        if (((CbpactSolicitudEquipo) this.dataIngreso.get("solicitudInfo")).getId() != null) {
            this.devolucionInfo = ((CbpactSolicitudEquipo) this.dataIngreso.get("solicitudInfo")).getDevolucionesList().get(((CbpactSolicitudEquipo) this.dataIngreso.get("solicitudInfo")).getDevolucionesList().size() - 1);
        }
    }

    public void rechazarSolicitud() {
        try {
            CbpactSolicitudEquipo sol = (CbpactSolicitudEquipo) dataIngreso.get("solicitud");
            sol.setEstado(3);
            sol.setUsuarioAprobacion(Long.parseLong("" + ((MEmpleados) this.dataIngreso.get("empleadoResolucion")).getMEmpleadosPK().getNOMEMPcodigo()));
            sol.setFechaAprobacion(new Date());
            this.entity.merge(sol);
            JsfUtil.messageInfo(null, "Solicitud rechazada satisfactoriamente", "");
            bitacora.saveActionLog(ActionsTransaccion.RECHAZAR_SOLICITUD, "" + sol.getId());
            initSolicitud();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void aceptarMatenimiento() {
        if (((CbpactSolicitudEquipo) dataIngreso.get("solicitud")).getId() == null) {
            JsfUtil.messageInfo(null, "Debe seleccionar el equipo antes de devolver", "");
            return;
        } else {
            devolucion = new CbpactDevolucion();
        }
    }

    public void setEmpleado(MEmpleados emp) {
        if (emp != null) {
            JsfUtil.messageInfo(null, "Empleado seleccionado correctamente", "");
            this.dataIngreso.put("empleadoResolucion", emp);
        } else {
            JsfUtil.messageInfo(null, "No ha seleccionado al empleado", "");
        }
    }

    public void busquedaOficina() {
        try {
            Object o = this.generalEjb.consultarOficina((String) this.dataIngreso.get("valOfi"));
            if (o != null) {
                this.dataIngreso.put("oficina", (CbpgenrOficina) o);
                m.put("1", ((CbpgenrOficina) dataIngreso.get("oficina")).getGenofiSecuencia());
                clientes = new CxcmClienteLazy(Long.parseLong(((CbpgenrOficina) dataIngreso.get("oficina")).getGenofiSecuencia() + ""),
                        Long.parseLong("" + session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia()));
            } else {
                JsfUtil.update("frmdlgOficina");
                JsfUtil.executeJS("PF('dlgOficina').show();");
            }
        } catch (Exception e) {
            Logger.getLogger(IngresoBodegaView.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public void busquedaProveedor() {
        CxpmProveedorPK pk = new CxpmProveedorPK(session.getCaja().getGENCIAcodigo(), ((CbpgenrOficina) dataIngreso.get("oficina")).getGenofiSecuencia(), (String) dataIngreso.get("valProv"));
        CxpmProveedor cp = entity.find(CxpmProveedor.class, pk);
        if (cp != null) {
            dataIngreso.put("valProv", cp.getCxpmProveedorPK().getCXPPROcodigo());
            dataIngreso.put("proveedor", cp);
        } else {
            JsfUtil.update("proveedoresFrm");
            JsfUtil.executeJS("PF('wdProveedores').show();");
        }
    }

    public HashMap getDataIngreso() {
        return dataIngreso;
    }

    public void setDataIngreso(HashMap dataIngreso) {
        this.dataIngreso = dataIngreso;
    }

    public CbpgenrOficinaLazyTest getOficinasLazy() {
        return oficinasLazy;
    }

    public void setOficinasLazy(CbpgenrOficinaLazyTest oficinasLazy) {
        this.oficinasLazy = oficinasLazy;
    }

    public CxcmClienteLazy getClientes() {
        return clientes;
    }

    public void setClientes(CxcmClienteLazy clientes) {
        this.clientes = clientes;
    }

    public CbpactSolicitudEquipoLazy getSolicitudes() {
        return solicitudes;
    }

    public void setSolicitudes(CbpactSolicitudEquipoLazy solicitudes) {
        this.solicitudes = solicitudes;
    }

    public CbpactSolicitudEquipoLazy getSolicitudesConResolucion() {
        return solicitudesConResolucion;
    }

    public void setSolicitudesConResolucion(CbpactSolicitudEquipoLazy solicitudesConResolucion) {
        this.solicitudesConResolucion = solicitudesConResolucion;
    }

    public CbpactDevolucion getDevolucion() {
        return devolucion;
    }

    public void setDevolucion(CbpactDevolucion devolucion) {
        this.devolucion = devolucion;
    }

    public CbpactDevolucion getDevolucionInfo() {
        return devolucionInfo;
    }

    public void setDevolucionInfo(CbpactDevolucion devolucionInfo) {
        this.devolucionInfo = devolucionInfo;
    }

    public MEmpleadosLazy getEmpleadosLazy() {
        return empleadosLazy;
    }

    public void setEmpleadosLazy(MEmpleadosLazy empleadosLazy) {
        this.empleadosLazy = empleadosLazy;
    }

}
