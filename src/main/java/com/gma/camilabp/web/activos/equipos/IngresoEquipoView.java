/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.activos.equipos;

import com.gma.camilabo.enums.ActionsTransaccion;
import com.gma.camilabp.domain.CbpactEquipo;
import com.gma.camilabp.domain.CbpactModelEquipo;
import com.gma.camilabp.domain.CbpactSolicitudEquipo;
import com.gma.camilabp.domain.CbpgenrOficina;
import com.gma.camilabp.domain.CxpmProveedor;
import com.gma.camilabp.domain.CxpmProveedorPK;
import com.gma.camilabp.web.comercial.compras.IngresoBodegaView;
import com.gma.camilabp.web.lazy.CbpactEquipoLazy;
import com.gma.camilabp.web.lazy.CbpactModelEquipoLazy;
import com.gma.camilabp.web.lazy.CbpgenrOficinaLazyTest;
import com.gma.camilabp.web.lazy.CxpmProveedorLazy;
import com.gma.camilabp.web.service.ActivosEquiposLocal;
import com.gma.camilabp.web.service.BitacoraLocal;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.service.GeneralLocal;
import com.gma.camilabp.web.session.UserSession;
import com.gma.camilabp.web.util.JsfUtil;
import com.gma.camilabp.web.util.Utilidades;
import com.sun.javafx.scene.control.skin.VirtualFlow;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author userdb5
 */
@Named(value = "ingresoEquipoView")
@ViewScoped
public class IngresoEquipoView implements Serializable {

    @Inject
    private UserSession session;
    @EJB
    private EntityManagerLocal entity;
    @EJB
    private ActivosEquiposLocal activosServices;
    @EJB
    private GeneralLocal generalEjb;
    @EJB
    private BitacoraLocal bitacora;

    private CbpactModelEquipo equipo, marcaModeloEncontrado;
    private CbpactEquipo equipoEdit;
    private CbpactModelEquipoLazy modelsLazy;
    private HashMap dataIngreso;
    protected CxpmProveedorLazy proveedores;
    private CbpgenrOficinaLazyTest oficinasLazy;
    private HashMap m;
    private CbpactEquipo model;
    private List<CbpactModelEquipo> modelList;
    private List<CbpactEquipo> equiposList;
    private CbpactEquipoLazy equiposLazy;

    @PostConstruct
    public void initView() {
        m = new HashMap();

        dataIngreso = new HashMap();
        equipo = new CbpactModelEquipo();
        this.oficinasLazy = new CbpgenrOficinaLazyTest(Utilidades.getIdsOfList(this.session.getOficinas()));
        dataIngreso.put("oficina", (CbpgenrOficina) Utilidades.getFirstElement(this.session.getOficinas()));
        dataIngreso.put("proveedor", new CxpmProveedor());
        dataIngreso.put("valOfi", ((CbpgenrOficina) dataIngreso.get("oficina")).getGenofiSecuencia());
        dataIngreso.put("valProv", "");
        dataIngreso.put("valMarca", "");
        dataIngreso.put("valModelo", "");
        dataIngreso.put("valMarca2", "");
        dataIngreso.put("valModelo2", "");
        dataIngreso.put("cantidad", 0);
        this.llenarDatos();
        selectOficina(null);
        modelsLazy = new CbpactModelEquipoLazy();
        equiposLazy = new CbpactEquipoLazy();
        m.put("1", ((CbpgenrOficina) dataIngreso.get("oficina")).getGenofiSecuencia());
        m.put("0", session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia());
        model = new CbpactEquipo();
        this.equiposList = new ArrayList<>();
    }
    
    public void newModel(){
        equipo = new CbpactModelEquipo();
    }
    
    public void editarEquipo(){
        try{
            equipoEdit.setCxpmProveedor(((CxpmProveedor) dataIngreso.get("proveedor")).getCxpmProveedorPK().getCXPPROcodigo());
            equipoEdit.setGenofiCodigo(((CbpgenrOficina) dataIngreso.get("oficina")));
            equipoEdit.setModelEquipo(marcaModeloEncontrado);
            
            List<CbpactEquipo> eqpsList = new ArrayList();
            eqpsList.add(equipoEdit);
            
            if(!this.activosServices.verificarEquipos(marcaModeloEncontrado, eqpsList)){
                JsfUtil.messageInfo(null, "Hay equipos que ya fueron guardados con la información proporcionada. Verifique los datos e intente nuevamente.", "");
                return;
            }
            equipoEdit = (CbpactEquipo)entity.merge(equipoEdit);
            bitacora.saveActionLog(ActionsTransaccion.EDITAR_EQUIPO, ""+equipoEdit.getId());
            if(equipoEdit != null){
                this.dataIngreso.put("cantidad", 0);
                this.dataIngreso.put("valMarca", "");
                this.dataIngreso.put("valModelo", "");
                this.dataIngreso.put("valProv", "");
                this.dataIngreso.put("proveedor", new CxpmProveedor());
                JsfUtil.messageInfo(null, "Equipo editado correctamente", "");
            }else
                JsfUtil.messageInfo(null, "Error al editar el equipo", "");
        }catch(Exception e){
            e.printStackTrace();
            JsfUtil.messageInfo(null, "Error al editar el equipo", "");
        }
    }
    
    public void guardarEquipos(){
        try{
            if(equiposList == null || equiposList.isEmpty()){
                JsfUtil.messageInfo(null, "Debe ingresar equipos antes de guardar", "");
                return;
            }
            if(!this.activosServices.verificarEquipos(marcaModeloEncontrado, equiposList)){
                JsfUtil.messageInfo(null, "Hay equipos que fueron guardados con la información proporcionada. Verifique los datos e intente nuevamente.", "");
                return;
            }
            if(this.activosServices.guardarEquipos(marcaModeloEncontrado, equiposList)){
                this.marcaModeloEncontrado = new CbpactModelEquipo();
                this.equiposList = new VirtualFlow.ArrayLinkedList<>();
                this.model = new CbpactEquipo();
                this.dataIngreso.put("cantidad", 0);
                this.dataIngreso.put("valMarca", "");
                this.dataIngreso.put("valModelo", "");
                this.dataIngreso.put("valProv", "");
                this.dataIngreso.put("proveedor", new CxpmProveedor());
                JsfUtil.update("mainForm");
                JsfUtil.messageInfo(null, "Equipos guardado correctamente", "");
            }else{
                JsfUtil.messageInfo(null, "Error al guardar la informacion", "");
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void selectModel() {
        dataIngreso.put("valMarca", marcaModeloEncontrado.getMarca());
        dataIngreso.put("valModelo", marcaModeloEncontrado.getModelo());
        JsfUtil.update("mainForm");
        JsfUtil.update("frmEquipoModelEdit");
    }
    
    public void findAllModels(){
        modelList = entity.findAll(CbpactModelEquipo.class);
        JsfUtil.update("modelosMarcasFrm");
        JsfUtil.executeJS("PF('wdModelosMarcas').show();");
    }

    public void busquedaMarcaModelo() {
        if (dataIngreso.get("valMarca") != null && dataIngreso.get("valModelo") != null
                && dataIngreso.get("valMarca").toString().length() > 0 && dataIngreso.get("valModelo").toString().length() > 0) {
            modelList = entity.findListByParameters("SELECT m FROM CbpactModelEquipo m WHERE m.marca = :marca"
                    + " AND m.modelo = :modelo ", new String[]{"marca", "modelo"}, new Object[]{dataIngreso.get("valMarca").toString(), dataIngreso.get("valModelo").toString()});
            if (modelList != null && !modelList.isEmpty() && modelList.size() == 1) {
                marcaModeloEncontrado = modelList.get(0);
                return;
            }
        }
        if (dataIngreso.get("valMarca") != null && dataIngreso.get("valMarca").toString().length() > 0) {
            modelList = entity.findListByParameters("SELECT m FROM CbpactModelEquipo m WHERE m.marca = :marca",
                     new String[]{"marca"}, new Object[]{dataIngreso.get("valMarca").toString()});
            if (modelList != null && !modelList.isEmpty() && modelList.size() == 1) {
                marcaModeloEncontrado = modelList.get(0);
                this.dataIngreso.put("valModelo", marcaModeloEncontrado.getModelo());
                JsfUtil.update("mainForm");
                JsfUtil.update("frmEquipoModelEdit");
                JsfUtil.messageInfo(null, "Marca/modelo encontrado", "");
                return;
            }
        }
        if (dataIngreso.get("valModelo") != null && dataIngreso.get("valModelo").toString().length() > 0) {
            modelList = entity.findListByParameters("SELECT m FROM CbpactModelEquipo m WHERE m.marca = :modelo",
                     new String[]{"modelo"}, new Object[]{dataIngreso.get("valModelo").toString()});
            if (modelList != null && !modelList.isEmpty() && modelList.size() == 1) {
                marcaModeloEncontrado = modelList.get(0);
                this.dataIngreso.put("valModelo", marcaModeloEncontrado.getMarca());
                JsfUtil.update("mainForm");
                JsfUtil.update("frmEquipoModelEdit");
                JsfUtil.messageInfo(null, "Marca/modelo encontrado", "");
                return;
            }

        }
        if (modelList == null || modelList.isEmpty()) {
            modelList = entity.findAll(CbpactModelEquipo.class);
        }
        JsfUtil.update("modelosMarcasFrm");
        JsfUtil.executeJS("PF('wdModelosMarcas').show();");
    }

    public void guardarModels() {
        /*if(this.activosServices.guardarModel(equipoEdit, modelList))
            JsfUtil.messageInfo(null, "Datos ingresados correctamente", "");
        else
            JsfUtil.messageInfo(null, "Error al guardar la información", "");*/
    }

    public void addModel() {
        if (this.marcaModeloEncontrado == null) {
            JsfUtil.messageInfo(null, "Debe seleccionar marca/modelo", "");
            return;
        }
        if (this.model.getAnioFabricacion() == null || this.model.getFechaLlegadaEquipo() == null || Integer.parseInt(this.dataIngreso.get("cantidad") + "") == 0) {
            JsfUtil.messageInfo(null, "Debe ingresar todos los datos", "");
            return;
        }
        if (dataIngreso.get("proveedor") == null || dataIngreso.get("oficina") == null) {
            JsfUtil.messageInfo(null, "Debe seleccionar el proveedor y la oficina antes de proceder", "");
            return;
        }
        if (this.modelList == null) {
            this.modelList = new ArrayList();
        }
        
        CbpactEquipo temp;
        this.equiposList = new ArrayList<>();
        for (int i = 0  ; i < Integer.parseInt(this.dataIngreso.get("cantidad") + "") ; i++){
            temp = new CbpactEquipo();
            temp.setAnioFabricacion(this.model.getAnioFabricacion());
            temp.setCapacidadLitros(this.model.getCapacidadLitros());
            temp.setCxpmProveedor(((CxpmProveedor) dataIngreso.get("proveedor")).getCxpmProveedorPK().getCXPPROcodigo());
            temp.setFechaIngreso(new Date());
            temp.setEstado(1);
            temp.setGenofiCreauser(session.getCbpsegmUsuario());
            temp.setGenofiCodigo(((CbpgenrOficina) dataIngreso.get("oficina")));
            this.equiposList.add(temp);
        }
    }

    public String getNombreProveedor(String cod) {
        m.put("2", cod);
        return "" + entity.findObject("SQL", "SELECT s.CXPPRO_razonsocia FROM CXPM_PROVEEDOR s WHERE s.GENCIA_codigo = ? AND "
                + "GENOFI_codigo = ? AND CXPPRO_codigo = ?", m, null, false);

    }

    public void llenarDatos() {
        proveedores = new CxpmProveedorLazy(((CbpgenrOficina) dataIngreso.get("oficina")).getGenofiSecuencia(), this.session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia());
    }

    public void selectProveedor(SelectEvent event) {
        dataIngreso.put("valProv", ((CxpmProveedor) dataIngreso.get("proveedor")).getCxpmProveedorPK().getCXPPROcodigo());
    }

    public void busquedaProveedor() {
        CxpmProveedorPK pk = new CxpmProveedorPK(session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia(), ((CbpgenrOficina) dataIngreso.get("oficina")).getGenofiSecuencia(), (String) dataIngreso.get("valProv"));
        CxpmProveedor cp = entity.find(CxpmProveedor.class, pk);
        if (cp != null) {
            dataIngreso.put("valProv", cp.getCxpmProveedorPK().getCXPPROcodigo());
            dataIngreso.put("proveedor", cp);
        } else {
            JsfUtil.update("proveedoresFrm");
            JsfUtil.executeJS("PF('wdProveedores').show();");
        }
    }

    // VERIFICAR SI ESTE METODO SE USA
    public void editarMarcaModelo() {
        try {
            if (dataIngreso.get("proveedor") == null || dataIngreso.get("oficina") == null ) {
                JsfUtil.messageInfo(null, "Debe seleccionar e ingresar todos los datos antes de proceder", "");
                return;
            } else {
                HashMap busqueda = new HashMap();
                busqueda.put("cxpmProveedor", ((CxpmProveedor) dataIngreso.get("proveedor")).getCxpmProveedorPK().getCXPPROcodigo());
                busqueda.put("genofiCodigo", (CbpgenrOficina) dataIngreso.get("oficina"));
                Object o = entity.findObject(CbpactModelEquipo.class, busqueda);
                if (o != null) {
                    JsfUtil.messageInfo(null, "Equipo ingresado anteriormente", "");
                    return;
                } else {
                    //equipoEdit.setGenofiCodigo((CbpgenrOficina)dataIngreso.get("oficina"));

                    entity.merge(equipoEdit);
                    equipo = new CbpactModelEquipo();
                    modelsLazy = new CbpactModelEquipoLazy();
                    JsfUtil.messageInfo(null, "Equipo guardado correctamente", "");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            JsfUtil.messageInfo(null, "Error al guardar la información", "");
        }
    }

    public void agregarEquipo() {
        try {
            List<CbpactModelEquipo> me = entity.findListByParameters("SELECT m FROM CbpactModelEquipo m WHERE m.marca = :marca"
                    + " AND m.modelo = :modelo", new String[]{"marca", "modelo"}, new Object[]{equipo.getMarca().toUpperCase(), equipo.getModelo().toUpperCase()});
            if(me != null && !me.isEmpty()){
                JsfUtil.messageInfo(null, "Marca/modelo encontrado en la base de datos.", "");
                return;
            }
            equipo.setMarca(equipo.getMarca().toUpperCase());
            equipo.setModelo(equipo.getModelo().toUpperCase());
            equipo.setFechaIngreso(new Date());
            equipo.setGenofiCreauser(session.getCbpsegmUsuario());

            entity.merge(equipo);
            bitacora.saveActionLog(ActionsTransaccion.INGRESO_MODELO, ""+equipo.getId());
            equipo = new CbpactModelEquipo();
            modelsLazy = new CbpactModelEquipoLazy();
            JsfUtil.messageInfo(null, "Equipo guardado correctamente", "");

        } catch (Exception e) {
            e.printStackTrace();
            JsfUtil.messageInfo(null, "Error al guardar la información", "");
        }
    }
    
    public void actualizarModelo(){
        equipo.setMarca(equipo.getMarca().toUpperCase());
        equipo.setModelo(equipo.getModelo().toUpperCase());
        equipo = (CbpactModelEquipo)this.entity.merge(equipo);
        if(equipo != null){
            bitacora.saveActionLog(ActionsTransaccion.EDITAR_MODELO, ""+equipo.getId());
            this.modelsLazy = new CbpactModelEquipoLazy();
            equipo = new CbpactModelEquipo();
            JsfUtil.messageInfo(null, "Modelo actualizado correctamente", "");
        }else
            JsfUtil.messageInfo(null, "Error al actualizar modelo", "");
    }

    /*public void setEquipoEdicion(CbpactModelEquipo equipoEdit){
        this.equipoEdit = equipoEdit;
        this.dataIngreso.put("oficina", this.equipoEdit.getGenofiCodigo());
        CxpmProveedorPK pk = new CxpmProveedorPK(session.getCaja().getGENCIAcodigo(), ((CbpgenrOficina)dataIngreso.get("oficina")).getGenofiSecuencia(), this.equipoEdit.getCxpmProveedor());
        CxpmProveedor cp = entity.find(CxpmProveedor.class, pk);
        if(cp != null){
            dataIngreso.put("valProv", cp.getCxpmProveedorPK().getCXPPROcodigo());
            dataIngreso.put("proveedor", cp);
        }
    }*/
    public void selectOficina(SelectEvent event) {
        dataIngreso.put("valOfi", ((CbpgenrOficina) dataIngreso.get("oficina")).getGenofiSecuencia());
        llenarDatos();
        m.put("1", ((CbpgenrOficina) dataIngreso.get("oficina")).getGenofiSecuencia());
    }

    public void busquedaOficina() {
        try {
            Object o = this.generalEjb.consultarOficina((String) this.dataIngreso.get("valOfi"));
            if (o != null) {
                this.dataIngreso.put("oficina", (CbpgenrOficina) o);
                m.put("1", ((CbpgenrOficina) dataIngreso.get("oficina")).getGenofiSecuencia());
            } else {
                JsfUtil.update("frmdlgOficina");
                JsfUtil.executeJS("PF('dlgOficina').show();");
            }
        } catch (Exception e) {
            Logger.getLogger(IngresoBodegaView.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public CbpactModelEquipo getEquipo() {
        return equipo;
    }

    public void setEquipo(CbpactModelEquipo equipo) {
        this.equipo = equipo;
    }

    public HashMap getDataIngreso() {
        return dataIngreso;
    }

    public void setDataIngreso(HashMap dataIngreso) {
        this.dataIngreso = dataIngreso;
    }

    public UserSession getSession() {
        return session;
    }

    public void setSession(UserSession session) {
        this.session = session;
    }

    public CxpmProveedorLazy getProveedores() {
        return proveedores;
    }

    public void setProveedores(CxpmProveedorLazy proveedores) {
        this.proveedores = proveedores;
    }

    public CbpgenrOficinaLazyTest getOficinasLazy() {
        return oficinasLazy;
    }

    public void setOficinasLazy(CbpgenrOficinaLazyTest oficinasLazy) {
        this.oficinasLazy = oficinasLazy;
    }

    public CbpactModelEquipoLazy getModelsLazy() {
        return modelsLazy;
    }

    public void setModelsLazy(CbpactModelEquipoLazy modelsLazy) {
        this.modelsLazy = modelsLazy;
    }

    public CbpactEquipoLazy getEquiposLazy() {
        return equiposLazy;
    }

    public void setEquiposLazy(CbpactEquipoLazy equiposLazy) {
        this.equiposLazy = equiposLazy;
    }

    public CbpactEquipo getEquipoEdit() {
        return equipoEdit;
    }

    public void setEquipoEdit(CbpactEquipo equipoEdit) {
        this.equipoEdit = equipoEdit;
        for(CbpactSolicitudEquipo t : this.equipoEdit.getSolicitudesList()){
            if(t.getEstado() == 1){
                JsfUtil.messageInfo(null, "El equipo tiene solicitudes pendientes.", "");
                return;
            }
        }
        this.dataIngreso.put("valMarca", equipoEdit.getModelEquipo().getMarca());
        this.dataIngreso.put("valModelo", equipoEdit.getModelEquipo().getModelo());
        this.dataIngreso.put("valProv", equipoEdit.getCxpmProveedor());
        CxpmProveedorPK pk = new CxpmProveedorPK(session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia(), ((CbpgenrOficina)dataIngreso.get("oficina")).getGenofiSecuencia(), equipoEdit.getCxpmProveedor());
        CxpmProveedor cp = entity.find(CxpmProveedor.class, pk);
        this.dataIngreso.put("proveedor", cp);
        
        HashMap m = new HashMap();
        m.put("marca", equipoEdit.getModelEquipo().getMarca());
        m.put("modelo", equipoEdit.getModelEquipo().getModelo());
        this.marcaModeloEncontrado = entity.findObject(CbpactModelEquipo.class, m);
    }

    public CbpactEquipo getModel() {
        return model;
    }

    public void setModel(CbpactEquipo model) {
        this.model = model;
    }

    public List<CbpactModelEquipo> getModelList() {
        return modelList;
    }

    public void setModelList(List<CbpactModelEquipo> modelList) {
        this.modelList = modelList;
    }

    public List<CbpactEquipo> getEquiposList() {
        return equiposList;
    }

    public void setEquiposList(List<CbpactEquipo> equiposList) {
        this.equiposList = equiposList;
    }

    public CbpactModelEquipo getMarcaModeloEncontrado() {
        return marcaModeloEncontrado;
    }

    public void setMarcaModeloEncontrado(CbpactModelEquipo marcaModeloEncontrado) {
        this.marcaModeloEncontrado = marcaModeloEncontrado;
    }

}
