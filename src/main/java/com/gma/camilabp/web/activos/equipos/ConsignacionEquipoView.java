/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.activos.equipos;

import com.gma.camilabo.enums.ActionsTransaccion;
import com.gma.camilabp.domain.CbpactConsignacion;
import com.gma.camilabp.domain.CbpactEquipo;
import com.gma.camilabp.domain.CbpactSolicitudEquipo;
import com.gma.camilabp.domain.CbpgenrOficina;
import com.gma.camilabp.domain.CbpsegmUsuario;
import com.gma.camilabp.domain.CxpmProveedor;
import com.gma.camilabp.domain.CxpmProveedorPK;
import com.gma.camilabp.domain.MEmpleados;
import com.gma.camilabp.web.comercial.compras.IngresoBodegaView;
import com.gma.camilabp.web.lazy.CbpactSolicitudEquipoLazy;
import com.gma.camilabp.web.lazy.CbpgenrOficinaLazyTest;
import com.gma.camilabp.web.lazy.CbpsegmUsuarioLazy;
import com.gma.camilabp.web.lazy.CxcmClienteLazy;
import com.gma.camilabp.web.lazy.MEmpleadosLazy;
import com.gma.camilabp.web.service.ActivosEquiposLocal;
import com.gma.camilabp.web.service.BitacoraLocal;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.service.GeneralLocal;
import com.gma.camilabp.web.session.ServletSession;
import com.gma.camilabp.web.session.UserSession;
import com.gma.camilabp.web.util.JsfUtil;
import com.gma.camilabp.web.util.Utilidades;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author userdb5
 */
@Named(value = "consignacionEquipoView")
@ViewScoped
public class ConsignacionEquipoView implements Serializable{
    
    @Inject
    private UserSession session;
    @Inject
    private ServletSession datosSession;
    
    @EJB
    private EntityManagerLocal entity;
    @EJB
    private ActivosEquiposLocal activosServices;
    @EJB
    private GeneralLocal generalEjb;
    @EJB
    private BitacoraLocal bitacora;
    
    private HashMap m;
    private CbpactSolicitudEquipoLazy solicitudes, solicitudesConResolucion;
    private HashMap dataIngreso;
    private CbpgenrOficinaLazyTest oficinasLazy;
    private CbpactConsignacion consignacion, consignacionInfo;
    private CxcmClienteLazy clientes;
    private CbpsegmUsuarioLazy usuariosLazy;
    private List<Integer> tipos, estados, estados2;
    private MEmpleadosLazy empleadosLazy;
    
    
    @PostConstruct
    public void initView(){
        m = new HashMap();
        dataIngreso = new HashMap();
        this.oficinasLazy = new CbpgenrOficinaLazyTest(Utilidades.getIdsOfList(this.session.getOficinas()));
        dataIngreso.put("oficina", (CbpgenrOficina)Utilidades.getFirstElement(this.session.getOficinas()));
        dataIngreso.put("proveedor", new CxpmProveedor());
        dataIngreso.put("valOfi", ((CbpgenrOficina)dataIngreso.get("oficina")).getGenofiSecuencia());
        dataIngreso.put("valProv", "");
        dataIngreso.put("empleadoResolucion", new MEmpleados());
        dataIngreso.put("solicitud", new CbpactSolicitudEquipo());
        dataIngreso.put("solicitudInfo", new CbpactSolicitudEquipo());
        dataIngreso.put("equipo", new CbpactEquipo());
        tipos = new ArrayList();
        tipos.add(1);
        estados = new ArrayList();
        estados.add(1);
        this.solicitudes = new CbpactSolicitudEquipoLazy(estados, tipos, Utilidades.getIdsOfList(this.session.getOficinas()));
        m.put("1", ((CbpgenrOficina)dataIngreso.get("oficina")).getGenofiSecuencia());
        m.put("0", session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia());
        consignacion = new CbpactConsignacion();
        clientes = new CxcmClienteLazy(((CbpgenrOficina)dataIngreso.get("oficina")).getGenofiSecuencia(), 
                session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia());
        usuariosLazy = new CbpsegmUsuarioLazy();
        estados2 = new ArrayList();
        estados2.add(2);
        estados2.add(3);
        estados2.add(4);
        estados2.add(5);
        estados2.add(6);
        empleadosLazy = new MEmpleadosLazy(((CbpgenrOficina)dataIngreso.get("oficina")).getGenofiSecuencia(), 
                session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia());
        solicitudesConResolucion = new CbpactSolicitudEquipoLazy(estados2, tipos, Utilidades.getIdsOfList(this.session.getOficinas()));
    }
    
    public void verInfo(){
        CbpactSolicitudEquipo info = (CbpactSolicitudEquipo)dataIngreso.get("solicitudInfo");
        
        if(info.getId() != null && info.getConsignacionesList() != null && !info.getConsignacionesList().isEmpty()){
            dataIngreso.put("dataInfo", info.getConsignacionesList().get(info.getConsignacionesList().size()-1));
        }
        
        JsfUtil.update("frmdlgInfo");
        JsfUtil.executeJS("PF('dlgInfo').show()");
    }
    
    public void setearSolicitudAnterior(){
        if(((CbpactSolicitudEquipo)this.dataIngreso.get("solicitudInfo")).getId() != null)
            this.consignacion = ((CbpactSolicitudEquipo)this.dataIngreso.get("solicitudInfo")).getConsignacionesList().get(((CbpactSolicitudEquipo)this.dataIngreso.get("solicitudInfo")).getConsignacionesList().size()-1);
    }
    
    public void consignar(){        
        if(((CbpactSolicitudEquipo)dataIngreso.get("solicitud")).getId() == null){
            JsfUtil.messageInfo(null, "Debe seleccionar el equipo antes de consignar", "");
            return;
        }else{
            consignacion = new CbpactConsignacion();
        }
    }
    
    public void rechazarSolicitud(){
        try{
            CbpactSolicitudEquipo sol = (CbpactSolicitudEquipo)dataIngreso.get("solicitud");
            sol.setEstado(3);
            sol.setUsuarioAprobacion(Long.parseLong(""+((MEmpleados)this.dataIngreso.get("empleadoResolucion")).getMEmpleadosPK().getNOMEMPcodigo()));
            sol.setFechaAprobacion(new Date());
            this.entity.merge(sol);
            JsfUtil.messageInfo(null, "Solicitud rechazada satisfactoriamente", "");
            bitacora.saveActionLog(ActionsTransaccion.RECHAZAR_SOLICITUD, ""+sol.getId());
            initSolicitud();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public void setEmpleado(MEmpleados emp){
        if(emp != null){
            JsfUtil.messageInfo(null, "Empleado seleccionado correctamente", "");
            this.dataIngreso.put("empleadoResolucion", emp);
        }else
            JsfUtil.messageInfo(null, "No ha seleccionado al empleado", "");
    }
    
    public void initSolicitud(){
        consignacion = new CbpactConsignacion();
        this.dataIngreso.put("solicitud", new CbpactSolicitudEquipo());
        this.solicitudes = new CbpactSolicitudEquipoLazy(estados, tipos, Utilidades.getIdsOfList(this.session.getOficinas()));
        dataIngreso.put("empleadoResolucion", new MEmpleados());
        dataIngreso.put("proveedor", new CxpmProveedor());
        JsfUtil.update("mainForm");
        JsfUtil.messageInfo(null, "Mantenimiento guardado correctamente", "");
    }
    
    public void aceptarSolicitud(){
        try{
            if(this.consignacion.getFechaConsignacion() == null){
                JsfUtil.messageInfo(null, "Debe ingresar la fecha de consignacion", "");
                return;
            }
            if(this.consignacion.getNumContrato() == null || this.consignacion.getNumContrato() == ""){
                JsfUtil.messageInfo(null, "Debe ingresar el numero de contrato", "");
                return;
            }
            if(this.consignacion.getObservacion() == null){
                JsfUtil.messageInfo(null, "Debe ingresar una observacion", "");
                return;
            }
            CbpactSolicitudEquipo sol = (CbpactSolicitudEquipo)dataIngreso.get("solicitud");

            this.consignacion.setNumContrato(this.consignacion.getNumContrato().toUpperCase());
            this.consignacion.setObservacion(this.consignacion.getObservacion().toUpperCase());
            this.consignacion.setSolicitud((CbpactSolicitudEquipo)this.dataIngreso.get("solicitud"));
            this.consignacion.setFechaIngreso(new Date());
            this.consignacion.setUsuarioIngreso(session.getCbpsegmUsuario().getSegusuCodigo());
            this.consignacion.setEstado(1);
            this.consignacion.setUsuarioResolucion(Long.parseLong(""+((MEmpleados)this.dataIngreso.get("empleadoResolucion")).getMEmpleadosPK().getNOMEMPcodigo()));
            sol.setUsuarioAprobacion(Long.parseLong(""+((MEmpleados)this.dataIngreso.get("empleadoResolucion")).getMEmpleadosPK().getNOMEMPcodigo()));
            sol.setEstado(2);
            sol.setFechaAprobacion(new Date());
            this.entity.merge(sol);
            CbpactEquipo temp = (CbpactEquipo)sol.getEquipo();
            temp.setEstado(4);
            
            this.consignacion = (CbpactConsignacion)this.entity.merge(this.consignacion);
            this.entity.merge(temp);
            
            bitacora.saveActionLog(ActionsTransaccion.ACEPTAR_SOLICITUD, ""+sol.getId());
            
            imprimirConsignacion(false);
            initSolicitud();
            
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public void imprimirConsignacion(Boolean reimpresion){
        if(this.consignacion.getId() == null){
            JsfUtil.messageInfo(null, "Debe seleccionar la consignacion antes de reimprimir.", "");
            return;
        }
        datosSession.borrarDatos();
        datosSession.setTieneDatasource(Boolean.FALSE);
        datosSession.setNombreReporte("activos/consignacion_equipo");
        
        datosSession.agregarParametro("ID", this.consignacion.getId());
        JsfUtil.redirectNewTab("/CamilaPOS/Documento");
    }
    
    public void selectOficina(SelectEvent event){
        dataIngreso.put("valOfi", ((CbpgenrOficina)dataIngreso.get("oficina")).getGenofiSecuencia());
        JsfUtil.update("mainForm:secuenciaOficina");
        m.put("1", ((CbpgenrOficina)dataIngreso.get("oficina")).getGenofiSecuencia());
        clientes = new CxcmClienteLazy(Long.parseLong(((CbpgenrOficina)dataIngreso.get("oficina")).getGenofiSecuencia()+""), 
                Long.parseLong(""+session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia()));
    }
    
    public void busquedaOficina(){
        try{
            Object o = this.generalEjb.consultarOficina((String)this.dataIngreso.get("valOfi"));
            if(o != null){
                this.dataIngreso.put("oficina", (CbpgenrOficina)o);
                m.put("1", ((CbpgenrOficina)dataIngreso.get("oficina")).getGenofiSecuencia());
                clientes = new CxcmClienteLazy(Long.parseLong(((CbpgenrOficina)dataIngreso.get("oficina")).getGenofiSecuencia()+""), 
                Long.parseLong(""+session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia()));
            }else{
                JsfUtil.update("frmdlgOficina");
                JsfUtil.executeJS("PF('dlgOficina').show();");
            }
        } catch (Exception e) {
            Logger.getLogger(IngresoBodegaView.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void busquedaProveedor(){
        CxpmProveedorPK pk = new CxpmProveedorPK(session.getCaja().getGENCIAcodigo(), ((CbpgenrOficina)dataIngreso.get("oficina")).getGenofiSecuencia(), (String)dataIngreso.get("valProv"));
        CxpmProveedor cp = entity.find(CxpmProveedor.class, pk);
        if(cp != null){
            dataIngreso.put("valProv", cp.getCxpmProveedorPK().getCXPPROcodigo());
            dataIngreso.put("proveedor", cp);
        }else{
            JsfUtil.update("proveedoresFrm");
            JsfUtil.executeJS("PF('wdProveedores').show();");
        }
    }

    public CbpactSolicitudEquipoLazy getSolicitudes() {
        return solicitudes;
    }

    public void setSolicitudes(CbpactSolicitudEquipoLazy solicitudes) {
        this.solicitudes = solicitudes;
    }

    public HashMap getDataIngreso() {
        return dataIngreso;
    }

    public void setDataIngreso(HashMap dataIngreso) {
        this.dataIngreso = dataIngreso;
    }

    public CbpgenrOficinaLazyTest getOficinasLazy() {
        return oficinasLazy;
    }

    public void setOficinasLazy(CbpgenrOficinaLazyTest oficinasLazy) {
        this.oficinasLazy = oficinasLazy;
    }

    public CbpactConsignacion getConsignacion() {
        return consignacion;
    }

    public void setConsignacion(CbpactConsignacion consignacion) {
        this.consignacion = consignacion;
    }

    public CxcmClienteLazy getClientes() {
        return clientes;
    }

    public void setClientes(CxcmClienteLazy clientes) {
        this.clientes = clientes;
    }

    public CbpsegmUsuarioLazy getUsuariosLazy() {
        return usuariosLazy;
    }

    public void setUsuariosLazy(CbpsegmUsuarioLazy usuariosLazy) {
        this.usuariosLazy = usuariosLazy;
    }

    public CbpactSolicitudEquipoLazy getSolicitudesConResolucion() {
        return solicitudesConResolucion;
    }

    public void setSolicitudesConResolucion(CbpactSolicitudEquipoLazy solicitudesConResolucion) {
        this.solicitudesConResolucion = solicitudesConResolucion;
    }

    public CbpactConsignacion getConsignacionInfo() {
        return consignacionInfo;
    }

    public void setConsignacionInfo(CbpactConsignacion consignacionInfo) {
        this.consignacionInfo = consignacionInfo;
    }

    public MEmpleadosLazy getEmpleadosLazy() {
        return empleadosLazy;
    }

    public void setEmpleadosLazy(MEmpleadosLazy empleadosLazy) {
        this.empleadosLazy = empleadosLazy;
    }
    
}
