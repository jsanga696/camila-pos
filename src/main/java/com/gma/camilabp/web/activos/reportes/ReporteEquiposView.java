/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.activos.reportes;

import com.gma.camilabp.domain.CbpactEquipo;
import com.gma.camilabp.domain.CbpactEquipoCategoria;
import com.gma.camilabp.domain.CbpactSolicitudEquipo;
import com.gma.camilabp.domain.CbpgenrOficina;
import com.gma.camilabp.domain.CxcmCliente;
import com.gma.camilabp.web.comercial.compras.IngresoBodegaView;
import com.gma.camilabp.web.lazy.CbpactEquipoLazy;
import com.gma.camilabp.web.lazy.CbpactSolicitudEquipoLazy;
import com.gma.camilabp.web.lazy.CbpgenrOficinaLazyTest;
import com.gma.camilabp.web.service.ActivosEquiposLocal;
import com.gma.camilabp.web.service.BitacoraLocal;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.service.GeneralLocal;
import com.gma.camilabp.web.session.UserSession;
import com.gma.camilabp.web.util.JsfUtil;
import com.gma.camilabp.web.util.Utilidades;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author userdb5
 */
@Named(value = "reporteEquiposView")
@ViewScoped
public class ReporteEquiposView implements Serializable {

    @Inject
    private UserSession session;

    @EJB
    private EntityManagerLocal entity;
    @EJB
    private ActivosEquiposLocal activosServices;
    @EJB
    private BitacoraLocal bitacora;
    @EJB
    private GeneralLocal generalEjb;

    private HashMap dataIngreso;
    private CbpgenrOficinaLazyTest oficinasLazy;
    private HashMap m;
    private CbpactEquipoLazy equiposLazy;
    private CbpactSolicitudEquipoLazy solicitudes;
    private List<Integer> tipos, estados;

    @PostConstruct
    public void initView() {
        m = new HashMap();
        dataIngreso = new HashMap();
        this.oficinasLazy = new CbpgenrOficinaLazyTest(Utilidades.getIdsOfList(this.session.getOficinas()));
        dataIngreso.put("oficina", (CbpgenrOficina) Utilidades.getFirstElement(this.session.getOficinas()));
        dataIngreso.put("valOfi", ((CbpgenrOficina) dataIngreso.get("oficina")).getGenofiSecuencia());
        dataIngreso.put("valEq", "");
        dataIngreso.put("cliente", new CxcmCliente());
        dataIngreso.put("equipoCategoria", new CbpactEquipoCategoria());
        dataIngreso.put("equipo", new CbpactEquipo());
        dataIngreso.put("solicitud", new CbpactSolicitudEquipo());
        equiposLazy = new CbpactEquipoLazy(((CbpgenrOficina) dataIngreso.get("oficina")).getGenofiCodigo());
        m.put("0", session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia());
        m.put("1", ((CbpgenrOficina) dataIngreso.get("oficina")).getGenofiSecuencia());
        
        //
        
        tipos = new ArrayList();
        tipos.add(1);
        estados = new ArrayList();
        estados.add(2);
        this.solicitudes = new CbpactSolicitudEquipoLazy(estados, tipos, Utilidades.getIdsOfList(this.session.getOficinas()));
    }
    
    public void selectEquipo(){
        this.dataIngreso.put("equipo", ((CbpactSolicitudEquipo)this.dataIngreso.get("solicitud")).getEquipo());
        HashMap m = new HashMap();
        m.put("CXCCLIcodigo", ((CbpactSolicitudEquipo)this.dataIngreso.get("solicitud")).getCxcmClienteSolicitante());

        CxcmCliente eq = (CxcmCliente) entity.findObject("HQL", "SELECT m FROM CxcmCliente m WHERE m.cxcmClientePK.cXCCLIcodigo = :CXCCLIcodigo",
                m, CxcmCliente.class, Boolean.TRUE);
        
        this.dataIngreso.put("cliente", eq);
    }

    public void selectOficina(SelectEvent event) {
        dataIngreso.put("valOfi", ((CbpgenrOficina) dataIngreso.get("oficina")).getGenofiSecuencia());
        JsfUtil.update("mainForm:secuenciaOficina");
        m.put("1", ((CbpgenrOficina) dataIngreso.get("oficina")).getGenofiSecuencia());
        equiposLazy = new CbpactEquipoLazy(((CbpgenrOficina) dataIngreso.get("oficina")).getGenofiCodigo());
    }

    public void busquedaEquipo() {

        HashMap m = new HashMap();
        m.put("placa", dataIngreso.get("valEq"));
        m.put("genofiCodigo", (CbpgenrOficina) dataIngreso.get("oficina"));

        CbpactEquipo eq = (CbpactEquipo) entity.findObject("HQL", "SELECT m FROM CbpactEquipo m WHERE m.placa = :placa AND m.genofiCodigo = :genofiCodigo",
                m, CbpactEquipo.class, Boolean.TRUE);

        if (eq != null) {
            m = new HashMap();
            m.put("tipoSolicitud", 1);
            m.put("estado", 2);
            m.put("equipo", eq);
            List<CbpactSolicitudEquipo> consignaciones = entity.findAllByParameter(CbpactSolicitudEquipo.class, m, "id");

            if (consignaciones == null || consignaciones.isEmpty()) {
                JsfUtil.messageInfo(null, "El equipo no tiene consignaciones registradas", "");
                return;
            }
            CbpactSolicitudEquipo c = consignaciones.get(consignaciones.size() - 1);

            dataIngreso.put("equipo", eq);
            m = new HashMap();
            m.put("gENOFIcodigo", ((CbpgenrOficina) dataIngreso.get("oficina")).getGenofiSecuencia());
            m.put("cXCCLIcodigo", c.getCxcmClienteSolicitante());
            CxcmCliente cl = (CxcmCliente) entity.findObject("HQL", "SELECT m FROM CxcmCliente m WHERE m.cxcmClientePK.gENOFIcodigo = :gENOFIcodigo "
                    + "AND m.cxcmClientePK.cXCCLIcodigo = :cXCCLIcodigo", m, CxcmCliente.class, Boolean.TRUE);
            dataIngreso.put("cliente", cl);
            JsfUtil.update("frmDatos");
            JsfUtil.executeJS("PF('wvDatos').show();");
        } else {
            JsfUtil.update("frmdlgEquipo");
            JsfUtil.executeJS("PF('wdEquipos').show();");
        }
    }

    public void setValEq() {
        m = new HashMap();
        m.put("tipoSolicitud", 1);
        m.put("estado", 2);
        m.put("equipo", ((CbpactEquipo)dataIngreso.get("equipo")));
        List<CbpactSolicitudEquipo> consignaciones = entity.findAllByParameter(CbpactSolicitudEquipo.class, m, "id");

        if (consignaciones == null || consignaciones.isEmpty()) {
            JsfUtil.messageInfo(null, "El equipo no tiene consignaciones registradas", "");
            return;
        }
        dataIngreso.put("valEq", dataIngreso.get("equipo"));
        CbpactSolicitudEquipo c = consignaciones.get(consignaciones.size() - 1);

        m = new HashMap();
        m.put("gENOFIcodigo", ((CbpgenrOficina) dataIngreso.get("oficina")).getGenofiSecuencia());
        m.put("cXCCLIcodigo", c.getCxcmClienteSolicitante());
        CxcmCliente cl = (CxcmCliente) entity.findObject("HQL", "SELECT m FROM CxcmCliente m WHERE m.cxcmClientePK.gENOFIcodigo = :gENOFIcodigo "
                + "AND m.cxcmClientePK.cXCCLIcodigo = :cXCCLIcodigo", m, CxcmCliente.class, Boolean.TRUE);
        dataIngreso.put("cliente", cl);
        JsfUtil.update("frmDatos");
        JsfUtil.executeJS("PF('wvDatos').show();");
    }

    public void busquedaOficina() {
        try {
            Object o = this.generalEjb.consultarOficina((String) this.dataIngreso.get("valOfi"));
            if (o != null) {
                this.dataIngreso.put("oficina", (CbpgenrOficina) o);
                m.put("1", ((CbpgenrOficina) dataIngreso.get("oficina")).getGenofiSecuencia());
            } else {
                JsfUtil.update("frmdlgOficina");
                JsfUtil.executeJS("PF('dlgOficina').show();");
            }
        } catch (Exception e) {
            Logger.getLogger(IngresoBodegaView.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public HashMap getDataIngreso() {
        return dataIngreso;
    }

    public void setDataIngreso(HashMap dataIngreso) {
        this.dataIngreso = dataIngreso;
    }

    public CbpgenrOficinaLazyTest getOficinasLazy() {
        return oficinasLazy;
    }

    public void setOficinasLazy(CbpgenrOficinaLazyTest oficinasLazy) {
        this.oficinasLazy = oficinasLazy;
    }

    public CbpactEquipoLazy getEquiposLazy() {
        return equiposLazy;
    }

    public void setEquiposLazy(CbpactEquipoLazy equiposLazy) {
        this.equiposLazy = equiposLazy;
    }

    public CbpactSolicitudEquipoLazy getSolicitudes() {
        return solicitudes;
    }

    public void setSolicitudes(CbpactSolicitudEquipoLazy solicitudes) {
        this.solicitudes = solicitudes;
    }

}
