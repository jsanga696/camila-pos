/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.activos.clientes;

import com.gma.camilabo.enums.ActionsTransaccion;
import com.gma.camilabp.domain.CbpactRangosCliente;
import com.gma.camilabp.web.service.ActivosEquiposLocal;
import com.gma.camilabp.web.service.BitacoraLocal;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.service.GeneralLocal;
import com.gma.camilabp.web.session.ServletSession;
import com.gma.camilabp.web.session.UserSession;
import com.gma.camilabp.web.util.JsfUtil;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author userdb5
 */
@Named(value = "rangosClientesView")
@ViewScoped
public class RangosClientesView implements Serializable{
    
    @Inject
    private UserSession session;
    @Inject
    private ServletSession datosSession;
    
    @EJB
    private EntityManagerLocal entity;
    @EJB
    private ActivosEquiposLocal activosServices;
    @EJB
    private GeneralLocal generalEjb;
    @EJB
    private BitacoraLocal bitacora;
    
    private HashMap dataIngreso, m;
    private List<CbpactRangosCliente> rangos, rangosNew;
    
    @PostConstruct
    public void initView(){
        dataIngreso = new HashMap();
        obtenerValoresRangos();
    }
    
    public void obtenerValoresRangos(){
        rangos = entity.findListByParameters("SELECT m FROM CbpactRangosCliente m "
                + "WHERE m.fechaFin is null", new String[]{}, new Object[]{});
    }

    public void setearValoresEdit(){
        rangosNew = new ArrayList();
        CbpactRangosCliente rc;
        
        for(CbpactRangosCliente t : rangos){
            rc = new CbpactRangosCliente();
            rc.setClaseCliente(t.getClaseCliente());
            rc.setValorMax(t.getValorMax());
            rc.setValorMin(t.getValorMin());
            rangosNew.add(rc);
        }
    }
    
    public void guardarDatos(){
        if(activosServices.verificarRangos(rangosNew))
            if(activosServices.guardarRangos(rangos, rangosNew)){
                bitacora.saveActionLog(ActionsTransaccion.INGRESO_RANGOS_CLIENTE, "");
                obtenerValoresRangos();
                JsfUtil.update("mainForm");
                JsfUtil.executeJS("PF('dlgRangos').hide()");
                JsfUtil.messageInfo(null, "Datos guardados correctamente", "");
            }else
                JsfUtil.messageInfo(null, "Error al guardar la información", "");
        else
            JsfUtil.messageInfo(null, "No se pudo guardar la información. Hay valores inconsistentes.", "");
    }
    
    public List<CbpactRangosCliente> getRangos() {
        return rangos;
    }

    public void setRangos(List<CbpactRangosCliente> rangos) {
        this.rangos = rangos;
    }

    public List<CbpactRangosCliente> getRangosNew() {
        return rangosNew;
    }

    public void setRangosNew(List<CbpactRangosCliente> rangosNew) {
        this.rangosNew = rangosNew;
    }
    
}
