/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.activos.reportes;

import com.gma.camilabp.domain.CbpactEquipo;
import com.gma.camilabp.domain.CbpactSolicitudEquipo;
import com.gma.camilabp.domain.CbpactTipologiaCliente;
import com.gma.camilabp.domain.CbpgenrOficina;
import com.gma.camilabp.domain.CxcmCliente;
import com.gma.camilabp.web.comercial.compras.IngresoBodegaView;
import com.gma.camilabp.web.lazy.CbpactSolicitudEquipoLazy;
import com.gma.camilabp.web.lazy.CbpgenrOficinaLazyTest;
import com.gma.camilabp.web.lazy.CxcmClienteLazy;
import com.gma.camilabp.web.service.ActivosEquiposLocal;
import com.gma.camilabp.web.service.BitacoraLocal;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.service.GeneralLocal;
import com.gma.camilabp.web.session.UserSession;
import com.gma.camilabp.web.util.JsfUtil;
import com.gma.camilabp.web.util.Utilidades;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author userdb5
 */
@Named(value = "reporteClientesView")
@ViewScoped
public class ReporteClientesView implements Serializable {

    @Inject
    private UserSession session;

    @EJB
    private EntityManagerLocal entity;
    @EJB
    private ActivosEquiposLocal activosServices;
    @EJB
    private BitacoraLocal bitacora;
    @EJB
    private GeneralLocal generalEjb;

    private HashMap dataIngreso;
    private CbpgenrOficinaLazyTest oficinasLazy;
    private CxcmClienteLazy clientes;
    private HashMap m;
    private CbpactSolicitudEquipoLazy solicitudes;
    private List<CbpactSolicitudEquipo> consignaciones;
    private List<Integer> tipos, estados;

    @PostConstruct
    public void initView() {
        try {
            m = new HashMap();
            dataIngreso = new HashMap();
            this.oficinasLazy = new CbpgenrOficinaLazyTest(Utilidades.getIdsOfList(this.session.getOficinas()));
            
            //
            
            tipos = new ArrayList();
            tipos.add(1);
            estados = new ArrayList();
            estados.add(2);
            this.solicitudes = new CbpactSolicitudEquipoLazy(estados, tipos, Utilidades.getIdsOfList(this.session.getOficinas()));
            
            dataIngreso.put("oficina", (CbpgenrOficina) Utilidades.getFirstElement(this.session.getOficinas()));
            dataIngreso.put("valOfi", ((CbpgenrOficina) dataIngreso.get("oficina")).getGenofiSecuencia());
            dataIngreso.put("solicitud", new CbpactSolicitudEquipo());
            dataIngreso.put("tipologiaCliente", new CbpactTipologiaCliente());

            clientes = new CxcmClienteLazy(((CbpgenrOficina) dataIngreso.get("oficina")).getGenofiCodigo());
            m.put("0", session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia());
            m.put("1", ((CbpgenrOficina) dataIngreso.get("oficina")).getGenofiSecuencia());
            clientes = new CxcmClienteLazy(((CbpgenrOficina) dataIngreso.get("oficina")).getGenofiSecuencia(),
                    session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia());
            consignaciones = new ArrayList();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void selectEquipo(){
        this.dataIngreso.put("equipo", ((CbpactSolicitudEquipo)this.dataIngreso.get("solicitud")).getEquipo());
        HashMap m = new HashMap();
        m.put("CXCCLIcodigo", ((CbpactSolicitudEquipo)this.dataIngreso.get("solicitud")).getCxcmClienteSolicitante());

        CxcmCliente eq = (CxcmCliente) entity.findObject("HQL", "SELECT m FROM CxcmCliente m WHERE m.cxcmClientePK.cXCCLIcodigo = :CXCCLIcodigo",
                m, CxcmCliente.class, Boolean.TRUE);
        
        m = new HashMap();
        m.put("tipoSolicitud", 1);
        m.put("estado", 2);
        m.put("cxcmClienteSolicitante", ((CbpactSolicitudEquipo)this.dataIngreso.get("solicitud")).getCxcmClienteSolicitante());
        consignaciones = entity.findAllByParameter(CbpactSolicitudEquipo.class, m, "id");
        
        this.dataIngreso.put("cliente", eq);
    }

    public void busquedaSolicitante() {
        HashMap m = new HashMap();
        m.put("cXCCLIcodigo", dataIngreso.get("valSol"));
        m.put("gENOFIcodigo", ((CbpgenrOficina) dataIngreso.get("oficina")).getGenofiSecuencia());

        CxcmCliente cl = (CxcmCliente) entity.findObject("HQL", "SELECT m FROM CxcmCliente m WHERE m.cxcmClientePK.gENOFIcodigo = :gENOFIcodigo "
                + "AND m.cxcmClientePK.cXCCLIcodigo = :cXCCLIcodigo", m, CxcmCliente.class, Boolean.TRUE);

        if (cl != null) {
            dataIngreso.put("cliente", cl);
            m = new HashMap();
            m.put("tipoSolicitud", 1);
            m.put("estado", 2);
            m.put("cxcmClienteSolicitante", dataIngreso.get("valSol"));
            consignaciones = entity.findAllByParameter(CbpactSolicitudEquipo.class, m, "id");

            if (consignaciones == null || consignaciones.isEmpty()) {
                JsfUtil.messageInfo(null, "El cliente no tiene equipos consignados", "");
                return;
            }
            JsfUtil.update("frmDatos");
            JsfUtil.executeJS("PF('wvDatos').show();");
        } else {
            JsfUtil.update("frmdlgSolicitante");
            JsfUtil.executeJS("PF('dlgSolicitante').show();");
        }
    }

    public void setDataEquipo() {
        this.dataIngreso.put("equipo", ((CbpactSolicitudEquipo) this.dataIngreso.get("solicitud")).getEquipo());
    }

    public void setValSol() {
        dataIngreso.put("valSol", ((CxcmCliente) dataIngreso.get("cliente")).getCxcmClientePK().getCXCCLIcodigo());
        busquedaSolicitante();
    }

    public void busquedaOficina() {
        try {
            Object o = this.generalEjb.consultarOficina((String) this.dataIngreso.get("valOfi"));
            if (o != null) {
                this.dataIngreso.put("oficina", (CbpgenrOficina) o);
                m.put("1", ((CbpgenrOficina) dataIngreso.get("oficina")).getGenofiSecuencia());
            } else {
                JsfUtil.update("frmdlgOficina");
                JsfUtil.executeJS("PF('dlgOficina').show();");
            }
        } catch (Exception e) {
            Logger.getLogger(IngresoBodegaView.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public HashMap getDataIngreso() {
        return dataIngreso;
    }

    public void setDataIngreso(HashMap dataIngreso) {
        this.dataIngreso = dataIngreso;
    }

    public CbpgenrOficinaLazyTest getOficinasLazy() {
        return oficinasLazy;
    }

    public void setOficinasLazy(CbpgenrOficinaLazyTest oficinasLazy) {
        this.oficinasLazy = oficinasLazy;
    }

    public CxcmClienteLazy getClientes() {
        return clientes;
    }

    public void setClientes(CxcmClienteLazy clientes) {
        this.clientes = clientes;
    }

    public List<CbpactSolicitudEquipo> getConsignaciones() {
        return consignaciones;
    }

    public void setConsignaciones(List<CbpactSolicitudEquipo> consignaciones) {
        this.consignaciones = consignaciones;
    }

    public CbpactSolicitudEquipoLazy getSolicitudes() {
        return solicitudes;
    }

    public void setSolicitudes(CbpactSolicitudEquipoLazy solicitudes) {
        this.solicitudes = solicitudes;
    }

}
