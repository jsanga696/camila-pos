/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.acl.menu;

import com.gma.camilabp.domain.CbpsegmFuncion;
import com.gma.camilabp.domain.CbpsegmModulo;
import com.gma.camilabp.domain.CbpsegmOpcion;
import com.gma.camilabp.domain.CbpsegmPaquete;
import com.gma.camilabp.web.acl.MenuView;
import com.gma.camilabp.web.lazy.CbpsegmPaqueteLazy;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.session.UserSession;
import com.gma.camilabp.web.util.JsfUtil;
import com.gma.camilabp.web.util.StringValues;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

/**
 *
 * @author userdb6
 */
@Named(value = "gestionMenu")
@ViewScoped
public class GestionMenu implements Serializable{
    @Inject
    private MenuView menuView;
    @Inject
    private UserSession userSession;
    @EJB
    private EntityManagerLocal entity;
    
    protected CbpsegmPaqueteLazy paquetesLazy;
    protected CbpsegmPaquete paquete;
    protected CbpsegmModulo modulo;
    protected CbpsegmOpcion opcion;
    
    @PostConstruct
    public void init() {
        try{
            this.paquetesLazy= new CbpsegmPaqueteLazy();
        } catch (Exception e) {
            Logger.getLogger(GestionMenu.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void actualizarMenu(){
        try{
            this.menuView.init();
        } catch (Exception e) {
            Logger.getLogger(GestionMenu.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void registrarPaquete(){
        try{
            this.paquete = new CbpsegmPaquete();
            this.paquete.setSegpaqCreauser(this.userSession.getCbpsegmUsuario().getSegusuCodigo());
            this.seleccionarPaquete();
        } catch (Exception e) {
            Logger.getLogger(GestionMenu.class.getName()).log(Level.SEVERE, null, e);
        }
    }
            
    public void seleccionarPaquete(){
        try{
            if(this.paquete!=null){
                this.modulo = new CbpsegmModulo();
                JsfUtil.update("frmdlgPaquete");
                JsfUtil.executeJS("PF('dlgPaquete').show();");
            }
        } catch (Exception e) {
            Logger.getLogger(GestionMenu.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void grabarPaquete(){
        this.paquete.setSegpaqDescripcio(this.paquete.getSegpaqDescripcio().trim().toUpperCase());
        this.grabarRegistroPaquete(this.paquete);
    }
    
    public void grabarRegistroPaquete(CbpsegmPaquete p){
        try{
            List<CbpsegmModulo> modulos = (List<CbpsegmModulo>) p.getCbpsegmModuloCollection();
            p.setCbpsegmModuloCollection(null);
            p=(CbpsegmPaquete) this.entity.merge(p);
            if(modulos!=null){
                for (CbpsegmModulo modulo1 : modulos) {
                    modulo1.setSegpaqCodigo(p);
                    modulo1.setSegmodCreauser(userSession.getCbpsegmUsuario().getSegusuCodigo());
                    this.entity.merge(modulo1);
                }
            }
            JsfUtil.messageInfo(null, StringValues.msgRegistroExitoso, "");
            JsfUtil.executeJS("PF('dlgPaquete').hide();");
        } catch (Exception e) {
            JsfUtil.messageError(null, StringValues.msgRegistroNoExitoso, "");
            Logger.getLogger(GestionMenu.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void seleccionarModulo(){
        try{
            if(this.modulo!=null){
                this.opcion = new CbpsegmOpcion();
                JsfUtil.update("frmdlgOpcion");
                JsfUtil.executeJS("PF('dlgOpcion').show();");
            }
        } catch (Exception e) {
            Logger.getLogger(GestionMenu.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void agregarModulo(){
        try{
            if(this.paquete!=null && this.modulo!=null && this.modulo.getSegmodDescripcio()!=null){
                if(this.paquete.getCbpsegmModuloCollection()==null)
                    this.paquete.setCbpsegmModuloCollection(new ArrayList<>());
                CbpsegmModulo m = new CbpsegmModulo();
                m.setSegmodDescripcio(this.modulo.getSegmodDescripcio().toUpperCase());
                this.paquete.getCbpsegmModuloCollection().add(m);
                JsfUtil.messageInfo(null, "Item Agregado", "");
            }
        } catch (Exception e) {
            Logger.getLogger(GestionMenu.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void eliminarModulo(){
        try{
            
        } catch (Exception e) {
            Logger.getLogger(GestionMenu.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void agregarOpcion(){
        try{
            if(this.modulo!=null && this.opcion!=null && this.opcion.getSegopcDescripcio()!=null && this.opcion.getSegopcUrl()!=null){
                if(this.modulo.getCbpsegmOpcionCollection()==null)
                    this.modulo.setCbpsegmOpcionCollection(new ArrayList<>());
                for (CbpsegmOpcion cbpsegmOpcion : this.modulo.getCbpsegmOpcionCollection()) {
                    if(cbpsegmOpcion.getSegopcSecuencia().equalsIgnoreCase(this.opcion.getSegopcSecuencia())){
                        JsfUtil.messageInfo(null, "SECUENCIA YA REGISTRADA", "");
                        return;
                    }
                }
                CbpsegmOpcion o = new CbpsegmOpcion();
                o.setSegopcDescripcio(this.opcion.getSegopcDescripcio().toUpperCase());
                o.setSegopcUrl(this.opcion.getSegopcUrl());
                o.setSegopcSecuencia(this.opcion.getSegopcSecuencia().toUpperCase());
                o.setSegopcAcceso(this.opcion.getSegopcAcceso());
                o.setSegopcMenu(this.opcion.getSegopcMenu());
                o.setSegopcTurno(this.opcion.getSegopcTurno());
                this.modulo.getCbpsegmOpcionCollection().add(o);
                JsfUtil.messageInfo(null, "Item Agregado", "");
            }
        } catch (Exception e) {
            Logger.getLogger(GestionMenu.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void grabarModulo(CbpsegmModulo m){
        try{
            m.setSegmodDescripcio(m.getSegmodDescripcio().toUpperCase());
            this.entity.merge(m);
            JsfUtil.messageInfo(null, StringValues.msgRegistroExitoso, "");
        } catch (Exception e) {
            JsfUtil.messageError(null, StringValues.msgRegistroNoExitoso, "");
            Logger.getLogger(GestionMenu.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void grabarModulo(){
        try{
            for (CbpsegmOpcion op : this.modulo.getCbpsegmOpcionCollection()) {
                op.setSegopcDescripcio(op.getSegopcDescripcio().toUpperCase());
                op.setSegmodCodigo(this.modulo);
                op.setSegpaqCodigo(this.modulo.getSegpaqCodigo());
                op.setSegopcCreauser(userSession.getCbpsegmUsuario().getSegusuCodigo());
                op.setSegfunCodigo(new CbpsegmFuncion(1L));
                this.entity.merge(op);
            }
            JsfUtil.messageInfo(null, StringValues.msgRegistroExitoso, "");
        } catch (Exception e) {
            JsfUtil.messageError(null, StringValues.msgRegistroNoExitoso, "");
            Logger.getLogger(GestionMenu.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void grabarOpcion(CbpsegmOpcion op){
        try{
            op.setSegopcDescripcio(op.getSegopcDescripcio().toUpperCase());
            this.entity.merge(op);
            JsfUtil.messageInfo(null, StringValues.msgRegistroExitoso, "");
        } catch (Exception e) {
            JsfUtil.messageError(null, StringValues.msgRegistroNoExitoso, "");
            Logger.getLogger(GestionMenu.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public CbpsegmPaqueteLazy getPaquetesLazy() {
        return paquetesLazy;
    }

    public void setPaquetesLazy(CbpsegmPaqueteLazy paquetesLazy) {
        this.paquetesLazy = paquetesLazy;
    }

    public CbpsegmPaquete getPaquete() {
        return paquete;
    }

    public void setPaquete(CbpsegmPaquete paquete) {
        this.paquete = paquete;
    }

    public CbpsegmModulo getModulo() {
        return modulo;
    }

    public void setModulo(CbpsegmModulo modulo) {
        this.modulo = modulo;
    }

    public CbpsegmOpcion getOpcion() {
        return opcion;
    }

    public void setOpcion(CbpsegmOpcion opcion) {
        this.opcion = opcion;
    }
    
}
