/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.acl;


import com.gma.camilabp.domain.storedprocedure.UsuarioPrueba;
import com.gma.camilabp.web.service.SecuenciaLocal;
import com.gma.camilabp.web.util.JsfUtil;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/**
 *
 * @author userdb
 */
@Named
@ViewScoped
public class PruebaBean implements Serializable{
    private String cad="dd";
    
    @EJB
    private SecuenciaLocal secuenciaLocal;
    
    private List<UsuarioPrueba> usuarioPruebaList = new ArrayList<>();
    
    public PruebaBean() {
    }
    @PostConstruct
    public void init() {
       
     /*   JsfUtil.executeJS("document.getElementById('formId:db').rendered = true;");
        
        JsfUtil.executeJS("PF('da').disabled();");
        
        JsfUtil.executeJS("PF('formId:da').disabled();");
       */ 
        /*
        UIComponent component = findComponent("db");
        System.out.println("____ "+component.getId());
        
       component.getAttributes().put("value", "dddd");
       */
    }
    public UIComponent findComponent(String id) {

        UIComponent result = null;
        UIComponent root = FacesContext.getCurrentInstance().getViewRoot();
        if (root != null) {
        result = findComponent(root, id);
        }
        return result;

    }

    private UIComponent findComponent(UIComponent root, String id) {
        UIComponent result = null;
        if (root.getId().equals(id))
            return root;

        for (UIComponent child : root.getChildren()) {
        if (child.getId().equals(id)) {
            result = child;
            break;
        }
        result = findComponent(child, id);
        if (result != null)
            break;
        }
        return result;
    }
    public void prueba(){
        usuarioPruebaList = secuenciaLocal.getUsuarios();
   
        JsfUtil.update("formId");
    }

    public String getCad() {
        return cad;
    }

    public void setCad(String cad) {
        this.cad = cad;
    }

    public List<UsuarioPrueba> getUsuarioPruebaList() {
        return usuarioPruebaList;
    }

    public void setUsuarioPruebaList(List<UsuarioPrueba> usuarioPruebaList) {
        this.usuarioPruebaList = usuarioPruebaList;
    }

}
