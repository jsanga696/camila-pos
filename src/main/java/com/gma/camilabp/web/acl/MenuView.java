/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.acl;


import com.gma.camilabp.domain.CbpsegmPaquete;
import com.gma.camilabp.web.service.MenuLocal;
import com.gma.camilabp.web.session.UserSession;
import com.gma.camilabp.web.util.JsfUtil;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;

/**
 *
 * @author userdb
 */
@Named
@SessionScoped
public class MenuView implements Serializable{
    private MenuModel model;
    private List<CbpsegmPaquete> segmPaqueteList;
    @Inject
    private UserSession userSession;
    @EJB
    private MenuLocal menuLocal;
    /**
     * Creates a new instance of MenuView
     */
    public MenuView() {
    }
    @PostConstruct
    public void init(){
        model = new DefaultMenuModel();
        //segmPaqueteList = menuLocal.getCbpsegmPaqueteList(true);
        //llenarMenu();
        llenarMenuByUser();
    }

    public MenuModel getModel() {
        return model;
    }

    public void setModel(MenuModel model) {
        this.model = model;
    }
    
    /*private void llenarMenu(){
        for (CbpsegmPaquete spaq : segmPaqueteList) {
            DefaultSubMenu nivel1Submenu = new DefaultSubMenu(spaq.getSegpaqDescripcio());
            for (CbpsegmModulo smod : spaq.getCbpsegmModuloCollection()) {
                DefaultSubMenu nivel2Submenu = new DefaultSubMenu(smod.getSegmodDescripcio());
                for (CbpsegmOpcion sopc : smod.getCbpsegmOpcionCollection()) {
                    DefaultMenuItem item = new DefaultMenuItem(sopc.getSegopcDescripcio());
                    item.setUrl(sopc.getSegopcUrl());
                   // item.setIcon("ui-icon-home");
                    nivel2Submenu.addElement(item);
                }
                nivel1Submenu.addElement(nivel2Submenu);
            }
            model.addElement(nivel1Submenu);
        }
    }*/
    
    private void llenarMenuByUser(){
        List<CbpsegmPaquete> listPaquetes = menuLocal.getCbpsegmPaqueteListByUser(userSession.getCbpsegmUsuario());
        listPaquetes.forEach((p) -> {
            DefaultSubMenu nivel1Submenu = new DefaultSubMenu(p.getSegpaqDescripcio());
            menuLocal.getCbpsegmModuloListByUser(p, userSession.getCbpsegmUsuario()).forEach((m) -> {
                DefaultSubMenu nivel2Submenu = new DefaultSubMenu(m.getSegmodDescripcio());
                menuLocal.getCbpsegmOpcionListByUser(p, m, userSession.getCbpsegmUsuario()).stream().map((o) -> {
                    DefaultMenuItem item = new DefaultMenuItem(o.getSegopcDescripcio());
                    item.setUrl(o.getSegopcUrl());
                    return item;
                }).forEachOrdered((item) -> {
                    nivel2Submenu.addElement(item);
                });
                nivel1Submenu.addElement(nivel2Submenu);
            });
            model.addElement(nivel1Submenu);
        });        
    }
    
    public void irA(String url){
        JsfUtil.redirectFaces(url);
    }
}
