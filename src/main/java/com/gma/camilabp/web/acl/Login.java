/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.acl;

import com.gma.camilabp.domain.CbpsegmPerfilrestricion;
import com.gma.camilabp.domain.CbpsegmUsuario;
import com.gma.camilabp.domain.GenmCaja;
import com.gma.camilabp.model.logica.Render;
import com.gma.camilabp.sql.QuerysPos;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.service.PuntoVentaLocal;
import com.gma.camilabp.web.session.UserSession;
import com.gma.camilabp.web.util.JsfUtil;
import com.gma.camilabp.web.util.Utilidades;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author dbriones
 */
@Named
@ViewScoped
public class Login implements Serializable{
    @Inject
    private UserSession userSession1;
    @Inject
    private MenuView menuView;
    @EJB
    private PuntoVentaLocal posService;
    @EJB
    private EntityManagerLocal entity;
    protected String user;
    protected String pass;
    protected String empresa=null;
    private Map<String, Object> parametros;
    /** Creates a new instance of Login */
    public Login() {
        
    }
    
    public void redirectRecuperaClave(){
        JsfUtil.redirectFaces("/faces/recuperarClave.xhtml");
    }
    
    public void doLogin(){        
        if (!formValido()) {
            return;
        }    
        CbpsegmUsuario usuario1 = null;
        GenmCaja caja = null;
        
        this.parametros= new HashMap<>();
        this.parametros.put("segusuLogin", this.user.trim());
        this.parametros.put("segusuClave", Utilidades.password("E", this.pass.trim().toUpperCase()));
        this.parametros.put("segusuEstado", Boolean.TRUE);
        usuario1 = this.entity.findObject(CbpsegmUsuario.class, this.parametros);
        if (usuario1!=null) {
            JsfUtil.messageInfo(null, "Bienvenid@ "+user.trim(), "");
        }
        
        
        // si el login fue exisoto:
        if(usuario1!=null){
            parametros= new HashMap<>();
            parametros.put("segusuCodigo", usuario1);
            parametros.put("estado", Boolean.TRUE);
            userSession1.setOficinas(entity.findAll(QuerysPos.oficinasByUser, parametros));
            this.parametros= new HashMap<>();
            System.out.println("usuario1.getSegusuLogin(): " + usuario1.getSegusuLogin());
            this.parametros.put("sEGUSUcodigo", usuario1.getSegusuLogin());
            this.parametros.put("gENCAJestado", "A");
            caja = entity.findObject(GenmCaja.class, this.parametros);
            userSession1.setCaja(caja);           
            userSession1.setTurno(posService.consultarTurno(usuario1));
            userSession1.setCbpsegmUsuario(usuario1);
            userSession1.setName(usuario1.getSegusuNombre()+" "+usuario1.getSegusuApellido());
            userSession1.setFecha(new Date());
            userSession1.setCodigoCF((String) posService.getValorDefault(1, usuario1.getGenciaCodigo().getGenciaSecuencia(), null, "CODCLISRI"));
            userSession1.setFormaRv("B".equalsIgnoreCase((String) posService.getValorDefault(1, usuario1.getGenciaCodigo().getGenciaSecuencia(), null, "POSRV")));
            userSession1.setVisualizarDv("S".equalsIgnoreCase((String) posService.getValorDefault(1, usuario1.getGenciaCodigo().getGenciaSecuencia(), null, "POSDV")));
            userSession1.setValidacionStock(!"N".equalsIgnoreCase((String) posService.getValorDefault(1, usuario1.getGenciaCodigo().getGenciaSecuencia(), null, "POSTIPOVENTA")));
            userSession1.setControlPassword("S".equalsIgnoreCase((String) posService.getValorDefault(1, usuario1.getGenciaCodigo().getGenciaSecuencia(), null, "CONTROLPASS")));
            if(userSession1.getCaja()!=null)
                userSession1.setVisualizarItem("S".equalsIgnoreCase((String) posService.getValorDefault(2, usuario1.getGenciaCodigo().getGenciaSecuencia(), userSession1.getCaja().getGENOFIcodigo(), "POSVERITEMS")));
            if(usuario1.getSeggppCodigo()!=null)
                userSession1.setAccesos(posService.getCbpsegmOpcionListByUser(usuario1));
            userSession1.setPagDefault(posService.getCbpsegmOpcionListDefault());
            userSession1.setPagSinTurno(posService.getCbpsegmOpcionListSinTurno());
            this.menuView.init();
            /* RESTRICCIONES */
            this.parametros= new HashMap<>();
            this.parametros.put("segprrEstado", true);
            this.parametros.put("seggppCodigo", usuario1.getSeggppCodigo().getSeggppCodigo());
            this.parametros.put("genciaCodigo", usuario1.getGenciaCodigo().getGenciaCodigo());
            Render r = new Render();
            r.registrarRestricciones(entity.findAllByParameter(CbpsegmPerfilrestricion.class, parametros,null));
            userSession1.setRender(r);
            userSession1.redirectUrlSolicitada();
        }
        else{
            JsfUtil.messageError(null, "Usuario o Clave incorrecta", "");
        }
        
    }
    
    public Boolean formValido() {
        Boolean esValido = true;
        if (Utilidades.isEmptyTrim(user) || Utilidades.isEmptyTrim(pass)) {
            esValido = false;
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Usuario y/o Contraseña vacios", ""));
        }        
        return esValido;
    }
    

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

}
