/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.acl;

import com.gma.camilabp.domain.CbpsegmUsuario;
import com.gma.camilabp.web.configuracion.Informacion;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.session.UserSession;
import com.gma.camilabp.web.util.Utilidades;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

/**
 *
 * @author Acer
 */
@Named(value = "cambioPassword")
@ViewScoped
public class CambioPassword implements Serializable{
    @Inject
    private UserSession userSession;
    @EJB
    private EntityManagerLocal entity;
    
    protected CbpsegmUsuario usuario;    
    
    @PostConstruct
    public void init() {
        try {
            this.usuario = this.entity.find(CbpsegmUsuario.class, this.userSession.getCbpsegmUsuario().getSegusuCodigo());
            System.out.println(Utilidades.password("D", this.usuario.getSegusuClave()));
        } catch (Exception e) {
            Logger.getLogger(CambioPassword.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void cambiarPassword(){
        try {
            if(this.usuario!=null){
                //this.entity.persist(this.usuario);
                System.out.println(this.usuario.getSegusuClave());
                System.out.println(Utilidades.password("E", this.usuario.getSegusuClave()));
            }
        } catch (Exception e) {
            Logger.getLogger(CambioPassword.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public CbpsegmUsuario getUsuario() {
        return usuario;
    }

    public void setUsuario(CbpsegmUsuario usuario) {
        this.usuario = usuario;
    }
    
}
