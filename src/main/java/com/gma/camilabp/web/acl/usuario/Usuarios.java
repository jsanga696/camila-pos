/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.acl.usuario;

import com.gma.camilabp.web.lazy.CbpsegmUsuarioLazy;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

/**
 *
 * @author userdb6
 */
@Named(value = "usuarios")
@ViewScoped
public class Usuarios implements Serializable{
    protected CbpsegmUsuarioLazy usuariosLazy;
    
    @PostConstruct
    public void init() {
        try{
            this.usuariosLazy = new CbpsegmUsuarioLazy();
        } catch (Exception e) {
            Logger.getLogger(Usuarios.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public CbpsegmUsuarioLazy getUsuariosLazy() {
        return usuariosLazy;
    }

    public void setUsuariosLazy(CbpsegmUsuarioLazy usuariosLazy) {
        this.usuariosLazy = usuariosLazy;
    }
    
}
