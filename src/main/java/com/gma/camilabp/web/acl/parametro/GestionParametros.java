/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.acl.parametro;

import com.gma.camilabp.domain.GenpParamcompania;
import com.gma.camilabp.domain.GenpParamcompaniaPK;
import com.gma.camilabp.web.lazy.GenpParamcompaniaLazy;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.session.UserSession;
import com.gma.camilabp.web.util.JsfUtil;
import com.gma.camilabp.web.util.StringValues;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

/**
 *
 * @author userdb6
 */
@Named(value = "gestionParametros")
@ViewScoped
public class GestionParametros implements Serializable{
    @EJB
    private EntityManagerLocal entity;
    @Inject
    private UserSession userSession;
    
    protected GenpParamcompaniaLazy parametrosLazy;
    protected GenpParamcompania parametro;
    protected GenpParamcompania parametroIngreso;
    
    @PostConstruct
    public void init() {
        try{
            this.parametrosLazy= new GenpParamcompaniaLazy();
            this.parametroIngreso = new GenpParamcompania(new GenpParamcompaniaPK());
        } catch (Exception e) {
            Logger.getLogger(GestionParametros.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void agregarParametro(){
        try{
            if(this.parametroIngreso.getGenpParamcompaniaPK().getGENAPLcodigo()!=null
                    && this.parametroIngreso.getGenpParamcompaniaPK().getGENPCOetiqueta()!=null
                    && this.parametroIngreso.getGENPCOnombre()!=null && this.parametroIngreso.getGENPCOnombrevalor()!=null){
                this.parametroIngreso.getGenpParamcompaniaPK().setGENCIAcodigo(this.userSession.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia());
                this.entity.merge(this.parametroIngreso);
                this.parametrosLazy= new GenpParamcompaniaLazy();
                JsfUtil.messageInfo(null, StringValues.msgRegistroExitoso, "");
            }else{
                JsfUtil.messageWarning(null, "CAMPOS OBLIGATORIOS", "");
            }
        } catch (Exception e) {
            Logger.getLogger(GestionParametros.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void editarParametro(GenpParamcompania p){
        try{
            this.entity.merge(p);
            JsfUtil.messageInfo(null, StringValues.msgRegistroExitoso, "");
        } catch (Exception e) {
            Logger.getLogger(GestionParametros.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public GenpParamcompaniaLazy getParametrosLazy() {
        return parametrosLazy;
    }

    public void setParametrosLazy(GenpParamcompaniaLazy parametrosLazy) {
        this.parametrosLazy = parametrosLazy;
    }

    public GenpParamcompania getParametro() {
        return parametro;
    }

    public void setParametro(GenpParamcompania parametro) {
        this.parametro = parametro;
    }

    public GenpParamcompania getParametroIngreso() {
        return parametroIngreso;
    }

    public void setParametroIngreso(GenpParamcompania parametroIngreso) {
        this.parametroIngreso = parametroIngreso;
    }
    
}
