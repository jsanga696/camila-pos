/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.acl;


import com.gma.camilabp.web.session.UserSession;
import com.gma.camilabp.web.util.JsfUtil;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author userdb
 */
@Named
@ViewScoped
public class Logout implements Serializable{
    @Inject
    private UserSession userSession;
    public Logout() {
    }
    @PostConstruct
    public void init() {
        this.logout();
    }
    
    private void logout(){
        try {
            /*UserSession userSession1 = (UserSession) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userSession");       
            userSession1.setCbpsegmUsuario(null);
            userSession1.setUserId((long) -1);
            userSession1.setName("invitado");*/
            userSession.logout();
            JsfUtil.redirectFaces("/login.xhtml");
        }
        catch(Exception ex){
            System.out.println("Session cerrada");
        }
        
        
    }

}
