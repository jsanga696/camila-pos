/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.configuracion;

import com.gma.camilabp.domain.CbpgenrBodega;
import com.gma.camilabp.domain.CbpgenrControltrans;
import com.gma.camilabp.domain.CbpsegmUsuario;
import com.gma.camilabp.sql.QuerysPos;
import com.gma.camilabp.web.lazy.CbpgenrControltransLazy;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.session.UserSession;
import com.gma.camilabp.web.util.JsfUtil;
import com.gma.camilabp.web.util.StringValues;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

/**
 *
 * @author Acer
 */
@Named(value = "controlTransaccional")
@ViewScoped
public class ControlTransaccional implements Serializable{
    @Inject
    private UserSession userSession;
    @EJB
    private EntityManagerLocal entity;
    
    protected CbpgenrControltrans controltran;
    protected List<CbpgenrBodega> bodegas;
    protected CbpgenrControltransLazy controlLazy;
    protected CbpgenrControltrans controlSeleccion;
    
    private Map<String, Object> parametros;
    
    @PostConstruct
    public void init() {
        try {
            this.controltran = new CbpgenrControltrans();
            this.bodegas = this.entity.findAll(CbpgenrBodega.class);
            this.controlLazy = new CbpgenrControltransLazy();
        } catch (Exception e) {
            Logger.getLogger(ControlTransaccional.class.getName()).log(Level.SEVERE, null, e);
        }
    }        
    
    public CbpsegmUsuario usuarioSupervidorBodega(CbpgenrBodega b){
        CbpsegmUsuario u=null;
        try{
            this.parametros= new HashMap<>();
            this.parametros.put("0", b.getFacsupCodigo());
            String codUsuSup = (String) this.entity.findObject("SQL","SELECT SEGUSU_codigo FROM FACR_SUPERVISOR WHERE SEGUSU_codigo IS NOT NULL AND FACSUP_codigo=?", this.parametros, null,Boolean.TRUE);
            if(codUsuSup!=null){
                this.parametros= new HashMap<>();
                this.parametros.put("segusuLogin", codUsuSup);
                u = this.entity.findObject(CbpsegmUsuario.class, this.parametros);
            }
        } catch (Exception e) {
            Logger.getLogger(ControlTransaccional.class.getName()).log(Level.SEVERE, null, e);
        }
        return u;
    }
    
    public void grabarRegistroControl(){
        try{
            if(this.controltran.getGenrconPeriodo() || this.controltran.getGenrconTransaccion()){
                if(this.controltran.getGenrconPeriodo() && this.controltran.getGenrconDias()==null){
                    JsfUtil.messageWarning(null, "DEBE INGRESAR CANTIDAD DE DIAS", "");
                    return;
                }
                if(this.controltran.getGenrconTransaccion() && this.controltran.getGenrconCantidad()==null){
                    JsfUtil.messageWarning(null, "DEBE INGRESAR CANTIDAD", "");
                    return;
                }
                if(this.controltran.getGenbodCodigo() == null){
                    //TODOS
                    this.parametros = new HashMap<>();                    
                    for (CbpgenrBodega bodega : this.bodegas) {
                        this.parametros.put("0", bodega.getFacsupCodigo());
                        CbpsegmUsuario u = this.usuarioSupervidorBodega(bodega);
                        this.controltran.setGenbodCodigo(bodega);
                        this.controltran.setSegusuCodigoSup(u);
                        this.controltran.setSegusuCodigo(this.userSession.getCbpsegmUsuario());
                        this.controltran.setGenrconFechaact((Date) this.entity.findObject("SQL", QuerysPos.fechaClaveSupervisor, this.parametros, null, Boolean.TRUE));
                        if(u!=null)
                            this.entity.merge(this.controltran);
                    }
                }else{
                    CbpsegmUsuario u = this.usuarioSupervidorBodega(this.controltran.getGenbodCodigo());
                    this.parametros = new HashMap<>();
                    this.parametros.put("0", this.controltran.getGenbodCodigo().getFacsupCodigo());
                    this.controltran.setSegusuCodigoSup(u);
                    this.controltran.setSegusuCodigo(this.userSession.getCbpsegmUsuario());                    
                    this.controltran.setGenrconFechaact((Date) this.entity.findObject("SQL", QuerysPos.fechaClaveSupervisor, this.parametros, null, Boolean.TRUE));
                    if(u!=null)
                        this.entity.merge(this.controltran);
                }
                this.init();
                JsfUtil.messageInfo(null, "PROCESO REALIZADO", "");
            }else{
                JsfUtil.messageWarning(null, "DEBE SELECCIONAR AL MENOS UN TIPO DE CONTROL", "");
            }
        } catch (Exception e) {
            JsfUtil.messageWarning(null, "ERROR EN PROCESO", "");
            Logger.getLogger(ControlTransaccional.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void actualizarRegistro(CbpgenrControltrans ct){
        try{
            if(ct.getGenrconPeriodo() && ct.getGenrconDias()==null){
                JsfUtil.messageWarning(null, "DEBE INGRESAR CANTIDAD DE DIAS", "");
                return;
            }
            if(ct.getGenrconTransaccion() && ct.getGenrconCantidad()==null){
                JsfUtil.messageWarning(null, "DEBE INGRESAR CANTIDAD", "");
                return;
            }
            ct.setGenrconActualizacion(new Date());
            this.entity.merge(ct);
            JsfUtil.messageInfo(null, StringValues.msgRegistroExitoso, "");
        } catch (Exception e) {
            JsfUtil.messageWarning(null, "ERROR EN PROCESO", "");
            Logger.getLogger(ControlTransaccional.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public CbpgenrControltrans getControltran() {
        return controltran;
    }

    public void setControltran(CbpgenrControltrans controltran) {
        this.controltran = controltran;
    }

    public List<CbpgenrBodega> getBodegas() {
        return bodegas;
    }

    public void setBodegas(List<CbpgenrBodega> bodegas) {
        this.bodegas = bodegas;
    }

    public CbpgenrControltransLazy getControlLazy() {
        return controlLazy;
    }

    public void setControlLazy(CbpgenrControltransLazy controlLazy) {
        this.controlLazy = controlLazy;
    }

    public CbpgenrControltrans getControlSeleccion() {
        return controlSeleccion;
    }

    public void setControlSeleccion(CbpgenrControltrans controlSeleccion) {
        this.controlSeleccion = controlSeleccion;
    }
    
}
