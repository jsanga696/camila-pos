/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.configuracion;

import com.gma.camilabp.domain.CbpgenrBodega;
import com.gma.camilabp.domain.CbpgenrControltrans;
import com.gma.camilabp.sql.QuerysPos;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.service.PuntoVentaLocal;
import com.gma.camilabp.web.session.UserSession;
import com.gma.camilabp.web.util.JsfUtil;
import com.gma.camilabp.web.util.Utilidades;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

/**
 *
 * @author Acer
 */
@Named(value = "informacion")
@ViewScoped
public class Informacion implements Serializable{
    @Inject
    private UserSession userSession;
    @EJB
    private PuntoVentaLocal posService;
    @EJB
    private EntityManagerLocal entity;
    
    protected Boolean supervisor= Boolean.FALSE;
    protected List<CbpgenrControltrans> listControl;
    protected Boolean actualizarClave= Boolean.FALSE;
    
    private Map<String, Object> parametros;
    private String claveSupervisor;
    private Date fechaActualizacion;
    protected String claveNuevaSupervisor;
    protected String claveNuevaValidacionSupervisor;

    @PostConstruct
    public void init() {
        try {
            this.parametros= new HashMap<>();;
            this.supervisor = this.posService.esSupervisor(this.userSession.getCbpsegmUsuario());
            if(this.supervisor){
                this.parametros.put("segusuCodigoSup", this.userSession.getCbpsegmUsuario());
                this.parametros.put("genrconEstado", Boolean.TRUE);
                this.listControl = this.entity.findAllByParameter(CbpgenrControltrans.class, this.parametros,null);
                for (CbpgenrControltrans ct : listControl) {
                    this.parametros = new HashMap<>();
                    this.parametros.put("segusuCodigo", this.userSession.getCbpsegmUsuario().getSegusuCodigo());
                    this.parametros.put("desde", ct.getGenrconFechaact());
                    this.parametros.put("hasta", new Date());
                    ct.setTransacciones((Long)this.entity.findObject("HQL", "SELECT COUNT(l) FROM CbplogBitacora l WHERE l.segusuCodigo = :segusuCodigo AND l.cbpbitFecha BETWEEN :desde AND :hasta", parametros, null, Boolean.FALSE));
                    if(ct.getGenrconActualizar() || 
                            (ct.getGenrconPeriodo()?(ct.getDias()>=ct.getGenrconDias().intValue()):false) || 
                            (ct.getGenrconTransaccion()?(ct.getTransacciones().intValue()>ct.getGenrconCantidad().intValue()):false)){
                        this.actualizarClave=Boolean.TRUE;
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(Informacion.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void grabarClaveSupervisor(){
        try{
            System.out.println(this.claveNuevaSupervisor);
            System.out.println(this.claveNuevaValidacionSupervisor);
            this.parametros = new HashMap<>();
            this.parametros.put("0", this.userSession.getCbpsegmUsuario().getSegusuLogin());
            String codigoSupervisor = (String) this.entity.findObject("SQL", QuerysPos.codigoSupervisor, this.parametros, null, Boolean.FALSE);
            this.parametros = new HashMap<>();
            this.parametros.put("0", codigoSupervisor);
            this.claveSupervisor = (String) this.entity.findObject("SQL", QuerysPos.claveSupervisor, this.parametros, null, Boolean.FALSE);
            
            if(this.claveNuevaSupervisor!=null && this.claveNuevaValidacionSupervisor!=null && this.claveNuevaSupervisor.equalsIgnoreCase(this.claveNuevaValidacionSupervisor)){
                String claveEncriptada= Utilidades.password("E", this.claveNuevaSupervisor.toUpperCase());
                System.out.println(claveEncriptada);
                System.out.println(this.claveSupervisor);
                if(!claveEncriptada.equalsIgnoreCase(this.claveSupervisor)){
                    this.parametros = new HashMap<>();
                    this.parametros.put("0", new Date());
                    entity.executeProcedure("UPDATE FACR_SUPERVISOR SET FACSUP_clave='" + claveEncriptada + "', FACSUP_editclave=? "+" WHERE FACSUP_codigo='"+codigoSupervisor+"'", this.parametros);
                    for (CbpgenrControltrans cbpgenrControltrans : listControl) {
                        cbpgenrControltrans.setGenrconActualizar(Boolean.FALSE);
                        cbpgenrControltrans.setGenrconFechaact(new Date());
                        this.entity.merge(cbpgenrControltrans);
                    }
                    JsfUtil.messageInfo(null, "ACTUALIZACION REALIZADA", "");
                    JsfUtil.executeJS("PF('dlgClaveSupervisor').hide();");
                }else{
                    JsfUtil.messageWarning(null, "CLAVE DEBE SER DIFERENTE A LA ACTUAL", "");
                }
            }else{
                JsfUtil.messageWarning(null, "CAMPOS OBLIGATORIOS. LOS VALORES DE CLAVE NUEVA DEBEN SER IGUALES", "");
            }
        } catch (Exception e) {
            Logger.getLogger(Informacion.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public Boolean getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(Boolean supervisor) {
        this.supervisor = supervisor;
    }

    public List<CbpgenrControltrans> getListControl() {
        return listControl;
    }

    public void setListControl(List<CbpgenrControltrans> listControl) {
        this.listControl = listControl;
    }    

    public Boolean getActualizarClave() {
        return actualizarClave;
    }

    public void setActualizarClave(Boolean actualizarClave) {
        this.actualizarClave = actualizarClave;
    }

    public String getClaveNuevaSupervisor() {
        return claveNuevaSupervisor;
    }

    public void setClaveNuevaSupervisor(String claveNuevaSupervisor) {
        this.claveNuevaSupervisor = claveNuevaSupervisor;
    }

    public String getClaveNuevaValidacionSupervisor() {
        return claveNuevaValidacionSupervisor;
    }

    public void setClaveNuevaValidacionSupervisor(String claveNuevaValidacionSupervisor) {
        this.claveNuevaValidacionSupervisor = claveNuevaValidacionSupervisor;
    }
    
}
