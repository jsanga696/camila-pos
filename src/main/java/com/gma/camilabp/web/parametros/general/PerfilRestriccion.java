/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.parametros.general;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author userdb
 */
public class PerfilRestriccion implements Serializable{
    private Long Id;
    private String nombre;
    private List <PerfilRestriccion> perfilRestriccionList = new ArrayList<>();

    public PerfilRestriccion(Long Id, String nombre) {
        this.Id = Id;
        this.nombre = nombre;
        this.perfilRestriccionList = new ArrayList<>();
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<PerfilRestriccion> getPerfilRestriccionList() {
        return perfilRestriccionList;
    }

    public void setPerfilRestriccionList(List<PerfilRestriccion> perfilRestriccionList) {
        this.perfilRestriccionList = perfilRestriccionList;
    }
 
}
