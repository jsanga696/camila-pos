/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.parametros.general;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author userdb
 */
public class Perfil implements Serializable, Comparable<Perfil>{
    private String nombre;
    private Long paqueteId;
    private Long moduloId;
    private Long funcionId;
    private Long opcionId;
    private boolean seleccionado;

    public Perfil(String nombre, Long paqueteId, Long moduloId, Long funcionId, Long opcionId) {
        this.nombre = nombre;
        this.paqueteId = paqueteId;
        this.moduloId = moduloId;
        this.funcionId = funcionId;
        this.opcionId = opcionId;
        this.seleccionado = false;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Long getPaqueteId() {
        return paqueteId;
    }

    public void setPaqueteId(Long paqueteId) {
        this.paqueteId = paqueteId;
    }

    public Long getModuloId() {
        return moduloId;
    }

    public void setModuloId(Long moduloId) {
        this.moduloId = moduloId;
    }

    public Long getFuncionId() {
        return funcionId;
    }

    public void setFuncionId(Long funcionId) {
        this.funcionId = funcionId;
    }

    public Long getOpcionId() {
        return opcionId;
    }

    public void setOpcionId(Long opcionId) {
        this.opcionId = opcionId;
    }

    public boolean isSeleccionado() {
        return seleccionado;
    }

    public void setSeleccionado(boolean seleccionado) {
        this.seleccionado = seleccionado;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
        result = prime * result + ((paqueteId == null) ? 0 : paqueteId.hashCode());
        result = prime * result + ((moduloId == null) ? 0 : moduloId.hashCode());
        result = prime * result + ((funcionId == null) ? 0 : funcionId.hashCode());
        result = prime * result + ((opcionId == null) ? 0 : opcionId.hashCode());
        return result;
    }
 
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Perfil other = (Perfil) obj;
        if (nombre == null) {
            if (other.nombre != null)
                return false;
        } else if (!nombre.equals(other.nombre))
            return false;
        if (paqueteId == null) {
            if (other.paqueteId != null)
                return false;
        } else if (!paqueteId.equals(other.paqueteId))
            return false;
        if (moduloId == null) {
            if (other.moduloId != null)
                return false;
        } else if (!moduloId.equals(other.moduloId))
            return false;
        if (funcionId == null) {
            if (other.funcionId != null)
                return false;
        } else if (!funcionId.equals(other.funcionId))
            return false;
        if (opcionId == null) {
            if (other.opcionId != null)
                return false;
        } else if (!opcionId.equals(other.opcionId))
            return false;
        return true;
    }
 
    @Override
    public String toString() {
        return nombre;
    }
 
    public int compareTo(Perfil perfil) {
        return this.getNombre().compareTo(perfil.getNombre());
    }
    
    
    
}
