/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.parametros.general;

import com.gma.camilabp.domain.CbpsegmPerfilrestricion;
import com.gma.camilabp.domain.CbpsegrGrupousuario;
import com.gma.camilabp.web.service.GeneralLocal;
import com.gma.camilabp.web.session.UserSession;
import com.gma.camilabp.web.util.JsfUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.PrimeFaces;
//import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

/**
 *
 * @author userdb4
 */
@Named(value = "asignarPerfilBean")
@ViewScoped
public class AsignacionPerfilBean implements Serializable{
    private static final Long serialVersionUID = 1L;
    @EJB
    private GeneralLocal generalLocal;
    @Inject
    private UserSession userSession;

    private CbpsegrGrupousuario grupoUsuario;
    private CbpsegrGrupousuario grupoUsuarioCargaPerfil;
    private List<PerfilRestriccion> perfilRestriccionList = new ArrayList<>();
    private List<CbpsegmPerfilrestricion> segmPerfilrestricionList = new ArrayList<>();
    private List<CbpsegmPerfilrestricion> segmPerfilrestricionBaseList = new ArrayList<>();
    private CbpsegrGrupousuario grupoUsuarioSelecc;
    private TreeNode rootPerfil;
    private TreeNode[] selectedNodes;
    private int tabIndexDG =0;
    
    @PostConstruct
    public void initView() {
    
    }
    
    public void openDlgPerfilRestriccion(CbpsegrGrupousuario gu) {
        grupoUsuario = gu;
        segmPerfilrestricionList= generalLocal.getSegmPerfilrestricionList(grupoUsuario.getSeggppCodigo());
        perfilRestriccionList = generalLocal.getPerfilRestriccionList();
        if (perfilRestriccionList == null) {
            perfilRestriccionList = new ArrayList<>();
        }
        rootPerfil = this.crearTreeNode();
        grupoUsuarioSelecc = null;
        segmPerfilrestricionBaseList = new ArrayList<>();
        JsfUtil.update("frmdlgPerfiles");
        //JsfUtil.executeJS("PF('dlgPerfiles').show()");
        
    }
    
    public TreeNode crearTreeNode(){
        TreeNode root = new DefaultTreeNode(new Perfil("Perfiles",0L,0L,0L,0L), null);
        perfilRestriccionList.stream().forEach(p->{
            TreeNode paquetes = new DefaultTreeNode(new Perfil(p.getNombre(),p.getId(),0L,0L,0L), root);
            p.getPerfilRestriccionList().stream().forEach(m->{
                    TreeNode modulo = new DefaultTreeNode(new Perfil(m.getNombre(),p.getId(),m.getId(),0L,0L), paquetes);
                    m.getPerfilRestriccionList().stream().forEach(f->{
                    //    if (f.getPerfilRestriccionList().size()>0) {
                            TreeNode funcion = new DefaultTreeNode(new Perfil(f.getNombre(),p.getId(),m.getId(),f.getId(),0L), modulo);
                            f.getPerfilRestriccionList().stream().forEach(o->{
                             //   TreeNode opcion = new DefaultTreeNode(new Perfil(o.getNombre(),p.getId(),m.getId(),f.getId(),o.getId()), funcion);
                                funcion.getChildren().add(new DefaultTreeNode(new Perfil(o.getNombre(),p.getId(),m.getId(),f.getId(),o.getId())));
                            });
                   //     }
                    });
            });
        });
        return root;
    }
    
    public CbpsegrGrupousuario getGrupoUsuarioSelecc() {
        return grupoUsuarioSelecc;
    }

    public void setGrupoUsuarioSelecc(CbpsegrGrupousuario grupoUsuarioSelecc) {
        this.grupoUsuarioSelecc = grupoUsuarioSelecc;
    }
    
    public CbpsegrGrupousuario getGrupoUsuarioCargaPerfil() {
        return grupoUsuarioCargaPerfil;
    }

    public void setGrupoUsuarioCargaPerfil(CbpsegrGrupousuario grupoUsuarioCargaPerfil) {
        this.grupoUsuarioCargaPerfil = grupoUsuarioCargaPerfil;
    }

    
    public boolean perfilRepetido(Perfil perfil)
    {
        return segmPerfilrestricionList.stream()
                .filter(p-> p.getGenciaCodigo() == userSession.getCompaniaId().longValue() && p.getSeggppCodigo().equals(grupoUsuario.getSeggppCodigo())
                && p.getSegopcCodigo().getSegopcCodigo().equals(perfil.getOpcionId()))
                .count()>0 ? true:false;
    }
    
    public void grupoUsuarioSeleccionada(SelectEvent event) {
        grupoUsuarioSelecc = (CbpsegrGrupousuario) event.getObject();
        segmPerfilrestricionBaseList = generalLocal.getSegmPerfilrestricionList(grupoUsuarioSelecc.getSeggppCodigo());
        openDlgPerfilRestriccion(grupoUsuarioSelecc);
        JsfUtil.update("frmdlgPerfiles:tabViewDG:dtPerfilAsignadosCP");
    }
    
    public void grupoUsuarioSeleccionada2(SelectEvent event) {
        grupoUsuarioSelecc = (CbpsegrGrupousuario) event.getObject();
        segmPerfilrestricionBaseList = generalLocal.getSegmPerfilrestricionList(grupoUsuarioSelecc.getSeggppCodigo());
        //openDlgPerfilRestriccion(grupoUsuarioSelecc);
        JsfUtil.update("frmdlgPerfiles:tabViewDG:dtPerfilAsignadosCP");
    }
    
    public List<CbpsegmPerfilrestricion> getSegmPerfilrestricionList() {
        return segmPerfilrestricionList;
    }

    public void setSegmPerfilrestricionList(List<CbpsegmPerfilrestricion> segmPerfilrestricionList) {
        this.segmPerfilrestricionList = segmPerfilrestricionList;
    }
    
    public List<CbpsegmPerfilrestricion> getSegmPerfilrestricionBaseList() {
        return segmPerfilrestricionBaseList;
    }
    
    
    public void setSegmPerfilrestricionBaseList(List<CbpsegmPerfilrestricion> segmPerfilrestricionBaseList) {
        this.segmPerfilrestricionBaseList = segmPerfilrestricionBaseList;
    }
    
    public void perfilSeleccionado(Perfil p) {
        if (p.isSeleccionado() && !perfilRepetido(p)) {
            generalLocal.saveSegmPerfilrestricion(p.getOpcionId(), grupoUsuario);
            segmPerfilrestricionList= generalLocal.getSegmPerfilrestricionList(grupoUsuario.getSeggppCodigo());
            JsfUtil.messageInfo(null, "Se agregaron las opciones de "+p.getNombre(), "");
            JsfUtil.update("frmdlgPerfiles:tabViewDG:dtPerfilAsignados");
        }
    }
    
    public void delete(CbpsegmPerfilrestricion pr){
        generalLocal.deleteSegmPerfilrestricion(pr);
        segmPerfilrestricionList= generalLocal.getSegmPerfilrestricionList(grupoUsuario.getSeggppCodigo());
        JsfUtil.messageInfo(null, "Se elimino la opcion de "+pr.getSegopcCodigo().getSegopcDescripcio()+" "+pr.getSegaccCodigo().getSegaccDescripcio(), "");
        JsfUtil.update("frmdlgPerfiles:tabViewDG:dtPerfilAsignados");
    }
    
    public TreeNode getRootPerfil() {
        return rootPerfil;
    }

    public void setRootPerfil(TreeNode rootPerfil) {
        this.rootPerfil = rootPerfil;
    }

    public TreeNode[] getSelectedNodes() {
        return selectedNodes;
    }
    
    public int getTabIndexDG() {
        return tabIndexDG;
    }

    public void setTabIndexDG(int tabIndexDG) {
        this.tabIndexDG = tabIndexDG;
    }
    
    public CbpsegrGrupousuario getGrupoUsuario() {
        return grupoUsuario;
    }

    public void setGrupoUsuario(CbpsegrGrupousuario grupoUsuario) {
        this.grupoUsuario = grupoUsuario;
    }
    
    public void abrirDF(){
        System.out.println("abrirDF");
        Map<String,Object> options = new HashMap<>();
        options.put("width", "70%");
        options.put("height", "550");
        options.put("closable", true);
        options.put("contentHeight", "100%"); 
        options.put("contentWidth", "100%"); 
        options.put("resizable", false);
        options.put("draggable", false);
        options.put("modal", true);
        /*options.put("responsive", true);  
        options.put("position", "center center");
        options.put("appendToBody", true);*/
        PrimeFaces.current().dialog().openDynamic("/pages/df/segrGrupousuarioDF.xhtml", options, null);
    }
    
    public void seleccionado(CbpsegrGrupousuario obj) {
        PrimeFaces.current().dialog().closeDynamic(obj);
    }
    
    public void asignarPerfiles()
    {
        try {
            validacion();
            generalLocal.asignarPerfiles(segmPerfilrestricionBaseList, grupoUsuario);
            segmPerfilrestricionList= generalLocal.getSegmPerfilrestricionList2(grupoUsuario.getSeggppCodigo());
            tabIndexDG =0;
            JsfUtil.messageInfo(null, "Se realizó la copia de los perfiles correctamente", "");
        } catch (Exception e) {
            JsfUtil.messageError(null, e.getMessage(), "");
        }
    }
    
    private void validacion() throws Exception
    {
        if (segmPerfilrestricionBaseList==null || segmPerfilrestricionBaseList.isEmpty()) {
            throw new Exception("El grupo de usuario "+grupoUsuarioSelecc.getSeggppSecuencia()+" "+grupoUsuarioSelecc.getSeggppDescripcio()+", no tienes opciones por copiar");
        }
        if (!segmPerfilrestricionList.isEmpty()) {
            throw new Exception("El grupo de usuario "+grupoUsuario.getSeggppSecuencia()+" "+grupoUsuario.getSeggppDescripcio()+", no debe tener opciones asignadas");
        }
        
    }
}
