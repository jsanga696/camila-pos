/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.parametros.general;

import com.gma.camilabp.domain.CbpsegmAccion;
import com.gma.camilabp.domain.CbpsegmOpcion;
import com.gma.camilabp.domain.CbpsegmPerfil;
import com.gma.camilabp.domain.CbpsegrGrupousuario;
import com.gma.camilabp.web.lazy.GrupousuarioLazy;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.service.GrupoUsuariosLocal;
import com.gma.camilabp.web.service.MenuLocal;
import com.gma.camilabp.web.session.UserSession;
import com.gma.camilabp.web.util.JsfUtil;
import com.gma.camilabp.web.util.StringValues;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.model.DualListModel;

/**
 *
 * @author userdb2
 */
@Named(value = "grupousuarioBean")
@ViewScoped
public class SegrGrupousuarioBean implements Serializable {
    private static final Long serialVersionUID = 1L;
    @Inject
    private UserSession userSession;
    @EJB
    private GrupoUsuariosLocal grupoUsuariosLocal;
    @EJB
    private EntityManagerLocal entity;
    @EJB
    private MenuLocal menuLocal;
   
    private CbpsegrGrupousuario grupousuario;
    private GrupousuarioLazy grupousuarioLazy = new GrupousuarioLazy();
    private List<CbpsegrGrupousuario> segrGrupousuarioList = new ArrayList<>();
    private boolean nuevo = true;
    private String headerDLG = "";
    protected CbpsegrGrupousuario grupoUsuarioSeleccion;
    protected List<CbpsegmOpcion> opcioneAsignadas;
    protected List<CbpsegmOpcion> opcionesSource;
    protected List<CbpsegmOpcion> opcionesTarget;
    protected DualListModel<CbpsegmOpcion> opciones;
    private Map<String, Object> parametros;
    protected List<CbpsegmPerfil> listPerfil;

    @PostConstruct
    public void initView() {
        segrGrupousuarioList = grupoUsuariosLocal.getSegrGrupousuarios();
        this.opciones = new DualListModel<>();
    }

    public void nuevoDlg() {
        grupousuario = new CbpsegrGrupousuario();
        nuevo = true;
        headerDLG = "Nuevo Grupo de Usuarios";
        JsfUtil.update("frmdlgGruposUsuarios");
        JsfUtil.executeJS("PF('dlgGruposUsuarios').show()");
    }

    public void guardarGrupoUsuario() {
        try {
            grupousuario.setSeggppCreausuer(userSession.getCbpsegmUsuario().getSegusuCodigo());
            grupoUsuariosLocal.saveSegrGrupousuario(grupousuario);
            JsfUtil.messageInfo(null, "Se guardo correctamente", "");
            JsfUtil.executeJS("PF('dlgGruposUsuarios').hide()");
            JsfUtil.update("formGruposUsuarios");
        } catch (Exception e) {
            JsfUtil.messageError(null, e.getMessage(), "");
        }
    }

    public void editarDlg(CbpsegrGrupousuario gru) {
        grupousuario = new CbpsegrGrupousuario();
        grupousuario = gru;
        nuevo = false;
        headerDLG = "Editar Grupo de Usuarios";
        JsfUtil.update("frmdlgGruposUsuarios");
        JsfUtil.executeJS("PF('dlgGruposUsuarios').show()");
    }
    
    public void seleccionarGrupo(){
        this.opcioneAsignadas = this.menuLocal.getCbpsegmOpcionListByGrupo(this.grupoUsuarioSeleccion);
        this.listPerfil = this.menuLocal.getCbpsegmPerfilListByGrupo(this.grupoUsuarioSeleccion);
        this.opcionesTarget = new ArrayList<>();
        this.opcionesSource = this.entity.findAll(CbpsegmOpcion.class);
        this.opcionesSource.removeAll(this.opcioneAsignadas);
        this.opciones = new DualListModel<>(this.opcionesSource, this.opcionesTarget);
    }
    
    public void asignarOpcion(){
        try{
            System.out.println(this.opcionesTarget);
            System.out.println(this.opciones.getTarget());
        } catch (Exception e) {
            Logger.getLogger(SegrGrupousuarioBean.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void grabarAsignacion(CbpsegmPerfil perfil){
        try{
            this.entity.merge(perfil);
            JsfUtil.messageInfo(null, StringValues.msgRegistroExitoso, "");
        } catch (Exception e) {
            Logger.getLogger(SegrGrupousuarioBean.class.getName()).log(Level.SEVERE, null, e);
            JsfUtil.messageError(null, StringValues.msgRegistroNoExitoso, "");
        }
    }
    
    public void grabarAsignaciones(){
        try{
            CbpsegmPerfil p;
            for (CbpsegmOpcion opc : this.opciones.getTarget()) {
                p= new CbpsegmPerfil();
                p.setSeggppCodigo(this.grupoUsuarioSeleccion.getSeggppCodigo());
                p.setGenciaCodigo(this.grupoUsuarioSeleccion.getGenciaCodigo());
                p.setSegperCreauser(this.userSession.getCbpsegmUsuario().getSegusuCodigo());
                p.setSegaccCodigo(new CbpsegmAccion(1L));
                p.setSegopcCodigo(opc);
                this.entity.merge(p);
            }
            JsfUtil.messageInfo(null, StringValues.msgRegistroExitoso, "");
        } catch (Exception e) {
            Logger.getLogger(SegrGrupousuarioBean.class.getName()).log(Level.SEVERE, null, e);
            JsfUtil.messageError(null, StringValues.msgRegistroNoExitoso, "");
        }
    }

    public boolean isNuevo() {
        return nuevo;
    }

    public void setNuevo(boolean nuevo) {
        this.nuevo = nuevo;
    }

    public String getHeaderDLG() {
        return headerDLG;
    }

    public void setHeaderDLG(String headerDLG) {
        this.headerDLG = headerDLG;
    }

    public GrupoUsuariosLocal getGrupoUsuariosLocal() {
        return grupoUsuariosLocal;
    }

    public void setGrupoUsuariosLocal(GrupoUsuariosLocal grupoUsuariosLocal) {
        this.grupoUsuariosLocal = grupoUsuariosLocal;
    }

    public CbpsegrGrupousuario getGrupousuario() {
        return grupousuario;
    }

    public void setGrupousuario(CbpsegrGrupousuario grupousuario) {
        this.grupousuario = grupousuario;
    }

    public List<CbpsegrGrupousuario> getSegrGrupousuarioList() {
        return segrGrupousuarioList;
    }

    public void setSegrGrupousuarioList(List<CbpsegrGrupousuario> segrGrupousuarioList) {
        this.segrGrupousuarioList = segrGrupousuarioList;
    }

    public GrupousuarioLazy getGrupousuarioLazy() {
        return grupousuarioLazy;
    }

    public void setGrupousuariolazy(GrupousuarioLazy grupousuarioLazy) {
        this.grupousuarioLazy = grupousuarioLazy;
    }

    public CbpsegrGrupousuario getGrupoUsuarioSeleccion() {
        return grupoUsuarioSeleccion;
    }

    public void setGrupoUsuarioSeleccion(CbpsegrGrupousuario grupoUsuarioSeleccion) {
        this.grupoUsuarioSeleccion = grupoUsuarioSeleccion;
    }

    public List<CbpsegmOpcion> getOpcioneAsignadas() {
        return opcioneAsignadas;
    }

    public void setOpcioneAsignadas(List<CbpsegmOpcion> opcioneAsignadas) {
        this.opcioneAsignadas = opcioneAsignadas;
    }

    public DualListModel<CbpsegmOpcion> getOpciones() {
        return opciones;
    }

    public void setOpciones(DualListModel<CbpsegmOpcion> opciones) {
        this.opciones = opciones;
    }

    public List<CbpsegmPerfil> getListPerfil() {
        return listPerfil;
    }

    public void setListPerfil(List<CbpsegmPerfil> listPerfil) {
        this.listPerfil = listPerfil;
    }

}
