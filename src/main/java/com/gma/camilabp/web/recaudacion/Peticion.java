/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.recaudacion;

import com.gma.camilabo.enums.TipoMovimiento;
import com.gma.camilabp.domain.BanmTiposmovimientos;
import com.gma.camilabp.domain.CbpgenrRecaudacion;
import com.gma.camilabp.domain.CbpsegmTurno;
import com.gma.camilabp.domain.GenmCaja;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.service.PuntoVentaLocal;
import com.gma.camilabp.web.service.RecaudacionLocal;
import com.gma.camilabp.web.session.ServletSession;
import com.gma.camilabp.web.session.UserSession;
import com.gma.camilabp.web.util.JsfUtil;
import com.gma.camilabp.web.util.StringValues;
import com.gma.camilabp.web.util.Utilidades;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

/**
 *
 * @author Acer
 */
@Named(value = "peticion")
@ViewScoped
public class Peticion implements Serializable{
    @EJB
    private EntityManagerLocal entity;
    @EJB
    private RecaudacionLocal recaudacionService;
    @EJB
    private PuntoVentaLocal posService;
    @Inject
    private UserSession userSession;
    @Inject
    private ServletSession datosSession;
    
    protected GenmCaja caja;
    protected CbpgenrRecaudacion recaudacion;
    protected List<BanmTiposmovimientos> tiposMovimiento;
    protected CbpsegmTurno turno;
    protected List<GenmCaja> cajaList;
    protected CbpgenrRecaudacion r;
    
    private Map<String, Object> parametros;

    @PostConstruct
    public void init() {
        try {
            this.recaudacion = new CbpgenrRecaudacion();
            this.caja = this.entity.find(GenmCaja.class, this.userSession.getCaja().getFacscaIdsececaja());
            this.turno = this.entity.find(CbpsegmTurno.class, this.userSession.getTurno().getSegturCodigo());
            this.parametros= new HashMap<>();
            this.parametros.put("bANTMVingresoegreso", 'E');
            this.parametros.put("bANTMVorigenmov", "BAN");
            this.parametros.put("bANTMVtipoentidad", this.caja.getGenpgeCodigo());
            this.tiposMovimiento = this.entity.findAllByParameter(BanmTiposmovimientos.class, parametros, "bANTMVnombre");
            this.recaudacion.setBANTMVcodigo(((BanmTiposmovimientos)Utilidades.getFirstElement(this.tiposMovimiento)).getBanmTiposmovimientosPK().getBANTMVcodigo());
            this.recaudacion.setGenrrecTipotransaccion(TipoMovimiento.TRANSFERENCIA.getCode());
            
            this.parametros= new HashMap<>();
            this.parametros.put("gENBODcodigo", this.caja.getGENBODcodigo());   
            this.parametros.put("gENCAJestado", "A");
            this.cajaList= this.entity.findAllByParameter(GenmCaja.class, this.parametros, null);
            this.cajaList.remove(this.caja);            
            this.recaudacion.setCajaTransfer((GenmCaja)Utilidades.getFirstElement(this.cajaList));
        } catch (Exception e) {
            Logger.getLogger(Peticion.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public Boolean validarRegistro(CbpgenrRecaudacion r){
        Boolean valido= Boolean.FALSE;
        try{
            if(r.getGenrrecValor()!=null && r.getGenrrecValor().compareTo(BigDecimal.ZERO)>0 && r.getGenrrecDestinatario()!=null && r.getGenrrecReferencia()!=null && r.getGenrrecDescripcion()!=null){
                valido = Boolean.TRUE;
            }
        } catch (Exception e) {
            valido = Boolean.FALSE;
            Logger.getLogger(Recaudacion.class.getName()).log(Level.SEVERE, null, e);
        }
        return valido;
    }
    
    public void grabarPeticion(){
        try{
            if(this.validarRegistro(this.recaudacion)){
                this.r = this.recaudacionService.transferenciaValor(this.recaudacion, this.caja, this.turno);
                if(this.r!=null){
                    JsfUtil.messageInfo(null, StringValues.msgRegistroExitoso, "");
                    JsfUtil.update("frmdlgInformacion");
                    JsfUtil.executeJS("PF('dlgInformacion').show();");
                }else{
                    JsfUtil.messageInfo(null, StringValues.msgRegistroNoExitoso, "");
                }
            }else{
                JsfUtil.messageWarning(null, "VERIFICAR DATOS", "");
            }
        } catch (Exception e) {
            Logger.getLogger(Recaudacion.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void aceptarTransaccion(){
        try{                        
            if (datosSession != null) {
                datosSession.borrarDatos();
                datosSession.setNombreReporte("recaudacion/transaccion");
                datosSession.agregarParametro("ID", this.r.getGenrrecId());
                datosSession.agregarParametro("TURNO", this.r.getSegturNumero());
                datosSession.agregarParametro("CAJA", this.r.getCaja());
                datosSession.agregarParametro("TIPO_TRANSACCION", Utilidades.stringEnum(this.r.getGenrrecTipotransaccion()));
                datosSession.agregarParametro("USUARIO", this.userSession.getCbpsegmUsuario().getSegusuLogin()+"-"+this.userSession.getCbpsegmUsuario().getSegusuAbreviado());
                JsfUtil.redirectNewTab("/CamilaPOS/Documento");
            }
            this.r=null;
            JsfUtil.executeJS("PF('dlgInformacion').hide();");
            this.init();
            JsfUtil.update("mainForm");
        } catch (Exception e) {
            Logger.getLogger(Recaudacion.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public GenmCaja getCaja() {
        return caja;
    }

    public void setCaja(GenmCaja caja) {
        this.caja = caja;
    }

    public CbpgenrRecaudacion getRecaudacion() {
        return recaudacion;
    }

    public void setRecaudacion(CbpgenrRecaudacion recaudacion) {
        this.recaudacion = recaudacion;
    }

    public List<BanmTiposmovimientos> getTiposMovimiento() {
        return tiposMovimiento;
    }

    public void setTiposMovimiento(List<BanmTiposmovimientos> tiposMovimiento) {
        this.tiposMovimiento = tiposMovimiento;
    }

    public CbpgenrRecaudacion getR() {
        return r;
    }

    public void setR(CbpgenrRecaudacion r) {
        this.r = r;
    }

    public List<GenmCaja> getCajaList() {
        return cajaList;
    }

    public void setCajaList(List<GenmCaja> cajaList) {
        this.cajaList = cajaList;
    }
    
}
