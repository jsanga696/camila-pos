/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.recaudacion;

import com.gma.camilabp.domain.CbpgenrRecaudacion;
import com.gma.camilabp.web.lazy.CbpgenrRecaudacionLazy;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.session.ServletSession;
import com.gma.camilabp.web.session.UserSession;
import com.gma.camilabp.web.util.JsfUtil;
import com.gma.camilabp.web.util.Utilidades;
import java.io.Serializable;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

/**
 *
 * @author Acer
 */
@Named(value = "movimientos")
@ViewScoped
public class Movimientos implements Serializable{
    @EJB
    private EntityManagerLocal entity;
    @Inject
    private ServletSession datosSession;
    @Inject
    private UserSession userSession;
    
    protected CbpgenrRecaudacionLazy recaudacionIngresos;
    protected CbpgenrRecaudacionLazy recaudacionEgresos;
    protected CbpgenrRecaudacionLazy recaudacionTransferencia;
    protected CbpgenrRecaudacion movimiento;
    
    private Map<String, Object> parametros;
    
    @PostConstruct
    public void init() {
        try {
            this.recaudacionIngresos = new CbpgenrRecaudacionLazy(1);
            this.recaudacionEgresos = new CbpgenrRecaudacionLazy(2);
            this.recaudacionTransferencia = new CbpgenrRecaudacionLazy(3);
        } catch (Exception e) {
            Logger.getLogger(Movimientos.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void imprimirMovimiento(CbpgenrRecaudacion m){
        try{
            if (datosSession != null) {
                datosSession.borrarDatos();
                datosSession.setNombreReporte("recaudacion/transaccion");
                datosSession.agregarParametro("ID", m.getGenrrecId());
                datosSession.agregarParametro("TURNO", m.getSegturNumero());
                datosSession.agregarParametro("CAJA", m.getCaja());
                datosSession.agregarParametro("TIPO_TRANSACCION", Utilidades.stringEnum(m.getGenrrecTipotransaccion()));
                datosSession.agregarParametro("USUARIO", this.userSession.getCbpsegmUsuario().getSegusuLogin()+"-"+this.userSession.getCbpsegmUsuario().getSegusuAbreviado());
                JsfUtil.redirectNewTab("/CamilaPOS/Documento");
            }
        } catch (Exception e) {
            Logger.getLogger(Movimientos.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public CbpgenrRecaudacionLazy getRecaudacionIngresos() {
        return recaudacionIngresos;
    }

    public void setRecaudacionIngresos(CbpgenrRecaudacionLazy recaudacionIngresos) {
        this.recaudacionIngresos = recaudacionIngresos;
    }

    public CbpgenrRecaudacionLazy getRecaudacionEgresos() {
        return recaudacionEgresos;
    }

    public void setRecaudacionEgresos(CbpgenrRecaudacionLazy recaudacionEgresos) {
        this.recaudacionEgresos = recaudacionEgresos;
    }

    public CbpgenrRecaudacionLazy getRecaudacionTransferencia() {
        return recaudacionTransferencia;
    }

    public void setRecaudacionTransferencia(CbpgenrRecaudacionLazy recaudacionTransferencia) {
        this.recaudacionTransferencia = recaudacionTransferencia;
    }

    public CbpgenrRecaudacion getMovimiento() {
        return movimiento;
    }

    public void setMovimiento(CbpgenrRecaudacion movimiento) {
        this.movimiento = movimiento;
    }

    
}
