/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.lazy;

import com.gma.camilabp.domain.CbpactEquipo;
import com.gma.camilabp.domain.CxcmCliente;
import com.gma.camilabp.domain.MEmpleados;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author userdb5
 */
public class MEmpleadosLazy extends BaseLazyDataModel<MEmpleados> {
    
    private String codOficina;
    private String codCompania;
    
    public MEmpleadosLazy(String codigoOficina, String genciaCodigo) {
        super(MEmpleados.class);
        this.codOficina = codigoOficina;
        this.codCompania = genciaCodigo;
    }
    
    @Override
    public void criteriaFilterSetup(Criteria crit, Map<String, Object> filters) {
        
        if(this.codCompania!=null)
            crit.add(Restrictions.eq("mEmpleadosPK.gENCIAcodigo", this.codCompania));
        if(this.codOficina!=null)
            crit.add(Restrictions.eq("mEmpleadosPK.gENOFIcodigo", this.codOficina));
        
        if (filters.containsKey("NOMEMPcedula")) {
            crit.add(Restrictions.eq("nOMEMPcedula", filters.get("NOMEMPcedula").toString().trim()));
        }
        
        if (filters.containsKey("NOMEMPapellidos")) {
            crit.add(Restrictions.ilike("nOMEMPapellidos", "%"+filters.get("NOMEMPapellidos").toString().replace(" ", "%")+"%"));
        }
    }
    
}
