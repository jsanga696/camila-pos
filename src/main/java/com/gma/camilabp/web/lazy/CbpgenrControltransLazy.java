/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.lazy;

import com.gma.camilabp.domain.CbpgenrControltrans;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Acer
 */
public class CbpgenrControltransLazy extends BaseLazyDataModel<CbpgenrControltrans>{

    public CbpgenrControltransLazy() {
        super(CbpgenrControltrans.class, "genrconId", "ASC");
    }
    
    @Override
    public void criteriaFilterSetup(Criteria crit, Map<String, Object> filters) throws Exception {
        if (filters.containsKey("genrconId"))
            crit.add(Restrictions.eq("genrconId", new Long(filters.get("genrconId").toString().trim())));
        if (filters.containsKey("genbodCodigo"))
            crit.createCriteria("genbodCodigo").add(Restrictions.eq("genbodCodigo", new Long(filters.get("genbodCodigo").toString().trim())));
        if (filters.containsKey("genbodSecuencia"))
            crit.createCriteria("genbodCodigo").add(Restrictions.eq("genbodSecuencia", filters.get("genbodSecuencia").toString().trim()));
        if (filters.containsKey("genbodNombre"))
            crit.createCriteria("genbodCodigo").add(Restrictions.ilike("genbodNombre", "%"+filters.get("genbodNombre").toString().trim()+"%"));
        if (filters.containsKey("segusuCodigoSup"))
            crit.createCriteria("segusuCodigoSup").add(Restrictions.eq("segusuCodigo", new Long(filters.get("segusuCodigoSup").toString().trim())));
        if (filters.containsKey("segusuLogin"))
            crit.createCriteria("segusuCodigoSup").add(Restrictions.eq("segusuLogin", filters.get("segusuLogin").toString().trim()));
        if (filters.containsKey("segusuAbreviado"))
            crit.createCriteria("segusuCodigoSup").add(Restrictions.ilike("segusuAbreviado", "%"+filters.get("segusuAbreviado").toString().trim()+"%"));
    }
}
