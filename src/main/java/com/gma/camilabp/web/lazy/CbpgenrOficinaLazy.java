/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.lazy;

import com.gma.camilabp.domain.CbpgenrOficina;
import com.gma.camilabp.web.service.EntityManagerLocal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author userdb2
 */
public class CbpgenrOficinaLazy <Ent extends CbpgenrOficina> extends LazyDataModel<Ent> {
    
    @EJB
    private EntityManagerLocal entity;
    
    private Class<CbpgenrOficina> entC = CbpgenrOficina.class;
    private HashMap<String, String> dicc;
    private String sortOrder_default = "ASC";
    private String sortField_default = "genofiCodigo"; 
    private int rowCount = 0;
    private boolean isActivo=false;

    private void criteriaMainSetup(Criteria crit) {
        if (isActivo) {
            crit.add(Restrictions.eq("genofiEstado", isActivo) );
        }
    }
    
    private void criteriaFilterSetup(Criteria crit, Map<String, Object> filters) {
        if (filters.containsKey("genofiSecuencia")) {
            crit.add(Restrictions.ilike("genofiSecuencia", "%" + filters.get("genofiSecuencia").toString().trim() + "%"));
        }
        if (filters.containsKey("genofiNombre")) {
            crit.add(Restrictions.ilike("genofiNombre", "%" + filters.get("genofiNombre").toString().trim() + "%"));
        }
        if (filters.containsKey("genofinomComercial")) {
            crit.add(Restrictions.ilike("genofinomComercial", "%" + filters.get("genofinomComercial").toString().trim() + "%"));
        }
    }

    public CbpgenrOficinaLazy() {
        dicc = new HashMap<String, String>();
    }
    public CbpgenrOficinaLazy(boolean activo) {
        dicc = new HashMap<String, String>();
        this.isActivo=activo;
    }
    
    @Override
    public List<Ent> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        
        List<Ent> theList = null;
        //HibernateUtil.getSessionFactory().openSession();
        Session sess1 = entity.getSession();
        Transaction trans = null;
        List<Ent> resultList = null;
        
        try {
            // I use two queries: one to count filtered result and the second to get paged result
            
            Criteria countCrit = sess1.createCriteria(entC,"ent1");
            this.criteriaFilterSetup(countCrit, filters);
            countCrit.setProjection( Projections.projectionList()
                .add( Projections.rowCount() ) );
            Criteria dataCrit = sess1.createCriteria(entC,"ent1");
            this.criteriaFilterSetup(dataCrit, filters);
            this.criteriaSortSetup(dataCrit, sortField, sortOrder);
            this.criteriaPageSetup(dataCrit, first, pageSize);

            rowCount=0;
            rowCount = ((Long) countCrit.uniqueResult()).intValue();
            this.setRowCount(rowCount);
            
            resultList = dataCrit.list();
            
        } catch (HibernateException e) {
            if(trans!=null) trans.rollback();
			System.out.println("*** Hibernate Error: " + e.getMessage() );
			e.printStackTrace();
        } catch (Exception e) {
			if(trans!=null) trans.rollback();
			System.out.println("*** Error: " + e.getMessage() );
			e.printStackTrace();
        } finally {
            sess1.close();
        }
        return resultList;
    }
    
    private void criteriaPageSetup(Criteria crit, int first, int pageSize) {
        crit.setFirstResult(first);
        crit.setMaxResults(pageSize);
    }
    
    private void criteriaSortSetup(Criteria crit, String field, SortOrder order) {

        if (field == null) {
            crit.addOrder((sortOrder_default.equalsIgnoreCase("ASC")) ? Order.asc(sortField_default) : Order.desc(sortField_default));
        } else {
            if (order.equals(SortOrder.ASCENDING)) {
                crit.addOrder(Order.asc(field));
            } else {
                crit.addOrder(Order.desc(field));
            }
        }
    }
}
