/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.lazy;

import com.gma.camilabp.domain.CbpsegmTurno;
import java.util.Date;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author userdb6
 */
public class CbpsegmTurnoLazy extends BaseLazyDataModel<CbpsegmTurno>{
    private Date fecha;
    private String genciaCodigo;
    private String genofiCodigo;
    private int tipo;

    public CbpsegmTurnoLazy() {
        super(CbpsegmTurno.class, "segturCodigo", "DESC");
    }

    public CbpsegmTurnoLazy(Date fecha, String genciaCodigo, String genofiCodigo, int tipo) {
        super(CbpsegmTurno.class, "segturCodigo", "DESC");
        this.fecha = fecha;
        this.genciaCodigo = genciaCodigo;
        this.genofiCodigo = genofiCodigo;
        this.tipo=tipo;
    }
    
    @Override
    public void criteriaFilterSetup(Criteria crit, Map<String, Object> filters) throws Exception {
        if(this.genciaCodigo!=null)
            crit.add(Restrictions.eq("segusuCodigo.genciaCodigo.genciaSecuencia", genciaCodigo));
        if(this.genofiCodigo!=null)
            crit.add(Restrictions.eq("segusuCodigo", genofiCodigo));
        if(this.fecha!=null){
            if(tipo==1)
                crit.add(Restrictions.ge("segturApertura", fecha));
            else
                crit.add(Restrictions.le("segturApertura", fecha));
        }
        if (filters.containsKey("segturNumero"))
            crit.add(Restrictions.eq("segturNumero", new Long(filters.get("segturNumero").toString())));
    }
}
