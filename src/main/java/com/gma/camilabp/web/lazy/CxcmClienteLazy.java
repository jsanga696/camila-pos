/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.lazy;

import com.gma.camilabp.domain.CxcmCliente;
import java.util.List;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author userdb6
 */
public class CxcmClienteLazy extends BaseLazyDataModel<CxcmCliente>{
    
    private Long codigoOficina;
    private Long genciaCodigo;
    private List<Long> oficinaCodigos;
    private String codOficina;
    private String codCompania;
    private String codigo;
    private String identificacion;
    private String razonSocial;

    public CxcmClienteLazy() {
        super(CxcmCliente.class, "cxcmClientePK.cXCCLIcodigo", "DESC");
    }
    
    public CxcmClienteLazy(Long codigoOficina) {
        super(CxcmCliente.class, "cxcmClientePK.cXCCLIcodigo", "DESC");
        this.codigoOficina=codigoOficina;
    }     
    
    public CxcmClienteLazy(Long codigoOficina, Long genciaCodigo) {
        super(CxcmCliente.class, "cxcmClientePK.cXCCLIcodigo", "DESC");
        this.codigoOficina=codigoOficina;
        this.genciaCodigo = genciaCodigo;
    }
    
    public CxcmClienteLazy(String codigoOficina, String genciaCodigo) {
        super(CxcmCliente.class, "cxcmClientePK.cXCCLIcodigo", "DESC");
        this.codOficina = codigoOficina;
        this.codCompania = genciaCodigo;
    }

    public CxcmClienteLazy(String codCompania, String codOficina, String codigo, String identificacion, String razonSocial) {
        super(CxcmCliente.class, "cxcmClientePK.cXCCLIcodigo", "DESC");
        this.codCompania = codCompania;
        this.codOficina = codOficina;
        this.codigo= codigo;
        this.identificacion= identificacion;
        this.razonSocial= razonSocial;
    }    
    
    @Override
    public void criteriaFilterSetup(Criteria crit, Map<String, Object> filters) throws Exception {
        
        crit.add(Restrictions.eq("cXCCLIestactivo", "A") );
        if(this.codCompania!=null)
            crit.add(Restrictions.eq("cxcmClientePK.gENCIAcodigo", this.codCompania));
        if(this.codOficina!=null)
            crit.add(Restrictions.eq("cxcmClientePK.gENOFIcodigo", this.codOficina));
        if(this.codigo!=null)
            crit.add(Restrictions.eq("cxcmClientePK.cXCCLIcodigo", this.codigo));
        if(this.identificacion!=null)
            crit.add(Restrictions.eq("cXCCLIrucci", this.identificacion));
        if(this.razonSocial!=null)
            crit.add(Restrictions.ilike("cXCCLIrazonsocia", "%"+this.razonSocial+"%"));
        
        if(this.genciaCodigo!=null){
            crit.add(Restrictions.eq("cxcmClientePK.gENCIAcodigo", genciaCodigo));
        }        
        if(this.codigoOficina!=null){
            crit.add(Restrictions.eq("genofiCodigo", codigoOficina));
        }        
        if(this.oficinaCodigos!=null && !this.oficinaCodigos.isEmpty()){
            crit.add(Restrictions.in("genofiCodigo", this.oficinaCodigos));
        }        
        if (filters.containsKey("CXCCLIcodigo")) {
            crit.add(Restrictions.eq("cxcmClientePK.cXCCLIcodigo", filters.get("CXCCLIcodigo").toString().trim()));
        }
        if (filters.containsKey("CXCCLIrucci")) {
            crit.add(Restrictions.eq("cXCCLIrucci", filters.get("CXCCLIrucci").toString().trim()));
        }
        if (filters.containsKey("CXCCLIrazonsocia")) {
            crit.add(Restrictions.ilike("cXCCLIrazonsocia", "%" + filters.get("CXCCLIrazonsocia").toString().trim() + "%"));
        }
        if (filters.containsKey("CXCCLIdireccion1")) {
            crit.add(Restrictions.ilike("cXCCLIdireccion1", "%" + filters.get("CXCCLIdireccion1").toString().trim() + "%"));
        }
    }
    
}
