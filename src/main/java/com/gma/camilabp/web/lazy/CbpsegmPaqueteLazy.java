/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.lazy;

import com.gma.camilabp.domain.CbpsegmPaquete;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author userdb6
 */
public class CbpsegmPaqueteLazy extends BaseLazyDataModel<CbpsegmPaquete>{

    public CbpsegmPaqueteLazy() {
        super(CbpsegmPaquete.class,"segpaqCodigo","ASC");
    }
    
    @Override
    public void criteriaFilterSetup(Criteria crit, Map<String, Object> filters) throws Exception {
        if (filters.containsKey("segpaqCodigo")) {
            crit.add(Restrictions.eq("segpaqCodigo", new Long(filters.get("segpaqCodigo").toString().trim())));
        }
        if (filters.containsKey("segpaqDescripcio")) {
            crit.add(Restrictions.ilike("segpaqDescripcio", "%"+filters.get("segpaqDescripcio").toString().trim()+"%"));
        }
    }
}
