/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.lazy;

import com.gma.camilabp.domain.CbpactEquipo;
import com.gma.camilabp.domain.CbpgenrBodega;
import com.gma.camilabp.domain.InvrCablistaprecio;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author userdb6
 */
public class InvrCablistaprecioLazy extends BaseLazyDataModel<InvrCablistaprecio> {

    private String genofiCodigo = null;
    private String gENCIAcodigo = null;

    public InvrCablistaprecioLazy() {
        super(InvrCablistaprecio.class);
    }
    
    /*public InvrCablistaprecioLazy(List<> ids) {
        super(InvrCablistaprecio.class);
    }*/

    public InvrCablistaprecioLazy(String genofiCodigo, String gENCIAcodigo) {
        super(InvrCablistaprecio.class);
        this.genofiCodigo = genofiCodigo;
        this.gENCIAcodigo = gENCIAcodigo;
    }

    @Override
    public void criteriaFilterSetup(Criteria crit, Map<String, Object> filters) {

        if (this.genofiCodigo != null) {
            crit.add(Restrictions.eq("genofiCodigo", this.genofiCodigo));
        }
        
        if (this.gENCIAcodigo != null) {
            crit.add(Restrictions.eq("gENCIAcodigo", this.gENCIAcodigo));
        }
    }
}
