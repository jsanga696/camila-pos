/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.lazy;

import com.gma.camilabp.domain.CbpsegmOpcion;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author userdb6
 */
public class CbpsegmOpcionLazy extends BaseLazyDataModel<CbpsegmOpcion>{

    public CbpsegmOpcionLazy() {
        super(CbpsegmOpcion.class,"segopcCodigo","ASC");
    }
    
    @Override
    public void criteriaFilterSetup(Criteria crit, Map<String, Object> filters) throws Exception {
        if (filters.containsKey("segopcCodigo")) 
            crit.add(Restrictions.eq("segopcCodigo", new Long(filters.get("segopcCodigo").toString().trim())));
        if (filters.containsKey("segopcSecuencia")) 
            crit.add(Restrictions.ilike("segopcSecuencia", "%"+filters.get("segopcSecuencia").toString().trim()+"%"));
        if (filters.containsKey("segopcDescripcio")) 
            crit.add(Restrictions.ilike("segopcDescripcio", "%"+filters.get("segopcDescripcio").toString().trim()+"%"));
        if (filters.containsKey("segpaqCodigo")) 
            crit.add(Restrictions.ilike("segpaqCodigo.segpaqDescripcio", "%"+filters.get("segpaqCodigo").toString().trim()+"%"));
        if (filters.containsKey("segmodCodigo")) 
            crit.add(Restrictions.ilike("segmodCodigo.segmodDescripcio", "%"+filters.get("segmodCodigo").toString().trim()+"%"));
    }
}
