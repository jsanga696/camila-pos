/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.lazy;

import com.gma.camilabp.domain.CbpposCabfacttemporal;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author userdb6
 */
public class CbpposCabfacttemporalLazy extends BaseLazyDataModel<CbpposCabfacttemporal>{
    
    private String estado;
    private Long turno;

    public CbpposCabfacttemporalLazy() {
        super(CbpposCabfacttemporal.class, "cbpfaccCodigo", "DESC");
    }

    public CbpposCabfacttemporalLazy(String estado, Long turno) {
        super(CbpposCabfacttemporal.class, "cbpfaccCodigo", "DESC");
        this.estado = estado;
        this.turno = turno;
    }
    
    @Override
    public void criteriaFilterSetup(Criteria crit, Map<String, Object> filters) throws Exception {
        if(this.turno!=null)
            crit.add(Restrictions.eq("segturNumero", turno));
        if(this.estado!=null)
            crit.add(Restrictions.eq("cbpfaccEstado", estado));
    }
}
