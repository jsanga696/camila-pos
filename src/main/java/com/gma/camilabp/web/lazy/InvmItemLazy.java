/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.lazy;

import com.gma.camilabp.domain.InvmItem;
import java.util.Collection;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author userdb6
 */
public class InvmItemLazy extends BaseLazyDataModel<InvmItem>{
    //INVITM_esttipo=T,INVITM_estactivo=A,INVITM_estdispvta=S
    private Character itemEstTipo;
    private Character itemEstActivo;
    private Character itemEstVenta;
    private String codigo;
    private String nombre;
    private Collection listCodigo;
    
    public InvmItemLazy() {
        super(InvmItem.class, "invmItemPK.iNVITMcodigo", "DESC");
    }

    public InvmItemLazy(Character itemEstTipo, Character itemEstActivo, Character itemEstVenta,String codigo, String nombre) {
        super(InvmItem.class, "invmItemPK.iNVITMcodigo", "DESC");
        this.itemEstTipo = itemEstTipo;
        this.itemEstActivo = itemEstActivo;
        this.itemEstVenta = itemEstVenta;
        this.codigo = codigo;
        this.nombre = nombre;
    }        

    public InvmItemLazy(Collection listCodigo) {
        super(InvmItem.class, "invmItemPK.iNVITMcodigo", "DESC");
        this.listCodigo = listCodigo;
    }
    
    @Override
    public void criteriaFilterSetup(Criteria crit, Map<String, Object> filters) throws Exception {
        //crit.add(Restrictions.ne("iNVITMestaPromocion", "S"));
        //crit.add(Restrictions.isNull("iNVITMestaPromocion")).add(Restrictions.or(Restrictions.ne("iNVITMestaPromocion", "S")));
        crit.add(Restrictions.or(Restrictions.ne("iNVITMestaPromocion", "S"),Restrictions.isNull("iNVITMestaPromocion")));
        if(this.itemEstTipo!=null){
            crit.add(Restrictions.eq("iNVITMesttipo", this.itemEstTipo));
        }
        if(this.itemEstActivo!=null){
            crit.add(Restrictions.eq("iNVITMestactivo", this.itemEstActivo));
        }
        if(this.itemEstVenta!=null){
            crit.add(Restrictions.eq("iNVITMestdispvta", this.itemEstVenta));
        }
        if(this.nombre!=null){
            crit.add(Restrictions.ilike("iNVITMnombre", "%"+this.nombre+"%"));
        }
        if(this.codigo!=null){
            crit.add(Restrictions.ilike("invmItemPK.iNVITMcodigo", "%"+this.codigo+"%"));
        }
        if (listCodigo != null && !listCodigo.isEmpty()) {
            crit.add(Restrictions.in("invmItemPK.iNVITMcodigo", listCodigo));                   
        }
        if (filters.containsKey("INVITMcodigo")) {
            crit.add(Restrictions.ilike("invmItemPK.iNVITMcodigo", "%"+filters.get("INVITMcodigo").toString().trim()+"%"));
        }
        if (filters.containsKey("INVITMcodigbarra")) {
            crit.add(Restrictions.ilike("iNVITMcodigbarra", "%"+filters.get("INVITMcodigbarra").toString().trim()+"%"));
        }
        if (filters.containsKey("INVITMnombre")) {
            crit.add(Restrictions.ilike("iNVITMnombre", "%"+filters.get("INVITMnombre").toString().trim()+"%"));
        }        
    }
}
