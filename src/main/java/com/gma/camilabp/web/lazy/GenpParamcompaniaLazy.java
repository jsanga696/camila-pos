/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.lazy;

import com.gma.camilabp.domain.GenpParamcompania;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author userdb6
 */
public class GenpParamcompaniaLazy extends BaseLazyDataModel<GenpParamcompania>{

    public GenpParamcompaniaLazy() {
        super(GenpParamcompania.class, "genpParamcompaniaPK.gENPCOetiqueta","ASC");
    }
    
    @Override
    public void criteriaFilterSetup(Criteria crit, Map<String, Object> filters) throws Exception {
        if (filters.containsKey("gENAPLcodigo")) {
            crit.add(Restrictions.ilike("genpParamcompaniaPK.gENAPLcodigo", "%"+filters.get("gENAPLcodigo").toString().trim()+"%"));
        }
        if (filters.containsKey("gENPCOetiqueta")) {
            crit.add(Restrictions.ilike("genpParamcompaniaPK.gENPCOetiqueta", "%"+filters.get("gENPCOetiqueta").toString().trim()+"%"));
        }
    }
}
