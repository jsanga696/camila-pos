/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.lazy;

import com.gma.camilabp.domain.CbpgenrPoliticabase;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Acer
 */
public class CbpgenrPoliticaLazy extends BaseLazyDataModel<CbpgenrPoliticabase>{

    public CbpgenrPoliticaLazy() {
        super(CbpgenrPoliticabase.class, "genpolCodigo", "DESC");
    }
    
    @Override
    public void criteriaFilterSetup(Criteria crit, Map<String, Object> filters) throws Exception {
        if (filters.containsKey("genpolCodigo"))
            crit.add(Restrictions.eq("genpolCodigo", new Long(filters.get("genpolCodigo").toString())));
        if (filters.containsKey("genciaNombre"))
            crit.createCriteria("genciaCodigo").add(Restrictions.ilike("genciaNombre", "%"+filters.get("genciaNombre").toString().trim()+"%"));
        if (filters.containsKey("genofiNombre"))
            crit.createCriteria("genofiCodigo").add(Restrictions.ilike("genofiNombre", "%"+filters.get("genofiNombre").toString().trim()+"%"));
        if (filters.containsKey("genpolNombre"))
            crit.add(Restrictions.ilike("genpolNombre", "%"+filters.get("genpolNombre").toString().trim()+"%"));
        if (filters.containsKey("genpolCantidad"))
            crit.add(Restrictions.eq("genpolCantidad", new Long(filters.get("genpolCantidad").toString())));
    }
}
