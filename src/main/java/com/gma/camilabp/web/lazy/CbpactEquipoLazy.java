/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.lazy;

import com.gma.camilabp.domain.CbpactEquipo;
import com.gma.camilabp.web.service.EntityManagerLocal;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author userdb5
 */
public class CbpactEquipoLazy extends BaseLazyDataModel<CbpactEquipo> {

    @EJB
    private EntityManagerLocal entity;
    
    private List<Integer> estados = null;
    private Long idOficina = null;

    public CbpactEquipoLazy() {
        super(CbpactEquipo.class);
    }

    public CbpactEquipoLazy(Long idOficina) {
        super(CbpactEquipo.class);
        this.idOficina = idOficina;
    }

    public CbpactEquipoLazy(List<Integer> estados) {
        super(CbpactEquipo.class);
        this.estados = estados;
    }

    @Override
    public void criteriaFilterSetup(Criteria crit, Map<String, Object> filters) {
        try {
            Criteria modelo = crit.createCriteria("modelEquipo");
            Criteria oficina = crit.createCriteria("genofiCodigo");

            if (this.estados != null) {
                crit.add(Restrictions.in("estado", this.estados));
            }

            if (this.idOficina != null) {
                oficina.add(Restrictions.eq("genofiCodigo", this.idOficina));
            }

            if (filters.containsKey("capacidadLitros")) {
                crit.add(Restrictions.ilike("capacidadLitros", filters.get("capacidadLitros").toString().trim()));
            }

            if (filters.containsKey("proveedorText")) {
                Session session = entity.getSession();

                Transaction tx = session.beginTransaction();
                SQLQuery query = session.createSQLQuery("SELECT CXPPRO_codigo FROM CXPM_PROVEEDOR WHERE CXPPRO_razonsocia LIKE ?");
                query.setParameter(0, "%" + filters.get("proveedorText").toString().replace(" ", "%") + "%");
                List<String> l = query.list();
                tx.commit();
                if (l != null && !l.isEmpty()) {
                    crit.add(Restrictions.in("cxpmProveedor", l));
                }
            }

            if (filters.containsKey("anioFabricacion")) {
                crit.add(Restrictions.ilike("anioFabricacion", filters.get("anioFabricacion").toString().trim()));
            }

            if (filters.containsKey("placa")) {
                crit.add(Restrictions.ilike("placa", "%" + filters.get("placa").toString().replace(" ", "%") + "%"));
            }

            if (filters.containsKey("estado")) {
                crit.add(Restrictions.eq("estado", Integer.parseInt(filters.get("estado") + "")));
            }

            if (filters.containsKey("modelEquipo.modelo")) {
                modelo.add(Restrictions.ilike("modelo", "%" + filters.get("modelEquipo.modelo").toString().replace(" ", "%") + "%"));
            }

            if (filters.containsKey("modelEquipo.marca")) {
                modelo.add(Restrictions.ilike("marca", "%" + filters.get("modelEquipo.marca").toString().replace(" ", "%") + "%"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
