/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.lazy;

import com.gma.camilabp.domain.CbpsegrGrupousuario;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author userdb2
 */
public class GrupousuarioLazy extends BaseLazyDataModel<CbpsegrGrupousuario> {

    public GrupousuarioLazy() {
        super(CbpsegrGrupousuario.class,"seggppCodigo","ASC");
    }
    
    @Override
    public void criteriaFilterSetup(Criteria crit, Map<String, Object> filters) throws Exception {
        if (filters.containsKey("seggppCodigo"))
            crit.add(Restrictions.eq("seggppCodigo", new Long(filters.get("seggppCodigo").toString())));
        if (filters.containsKey("seggppDescripcio"))
            crit.add(Restrictions.ilike("seggppDescripcio", "%" + filters.get("seggppDescripcio").toString().trim() + "%"));
    }
        
}
