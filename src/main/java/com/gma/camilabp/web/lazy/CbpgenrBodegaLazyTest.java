/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.lazy;

import com.gma.camilabp.domain.CbpgenrBodega;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author userdb6
 */
public class CbpgenrBodegaLazyTest extends BaseLazyDataModel<CbpgenrBodega>{
    
    private Long codigoOficina;
    private Long genciaCodigo;
    private Criteria tipoBodega;

    public CbpgenrBodegaLazyTest() {
        super(CbpgenrBodega.class, "genbodCodigo", "DESC");
    }
    
    public CbpgenrBodegaLazyTest(Long codigoOficina, Long genciaCodigo) {
        super(CbpgenrBodega.class, "genbodCodigo", "DESC");
        this.codigoOficina=codigoOficina;
        this.genciaCodigo = genciaCodigo;
    }
    
    @Override
    public void criteriaFilterSetup(Criteria crit, Map<String, Object> filters) throws Exception {
        //tipoBodega= crit.createCriteria("gentboCodigo");
        if(this.genciaCodigo!=null){
            crit.add(Restrictions.eq("genciaCodigo", genciaCodigo));
        }        
        if(this.codigoOficina!=null){
            crit.add(Restrictions.eq("genofiCodigo.genofiCodigo", codigoOficina));
        }
     
        if (filters.containsKey("genbodCodigo")) {
            crit.add(Restrictions.eq("genbodCodigo", new Long(filters.get("genbodCodigo").toString().trim())));
        }
        if (filters.containsKey("genbodSecuencia")) {
            crit.add(Restrictions.eq("genbodSecuencia", filters.get("genbodSecuencia").toString().trim()));
        }
        if (filters.containsKey("genbodNombre")) {
            crit.add(Restrictions.ilike("genbodNombre", "%" + filters.get("genbodNombre").toString().trim() + "%"));
        }
        if (filters.containsKey("genbodDireccion")) {
            crit.add(Restrictions.ilike("genbodDireccion", "%" + filters.get("genbodDireccion").toString().trim() + "%"));
        }
        /*if (filters.containsKey("gentboCodigo.gentboDescripcio")) {
            tipoBodega.add(Restrictions.ilike("gentboDescripcio", "%" + filters.get("gentboCodigo.gentboDescripcio").toString().trim() + "%"));
        }*/
    }
}
