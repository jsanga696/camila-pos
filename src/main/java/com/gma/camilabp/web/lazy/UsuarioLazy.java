/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.lazy;

import com.gma.camilabp.domain.CbpsegmUsuario;
import com.gma.camilabp.web.service.EntityManagerLocal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author userdb2
 */
public class UsuarioLazy <Ent extends CbpsegmUsuario> extends LazyDataModel<Ent> {
    
    @EJB
    private EntityManagerLocal entity;
    
    private Class<CbpsegmUsuario> entC = CbpsegmUsuario.class;
    private HashMap<String, String> dicc;
    private String sortOrder_default = "ASC";
    private String sortField_default = "segusuCodigo"; 
    private int rowCount = 0;
    private boolean isActivo=false;

    private void criteriaMainSetup(Criteria crit) {
        if (isActivo) {
            crit.add(Restrictions.eq("segusuEstado", isActivo) );
        }
    }
    
    private void criteriaFilterSetup(Criteria crit, Map<String, Object> filters) {
        if (filters.containsKey("segusuCodigo")) {
            crit.add(Restrictions.ilike("segusuCodigo", "%" + filters.get("segusuCodigo").toString().trim() + "%"));
        }
        if (filters.containsKey("segusuLogin")) {
            crit.add(Restrictions.ilike("segusuLogin", "%" + filters.get("segusuLogin").toString().trim() + "%"));
        }
        if (filters.containsKey("segusuNombre")) {
            crit.add(Restrictions.ilike("segusuNombre", "%" + filters.get("segusuNombre").toString().trim() + "%"));
        }
        if (filters.containsKey("segusuApellido")) {
            crit.add(Restrictions.ilike("segusuApellido", "%" + filters.get("segusuApellido").toString().trim() + "%"));
        }
        if (filters.containsKey("segusuAbreviado")) {
            crit.add(Restrictions.ilike("segusuAbreviado", "%" + filters.get("segusuAbreviado").toString().trim() + "%"));
        }
        if (filters.containsKey("segusuEstado")) {
            crit.add(Restrictions.ilike("segusuEstado", "%" + filters.get("segusuEstado").toString().trim() + "%"));
        }
        
        if (filters.containsKey("segusuEmail")) {
            crit.add(Restrictions.ilike("segusuEmail", "%" + filters.get("segusuEmail").toString().trim() + "%"));
        }
    }

    public UsuarioLazy() {
        dicc = new HashMap<String, String>();
    }
    public UsuarioLazy(boolean activo) {
        dicc = new HashMap<String, String>();
        this.isActivo=activo;
    }
    
    @Override
    public List<Ent> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        
        List<Ent> theList = null;
        //HibernateUtil.getSessionFactory().openSession();
        Session sess1 = entity.getSession();
        Transaction trans = null;
        List<Ent> resultList = null;
        
        try {
            // I use two queries: one to count filtered result and the second to get paged result
            
            Criteria countCrit = sess1.createCriteria(entC,"ent1");
            this.criteriaFilterSetup(countCrit, filters);
            countCrit.setProjection( Projections.projectionList()
                .add( Projections.rowCount() ) );
            Criteria dataCrit = sess1.createCriteria(entC,"ent1");
            this.criteriaFilterSetup(dataCrit, filters);
            this.criteriaSortSetup(dataCrit, sortField, sortOrder);
            this.criteriaPageSetup(dataCrit, first, pageSize);

            rowCount=0;
            rowCount = ((Long) countCrit.uniqueResult()).intValue();
            this.setRowCount(rowCount);
            
            resultList = dataCrit.list();
            
        }  catch (Exception e) {
			if(trans!=null) trans.rollback();
			System.out.println("*** Error: " + e.getMessage() );
			e.printStackTrace();
        } finally {
            sess1.close();
        }
        return resultList;
    }
    
    private void criteriaPageSetup(Criteria crit, int first, int pageSize) {
        crit.setFirstResult(first);
        crit.setMaxResults(pageSize);
    }
    
    private void criteriaSortSetup(Criteria crit, String field, SortOrder order) {

        if (field == null) {
            crit.addOrder((sortOrder_default.equalsIgnoreCase("ASC")) ? Order.asc(sortField_default) : Order.desc(sortField_default));
        } else {
            if (order.equals(SortOrder.ASCENDING)) {
                crit.addOrder(Order.asc(field));
            } else {
                crit.addOrder(Order.desc(field));
            }
        }
    }
}
