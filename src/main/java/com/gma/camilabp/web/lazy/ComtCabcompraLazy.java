/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.lazy;

import com.gma.camilabp.domain.ComtCabcompra;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author userdb5
 */
public class ComtCabcompraLazy extends BaseLazyDataModel<ComtCabcompra>{
    private String gentdoCodigo;
    private String comtdoCodigorel;
    
    public ComtCabcompraLazy() {
        super(ComtCabcompra.class, "cOMCCMnumsecucial", "DESC");
    }

    public ComtCabcompraLazy(String gentdoCodigo, String comtdoCodigorel) {
        super(ComtCabcompra.class, "cOMCCMnumsecucial", "DESC");
        this.gentdoCodigo = gentdoCodigo;
        this.comtdoCodigorel = comtdoCodigorel;
    }
    
    @Override
    public void criteriaFilterSetup(Criteria crit, Map<String, Object> filters) throws Exception {
        if(this.gentdoCodigo!=null)
            crit.add(Restrictions.eq("gENTDOcodigo",  this.gentdoCodigo ));
        if(this.comtdoCodigorel!=null)
            crit.add(Restrictions.eq("cOMTDOcodigorel",  this.comtdoCodigorel ));
        if(filters.containsKey("COMCCMnumsecucial"))
            crit.add(Restrictions.eq("cOMCCMnumsecucial",  new Long(filters.get("COMCCMnumsecucial").toString().trim()) ));
        if(filters.containsKey("GENOFIcodigo"))
            crit.add(Restrictions.eq("gENOFIcodigo", filters.get("GENOFIcodigo").toString().trim() ));
        if(filters.containsKey("COMCCMnumdocumen"))
            crit.add(Restrictions.eq("cOMCCMnumdocumen", new Integer(filters.get("COMCCMnumdocumen").toString().trim()) ));
        if(filters.containsKey("CXPPROcodigo"))
            crit.add(Restrictions.eq("cXPPROcodigo", filters.get("CXPPROcodigo").toString().trim() ));
        if(filters.containsKey("proveedor"))
            crit.add(Restrictions.ilike("proveedor", "%"+filters.get("proveedor").toString().trim()+"%" ));
        if(filters.containsKey("COMCCMnumfacprov"))
            crit.add(Restrictions.ilike("cOMCCMnumfacprov", "%"+filters.get("COMCCMnumfacprov").toString().trim()+"%" ));
        if(filters.containsKey("COMCCMnumdocurel"))
            crit.add(Restrictions.ilike("cOMCCMnumdocurel", "%"+filters.get("COMCCMnumdocurel").toString().trim()+"%" ));
    }
    
}
