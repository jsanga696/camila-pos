/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.lazy;

import com.gma.camilabp.domain.CbpactSolicitudEquipo;
import com.gma.camilabp.web.service.EntityManagerLocal;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author userdb5
 */
public class CbpactSolicitudEquipoLazy extends BaseLazyDataModel<CbpactSolicitudEquipo> {
    
    @EJB
    private EntityManagerLocal entity;
    
    private List<Integer> estados = null;
    private List<Integer> tipos = null;
    private List<Long> oficinas = null;

    public CbpactSolicitudEquipoLazy(List<Integer> estados, List<Integer> tipos, List<Long> oficinas) {
        super(CbpactSolicitudEquipo.class);
        this.estados = estados;
        this.tipos = tipos;
        this.oficinas = oficinas;
    }

    @Override
    public void criteriaFilterSetup(Criteria crit, Map<String, Object> filters) {

        Criteria equipo = crit.createCriteria("equipo");
        Criteria modelo = equipo.createCriteria("modelEquipo");
        Criteria oficina = equipo.createCriteria("genofiCodigo");
        
        if (this.estados != null) {
            crit.add(Restrictions.in("estado", this.estados));
        }
        if (this.tipos != null) {
            crit.add(Restrictions.in("tipoSolicitud", this.tipos));
        }
        if (this.oficinas != null) {
            oficina.add(Restrictions.in("genofiCodigo", this.oficinas));
        }

        if (filters.containsKey("nombreSolicitante")) {
            Session session = entity.getSession();
            
            Transaction tx = session.beginTransaction();
            SQLQuery query = session.createSQLQuery("SELECT CXCCLI_codigo FROM CXCM_CLIENTE WHERE CXCCLI_razonsocia LIKE ?");
            query.setParameter(0, "%" + filters.get("nombreSolicitante").toString().replace(" ", "%") + "%");
            List<String> l = query.list();
            tx.commit();
            if(l != null && !l.isEmpty())
                crit.add(Restrictions.in("cxcmClienteSolicitante", l));
        }
        
        if (filters.containsKey("equipo.proveedorText")) {
            Session session = entity.getSession();

            Transaction tx = session.beginTransaction();
            SQLQuery query = session.createSQLQuery("SELECT CXPPRO_codigo FROM CXPM_PROVEEDOR WHERE CXPPRO_razonsocia LIKE ?");
            query.setParameter(0, "%" + filters.get("equipo.proveedorText").toString().replace(" ", "%") + "%");
            List<String> l = query.list();
            tx.commit();
            if (l != null && !l.isEmpty()) {
                equipo.add(Restrictions.in("cxpmProveedor", l));
            }
        }
        
        if (filters.containsKey("equipo.placa")) {
            equipo.add(Restrictions.ilike("placa", "%" + filters.get("equipo.placa").toString().replace(" ", "%") + "%"));
        }

        if (filters.containsKey("equipo.modelEquipo.modelo")) {
            modelo.add(Restrictions.ilike("modelo", "%" + filters.get("equipo.modelEquipo.modelo").toString().replace(" ", "%") + "%"));
        }

        if (filters.containsKey("equipo.modelEquipo.marca")) {
            modelo.add(Restrictions.ilike("marca", "%" + filters.get("equipo.modelEquipo.marca").toString().replace(" ", "%") + "%"));
        }
    }

}
