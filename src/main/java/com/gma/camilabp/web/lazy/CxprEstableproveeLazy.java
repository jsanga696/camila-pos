/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.lazy;

import com.gma.camilabp.domain.CxpmProveedor;
import com.gma.camilabp.domain.CxprEstableprovee;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author userdb5
 */
public class CxprEstableproveeLazy extends BaseLazyDataModel<CxprEstableprovee> {

    private CxpmProveedor proveedor;
    
    public CxprEstableproveeLazy() {
        super(CxprEstableprovee.class);
    }
    
    public CxprEstableproveeLazy(CxpmProveedor proveedor) {
        super(CxprEstableprovee.class);
        this.proveedor = proveedor;
    }
    
    @Override
    public void criteriaFilterSetup(Criteria crit, Map<String, Object> filters) {
        if(this.proveedor != null){
            crit.add(Restrictions.eq("cXPPROcodigo", this.proveedor.getCxpmProveedorPK().getCXPPROcodigo()));
            crit.add(Restrictions.eq("gENCIAcodigo", this.proveedor.getCxpmProveedorPK().getGENCIAcodigo()));
            crit.add(Restrictions.eq("gENOFIcodigo", this.proveedor.getCxpmProveedorPK().getGENOFIcodigo()));
        }
        if(filters.containsKey("idSecuencia"))
            crit.add(Restrictions.eq("idSecuencia",  new Long(filters.get("idSecuencia").toString().trim()) ));
        if(filters.containsKey("CXPPROcodestableci"))
            crit.add(Restrictions.ilike("cXPPROcodestableci", "%"+filters.get("CXPPROcodestableci").toString().trim()+"%" ));
        if(filters.containsKey("CXPPROcodserie"))
            crit.add(Restrictions.ilike("cXPPROcodserie", "%"+filters.get("CXPPROcodserie").toString().trim()+"%" ));
    }
    
}
