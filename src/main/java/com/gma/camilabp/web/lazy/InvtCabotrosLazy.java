/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.lazy;

import com.gma.camilabp.domain.InvtCabotros;
import java.util.Collection;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Acer
 */
public class InvtCabotrosLazy extends BaseLazyDataModel<InvtCabotros>{
    private Collection listGentdoCodigo;
    private Collection listGenofiCodigo;
    private String codigoBodega;

    public InvtCabotrosLazy() {
        super(InvtCabotros.class, "iNVCOTnumsecuenc","DESC");
    }
    
    public InvtCabotrosLazy(Collection listGentdoCodigo, Collection listGenofiCodigo, String codigoBodega) {
        super(InvtCabotros.class, "iNVCOTnumsecuenc","DESC");
        this.listGentdoCodigo = listGentdoCodigo;
        this.listGenofiCodigo = listGenofiCodigo;
        this.codigoBodega = codigoBodega;
    }
    
    @Override
    public void criteriaFilterSetup(Criteria crit, Map<String, Object> filters) throws Exception {
        if(this.listGentdoCodigo!=null){
            crit.add(Restrictions.in("gENTDOcodigo", this.listGentdoCodigo));
        }
        if(this.listGenofiCodigo!=null){
            crit.add(Restrictions.in("gENOFIcodigo", this.listGenofiCodigo));
        }
        if(this.codigoBodega!=null){
            crit.add(Restrictions.eq("gENBODcodigorela", this.codigoBodega ));
        }
        if (filters.containsKey("GENOFIcodigo")) {
            crit.add(Restrictions.ilike("gENOFIcodigo", "%"+filters.get("GENOFIcodigo").toString().trim()+"%" ));
        }
        if (filters.containsKey("GENBODcodigorige")) {
            crit.add(Restrictions.ilike("gENBODcodigorige", "%"+filters.get("GENBODcodigorige").toString().trim()+"%" ));
        }
        if (filters.containsKey("GENBODcodigorela")) {
            crit.add(Restrictions.ilike("gENBODcodigorela", "%"+filters.get("GENBODcodigorela").toString().trim()+"%" ));
        }
        if (filters.containsKey("INVCOTnumsecuenc")) {
            crit.add(Restrictions.eq("iNVCOTnumsecuenc", new Long(filters.get("INVCOTnumsecuenc").toString()) ));
        }
        if (filters.containsKey("INVCOTnumdocumen")) {
            crit.add(Restrictions.eq("iNVCOTnumdocumen", new Integer(filters.get("INVCOTnumdocumen").toString()) ));
        }
        if (filters.containsKey("GENTDOcodigo")) {
            crit.add(Restrictions.ilike("gENTDOcodigo","%"+filters.get("GENTDOcodigo").toString().trim()+"%" ));
        }
        if (filters.containsKey("INVCOTcodusuario")) {
            crit.add(Restrictions.ilike("iNVCOTcodusuario", "%"+filters.get("INVCOTcodusuario").toString().trim()+"%" ));
        }
    }
}
