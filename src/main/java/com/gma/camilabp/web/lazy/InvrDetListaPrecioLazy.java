/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.lazy;

import com.gma.camilabp.domain.INVRdetLISTAPRECIO;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;


/**
 *
 * @author userdb6
 */
public class InvrDetListaPrecioLazy extends BaseLazyDataModel<INVRdetLISTAPRECIO>{
    private Long secuenciaListaPrecio;
    
    public InvrDetListaPrecioLazy() {
        super(INVRdetLISTAPRECIO.class, "iNVRdetLISTAPRECIOPK.iNVLPRsecuencia", "DESC");
    }
    
    public InvrDetListaPrecioLazy(Long secuenciaListaPrecio) {
        super(INVRdetLISTAPRECIO.class, "iNVRdetLISTAPRECIOPK.iNVLPRsecuencia", "DESC");
        this.secuenciaListaPrecio=secuenciaListaPrecio;
    }
    
    @Override
    public void criteriaFilterSetup(Criteria crit, Map<String, Object> filters) throws Exception {
        if(this.secuenciaListaPrecio!=null)
            crit.add(Restrictions.eq("iNVRdetLISTAPRECIOPK.iNVLPRsecuencia", this.secuenciaListaPrecio));
        if (filters.containsKey("INVITMcodigo")) {
            crit.add(Restrictions.ilike("iNVRdetLISTAPRECIOPK.iNVITMcodigo", "%"+filters.get("INVITMcodigo").toString().trim()+"%"));
        }
        if (filters.containsKey("nombreItem")) {
            crit.add(Restrictions.ilike("nombreItem", "%"+filters.get("nombreItem").toString().trim()+"%"));
        }
        
    }
}
