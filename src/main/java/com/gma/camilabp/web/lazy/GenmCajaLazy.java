/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.lazy;

import com.gma.camilabp.domain.GenmCaja;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author userdb6
 */
public class GenmCajaLazy extends BaseLazyDataModel<GenmCaja>{
    private String genciaCodigo;
    private String genofiCodigo;
    private String genbodCodigo;
    public GenmCajaLazy() {
        super(GenmCaja.class, "gENCAJcodigo", "DESC");
    }

    public GenmCajaLazy(String genciaCodigo, String genofiCodigo, String genbodCodigo) {
        super(GenmCaja.class, "gENCAJcodigo", "DESC");
        this.genciaCodigo = genciaCodigo;
        this.genofiCodigo = genofiCodigo;
        this.genbodCodigo=genbodCodigo;
    }
    
    @Override
    public void criteriaFilterSetup(Criteria crit, Map<String, Object> filters) throws Exception {
        if(this.genciaCodigo!=null)
            crit.add(Restrictions.eq("gENCIAcodigo", this.genciaCodigo));
        if(this.genofiCodigo!=null)
            crit.add(Restrictions.eq("gENOFIcodigo", this.genofiCodigo));
        if(this.genbodCodigo!=null)
            crit.add(Restrictions.eq("gENBODcodigo", this.genbodCodigo));
        if (filters.containsKey("facscaIdsececaja"))
            crit.add(Restrictions.eq("facscaIdsececaja", new Long(filters.get("facscaIdsececaja").toString())));
        if (filters.containsKey("GENCIAcodigo"))
            crit.add(Restrictions.eq("gENCIAcodigo", filters.get("GENCIAcodigo").toString().trim()));
        if (filters.containsKey("GENOFIcodigo"))
            crit.add(Restrictions.eq("gENOFIcodigo", filters.get("GENOFIcodigo").toString().trim()));
        if (filters.containsKey("GENCAJcodigo")) 
            crit.add(Restrictions.ilike("gENCAJcodigo", "%" + filters.get("GENCAJcodigo").toString().trim() + "%"));
        if (filters.containsKey("GENCAJdescripcio"))
            crit.add(Restrictions.ilike("gENCAJdescripcio", "%" + filters.get("GENCAJdescripcio").toString().trim() + "%"));
        if (filters.containsKey("SEGUSUcodigo")) 
            crit.add(Restrictions.ilike("sEGUSUcodigo", "%" + filters.get("SEGUSUcodigo").toString().trim() + "%"));
        if (filters.containsKey("FACVDRcodigo"))
            crit.add(Restrictions.ilike("fACVDRcodigo", "%" + filters.get("FACVDRcodigo").toString().trim() + "%"));
    }
}
