/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.lazy;

import com.gma.camilabp.domain.ComtCabpedsugerido;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author userdb5
 */
public class ComtCabpedsugeridoLazy extends BaseLazyDataModel<ComtCabpedsugerido>{
    
    private String secuenciaOficina;
    private String estado;
    
    public ComtCabpedsugeridoLazy(){
        super(ComtCabpedsugerido.class,"cOMPDSsecuencia","DESC");
    }

    public ComtCabpedsugeridoLazy(String secuenciaOficina, String estado) {
        super(ComtCabpedsugerido.class,"cOMPDSsecuencia","DESC");
        this.secuenciaOficina = secuenciaOficina;
        this.estado = estado;
    }
    
    @Override
    public void criteriaFilterSetup(Criteria crit, Map<String, Object> filters) throws Exception {
        if(this.secuenciaOficina!=null)
            crit.add(Restrictions.eq("gENOFIcodigo", this.secuenciaOficina));
        if(this.estado!=null)
            crit.add(Restrictions.eq("cOMPDSestado", this.estado));
        if(filters.containsKey("COMPDSsecuencia"))
            crit.add(Restrictions.eq("cOMPDSsecuencia",  new Long(filters.get("COMPDSsecuencia").toString().trim()) ));
        if(filters.containsKey("GENOFIcodigo"))
            crit.add(Restrictions.eq("gENOFIcodigo", filters.get("GENOFIcodigo").toString().trim() ));
        if(filters.containsKey("COMPDSnumero"))
            crit.add(Restrictions.eq("cOMPDSnumero", new Integer(filters.get("COMPDSnumero").toString().trim()) ));
        if(filters.containsKey("SEGUSUcodigo"))
            crit.add(Restrictions.eq("sEGUSUcodigo", filters.get("SEGUSUcodigo").toString().trim() ));
        if(filters.containsKey("CXPPROcodigo"))
            crit.add(Restrictions.eq("cXPPROcodigo", filters.get("CXPPROcodigo").toString().trim() ));
        if(filters.containsKey("proveedor"))
            crit.add(Restrictions.ilike("proveedor", "%"+filters.get("proveedor").toString().trim()+"%" ));
        if(filters.containsKey("COMPDSestado"))
            crit.add(Restrictions.ilike("cOMPDSestado", "%"+filters.get("COMPDSestado").toString().trim()+"%" ));
        if(filters.containsKey("COMPDSdescripcion"))
            crit.add(Restrictions.ilike("cOMPDSdescripcion", "%"+filters.get("COMPDSdescripcion").toString().trim()+"%" ));
        if(filters.containsKey("COMPDScodbodega"))
            crit.add(Restrictions.eq("cOMPDScodbodega", filters.get("COMPDScodbodega").toString().trim() ));        
    }
    
}
