/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.lazy;

import com.gma.camilabp.domain.CbpgenrOficina;
import java.util.List;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author userdb6
 */
public class CbpgenrOficinaLazyTest extends BaseLazyDataModel<CbpgenrOficina>{
    
    private Long genciaCodigo;
    private List<Long> genofiCodigoList;

    public CbpgenrOficinaLazyTest() {
        super(CbpgenrOficina.class, "genofiCodigo", "DESC");
    }

    public CbpgenrOficinaLazyTest(Long genciaCodigo) {
        super(CbpgenrOficina.class, "genofiCodigo", "DESC");
        this.genciaCodigo = genciaCodigo;
    }
    
    public CbpgenrOficinaLazyTest(List<Long> genofiCodigoList) {
        super(CbpgenrOficina.class, "genofiCodigo", "DESC");
        this.genofiCodigoList = genofiCodigoList;
    }
    
    @Override
    public void criteriaFilterSetup(Criteria crit, Map<String, Object> filters) throws Exception {
        if(this.genciaCodigo!=null){
            crit.add(Restrictions.eq("genciaCodigo", genciaCodigo));
        }
        if(this.genofiCodigoList!=null){
            crit.add(Restrictions.in("genofiCodigo", this.genofiCodigoList));
        }
        if (filters.containsKey("genofiSecuencia")) {
            crit.add(Restrictions.eq("genofiSecuencia", filters.get("genofiSecuencia").toString().trim()));
        }
        if (filters.containsKey("genofiCodigo")) {
            crit.add(Restrictions.eq("genofiCodigo", new Long(filters.get("genofiCodigo").toString().trim())));
        }
        if (filters.containsKey("genofiNombre")) {
            crit.add(Restrictions.ilike("genofiNombre", "%" + filters.get("genofiNombre").toString().trim() + "%"));
        }
        if (filters.containsKey("genofinomComercial")) {
            crit.add(Restrictions.ilike("genofinomComercial", "%" + filters.get("genofinomComercial").toString().trim() + "%"));
        }
    }
}
