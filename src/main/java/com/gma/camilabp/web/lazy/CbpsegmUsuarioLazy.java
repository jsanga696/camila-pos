/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.lazy;

import com.gma.camilabp.domain.CbpsegmUsuario;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author userdb6
 */
public class CbpsegmUsuarioLazy extends BaseLazyDataModel<CbpsegmUsuario>{

    public CbpsegmUsuarioLazy() {
        super(CbpsegmUsuario.class,"segusuCodigo","DESC");
    }
    
    @Override
    public void criteriaFilterSetup(Criteria crit, Map<String, Object> filters) throws Exception {
        crit.add(Restrictions.and(Restrictions.ne("seggppCodigo.seggppCodigo", 1L),Restrictions.ne("seggppCodigo.seggppCodigo", 2L)));
        if (filters.containsKey("segusuCodigo")) {
            crit.add(Restrictions.ilike("segusuCodigo", "%" + filters.get("segusuCodigo").toString().trim() + "%"));
        }
        if (filters.containsKey("segusuLogin")) {
            crit.add(Restrictions.ilike("segusuLogin", "%" + filters.get("segusuLogin").toString().trim() + "%"));
        }
        if (filters.containsKey("segusuNombre")) {
            crit.add(Restrictions.ilike("segusuNombre", "%" + filters.get("segusuNombre").toString().trim() + "%"));
        }
        if (filters.containsKey("segusuApellido")) {
            crit.add(Restrictions.ilike("segusuApellido", "%" + filters.get("segusuApellido").toString().trim() + "%"));
        }
        if (filters.containsKey("segusuAbreviado")) {
            crit.add(Restrictions.ilike("segusuAbreviado", "%" + filters.get("segusuAbreviado").toString().trim() + "%"));
        }
        if (filters.containsKey("codigoCaja")) {
            crit.add(Restrictions.ilike("codigoCaja", "%" + filters.get("codigoCaja").toString().trim() + "%"));
        }
        
    }
}
