/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.lazy;

import com.gma.camilabp.domain.FactCabvtacaja;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author userdb6
 */
public class FactCabvtacajaLazy extends BaseLazyDataModel<FactCabvtacaja>{
    private String genciaCodigo;
    private String genofiCodigo;
    private Integer turno;
    private String caja;

    public FactCabvtacajaLazy() {        
        super(FactCabvtacaja.class, "facvtcSecuenvta", "DESC");
    }

    public FactCabvtacajaLazy(String genciaCodigo, String genofiCodigo, Integer turno) {
        super(FactCabvtacaja.class, "facvtcSecuenvta", "DESC");
        this.genciaCodigo = genciaCodigo;
        this.genofiCodigo = genofiCodigo;
        this.turno = turno;
    }
    
    public FactCabvtacajaLazy(String genciaCodigo, String genofiCodigo, String caja) {
        super(FactCabvtacaja.class, "facvtcSecuenvta", "DESC");
        this.genciaCodigo = genciaCodigo;
        this.genofiCodigo = genofiCodigo;
        this.caja = caja;
    }
    
    @Override
    public void criteriaFilterSetup(Criteria crit, Map<String, Object> filters) throws Exception {
        if(this.genciaCodigo!=null)
            crit.add(Restrictions.eq("gENCIAcodigo", this.genciaCodigo));
        if(this.genofiCodigo!=null)
            crit.add(Restrictions.eq("gENOFIcodigo", this.genofiCodigo));
        if(this.turno!=null)
            crit.add(Restrictions.eq("fACSCAidsececaja", this.turno));
        if(this.caja!=null)
            crit.add(Restrictions.eq("gENCAJcodigo", this.caja));
        if (filters.containsKey("facvtcSecuenvta"))
            crit.add(Restrictions.eq("facvtcSecuenvta", new Long(filters.get("facvtcSecuenvta").toString())));
        if (filters.containsKey("documento")){
            crit.add(Restrictions.eq("documento", new Long(filters.get("documento").toString())));
        }
        if (filters.containsKey("documentoFact"))
            crit.add(Restrictions.ilike("documentoFact", "%" + filters.get("documentoFact").toString().trim() + "%"));
        if (filters.containsKey("FACVTCclierucci"))
            crit.add(Restrictions.ilike("fACVTCclierucci", "%" + filters.get("FACVTCclierucci").toString().trim() + "%"));
        if (filters.containsKey("FACVTCclienombre"))
            crit.add(Restrictions.ilike("fACVTCclienombre", "%" + filters.get("FACVTCclienombre").toString().trim() + "%"));
    }
}
