/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.lazy;

import com.gma.camilabp.domain.CxpmProveedor;
import com.gma.camilabp.domain.FactCabvtacaja;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author userdb5
 */
public class ProveedoresLazy extends BaseLazyDataModel<CxpmProveedor>{
    
    private String gENCIAcodigo, gENOFIcodigo;
    
    public ProveedoresLazy() {        
        super(CxpmProveedor.class);
    }
    
    public ProveedoresLazy(String gENCIAcodigo, String gENOFIcodigo) {        
        super(CxpmProveedor.class);
        this.gENCIAcodigo = gENCIAcodigo;
        this.gENOFIcodigo = gENOFIcodigo;
    }
    
    @Override
    public void criteriaFilterSetup(Criteria crit, Map<String, Object> filters) throws Exception {
        if (filters.containsKey("CXPPROrazonsocia")) {
            crit.add(Restrictions.ilike("cXPPROrazonsocia", "%"+filters.get("CXPPROrazonsocia").toString().trim()+"%"));
        }
        if (filters.containsKey("CXPPROruccedula")) {
            crit.add(Restrictions.ilike("cXPPROruccedula", "%"+filters.get("CXPPROruccedula").toString().trim()+"%"));
        }
        if (filters.containsKey("CXPPROdireccion")) {
            crit.add(Restrictions.ilike("cXPPROdireccion", "%"+filters.get("CXPPROdireccion").toString().trim()+"%"));
        }
    }
}
