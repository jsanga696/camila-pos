/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.lazy;

import com.gma.camilabp.domain.CxpmProveedor;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author userdb6
 */
public class CxpmProveedorLazy extends BaseLazyDataModel<CxpmProveedor>{
    
    private String codOficina;
    private String codCompania;

    public CxpmProveedorLazy() {
        super(CxpmProveedor.class, "cxpmProveedorPK.cXPPROcodigo", "ASC");
    }

    public CxpmProveedorLazy(String codOficina, String codCompania) {
        super(CxpmProveedor.class, "cxpmProveedorPK.cXPPROcodigo", "ASC");
        this.codOficina = codOficina;
        this.codCompania = codCompania;
    }
    
    @Override
    public void criteriaFilterSetup(Criteria crit, Map<String, Object> filters) throws Exception {       
        crit.add(Restrictions.eq("cXPPROafectainv", "S") );
        if(this.codCompania!=null)
            crit.add(Restrictions.eq("cxpmProveedorPK.gENCIAcodigo", this.codCompania));
        if(this.codOficina!=null)
            crit.add(Restrictions.eq("cxpmProveedorPK.gENOFIcodigo", this.codOficina));
        if (filters.containsKey("CXPPROcodigo")) 
            crit.add(Restrictions.eq("cxpmProveedorPK.cXPPROcodigo", filters.get("CXPPROcodigo").toString().trim()));
        if (filters.containsKey("CXPPROruccedula"))
            crit.add(Restrictions.ilike("cXPPROruccedula", "%" + filters.get("CXPPROruccedula").toString().trim() + "%"));
        if (filters.containsKey("CXPPROrazonsocia"))
            crit.add(Restrictions.ilike("cXPPROrazonsocia", "%" + filters.get("CXPPROrazonsocia").toString().trim() + "%"));
    }
}
