/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.lazy;

import com.gma.camilabp.domain.CbpgenrRecaudacion;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Acer
 */
public class CbpgenrRecaudacionLazy extends BaseLazyDataModel<CbpgenrRecaudacion>{
    private Integer tipo;

    public CbpgenrRecaudacionLazy() {
        super(CbpgenrRecaudacion.class, "genrrecId", "DESC");
    }

    public CbpgenrRecaudacionLazy(Integer tipo) {
        super(CbpgenrRecaudacion.class, "genrrecId", "DESC");
        this.tipo = tipo;
    }
    
    @Override
    public void criteriaFilterSetup(Criteria crit, Map<String, Object> filters) throws Exception {
        if(this.tipo!=null)
            crit.add(Restrictions.eq("genrrecTipotransaccion", this.tipo));
        if (filters.containsKey("genrrecId"))
            crit.add(Restrictions.eq("genrrecId", new Long(filters.get("genrrecId").toString())));
        if (filters.containsKey("genrrecCodigo"))
            crit.add(Restrictions.ilike("genrrecCodigo", "%"+filters.get("genrrecCodigo").toString().trim()+"%"));
        if (filters.containsKey("GENCAJcodigo"))
            crit.createCriteria("caja").add(Restrictions.eq("gENCAJcodigo", filters.get("GENCAJcodigo").toString().trim()));
        if (filters.containsKey("SEGUSUcodigo"))
            crit.createCriteria("caja").add(Restrictions.eq("sEGUSUcodigo", filters.get("SEGUSUcodigo").toString().trim()));
        if (filters.containsKey("segturNumero"))
            crit.add(Restrictions.eq("segturNumero", new Long(filters.get("segturNumero").toString())));
        if (filters.containsKey("BANTMVcodigo"))
            crit.add(Restrictions.eq("bANTMVcodigo", filters.get("BANTMVcodigo").toString().trim()));
        if (filters.containsKey("movimiento"))
            crit.add(Restrictions.ilike("movimiento", "%"+filters.get("movimiento").toString().trim()+"%"));
    }
    
}
