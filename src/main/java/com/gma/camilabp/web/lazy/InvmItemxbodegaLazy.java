/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.lazy;

import com.gma.camilabp.domain.InvmItemxbodega;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author userdb6
 */
public class InvmItemxbodegaLazy extends BaseLazyDataModel<InvmItemxbodega>{    
    private String codigo;
    private String nombre;
    private String codigoBodega;

    public InvmItemxbodegaLazy() {
        super(InvmItemxbodega.class, "invmItemxbodegaPK.iNVITMcodigo", "DESC");
    }
    
    public InvmItemxbodegaLazy(String codigoBodega,String codigo, String nombre) {
        super(InvmItemxbodega.class, "invmItemxbodegaPK.iNVITMcodigo", "DESC");
        this.codigoBodega = codigoBodega;
        this.codigo = codigo;
        this.nombre = nombre;
    }
    
    @Override
    public void criteriaFilterSetup(Criteria crit, Map<String, Object> filters) throws Exception {
        crit.add(Restrictions.isNotNull("nombreItem"));
        if(this.codigoBodega!=null){
            crit.add(Restrictions.eq("invmItemxbodegaPK.gENBODcodigo", this.codigoBodega));
        }
        if(this.nombre!=null){
            crit.add(Restrictions.ilike("nombreItem", "%"+this.nombre+"%"));
        }
        if(this.codigo!=null){
            crit.add(Restrictions.ilike("invmItemxbodegaPK.iNVITMcodigo", "%"+this.codigo+"%"));
        }
        if (filters.containsKey("INVITMcodigo")) {
            crit.add(Restrictions.ilike("invmItemxbodegaPK.iNVITMcodigo", "%"+filters.get("INVITMcodigo").toString().trim()+"%"));
        }
        if (filters.containsKey("nombreItem")) {
            crit.add(Restrictions.ilike("nombreItem", "%"+filters.get("nombreItem").toString().trim()+"%"));
        } 
    }
}
