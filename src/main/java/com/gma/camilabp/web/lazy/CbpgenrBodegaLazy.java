/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.lazy;

import com.gma.camilabp.domain.CbpgenrBodega;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author userdb2
 */
public class CbpgenrBodegaLazy extends BaseLazyDataModel<CbpgenrBodega> {

    private Long idOficina = null;
    
    public CbpgenrBodegaLazy() {
        super(CbpgenrBodega.class);
    }
    
    public CbpgenrBodegaLazy(Long idOficina) {
        super(CbpgenrBodega.class);
        this.idOficina = idOficina;
    }

    @Override
    public void criteriaFilterSetup(Criteria crit, Map<String, Object> filters) {
        if(idOficina != null){
            crit.add(Restrictions.eq("genofiCodigo.genofiCodigo", idOficina));
        }
        if (filters.containsKey("genbodSecuencia")) {
            crit.add(Restrictions.ilike("genbodSecuencia", "%" + filters.get("genbodSecuencia").toString().trim() + "%"));
        }
        if (filters.containsKey("genbodNombre")) {
            crit.add(Restrictions.ilike("genbodNombre", "%" + filters.get("genbodNombre").toString().trim() + "%"));
        }
    }
}
