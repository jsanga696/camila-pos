/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.service.Imp;

/**
 *
 * @author dbriones
 */
public class ServiceException extends Exception{
    
    private static final long serialVersionUID = 1L;

    public ServiceException (String mensaje){
            super(mensaje);

    }
}
