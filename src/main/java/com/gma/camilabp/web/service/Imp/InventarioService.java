/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.service.Imp;

import com.gma.camilabp.domain.CbpgenrCompania;
import com.gma.camilabp.model.consultas.InveRepdiasInventario;
import com.gma.camilabp.model.consultas.ResultadoTransferenciaAjuste;
import com.gma.camilabp.model.consultas.SaldoInventarioModel;
import com.gma.camilabp.model.consultas.TempAjusteTransferenciaInv;
import com.gma.camilabp.model.consultas.TipoFiltro;
import com.gma.camilabp.model.logica.TransferenciaAjusteInventario;
import com.gma.camilabp.sql.QuerysPos;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.service.InventarioLocal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import org.hibernate.HibernateException;

/**
 *
 * @author userdb6
 */
@Singleton
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
public class InventarioService implements InventarioLocal{
    @EJB
    private EntityManagerLocal entity;
    
    private Map<String, Object> parametros;

    @Override
    public List<InveRepdiasInventario> reporteSaldoInventario(SaldoInventarioModel sim) {
        List<InveRepdiasInventario> listado= null;
        try{
            System.out.println("OF:"+sim.getOficina().getGenofiSecuencia()+" US:"+sim.getUsuario()+" OP:"+sim.getOpcion()+" MA:"+sim.getMaquina());
            System.out.println("CP:"+sim.getCodigoCompania()+" OF:"+sim.getSecuenciaOficina());
            System.out.println("TC:"+sim.getTipoCosto()+" TI:"+sim.getCodigoTipoSaldoInventario());
            System.out.println("CR:"+(sim.getCorte()?sim.getFechaCorte():null));
            System.out.println("BOD:"+sim.getCadenaBodega()+" LP:"+sim.getCablistaprecio().getINVLPRcodigo());
            System.out.println("FILTRO"+(sim.getFiltro()?sim.getCodigoFiltro():null));
            System.out.println("COD. FILTRO"+(sim.getFiltro()?sim.getCodigoBusquedaFiltro():null));
            this.parametros = new HashMap<>();
            this.parametros.put("0", sim.getUsuario());
            this.parametros.put("1", sim.getOpcion());
            this.parametros.put("2", sim.getMaquina());
            this.parametros.put("3", sim.getCodigoCompania());
            this.parametros.put("4", sim.getOficina().getGenofiSecuencia());
            this.parametros.put("5", sim.getTipoCosto());
            this.parametros.put("6", sim.getCodigoTipoSaldoInventario());
            this.parametros.put("7", sim.getCorte()?sim.getFechaCorte():new Date());
            this.parametros.put("8", sim.getCadenaBodega());
            this.parametros.put("9", sim.getCablistaprecio().getINVLPRcodigo());
            this.parametros.put("10", sim.getFiltro()?sim.getCodigoFiltro():null);
            this.parametros.put("11", sim.getFiltro()?sim.getCodigoBusquedaFiltro():null);
            this.entity.executeProcedure("EXECUTE SP_INVR_REPGENERALINVENTARIO ?,?,?,?,?,?,?,?,?,?,?,?", this.parametros);
            
            this.parametros = new HashMap<>();
            this.parametros.put("0", sim.getUsuario());
            this.parametros.put("1", sim.getOpcion());
            this.parametros.put("2", sim.getMaquina());
            String complementoQuery="";
            int valor=3;
            sim.getEstadosItem().remove("TODOS");
            if(!sim.getEstadosItem().isEmpty()){
                for (String ei : sim.getEstadosItem()) {
                    complementoQuery=complementoQuery+"AND "+ei+"=? ";
                    this.parametros.put(valor+"", ei.equalsIgnoreCase("INVITM_estactivo")?"A":"S");
                    valor=valor+1;
                }
            }
            if(!sim.getCodigoStock().equalsIgnoreCase("T")){
                complementoQuery=complementoQuery+"AND INVITM_stockreal"+(sim.getCodigoStock().equalsIgnoreCase("D")?"!=":"=")+"? ";
                this.parametros.put(valor+"", 0);
                valor=valor+1;
            }
            if(!sim.getCodigoTipoItem().equalsIgnoreCase("TODOS")){                
                if(sim.getCodigoTipoItem().length()>1){
                    complementoQuery=complementoQuery+"AND INVITM_esttipo IN (:"+valor+") ";
                    this.parametros.put(valor+"", Arrays.asList(sim.getCodigoTipoItem().split(",")));
                }else{
                    complementoQuery=complementoQuery+"AND INVITM_esttipo=? ";
                    this.parametros.put(valor+"", sim.getCodigoTipoItem());
                    valor=valor+1;
                }
            }
            listado= this.entity.findAllSQL(QuerysPos.inveRepdiasInventarioSelect+QuerysPos.inveRepdiasInventarioWhere+complementoQuery+QuerysPos.inveRepdiasInventarioOrder, this.parametros, InveRepdiasInventario.class);
        } catch (HibernateException e) {
            Logger.getLogger(InventarioService.class.getName()).log(Level.SEVERE, null, e);
        }
        return listado;
    }
    
    @Override
    public List<TipoFiltro> nivelesInventario(CbpgenrCompania compania){
        List<TipoFiltro> niveles= new ArrayList<>();
        try{
            niveles.add(new TipoFiltro("N1", compania.getGenciaEtiinvnv01()));
            niveles.add(new TipoFiltro("N2", compania.getGenciaEtiinvnv02()));
            niveles.add(new TipoFiltro("N3", compania.getGenciaEtiinvnv03()));
            niveles.add(new TipoFiltro("N4", compania.getGenciaEtiinvnv04()));
        } catch (HibernateException e) {
            Logger.getLogger(InventarioService.class.getName()).log(Level.SEVERE, null, e);
        }
        return niveles;
    }

    @Override
    public List<TempAjusteTransferenciaInv> getAjusteTransferenciaTemporalList(String tabla) {
        List<TempAjusteTransferenciaInv> listado =null;
        try{
            String resultado = (String) entity.findObject("SQL", "SELECT CAST(NAME AS VARCHAR(50)) AS NAME FROM SYSOBJECTS WHERE name = '" + tabla + "'", null, null, Boolean.TRUE);
            if (resultado != null) {
                listado = this.entity.findAllSQL(QuerysPos.registrosTablaTemporal+tabla, null, TempAjusteTransferenciaInv.class);
            }
        } catch (HibernateException e) {
            Logger.getLogger(InventarioService.class.getName()).log(Level.SEVERE, null, e);
        }
        return listado;
    }
    
    @Override
    public TempAjusteTransferenciaInv getAjusteTransferenciaTemporal(String tabla, Integer id){
        TempAjusteTransferenciaInv registro=null;
        try{
            String resultado = (String) entity.findObject("SQL", "SELECT CAST(NAME AS VARCHAR(50)) AS NAME FROM SYSOBJECTS WHERE name = '" + tabla + "'", null, null, Boolean.TRUE);
            if (resultado != null) {
                this.parametros = new HashMap<>();
                this.parametros.put("0",tabla);
                this.parametros.put("1",id);
                registro = (TempAjusteTransferenciaInv) this.entity.findObject("SQL", QuerysPos.registrosTablaTemporal+QuerysPos.campoConsultaTablaTemporal, this.parametros, TempAjusteTransferenciaInv.class, Boolean.TRUE);
            }
        } catch (HibernateException e) {
            Logger.getLogger(InventarioService.class.getName()).log(Level.SEVERE, null, e);
        }
        return registro;
    }
    
    @Override
    public void insertTableTemp(String table, List<TempAjusteTransferenciaInv> listado, TransferenciaAjusteInventario model){
        try{
            String query = "INSERT INTO " + table + " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            this.parametros = new HashMap<>();
            for (TempAjusteTransferenciaInv t : listado) {                
                this.parametros.put("0", t.getINVCOT_numsecuenc());
                this.parametros.put("1", t.getINVCOT_numdocumen());
                this.parametros.put("2", t.getGENCIA_codigo());
                this.parametros.put("3", model.getOficina().getGenofiSecuencia());
                this.parametros.put("4", model.getTipoMovimiento().getGenrDocumentoPK().getGENMODcodigo());
                this.parametros.put("5", model.getTipoMovimiento().getGenrDocumentoPK().getGENTDOcodigo());
                this.parametros.put("6", t.getINVCOT_motvajuste());
                this.parametros.put("7", model.getBodegaOrigen().getGenbodSecuencia());
                this.parametros.put("8", t.getINVITM_codigo());
                this.parametros.put("9", t.getINVLOT_codigo());
                this.parametros.put("10", t.getINVLOT_fechavenci());
                this.parametros.put("11", t.getINVITM_uniembala());
                this.parametros.put("12", t.getINVDOT_precio());
                this.parametros.put("13", t.getINVDOT_cantifunci());
                this.parametros.put("14", t.getINVDOT_cantcajas());
                this.parametros.put("15", t.getINVDOT_cantunida());
                this.parametros.put("16", t.getINVDOT_estMultUnidad());
                this.parametros.put("17", t.getINVDOT_MultUniCod());
                this.parametros.put("18", t.getINVDOT_subtotamov());
                this.parametros.put("19", t.getItem_nombre());
                this.parametros.put("20", model.getBodegaDestino()!=null?model.getBodegaDestino().getGenbodSecuencia():null);
                this.entity.executeProcedure(query, this.parametros);
            }
        } catch (HibernateException e) {
            Logger.getLogger(InventarioService.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    @Override
    public ResultadoTransferenciaAjuste registrarTransferenciaAjusteInventario(String table, TransferenciaAjusteInventario model, String usuario) {
        ResultadoTransferenciaAjuste r=null;
        try{
            this.parametros = new HashMap<>();
            this.parametros.put("0", table);
            this.parametros.put("1", model.getTipoMovimiento().getGenrDocumentoPK().getGENCIAcodigo());
            this.parametros.put("2", model.getOficina().getGenofiSecuencia());
            this.parametros.put("3", new Date());
            this.parametros.put("4", "");
            this.parametros.put("5", model.getTipoMovimiento().getGenrDocumentoPK().getGENTDOcodigo());
            this.parametros.put("6", usuario);
            r=(ResultadoTransferenciaAjuste) this.entity.findObject("SQL", "EXECUTE SP_INVP_GRABAAJUSTEINVENTARIO ?,?,?,?,?,?,?", this.parametros, ResultadoTransferenciaAjuste.class, Boolean.TRUE);
        } catch (HibernateException e) {
            Logger.getLogger(InventarioService.class.getName()).log(Level.SEVERE, null, e);
        }
        return r;
    }
    
    @Override
    public String queryNivel(String nivel){
        String query=null;
        try{
            switch(nivel){
                case "N1":
                    query=QuerysPos.filtroNivel1;
                    break;
                case "N2":
                    query=QuerysPos.filtroNivel2;
                    break;
                case "N3":
                    query=QuerysPos.filtroNivel3;
                    break;
                case "N4":
                    query=QuerysPos.filtroNivel4;
                    break;
                default:
                    break;
            }
        } catch (HibernateException e) {
            Logger.getLogger(InventarioService.class.getName()).log(Level.SEVERE, null, e);
        }
        return query;
    }
    
    @Override
    public List<String> tipoTransferencias(){
        List<String> codigos= new ArrayList<>();
        codigos.add("TIP");
        codigos.add("TEP");
        return codigos;
    }
}
