/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.service;

import com.gma.camilabp.model.logica.CxprRetencionesTemp;
import javax.ejb.Local;

/**
 *
 * @author userdb2
 */
@Local
public interface RetencionesServiceLocal {

    /**
     * Si el numero de la factura es nulo solo consulta todos los retenciones
     * pendientes por el usuario
     *
     * @param numFactura Numero de la factura
     * @return CxprRetencionesTemp
     */
    public CxprRetencionesTemp getRetencion(String numFactura);

    /**
     * Elimina los datos temporales de retencion del usuario
     * @param estado estado de Retencion
     * @return true si se eliminaron exito, caso contrario false.
     */
    public boolean getEliminarDatosTemporales(String estado);

    public Boolean registarDatosRetencion(CxprRetencionesTemp retencionesTem);

    /**
     * Actualiza los datos ingresados en la pantalla de retencion
     *
     * @param retencionesTem
     * @param terminado indica si ya termino el proceso de retencion para el
     * registro
     * @return true si se almaceno correctamente, caso contrario false.
     */
    public Boolean respaldarRetencion(CxprRetencionesTemp retencionesTem, Boolean terminado);
    
    public void registrarDatosRetencionTemporal(String usuario, String tabla);

    /**
     * Guarda en la tabla temporal de retenciones
     *
     * @param nombreTableTempFact Tabla temporal de ingreso de factura
     * @return Datos ingresados en tabla temporal de la factura.
     */
    public CxprRetencionesTemp getRetencionTempFact(String nombreTableTempFact);

    /**
     * Verifica si tiene retencion la factura
     *
     * @param numfactura Factura a verificar 002-002-00000000289
     * @return True si tiene retencion caso contrario fale.
     */
    public boolean tieneRetencion(String numfactura);

}
