/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.service.Imp;

import com.gma.camilabp.domain.CbpgenrCompania;
import com.gma.camilabp.domain.CbpsegrGrupousuario;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.service.GrupoUsuariosLocal;
import java.io.Serializable;
import java.util.Calendar;
import java.util.List;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.interceptor.Interceptors;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author userdb2
 */
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
@Singleton
public class GrupoUsuariosService implements GrupoUsuariosLocal, Serializable {
    
    @EJB
    private EntityManagerLocal entity;

    @Override
    public void saveSegrGrupousuario(CbpsegrGrupousuario segrGrupousuario) {
        
        Session sess = entity.getSession();
        try {
            Calendar cl = Calendar.getInstance();            
            segrGrupousuario.setGenciaCodigo(1);
            segrGrupousuario.setSeggppCreafecha(cl.getTime());
            sess.merge(segrGrupousuario);
            sess.flush();
        } catch (HibernateException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public List<CbpsegrGrupousuario> getSegrGrupousuarios() {
        Session sess = entity.getSession();
        Query q1 = sess.createQuery("SELECT gu FROM CbpsegrGrupousuario gu");
        return q1.list();
    }

    @Override
    public CbpgenrCompania getGenrCompania(Long id) {
        Session sess = entity.getSession();
        Query q1 = sess.createQuery("SELECT gc FROM CbpgenrCompania gc where gc.genciaCodigo =:id");
        q1.setLong("id", id);
        return (CbpgenrCompania) q1.uniqueResult();
    }
    
        
}
