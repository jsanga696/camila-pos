/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.service;
import com.gma.camilabp.domain.CbpsegmUsuario;
import com.gma.camilabp.model.consultas.IngresoBodegaDetModel;
import com.gma.camilabp.model.consultas.IngresoBodegaModel;
import com.gma.camilabp.model.consultas.ResultadoIngresoCompra;
import com.gma.camilabp.model.consultas.TipoFiltro;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author userdb5
 */
@Local
public interface IngresoCompraLocal {
    public ResultadoIngresoCompra guardarIngresoBodega(IngresoBodegaModel compra, List<IngresoBodegaDetModel> detalle, HashMap datosAdicionales, CbpsegmUsuario usuario);
    public List<TipoFiltro> compraPorNumeroFactura(String secuenciaOficina, String codigoProveedor, String numFactura, Boolean posteado);
    public BigDecimal precioItemLista(String tipo, String codigoItem);
}



