/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.service.Imp;

import com.gma.camilabp.domain.CbpsegmAccion;
import com.gma.camilabp.domain.CbpsegmFuncion;
import com.gma.camilabp.domain.CbpsegmModulo;
import com.gma.camilabp.domain.CbpsegmOpcion;
import com.gma.camilabp.domain.CbpsegmPaquete;
import com.gma.camilabp.domain.CbpsegmPerfilrestricion;
import com.gma.camilabp.domain.CbpsegrGrupousuario;
import com.gma.camilabp.web.parametros.general.PerfilRestriccion;
import com.gma.camilabp.domain.CbpgenpParamcompania;
import com.gma.camilabp.domain.CbpgenpParamoficina;
import com.gma.camilabp.domain.CbpgenrBodega;
import com.gma.camilabp.domain.CbpgenrCompania;
import com.gma.camilabp.domain.CbpgenrOficina;
import com.gma.camilabp.domain.CbpgenrUsuarioxofi;
import com.gma.camilabp.domain.CbpsegmUsuario;
import com.gma.camilabp.domain.CxpmProveedor;
import com.gma.camilabp.domain.CxpmProveedorPK;
import com.gma.camilabp.domain.GenpParamgenerales;
import com.gma.camilabp.domain.InvmItem;
import com.gma.camilabp.sql.QuerysPos;
import com.gma.camilabp.web.service.EntityManagerLocal;
import java.util.List;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Singleton;
import javax.interceptor.Interceptors;
import org.hibernate.Query;
import org.hibernate.Session;
import com.gma.camilabp.web.service.GeneralLocal;
import com.gma.camilabp.web.service.SecuenciaLocal;
import com.gma.camilabp.web.session.UserSession;
import com.gma.camilabp.web.util.JsfUtil;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import org.hibernate.HibernateException;

/**
 *
 * @author desarrollo2
 */
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
@Singleton
public class GeneralService implements GeneralLocal, Serializable{    
    
    @EJB
    private SecuenciaLocal secuenciaLocal;
    @EJB
    private EntityManagerLocal entityManager;
    
    @Override
    public void saveGenpParamcompania(CbpgenpParamcompania genParaCompania) {
        Session sess = entityManager.getSession();
        try {
            sess.merge(genParaCompania);
            sess.flush();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    @Override
    public void saveGenpParamoficina(CbpgenpParamoficina genParaOficina) {
        Session sess = entityManager.getSession();
        try {
            sess.merge(genParaOficina);
            sess.flush();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    @Override
    public List<CbpgenpParamcompania> getGeneParaCompa(){
        Session sess = entityManager.getSession();
        Query q1 = sess.createQuery("SELECT p FROM GenpParamcompania p");
        return q1.list();
    }
    
    @Override
    public List<CbpsegmFuncion> getSegmFuncionList() {
        Session sess = entityManager.getSession();
        Query q1 = sess.createQuery("SELECT f FROM SegmFuncion f WHERE f.segfunEstado=1");
        return q1.list();
    }

    @Override
    public List<CbpsegmPaquete> getSegmPaqueteList() {
        Session sess = entityManager.getSession();
        Query q1 = sess.createQuery("SELECT p FROM SegmPaquete p WHERE p.segpaqEstado=1");
        return q1.list();
    }
    
    @Override
    public List<CbpsegmOpcion> getSegmOpcionList() {
        Session sess = entityManager.getSession();
        List<CbpsegmOpcion> opciones;
        List<CbpsegmOpcion> opcionesAcciones;
        Query q1 = sess.createQuery("Select a.segopcCodigo FROM SegmAccion a WHERE a.segaccSecuencia!='1' and a.segaccEstado=1 and a.segopcCodigo.segopcCodigo is not null");
        Query q2 = sess.createQuery("SELECT o FROM SegmOpcion o WHERE o.segopcEstado=1");
        opcionesAcciones = q1.list();
        opciones = q2.list();
        opciones.retainAll(opcionesAcciones);
        return opciones;
    }
    
    @Override
    public List<PerfilRestriccion> getPerfilRestriccionList() {
        List<PerfilRestriccion> perfilRestriccionList;
        List<CbpsegmPaquete> segmPaqueteList;
        segmPaqueteList = this.getSegmPaqueteList(); // get paquetes y modulos
        perfilRestriccionList = this.segmPaqueteToPerfilRestriccion(segmPaqueteList);
        this.agregarSegmFuncion(perfilRestriccionList);
        this.agregarSegmOpcion(perfilRestriccionList);
        return perfilRestriccionList;
    }

    public List<PerfilRestriccion> segmPaqueteToPerfilRestriccion(List<CbpsegmPaquete> segmPaqueteList)
    {
        List<PerfilRestriccion> perfilRestriccionList = new ArrayList<>();
        segmPaqueteList.stream().filter(p-> p.getCbpsegmModuloCollection().size()>0) // verifico que el paquete tenga modulos asignados
                .forEach(p -> //Paquete
        {
            PerfilRestriccion perfilRestriccionPAQ = new PerfilRestriccion(p.getSegpaqCodigo(),p.getSegpaqDescripcio());
            
            ((List <CbpsegmModulo>) p.getCbpsegmModuloCollection()).stream().filter(m -> m.getSegmodEstado())
                    .forEach(m ->{ //Modulo
                PerfilRestriccion perfilRestriccionMOD = new PerfilRestriccion(m.getSegmodCodigo(),m.getSegmodDescripcio());
                perfilRestriccionPAQ.getPerfilRestriccionList().add(perfilRestriccionMOD);
            });
            perfilRestriccionList.add(perfilRestriccionPAQ);
        });
        
        return perfilRestriccionList;
    }
    
    public void agregarSegmFuncion(List<PerfilRestriccion> perfilRestriccionList)
    {
        List<CbpsegmFuncion> segmFuncionList;
        segmFuncionList = this.getSegmFuncionList();
        perfilRestriccionList.stream().forEach(p->{ //paquete
            p.getPerfilRestriccionList().stream().forEach(m->{ // Modulo
                m.setPerfilRestriccionList(this.segmFuncionToPerfilRestriccion(segmFuncionList));
            });
        });
    }
    public List<PerfilRestriccion> segmFuncionToPerfilRestriccion(List<CbpsegmFuncion> segmFuncionList)
    {
        List<PerfilRestriccion> perfilRestriccionFuncList = new ArrayList<>();
        segmFuncionList.stream().forEach(f->{ //funcion
           perfilRestriccionFuncList.add(new PerfilRestriccion(f.getSegfunCodigo(),f.getSegfunDescripcio()));
        });
        return perfilRestriccionFuncList;
    }
    
    public List<PerfilRestriccion> segmOpcionToPerfilRestriccionByPaqModFun(List<CbpsegmOpcion> segmOpcionList,Long paqu, Long modu, Long func)
    {
        List<PerfilRestriccion> perfilRestriccionOpcList = new ArrayList<>();
        segmOpcionList.stream()
                .filter(o-> o.getSegopcEstado() && o.getSegpaqCodigo().equals(paqu) && o.getSegmodCodigo().getSegmodCodigo().equals(modu) && o.getSegfunCodigo().getSegfunCodigo().equals(func))
                .forEach(o->{ //Opcion
                      perfilRestriccionOpcList.add(new PerfilRestriccion(o.getSegopcCodigo(),o.getSegopcDescripcio()));
        });
        return perfilRestriccionOpcList;
    }

    public void agregarSegmOpcion(List<PerfilRestriccion> perfilRestriccionList)
    {
        List<CbpsegmOpcion> segmOpcionList;
        segmOpcionList = this.getSegmOpcionList();
        perfilRestriccionList.stream().forEach(p->{ //paquete
            p.getPerfilRestriccionList().stream().forEach(m->{ // Modulo
                m.getPerfilRestriccionList().stream().forEach(f->{ // Funcion
                    f.setPerfilRestriccionList(this.segmOpcionToPerfilRestriccionByPaqModFun(segmOpcionList, p.getId(), m.getId(), f.getId()));
               });
            });
        });
    }

    @Override
    public List<CbpsegmPerfilrestricion> getSegmPerfilrestricionList(Long segrGrupoUsuarioId) {
        Session sess = entityManager.getSession();
        Query q1 = sess.createQuery("SELECT p FROM SegmPerfilrestricion p WHERE p.seggppCodigo.seggppCodigo=:id and p.segprrEstado=1");
        q1.setLong("id", segrGrupoUsuarioId);
        return q1.list();
    }
    
    @Override
    public List<CbpsegmPerfilrestricion> getSegmPerfilrestricionList2(Long segrGrupoUsuarioId) {
        Session sess = entityManager.getSession();
        Query q1 = sess.createQuery("SELECT p FROM SegmPerfilrestricion p WHERE p.seggppCodigo.seggppCodigo=:id and p.segprrEstado=1");
        q1.setLong("id", segrGrupoUsuarioId);
        return q1.list();
    }

    @Override
    public CbpsegmOpcion getSegmOpcion(Long opcionCodigoId) {
        Session sess = entityManager.getSession();
        Query q1 = sess.createQuery("SELECT o FROM SegmOpcion o WHERE o.segopcCodigo=:id");
        q1.setLong("id", opcionCodigoId);
        return (CbpsegmOpcion) q1.uniqueResult();
    }

    @Override
    public void saveSegmPerfilrestricion(Long opcionId, CbpsegrGrupousuario grupoUsuario) {
        Session sess = entityManager.getSession();
      //  HiberUtil.requireTransaction();
        sess.beginTransaction();
        try {
            UserSession us = (UserSession) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userSession");
            Calendar cl = Calendar.getInstance();
            CbpsegmOpcion opcion = this.getSegmOpcion(opcionId);
            ((List<CbpsegmAccion>)opcion.getCbpsegmAccionCollection()).stream().filter(a-> !(a.getSegaccSecuencia().equalsIgnoreCase("1"))).forEach(a->{
                CbpsegmPerfilrestricion spr = new CbpsegmPerfilrestricion();
                spr.setSegprrSecuencia(secuenciaLocal.getSecuencia(sess, "SEGM_PERFILRESTRICION", BigInteger.ONE, BigInteger.ZERO, BigInteger.ZERO, BigInteger.ZERO, BigInteger.ZERO));
                spr.setSeggppCodigo(grupoUsuario.getSeggppCodigo());
                spr.setSegopcCodigo(opcion);
                spr.setGenciaCodigo(us.getCompaniaId());
                spr.setSegaccCodigo(a);
                spr.setSegprrEstado(true);
                spr.setSegprrCreauser(us.getCbpsegmUsuario().getSegusuCodigo());
                spr.setSegprrCreafecha(cl.getTime());
                sess.merge(spr);
            });
           sess.flush();
        } catch (HibernateException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void deleteSegmPerfilrestricion(CbpsegmPerfilrestricion segmPerfilrestricion) {
        Session sess = entityManager.getSession();
      //  HiberUtil.requireTransaction();
        sess.beginTransaction();
        try {
            sess.delete(segmPerfilrestricion);
            sess.flush();

        } catch (HibernateException ex) {
            
            ex.printStackTrace();
         //   throw new ServiceException(ex.getMessage());
            
        }
    }

    @Override
    public void duplicarPerfiles(List<CbpsegmPerfilrestricion> segmPerfilrestricionBaseList, CbpsegrGrupousuario grupoUsuario) {
        Session sess = entityManager.getSession();
        sess.beginTransaction();
        try {
            UserSession us = (UserSession) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userSession");
            Calendar cl = Calendar.getInstance();
            segmPerfilrestricionBaseList.stream().forEach(p->{
                p.setSegprrSecuencia(secuenciaLocal.getSecuencia(sess, "CBPSEGM_PERFILRESTRICION", BigInteger.ONE, BigInteger.ZERO, BigInteger.ZERO, BigInteger.ZERO, BigInteger.ZERO));
                p.setSeggppCodigo(grupoUsuario.getSeggppCodigo());
                p.setGenciaCodigo(us.getCompaniaId());
                p.setSegprrCreauser(us.getCbpsegmUsuario().getSegusuCodigo());
                p.setSegprrCreafecha(cl.getTime());
                sess.merge(p);
            });
            
            
           sess.flush();
        } catch (HibernateException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void asignarPerfiles(List<CbpsegmPerfilrestricion> segmPerfilrestricionBaseList, CbpsegrGrupousuario grupoUsuario) {
        Session sess = entityManager.getSession();
        sess.beginTransaction();
        try {
            UserSession us = (UserSession) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userSession");
            Calendar cl = Calendar.getInstance();
            segmPerfilrestricionBaseList.stream().forEach(p->{
                CbpsegmPerfilrestricion spr = new CbpsegmPerfilrestricion();
                spr.setSegprrSecuencia(secuenciaLocal.getSecuencia(sess, "SEGM_PERFILRESTRICION", BigInteger.ONE, BigInteger.ZERO, BigInteger.ZERO, BigInteger.ZERO, BigInteger.ZERO));
                spr.setSeggppCodigo(grupoUsuario.getSeggppCodigo());
                spr.setSegopcCodigo(p.getSegopcCodigo());
                spr.setGenciaCodigo(us.getCompaniaId());
                spr.setSegaccCodigo(p.getSegaccCodigo());
                spr.setSegprrEstado(true);
                spr.setSegprrCreauser(us.getCbpsegmUsuario().getSegusuCodigo());
                spr.setSegprrCreafecha(cl.getTime());
                sess.merge(spr);
//                p.setSeggppCodigo(grupoUsuario);
//                p.setGenciaCodigo(us.getCompaniaId());
//                p.setSegprrCreauser(us.getUserId());
//                p.setSegprrCreafecha(cl.getTime());
//                sess.merge(p);
            });
            
            
           sess.flush();
        } catch (HibernateException ex) {
            ex.printStackTrace();
        }
    }
    
    @Override
    public List<CbpgenrUsuarioxofi> getGenrUsuarioxofi(long idOficina, long idBodega) {
        Session sess = entityManager.getSession();
        Query q1;
        if (idBodega>0) {
            q1 = sess.createQuery("SELECT o FROM GenrUsuarioxofi o WHERE o.genofiCodigo=:idOfic and o.genbodCodigo=:idBod");
            q1.setLong("idOfic", idOficina);
            q1.setLong("idBod", idBodega);
        }else{
            q1 = sess.createQuery("SELECT o FROM GenrUsuarioxofi o WHERE o.genofiCodigo=:id");
            q1.setLong("id", idOficina);
        }
        
        return  q1.list();
    }

    @Override
    public void saveGenrUsuarioxofi(List<CbpsegmUsuario> segmUsuarioList, long idOficina, long idBodega) {
        Session sess = entityManager.getSession();
        sess.beginTransaction();
        UserSession us = (UserSession) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userSession");
        try {
            segmUsuarioList.stream().filter(u -> u.isSeleccionado()).forEach(u->{
                CbpgenrUsuarioxofi gu = new CbpgenrUsuarioxofi();
                gu.setGenbodCodigo(idBodega);
                gu.setGenciaCodigo(new CbpgenrCompania(us.getCompaniaId()));
                gu.setGenofiCodigo(new CbpgenrOficina(idOficina));
                //gu.setGenuxoCodigo(IDENTITY PK);
                gu.setSegusuCodigo(new CbpsegmUsuario(u.getSegusuCodigo()));
                gu.setSegusuLogin(u.getSegusuLogin());
                 sess.merge(gu);
            });
            
            
           sess.flush();
        } catch (HibernateException ex) {
            ex.printStackTrace();
        }
    }
/*
    @Override
    public void saveMantMotiInventario(InvrMotvajuste motivo) {
        Session sess = HiberUtil.getSession();
      //  HiberUtil.requireTransaction();
        sess.beginTransaction();
        try {
            Calendar cl = Calendar.getInstance();
            if (Utilidades.isEmptyTrim(motivo.getInvmajSecuencia())) {
                motivo.setInvmajSecuencia(secuenciaLocal.getSecuencia(sess,"INVR_MOTVAJUSTE", BigInteger.ONE, BigInteger.ZERO, BigInteger.ZERO, BigInteger.ZERO, BigInteger.ZERO));
            }
            //cxpmProveedor.setCxpproCodirela(1L);
            //motivo.setCxpproCupocredit(BigDecimal.ZERO);
            motivo.setInvmajCtacontab(1);
            motivo.setInvmajEstmodcta(1);
            motivo.setInvmajTrancontab(1);
            motivo.setInvmajSubtrancont(1);
            motivo.setInvmajCreafecha(cl.getTime());            
            motivo.setInvmajCreauser(1);
            sess.merge(motivo);
            sess.flush();

        } catch (HibernateException ex) {
            
            ex.printStackTrace();
            HiberUtil.rollback();
         //   throw new ServiceException(ex.getMessage());
            
        }
    }
  */
    
    public List<GenpParamgenerales> obtenerParametrosGeneralesPorCodigo(String cod){
        try{
            List<GenpParamgenerales> ls = this.entityManager.findListByParameters("SELECT m FROM GenpParamgenerales m WHERE m.genpParamgeneralesPK.gENPGEtabla = :cod", new String[]{"cod"}, new Object[]{cod});
            return ls;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
    
    @Override
    public Integer numeroDocumento(String compania, String oficina, String modulo, String documento){
        Map<String, Object> parametros= new HashMap<>();
        parametros.put("0", compania);
        parametros.put("1", oficina);
        parametros.put("2", modulo);
        parametros.put("3", documento);
        return (Integer) this.entityManager.findObject("SQL", QuerysPos.numeroDocumentoModulo, parametros, null, Boolean.TRUE);
    }
    
    @Override
    public BigDecimal secuenciaModulo(String modulo, String secuenciaCompania){
        Map<String, Object> parametros= new HashMap<>();
        parametros.put("0", modulo);
        parametros.put("1", secuenciaCompania);
        return (BigDecimal) this.entityManager.findObject("SQL", QuerysPos.secuenciaModulo, parametros, null, Boolean.TRUE);
    }
    
    @Override
    public Integer numeroDocumentoCompra(String compania, String oficina, String documento){
        Map<String, Object> parametros= new HashMap<>();
        parametros.put("0", compania);
        parametros.put("1", oficina);
        parametros.put("2", documento);
        return new Integer((String) this.entityManager.findObject("SQL", QuerysPos.numeroDocumentoCompra, parametros, null, Boolean.TRUE));
    }
    
    @Override
    public CbpgenrOficina consultarOficina(String secuenciaOficina){
        Map<String, Object> parametros;
        CbpgenrOficina oficina;
        if(secuenciaOficina!=null && secuenciaOficina.trim().length()>0){
            parametros= new HashMap<>();
            parametros.put("genofiSecuencia", secuenciaOficina.trim());
            oficina = this.entityManager.findObject(CbpgenrOficina.class, parametros);
            if(oficina==null)
                oficina = new CbpgenrOficina();
        }else{
            oficina = new CbpgenrOficina();
        }
        if(oficina.getGenofiCodigo()==null){
            JsfUtil.update("frmdlgOficina");
            JsfUtil.executeJS("PF('dlgOficina').show();");
        }
        return oficina;
    }    
    
    @Override
    public CbpgenrBodega consultarBodega(String secuenciaBodega){
        Map<String, Object> parametros;
        CbpgenrBodega bodega;
        if(secuenciaBodega!=null && secuenciaBodega.trim().length()>0){
            parametros= new HashMap<>();
            parametros.put("genbodSecuencia", secuenciaBodega.trim());
            bodega = this.entityManager.findObject(CbpgenrBodega.class, parametros);
            if(bodega==null)
                bodega = new CbpgenrBodega();
        }else{
            bodega = new CbpgenrBodega();
        }
        if(bodega.getGenbodCodigo()==null){
            JsfUtil.update("frmdlgBodega");
            JsfUtil.executeJS("PF('dlgBodega').show();");
        }
        return bodega;
    }
    
    @Override
    public CxpmProveedor consultarProveedor(String secuenciaCompania, String secuenciaOficina, String secuenciaProveedor){
        CxpmProveedor p;        
        if(secuenciaCompania!=null && secuenciaOficina!=null && secuenciaProveedor!=null && secuenciaProveedor.trim().length()>0){
            p = this.entityManager.find(CxpmProveedor.class, new CxpmProveedorPK(secuenciaCompania, secuenciaOficina, secuenciaProveedor));
            if(p==null)
                p = new CxpmProveedor();
        }else{
            p = new CxpmProveedor();
        }
        if(p.getCxpmProveedorPK()==null || p.getCxpmProveedorPK().getCXPPROcodigo()==null){
            JsfUtil.update("frmdlgProveedor");
            JsfUtil.executeJS("PF('dlgProveedor').show();");
        }
        return p;
    }
    
    @Override
    public BigDecimal valorImpuestoItem(String codItem, String codCompania, BigDecimal precio, Date fecha){
        Map<String, Object> parametros= new HashMap<>();
        parametros.put("0", codItem);
        parametros.put("1", codCompania);
        parametros.put("2", new SimpleDateFormat("yyyyMMdd").format(fecha));
        return precio.multiply((BigDecimal)this.entityManager.findObject("SQL", QuerysPos.porcentajeImpuesto, parametros, null, Boolean.TRUE)).divide(BigDecimal.valueOf(100.0)).setScale(2, BigDecimal.ROUND_HALF_UP);
    }
    
    @Override
    public InvmItem registrarItem(InvmItem item, CbpsegmUsuario usuario){
        try{
            HashMap map = new HashMap();
            map.put("0", item.getInvmItemPK().getGENCIAcodigo());
            map.put("1", item.getInvmItemPK().getINVITMcodigo());
            map.put("2", item.getINVITMnombre().toUpperCase());
            map.put("3", item.getINVITMnombre().toUpperCase());
            map.put("4", item.getINVITMporceutili());
            map.put("5", item.getINVITMporcutimin());
            map.put("6", item.getINVITMgrupodscto());
            map.put("7", item.getINVITMpeso());
            map.put("8", item.getINVITMvolumen());
            map.put("9", item.getINVITMunidembala());
            map.put("10", item.getINVUNIunidadgrup());
            map.put("11", item.getINVUNIunidadexis());
            map.put("12", item.getINVITMstock());
            map.put("13", item.getINVITMstkpending());
            map.put("14", item.getINVITMstkreserva());
            map.put("15", item.getINVITMstktransit());
            map.put("16", item.getINVITMstkminimo());
            map.put("17", item.getINVITMpreciopvp());
            map.put("18", item.getINVITMstkmaximo());
            map.put("19", item.getINVITMcostoprom1());
            map.put("20", item.getINVITMprecilista());
            map.put("21", item.getINVITMcostoprom2());
            map.put("22", item.getINVITMcostomerc1());
            map.put("23", item.getINVITMcostomerc2());
            map.put("24", item.getINVITMfechaingre());
            map.put("25", item.getINVITMobservacio());
            map.put("26", item.getINVITMesttipo());
            map.put("27", item.getINVITMestactivo());
            map.put("28", item.getINVTIMtipoitem());
            map.put("29", item.getINVITMestretorna());
            map.put("30", item.getINVITMestensamb());
            map.put("31", item.getINVITMestproduce());
            map.put("32", item.getINVITMestafecinv());
            map.put("33", item.getINVITMestdispvta());
            map.put("34", item.getINVITMestvtafrac());
            map.put("35", item.getINVITMestmovimie());
            map.put("36", item.getINVITMestadolote());
            map.put("37", item.getINVITMestadserie());            
            map.put("38", item.getINVITMestadescar());
            map.put("39", item.getINVITMestactpubl());
            map.put("40", item.getINVITMestmovfrac());
            map.put("41", item.getINVITMunimedvol());
            map.put("42", item.getINVITMtipocombo());
            map.put("43", item.getInvmItemPK().getINVITMcodigo());
            map.put("44", item.getINVITMestcompe());
            map.put("45", item.getINVITMpolitidesc());
            map.put("46", item.getINVITMasumereten());
            map.put("47", item.getINVITMmanedecimales());
            map.put("48", item.getINVITMmanedisplay());
            map.put("49", item.getIva()?"001":"002");
            map.put("50", item.getINVITMimpplastico());
            map.put("51", item.getINVITMtipolistaprecio());
            map.put("52", item.getINVITMuniAsignada());
            map.put("53", item.getINVITMestfecha());
            map.put("54", item.getINVITMunidmedbase());
            map.put("55", item.getINVITMestamultunid());
            map.put("56", item.getINVIN1codin1item());
            map.put("57", item.getINVIN2codin2item());
            map.put("58", item.getINVIN3codin3item());
            map.put("59", item.getINVIN4codin4item());
            this.entityManager.executeSQL(QuerysPos.insertItem, map);
            map = new HashMap();
            map.put("0", usuario.getSegusuLogin());
            map.put("1", new Date());
            map.put("2", item.getInvmItemPK().getINVITMcodigo());
            this.entityManager.executeSQL(QuerysPos.insertItemBodega, map);
            if(item.getIva()){
                map = new HashMap();
                map.put("0", item.getInvmItemPK().getINVITMcodigo());
                this.entityManager.executeSQL(QuerysPos.insertItemImpuesto, map);
            }
            map = new HashMap();
            map.put("0", item.getInvmItemPK().getINVITMcodigo());
            map.put("1", item.getCostoCompra());
            map.put("2", item.getInvmItemPK().getGENCIAcodigo());
            this.entityManager.executeSQL(QuerysPos.insertItemCosto, map);
            return this.entityManager.find(InvmItem.class, item.getInvmItemPK());
        } catch (HibernateException ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
