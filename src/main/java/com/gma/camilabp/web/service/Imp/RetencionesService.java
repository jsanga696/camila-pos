/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.service.Imp;

import com.gma.camilabp.domain.FactCabvtacaja;
import com.gma.camilabp.model.logica.CxprRetencionesDetalleTemp;
import com.gma.camilabp.model.logica.CxprRetencionesTemp;
import com.gma.camilabp.sql.QuerysPos;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.service.RetencionesServiceLocal;
import com.gma.camilabp.web.session.UserSession;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.hibernate.HibernateException;

/**
 *
 * @author userdb2
 */
@Stateless
public class RetencionesService implements RetencionesServiceLocal {

    private static final Logger LOG = Logger.getLogger(RetencionesService.class.getName());

    @EJB
    private EntityManagerLocal entity;

    private Map<String, Object> parametros;
    @Inject
    private UserSession session;

    /**
     * Si el numero de la factura es nulo solo consulta todos los retenciones
     * pendientes por el usuario
     *
     * @param numFactura Numero de la factura
     * @return CxprRetencionesTemp
     */
    @Override
    public CxprRetencionesTemp getRetencion(String numFactura) {
        parametros = new HashMap<>();
        parametros.put("0", session.getCbpsegmUsuario().getSegusuLogin());
        CxprRetencionesTemp retencion = null;
        if (numFactura != null) {
            parametros.put("1", numFactura);
            entity.executeProcedure("EXEC POS_RETENCION_FACTURA ?,?", parametros);
            retencion = (CxprRetencionesTemp) entity.findObject("SQL", QuerysPos.datosRetencion, parametros, CxprRetencionesTemp.class, Boolean.TRUE);
            retencion.setDetalle(entity.findAllSQL(QuerysPos.datosRetencionDetalle, parametros, CxprRetencionesDetalleTemp.class));
            return retencion;
        } else {
            parametros.put("1", "T");
            retencion = (CxprRetencionesTemp) entity.findObject("SQL", QuerysPos.datosRetencionUser, parametros, CxprRetencionesTemp.class, Boolean.TRUE);
            if (retencion != null) {
                parametros.put("1", retencion.getNum_factura());
                retencion.setDetalle(entity.findAllSQL(QuerysPos.datosRetencionUserDetalle, parametros, CxprRetencionesDetalleTemp.class));
            }
            return retencion;
        }
    }

    @Override
    public boolean getEliminarDatosTemporales(String estado) {
        try {
            parametros = new HashMap<>();
            parametros.put("0", session.getCbpsegmUsuario().getSegusuLogin());
            parametros.put("1", estado);
            CxprRetencionesTemp retencion = (CxprRetencionesTemp) entity.findObject("SQL", QuerysPos.datosRetencionUser, parametros, CxprRetencionesTemp.class, Boolean.TRUE);
            if (retencion != null) {
                parametros.put("1", retencion.getNum_factura());
            }
            entity.executeProcedure(QuerysPos.eliminarRetencionUserDetalle, parametros);
//            parametros.put("1", estado);
            return entity.executeProcedure(QuerysPos.eliminarRetencionUser, parametros);
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Error el ejecutar getEliminarDatosTemporales()", e);
            return false;
        }
    }

    @Override
    public Boolean registarDatosRetencion(CxprRetencionesTemp retencionesTem) {
        try {
            FactCabvtacaja factura = entity.find(FactCabvtacaja.class, retencionesTem.getSecuenciafactura().longValue());
            Boolean ok = false;
            for (CxprRetencionesDetalleTemp grupoRetencion : retencionesTem.getDetalle()) {
                parametros = new HashMap<>();
                parametros.put("0", session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia()); // CODIGO COMPANIA
//            parametros.put("1", "@CXCDEU_numesecuen = @CXCDEU_numesecuen OUT");
                parametros.put("1", 0);
                parametros.put("2", session.getCaja().getGENOFIcodigo()); // codigo oficina, GENCIA_CODIGO
                parametros.put("3", factura.getCXCCLIcodigo()); //CODIGO CLIENTE 
                parametros.put("4", retencionesTem.getFecha_emision());//FECHA EMI
                parametros.put("5", retencionesTem.getFecha_vigencia()); //FECHA VEN
                parametros.put("6", session.getCbpsegmUsuario().getSegusuCodigo().toString()); // CODIGO VENDEDOR
                parametros.put("7", retencionesTem.getCXCFPG_codigo());
                parametros.put("8", "99");// forma pago //CLIENTE
                parametros.put("9", "CXC"); // 10
                parametros.put("10", "RET");
                parametros.put("11", "");
//            parametros.put("12", "@CXCDEU_numdocumen = @CXCDEU_numdocumen OUT");
                parametros.put("12", "0");
                if (grupoRetencion.getRetencionTipo().contains("F")) {
                    parametros.put("13", "RET"); //causa verlo en select * from CXCR_CAUSADBCR
                } else {
                    parametros.put("13", "RIN"); //causa verlo en select * from CXCR_CAUSADBCR
                }

                parametros.put("14", factura.getGENCIAcodigo());//codigo de la cia  de la factura  //SACAR DE LA FACTURA
                parametros.put("15", factura.getGENOFIcodigo());//codigo de la oficina de la factura // SACAR DE LA FACTURA
                parametros.put("16", "CXC");
                parametros.put("17", "FAC");
                parametros.put("18", factura.getGENTDOcodigo());//codigo de la ruta de distribucion de la factura // SACAR DE LA FACTURA
                parametros.put("19", ""); // 20
                parametros.put("20", grupoRetencion.getValorRetenido());
                parametros.put("21", grupoRetencion.getValorRetenido());
                parametros.put("22", grupoRetencion.getValorRetenido());
                parametros.put("23", BigDecimal.ZERO);
                parametros.put("24", BigDecimal.ZERO);
                String descripcion = "CANJE DE RETENCION " + retencionesTem.getSRIRETcodigo() + " FACTURA NRO. " + retencionesTem.getNum_factura();
                parametros.put("25", descripcion);//descripcion ingresada por usuario en la pantalla
                parametros.put("26", descripcion);// descripcion ingresada por usuario en la pantalla
                parametros.put("27", "");
                parametros.put("28", "");
                parametros.put("29", ""); // 30
                parametros.put("30", ""); //
                parametros.put("31", session.getCbpsegmUsuario().getSegusuLogin());
                if (grupoRetencion.getRetencionTipo().contains("F")) {
                    parametros.put("32", "RET"); //cta contable de la causa verlo en select * from CXCR_CAUSADBCR
                } else {
                    parametros.put("32", "RIN"); //cta contable de la causa verlo en select * from CXCR_CAUSADBCR
                }
                parametros.put("33", "");
                parametros.put("34", "");
                parametros.put("35", retencionesTem.getNum_autorizacion());
                parametros.put("36", retencionesTem.getSRIRETcodigo());
                parametros.put("37", 0);
                parametros.put("38", BigDecimal.ZERO);
                parametros.put("39", BigDecimal.ZERO); // 40
                parametros.put("40", BigDecimal.ZERO);
                parametros.put("41", BigDecimal.ZERO);
                parametros.put("42", "A");
                ok = entity.executeProcedure(QuerysPos.registroRetencionTablasCamila, parametros);
            }
            if (ok) {
                respaldarRetencion(retencionesTem, ok);
            }
            return ok;
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Error el ejecutar getEliminarDatosTemporales()", e);
            return false;
        }
    }

    @Override
    public Boolean respaldarRetencion(CxprRetencionesTemp retencionesTem, Boolean terminado) {
        try {
            parametros = new HashMap<>();
            parametros.put("0", retencionesTem.getSRIRETcodigo());
            parametros.put("1", retencionesTem.getTipo_retencion());
            parametros.put("2", retencionesTem.getNum_autorizacion());
            parametros.put("3", session.getTurno().getSegturNumero().intValue());
            System.out.println("Terminado " + terminado);
            if (terminado) {
                parametros.put("4", "P"); // ESTADO PROCESADO
                parametros.put("5", session.getCbpsegmUsuario().getSegusuLogin());
                parametros.put("6", retencionesTem.getNum_factura());
                entity.executeProcedure(QuerysPos.actualizarRetencionTempP, parametros);
            } else {
                parametros.put("4", session.getCbpsegmUsuario().getSegusuLogin());
                parametros.put("5", retencionesTem.getNum_factura());
                entity.executeProcedure(QuerysPos.actualizarRetencionTemp, parametros);
            }
            return true;
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Error el ejecutar respaldarRetencion()", e);
            return false;
        }
    }

    @Override
    public CxprRetencionesTemp getRetencionTempFact(String nombreTableTempFact) {
        parametros = new HashMap<>();
        parametros.put("0", session.getCbpsegmUsuario().getSegusuLogin());
        CxprRetencionesTemp retencion = null;
        parametros.put("1", nombreTableTempFact);
        entity.executeProcedure("EXEC POS_RETENCION_FACTURA_TEMP ?,?", parametros);
        retencion = (CxprRetencionesTemp) entity.findObject("SQL", QuerysPos.datosRetencionTempFact, parametros, CxprRetencionesTemp.class, Boolean.TRUE);
        parametros.put("1", retencion.getNum_factura());
        retencion.setDetalle(entity.findAllSQL(QuerysPos.datosRetencionDetalleTempFact, parametros, CxprRetencionesDetalleTemp.class));
        return retencion;
    }

    @Override
    public void registrarDatosRetencionTemporal(String usuario, String tabla) {
        try {
            parametros = new HashMap<>();
            parametros.put("0", usuario);
            parametros.put("1", tabla);
            entity.executeProcedure("EXECUTE POS_RETENCION_FACTURA_TEMP ?,? ", this.parametros);
        } catch (HibernateException e) {
            Logger.getLogger(RetencionesService.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    @Override
    public boolean tieneRetencion(String numfactura) {
        try {
            //VERIFICAMOS QUE LA FACTURA NO TENGA REGISTRADA UNA FACTURA
            parametros = new HashMap<>();
            parametros.put("0", numfactura);
            parametros.put("1", "P"); // RETENCION YA PROCESADA
            CxprRetencionesTemp retencionProcesada = (CxprRetencionesTemp) entity.findObject("SQL", QuerysPos.datosRetencionP, parametros, CxprRetencionesTemp.class, Boolean.TRUE);
            return (retencionProcesada != null);
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Error al procesar metodo tieneRetencion", e);
            return false;
        }
    }

}
