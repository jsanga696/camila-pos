/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.service.Imp;

import com.gma.camilabo.enums.ActionsTransaccion;
import com.gma.camilabp.domain.CbpgenmDenominacion;
import com.gma.camilabp.domain.CbpgenrBodega;
import com.gma.camilabp.domain.CbpgenrCobro;
import com.gma.camilabp.domain.CbpgenrCompania;
import com.gma.camilabp.domain.CbpgenrControltrans;
import com.gma.camilabp.domain.CbpgenrDetregistroDenominacion;
import com.gma.camilabp.domain.CbpgenrOficina;
import com.gma.camilabp.domain.CbpgenrPoliticabase;
import com.gma.camilabp.domain.CbpgenrPoliticabodega;
import com.gma.camilabp.domain.CbpgenrPoliticaitem;
import com.gma.camilabp.domain.CbpgenrRegistroDenominacion;
import com.gma.camilabp.domain.CbpposCabfacttemporal;
import com.gma.camilabp.domain.CbpposFacttemporal;
import com.gma.camilabp.domain.CbpsegmTurno;
import com.gma.camilabp.domain.CbpsegmUsuario;
import com.gma.camilabp.domain.CxcmCliente;
import com.gma.camilabp.domain.CxcmClientePK;
import com.gma.camilabp.domain.FactCabvtacaja;
import com.gma.camilabp.domain.FactSesioncaja;
import com.gma.camilabp.domain.GenmCaja;
import com.gma.camilabp.domain.InvmItem;
import com.gma.camilabp.domain.InvmItemxbodega;
import com.gma.camilabp.model.consultas.Facturacion;
import com.gma.camilabp.model.consultas.ResultadoCobro;
import com.gma.camilabp.model.consultas.ResultadoFacturacion;
import com.gma.camilabp.model.consultas.TemDevolucionFactura;
import com.gma.camilabp.model.consultas.TempCbpCobUsuarioOpcion;
import com.gma.camilabp.model.consultas.TempCbpPcUsuarioOpcion;
import com.gma.camilabp.model.logica.Pago;
import com.gma.camilabp.model.logica.ValoresPago;
import com.gma.camilabp.sql.QuerysPos;
import com.gma.camilabp.web.service.BitacoraLocal;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.service.PuntoVentaLocal;
import com.gma.camilabp.web.session.UserSession;
import com.gma.camilabp.web.util.JsfUtil;
import com.gma.camilabp.web.util.Utilidades;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.EJB;
//import javax.ejb.Lock;
//import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.inject.Inject;
//import javax.interceptor.Interceptors;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author userdb6
 */
@Singleton
//@Interceptors(value = {HibernateEjbInterceptor.class})
//@Lock(LockType.READ)
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
public class PuntoVentaService implements PuntoVentaLocal {

    @EJB
    private EntityManagerLocal entity;
    @EJB
    private BitacoraLocal bitacora;
    private Map<String, Object> parametros;
    @Inject
    private UserSession session;

    @Override
    public String validarIdentificacion(String tipo, String identificacion) {
        String resultado = "";
        try {
            Session sess = entity.getSession();
            Query q = sess.createSQLQuery("SELECT dbo.SP_VERIFICA_CEDULA ('" + identificacion + "','" + tipo + "')");
            resultado = ((Character) q.uniqueResult()).toString();
        } catch (HibernateException e) {
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
        }
        return resultado;
    }
    
    @Override
    public Boolean existeCodigoCLiente(String codigoCompania, String codigoOficina, String codigoCliente){
        Boolean resultado = Boolean.FALSE;
        try {
            CxcmCliente c = this.entity.find(CxcmCliente.class, new CxcmClientePK(codigoCompania, codigoOficina, codigoCliente.toUpperCase()));
            resultado = c!=null;
        } catch (HibernateException e) {
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
        }
        return resultado;
    }

    @Override
    public String grabarCliente(CxcmCliente c, GenmCaja caja, Boolean generaCodigo) {
        String codigoCliente = null;
        try {
            Session sess = entity.getSession();
            Query q = sess.createSQLQuery("EXECUTE POS_REGISTRAR_CXCMCLIENTE ?,?,?,?,?,?,?,?,?,?,?,?,?");
            q.setParameter(0, (c.getCxcmClientePK() != null && c.getCxcmClientePK().getCXCCLIcodigo() != null) ? "M" : "A");
            q.setParameter(1, (c.getCxcmClientePK() != null && c.getCxcmClientePK().getCXCCLIcodigo() != null) ? c.getCxcmClientePK().getCXCCLIcodigo() : (generaCodigo?null:c.getCodigoCliente().toUpperCase()));
            q.setParameter(2, caja.getGENCIAcodigo());
            q.setParameter(3, caja.getGENOFIcodigo());
            q.setParameter(4, c.getCXCCLIrazonsocia().toUpperCase());
            q.setParameter(5, c.getCXCCLItipo());
            q.setParameter(6, c.getCXCCLIrucci());
            q.setParameter(7, c.getCXCCLItelefono());
            q.setParameter(8, c.getCXCCLIemail());
            q.setParameter(9, c.getCXCCLIdireccion1().toUpperCase());
            q.setParameter(10, caja.getCablistaprecio().getINVLPRcodigo());
            q.setParameter(11, caja.getCxcrutCodigo());
            q.setParameter(12, generaCodigo?"S":"N");
            codigoCliente = (String) q.uniqueResult();
        } catch (HibernateException e) {
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
        }
        return codigoCliente;
    }

    @Override
    public List<CxcmCliente> consultarCliente(String codigoCliente, String identificacionCliente, String razonSocial) {
        List<CxcmCliente> c = null;
        Map<String, Object> parametrosString = null;
        try {
            this.parametros = new HashMap<>();
            if (codigoCliente != null) {
                this.parametros.put("cxcmClientePK.cXCCLIcodigo", codigoCliente);
            }
            if (identificacionCliente != null) {
                this.parametros.put("cXCCLIrucci", identificacionCliente);
            }
            if (razonSocial != null) {
                parametrosString = new HashMap<>();
                parametrosString.put("cXCCLIrazonsocia", "%" + razonSocial + "%");
            }
            c = entity.findAllByParameterAndParameterIlike(CxcmCliente.class, this.parametros, parametrosString);
        } catch (HibernateException e) {
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
        }
        return c;
    }

    @Override
    public String secuenciaDocumento(String genciaCodigo, int idGenpTalonario, String tipoEjecucion) {
        String secuencia = null;
        try {
            this.parametros = new HashMap<>();
            this.parametros.put("0", genciaCodigo);
            this.parametros.put("1", idGenpTalonario);
            this.parametros.put("2", tipoEjecucion);
            secuencia = (String) entity.findObject("SQL", "DECLARE @secuencia VARCHAR(10) EXECUTE SP_GENP_SACASECUENCIADOCUMEN ?,?,?,@secuencia OUT SELECT @secuencia", this.parametros, null, tipoEjecucion.equalsIgnoreCase("P"));
        } catch (HibernateException e) {
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
        }
        return secuencia;
    }

    @Override
    public Boolean registroTablaTemporal(String procedimiento, String nombreTabla) {
        Boolean registrado = Boolean.FALSE;
        try {
            this.parametros = new HashMap<>();
            this.parametros.put("0", nombreTabla);
            registrado = entity.executeProcedure("EXECUTE " + procedimiento + " ?", this.parametros);
        } catch (HibernateException e) {
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
        }
        return registrado;
    }

    @Override
    public Integer insertarRegistroTablaTemporal(String nombreTabla, TempCbpPcUsuarioOpcion rt) {
        Integer id = null;
        try {
            String query = "INSERT INTO " + nombreTabla + " (FACCPD_numdocumen,FACCPD_numsectrans,ID_Pedidos,ID_lineaPed,GENCIA_codigo,GENOFI_codigo,FACCPD_fechaemisi,FACVDR_tiporeg,CXCCLI_codigo,FACVDR_codigo,CXCRUT_codigo,FACCPD_etipoped,"
                    + "INVIN1_codigo,INVITM_codigo,FACDVT_detallefactserv,FACDPD_cantfuncional,FACDPD_porcendscto,FACDPD_precio,FACDPD_subtotal,FACDPD_descuento,FACDPD_impuesto,FACDPD_neto,FACDPD_proveedor,FACDPD_listaprecio,"
                    + "CONPLC_codigo,CXCFPG_codigo,CXCTPG_codigo,impuesto,INVITM_peso,INVITM_volumen,INVITM_unidembala) OUTPUT Inserted.ID_secuencia VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            if (rt.getIdSecuencia() != null) {
                query = "UPDATE " + nombreTabla + " SET FACCPD_numdocumen=?,FACCPD_numsectrans=?,ID_Pedidos=?,ID_lineaPed=?,GENCIA_codigo=?,GENOFI_codigo=?,FACCPD_fechaemisi=?,FACVDR_tiporeg=?,CXCCLI_codigo=?,FACVDR_codigo=?,CXCRUT_codigo=?,FACCPD_etipoped=?,"
                        + "INVIN1_codigo=?,INVITM_codigo=?,FACDVT_detallefactserv=?,FACDPD_cantfuncional=?,FACDPD_porcendscto=?,FACDPD_precio=?,FACDPD_subtotal=?,FACDPD_descuento=?,FACDPD_impuesto=?,FACDPD_neto=?,FACDPD_proveedor=?,FACDPD_listaprecio=?,"
                        + "CONPLC_codigo=?,CXCFPG_codigo=?,CXCTPG_codigo=?,impuesto=?,INVITM_peso=?,INVITM_volumen=?,INVITM_unidembala=? OUTPUT Inserted.ID_secuencia WHERE ID_secuencia=?";
            }
            this.parametros = new HashMap<>();
            this.parametros.put("0", rt.getFaccpdNumdocumen());
            this.parametros.put("1", rt.getFaccpdNumsectrans());
            this.parametros.put("2", rt.getIdPedidos());
            this.parametros.put("3", rt.getIdLineaPed());
            this.parametros.put("4", rt.getGenciaCodigo());
            this.parametros.put("5", rt.getGenofiCodigo());
            this.parametros.put("6", rt.getFaccpdFechaemisi());
            this.parametros.put("7", rt.getFacvdrTiporeg());
            this.parametros.put("8", rt.getCxccliCodigo());
            this.parametros.put("9", rt.getFacvdrCodigo());
            this.parametros.put("10", rt.getCxcrutCodigo());
            this.parametros.put("11", rt.getFaccpdEtipoped());
            this.parametros.put("12", rt.getInviniCodigo());
            this.parametros.put("13", rt.getInvitmCodigo());
            this.parametros.put("14", rt.getFacdvtDetallefactserv());
            this.parametros.put("15", rt.getFacdpdCantfuncional());
            this.parametros.put("16", rt.getFacdpdPorcendscto());
            this.parametros.put("17", rt.getFacdpdPrecio());
            this.parametros.put("18", rt.getFacdpdSubtotal());
            this.parametros.put("19", rt.getFacdpdDescuento());
            this.parametros.put("20", rt.getFacdpdImpuesto());
            this.parametros.put("21", rt.getFacdpdNeto());
            this.parametros.put("22", rt.getFacdpdProveedor());
            this.parametros.put("23", rt.getFacdpdListaprecio());
            this.parametros.put("24", rt.getConplcCodigo());
            this.parametros.put("25", rt.getCxcfpgCodigo());
            this.parametros.put("26", rt.getCxctpgCodigo());
            this.parametros.put("27", rt.getImpuesto());
            this.parametros.put("28", rt.getInvitmPeso());
            this.parametros.put("29", rt.getInvitmVolumen());
            this.parametros.put("30", rt.getInvitmUnidembala());
            if (rt.getIdSecuencia() != null) {
                this.parametros.put("31", rt.getIdSecuencia());
            }
            id = (Integer) entity.findObject("SQL", query, this.parametros, null, Boolean.TRUE);
            this.parametros = new HashMap<>();
            this.parametros.put("0", nombreTabla);
            this.parametros.put("1", rt.getFacdpdListaprecio());
            this.parametros.put("2", rt.getGenciaCodigo());
            this.parametros.put("3", rt.getGenofiCodigo());
            this.parametros.put("4", id);
            entity.executeProcedure("EXECUTE CBPPTO_VTACALVALORTMP ?,?,?,?,?", this.parametros);
        } catch (HibernateException e) {
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
        }
        return id;
    }

    @Override
    public List<TempCbpPcUsuarioOpcion> consultarTablaTemporal(String nombreTabla) {
        List<TempCbpPcUsuarioOpcion> listado = null;
        try {
            String resultado = (String) entity.findObject("SQL", "SELECT CAST(NAME AS VARCHAR(50)) AS NAME FROM SYSOBJECTS WHERE name = '" + nombreTabla + "'", null, null, Boolean.TRUE);
            if (resultado != null) {
                this.parametros = new HashMap<>();
                this.parametros.put("0", nombreTabla);
                this.parametros.put("1", null);
                listado = entity.findAllSQL("EXEC POS_CONSULTAR_TEMPORAL ?,?", this.parametros, TempCbpPcUsuarioOpcion.class);
            }
        } catch (HibernateException e) {
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
        }
        return listado;
    }

    @Override
    public TempCbpPcUsuarioOpcion consultarTemporal(String nombreTabla, Integer codigo) {
        TempCbpPcUsuarioOpcion r = null;
        try {
            this.parametros = new HashMap<>();
            this.parametros.put("0", nombreTabla);
            this.parametros.put("1", codigo);
            r = (TempCbpPcUsuarioOpcion) entity.findObject("SQL", "EXEC POS_CONSULTAR_TEMPORAL ?,?", parametros, TempCbpPcUsuarioOpcion.class, Boolean.TRUE);
        } catch (HibernateException e) {
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
        }
        return r;
    }

    @Override
    public void borrarTemporal(String nombreTabla, Integer codigo, String codigoItem, GenmCaja caja) {
        try {
            this.parametros = new HashMap<>();
            this.parametros.put("0", codigo);
            entity.executeProcedure("DELETE FROM " + nombreTabla + " WHERE ID_secuencia=?", this.parametros);
            if (nombreTabla.startsWith("X_CBPFACT") && caja!=null)
                bitacora.saveActionLog(ActionsTransaccion.ELIMINAR_ITEM, codigoItem, this.idSupervisor(caja));            
        } catch (HibernateException e) {
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    @Override
    public void borrarTemporalPorTipoPago(String nombreTabla, Integer tipo) {
        try {
            this.parametros = new HashMap<>();
            this.parametros.put("0", tipo);
            String resultado = (String) entity.findObject("SQL", "SELECT CAST(NAME AS VARCHAR(50)) AS NAME FROM SYSOBJECTS WHERE name = '" + nombreTabla + "'", null, null, Boolean.FALSE);
            if (resultado != null) {
                entity.executeProcedure("DELETE FROM " + nombreTabla + " WHERE tipo_pago=?", this.parametros);
            }
        } catch (HibernateException e) {
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    @Override
    public String consultarCodigoClienteTemporal(String nombreTabla) {
        try {
            return (String) entity.findObject("SQL", "SELECT DISTINCT CXCCLI_codigo FROM " + nombreTabla, null, null, Boolean.FALSE);
        } catch (HibernateException e) {
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
        }
        return null;
    }

    @Override
    public void actualizarCodigoClienteTemporal(String nombreTabla, String codigoCliente, String codigoCompania, String codigoOficina, String listaPrecio) {
        try {
            String complemento="";
            complemento=", FACDPD_listaprecio='"+listaPrecio+"'";
            entity.executeProcedure("UPDATE " + nombreTabla + " SET CXCCLI_codigo='" + codigoCliente + "'"+complemento, null);
            if(listaPrecio!=null && listaPrecio.trim().length()>1){
                this.parametros = new HashMap<>();
                this.parametros.put("0", nombreTabla);
                this.parametros.put("1", listaPrecio);
                this.parametros.put("2", codigoCompania);
                this.parametros.put("3", codigoOficina);
                this.parametros.put("4", 0);
                entity.executeProcedure("EXECUTE CBPPTO_VTACALVALORTMP ?,?,?,?,?", this.parametros);
            }
        } catch (HibernateException e) {
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    @Override
    public List<InvmItem> consultarItem(String codigoItem, String nombreItem, String codigoBarra) {
        List<InvmItem> li = null;
        Map<String, Object> parametrosString;
        try {
            this.parametros = new HashMap<>();
            parametrosString = new HashMap<>();
            if (codigoItem != null) {
                this.parametros.put("invmItemPK.iNVITMcodigo", codigoItem );
            }
            if (nombreItem != null) {
                parametrosString.put("iNVITMnombre", "%" + nombreItem + "%");
            }
            if (codigoBarra != null) {
                this.parametros.put("iNVITMcodigbarra", codigoBarra);
            }
            li = entity.findAllByParameterAndParameterIlike(InvmItem.class, this.parametros, parametrosString);
        } catch (HibernateException e) {
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
        }
        return li;
    }
    
    @Override
    public List<InvmItemxbodega> consultarItemBodega(String codigoItem, String nombreItem, String codigoBarra) {
        List<InvmItemxbodega> li = null;
        Map<String, Object> parametrosString;
        try {
            this.parametros = new HashMap<>();
            parametrosString = new HashMap<>();
            if (codigoItem != null) {
                parametrosString.put("invmItemxbodegaPK.iNVITMcodigo", "%" + codigoItem + "%");
            }
            if (nombreItem != null) {
                parametrosString.put("nombreItem", "%" + nombreItem + "%");
            }
            if (codigoBarra != null) {
                this.parametros.put("codigoBarraItem", codigoBarra);
            }
            li = entity.findAllByParameterAndParameterIlike(InvmItemxbodega.class, this.parametros, parametrosString);
        } catch (HibernateException e) {
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
        }
        return li;
    }

    @Override
    public CbpsegmTurno consultarTurno(CbpsegmUsuario usuario) {
        CbpsegmTurno t = null;
        try {
            this.parametros = new HashMap<>();
            this.parametros.put("segusuCodigo", usuario);
            this.parametros.put("segturEstado", true);
            t = entity.findObject(CbpsegmTurno.class, parametros);
        } catch (HibernateException e) {
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
        }
        return t;
    }

    @Override
    public CbpsegmTurno registrarTurno(CbpsegmUsuario usuario) {
        CbpsegmTurno t = null;
        try {
            t = new CbpsegmTurno();
            t.setSegusuCodigo(usuario);
            t.setSegturMaquina(JsfUtil.getIpCliente());
            Integer numeroTurno = (Integer) entity.findObject("SQL", "SELECT NEXT VALUE FOR dbo.numero_turno", null, null, Boolean.TRUE);
            Long numeroTurnoMax = (Long) entity.findObject("HQL", "SELECT MAX(t.segturNumero) FROM CbpsegmTurno t", null, null, Boolean.TRUE);
            while(numeroTurnoMax.intValue()>=numeroTurno){
                numeroTurno = (Integer) entity.findObject("SQL", "SELECT NEXT VALUE FOR dbo.numero_turno", null, null, Boolean.TRUE);
            }
            t.setSegturNumero(numeroTurno.longValue());
            t = (CbpsegmTurno) entity.merge(t);
        } catch (HibernateException e) {
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
        }
        return t;
    }

    @Override
    public Boolean cerrarTurno(CbpsegmTurno turno) {
        Boolean cerrado = Boolean.FALSE;
        try {
            turno.setSegturCierre(new Date());
            turno.setSegturEstado(false);
            turno = (CbpsegmTurno) this.entity.merge(turno);
            if (turno != null && !turno.getSegturEstado()) {
                cerrado = Boolean.TRUE;
            }
            bitacora.saveActionLog(ActionsTransaccion.CIERRE_TURNO, null);
        } catch (HibernateException e) {
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
        }
        return cerrado;
    }

    /**
     *
     * @param tipo 1:GENP_PARAMCOMPANIA 2:GENP_PARAMOFICINA
     * @param genciaCodigo
     * @param oficinaCodigo
     * @param etiqueta
     * @return
     */
    @Override
    public Object getValorDefault(int tipo, String genciaCodigo, String oficinaCodigo, String etiqueta) {
        String query = "";
        try {
            if (tipo == 1) {
                query = "SELECT GENPCO_nombrevalor FROM GENP_PARAMCOMPANIA WHERE GENPCO_etiqueta='" + etiqueta + "' AND GENCIA_codigo='" + genciaCodigo + "'";
            } else if (tipo == 2) {
                query = "SELECT GENPOF_nombrevalor FROM GENP_PARAMOFICINA WHERE GENPOF_etiqueta='" + etiqueta + "' AND GENCIA_codigo='" + genciaCodigo + "' AND GENOFI_codigo='" + oficinaCodigo + "'";
            }
            return entity.findObject("SQL", query, null, null, Boolean.FALSE);
        } catch (HibernateException e) {
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
        }
        return null;
    }

    @Override
    public ValoresPago procesarValoresRegistrados(List<TempCbpPcUsuarioOpcion> listadoItem) {
        ValoresPago vp = new ValoresPago();
        try {
            listadoItem.forEach((i) -> {
                vp.actualizarValores(i, 1);
            });
            vp.actualizarValoresTotales();
        } catch (Exception e) {
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
        }
        return vp;
    }

    @Override
    public List<TempCbpCobUsuarioOpcion> consultarTablaTemporalCobro(String nombreTabla) {
        List<TempCbpCobUsuarioOpcion> listado = null;
        try {
            String resultado = (String) entity.findObject("SQL", "SELECT CAST(NAME AS VARCHAR(50)) AS NAME FROM SYSOBJECTS WHERE name = '" + nombreTabla + "'", null, null, Boolean.FALSE);
            if (resultado != null) {
                this.parametros = new HashMap<>();
                this.parametros.put("0", nombreTabla);
                listado = entity.findAllSQL("EXEC POS_CONSULTAR_COBRO_TEMPORAL ?", this.parametros, TempCbpCobUsuarioOpcion.class);
            }
        } catch (HibernateException e) {
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
        }
        return listado;
    }

    @Override
    public Integer insertarRegistroTablaTemporalCobro(String nombreTabla, TempCbpCobUsuarioOpcion rt) {
        Integer id = null;
        try {
            String query = "INSERT INTO " + nombreTabla + " (CXCDEU_valorpago,CXCDEU_vuelto,CXCDEU_tipopag,CXCDEU_tipoent,CXCDEU_codent,"
                    + "CXCDEU_numcuenta,CXCDEU_numcheque,CXCDEU_fechavenci,FACCOB_fechavalidez,FACCOB_titular,tipo_pago) OUTPUT Inserted.id_secuencia VALUES (?,?,?,?,?,?,?,?,?,?,?)";
            this.parametros = new HashMap<>();
            this.parametros.put("0", rt.getCxcdeuValorPago());
            this.parametros.put("1", rt.getCxcdeuVuelto());
            this.parametros.put("2", rt.getCxcdeuTipopag());
            this.parametros.put("3", rt.getCxcdeuTipoent());
            this.parametros.put("4", rt.getCxcdeuCodent());
            this.parametros.put("5", rt.getCxcdeuNumcuenta());
            this.parametros.put("6", rt.getCxcdeuNumcheque());
            this.parametros.put("7", rt.getCxcdeuFechavenci());
            this.parametros.put("8", rt.getFaccobFechavalidez());
            this.parametros.put("9", rt.getTitular());
            this.parametros.put("10", rt.getTipoPago());
            id = (Integer) entity.findObject("SQL", query, this.parametros, null, Boolean.TRUE);
        } catch (HibernateException e) {
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
        }
        return id;
    }

    @Override
    public Integer actualizarRegistroTablaTemporalCobro(String nombreTabla, TempCbpCobUsuarioOpcion rt) {
        Integer id = null;
        try {
            this.parametros = new HashMap<>();
            this.parametros.put("0", rt.getCxcdeuValorPago());
            this.parametros.put("1", rt.getIdSecuencia());
            id = (Integer) entity.findObject("SQL", "UPDATE " + nombreTabla + " SET CXCDEU_valorpago=? OUTPUT Inserted.id_secuencia WHERE id_secuencia=?", this.parametros, null, Boolean.TRUE);
        } catch (HibernateException e) {
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
        }
        return id;
    }

    @Override
    public Integer eliminarRegistroTablaTemporalCobro(String nombreTabla, TempCbpCobUsuarioOpcion rt) {
        Integer id = null;
        try {
            this.parametros = new HashMap<>();
            this.parametros.put("0", rt.getIdSecuencia());
            id = rt.getIdSecuencia();
            entity.executeProcedure("DELETE FROM " + nombreTabla + " WHERE id_secuencia=?", this.parametros);
        } catch (HibernateException e) {
            id = null;
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
        }
        return id;
    }

    @Override
    public ResultadoCobro registrarCobro(String nombreTabla, ValoresPago vp, Pago p, ResultadoFacturacion rf, GenmCaja caja) {
        ResultadoCobro rc = null;
        try {
            System.out.println(rf.getSecuencia() + "-" + rf.getNumero() + "-" + rf.getSecuenciaCxc());
            this.parametros = new HashMap<>();
            this.parametros.put("0", p.getValorCambioEfectivo());
            entity.executeProcedure("UPDATE " + nombreTabla + " SET CXCDEU_vuelto=? WHERE tipo_pago=1", this.parametros);
            entity.executeProcedure("UPDATE " + nombreTabla + " SET CXCDEU_tipopag=CXCFPG_codigo FROM CXCM_FORMAPAGO WHERE POS_tipopago=tipo_pago ", null);
            this.parametros = new HashMap<>();
            this.parametros.put("0", caja.getGENCIAcodigo());       this.parametros.put("1", caja.getGENOFIcodigo());
            this.parametros.put("2", nombreTabla);                  this.parametros.put("3", rf.getSecuenciaCxc());
            this.parametros.put("4", caja.getSEGUSUcodigo());       this.parametros.put("5", (Date) entity.findObject("SQL", "SELECT FACCVT_fechaemisi FROM FACT_CABVENTA WHERE FACCVT_numsecuenc=" + rf.getSecuencia(), null, null, Boolean.FALSE));
            this.parametros.put("6", "N");
            rc = (ResultadoCobro) entity.findObject("SQL", "EXECUTE SP_CXCT_CANCELAFACTURALIBRE ?,?,?,?,?,?,?", this.parametros, ResultadoCobro.class, Boolean.TRUE);
            bitacora.saveActionLog(ActionsTransaccion.REGISTRO_COBRO, rc.getDocumento());
        } catch (HibernateException e) {
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
            bitacora.saveActionLog(ActionsTransaccion.ERROR, "");
        }
        return rc;
    }

    @Override
    public Boolean eliminarTabla(String nombreTabla) {
        Boolean eliminacion = Boolean.FALSE;
        try {
            String resultado = (String) entity.findObject("SQL", "SELECT CAST(NAME AS VARCHAR(50)) AS NAME FROM SYSOBJECTS WHERE name = '" + nombreTabla + "'", null, null, Boolean.FALSE);
            if (resultado != null) {
                entity.executeProcedure("DROP TABLE " + nombreTabla, null);
            }
            eliminacion = Boolean.TRUE;
        } catch (HibernateException e) {
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
        }
        return eliminacion;
    }

    @Override
    public int secuenciaTransaccion(String genciaCodigo, String modulo) {
        int secuencia = 0;
        try {
            this.parametros = new HashMap<>();
            this.parametros.put("0", genciaCodigo);
            this.parametros.put("1", modulo);
            secuencia = (int) entity.findObject("SQL", "DECLARE @secuencia int EXECUTE SP_GENP_NUMSECTRAN ?,?, @secuencia OUT SELECT @secuencia", this.parametros, null, Boolean.TRUE);
        } catch (HibernateException e) {
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
        }
        return secuencia;
    }

    @Override
    public void aplicarPoliciticas(String nombreTabla, String codigoCompania, String codigoOficina, Boolean rvb, String facvdrCodigo) {
        try {
            if (rvb) {
                actualizarCodigoVendedorTemporal(nombreTabla, facvdrCodigo);
            }
            this.parametros = new HashMap<>();
            this.parametros.put("0", nombreTabla);
            this.parametros.put("1", codigoCompania);
            this.parametros.put("2", codigoOficina);
            entity.executeProcedure("EXECUTE CBPPTO_VTACALDESCTOTMP ?,?,?", this.parametros);
        } catch (HibernateException e) {
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    @Override
    public ResultadoFacturacion generarFactura(String tabla, GenmCaja caja, Date fecha, CbpsegmTurno turno, Boolean rvb, String facvdrCodigo) {
        ResultadoFacturacion r = null;
        try {
            if (rvb) 
                actualizarCodigoVendedorTemporal(tabla, facvdrCodigo);            
            int i = this.secuenciaTransaccion(caja.getGENCIAcodigo(), "FAC");
            System.out.println("TRANSACCION " + i);
            GenmCaja c = entity.find(GenmCaja.class, caja.getFacscaIdsececaja());
            this.parametros = new HashMap<>();
            this.parametros.put("0", tabla);                    this.parametros.put("1", caja.getGENCIAcodigo());
            this.parametros.put("2", caja.getGENOFIcodigo());   this.parametros.put("3", i);
            this.parametros.put("4", new SimpleDateFormat("yyyyMMdd").format(fecha));
            this.parametros.put("5", "PTO");                    this.parametros.put("6", "PTO VTA");
            this.parametros.put("7", caja.getSEGUSUcodigo());   this.parametros.put("8", turno.getSegturNumero().intValue());
            this.parametros.put("9", "WEB");                    this.parametros.put("10", c.getGentalSecuencia().getGENTALsecuencia().intValue());
            this.parametros.put("11", "FACT");                  this.parametros.put("12", c.getGENBODcodigo());
            r = (ResultadoFacturacion) entity.findObject("SQL", "EXEC SP_FACT_GENERARFACTURA_ESQUEMA3 ?,?,?,?,?,?,?,?,?,?,?,?,?", this.parametros, ResultadoFacturacion.class, Boolean.TRUE);
            bitacora.saveActionLog(ActionsTransaccion.REGISTRO_FACTURA, r.getNumero());
        } catch (HibernateException e) {
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
            bitacora.saveActionLog(ActionsTransaccion.ERROR, "generarFactura");
        }
        return r;
    }

    @Override
    public FactSesioncaja registrarSessionCaja(GenmCaja caja, CbpsegmTurno turno) {
        FactSesioncaja s = null;
        try {
            Date horaApertura = new Date();
            s = new FactSesioncaja(caja.getGENCIAcodigo(), caja.getGENOFIcodigo(), turno.getSegusuCodigo().getCodigoCaja(), horaApertura, turno.getSegturNumero().intValue());
            s.setSegusuCodigo(caja.getSEGUSUcodigo());
            s.setFACSCAhorainicio(horaApertura);
            s.setGenmonCodigo((String) this.getValorDefault(1, caja.getGENCIAcodigo(), null, "MONFUNC1"));
            s = (FactSesioncaja) entity.merge(s);
        } catch (HibernateException e) {
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
        }
        return s;
    }

    @Override
    public Boolean cerrarSessionCaja(FactSesioncaja sc) {
        Boolean cerrado = Boolean.FALSE;
        try {
            Date horaCierre = new Date();
            sc.setFACSCAfeccierre(horaCierre);
            sc.setFACSCAestado('C');
            sc.setFACSCAhorasalida(horaCierre);
            sc = (FactSesioncaja) this.entity.merge(sc);
            if (sc != null && sc.getFACSCAestado().equals('C')) {
                cerrado = Boolean.TRUE;
            }
        } catch (HibernateException e) {
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
        }
        return cerrado;
    }

    @Override
    public CbpgenrRegistroDenominacion registrarValoresCaja(CbpgenrRegistroDenominacion registro, List<CbpgenmDenominacion> denominaciones, CbpsegmTurno turno) {
        CbpgenrRegistroDenominacion r;
        String tipoRegistro="";
        try {
            registro.setGenrdeFecha(new Date());
            registro.setSegturCodigo(turno);
            r = (CbpgenrRegistroDenominacion) entity.merge(registro);
            denominaciones.stream().filter((d) -> (d.getTotal().compareTo(BigDecimal.ZERO) > 0)).forEach((de) -> {
                entity.merge(new CbpgenrDetregistroDenominacion(de.getCantidad(), de.getTotal(), de, r));
            });
            parametros = new HashMap<>();
            parametros.put("fACSCAidsececaja", turno.getSegturNumero().intValue());
            switch (registro.getGenrdeTipo().intValue()){
                case 1:
                    tipoRegistro="INGRESO:";
                    if(this.cantidadTransaccionesTurno(turno).equals(0L) && turno.getSegturAsiginicial().compareTo(BigDecimal.ZERO)<1){
                        turno.setSegturAsiginicial(registro.getGenrdeMonto());
                    }else{
                        turno.setSegturAdicional(turno.getSegturAdicional().add(registro.getGenrdeMonto()));
                    }
                    break;
                case 2:                    
                    tipoRegistro="EGRESO:";
                    turno.setSegturEgreso(turno.getSegturEgreso().add(registro.getGenrdeMonto()));
                    break;
                case 3:
                    tipoRegistro="CIERRE:";
                    turno.setSegturValorcierre(turno.getSegturValorcierre().add(registro.getGenrdeMonto()));
                    break;
                default:
                    break;
            }
            entity.merge(turno);
            bitacora.saveActionLog(ActionsTransaccion.ASIGNACION_VALORES, tipoRegistro+registro.getGenrdeMonto().toString());
        } catch (HibernateException e) {
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
            return null;
        }
        return r;
    }

    @Override
    public GenmCaja grabarCaja(GenmCaja c, CbpgenrCompania compania, CbpgenrOficina oficina) {
        GenmCaja caja = null;
        try {
            c.setGENCIAcodigo(compania.getGenciaSecuencia());
            c.setGENOFIcodigo(oficina.getGenofiSecuencia());
            c.setGENCAJcodigo(c.getGENCAJcodigo().toUpperCase());
            c.setGENCAJdescripcio(c.getGENCAJdescripcio().toUpperCase());
            caja = (GenmCaja) entity.merge(c);
        } catch (HibernateException e) {
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
        }
        return caja;
    }

    @Override
    public CbpgenrCobro grabaDetalleCobro(Facturacion rf, List<TempCbpCobUsuarioOpcion> lc) {
        CbpgenrCobro cobro = null;
        try {
            for (TempCbpCobUsuarioOpcion c : lc) {
                cobro = new CbpgenrCobro();
                cobro.setFacvtcSecuenvta(rf.getFactura().getSecuencia().longValue());
                cobro.setCxcddeNumsecuen(rf.getFactura().getSecuenciaCxc().longValue());
                cobro.setGencobTipo(c.getTipoPago());
                cobro.setGencobCambio(c.getCxcdeuVuelto());
                cobro.setGencobValor(c.getCxcdeuValorPago());
                cobro.setGencobCodent(c.getCxcdeuCodent()==null?null:c.getCxcdeuCodent().substring(0, c.getCxcdeuCodent().indexOf("-")));
                cobro.setGencobNumcuenta(c.getCxcdeuNumcuenta());
                cobro.setGencobNumcheque(c.getCxcdeuNumcheque());
                cobro.setCxcccrNumsecuen(rf.getCobro().getSecuencia());
                cobro.setCxcccrNumdocumcr(rf.getCobro().getDocumento());
                cobro.setFaccvtNumdocumen(Integer.valueOf(rf.getFactura().getNumero().trim()));
                cobro = (CbpgenrCobro) entity.merge(cobro);
            }
        } catch (HibernateException e) {
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
        }
        return cobro;
    }

    @Override
    public void reporteCaja(String usuario, Long turno) {
        try {
            this.parametros = new HashMap<>();
            this.parametros.put("0", usuario);
            this.parametros.put("1", turno.intValue());
            entity.executeProcedure("EXECUTE POS_REPORTES_CAJA ?,? ", this.parametros);
        } catch (HibernateException e) {
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    @Override
    public Boolean registroTransaccionStandby(String nombreTabla, CbpsegmTurno turno) {
        Boolean resultado = Boolean.FALSE;
        try {
            List<TempCbpPcUsuarioOpcion> l = this.consultarTablaTemporal(nombreTabla);
            if (l != null && !l.isEmpty()) {
                CbpposFacttemporal rt;
                CbpposCabfacttemporal ct = new CbpposCabfacttemporal();
                ct.setCbpfaccFecha(new Date());
                ct.setSegturNumero(turno.getSegturNumero());
                ct.setSegusuCodigo(turno.getSegusuCodigo().getSegusuCodigo());
                ct.setCbpfaccCliente(l.get(0).getCxccliCodigo());
                ct = (CbpposCabfacttemporal) this.entity.merge(ct);
                for (TempCbpPcUsuarioOpcion r : l) {
                    rt = new CbpposFacttemporal(r);
                    rt.setCbpfaccCodigo(ct);
                    this.entity.merge(rt);
                }
                resultado = Boolean.TRUE;
            }
            this.bitacora.saveActionLog(ActionsTransaccion.FACTURA_STANDBY, ""+resultado);
        } catch (HibernateException e) {
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
        }
        return resultado;
    }

    @Override
    public void cargarTransaccionesStandby(String nombreTabla, CbpposCabfacttemporal rt) {
        try {
            this.parametros = new HashMap<>();
            this.parametros.put("0", nombreTabla);
            this.parametros.put("1", rt.getCbpfaccCodigo());
            entity.executeProcedure("EXECUTE POS_CARGARTEMPORAL ?,? ", this.parametros);
            this.bitacora.saveActionLog(ActionsTransaccion.CARGA_STANDBY, ""+rt.getCbpfaccCodigo());
        } catch (HibernateException e) {
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public void actualizarCodigoVendedorTemporal(String nombreTabla, String codigoVendedor) {
        try {
            entity.executeProcedure("UPDATE " + nombreTabla + " SET FACVDR_codigo='" + codigoVendedor + "'", null);
        } catch (HibernateException e) {
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    @Override
    public List<TemDevolucionFactura> detalleFactura(Long idSecuenciaFact) {
        try {
            parametros = new HashMap<>();
            parametros.put("0", idSecuenciaFact);
            return entity.findAllSQL(QuerysPos.consultaDetalleFactura, parametros, TemDevolucionFactura.class);
        } catch (HibernateException e) {
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
            return null;
        }
    }

    @Override
    public Boolean eliminarDatosTempDevolucion(String tablaTemp) {
        try {
            if (!existeTableTemp(tablaTemp)) {
                return true;
            }
            return entity.executeProcedure("DROP TABLE " + tablaTemp, null);
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public List<TemDevolucionFactura> articulosADevolucion(String tablaTemp) {
        try {
            if (!existeTableTemp(tablaTemp)) {
                return null;
            }
            return entity.findAllSQL("SELECT * FROM " + tablaTemp, null, TemDevolucionFactura.class);
        } catch (HibernateException e) {
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
            return null;
        }
    }

    @Override
    public boolean existeTableTemp(String tabla) {
        parametros = new HashMap<>();
        parametros.put("0", tabla);
        Integer findObject = (Integer) entity.findObject("SQL", "SELECT COUNT(NAME) FROM SYSOBJECTS WHERE name = ?", parametros, null, Boolean.FALSE);
        return (findObject > 0);
    }

    @Override
    public void registrarDevolucionTemp(String tabla, FactCabvtacaja factura, List<TemDevolucionFactura> productosSeleccionados) {
        try {
            for (TemDevolucionFactura detFacDev : productosSeleccionados) {
                parametros = new HashMap<>();
                
                parametros.put("0", tabla);
                parametros.put("1", detFacDev.getGENCIA_codigo());
                parametros.put("2", detFacDev.getGENOFI_codigo());
                parametros.put("3", (session.getCaja().getGENBODcodigo() == null ? "" : session.getCaja().getGENBODcodigo()));
                parametros.put("4", detFacDev.getID_lineaFAC() + "");
                parametros.put("5", new SimpleDateFormat("yyyyMMdd").format(new Date()));
                parametros.put("6", detFacDev.getCXCCLI_codigo());
                parametros.put("7", detFacDev.getFACVDR_codigo());
                parametros.put("8", detFacDev.getCXCRUT_codigo());
                parametros.put("9", detFacDev.getINVITM_codigo());
                parametros.put("10", detFacDev.getINVITM_estaitem());
                parametros.put("11", detFacDev.getFACDVT_cantfuncional().toString());
                parametros.put("12", detFacDev.getPrecioImporte().toString());
                parametros.put("13", detFacDev.getsubTotalImporte().toString());
                parametros.put("14", detFacDev.getDescuentoImporte().toString());
                parametros.put("15", detFacDev.getImpuestoImporte().toString());
                parametros.put("16", detFacDev.getNetoImporte().toString());
                parametros.put("17", detFacDev.getFACDVT_listaprecio());
                Boolean executeProcedure = entity.executeProcedure(QuerysPos.devolucionTemp, parametros);
            }
        } catch (HibernateException e) {
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    @Override
    public String registarDevolucion(FactCabvtacaja factura, String motivoDev, String observaciones, String tabla, CbpsegmUsuario cbpsegmUsuario) {
        try {
            parametros = new HashMap<>();
            parametros.put("0", tabla);
            parametros.put("1", factura.getGENCIAcodigo());
            parametros.put("2", factura.getGENOFIcodigo());
            parametros.put("3", cbpsegmUsuario.getSegusuLogin());
            parametros.put("4", new Date());
            parametros.put("5", factura.getFacvtcSecuenvta());
            parametros.put("6", motivoDev == null ? "cambio" : motivoDev);
            parametros.put("7", observaciones == null ? "Sin Observaciones" : observaciones);
            parametros.put("8", session.getCaja().getGentalSecuenciaDev().getGENTALsecuencia());
//            parametros.put("9", 0);
//            entity.executeProcedure(QuerysPos.registrarDevolucion1, parametros);
            String numNotaCredito = (String) entity.findObject("SQL", QuerysPos.registrarDevolucion1, parametros, null, Boolean.TRUE);
            bitacora.saveActionLog(ActionsTransaccion.DEVOLUCION, numNotaCredito);
            return numNotaCredito;
        } catch (Exception e) {
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
            return null;
        }
    }

    @Override
    public Boolean anulacionFactura(FactCabvtacaja venta, GenmCaja caja) {
        Boolean anulacion= Boolean.FALSE;
        try {
            parametros= new HashMap<>();
            parametros.put("0", venta.getGENCIAcodigo());
            parametros.put("1", venta.getGENOFIcodigo());
            parametros.put("2", "PTO");
            parametros.put("3", venta.getFacvtcSecuenvta()+"");
            parametros.put("4", caja.getGentalSecuenciaDev().getGENTALsecuencia());
            parametros.put("5", caja.getSEGUSUcodigo());
            parametros.put("6", JsfUtil.getIpCliente());
            parametros.put("7", "N");
            parametros.put("8", new Date());
            parametros.put("9", "ANULACION POS");
            String mensaje= (String) entity.findObject("SQL", "DECLARE @mensaje VARCHAR(1000) EXECUTE SP_GENP_PROCESOANULARDOC ?,?,?,?,?,?,?,?,?,?,@mensaje OUT SELECT @mensaje", this.parametros,null, Boolean.TRUE);
            System.out.println("MENSAJE: "+mensaje);
            if(mensaje!=null){
                parametros= new HashMap<>();
                parametros.put("estado", "A");
                parametros.put("facvtcSecuenvta", venta.getFacvtcSecuenvta());
                List<CbpgenrCobro> cobros = entity.findAllByParameter(CbpgenrCobro.class, parametros,null);
                CbpgenrCobro cobro;
                for (CbpgenrCobro c : cobros) {
                    cobro = new CbpgenrCobro();
                    cobro.setFacvtcSecuenvta(c.getFacvtcSecuenvta());
                    cobro.setCxcddeNumsecuen(c.getCxcddeNumsecuen());
                    cobro.setGencobTipo(c.getGencobTipo());
                    cobro.setGencobCambio(BigDecimal.ZERO);
                    cobro.setGencobValor(c.getGencobValor().subtract(c.getGencobCambio()));
                    cobro.setEstado("I");
                    cobro = (CbpgenrCobro) entity.merge(cobro);
                }
                anulacion = Boolean.TRUE;
                bitacora.saveActionLog(ActionsTransaccion.ANULAR_FACTURA, mensaje);
            }
        } catch (HibernateException e) {
            bitacora.saveActionLog(ActionsTransaccion.ANULAR_FACTURA, e.getMessage());
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
        }
        return anulacion;
    }
    
    @Override
    public Long cantidadTransaccionesTurno(CbpsegmTurno turno){
        Long cantidad=0L;
        try{
            this.parametros = new HashMap<>();
            this.parametros.put("turno", turno.getSegturNumero().intValue());
            cantidad = (Long) this.entity.findObject("HQL", QuerysPos.countTransaciones, parametros, null, Boolean.FALSE);
        } catch (HibernateException e) {
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
        }
        return cantidad;
    }

    @Override
    public void actualizarDatosItem(String tabla, FactCabvtacaja factura, TemDevolucionFactura devolucion) {
        String sql = "UPDATE " + tabla + " SET ";
    }

    @Override
    public Boolean validarClaveSupervisor(GenmCaja caja, String claveSupervisor) {
        Boolean valida=Boolean.FALSE;
        try{
            this.parametros = new HashMap<>();
            this.parametros.put("genbodSecuencia", caja.getGENBODcodigo());
            CbpgenrBodega bodega= this.entity.findObject(CbpgenrBodega.class, parametros);
            this.parametros = new HashMap<>();
            this.parametros.put("0", bodega.getFacsupCodigo());
            String claveSupervisorBase= (String) this.entity.findObject("SQL", QuerysPos.claveSupervisor, this.parametros, null, Boolean.TRUE);
            valida= claveSupervisor.toUpperCase().equals(Utilidades.password("D", claveSupervisorBase));
        } catch (HibernateException e) {
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
        }
        return valida;
    }
    
    @Override
    public Boolean validarTransaccionSupervisor(CbpgenrBodega bodega, CbpsegmUsuario supervisor){
        Boolean valida=Boolean.FALSE;
        try{
            this.parametros = new HashMap<>();
            this.parametros.put("genbodCodigo", bodega);
            this.parametros.put("segusuCodigoSup", supervisor);
            this.parametros.put("genrconEstado", Boolean.TRUE);
            CbpgenrControltrans c = this.entity.findObject(CbpgenrControltrans.class, this.parametros);
            if(c==null){
                return Boolean.FALSE;
            } else{
                if(c.getGenrconActualizar())
                    return Boolean.FALSE;
                if(c.getGenrconPeriodo()){
                    int dias= Utilidades.diferenciaDias(c.getGenrconFechaact(), new Date());
                    System.out.println("DIAS "+dias);
                    if(dias<=c.getGenrconDias().intValue())
                        valida=Boolean.TRUE;
                }
                if(c.getGenrconTransaccion()){
                    valida=Boolean.FALSE;
                    this.parametros = new HashMap<>();
                    this.parametros.put("segusuCodigo", supervisor.getSegusuCodigo());
                    this.parametros.put("desde", c.getGenrconFechaact());
                    this.parametros.put("hasta", new Date());
                    Long cant = (Long) this.entity.findObject("HQL", "SELECT COUNT(l) FROM CbplogBitacora l WHERE l.segusuCodigo = :segusuCodigo AND l.cbpbitFecha BETWEEN :desde AND :hasta", parametros, null, Boolean.FALSE);
                    System.out.println("CANT "+cant);
                    if(cant<c.getGenrconCantidad().intValue())
                        valida=Boolean.TRUE;
                }
            }
        } catch (HibernateException e) {
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
        }
        return valida;
    }
    
    @Override
    public Boolean esSupervisor(CbpsegmUsuario usuario){
        Boolean supervisor=Boolean.FALSE;
        try{
            this.parametros = new HashMap<>();
            this.parametros.put("1", usuario.getSegusuLogin());
            String codigoSupervisor = (String)this.entity.findObject("SQL", QuerysPos.codigoSupervisor, this.parametros, null, Boolean.TRUE);
            supervisor= codigoSupervisor!=null;
        } catch (HibernateException e) {
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
        }        
        return supervisor;
    }
    
    @Override
    public Long idSupervisor(GenmCaja caja){
        CbpsegmUsuario u=null;
        try{                    
            this.parametros= new HashMap<>();
            this.parametros.put("0", caja.getGENBODcodigo());
            String codUsuSup = (String) this.entity.findObject("SQL","SELECT SEGUSU_codigo FROM FACR_SUPERVISOR WHERE SEGUSU_codigo IS NOT NULL AND FACSUP_codigo= (SELECT FACSUP_codigo FROM CBPGENR_BODEGA WHERE genbod_secuencia = ?)", this.parametros, null,Boolean.FALSE);
            if(codUsuSup!=null){
                this.parametros= new HashMap<>();
                this.parametros.put("segusuLogin", codUsuSup);
                u = this.entity.findObject(CbpsegmUsuario.class, this.parametros);
            }
        } catch (HibernateException e) {
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
        }        
        return u!=null?u.getSegusuCodigo():null;
    }
    
    @Override
    public Boolean generacionCodigoCliente(String genciaSecuencia){
        Boolean automatica=Boolean.TRUE;
        try{
            this.parametros = new HashMap<>();
            this.parametros.put("0", genciaSecuencia);
            String generacion = (String)this.entity.findObject("SQL", QuerysPos.tipoGeneracionCodigoCliente, this.parametros, null, Boolean.FALSE);
            automatica= "S".equalsIgnoreCase(generacion==null?"":generacion.trim());
        } catch (HibernateException e) {
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
        }        
        return automatica;
    }
    
    @Override
    public List<String> getCbpsegmOpcionListDefault(){
        Session sess = entity.getSession();
        Query q1 = sess.createQuery("SELECT DISTINCT o.segopcUrl FROM CbpsegmOpcion o WHERE o.segopcAcceso=0 AND o.segopcEstado=1 ");
        return q1.list();
    }
    
    @Override
    public List<String> getCbpsegmOpcionListSinTurno(){
        Session sess = entity.getSession();
        Query q1 = sess.createQuery("SELECT DISTINCT o.segopcUrl FROM CbpsegmOpcion o WHERE o.segopcTurno=0 AND o.segopcEstado=1 ");
        return q1.list();
    }
        
    @Override
    public List<String> getCbpsegmOpcionListByUser(CbpsegmUsuario user){
        Session sess = entity.getSession();
        Query q1 = sess.createQuery("SELECT DISTINCT p.segopcCodigo.segopcUrl FROM CbpsegmPerfil p WHERE p.seggppCodigo=:grupo AND p.segperEstado=:estado ");
        q1.setLong("grupo", user.getSeggppCodigo().getSeggppCodigo());
        q1.setBoolean("estado", true);
        return q1.list();
    }
    
    @Override
    public CbpgenrPoliticabase grabarPolitica(CbpgenrPoliticabase p, CbpsegmUsuario u, List<InvmItem> items, List<CbpgenrBodega> bodegas){
        CbpgenrPoliticabase politica;
        try{
            if(p.getGenpolCodigo()==null){
                p.setGenpolCrefecha(new Date());
                p.setGenpolCreusuario(u);
            }else{
                p.setGenpolEdifecha(new Date());
                p.setGenpolEdiusuario(u);
            }
            politica= (CbpgenrPoliticabase) this.entity.merge(p);                                    
            //BODEGAS
            CbpgenrPoliticabodega pbod;
            if(p.getCbpgenrPoliticabodegaCollection()==null || p.getCbpgenrPoliticabodegaCollection().isEmpty()){
                for (CbpgenrBodega bod : bodegas) {
                    pbod= new CbpgenrPoliticabodega(bod);
                    pbod.setGenpolCodigo(politica);
                    this.entity.merge(pbod);
                }
            }else{
                //ELIMINACION
                for (CbpgenrPoliticabodega pb : p.getCbpgenrPoliticabodegaCollection()) {
                    if(!bodegas.contains(pb.getGenbodCodigo())){
                        //this.entity.remove(pb);
                        pb.setGenpboEstado(Boolean.FALSE);
                        this.entity.merge(pb);
                    }
                }
                //AGREGAR
                this.parametros = new HashMap<>();
                this.parametros.put("politica", p);
                List<CbpgenrBodega> bodegasPolitica = this.entity.findAll(QuerysPos.bodegasPolitica, parametros);
                bodegas.removeAll(bodegasPolitica);
                for (CbpgenrBodega bod : bodegas) {
                    pbod= new CbpgenrPoliticabodega(bod);
                    pbod.setGenpolCodigo(politica);
                    this.entity.merge(pbod);
                }
            }
            //ITEMS
            CbpgenrPoliticaitem pitem;
            if(p.getCbpgenrPoliticaitemCollection()==null || p.getCbpgenrPoliticaitemCollection().isEmpty()){
                for (InvmItem item : items) {
                    pitem = new CbpgenrPoliticaitem(item.getInvmItemPK().getINVITMcodigo().trim());
                    pitem.setGenpolCodigo(politica);
                    pitem.setGenpitGrupo(item.getGrupo());
                    this.entity.merge(pitem);
                }      
            }else{
                //ELIMINACION
                for (CbpgenrPoliticaitem pi : p.getCbpgenrPoliticaitemCollection()) {
                    if(!items.contains(new InvmItem(p.getGenciaCodigo().getGenciaSecuencia(), pi.getINVITMcodigo().trim()))){
                        //this.entity.remove(pi);
                        pi.setGenpitEstado(Boolean.FALSE);
                        this.entity.merge(pi);
                    }
                }
                //AGREGAR
                this.parametros = new HashMap<>();
                this.parametros.put("politica", p);
                this.parametros.put("genciaSecuencia", p.getGenciaCodigo().getGenciaSecuencia());
                List<InvmItem> itemsPolitica = this.entity.findAll(QuerysPos.itemsPolitica, parametros);
                this.parametros = new HashMap<>();
                this.parametros.put("politica", p);
                List<CbpgenrPoliticaitem> politicaItems = this.entity.findAll(QuerysPos.politicaItems, parametros);
                for (CbpgenrPoliticaitem politicaItem : politicaItems) {
                    for (InvmItem item : items) {
                        if(politicaItem.getINVITMcodigo().trim().equalsIgnoreCase(item.getInvmItemPK().getINVITMcodigo().trim())){
                            politicaItem.setGenpitGrupo(item.getGrupo());
                            this.entity.merge(politicaItem);
                        }
                    }
                }
                items.removeAll(itemsPolitica);
                for (InvmItem itm : items) {
                    pitem= new CbpgenrPoliticaitem(itm.getInvmItemPK().getINVITMcodigo().trim());
                    pitem.setGenpolCodigo(politica);
                    pitem.setGenpitGrupo(itm.getGrupo());
                    this.entity.merge(pitem);
                }
            }
        } catch (Exception e) {
            Logger.getLogger(PuntoVentaService.class.getName()).log(Level.SEVERE, null, e);
            return null;
        }
        return politica;
    }
}
