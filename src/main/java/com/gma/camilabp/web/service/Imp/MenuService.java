/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.service.Imp;

import com.gma.camilabp.domain.CbpsegmModulo;
import com.gma.camilabp.domain.CbpsegmOpcion;
import com.gma.camilabp.domain.CbpsegmPaquete;
import com.gma.camilabp.domain.CbpsegmPerfil;
import com.gma.camilabp.domain.CbpsegmUsuario;
import com.gma.camilabp.domain.CbpsegrGrupousuario;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.service.MenuLocal;
import java.util.List;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.interceptor.Interceptors;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author desarrollo2
 */
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
@Singleton
public class MenuService implements MenuLocal{

    @EJB
    private EntityManagerLocal entityManager;
    
    @Override
    public List<CbpsegmPaquete> getCbpsegmPaqueteList(boolean activo) {
        Session sess = entityManager.getSession();
        Query q1 = sess.createQuery("SELECT p FROM CbpsegmPaquete p where p.segpaqEstado=:activo ORDER BY p.segpaqCodigo ASC");
        q1.setBoolean("activo", activo);
        return q1.list();
    }
    
    @Override
    public List<CbpsegmPaquete> getCbpsegmPaqueteListByUser(CbpsegmUsuario user){
        Session sess = entityManager.getSession();
        Query q1 = sess.createQuery("SELECT DISTINCT p.segopcCodigo.segpaqCodigo FROM CbpsegmPerfil p WHERE p.seggppCodigo=:grupo AND p.segperEstado=:estado AND p.segopcCodigo.segpaqCodigo.segpaqEstado=:estado");
        q1.setLong("grupo", user.getSeggppCodigo().getSeggppCodigo());
        q1.setBoolean("estado", true);
        return q1.list();
    }
    
    @Override
    public List<CbpsegmModulo> getCbpsegmModuloListByUser(CbpsegmPaquete paquete,CbpsegmUsuario user){
        Session sess = entityManager.getSession();
        Query q1 = sess.createQuery("SELECT DISTINCT p.segopcCodigo.segmodCodigo FROM CbpsegmPerfil p WHERE p.seggppCodigo=:grupo AND p.segperEstado=:estado AND p.segopcCodigo.segpaqCodigo=:paquete");
        q1.setLong("grupo", user.getSeggppCodigo().getSeggppCodigo());
        q1.setEntity("paquete", paquete);
        q1.setBoolean("estado", true);
        return q1.list();
    }
    
    @Override
    public List<CbpsegmOpcion> getCbpsegmOpcionListByUser(CbpsegmPaquete paquete,CbpsegmModulo modulo, CbpsegmUsuario user){
        Session sess = entityManager.getSession();
        Query q1 = sess.createQuery("SELECT DISTINCT p.segopcCodigo FROM CbpsegmPerfil p WHERE p.seggppCodigo=:grupo AND p.segperEstado=:estado AND p.segopcCodigo.segpaqCodigo=:paquete AND p.segopcCodigo.segmodCodigo=:modulo");
        q1.setLong("grupo", user.getSeggppCodigo().getSeggppCodigo());
        q1.setEntity("paquete", paquete);
        q1.setEntity("modulo", modulo);
        q1.setBoolean("estado", true);
        return q1.list();
    }
    
    @Override
    public List<CbpsegmOpcion> getCbpsegmOpcionListByGrupo(CbpsegrGrupousuario grupo){
        Session sess = entityManager.getSession();
        Query q1 = sess.createQuery("SELECT DISTINCT p.segopcCodigo FROM CbpsegmPerfil p WHERE p.seggppCodigo=:grupo");
        q1.setLong("grupo", grupo.getSeggppCodigo());
        return q1.list();
    }
    
    @Override
    public List<CbpsegmPerfil> getCbpsegmPerfilListByGrupo(CbpsegrGrupousuario grupo){
        Session sess = entityManager.getSession();
        Query q1 = sess.createQuery("SELECT p FROM CbpsegmPerfil p WHERE p.seggppCodigo=:grupo");
        q1.setLong("grupo", grupo.getSeggppCodigo());
        return q1.list();
    }
}
