/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.service;

import com.gma.camilabp.domain.CbpsegmTurno;
import com.gma.camilabp.domain.CbpsegmUsuario;
import com.gma.camilabp.domain.GenmCaja;
import com.gma.camilabp.model.consultas.AgrupacionInventario;
import com.gma.camilabp.model.consultas.Facturacion;
import com.gma.camilabp.model.consultas.SugerenciaCompraSecuencia;
import com.gma.camilabp.model.logica.Pago;
import com.gma.camilabp.model.logica.SugeridoCompra;
import com.gma.camilabp.model.logica.ValoresPago;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author userdb6
 */
@Local
public interface TransaccionalLocal {
    public Facturacion generarFacturaCobro(String tabla, GenmCaja caja,Date fecha, CbpsegmTurno turno, Boolean rvb, String facvdrCodigo, String nombreTabla, ValoresPago vp, Pago p);    
    public SugerenciaCompraSecuencia registrarSugerencia(String accion, CbpsegmUsuario usuario, SugeridoCompra sugeridoCompra, List<AgrupacionInventario> agrupaciones);
}
