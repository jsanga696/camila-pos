/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.service;

import java.util.List;
import java.util.Map;
import javax.ejb.Local;
import javax.persistence.EntityManager;
import org.hibernate.Session;

/**
 *
 * @author userdb6
 */
@Local
public interface EntityManagerLocal {
    
    public EntityManager getManager();

    public Session getSession();

    public <T extends Object> T find(Class<T> entity, Object p);

    public Object find(String query, Map<String, Object> params);

    public List findAll(String query, Map<String, Object> params);

    public <T> T findByParameter(Class entity, Map<String, Object> paramt);

    public <T> T findByParameterFull(Class entity, Map<String, Object> parameterEq, Map<String, Object> parameterNot);

    public <T extends Object> List<T> findAllByParameter(Class<T> entity, Map<String, Object> paramt);

    public Object merge(Object o);

    public Integer update(String query, Map<String, Object> params);

    public List findAll(String query, Map<String, Object> params, Integer min, Integer max);

    public Boolean deleteEntity(Object o);
    
    public <T> List<T> getNative2Alias(Class<T> clase, String query, Map<String, Object> params);
    
    public Object findObject(String tipoQuery, String query, Map<String, Object> parametros, Class modelo, Boolean requireTransaction);
    
    public <T> T findObject(Class<T> entity, Map<String, Object> parametros);
    
    public <T> List<T> findAllByParameter(Class entity, Map<String, Object> parametros, String order);
    
    public <T extends Object> List<T> findListByParameters(String query, String[] par, Object[] val);
    
    public <T extends Object> List<T> findAll(Class<T> entity);
    
    public List findAllSQL(String query, Map<String, Object> parametros, Class modelo);
    
    public Boolean executeProcedure(String sqlProcedure, Map<String, Object> parametros);
    
    public Integer executeSQL(String query, Map<String, Object> parametros);
    
    public <T> List<T> findAllByParameterAndParameterIlike(Class entity, Map<String, Object> parametros, Map<String, Object> parametrosString);
            
}
