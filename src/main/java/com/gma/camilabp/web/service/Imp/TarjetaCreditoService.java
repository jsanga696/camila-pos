/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.service.Imp;

import com.gma.camilabp.model.consultas.IngresoBodegaDetModel;
import com.gma.camilabp.model.consultas.IngresoBodegaModel;
import com.gma.camilabp.model.consultas.IngresoRegDepoCheque;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.service.TarjetaCreditoLocal;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.interceptor.Interceptors;

/**
 *
 * @author userdb5
 */
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
@Singleton
public class TarjetaCreditoService implements TarjetaCreditoLocal, Serializable{
    
    @EJB
    private EntityManagerLocal entity;

    @Override
    public Boolean guardarTarjetaCredito(IngresoRegDepoCheque ingreso, HashMap datosAdicionales) {
        try{
            Map<String, Object> parametros;
            Integer i = 1;
            parametros = new HashMap<>();
            parametros.put("0", ingreso.getGENCIA_codigo());
            parametros.put("1", ingreso.getGENOFI_codigo());
            parametros.put("2", ingreso.getFECHA_REGISTRO());
            parametros.put("3", ingreso.getSEGUSU_codigo());
            parametros.put("4", ingreso.getSEGNAQ_codigo());
            parametros.put("5", ingreso.getBANENT_codigo());
            parametros.put("6", ingreso.getBANCTA_codigo());
            parametros.put("7", ingreso.getNUM_PAPELETA());
            parametros.put("8", ingreso.getNAMETABLE());
            
            this.entity.executeProcedure("EXECUTE SP_CXCP_REGDEPOTC ?,?,?,?,?,?,?,?,?", parametros);
            
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }
    
}
