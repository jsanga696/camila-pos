/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.service;

import com.gma.camilabp.domain.CbpgenpParamcompania;
import com.gma.camilabp.domain.CbpgenpParamoficina;
import com.gma.camilabp.domain.CbpgenrBodega;
import com.gma.camilabp.domain.CbpgenrOficina;
import com.gma.camilabp.domain.CbpgenrUsuarioxofi;
import com.gma.camilabp.domain.CbpsegmFuncion;
import com.gma.camilabp.domain.CbpsegmOpcion;
import com.gma.camilabp.domain.CbpsegmPaquete;
import com.gma.camilabp.domain.CbpsegmPerfilrestricion;
import com.gma.camilabp.domain.CbpsegmUsuario;
import com.gma.camilabp.domain.CbpsegrGrupousuario;
import com.gma.camilabp.domain.CxpmProveedor;
import com.gma.camilabp.domain.GenpParamgenerales;
import com.gma.camilabp.domain.InvmItem;
import com.gma.camilabp.web.parametros.general.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author desarrollo2
 */
@Local
public interface GeneralLocal {
    void saveGenpParamcompania(CbpgenpParamcompania genParaCompania);
    void saveGenpParamoficina(CbpgenpParamoficina genParaOficina);
    //void saveMantMotiInventario(InvrMotvajuste motivo);
    List<CbpgenpParamcompania> getGeneParaCompa();
    List<PerfilRestriccion> getPerfilRestriccionList();
    List<CbpsegmFuncion> getSegmFuncionList();
    List<CbpsegmPaquete> getSegmPaqueteList();
    List<CbpsegmOpcion> getSegmOpcionList();
    void duplicarPerfiles(List<CbpsegmPerfilrestricion> segmPerfilrestricionBaseList,CbpsegrGrupousuario grupoUsuario);
    List<CbpsegmPerfilrestricion> getSegmPerfilrestricionList(Long segrGrupoUsuarioId);
    void asignarPerfiles(List<CbpsegmPerfilrestricion> segmPerfilrestricionBaseList,CbpsegrGrupousuario grupoUsuario);
    List<CbpsegmPerfilrestricion> getSegmPerfilrestricionList2(Long segrGrupoUsuarioId);
    CbpsegmOpcion getSegmOpcion(Long opcionCodigoId);
    void saveSegmPerfilrestricion(Long opcionId, CbpsegrGrupousuario grupoUsuario);
    void deleteSegmPerfilrestricion(CbpsegmPerfilrestricion segmPerfilrestricion);
    List<CbpgenrUsuarioxofi> getGenrUsuarioxofi(long idOficina,long idBodega);
    void saveGenrUsuarioxofi(List<CbpsegmUsuario> segmUsuarioList, long idOficina, long idBodega);
    List<GenpParamgenerales> obtenerParametrosGeneralesPorCodigo(String cod);
    
    //POS
    Integer numeroDocumento(String compania, String oficina, String modulo, String documento);
    BigDecimal secuenciaModulo(String modulo, String secuenciaCompania);
    Integer numeroDocumentoCompra(String compania, String oficina, String documento);
    CbpgenrOficina consultarOficina(String secuenciaOficina);
    CbpgenrBodega consultarBodega(String secuenciaBodega);
    CxpmProveedor consultarProveedor(String secuenciaCompania, String secuenciaOficina, String secuenciaProveedor);
    BigDecimal valorImpuestoItem(String codItem, String codCompania, BigDecimal precio, Date fecha);
    InvmItem registrarItem(InvmItem item, CbpsegmUsuario usuario);
}
