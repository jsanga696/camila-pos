/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.service;

import com.gma.camilabp.domain.CbpsegmModulo;
import com.gma.camilabp.domain.CbpsegmOpcion;
import com.gma.camilabp.domain.CbpsegmPaquete;
import com.gma.camilabp.domain.CbpsegmPerfil;
import com.gma.camilabp.domain.CbpsegmUsuario;
import com.gma.camilabp.domain.CbpsegrGrupousuario;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author desarrollo2
 */
@Local
public interface MenuLocal {
    List<CbpsegmPaquete> getCbpsegmPaqueteList(boolean activo);
    List<CbpsegmPaquete> getCbpsegmPaqueteListByUser(CbpsegmUsuario user);
    List<CbpsegmModulo> getCbpsegmModuloListByUser(CbpsegmPaquete paquete,CbpsegmUsuario user);
    List<CbpsegmOpcion> getCbpsegmOpcionListByUser(CbpsegmPaquete paquete,CbpsegmModulo modulo, CbpsegmUsuario user);
    public List<CbpsegmOpcion> getCbpsegmOpcionListByGrupo(CbpsegrGrupousuario grupo);
    public List<CbpsegmPerfil> getCbpsegmPerfilListByGrupo(CbpsegrGrupousuario grupo);
}
