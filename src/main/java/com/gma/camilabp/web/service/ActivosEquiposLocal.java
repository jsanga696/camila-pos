/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.service;

import com.gma.camilabp.domain.CbpactEquipo;
import com.gma.camilabp.domain.CbpactModelEquipo;
import com.gma.camilabp.domain.CbpactRangosCliente;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author userdb5
 */
@Local
public interface ActivosEquiposLocal {
    
    public Boolean verificarEquipos(CbpactModelEquipo model, List<CbpactEquipo> equiposList);
    
    public Boolean guardarEquipos(CbpactModelEquipo model, List<CbpactEquipo> equiposList);
    
    public Boolean verificarRangos(List<CbpactRangosCliente> nuevo);
    
    public Boolean guardarRangos(List<CbpactRangosCliente> anterior, List<CbpactRangosCliente> nuevo);
    
    /*public String getNombreProveedor(String cxpmProveedor, Long oficina);
    
    public String getNombreCliente(String cxcmCliente, Long oficina);*/
    
}
