/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.service.Imp;

import com.gma.camilabo.enums.ActionsTransaccion;
import com.gma.camilabp.domain.CbplogBitacora;
import com.gma.camilabp.web.service.BitacoraLocal;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.session.UserSession;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.inject.Inject;

/**
 *
 * @author userdb6
 */
@Singleton
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
public class BitacoraService implements BitacoraLocal{
    @EJB
    private EntityManagerLocal entity;
    @Inject
    private UserSession userSession;
    
    @Override
    public void saveActionLog(ActionsTransaccion action, String detalle) {
        CbplogBitacora registro = new CbplogBitacora();
        try{
            registro.setCbpbitAccion(action.getAction());
            registro.setCbpbitFecha(new Date());
            registro.setGencajCodigo(userSession.getCaja()!=null?userSession.getCaja().getGENCAJcodigo():null);
            registro.setSegturNumero(userSession.getTurno()!=null?userSession.getTurno().getSegturNumero():null);
            registro.setSegusuCodigo(userSession.getCbpsegmUsuario().getSegusuCodigo());
            registro.setCbpbitCodaccion(action.getCode());
            registro.setCbpbitDetalle(detalle);
            entity.merge(registro);
        } catch (Exception e) {
            Logger.getLogger(BitacoraService.class.getName()).log(Level.SEVERE, null, e);        
        }
    }
    
    @Override
    public void saveActionLog(ActionsTransaccion action, String detalle, Long usuarioTransaccion) {
        CbplogBitacora registro = new CbplogBitacora();
        try{
            registro.setCbpbitAccion(action.getAction());
            registro.setCbpbitFecha(new Date());
            registro.setGencajCodigo(userSession.getCaja()!=null?userSession.getCaja().getGENCAJcodigo():null);
            registro.setSegturNumero(userSession.getTurno()!=null?userSession.getTurno().getSegturNumero():null);
            registro.setSegusuCodigo(usuarioTransaccion);
            registro.setCbpbitCodaccion(action.getCode());
            registro.setCbpbitDetalle(detalle);
            entity.merge(registro);
        } catch (Exception e) {
            Logger.getLogger(BitacoraService.class.getName()).log(Level.SEVERE, null, e);        
        }
    }
    
}
