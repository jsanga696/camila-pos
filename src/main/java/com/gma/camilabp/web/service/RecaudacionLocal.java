/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.service;

import com.gma.camilabp.domain.CbpgenrRecaudacion;
import com.gma.camilabp.domain.CbpsegmTurno;
import com.gma.camilabp.domain.GenmCaja;
import javax.ejb.Local;

/**
 *
 * @author Acer
 */
@Local
public interface RecaudacionLocal {
    
    public CbpgenrRecaudacion ingresoValor(CbpgenrRecaudacion r, GenmCaja c, CbpsegmTurno t);
    public CbpgenrRecaudacion egresoValor(CbpgenrRecaudacion r, GenmCaja c, CbpsegmTurno t);
    public CbpgenrRecaudacion transferenciaValor(CbpgenrRecaudacion r, GenmCaja c, CbpsegmTurno t);
}
