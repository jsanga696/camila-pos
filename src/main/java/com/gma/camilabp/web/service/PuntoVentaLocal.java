/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.service;

import com.gma.camilabp.domain.CbpgenmDenominacion;
import com.gma.camilabp.domain.CbpgenrBodega;
import com.gma.camilabp.domain.CbpgenrCobro;
import com.gma.camilabp.domain.CbpgenrCompania;
import com.gma.camilabp.domain.CbpgenrOficina;
import com.gma.camilabp.domain.CbpgenrPoliticabase;
import com.gma.camilabp.domain.CbpgenrPoliticabodega;
import com.gma.camilabp.domain.CbpgenrPoliticaitem;
import com.gma.camilabp.domain.CbpgenrRegistroDenominacion;
import com.gma.camilabp.domain.CbpposCabfacttemporal;
import com.gma.camilabp.domain.CbpsegmTurno;
import com.gma.camilabp.domain.CbpsegmUsuario;
import com.gma.camilabp.domain.CxcmCliente;
import com.gma.camilabp.domain.FactCabvtacaja;
import com.gma.camilabp.domain.FactSesioncaja;
import com.gma.camilabp.domain.GenmCaja;
import com.gma.camilabp.domain.InvmItem;
import com.gma.camilabp.domain.InvmItemxbodega;
import com.gma.camilabp.model.consultas.Facturacion;
import com.gma.camilabp.model.consultas.ResultadoCobro;
import com.gma.camilabp.model.consultas.ResultadoFacturacion;
import com.gma.camilabp.model.consultas.TemDevolucionFactura;
import com.gma.camilabp.model.consultas.TempCbpCobUsuarioOpcion;
import com.gma.camilabp.model.consultas.TempCbpPcUsuarioOpcion;
import com.gma.camilabp.model.logica.Pago;
import com.gma.camilabp.model.logica.ValoresPago;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author userdb6
 */
@Local
public interface PuntoVentaLocal {
    public String validarIdentificacion(String tipo, String identificacion);
    public Boolean existeCodigoCLiente(String codigoCompania, String codigoOficina, String codigoCliente);
    public String grabarCliente(CxcmCliente c, GenmCaja caja, Boolean generaCodigo);
    public List<CxcmCliente> consultarCliente(String codigoCliente, String identificacionCliente, String razonSocial);
    public String secuenciaDocumento(String genciaCodigo, int idGenpTalonario, String tipoEjecucion);
    public Boolean registroTablaTemporal(String procedimiento, String nombreTabla);
    public Integer insertarRegistroTablaTemporal(String nombreTabla, TempCbpPcUsuarioOpcion rt);
    public List<TempCbpPcUsuarioOpcion> consultarTablaTemporal(String nombreTabla);
    public TempCbpPcUsuarioOpcion consultarTemporal(String nombreTabla, Integer codigo);
    public void borrarTemporal(String nombreTabla, Integer codigo, String codigoItem, GenmCaja caja);
    public void borrarTemporalPorTipoPago(String nombreTabla, Integer tipo);
    public String consultarCodigoClienteTemporal(String nombreTabla);
    public void actualizarCodigoClienteTemporal(String nombreTabla, String codigoCliente, String codigoCompania, String codigoOficina, String listaPrecio);
    public List<InvmItem> consultarItem(String codigoItem, String nombreItem, String codigoBarra);
    public List<InvmItemxbodega> consultarItemBodega(String codigoItem, String nombreItem, String codigoBarra);
    public CbpsegmTurno consultarTurno(CbpsegmUsuario usuario);
    public CbpsegmTurno registrarTurno(CbpsegmUsuario usuario);
    public Boolean cerrarTurno(CbpsegmTurno turno);
    public Object getValorDefault(int tipo,String genciaCodigo, String oficinaCodigo, String etiqueta);
    public ValoresPago procesarValoresRegistrados(List<TempCbpPcUsuarioOpcion> listadoItem);
    public List<TempCbpCobUsuarioOpcion> consultarTablaTemporalCobro(String nombreTabla);
    public Integer insertarRegistroTablaTemporalCobro(String nombreTabla, TempCbpCobUsuarioOpcion rt);
    public Integer actualizarRegistroTablaTemporalCobro(String nombreTabla, TempCbpCobUsuarioOpcion rt);
    public Integer eliminarRegistroTablaTemporalCobro(String nombreTabla, TempCbpCobUsuarioOpcion rt);
    public ResultadoCobro registrarCobro(String nombreTabla, ValoresPago vp, Pago p, ResultadoFacturacion rf, GenmCaja caja);
    public Boolean eliminarTabla(String nombreTabla);
    public int secuenciaTransaccion(String genciaCodigo, String modulo);
    public void aplicarPoliciticas(String nombreTabla, String codigoCompania, String codigoOficina, Boolean rvb, String facvdrCodigo);
    public ResultadoFacturacion generarFactura(String tabla, GenmCaja caja,Date fecha, CbpsegmTurno turno, Boolean rvb, String facvdrCodigo);
    public FactSesioncaja registrarSessionCaja(GenmCaja caja, CbpsegmTurno turno);
    public Boolean cerrarSessionCaja(FactSesioncaja sc);
    public CbpgenrRegistroDenominacion registrarValoresCaja(CbpgenrRegistroDenominacion registro, List<CbpgenmDenominacion> denominaciones, CbpsegmTurno turno);
    public GenmCaja grabarCaja(GenmCaja c, CbpgenrCompania compania, CbpgenrOficina oficina);
    public CbpgenrCobro grabaDetalleCobro(Facturacion rf, List<TempCbpCobUsuarioOpcion> lc);
    public void reporteCaja(String usuario, Long turno);
    public Boolean registroTransaccionStandby(String nombreTabla, CbpsegmTurno turno);
    public void cargarTransaccionesStandby(String nombreTabla, CbpposCabfacttemporal rt);

    public List<TemDevolucionFactura> detalleFactura(Long idSecuenciaFact);

    public Boolean eliminarDatosTempDevolucion(String tablaTemp);
    public List<TemDevolucionFactura> articulosADevolucion(String tablaTemp);

    public void registrarDevolucionTemp(String tabla, FactCabvtacaja factura, List<TemDevolucionFactura> productosSeleccionados);

    public boolean existeTableTemp(String tabla);

    public String registarDevolucion(FactCabvtacaja factura, String motivoDev, String observaciones, String tablaTemp, CbpsegmUsuario cbpsegmUsuario);

    public void actualizarDatosItem(String tabla, FactCabvtacaja factura, TemDevolucionFactura devolucion);
    public Boolean anulacionFactura(FactCabvtacaja venta, GenmCaja caja);
    public Long cantidadTransaccionesTurno(CbpsegmTurno turno);
    public Boolean validarClaveSupervisor(GenmCaja caja, String claveSupervisor);
    public Boolean validarTransaccionSupervisor(CbpgenrBodega bodega, CbpsegmUsuario supervisor);
    public Boolean esSupervisor(CbpsegmUsuario usuario);
    public Long idSupervisor(GenmCaja caja);
    public Boolean generacionCodigoCliente(String genciaSecuencia);
    public List<String> getCbpsegmOpcionListDefault();
    public List<String> getCbpsegmOpcionListSinTurno();
    public List<String> getCbpsegmOpcionListByUser(CbpsegmUsuario user);
    public CbpgenrPoliticabase grabarPolitica(CbpgenrPoliticabase p, CbpsegmUsuario u, List<InvmItem> items, List<CbpgenrBodega> bodegas);
}
