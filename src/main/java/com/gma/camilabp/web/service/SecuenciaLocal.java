/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.service;

import com.gma.camilabp.domain.storedprocedure.UsuarioPrueba;
import java.math.BigInteger;
import java.util.List;
import javax.ejb.Local;
import org.hibernate.Session;

/**
 *
 * @author desarrollo2
 */
@Local
public interface SecuenciaLocal {
    String getSecuencia(Session sess,String tabla, BigInteger compania, BigInteger oficina, BigInteger bodega, BigInteger modulo, BigInteger tipoDoc);
    List<UsuarioPrueba> getUsuarios();
}
