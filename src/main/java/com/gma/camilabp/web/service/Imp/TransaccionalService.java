/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.service.Imp;

import com.gma.camilabo.enums.ActionsTransaccion;
import com.gma.camilabp.domain.CbpsegmTurno;
import com.gma.camilabp.domain.CbpsegmUsuario;
import com.gma.camilabp.domain.GenmCaja;
import com.gma.camilabp.model.consultas.AgrupacionInventario;
import com.gma.camilabp.model.consultas.Facturacion;
import com.gma.camilabp.model.consultas.InveRepdiasInventario;
import com.gma.camilabp.model.consultas.ResultadoCobro;
import com.gma.camilabp.model.consultas.ResultadoFacturacion;
import com.gma.camilabp.model.consultas.SugerenciaCompraSecuencia;
import com.gma.camilabp.model.logica.Pago;
import com.gma.camilabp.model.logica.SugeridoCompra;
import com.gma.camilabp.model.logica.ValoresPago;
import com.gma.camilabp.web.service.BitacoraLocal;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.service.TransaccionalLocal;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.interceptor.Interceptors;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

/**
 *
 * @author userdb6
 */
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
@Singleton
public class TransaccionalService implements TransaccionalLocal {

    @EJB
    private EntityManagerLocal entity;
    @EJB
    private BitacoraLocal bitacora;

    @Override
    public Facturacion generarFacturaCobro(String tabla, GenmCaja caja, Date fecha, CbpsegmTurno turno, Boolean rvb, String facvdrCodigo, String nombreTabla, ValoresPago vp, Pago p) {
        Facturacion f= null;
        ResultadoFacturacion r = null;
        Map<String, Object> parametros;
        try {
            Session sess = entity.getSession();
            if (rvb) {
                Query q = sess.createSQLQuery("UPDATE " + tabla + " SET FACVDR_codigo='" + facvdrCodigo + "'");
                q.executeUpdate();
            }

            Query q1 = sess.createSQLQuery("DECLARE @secuencia int EXECUTE SP_GENP_NUMSECTRAN '" + caja.getGENCIAcodigo() + "', 'FAC', @secuencia OUT SELECT @secuencia");
            int i = (int) q1.uniqueResult();
            System.out.println("TRANSACCION " + i);

            GenmCaja c = (GenmCaja) sess.get(GenmCaja.class, caja.getFacscaIdsececaja());
            parametros = new HashMap<>();
            parametros.put("0", tabla);
            parametros.put("1", caja.getGENCIAcodigo());
            parametros.put("2", caja.getGENOFIcodigo());
            parametros.put("3", i);
            parametros.put("4", new SimpleDateFormat("yyyyMMdd").format(fecha));
            parametros.put("5", "PTO");
            parametros.put("6", "PTO VTA");
            parametros.put("7", caja.getSEGUSUcodigo());
            parametros.put("8", turno.getSegturNumero().intValue());
            parametros.put("9", "WEB");
            parametros.put("10", c.getGentalSecuencia().getGENTALsecuencia().intValue());
            parametros.put("11", "FACT");
            parametros.put("12", c.getGENBODcodigo());
            Query q2 = sess.createSQLQuery("EXEC SP_FACT_GENERARFACTURA_ESQUEMA3 ?,?,?,?,?,?,?,?,?,?,?,?,?");
            parametros.entrySet().forEach((entry) -> {
                q2.setParameter(Integer.parseInt(entry.getKey()), entry.getValue());
            });
            q2.setResultTransformer(Transformers.aliasToBean(ResultadoFacturacion.class));
            r = (ResultadoFacturacion) q2.uniqueResult();
            System.out.println(r.getSecuencia() + "-" + r.getNumero() + "-" + r.getSecuenciaCxc());
            
            ResultadoCobro rc = null;
            Query q3 = sess.createSQLQuery("UPDATE " + nombreTabla + " SET CXCDEU_vuelto=" + p.getValorCambioEfectivo() + " WHERE tipo_pago=1");
            q3.executeUpdate();
            Query q4 = sess.createSQLQuery("UPDATE " + nombreTabla + " SET CXCDEU_tipopag=CXCFPG_codigo FROM CXCM_FORMAPAGO WHERE POS_tipopago=tipo_pago ");
            q4.executeUpdate();

            Query qd = sess.createSQLQuery("SELECT FACCVT_fechaemisi FROM FACT_CABVENTA WHERE FACCVT_numsecuenc=" + r.getSecuencia());
            Date d = (Date) qd.uniqueResult();
            parametros = new HashMap<>();
            parametros.put("0", caja.getGENCIAcodigo());
            parametros.put("1", caja.getGENOFIcodigo());
            parametros.put("2", nombreTabla);
            parametros.put("3", r.getSecuenciaCxc());
            parametros.put("4", caja.getSEGUSUcodigo());
            parametros.put("5", d);
            parametros.put("6", "N");
            Query q5 = sess.createSQLQuery("EXECUTE SP_CXCT_CANCELAFACTURALIBRE ?,?,?,?,?,?,?");
            parametros.entrySet().forEach((entry) -> {
                q5.setParameter(Integer.parseInt(entry.getKey()), entry.getValue());
            });
            q5.setResultTransformer(Transformers.aliasToBean(ResultadoCobro.class));
            rc = (ResultadoCobro) q5.uniqueResult();

            System.out.println("DOC:" + rc.getDocumento() + " SEC:" + rc.getSecuencia());
            f = new Facturacion(r, rc);
            bitacora.saveActionLog(ActionsTransaccion.REGISTRO_COBRO, rc.getDocumento());
            bitacora.saveActionLog(ActionsTransaccion.REGISTRO_FACTURA, r.getNumero());
        } catch (HibernateException e) {
            Logger.getLogger(TransaccionalService.class.getName()).log(Level.SEVERE, null, e);
            bitacora.saveActionLog(ActionsTransaccion.ERROR, "generarFactura");
            return null;
        }
        return f;
    }

    @Override
    public SugerenciaCompraSecuencia registrarSugerencia(String accion, CbpsegmUsuario usuario, SugeridoCompra sugeridoCompra, List<AgrupacionInventario> agrupaciones) {
        try {
            BigDecimal monto = BigDecimal.ZERO;
            // Suma de todos los valores por agrupacion
            if(agrupaciones!=null){
                for (AgrupacionInventario agrupacion : agrupaciones) {
                    monto = monto.add(agrupacion.getINVITM_ventapromedioQ());
                }
            }
            Session sess = entity.getSession();
            // Ingreso de Cabezera de sugerencia de pedido
            String spGrabarSugerencia = "DECLARE @COMPDS_secuencia1 INT, @COMPDS_numero1 INT "
                    + " EXEC SP_COMT_CABPEDSUGERIDO ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,@COMPDS_secuencia1 OUTPUT,@COMPDS_numero1 OUTPUT, ? "
                    + " SELECT @COMPDS_secuencia1 secuencia, @COMPDS_numero1 numero";
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("0", usuario.getGenciaCodigo().getGenciaSecuencia());//@GENCIA_codigo = " & Repr(gstrCodCompania)  -- codigo de compañia
            parametros.put("1", sugeridoCompra.getOficina().getGenofiSecuencia()); //@GENOFI_codigo = " & Repr(TxtOficina.Text)  -- codigo de la oficina
            parametros.put("2", "");            //@COMPDS_descripcion = " & Repr("")   -----blanco
            parametros.put("3", monto);         //@COMPDS_monto = " & Repr(LblTotalPDS.Caption) --- total del valor del sugerido
            parametros.put("4", new Date());  //@COMPDS_fechaemisi = " & Repr(LblFecCorte.Caption)   ---fecha de documento
            parametros.put("5", usuario.getSegusuLogin());  //@SEGUSU_codigo = " & Repr(gstrCodUsuario) -- codigo de usuario que esta generando el documento
            parametros.put("6", (sugeridoCompra.getBodega().getGenbodSecuencia()));  //@COMPDS_codbodega     = " & Repr(gfstrEntreCar(CboBodega.Text, "<", ">")) ----codigo de la bodega
            parametros.put("7", sugeridoCompra.getDias());  //@COMPDS_diasprocesado = " & Trim$(gfstrUnFormat(TxtDias.Text)) -- dias ingresados
            parametros.put("8", sugeridoCompra.getAgrupadoPor());  //@COMPDS_nivelcons     = " & Repr(strAgrupa) -- seleccion del combo`` de agrupaccion
            parametros.put("9", sugeridoCompra.getFiltroNivel());  //@COMPDS_nivelfiltro   = " & Repr(strNivelFiltro)  -- seleccion del combo de nivel del filtro 1
            parametros.put("10", sugeridoCompra.getSecuenciaFiltro());  //@COMPDS_entidfiltro   = " & Repr(strEntidadFiltro)-- seleccion del elemento del filtro 1
            parametros.put("11", "");  //@COMPDS_rutafile      = " & Repr(TxtRutaFile.Text) --- en blanco
            parametros.put("12", accion);   //@COMPDS_rutafile   = " & Repr(TxtRutaFile.Text) --- en blanco
            parametros.put("13", 0);        //@COMPDS_secuenciag = " & Trim$(LblNumeroDocumento.Tag) --- secuencial del documento (cuando es nuevo, va el sugerido, si es actualizacion es el real)
            parametros.put("14", 0);        //@COMPDS_numerog    = " & Trim$(LblNumeroDocumento.Caption) --- numero del documento  (cuando es nuevo, va el sugerido, si es actualizacion es el real)
            parametros.put("15", sugeridoCompra.getProveedor().getCxpmProveedorPK().getCXPPROcodigo());   //@CXPPRO_codigo        = " & Trim$(TxtCodProveedor.Text) --- codigo de proveedor
            Query qGrabarSug = sess.createSQLQuery(spGrabarSugerencia);
            parametros.entrySet().forEach((entry) -> {
                qGrabarSug.setParameter(Integer.parseInt(entry.getKey()), entry.getValue());
            });
            qGrabarSug.setResultTransformer(Transformers.aliasToBean(SugerenciaCompraSecuencia.class));
            SugerenciaCompraSecuencia sugPedido = (SugerenciaCompraSecuencia) qGrabarSug.uniqueResult();
            // Detalle de la sugerencia de pedido
            sess.flush();
            String insertDetSegPedido = "INSERT INTO COMT_DETPEDSUGERIDO (COMPDS_secuencia,COMPDS_linea,INVITM_codigo,INVITM_invtarioCJ,INVITM_invtarioUN,INVITM_costoInventa,"
                    + "INVITM_DiasInventa,COMPDS_cantisugeridas, COMPDS_cantirecibidas,COMPDS_stockproveedor,COMPDS_cantunidadsugeridas,COMPDS_cantunidadrecibidas,"
                    + "COMPDS_cantfuncionalsugeridas,COMPDS_cantfuncionalrecibidas,COMPDS_unidembalasugeridas,COMPDS_unidembalarecibidas) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            Query qGrabarSugDet = sess.createSQLQuery(insertDetSegPedido);
            int count = 1;
            if(agrupaciones!=null){
                for (AgrupacionInventario agrupacion : agrupaciones) {
                    List<InveRepdiasInventario> itemList = agrupacion.getListadoItems().stream().filter(it -> it.getINVITM_CajasReales()>0).collect(Collectors.toList());                
                    for (InveRepdiasInventario item : itemList) {
                        parametros = new HashMap<>();
                        parametros.put("0", sugPedido.getSecuencia());//COMPDS_secuencia
                        parametros.put("1", count); //COMPDS_linea
                        parametros.put("2", item.getINVITM_codigo());           //INVITM_codigo
                        parametros.put("3", item.getINVITM_cajas());            //INVITM_invtarioCJ
                        parametros.put("4", item.getINVITM_unidades());         //INVITM_invtarioUN
                        parametros.put("5", item.getINVITM_costoprom1());       //INVITM_costoInventa
                        parametros.put("6", item.getINVITM_diasinventario());   //INVITM_DiasInventa
                        parametros.put("7", item.getINVITM_CajasReales());   //COMPDS_cantisugeridas
                        parametros.put("8", 0);                                 //COMPDS_cantirecibidas
                        parametros.put("9", item.getINVITM_totalinventario());  //COMPDS_stockproveedor
                        parametros.put("10", 0);  //COMPDS_cantunidadsugeridas
                        parametros.put("11", 0);  //COMPDS_cantunidadrecibidas
                        parametros.put("12", item.getINVITM_CajasReales()*item.getINVITM_unidembala());  //COMPDS_cantfuncionalsugeridas
                        parametros.put("13", 0);  //COMPDS_cantfuncionalrecibidas
                        parametros.put("14", item.getINVITM_unidembala());  //COMPDS_unidembalasugeridas
                        parametros.put("15", 0);  //COMPDS_unidembalarecibidas
                        
                        // AGREGAMOS LOS PARAMETROS QUE NECESITA LA CONSULTA
                        parametros.entrySet().forEach((entry) -> {
                            qGrabarSugDet.setParameter(Integer.parseInt(entry.getKey()), entry.getValue());
                        });
                        qGrabarSugDet.executeUpdate();
                        if (count % 100 == 0) { //100, same as the JDBC batch size
                            //flush a batch of inserts and release memory:
                            sess.flush();
                            sess.clear();
                        }
                        count++;
                    }
                }
            }
            sess.flush();
            return sugPedido;
        } catch (HibernateException e) {
            Logger.getLogger(TransaccionalService.class.getName()).log(Level.SEVERE, null, e);
            bitacora.saveActionLog(ActionsTransaccion.ERROR, "grabarSugerenciaCompra");
            return null;
        } catch (Exception ex) {
            Logger.getLogger(TransaccionalService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
