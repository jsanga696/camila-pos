/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.service;

import com.gma.camilabp.domain.CbpgenrCompania;
import com.gma.camilabp.model.consultas.InveRepdiasInventario;
import com.gma.camilabp.model.consultas.ResultadoTransferenciaAjuste;
import com.gma.camilabp.model.consultas.SaldoInventarioModel;
import com.gma.camilabp.model.consultas.TempAjusteTransferenciaInv;
import com.gma.camilabp.model.consultas.TipoFiltro;
import com.gma.camilabp.model.logica.TransferenciaAjusteInventario;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author userdb6
 */
@Local
public interface InventarioLocal {
    public List<InveRepdiasInventario> reporteSaldoInventario(SaldoInventarioModel sim);
    public List<TipoFiltro> nivelesInventario(CbpgenrCompania compania);
    public List<TempAjusteTransferenciaInv> getAjusteTransferenciaTemporalList(String tabla);
    public TempAjusteTransferenciaInv getAjusteTransferenciaTemporal(String tabla, Integer id);
    public void insertTableTemp(String table, List<TempAjusteTransferenciaInv> listado, TransferenciaAjusteInventario model);
    public ResultadoTransferenciaAjuste registrarTransferenciaAjusteInventario(String table, TransferenciaAjusteInventario model, String usuario);
    public String queryNivel(String nivel);
    public List<String> tipoTransferencias();
}
