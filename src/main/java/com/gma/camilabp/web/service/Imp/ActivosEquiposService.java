/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.service.Imp;

import com.gma.camilabp.domain.CbpactEquipo;
import com.gma.camilabp.domain.CbpactModelEquipo;
import com.gma.camilabp.domain.CbpactRangosCliente;
import com.gma.camilabp.web.service.ActivosEquiposLocal;
import com.gma.camilabp.web.service.EntityManagerLocal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.EJB;
import javax.ejb.Singleton;

/**
 *
 * @author userdb5
 */
@Singleton
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
public class ActivosEquiposService implements ActivosEquiposLocal{
    
    @EJB
    private EntityManagerLocal entity;
    
    public Boolean verificarRangos(List<CbpactRangosCliente> nuevo){
        try{
            CbpactRangosCliente t, t2;
            for(int i = 0; i < nuevo.size(); i++){
                t = nuevo.get(i);
                for(int y = 0; y < nuevo.size(); y++){
                    t2 = nuevo.get(y);
                    
                    if(i != y){
                        if((t.getValorMin().compareTo(t2.getValorMin())) > 0 &&
                        (t.getValorMin().compareTo(t2.getValorMax()) < 0))
                            return false;
                        if((t.getValorMax().compareTo(t2.getValorMin()) > 0
                        && t.getValorMax().compareTo(t2.getValorMax()) < 0))
                            return false;
                    }
                }
            }
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }
    
    public Boolean guardarRangos(List<CbpactRangosCliente> anterior, List<CbpactRangosCliente> nuevo){
        try{
            for(CbpactRangosCliente t : anterior){
                t.setFechaFin(new Date());
                entity.merge(t);
            }
            for(CbpactRangosCliente t : nuevo){
                t.setFechaInicio(new Date());
                entity.merge(t);
            }
            entity.executeSQL("DELETE FROM CBPACT_RANGOS_CLIENTE " +
            "WHERE convert(varchar, fecha_inicio, 1) = convert(varchar, fecha_fin, 1)", null);
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }
            
    public Boolean guardarEquipos(CbpactModelEquipo model, List<CbpactEquipo> equiposList){
        try{
            for(CbpactEquipo temp : equiposList){
                temp.setModelEquipo(model);
                entity.merge(temp);
            }            
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }
    
    public Boolean verificarEquipos(CbpactModelEquipo model, List<CbpactEquipo> equiposList){
        try{
            List obj;
            for(CbpactEquipo temp : equiposList){
                if(temp.getId() != null){
                    obj = entity.findListByParameters("SELECT m FROM CbpactEquipo m WHERE m.placa = :placa "
                            + "AND m.modelEquipo.marca = :marca AND m.modelEquipo.modelo = :modelo AND m.id != :id", new String[]{"placa", "marca", "modelo", "id"}, 
                            new Object[]{temp.getPlaca(), model.getMarca(), model.getModelo(), temp.getId()});
                }else{
                    obj = entity.findListByParameters("SELECT m FROM CbpactEquipo m WHERE m.placa = :placa "
                            + "AND m.modelEquipo.marca = :marca AND m.modelEquipo.modelo = :modelo", new String[]{"placa", "marca", "modelo"}, 
                            new Object[]{temp.getPlaca(), model.getMarca(), model.getModelo()});
                }
                if(obj != null && !obj.isEmpty())
                    return false;
            }
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }
    
    /*
    public String getNombreProveedor(String cxpmProveedor, Long oficina){
        HashMap m = new HashMap();
        m.put("", m)
        entity.findObject("SQL","SELECT s.CXPPRO_razonsocia FROM CXPM_PROVEEDOR s WHERE s.GENCIA_codigo = ? AND "
                    + "GENOFI_codigo = ? AND CXPPRO_codigo = ?", m, null, false)
        return "";
    }
    
    public String getNombreCliente(String cxcmCliente, Long oficina){
        return "";
    }*/
    
}
