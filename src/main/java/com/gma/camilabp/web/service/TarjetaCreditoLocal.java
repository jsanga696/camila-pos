/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.service;

import com.gma.camilabp.model.consultas.IngresoBodegaDetModel;
import com.gma.camilabp.model.consultas.IngresoBodegaModel;
import com.gma.camilabp.model.consultas.IngresoRegDepoCheque;
import java.util.HashMap;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author userdb5
 */
@Local
public interface TarjetaCreditoLocal {
    public Boolean guardarTarjetaCredito(IngresoRegDepoCheque ingreso, HashMap datosAdicionales);
}
