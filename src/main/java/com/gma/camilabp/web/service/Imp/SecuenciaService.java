/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.service.Imp;

import com.gma.camilabp.domain.storedprocedure.UsuarioPrueba;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.service.SecuenciaLocal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

/**
 *
 * @author desarrollo2
 */
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
@Singleton
public class SecuenciaService implements SecuenciaLocal{
    
    @EJB
    private EntityManagerLocal entity;

    @Override
    public String getSecuencia(Session sess, String tabla, BigInteger compania, BigInteger oficina, BigInteger bodega, BigInteger modulo, BigInteger tipoDoc)  {
        String sec="";
        try {
            
            Query q1 = sess.createSQLQuery("EXEC GMASGENR_SECUENCIA @gensec_tabla=:tabla, @gensec_compania=:compania, @gensec_oficina=:oficina, @gensec_bodega=:bodega, @gensec_modulo=:modulo, @gensec_tipodoc=:tipoDoc");
            q1.setString("tabla", tabla);
            q1.setBigInteger("compania", compania);
            q1.setBigInteger("oficina", oficina);
            q1.setBigInteger("bodega", bodega);
            q1.setBigInteger("modulo", modulo);
            q1.setBigInteger("tipoDoc", tipoDoc);
            sec = (String) q1.uniqueResult();
           // sess.flush();
        } catch (HibernateException ex) {
            try {
                throw new ServiceException(ex.getMessage());
            } catch (ServiceException ex1) {
                Logger.getLogger(SecuenciaService.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
        return sec;
    }

    @Override
    public List<UsuarioPrueba> getUsuarios() {
        try {
            Query q1 = entity.getSession().createSQLQuery("EXEC SEGM_USUARIOPRUEBA");
            q1.setResultTransformer(Transformers.aliasToBean(UsuarioPrueba.class));
            return (List<UsuarioPrueba>)q1.list();
        } catch (HibernateException ex) {
            try {
                throw new ServiceException(ex.getMessage());
            } catch (ServiceException ex1) {
                Logger.getLogger(SecuenciaService.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
        return new ArrayList<>();
    }
    
}
