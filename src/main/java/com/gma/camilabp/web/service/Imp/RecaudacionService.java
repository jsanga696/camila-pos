/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.service.Imp;

import com.gma.camilabp.domain.BanmCuentacaja;
import com.gma.camilabp.domain.BanmCuentacajaPK;
import com.gma.camilabp.domain.BanmTiposmovimientos;
import com.gma.camilabp.domain.BanmTiposmovimientosPK;
import com.gma.camilabp.domain.CbpgenrRecaudacion;
import com.gma.camilabp.domain.CbpsegmTurno;
import com.gma.camilabp.domain.GenmCaja;
import com.gma.camilabp.model.consultas.ResultadoMovimiento;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.service.PuntoVentaLocal;
import com.gma.camilabp.web.service.RecaudacionLocal;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.interceptor.Interceptors;

/**
 *
 * @author Acer
 */
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
@Singleton
public class RecaudacionService implements RecaudacionLocal{
    @EJB
    private EntityManagerLocal entity;
    @EJB
    private PuntoVentaLocal posService;

    @Override
    public CbpgenrRecaudacion ingresoValor(CbpgenrRecaudacion r, GenmCaja c, CbpsegmTurno t) {
        CbpgenrRecaudacion recaudacion;        
        try{
            r.setCaja(c);
            r.setSegturNumero(t.getSegturNumero());
            r.setGENPGEcodigo(c.getGenpgeCodigo());
            r.setBANENTcodigo(c.getBanentCodigo());
            r.setBANCTAcodigo(c.getBanctaCodigo());
            r.setGENCIAcodigo(c.getGENCIAcodigo());            
            r.tranformUpper();
            recaudacion = (CbpgenrRecaudacion) this.entity.merge(r);
            if(recaudacion!=null){
                ResultadoMovimiento rm = this.grabarMovimiento(recaudacion, c, recaudacion.getBANTMVcodigo());
                if(rm!=null){
                    recaudacion.setGenrrecCodigo(rm.getPscodigo());
                    this.entity.merge(recaudacion);
                }else{
                    return null;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(RecaudacionService.class.getName()).log(Level.SEVERE, null, e);
            return null;
        }
        return recaudacion;
    }

    @Override
    public CbpgenrRecaudacion egresoValor(CbpgenrRecaudacion r, GenmCaja c, CbpsegmTurno t) {
        CbpgenrRecaudacion recaudacion= null;
        try{
            r.setCaja(c);
            r.setSegturNumero(t.getSegturNumero());
            r.setGENPGEcodigo(c.getGenpgeCodigo());
            r.setBANENTcodigo(c.getBanentCodigo());
            r.setBANCTAcodigo(c.getBanctaCodigo());
            r.setGENCIAcodigo(c.getGENCIAcodigo());            
            r.tranformUpper();
            recaudacion = (CbpgenrRecaudacion) this.entity.merge(r);
            if(recaudacion!=null){
                ResultadoMovimiento rm = this.grabarMovimiento(recaudacion, c, recaudacion.getBANTMVcodigo());
                if(rm!=null){
                    recaudacion.setGenrrecCodigo(rm.getPscodigo());
                    this.entity.merge(recaudacion);
                }else{
                    return null;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(RecaudacionService.class.getName()).log(Level.SEVERE, null, e);
            return null;
        }
        return recaudacion;
    }

    @Override
    public CbpgenrRecaudacion transferenciaValor(CbpgenrRecaudacion r, GenmCaja c, CbpsegmTurno t) {
        CbpgenrRecaudacion recaudacion= null;
        try{
            r.setCaja(c);
            r.setSegturNumero(t.getSegturNumero());
            r.setGENPGEcodigo(c.getGenpgeCodigo());
            r.setBANENTcodigo(c.getBanentCodigo());
            r.setBANCTAcodigo(c.getBanctaCodigo());
            r.setGENCIAcodigo(c.getGENCIAcodigo());
            r.tranformUpper();
            recaudacion = (CbpgenrRecaudacion) this.entity.merge(r);
            if(recaudacion!=null){
                ResultadoMovimiento rm = this.grabarMovimiento(recaudacion, c, recaudacion.getBANTMVcodigo());
                if(rm!=null){
                    recaudacion.setGenrrecCodigo(rm.getPscodigo());
                    String bantmvCodigo = (String)posService.getValorDefault(1, c.getGENCIAcodigo(), null, "MOVINGRESO");
                    System.out.println(bantmvCodigo);
                    ResultadoMovimiento rmi = this.grabarMovimiento(recaudacion, c, bantmvCodigo);
                    recaudacion.setGenrrecCodigoIng(rmi.getPscodigo());
                    this.entity.merge(recaudacion);
                }else{
                    return null;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(RecaudacionService.class.getName()).log(Level.SEVERE, null, e);
            return null;
        }
        return recaudacion;
    }

    public ResultadoMovimiento grabarMovimiento(CbpgenrRecaudacion r, GenmCaja c, String bantmvCodigo){
        Map<String, Object> parametros;
        ResultadoMovimiento rm=null;
        try{
            BanmCuentacaja cc = this.entity.find(BanmCuentacaja.class, new BanmCuentacajaPK(r.getGENCIAcodigo(), r.getGENPGEcodigo(), r.getBANENTcodigo(), r.getBANCTAcodigo()));
            BanmTiposmovimientos tm = this.entity.find(BanmTiposmovimientos.class, new BanmTiposmovimientosPK(r.getGENCIAcodigo(), r.getBANTMVcodigo()));
            parametros = new HashMap<>();
            parametros.put("0", r.getGENCIAcodigo());
            parametros.put("1", c.getGENOFIcodigo());
            parametros.put("2", r.getGENPGEcodigo());
            parametros.put("3", r.getBANENTcodigo());
            parametros.put("4", r.getBANCTAcodigo());
            parametros.put("5", bantmvCodigo);
            parametros.put("6", r.getGenrrecDescripcion());
            parametros.put("7", r.getGenrrecMovimiento());
            parametros.put("8", r.getGenrrecIngreso());
            parametros.put("9", r.getGenrrecDestinatario());
            parametros.put("10", cc.getBANCTAreferenciacontable());
            parametros.put("11", r.getGenrrecReferencia());
            parametros.put("12", r.getGenrrecDocumento());
            parametros.put("13", 1);
            parametros.put("14", "");
            parametros.put("15", "");
            parametros.put("16", "");
            parametros.put("17", r.getGenrrecValor());
            parametros.put("18", r.getGenrrecForma());
            parametros.put("19", "");
            parametros.put("20", "");
            parametros.put("21", "");
            parametros.put("22", tm.getBANTMVflagcontabilizacion());
            parametros.put("23", tm.getBANTMVflagcomprobante());
            parametros.put("24", c.getSEGUSUcodigo());
            parametros.put("25", c.getSEGUSUcodigo());
            parametros.put("26", r.getGenrrecEstado());
            parametros.put("27", "");
            parametros.put("28", 1);
            parametros.put("29", null);
            rm = (ResultadoMovimiento)this.entity.findObject("SQL","DECLARE @pscodigo VARCHAR(9), @vosMsgErrorCorto VARCHAR(100), @vosMsgErrorLargo VARCHAR(100) EXECUTE SP_BANM_REGMOVIMIENTO ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,@pscodigo OUT, @vosMsgErrorCorto OUT, @vosMsgErrorLargo OUT,? SELECT @pscodigo AS pscodigo, @vosMsgErrorCorto AS vosMsgErrorCorto, @vosMsgErrorLargo AS vosMsgErrorLargo", parametros, ResultadoMovimiento.class, Boolean.TRUE);
        } catch (Exception e) {
            Logger.getLogger(RecaudacionService.class.getName()).log(Level.SEVERE, null, e);
            return null;
        }
        return rm;
    }
    
}
