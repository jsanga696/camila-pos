/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.service;

import com.gma.camilabp.domain.CbpgenrCompania;
import com.gma.camilabp.domain.CbpsegrGrupousuario;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author userdb2
 */
@Local
public interface GrupoUsuariosLocal {
    void saveSegrGrupousuario(CbpsegrGrupousuario segrGrupousuario);
    List<CbpsegrGrupousuario> getSegrGrupousuarios();
    public CbpgenrCompania getGenrCompania(Long id);
}
