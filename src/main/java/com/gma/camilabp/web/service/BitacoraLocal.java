/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.service;

import com.gma.camilabo.enums.ActionsTransaccion;
import javax.ejb.Local;

/**
 *
 * @author userdb6
 */
@Local
public interface BitacoraLocal {
    public void saveActionLog(ActionsTransaccion action, String detalle);
    public void saveActionLog(ActionsTransaccion action, String detalle, Long usuarioTransaccion);
}
