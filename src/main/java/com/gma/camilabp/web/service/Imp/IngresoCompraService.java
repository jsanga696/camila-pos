/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.service.Imp;

import com.gma.camilabp.domain.CbpgenrBodega;
import com.gma.camilabp.domain.CbpgenrOficina;
import com.gma.camilabp.domain.CbpsegmUsuario;
import com.gma.camilabp.domain.CxpmProveedor;
import com.gma.camilabp.model.consultas.IngresoBodegaDetModel;
import com.gma.camilabp.model.consultas.IngresoBodegaModel;
import com.gma.camilabp.model.consultas.ResultadoIngresoCompra;
import com.gma.camilabp.model.consultas.SugerenciaCompraSecuencia;
import com.gma.camilabp.model.consultas.TipoFiltro;
import com.gma.camilabp.model.logica.SugeridoCompra;
import com.gma.camilabp.sql.QueryPosCxc;
import com.gma.camilabp.sql.QuerysPos;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.service.IngresoCompraLocal;
import com.gma.camilabp.web.service.TransaccionalLocal;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.interceptor.Interceptors;
import org.hibernate.HibernateException;

/**
 *
 * @author userdb5
 */
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
@Singleton
public class IngresoCompraService implements IngresoCompraLocal, Serializable{
    
    @EJB
    private EntityManagerLocal entity;
    @EJB
    private TransaccionalLocal transaccional;
    
    @Override
    public ResultadoIngresoCompra guardarIngresoBodega(IngresoBodegaModel compra, List<IngresoBodegaDetModel> detalle, HashMap datosAdicionales, CbpsegmUsuario usuario){
        try{
            if(!(Boolean)datosAdicionales.get("conSugerido")){
                SugeridoCompra sc= new SugeridoCompra();
                sc.setOficina((CbpgenrOficina)datosAdicionales.get("oficina"));
                sc.setBodega((CbpgenrBodega)datosAdicionales.get("bodega"));
                sc.setProveedor((CxpmProveedor) datosAdicionales.get("proveedor"));
                SugerenciaCompraSecuencia rsc = transaccional.registrarSugerencia("G", usuario, sc, null);
                compra.setCOMCCM_numdocurel(rsc.getNumero().toString());
                compra.setCOMCCM_numsecrel(rsc.getSecuencia().longValue());
            }
            System.out.println("PK "+compra.getCOMCCM_numsecucial());
            System.out.println("DC "+compra.getCOMCCM_numdocumen());
            Map<String, Object> parametros= new HashMap<>();
            parametros.put("0", compra.getCOMCCM_numsecucial());
            parametros.put("1", compra.getCOMCCM_numdocumen());
            parametros.put("2", compra.getGENCIA_codigo());
            parametros.put("3", compra.getGENOFI_codigo());
            parametros.put("4", compra.getGENMOD_codigo());
            parametros.put("5", compra.getGENTDO_codigo());
            parametros.put("6", compra.getCXPPRO_codigo());
            parametros.put("7", compra.getCOMCCM_fecrecepci());
            parametros.put("8", compra.getCXPDOC_fechafactura());
            parametros.put("9", compra.getCOMCCM_descricort());
            parametros.put("10", compra.getCOMCCM_descrilarg());
            parametros.put("11", compra.getCOMCCM_numfacprov());
            parametros.put("12", compra.getCOMCCM_estado());
            parametros.put("13", compra.getGENMON_codigo());
            parametros.put("14", compra.getCOMTDO_codigorel());
            parametros.put("15", compra.getCOMCCM_numdocurel());
            parametros.put("16", compra.getCOMCCM_numsecrel());
            parametros.put("17", compra.getCOMCCM_subtotamov());
            parametros.put("18", compra.getCOMCCM_desctomov());
            parametros.put("19", compra.getCOMCCM_totimpumov());
            parametros.put("20", compra.getCOMCCM_netomov());
            parametros.put("21", compra.getCOMCCM_valimpICE());
            parametros.put("22", compra.getCOMCCM_estuso());
            parametros.put("23", compra.getCOMCCM_totalvolum());
            parametros.put("24", compra.getCOMCCM_totalpeso());
            parametros.put("25", compra.getCOMCCM_estelimina());
            parametros.put("26", compra.getSEGUSU_codigo());
            parametros.put("27", compra.getCOMCCM_prepostea()?"S":"N");
            parametros.put("28", compra.getACCION());
            parametros.put("29", null);
            parametros.put("30", null);
            parametros.put("31", null);
            parametros.put("32", null);
            parametros.put("33", null);
            parametros.put("34", null);
            parametros.put("35", null);
            parametros.put("36", null);
            parametros.put("37", null);
            parametros.put("38", null);
            ResultadoIngresoCompra compraResult = (ResultadoIngresoCompra)this.entity.findObject("SQL","EXECUTE SP_COMT_INGCOMPRA ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?", parametros, ResultadoIngresoCompra.class, Boolean.TRUE);
            
            // SP_COMM_DETCOMIMPU
            Integer i = 1;
            for(IngresoBodegaDetModel d : detalle){
                parametros = new HashMap<>();                
                parametros.put("0", compraResult.getCOMCCM_numsecucial());
                parametros.put("1", i);
                parametros.put("2", compraResult.getCOMCCM_numdocumen());
                parametros.put("3", compra.getGENCIA_codigo() );
                parametros.put("4", compra.getGENOFI_codigo());
                parametros.put("5", compra.getGENMOD_codigo());
                parametros.put("6", compra.getGENTDO_codigo() );
                parametros.put("7", ((CbpgenrBodega)datosAdicionales.get("bodega")).getGenbodSecuencia());
                parametros.put("8", d.getINVITM_codigo());                
                parametros.put("9", d.getCOMDCM_numlinrela());
                parametros.put("10", d.getCOMDCM_canticaja());
                parametros.put("11", d.getCOMDCM_cantiunida());
                parametros.put("12", d.getCOMDCM_cantifunci());
                parametros.put("13", d.getCOMDCM_cantfunpen());
                parametros.put("14", d.getCOMDCM_cantfundev());
                parametros.put("15", d.getCOMDCM_cantegresa());
                parametros.put("16", d.getCOMDCM_cantfactu());
                parametros.put("17", d.getINVUPR_unibascomp());
                parametros.put("18", d.getINVUPR_unirelcomp());
                parametros.put("19", d.getCOMDCM_unidembala());
                parametros.put("20", d.getCOMDCM_peso());
                parametros.put("21", d.getCOMDCM_costomov());
                parametros.put("22", d.getCOMDCM_preciomov());
                parametros.put("23", d.getCOMDCM_porcdescto());
                parametros.put("24", d.getCOMDCM_subtotamov());
                parametros.put("25", d.getCOMDCM_desctomov());
                parametros.put("26", d.getCOMDCM_totimpumov());
                parametros.put("27", d.getCOMDCM_otrosimpuestos());
                parametros.put("28", d.getCOMDCM_netomov());
                parametros.put("29", d.getCOMDCM_volumen());
                parametros.put("30", d.getCXPICA_codclase());
                parametros.put("31", d.getCOMCCM_prepostea());
                parametros.put("32", d.getCOMDCM_codimpICE());
                parametros.put("33", d.getCOMDCM_valimpICE());
                parametros.put("34", null);
                this.entity.executeProcedure("EXECUTE SP_COMT_INGDETCOMPRA ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?", parametros);
                parametros = new HashMap<>();                
                parametros.put("0", d.getCOMDCM_unidembala());
                parametros.put("1", d.getCOMDCM_cantifunci());
                parametros.put("2", d.getCOMDCM_cantifunci());
                parametros.put("3", d.getCOMDCM_unidembala());
                parametros.put("4", d.getCOMDCM_cantifunci());
                parametros.put("5", d.getCOMDCM_unidembala());
                parametros.put("6", compra.getCOMCCM_numsecrel());
                parametros.put("7", d.getINVITM_codigo());                
                Integer rdsug=(Integer) this.entity.findObject("SQL","UPDATE COMT_DETPEDSUGERIDO SET COMPDS_unidembalarecibidas=?, "
                        + "COMPDS_cantfuncionalrecibidas=COMPDS_cantfuncionalrecibidas+?, COMPDS_cantirecibidas=CAST((COMPDS_cantfuncionalrecibidas+?)/? AS INT),"
                        + "COMPDS_cantunidadrecibidas=(COMPDS_cantfuncionalrecibidas+?)%? "
                        + "OUTPUT Inserted.COMPDS_secuencia WHERE COMPDS_secuencia=? AND INVITM_codigo=?", parametros,null,Boolean.TRUE);
                if(rdsug==null){
                    parametros = new HashMap<>();                
                    parametros.put("0", compra.getCOMCCM_numsecrel());
                    Integer linea=(Integer) this.entity.findObject("SQL","SELECT 1+ISNULL(MAX(COMPDS_linea),0) FROM COMT_DETPEDSUGERIDO WHERE COMPDS_secuencia=?",parametros,null,Boolean.TRUE);                    
                    parametros.put("1", linea);
                    parametros.put("2", d.getINVITM_codigo());
                    parametros.put("3", 0);
                    parametros.put("4", 0);
                    parametros.put("5", 0);
                    parametros.put("6", 0);
                    parametros.put("7", 0);
                    parametros.put("8", d.getCOMDCM_canticaja());
                    parametros.put("9", 0);
                    parametros.put("10", 0);
                    parametros.put("11", d.getCOMDCM_cantiunida());
                    parametros.put("12", 0);
                    parametros.put("13", d.getCOMDCM_cantifunci());
                    parametros.put("14", d.getCOMDCM_unidembala());
                    parametros.put("15", d.getCOMDCM_unidembala());
                    this.entity.executeSQL("INSERT INTO COMT_DETPEDSUGERIDO (COMPDS_secuencia,COMPDS_linea,INVITM_codigo,INVITM_invtarioCJ,INVITM_invtarioUN,INVITM_costoInventa,INVITM_DiasInventa,COMPDS_cantisugeridas,COMPDS_cantirecibidas,COMPDS_stockproveedor,COMPDS_cantunidadsugeridas,COMPDS_cantunidadrecibidas,COMPDS_cantfuncionalsugeridas,COMPDS_cantfuncionalrecibidas,COMPDS_unidembalasugeridas,COMPDS_unidembalarecibidas) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", parametros);
                }
                i = i +1;
            }
            //ACTUALIZACION PARA SUGERIDOS CUANDO LA COMPRA SE COMPLETO
            parametros = new HashMap<>();                
            parametros.put("0", compra.getCOMCCM_numsecrel());
            int cantidadRegistro = (int) this.entity.findObject("SQL", QuerysPos.pendientesSugeridos, parametros, null, Boolean.TRUE);
            if(cantidadRegistro==0){
                this.entity.executeSQL("UPDATE COMT_CABPEDSUGERIDO SET COMPDS_estado='C' WHERE COMPDS_secuencia=?", parametros);
            }
            
            // POSTEO DE COMPRA
            if(compra.getCOMCCM_prepostea()){
                HashMap<String, Object> m = new HashMap();
                m.put("0", compra.getGENCIA_codigo());
                m.put("1", compra.getGENOFI_codigo());
                m.put("2", "COM");
                m.put("3", "COM");
                m.put("4", compraResult.getCOMCCM_numsecucial());                
                int invNumIngreso = (int)this.entity.findObject("SQL","DECLARE @secmovbod int EXECUTE SP_INVP_GENMOVBODING ?,?,?,?,?, @secmovbod OUT SELECT @secmovbod", m, null, Boolean.TRUE);
            }
            
            return compraResult;
        }catch(Exception e){
            Logger.getLogger(IngresoCompraService.class.getName()).log(Level.SEVERE, null, e);
            return null;
        }
    }
    
    
    @Override
    public List<TipoFiltro> compraPorNumeroFactura(String secuenciaOficina, String codigoProveedor, String numFactura, Boolean posteado){
        List<TipoFiltro> list=null;
        try{
            String condicion="";
            Map<String, Object> parametros= new HashMap<>();
            parametros.put("0", secuenciaOficina);
            parametros.put("1", secuenciaOficina);
            parametros.put("2", codigoProveedor);
            parametros.put("3", numFactura);
            parametros.put("4", secuenciaOficina);
            parametros.put("5", codigoProveedor);
            parametros.put("6", numFactura);
            if(posteado)
                condicion="AND c.COMCCM_prepostea='S'";
            list = this.entity.findAllSQL(QueryPosCxc.grupoSugeridoPorNumeroFactura.replace("posteo", condicion), parametros, TipoFiltro.class);
            return list;
        } catch (HibernateException e) {
            Logger.getLogger(IngresoCompraService.class.getName()).log(Level.SEVERE, null, e);
        }
        return list;
    }
    
    @Override
    public BigDecimal precioItemLista(String tipo, String codigoItem){
        BigDecimal precio = BigDecimal.ZERO;
        try{
            HashMap map2 = new HashMap();
            map2.put("0", tipo);
            map2.put("1", codigoItem);
            precio = (BigDecimal) entity.findObject("SQL", QuerysPos.precioListaItem, map2, null, Boolean.TRUE);
        }catch(Exception e){
            Logger.getLogger(IngresoCompraService.class.getName()).log(Level.SEVERE, null, e);
        }
        return precio;
    }
}
