/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.service.Imp;

import com.gma.camilabo.enums.ActionsTransaccion;
import com.gma.camilabp.domain.CbpsegmTurno;
import com.gma.camilabp.domain.GenmCaja;
import com.gma.camilabp.model.consultas.ResultadoCobro;
import com.gma.camilabp.model.consultas.ResultadoFacturacion;
import com.gma.camilabp.model.logica.Pago;
import com.gma.camilabp.model.logica.ValoresPago;
import com.gma.camilabp.web.service.EntityManagerLocal;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;

/**
 *
 * @author userdb6
 */
@Singleton(name = "entityManagerService")
@Lock(LockType.READ)
public class EntityManagerService implements EntityManagerLocal {
    
    private static final Logger log = Logger.getLogger(EntityManagerService.class.getName());
    
    @PersistenceContext(unitName = "camilapos")
    private EntityManager em;
    
    @Override
    @SuppressWarnings("deprecation")
    public EntityManager getManager() {
        return em;
    }

    @Override
    @SuppressWarnings("deprecation")
    public Session getSession() {
        return em.unwrap(Session.class);
    }

    @Override
    @SuppressWarnings("deprecation")
    public <T> T find(Class<T> entity, Object p) {
        try {
            return getManager().find(entity, p);
        } catch (Exception e) {
            log.log(Level.SEVERE, null, e);
        }
        return null;
    }

    @Override
    @SuppressWarnings("deprecation")
    public Object find(String query, Map<String, Object> params) {
        try {
            javax.persistence.Query q = getManager().createQuery(query);
            if (params != null) {
                params.entrySet().forEach((et) -> {
                    q.setParameter(et.getKey(), et.getValue());
                });
            }
            Object x = q.getResultList();
            if (x != null && ((List) x).size() > 0) {
                return q.getResultList().get(0);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, null, e);
        }
        return null;
    }

    @Override
    @SuppressWarnings("deprecation")
    public List findAll(String query, Map<String, Object> params) {
        try {
            javax.persistence.Query q = getManager().createQuery(query);
            if (params != null) {
                params.entrySet().forEach((et) -> {
                    q.setParameter(et.getKey(), et.getValue());
                });
            }
            return q.getResultList();
        } catch (Exception e) {
            log.log(Level.SEVERE, null, e);
        }
        return null;
    }

    @Override
    @SuppressWarnings("deprecation")
    public <T> T findByParameter(Class entity, Map<String, Object> paramt) {
        T ob = null;
        try {
            Criteria cq = getSession().createCriteria(entity);
            cq.add(Restrictions.allEq(paramt));
            ob = (T) cq.uniqueResult();
        } catch (Exception e) {
            log.log(Level.SEVERE, null, e);
        }
        return ob;
    }

    @Override
    @SuppressWarnings("deprecation")
    public <T> T findByParameterFull(Class entity, Map<String, Object> parameterEq, Map<String, Object> parameterNot) {
        T ob = null;
        try {
            Criteria cq = getSession().createCriteria(entity);
            if (parameterEq != null && !parameterEq.isEmpty()) {
                cq.add(Restrictions.allEq(parameterEq));
            }
            if (parameterNot != null && !parameterNot.isEmpty()) {
                parameterNot.entrySet().forEach((entry) -> {
                    cq.add(Restrictions.ne(entry.getKey(), entry.getValue()));
                });
            }
            ob = (T) cq.uniqueResult();
        } catch (Exception e) {
            log.log(Level.SEVERE, null, e);
        }
        return ob;
    }

    @Override
    @SuppressWarnings("deprecation")
    public <T extends Object> List<T> findAllByParameter(Class<T> entity, Map<String, Object> paramt) {
        List result = null;
        try {
            Criteria cq = getSession().createCriteria(entity);
            if (paramt != null && !paramt.isEmpty()) {
                cq.add(Restrictions.allEq(paramt));
            }
            result = (List) cq.list();
            Hibernate.initialize(result);
        } catch (Exception e) {
            log.log(Level.SEVERE, null, e);
        }
        return result;
    }

    @Override
    @SuppressWarnings("deprecation")
    public Object merge(Object o) {
        try {
            return getManager().merge(o);
        } catch (Exception e) {
            log.log(Level.SEVERE, null, e);
        }
        return null;
    }

    @Override
    @SuppressWarnings("deprecation")
    public Integer update(String query, Map<String, Object> params) {
        try {
            javax.persistence.Query q = getManager().createQuery(query);
            for (Map.Entry<String, Object> et : params.entrySet()) {
                q.setParameter(et.getKey(), et.getValue());
            }
            return q.executeUpdate();
        } catch (Exception e) {
            log.log(Level.SEVERE, null, e);
        }
        return null;
    }
    
    @Override
    @SuppressWarnings("deprecation")
    public Boolean deleteEntity(Object o){
        try{
            em.remove(em.contains(o) ? o : em.merge(o));
            return true;
        }catch (Exception e) {
            log.log(Level.SEVERE, null, e);
            return false;
        }
    }

    @Override
    @SuppressWarnings("deprecation")
    public List findAll(String query, Map<String, Object> params, Integer min, Integer max) {
        try {
            if (em == null) {
                return null;
            }
            javax.persistence.Query q = em.createQuery(query);
            if (params != null) {
                params.entrySet().forEach((et) -> {
                    q.setParameter(et.getKey(), et.getValue());
                });
            }
            q.setFirstResult(min);
            q.setMaxResults(max);
            return q.getResultList();
        } catch (Exception e) {
            log.log(Level.SEVERE, null, e);
        }
        return null;
    }
    
    @Override
    @SuppressWarnings("deprecation")
//    @Lock(LockType.READ)
//    @AccessTimeout(-1L)
    public <T> List<T> getNative2Alias(Class<T> clase, String query, Map<String, Object> params) {
        try {
            List<T> result = null;
            Session sess = this.getSession();
            if (sess != null) {
                org.hibernate.Query q = sess.createSQLQuery(query);
                if (params != null) {
                    params.entrySet().forEach((p) -> {
                        q.setParameter(p.getKey(), p.getValue());
                    });
                }
                result = q.setResultTransformer(Transformers.aliasToBean(clase)).list();
//                sess.flush();
//                sess.clear();
                return result;
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, null, e);
        }
        return null;
    }
    
    public Object findObject(String tipoQuery, String query, Map<String, Object> parametros, Class modelo, Boolean requireTransaction){
        Object o = null;
        try {
            Session sess = this.getSession();
            Query q;
            if (tipoQuery.equalsIgnoreCase("SQL")) {
                q = sess.createSQLQuery(query);
            } else//HQL
            {
                q = sess.createQuery(query).setMaxResults(1);
            }
            if (modelo != null) {
                q.setResultTransformer(Transformers.aliasToBean(modelo));
            }
            if (parametros != null) {
                parametros.entrySet().forEach((entry) -> {
                    if (entry.getValue() instanceof List) {
                        q.setParameterList(entry.getKey(), (Collection) entry.getValue());
                    } else {
                        //System.out.println(entry.getKey()+"-"+entry.getValue());
                        if (tipoQuery.equalsIgnoreCase("SQL")) {
                            q.setParameter(Integer.parseInt(entry.getKey()), entry.getValue());
                        } else {
                            q.setParameter(entry.getKey(), entry.getValue());
                        }
                    }
                });
            }
            o = (Object) q.uniqueResult();
            Hibernate.initialize(o);
        } catch (Exception e) {
            Logger.getLogger(EntityManagerService.class.getName()).log(Level.SEVERE, null, e);
        }
        return o;
    }
    
    @Override
    public <T> T findObject(Class<T> entity, Map<String, Object> parametros) {
        T o = null;
        try {
            Session sess = this.getSession();
            Criteria cq = sess.createCriteria(entity);
            cq.add(Restrictions.allEq(parametros));
            o = (T) cq.uniqueResult();
            Hibernate.initialize(o);
        } catch (Exception e) {
            Logger.getLogger(EntityManagerService.class.getName()).log(Level.SEVERE, null, e);
        }
        return o;
    }
    
    @Override
    public <T> List<T> findAllByParameter(Class entity, Map<String, Object> parametros, String order) {
        List<T> lo = null;
        Map<String, Object> p = new HashMap<>();
        List<Criterion> lc = new ArrayList<>();
        try {
            Session sess = this.getSession();
            Criteria cq = sess.createCriteria(entity);
            if (parametros != null) {
                parametros.entrySet().forEach((entry) -> {
                    if (entry.getValue() instanceof List) {
                        lc.add(Restrictions.in(entry.getKey(), (Collection) entry.getValue()));
                    } else {
                        p.put(entry.getKey(), entry.getValue());
                    }
                });
                cq.add(Restrictions.allEq(p));
                if (!lc.isEmpty()) {
                    lc.forEach((c) -> {
                        cq.add(c);
                    });
                }
            }
            if (order != null && order.length() > 0) {
                cq.addOrder(Order.asc(order));
            }
            lo = (List<T>) cq.list();
            lo.size();
            Hibernate.initialize(lo);
        } catch (Exception e) {
            Logger.getLogger(EntityManagerService.class.getName()).log(Level.SEVERE, null, e);
        }
        return lo;
    }
    
    @Override
    public <T extends Object> List<T> findListByParameters(String query, String[] par, Object[] val) {
        List result = null;
        try {
            Session sess = this.getSession();
            Query q = sess.createQuery(query);
            for (int i = 0; i < par.length; i++) {
                q.setParameter(par[i], val[i]);
            }
            result = (List) q.list();
            Hibernate.initialize(result);
            //HiberUtil.unproxy(ob);
        } catch (Exception e) {
            Logger.getLogger(EntityManagerService.class.getName()).log(Level.SEVERE, null, e);
        }
        return result;
    }
    
    @Override
    public <T extends Object> List<T> findAll(Class<T> entity) {
        try {
            Session sess = this.getSession();
            Criteria cq = sess.createCriteria(entity);
            Hibernate.initialize(cq.list());
            return cq.list();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    @Override
    public List findAllSQL(String query, Map<String, Object> parametros, Class modelo) {
        List l = null;
        try {
            Session sess = this.getSession();
            Query q = sess.createSQLQuery(query);
            if (modelo != null) {
                q.setResultTransformer(Transformers.aliasToBean(modelo));
            }
            if (parametros != null) {
                parametros.entrySet().forEach((entry) -> {
                    if (entry.getValue() instanceof List) {
                        q.setParameterList(entry.getKey(), (Collection) entry.getValue());
                    } else {
                        q.setParameter(Integer.parseInt(entry.getKey()), entry.getValue());
                    }
                });
            }
            l = q.list();
        } catch (Exception e) {
            Logger.getLogger(EntityManagerService.class.getName()).log(Level.SEVERE, null, e);
        }
        return l;
    }

    @Override
    public Boolean executeProcedure(String sqlProcedure, Map<String, Object> parametros) {
        Boolean execute = Boolean.FALSE;
        try {
            Session sess = this.getSession();
            Query q = sess.createSQLQuery(sqlProcedure);
            if (parametros != null) {
                parametros.entrySet().forEach((entry) -> {
                    if (entry.getValue() instanceof List) {
                        q.setParameterList(entry.getKey(), (Collection) entry.getValue());
                    } else {
                        q.setParameter(Integer.parseInt(entry.getKey()), entry.getValue());
                    }
                });
            }
            q.executeUpdate();
            execute = Boolean.TRUE;
        } catch (Exception e) {
            Logger.getLogger(EntityManagerService.class.getName()).log(Level.SEVERE, null, e);
        }
        return execute;
    }
    
    @Override
    public Integer executeSQL(String query, Map<String, Object> parametros) {
        Object o = null;
        try {
            Session sess = this.getSession();
            Query q = sess.createSQLQuery(query);
            
            if(parametros != null){
                parametros.entrySet().forEach((entry) -> {
                    if (entry.getValue() instanceof List) {
                        q.setParameterList(entry.getKey(), (Collection) entry.getValue());
                    } else {
                        q.setParameter(Integer.parseInt(entry.getKey()), entry.getValue());
                    }
                });
            }
            return q.executeUpdate();
        } catch (Exception e) {
            Logger.getLogger(EntityManagerService.class.getName()).log(Level.SEVERE, null, e);
            return null;
        }
    }
    
    @Override
    public <T> List<T> findAllByParameterAndParameterIlike(Class entity, Map<String, Object> parametros, Map<String, Object> parametrosString) {
        List<T> lo = null;
        try {
            //Session sess = HiberUtil.getSession();
            Session sess = this.getSession();
            Criteria cq = sess.createCriteria(entity);
            if (parametros != null && !parametros.isEmpty()) {
                cq.add(Restrictions.allEq(parametros));
            }
            if (parametrosString != null && !parametrosString.isEmpty()) {
                parametrosString.entrySet().forEach((entrySet) -> {
                    cq.add(Restrictions.ilike(entrySet.getKey(), entrySet.getValue()));
                });
            }
            lo = (List<T>) cq.list();
            lo.size();
            Hibernate.initialize(lo);
        } catch (Exception e) {
            Logger.getLogger(EntityManagerService.class.getName()).log(Level.SEVERE, null, e);
        }
        return lo;
    }
    
    /*@Override
    public <T> T find(Class<T> entity, Object id) {
        T o = null;
        try {
            Session sess = HiberUtil.getSession();
            o = (T) sess.get(entity, (Serializable) id);
            Hibernate.initialize(o);
        } catch (HibernateException e) {
            Logger.getLogger(EntityManagerService.class.getName()).log(Level.SEVERE, null, e);
        }
        return o;
    }

    @Override
    public Object findObject(String tipoQuery, String query, Map<String, Object> parametros, Class modelo, Boolean requireTransaction) {
        Object o = null;
        try {
            if (requireTransaction) {
                HiberUtil.requireTransaction();
            }
            Session sess = HiberUtil.getSession();
            Query q;
            if (tipoQuery.equalsIgnoreCase("SQL")) {
                q = sess.createSQLQuery(query);
            } else//HQL
            {
                q = sess.createQuery(query).setMaxResults(1);
            }
            if (modelo != null) {
                q.setResultTransformer(Transformers.aliasToBean(modelo));
            }
            if (parametros != null) {
                parametros.entrySet().forEach((entry) -> {
                    if (entry.getValue() instanceof List) {
                        q.setParameterList(entry.getKey(), (Collection) entry.getValue());
                    } else {
                        //System.out.println(entry.getKey()+"-"+entry.getValue());
                        if (tipoQuery.equalsIgnoreCase("SQL")) {
                            q.setParameter(Integer.parseInt(entry.getKey()), entry.getValue());
                        } else {
                            q.setParameter(entry.getKey(), entry.getValue());
                        }
                    }
                });
            }
            o = (Object) q.uniqueResult();
            Hibernate.initialize(o);
        } catch (HibernateException e) {
            Logger.getLogger(EntityManagerService.class.getName()).log(Level.SEVERE, null, e);
        }
        return o;
    }

    @Override
    public List findAllSQL(String query, Map<String, Object> parametros, Class modelo) {
        List l = null;
        try {
            Session sess = HiberUtil.getSession();
            Query q = sess.createSQLQuery(query);
            if (modelo != null) {
                q.setResultTransformer(Transformers.aliasToBean(modelo));
            }
            if (parametros != null) {
                parametros.entrySet().forEach((entry) -> {
                    if (entry.getValue() instanceof List) {
                        q.setParameterList(entry.getKey(), (Collection) entry.getValue());
                    } else {
                        q.setParameter(Integer.parseInt(entry.getKey()), entry.getValue());
                    }
                });
            }
            l = q.list();
        } catch (HibernateException e) {
            Logger.getLogger(EntityManagerService.class.getName()).log(Level.SEVERE, null, e);
        }
        return l;
    }

    @Override
    public <T> T findObject(Class<T> entity, Map<String, Object> parametros) {
        T o = null;
        try {
            Session sess = HiberUtil.getSession();
            Criteria cq = sess.createCriteria(entity);
            cq.add(Restrictions.allEq(parametros));
            o = (T) cq.uniqueResult();
            Hibernate.initialize(o);
        } catch (HibernateException e) {
            Logger.getLogger(EntityManagerService.class.getName()).log(Level.SEVERE, null, e);
        }
        return o;
    }

    @Override
    public List findAll(String query, Map<String, Object> paramt) {
        List l = null;
        try {
            Session sess = HiberUtil.getSession();
            Query q = sess.createQuery(query);
            if (paramt != null) {
                paramt.entrySet().forEach((entrySet) -> {
                    if (entrySet.getValue() instanceof List) {
                        q.setParameterList(entrySet.getKey(), (Collection) entrySet.getValue());
                    } else {
                        q.setParameter(entrySet.getKey(), entrySet.getValue());
                    }
                });
            }
            l = (List) q.list();
            l.size();
            Hibernate.initialize(l);
        } catch (HibernateException e) {
            Logger.getLogger(EntityManagerService.class.getName()).log(Level.SEVERE, null, e);
        }
        return l;
    }

    @Override
    public <T extends Object> List<T> findAll(Class<T> entity) {
        try {
            Session sess = HiberUtil.getSession();
            Criteria cq = sess.createCriteria(entity);
            Hibernate.initialize(cq.list());
            return cq.list();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public <T> List<T> findAllByParameter(Class entity, Map<String, Object> parametros, String order) {
        List<T> lo = null;
        Map<String, Object> p = new HashMap<>();
        List<Criterion> lc = new ArrayList<>();
        try {
            Session sess = HiberUtil.getSession();
            Criteria cq = sess.createCriteria(entity);
            if (parametros != null) {
                parametros.entrySet().forEach((entry) -> {
                    if (entry.getValue() instanceof List) {
                        lc.add(Restrictions.in(entry.getKey(), (Collection) entry.getValue()));
                    } else {
                        p.put(entry.getKey(), entry.getValue());
                    }
                });
                cq.add(Restrictions.allEq(p));
                if (!lc.isEmpty()) {
                    lc.forEach((c) -> {
                        cq.add(c);
                    });
                }
            }
            if (order != null && order.length() > 0) {
                cq.addOrder(Order.asc(order));
            }
            lo = (List<T>) cq.list();
            lo.size();
            Hibernate.initialize(lo);
        } catch (HibernateException e) {
            Logger.getLogger(EntityManagerService.class.getName()).log(Level.SEVERE, null, e);
        }
        return lo;
    }

    @Override
    public <T> List<T> findAllByParameterAndParameterIlike(Class entity, Map<String, Object> parametros, Map<String, Object> parametrosString) {
        List<T> lo = null;
        try {
            Session sess = HiberUtil.getSession();
            Criteria cq = sess.createCriteria(entity);
            if (parametros != null && !parametros.isEmpty()) {
                cq.add(Restrictions.allEq(parametros));
            }
            if (parametrosString != null && !parametrosString.isEmpty()) {
                parametrosString.entrySet().forEach((entrySet) -> {
                    cq.add(Restrictions.ilike(entrySet.getKey(), entrySet.getValue()));
                });
            }
            lo = (List<T>) cq.list();
            lo.size();
            Hibernate.initialize(lo);
        } catch (HibernateException e) {
            Logger.getLogger(EntityManagerService.class.getName()).log(Level.SEVERE, null, e);
        }
        return lo;
    }

    @Override
    public Object persist(Object object) {
        Object o = null;
        try {
            HiberUtil.requireTransaction();
            Session sess = HiberUtil.getSession();
            o = sess.merge(object);
        } catch (HibernateException e) {
            Logger.getLogger(EntityManagerService.class.getName()).log(Level.SEVERE, null, e);
        }
        return o;
    }

    @Override
    public Boolean executeProcedure(String sqlProcedure, Map<String, Object> parametros) {
        Boolean execute = Boolean.FALSE;
        try {
            HiberUtil.requireTransaction();
            Session sess = HiberUtil.getSession();
            Query q = sess.createSQLQuery(sqlProcedure);
            if (parametros != null) {
                parametros.entrySet().forEach((entry) -> {
                    if (entry.getValue() instanceof List) {
                        q.setParameterList(entry.getKey(), (Collection) entry.getValue());
                    } else {
                        q.setParameter(Integer.parseInt(entry.getKey()), entry.getValue());
                    }
                });
            }
            q.executeUpdate();
            execute = Boolean.TRUE;
        } catch (HibernateException e) {
            Logger.getLogger(EntityManagerService.class.getName()).log(Level.SEVERE, null, e);
        }
        return execute;
    }

    public <T extends Object> List<T> findListByParameters(String query, String[] par, Object[] val) {
        List result = null;
        try {
            HiberUtil.requireTransaction();
            Session sess = HiberUtil.getSession();
            Query q = sess.createQuery(query);
            for (int i = 0; i < par.length; i++) {
                q.setParameter(par[i], val[i]);
            }
            result = (List) q.list();
            Hibernate.initialize(result);
            //HiberUtil.unproxy(ob);
        } catch (Exception e) {
            Logger.getLogger(EntityManagerService.class.getName()).log(Level.SEVERE, null, e);
        }
        return result;
    }

    @Override
    public void remove(Object object) {
        try {
            HiberUtil.requireTransaction();
            Session sess = HiberUtil.getSession();
            sess.delete(object);
        } catch (HibernateException e) {
            Logger.getLogger(EntityManagerService.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    @Override
    public Integer executeSQL(String query, Map<String, Object> parametros) {
        Object o = null;
        try {
            HiberUtil.requireTransaction();
            Session sess = HiberUtil.getSession();
            Query q=sess.createSQLQuery(query);
            
            if(parametros != null){
                parametros.entrySet().forEach((entry) -> {
                    if (entry.getValue() instanceof List) {
                        q.setParameterList(entry.getKey(), (Collection) entry.getValue());
                    } else {
                        q.setParameter(Integer.parseInt(entry.getKey()), entry.getValue());
                    }
                });
            }
            return q.executeUpdate();
        } catch (HibernateException e) {
            Logger.getLogger(EntityManagerService.class.getName()).log(Level.SEVERE, null, e);
            return null;
        }
    }*/

}
