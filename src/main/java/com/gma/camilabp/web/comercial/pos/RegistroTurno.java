/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.comercial.pos;

import com.gma.camilabo.enums.ActionsTransaccion;
import com.gma.camilabp.domain.CbpgenrOficina;
import com.gma.camilabp.domain.CbpgenrRegistroDenominacion;
import com.gma.camilabp.domain.CbpposCabfacttemporal;
import com.gma.camilabp.domain.CbpsegmTurno;
import com.gma.camilabp.domain.FactSesioncaja;
import com.gma.camilabp.model.consultas.DatosReporte;
import com.gma.camilabp.model.consultas.Estadistica;
import com.gma.camilabp.model.consultas.FormaPago;
import com.gma.camilabp.model.consultas.Importe;
import com.gma.camilabp.model.consultas.LogBitacora;
import com.gma.camilabp.sql.QuerysPos;
import com.gma.camilabp.web.service.BitacoraLocal;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.service.PuntoVentaLocal;
import com.gma.camilabp.web.session.ServletSession;
import com.gma.camilabp.web.session.UserSession;
import com.gma.camilabp.web.util.JsfUtil;
import com.gma.camilabp.web.util.StringValues;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

/**
 *
 * @author userdb6
 */
@Named(value = "registroTurno")
@ViewScoped
public class RegistroTurno implements Serializable{
    @Inject
    private UserSession userSession;
    @Inject
    private ServletSession datosSession;
    @EJB
    private PuntoVentaLocal posService;
    @EJB
    private EntityManagerLocal entity;
    @EJB
    private BitacoraLocal bitacora;
    
    protected FactSesioncaja sessionCaja;
    private Map<String, Object> parametros;
    
    protected CbpgenrOficina oficina;
    protected Importe importe;
    protected Estadistica estadistica;
    protected FormaPago formaPago;
    protected CbpsegmTurno turno;
    
    @PostConstruct
    public void init() {
        try{
            if(this.userSession.getTurno()!=null){
                parametros= new HashMap<>();
                parametros.put("fACSCAidsececaja", this.userSession.getTurno().getSegturNumero().intValue());
                this.sessionCaja= entity.findObject(FactSesioncaja.class, parametros);
            }else{
                sessionCaja = new FactSesioncaja();
            }
        } catch (Exception e) {
            Logger.getLogger(RegistroTurno.class.getName()).log(Level.SEVERE, null, e);
        }
    }        
    
    public void generarTurno(){
        try{
            if(this.userSession.getTurno()==null){
                if(this.userSession.getCbpsegmUsuario().getCodigoCaja()!=null){
                    CbpsegmTurno t= posService.registrarTurno(this.userSession.getCbpsegmUsuario());
                    if(t!=null){
                        this.userSession.setTurno(t);
                        this.sessionCaja= posService.registrarSessionCaja(this.userSession.getCaja(), this.userSession.getTurno());
                        bitacora.saveActionLog(ActionsTransaccion.REGISTRO_TURNO, t.getSegturNumero().toString());
                        JsfUtil.messageInfo(null, StringValues.msgRegistroExitoso+t.getSegturNumero(), "");
                    }
                }else{
                    JsfUtil.messageWarning(null, "USUARIO DEBE REGISTRAR CAJA", "");
                }
            }else{
                JsfUtil.messageWarning(null, "EXISTE TURNO ACTIVO", "");
            }
        } catch (Exception e) {
            Logger.getLogger(RegistroTurno.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    /**
     * 1:PDF; 2:SERVLET; 3:DIALOGO
     * @param tipo 
     */
    public void transaccionesPreliminar(Long tipo){
        try{
            if(this.userSession.getTurno()!=null){
                turno= this.userSession.getTurno();
                if(datosSession!=null){
                    posService.reporteCaja(this.userSession.getCbpsegmUsuario().getSegusuLogin(), turno.getSegturNumero());
                    datosSession.borrarDatos();
                    datosSession.setPath(FacesContext.getCurrentInstance().getExternalContext().getRealPath("//"));
                    switch(tipo.intValue()){
                        case 1:
                            datosSession.setTieneDatasource(Boolean.FALSE);
                            datosSession.setNombreReporte("resumenTransacciones");
                            datosSession.agregarParametro("SUBREPORT_DIR", datosSession.getPath()+ "/reportes/pos/");
                            datosSession.agregarParametro("NUMERO_TURNO",turno.getSegturNumero());
                            datosSession.agregarParametro("SUPERVISOR",posService.esSupervisor(this.userSession.getCbpsegmUsuario()));
                            datosSession.agregarParametro("USUARIO",this.userSession.getCbpsegmUsuario().getSegusuLogin());
                            JsfUtil.redirectNewTab("/CamilaPOS/Documento");
                            break;
                        case 2:
                            this.parametros = new HashMap<>();
                            this.parametros.put("genofiSecuencia", this.userSession.getCaja().getGENOFIcodigo());
                            datosSession.agregarParametro("OFICINA", this.entity.findObject(CbpgenrOficina.class, this.parametros));
                            datosSession.agregarParametro("CAJA", this.userSession.getCaja());
                            datosSession.agregarParametro("USUARIO", this.userSession.getCbpsegmUsuario());
                            datosSession.agregarParametro("TURNO", turno);
                            this.parametros = new HashMap<>();
                            this.parametros.put("0", turno.getSegturNumero());
                            this.parametros.put("1", this.userSession.getCbpsegmUsuario().getSegusuLogin());
                            datosSession.agregarParametro("IMPORTE", entity.findObject("SQL",QuerysPos.importe, this.parametros, Importe.class, Boolean.FALSE));
                            datosSession.agregarParametro("ESTADISTICA", entity.findObject("SQL",QuerysPos.estadistica, this.parametros, Estadistica.class, Boolean.FALSE));
                            datosSession.agregarParametro("TOTALES", entity.findObject("SQL",QuerysPos.formaPago, this.parametros, FormaPago.class, Boolean.FALSE));
                            datosSession.agregarParametro("FORMATO", posService.getValorDefault(1, this.userSession.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia(), null, "POSFORMATO"+this.userSession.getCaja().getGENBODcodigo()));
                            datosSession.agregarParametro("RESTRINGIR", "S".equalsIgnoreCase((String)posService.getValorDefault(1, this.userSession.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia(), null, "RPTVALORES"))?!posService.esSupervisor(this.userSession.getCbpsegmUsuario()):false);
                            JsfUtil.redirectNewTab("/CamilaPOS/Preliminar");
                            break;
                            
                        case 3:
                            this.parametros = new HashMap<>();
                            this.parametros.put("genofiSecuencia", this.userSession.getCaja().getGENOFIcodigo());
                            this.oficina= this.entity.findObject(CbpgenrOficina.class, this.parametros);
                            this.parametros = new HashMap<>();
                            this.parametros.put("0", turno.getSegturNumero());
                            this.parametros.put("1", this.userSession.getCbpsegmUsuario().getSegusuLogin());
                            this.importe =(Importe) entity.findObject("SQL",QuerysPos.importe, this.parametros, Importe.class, Boolean.FALSE);
                            this.estadistica = (Estadistica) entity.findObject("SQL",QuerysPos.estadistica, this.parametros, Estadistica.class, Boolean.FALSE);
                            this.formaPago = (FormaPago) entity.findObject("SQL",QuerysPos.formaPago, this.parametros, FormaPago.class, Boolean.FALSE);
                            JsfUtil.update("frmdlgTransacciones");
                            JsfUtil.executeJS("PF('dlgTransacciones').show();");
                            break;
                    }
                }
            }else{
                JsfUtil.messageWarning(null, "NO EXISTE TURNO ACTIVO", "");
            }
        } catch (Exception e) {
            Logger.getLogger(RegistroTurno.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    /**
     * 1:PDF; 2:SERVLET; 3:DIALOGO
     * @param tipo 
     */
    public void cerrarTurno(Long tipo){
        try{
            if(this.userSession.getTurno()!=null){
                CbpsegmTurno t= this.userSession.getTurno();
                String cierre = (String) posService.getValorDefault(1, this.userSession.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia(), null, "CIERRE");
                parametros= new HashMap<>();
                parametros.put("segturCodigo", t);
                parametros.put("genrdeTipo", 3L);
                parametros.put("estado", Boolean.TRUE);
                List<CbpgenrRegistroDenominacion> rc = this.entity.findAllByParameter(CbpgenrRegistroDenominacion.class, parametros,null);                
                if("S".equalsIgnoreCase(cierre)?(rc!=null && !rc.isEmpty()):true){
                    parametros= new HashMap<>();
                    parametros.put("cbpfaccEstado", "A");
                    parametros.put("segturNumero", t.getSegturNumero());
                    List<CbpposCabfacttemporal> ft = this.entity.findAllByParameter(CbpposCabfacttemporal.class, parametros,null);
                    if(ft==null || ft.isEmpty()){
                        parametros= new HashMap<>();
                        parametros.put("fACSCAidsececaja", t.getSegturNumero().intValue());
                        this.sessionCaja= entity.findObject(FactSesioncaja.class, parametros);
                        if(this.posService.cerrarTurno(t)){
                            this.posService.cerrarSessionCaja(this.sessionCaja);
                            this.userSession.setTurno(null);
                            this.sessionCaja=null;
                            JsfUtil.messageInfo(null, "TURNO CERRADO "+t.getSegturNumero(), "");
                            posService.reporteCaja(this.userSession.getCbpsegmUsuario().getSegusuLogin(), t.getSegturNumero());
                            if(datosSession!=null){
                                datosSession.borrarDatos();
                                datosSession.setPath(FacesContext.getCurrentInstance().getExternalContext().getRealPath("//"));
                                switch (tipo.intValue()){
                                    case 1:
                                        datosSession.setTieneDatasource(Boolean.FALSE);
                                        datosSession.setNombreReporte("resumenTransaccionesCierre");
                                        datosSession.agregarParametro("NUMERO_TURNO",t.getSegturNumero());
                                        datosSession.agregarParametro("SUBREPORT_DIR",datosSession.getPath()+ "/reportes/pos/");
                                        datosSession.agregarParametro("SUPERVISOR",posService.esSupervisor(this.userSession.getCbpsegmUsuario()));
                                        DatosReporte dr = new DatosReporte();
                                        dr.setNombreReporte("cierreTurno");
                                        Map<String, Object> p = new HashMap<>();
                                        p.put("NUMERO_TURNO", t.getSegturNumero());
                                        dr.setParametros(p);                        
                                        datosSession.agregarReporte(dr);
                                        JsfUtil.redirectNewTab("/CamilaPOS/Documento");
                                        break;

                                    case 2:
                                        String formato = (String) posService.getValorDefault(1, this.userSession.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia(), null, "POSFORMATO"+this.userSession.getCaja().getGENBODcodigo());
                                        this.parametros = new HashMap<>();
                                        this.parametros.put("genofiSecuencia", this.userSession.getCaja().getGENOFIcodigo());
                                        datosSession.agregarParametro("OFICINA", this.entity.findObject(CbpgenrOficina.class, this.parametros));
                                        datosSession.agregarParametro("CAJA", this.userSession.getCaja());
                                        datosSession.agregarParametro("USUARIO", this.userSession.getCbpsegmUsuario());
                                        datosSession.agregarParametro("TURNO", this.entity.find(CbpsegmTurno.class, t.getSegturCodigo()));
                                        datosSession.agregarParametro("FORMATO", formato);
                                        this.parametros = new HashMap<>();
                                        this.parametros.put("0", t.getSegturNumero());
                                        this.parametros.put("1", this.userSession.getCbpsegmUsuario().getSegusuLogin());
                                        datosSession.agregarParametro("IMPORTE", entity.findObject("SQL",QuerysPos.importe, this.parametros, Importe.class, Boolean.FALSE));
                                        datosSession.agregarParametro("ESTADISTICA", entity.findObject("SQL",QuerysPos.estadistica, this.parametros, Estadistica.class, Boolean.FALSE));
                                        datosSession.agregarParametro("TOTALES", entity.findObject("SQL",QuerysPos.formaPago, this.parametros, FormaPago.class, Boolean.FALSE));
                                        JsfUtil.redirectNewTab("/CamilaPOS/Cierre");
                                        break;
                                }

                            }
                        }
                    }else{
                        JsfUtil.messageWarning(null, "EXISTEN REGISTROS EN STAND BY", "");
                    }
                }else{
                    JsfUtil.messageWarning(null, "DEBE REGISTRAR VALORES DE CIERRE", "");
                }
            }else{
                JsfUtil.messageWarning(null, "NO EXISTE TURNO ACTIVO", "");
            }
        } catch (Exception e) {
            Logger.getLogger(RegistroTurno.class.getName()).log(Level.SEVERE, null, e);
        }
    }        
    
    public void cancelarTransaccion(){
        posService.eliminarTabla("X_CBPFACTCOB"+userSession.getCbpsegmUsuario().getSegusuLogin()+"PTOVENTA");
        posService.eliminarTabla("X_CBPFACT"+userSession.getCbpsegmUsuario().getSegusuLogin()+"PTOVENTA");
    }
    
    public void generarBitacora(){
        try{
            if(this.userSession.getTurno()!=null){
                CbpsegmTurno t= this.userSession.getTurno();
                if(datosSession!=null){
                    datosSession.borrarDatos();
                    datosSession.setPath(FacesContext.getCurrentInstance().getExternalContext().getRealPath("//"));
                    parametros = new HashMap<>();
                    parametros.put("0", t.getSegturNumero());
                    datosSession.agregarParametro("LOGS", entity.findAllSQL(QuerysPos.logBitacora, parametros, LogBitacora.class));                
                    JsfUtil.redirectNewTab("/CamilaPOS/BitacoraLog");
                    /*datosSession.setTieneDatasource(Boolean.FALSE);
                    datosSession.setNombreReporte("logCaja");
                    datosSession.agregarParametro("NUMERO_TURNO",t.getSegturNumero());
                    JsfUtil.redirectNewTab("/CamilaPOS/DocumentoHtml");*/
                }
            }
        } catch (Exception e) {
            Logger.getLogger(RegistroTurno.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public FactSesioncaja getSessionCaja() {
        return sessionCaja;
    }

    public void setSessionCaja(FactSesioncaja sessionCaja) {
        this.sessionCaja = sessionCaja;
    }

    public CbpgenrOficina getOficina() {
        return oficina;
    }

    public void setOficina(CbpgenrOficina oficina) {
        this.oficina = oficina;
    }

    public Importe getImporte() {
        return importe;
    }

    public void setImporte(Importe importe) {
        this.importe = importe;
    }

    public Estadistica getEstadistica() {
        return estadistica;
    }

    public void setEstadistica(Estadistica estadistica) {
        this.estadistica = estadistica;
    }

    public FormaPago getFormaPago() {
        return formaPago;
    }

    public void setFormaPago(FormaPago formaPago) {
        this.formaPago = formaPago;
    }

    public CbpsegmTurno getTurno() {
        return turno;
    }

    public void setTurno(CbpsegmTurno turno) {
        this.turno = turno;
    }

}
