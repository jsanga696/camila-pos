/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.comercial.pos.reportes;

import com.gma.camilabp.domain.CbpgenrBodega;
import com.gma.camilabp.domain.CbpgenrOficina;
import com.gma.camilabp.domain.InvrCablistaprecio;
import com.gma.camilabp.model.logica.PrecioModel;
import com.gma.camilabp.web.comercial.compras.IngresoBodegaView;
import com.gma.camilabp.web.lazy.CbpgenrBodegaLazy;
import com.gma.camilabp.web.lazy.CbpgenrOficinaLazyTest;
import com.gma.camilabp.web.lazy.InvrCablistaprecioLazy;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.service.GeneralLocal;
import com.gma.camilabp.web.session.ServletSession;
import com.gma.camilabp.web.session.UserSession;
import com.gma.camilabp.web.util.JsfUtil;
import com.gma.camilabp.web.util.Utilidades;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author userdb5
 */
@Named(value = "impresionPreciosView")
@ViewScoped
public class ImpresionPreciosView implements Serializable {

    @Inject
    private UserSession session;
    @EJB
    private EntityManagerLocal entity;
    @EJB
    private GeneralLocal generalEjb;

    @Inject
    private ServletSession datosSession;
    
    private HashMap dataIngreso;
    private CbpgenrOficinaLazyTest oficinasLazy;
    private CbpgenrBodegaLazy bodegasLazy;
    private InvrCablistaprecioLazy listaPreciosLazy;
    private List<PrecioModel> l;

    @PostConstruct
    public void initView() {
        dataIngreso = new HashMap();
        this.oficinasLazy = new CbpgenrOficinaLazyTest(Utilidades.getIdsOfList(this.session.getOficinas()));
        dataIngreso.put("oficina", (CbpgenrOficina) Utilidades.getFirstElement(this.session.getOficinas()));
        dataIngreso.put("valOfi", ((CbpgenrOficina) dataIngreso.get("oficina")).getGenofiSecuencia());
        dataIngreso.put("bodega", new CbpgenrBodega());
        dataIngreso.put("listaPrecio", new InvrCablistaprecio());
        dataIngreso.put("valEst", "");
        dataIngreso.put("valLpr", "0");
        dataIngreso.put("no_impresiones", "1");
    }

    public void busquedaOficina() {
        try {
            Object o = this.generalEjb.consultarOficina((String) this.dataIngreso.get("valOfi"));
            if (o != null) {
                this.dataIngreso.put("oficina", (CbpgenrOficina) o);
            } else {
                JsfUtil.update("frmdlgOficina");
                JsfUtil.executeJS("PF('dlgOficina').show();");
            }
        } catch (Exception e) {
            Logger.getLogger(IngresoBodegaView.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public void onListaPrecioSelect(SelectEvent event) {
        dataIngreso.put("listaPrecio", (InvrCablistaprecio) event.getObject());
        dataIngreso.put("valLpr", ((InvrCablistaprecio) dataIngreso.get("listaPrecio")).getINVLPRsecuencia());
        JsfUtil.executeJS("PF('dlgListaPrecios').hide()");
    }

    public void onBodegaSelect(SelectEvent event) {
        dataIngreso.put("bodega", (CbpgenrBodega) event.getObject());
        dataIngreso.put("valEst", ((CbpgenrBodega) dataIngreso.get("bodega")).getGenbodSecuencia());
        JsfUtil.executeJS("PF('wdBodegas').hide()");
    }

    public void selectOficina(SelectEvent event) {
        dataIngreso.put("valOfi", ((CbpgenrOficina) dataIngreso.get("oficina")).getGenofiSecuencia());
    }

    public void busquedaBodega() {
        try {
            HashMap m = new HashMap();
            m.put("genbodSecuencia", dataIngreso.get("valEst"));
            m.put("genofiCodigo", ((CbpgenrOficina) dataIngreso.get("oficina")));
            Object o = this.entity.findObject(CbpgenrBodega.class, m);
            if (o != null) {
                this.dataIngreso.put("bodega", (CbpgenrBodega) o);
            } else {
                bodegasLazy = new CbpgenrBodegaLazy(((CbpgenrOficina) dataIngreso.get("oficina")).getGenofiCodigo());
                JsfUtil.update("bodegasFrm");
                JsfUtil.executeJS("PF('wdBodegas').show();");
            }
        } catch (Exception e) {
            Logger.getLogger(IngresoBodegaView.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public void generarReporte() {
        List<PrecioModel> lista = new ArrayList();
        datosSession.borrarDatos();
        datosSession.setTieneDatasource(Boolean.TRUE);
        datosSession.setNombreReporte("activos/etiquetas_precio");
        
        for(PrecioModel pm : l){
            for(int i = 0; i < Integer.parseInt(dataIngreso.get("no_impresiones")+""); i++){
                lista.add(pm);
            }
        }
        
        datosSession.setDataSource(lista);
        JsfUtil.redirectNewTab("/CamilaPOS/Documento");
    }

    public void busquedaListaPrecios() {
        try {
            if (dataIngreso.get("bodega") == null) {
                JsfUtil.messageInfo(null, "Debe seleccionar una bodega", "");
                return;
            }
            HashMap m = new HashMap();
            if (dataIngreso.get("valLpr") == null || dataIngreso.get("valLpr") == "") {
                listaPreciosLazy = new InvrCablistaprecioLazy(((CbpgenrOficina) dataIngreso.get("oficina")).getGenofiSecuencia(),
                        this.session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia());
                JsfUtil.update("frmdlgListaPrecios");
                JsfUtil.executeJS("PF('dlgListaPrecios').show();");
                return;
            }
            m.put("iNVLPRsecuencia", Long.parseLong(dataIngreso.get("valLpr").toString()));
            m.put("genofiCodigo", ((CbpgenrOficina) dataIngreso.get("oficina")).getGenofiSecuencia());
            m.put("gENCIAcodigo", this.session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia());
            Object o = this.entity.findObject(InvrCablistaprecio.class, m);
            if (o != null) {
                this.dataIngreso.put("listaPrecio", (InvrCablistaprecio) o);
            } else {
                listaPreciosLazy = new InvrCablistaprecioLazy(((CbpgenrOficina) dataIngreso.get("oficina")).getGenofiSecuencia(),
                        this.session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia());
                JsfUtil.update("frmdlgListaPrecios");
                JsfUtil.executeJS("PF('dlgListaPrecios').show();");
            }
        } catch (Exception e) {
            Logger.getLogger(ImpresionPreciosView.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public void verDatos() {
        try {
            if (((CbpgenrBodega) dataIngreso.get("bodega")).getGenbodCodigo() == null
                    || ((InvrCablistaprecio) dataIngreso.get("listaPrecio")).getINVLPRsecuencia() == null) {
                return;
            }
            HashMap m = new HashMap();
            m.put("0", dataIngreso.get("valEst"));
            m.put("1", Integer.parseInt("" + dataIngreso.get("valLpr")));

            l = (List<PrecioModel>) entity.findAllSQL("SELECT item.INVITM_codigo as item_cod, item.INVITM_nombre as item_nombre,  \n"
                    + "ROUND(det_lp.INVITM_prelista, 2) as valor, ROUND(impto.GENIMP_porcentaje, 2) as imp,"
                    + "(det_lp.INVITM_prelista + det_lp.INVITM_prelista*impto.GENIMP_porcentaje) as valorConImp FROM CBPGENR_BODEGA bodega \n"
                    + "INNER JOIN CBPGENR_OFICINA oficina ON bodega.genofi_codigo = oficina.genofi_codigo \n"
                    + "INNER JOIN CBPGENR_COMPANIA compania ON compania.gencia_codigo = oficina.gencia_codigo \n"
                    + "INNER JOIN INVM_ITEMXBODEGA itmbdg ON compania.gencia_secuencia = itmbdg.GENCIA_codigo AND \n"
                    + "itmbdg.GENOFI_codigo = oficina.genofi_secuencia AND bodega.genbod_secuencia = itmbdg.GENBOD_codigo \n"
                    + "INNER JOIN INVM_ITEM item ON item.GENCIA_codigo = itmbdg.GENCIA_codigo AND itmbdg.INVITM_codigo = item.INVITM_codigo \n"
                    + "INNER JOIN INVR_detLISTAPRECIO det_lp ON item.INVITM_codigo = det_lp.INVITM_codigo \n"
                    + "INNER JOIN INVR_CABLISTAPRECIO cab_lista ON cab_lista.INVLPR_secuencia = det_lp.INVLPR_secuencia \n"
                    + "AND getdate() BETWEEN cab_lista.INVLPR_fechadesde AND cab_lista.INVLPR_fechahasta \n"
                    + "AND cab_lista.GENCIA_codigo = compania.gencia_secuencia AND cab_lista.GENOFI_codigo = oficina.genofi_secuencia \n"
                    + "INNER JOIN INVR_IMPTOXPROD impxp ON impxp.INVITM_codigo = item.INVITM_codigo \n"
                    + "INNER JOIN GENR_IMPUESTO impto ON impto.GENIMP_codigo = impxp.GENIMP_codigo \n"
                    + "WHERE bodega.genbod_secuencia = ? AND det_lp.INVLPR_secuencia = ? AND \n"
                    + "getdate() BETWEEN impto.GENIMP_fechadesde AND impto.GENIMP_fechahasta", m, PrecioModel.class);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public HashMap getDataIngreso() {
        return dataIngreso;
    }

    public void setDataIngreso(HashMap dataIngreso) {
        this.dataIngreso = dataIngreso;
    }

    public CbpgenrOficinaLazyTest getOficinasLazy() {
        return oficinasLazy;
    }

    public void setOficinasLazy(CbpgenrOficinaLazyTest oficinasLazy) {
        this.oficinasLazy = oficinasLazy;
    }

    public CbpgenrBodegaLazy getBodegasLazy() {
        return bodegasLazy;
    }

    public void setBodegasLazy(CbpgenrBodegaLazy bodegasLazy) {
        this.bodegasLazy = bodegasLazy;
    }

    public InvrCablistaprecioLazy getListaPreciosLazy() {
        return listaPreciosLazy;
    }

    public void setListaPreciosLazy(InvrCablistaprecioLazy listaPreciosLazy) {
        this.listaPreciosLazy = listaPreciosLazy;
    }

    public List<PrecioModel> getL() {
        return l;
    }

    public void setL(List<PrecioModel> l) {
        this.l = l;
    }

}
