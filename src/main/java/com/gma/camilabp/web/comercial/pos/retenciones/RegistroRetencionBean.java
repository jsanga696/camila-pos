/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.comercial.pos.retenciones;

import com.gma.camilabp.domain.CbpgenrCompania;
import com.gma.camilabp.domain.FactCabvtacaja;
import com.gma.camilabp.model.logica.CxprRetencionesTemp;
import com.gma.camilabp.web.comercial.pos.Transacciones;
import com.gma.camilabp.web.service.RetencionesServiceLocal;
import com.gma.camilabp.web.session.ServletSession;
import com.gma.camilabp.web.session.UserSession;
import com.gma.camilabp.web.util.JsfUtil;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

/**
 *
 * @author userdb2
 */
@Named(value = "registroRetencionBean")
@ViewScoped
public class RegistroRetencionBean implements Serializable {

    private CxprRetencionesTemp retencionesTem;

    @EJB
    private RetencionesServiceLocal retencionesService;
    @Inject
    private ServletSession datosSession;
    @Inject
    private UserSession session;

    @PostConstruct
    public void initView() {
        retencionesTem = retencionesService.getRetencion(null);
        if (retencionesTem == null) {
            retencionesTem = new CxprRetencionesTemp();
        }
    }

    public CxprRetencionesTemp getRetencionesTem() {
        return retencionesTem;
    }

    public void setRetencionesTem(CxprRetencionesTemp retencionesTem) {
        this.retencionesTem = retencionesTem;
    }

    /**
     * Busca los datos de la factura
     */
    public void buscarFactura() {
        if (retencionesTem.getNum_factura() == null || retencionesTem.getNum_factura().trim().length() == 0) {
            JsfUtil.messageError(null, "Debe ingresar el número de factura.", "");
            return;
        }
        
        String temp = retencionesTem.getNum_factura();
        if(retencionesService.tieneRetencion(temp)){
            JsfUtil.messageError(null, "Factura tiene una retencion aplicada.", "");
            return;
        }
        retencionesTem = retencionesService.getRetencion(temp);
        if (retencionesTem == null) {
            JsfUtil.messageWarning(null, "NO SE ENCONTRO REGISTRO DE LA FACTURA", "");
            retencionesTem = new CxprRetencionesTemp();
            retencionesTem.setNum_factura(temp);
        }
    }

    public void cancelarRetencion() {
        if (retencionesService.getEliminarDatosTemporales("T")) {
            cerrar();
        } else {
            JsfUtil.messageWarning(null, "OCURRIO UN ERROR AL INTENTAR ELIMINAR DATOS TEMPORALES", "");
        }
    }

    public void respaldarDatos() {
        if (retencionesTem != null && retencionesTem.getSecuenciafactura() != null) {
            Boolean ok = retencionesService.respaldarRetencion(retencionesTem, false);
            System.out.println("Retencion respaldada " + ok);
        }
    }

    public void aceptar() {
        if (!retencionesTem.getFecha_emision().equals(retencionesTem.getFecha_vigencia())) {
            JsfUtil.messageError(null, "La fecha de emisión no puede ser menor a la fecha de vigencia.", "");
            return;
        }
        if (retencionesService.tieneRetencion(retencionesTem.getNum_factura())) {
            JsfUtil.messageError(null, "Factura tiene una retencion aplicada.", "");
            return;
        }

        Boolean ok = retencionesService.registarDatosRetencion(retencionesTem);
        // SI LA TRANSACCION SE EJECUTO CON EXITO ELIMINAMOS LOS DATOS TEMPORALES
        if (ok) {
            JsfUtil.messageInfo(null, "La retención se guardo correctamente.", "");
            // imprimimos el comprobante de la retencion
            this.imprimirFactura();
            // cambiamos el estado de la retencion temporal
            this.cancelarRetencion();

        }
    }

    public void imprimirFactura() {
        try {
            if (datosSession != null) {
                String numReg = retencionesTem.getNum_factura().substring(0, retencionesTem.getNum_factura().lastIndexOf("-"));
                datosSession.setTieneDatasource(Boolean.TRUE);
                datosSession.setNombreReporte("retencion/canjeRetencion");
                //datosSession.agregarParametro("ID_OPER", session.getUsuarioOficina().getSegusuCodigo().getSegusuCodigo().toString());
                //datosSession.agregarParametro("ID_TIENDA", session.getUsuarioOficina().getGenofiCodigo().getGenofiSecuencia());
                datosSession.agregarParametro("ID_OPER", session.getCbpsegmUsuario().getSegusuCodigo().toString());
                datosSession.agregarParametro("ID_TIENDA", session.getCaja().getGENOFIcodigo());
                datosSession.agregarParametro("NUM_REGIS", numReg);
                datosSession.agregarParametro("TURNO", numReg + ":" + session.getTurno().getSegturNumero());
                datosSession.agregarParametro("EFECTIVO", retencionesTem.getTotal());
                datosSession.agregarParametro("COMPROBANTE_RET", retencionesTem.getSRIRETcodigo());
                datosSession.agregarParametro("NUM_FACTURA", retencionesTem.getNum_factura());
                JsfUtil.redirectNewTab("/CamilaPOS/Documento");
            }
        } catch (Exception e) {
            Logger.getLogger(RegistroRetencionBean.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public void cerrar() {
        JsfUtil.redirectFaces("/faces/index.xhtml");
    }
}
