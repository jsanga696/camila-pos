/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.comercial.report;

import java.io.Serializable;

/**
 *
 * @author userdb4
 */
public class TotalesBodega implements Serializable{
    
    private String descripcion;
    
    private int producto;
     
    private int unidades;
     
    private int costounitario;
 
    private int costoinventario;
    
    public TotalesBodega() {}
    
    public TotalesBodega( int producto, String descripcion, int unidades, int costounitario, int costoinventario ) {
        this.producto = producto;
        this.descripcion = descripcion;
        this.unidades = unidades;
        this.costounitario = costounitario;
        this.costoinventario = costoinventario;
    }
 
    public int getProducto() {
        return producto;
    }
 
    public void setProducto(int producto) {
        this.producto = producto;
    }
 
    public int getUnidades() {
        return unidades;
    }
 
    public void setUnidades(int unidades) {
        this.unidades = unidades;
    }
    
    public int getCostounitario() {
        return costounitario;
    }
 
    public void setCostounitario(int costounitario) {
        this.costounitario = costounitario;
    }
    
    public int getCostoinventario() {
        return costoinventario;
    }
 
    public void setCostoinventario(int costoinventario) {
        this.costoinventario = costoinventario;
    }
 
    public String getDescripcion() {
        return descripcion;
    }
 
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
