/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.comercial.inventario;

import com.gma.camilabp.domain.CbpgenrBodega;
import com.gma.camilabp.domain.CbpgenrOficina;
import com.gma.camilabp.domain.GenrDocumento;
import com.gma.camilabp.domain.InvmItem;
import com.gma.camilabp.domain.InvmItemPK;
import com.gma.camilabp.domain.InvmItemxbodega;
import com.gma.camilabp.domain.InvmItemxbodegaPK;
import com.gma.camilabp.domain.InvtCabotros;
import com.gma.camilabp.domain.InvtDetotros;
import com.gma.camilabp.model.consultas.ResultadoTransferenciaAjuste;
import com.gma.camilabp.model.consultas.TempAjusteTransferenciaInv;
import com.gma.camilabp.model.logica.ItemCantidades;
import com.gma.camilabp.model.logica.TransferenciaAjusteInventario;
import com.gma.camilabp.sql.QuerysPos;
import com.gma.camilabp.web.lazy.CbpgenrOficinaLazyTest;
import com.gma.camilabp.web.lazy.InvmItemxbodegaLazy;
import com.gma.camilabp.web.lazy.InvtCabotrosLazy;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.service.GeneralLocal;
import com.gma.camilabp.web.service.InventarioLocal;
import com.gma.camilabp.web.service.PuntoVentaLocal;
import com.gma.camilabp.web.session.ServletSession;
import com.gma.camilabp.web.session.UserSession;
import com.gma.camilabp.web.util.JsfUtil;
import com.gma.camilabp.web.util.Utilidades;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

/**
 *
 * @author Acer
 */
@Named(value = "transferenciaInventario")
@ViewScoped
public class TransferenciaInventario implements Serializable{
    @Inject
    private UserSession userSession;
    @Inject
    private ServletSession datosSession;
    @EJB
    private EntityManagerLocal entity;
    @EJB
    private GeneralLocal generalService;
    @EJB
    private PuntoVentaLocal posService;
    @EJB
    private InventarioLocal invService;
    
    protected CbpgenrOficinaLazyTest oficinasLazy;
    protected TransferenciaAjusteInventario transferenciaModel;
    protected BigDecimal numTransaccion;
    protected List<CbpgenrBodega> bodegaOrigenList;
    protected List<CbpgenrBodega> bodegaDestinoList;
    protected InvmItemxbodegaLazy itemsBodegaLazy;
    protected List<GenrDocumento> tiposMovimiento;
    protected InvmItem item;
    protected InvmItemxbodega itemLazy;
    protected List<TempAjusteTransferenciaInv> tempTransferenciaInvList;
    protected TempAjusteTransferenciaInv tempTransferenciaInv;
    protected InvtCabotrosLazy cabotrosLazy;
    protected InvtCabotrosLazy cabotrosEgresoLazy;
    protected InvtCabotros cabOtros;
    protected List<InvtDetotros> detOtros; 
    
    private Map<String, Object> parametros;
    private String nombreTabla;

    @PostConstruct
    public void init() {
        try{
            this.nombreTabla="X_CBPTRANSINV"+this.userSession.getCbpsegmUsuario().getSegusuLogin()+"PTOVENTA";
            this.numTransaccion = (this.generalService.secuenciaModulo("INV", this.userSession.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia())).add(BigDecimal.ONE);
            this.transferenciaModel = new TransferenciaAjusteInventario();
            this.oficinasLazy = new CbpgenrOficinaLazyTest(Utilidades.getIdsOfList(this.userSession.getOficinas()));
            this.item =new InvmItem();
            this.tempTransferenciaInvList = this.invService.getAjusteTransferenciaTemporalList(this.nombreTabla);
            if(this.tempTransferenciaInvList!=null && !this.tempTransferenciaInvList.isEmpty()){
                this.parametros= new HashMap<>();
                this.parametros.put("genciaCodigo",this.userSession.getCbpsegmUsuario().getGenciaCodigo().getGenciaCodigo());
                this.parametros.put("genofiSecuencia", (String)this.entity.findObject("SQL", QuerysPos.selectDistinct.replaceAll("column", "GENOFI_codigo").replaceAll("table", nombreTabla), null, null, Boolean.TRUE));
                this.transferenciaModel.setOficina(this.entity.findObject(CbpgenrOficina.class, this.parametros));
                this.parametros= new HashMap<>();
                this.parametros.put("genofiCodigo", this.transferenciaModel.getOficina());
                this.parametros.put("genbodSecuencia", (String)this.entity.findObject("SQL", QuerysPos.selectDistinct.replaceAll("column", "GENBOD_codigo").replaceAll("table", nombreTabla), null, null, Boolean.TRUE));
                this.transferenciaModel.setBodegaOrigen(this.entity.findObject(CbpgenrBodega.class, this.parametros));
                this.parametros.put("genbodSecuencia", (String)this.entity.findObject("SQL", QuerysPos.selectDistinct.replaceAll("column", "GENBODDES_codigo").replaceAll("table", nombreTabla), null, null, Boolean.TRUE));
                this.transferenciaModel.setBodegaDestino(this.entity.findObject(CbpgenrBodega.class, this.parametros));
            }else{
                this.transferenciaModel.setOficina((CbpgenrOficina)Utilidades.getFirstElement(this.userSession.getOficinas()));             
            }
            if(this.transferenciaModel.getOficina().getGenofiCodigo()!=null){
                this.seleccionOficina();
            }
            this.cabotrosLazy = new InvtCabotrosLazy(this.invService.tipoTransferencias(),Utilidades.getSecuenciaList(this.userSession.getOficinas()),null);
        } catch (Exception e) {
            Logger.getLogger(TransferenciaInventario.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void busquedaOficina(){
        try{
            this.transferenciaModel.setOficina(this.generalService.consultarOficina(this.transferenciaModel.getSecuenciaOficina()));
            this.bodegaOrigenList = null;
            this.bodegaDestinoList = null;
            if(this.transferenciaModel.getOficina().getGenofiCodigo()!=null){
                this.seleccionOficina();
            }else{
                this.seleccionBodega();
            }
        } catch (Exception e) {
            Logger.getLogger(TransferenciaInventario.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void cargarMovimientos(){
        this.parametros= new HashMap<>();        
        this.parametros.put("genrDocumentoPK.gENCIAcodigo",this.userSession.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia());
        this.parametros.put("genrDocumentoPK.gENOFIcodigo",this.transferenciaModel.getOficina().getGenofiSecuencia());
        this.parametros.put("genrDocumentoPK.gENMODcodigo", "INV");
        this.parametros.put("gENTDOestado", 'A');            
        this.parametros.put("genrDocumentoPK.gENTDOcodigo", this.invService.tipoTransferencias());
        this.tiposMovimiento = this.entity.findAllByParameter(GenrDocumento.class, parametros,null);
    }
    
    public void seleccionOficina(){
        this.transferenciaModel.setSecuenciaOficina(this.transferenciaModel.getOficina().getGenofiSecuencia());
        this.cargarMovimientos();
        this.transferenciaModel.setTipoMovimiento((GenrDocumento) Utilidades.getFirstElement(this.tiposMovimiento));
        this.cargarBodegas();
    }
    
    public void cargarBodegas(){
        try{
            this.parametros= new HashMap<>();
            this.parametros.put("genofiCodigo", this.transferenciaModel.getOficina());
            this.bodegaOrigenList = this.entity.findAllByParameter(CbpgenrBodega.class, this.parametros, null);
            this.bodegaDestinoList = this.entity.findAllByParameter(CbpgenrBodega.class, this.parametros, null);
        } catch (Exception e) {
            Logger.getLogger(TransferenciaInventario.class.getName()).log(Level.SEVERE, null, e);
        }
    }    
    
    public void seleccionBodega(){
        try{
            this.tempTransferenciaInvList = new ArrayList<>();
            this.itemsBodegaLazy = new InvmItemxbodegaLazy(this.transferenciaModel.getBodegaOrigen()!=null?this.transferenciaModel.getBodegaOrigen().getGenbodSecuencia():"", null, null);
        } catch (Exception e) {
            Logger.getLogger(TransferenciaInventario.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void busquedaItem(){
        try{
            if(this.transferenciaModel.getBodegaOrigen()!=null){
                if(this.item.getCodigoItem()==null && this.item.getINVITMnombre()==null&& this.item.getINVITMcodigbarra()==null){
                    this.itemsBodegaLazy = new InvmItemxbodegaLazy(this.transferenciaModel.getBodegaOrigen().getGenbodSecuencia(), null, null);
                    JsfUtil.update("frmdlgItemBodega");
                    JsfUtil.executeJS("PF('dlgItemBodega').show();");
                }else{
                    List<InvmItem> i= posService.consultarItem(this.item.getCodigoItem()!=null?this.item.getCodigoItem().trim():null, this.item.getINVITMnombre()!=null?this.item.getINVITMnombre().trim().toUpperCase():null,this.item.getINVITMcodigbarra()!=null?this.item.getINVITMcodigbarra().trim():null);
                    if(i==null || i.isEmpty()){
                        JsfUtil.messageInfo(null, "REGISTRO NO ENCONTRADO", "");
                    }else{
                        if(i.size()==1){
                            this.item= i.get(0);
                            this.actualizarCodigoItem();
                        }else{
                            this.itemsBodegaLazy = new InvmItemxbodegaLazy(this.transferenciaModel.getBodegaOrigen().getGenbodSecuencia(),this.item.getCodigoItem()!=null?this.item.getCodigoItem().trim():null,this.item.getINVITMnombre()!=null?this.item.getINVITMnombre().trim().toUpperCase():null);
                            JsfUtil.update("frmdlgItemBodega");
                            JsfUtil.executeJS("PF('dlgItemBodega').show();");
                        }
                    }
                }
            }else{
                JsfUtil.messageInfo(null, "SELECCIONE UNA BODEGA", "");
            }
        } catch (Exception e) {
            Logger.getLogger(TransferenciaInventario.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void actualizarDatos(){
        if(this.tempTransferenciaInv!=null){
            this.item=this.entity.find(InvmItem.class, new InvmItemPK(this.tempTransferenciaInv.getGENCIA_codigo(), this.tempTransferenciaInv.getINVITM_codigo()));
            ItemCantidades ic = Utilidades.procesarCantidades(tempTransferenciaInv.getINVDOT_cantunida(), this.item.getINVITMunidembala(), tempTransferenciaInv.getINVDOT_cantcajas());
            tempTransferenciaInv.setINVDOT_cantcajas(ic.getCaja());
            tempTransferenciaInv.setINVDOT_cantunida(ic.getUnidad());
            tempTransferenciaInv.setINVDOT_cantifunci(ic.getFuncional());
            tempTransferenciaInv.setINVITM_uniembala(ic.getFactor());
            tempTransferenciaInv.setINVDOT_precio(this.item.getINVITMcostoprom1());
            tempTransferenciaInv.setINVDOT_subtotamov(this.item.getINVITMcostoprom1().multiply(tempTransferenciaInv.getINVDOT_cantifunci()));
            this.tempTransferenciaInvList.set(this.tempTransferenciaInvList.indexOf(tempTransferenciaInv), tempTransferenciaInv);            
            this.limpiarItem();
        }        
    }
    
    public void agregarRegistroTemporal(){
        try{
            BigDecimal cantidadTemporal= this.item.getCantidadFuncional();
            if(this.tempTransferenciaInvList==null)
                this.tempTransferenciaInvList = new ArrayList<>();
            for (TempAjusteTransferenciaInv t : this.tempTransferenciaInvList) {
                if(t.getINVITM_codigo().trim().equalsIgnoreCase(this.item.getInvmItemPK().getINVITMcodigo().trim())){
                    cantidadTemporal= cantidadTemporal.add(t.getINVDOT_cantifunci());
                    this.tempTransferenciaInvList.remove(t);
                    break;
                }
            }
            int cajas= cantidadTemporal.intValue()/this.item.getINVITMunidembala().intValue();
            int unidades= cantidadTemporal.intValue()%this.item.getINVITMunidembala().intValue();
            this.tempTransferenciaInvList.add(new TempAjusteTransferenciaInv(null, null, null, this.item.getInvmItemPK().getGENCIAcodigo(), this.transferenciaModel.getOficina().getGenofiSecuencia(), 
                    this.transferenciaModel.getTipoMovimiento().getGenrDocumentoPK().getGENMODcodigo(), this.transferenciaModel.getTipoMovimiento().getGenrDocumentoPK().getGENTDOcodigo(), null, 
                    this.transferenciaModel.getBodegaOrigen().getGenbodSecuencia(), this.item.getInvmItemPK().getINVITMcodigo(), null, null, this.item.getINVITMunidembala(),
                    this.item.getINVITMcostoprom1(), cantidadTemporal, new BigDecimal(cajas), 
                    new BigDecimal(unidades) , null, null, this.item.getINVITMcostoprom1().multiply(cantidadTemporal), this.item.getINVITMnombre(),this.transferenciaModel.getBodegaDestino().getGenbodSecuencia()));
        } catch (Exception e) {
            Logger.getLogger(TransferenciaInventario.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public BigDecimal obtenerCantidad(String codigoItem){
        if(this.tempTransferenciaInvList!=null && this.tempTransferenciaInvList.isEmpty())
            for (TempAjusteTransferenciaInv t : this.tempTransferenciaInvList) {
                if(t.getINVITM_codigo().trim().equalsIgnoreCase(codigoItem.trim())){
                    return t.getINVDOT_cantifunci();
                }
            }
        return BigDecimal.ZERO;
    }
    
    public void actualizarCodigoItem(){
        try{
            if(this.itemLazy!=null){
                this.item=this.entity.find(InvmItem.class, new InvmItemPK(this.itemLazy.getInvmItemxbodegaPK().getGENCIAcodigo(), this.itemLazy.getInvmItemxbodegaPK().getINVITMcodigo()));
            }
            this.itemLazy=null;
            this.item.setCodigoItem(this.item.getInvmItemPK().getINVITMcodigo());
            this.item.setCantidadFuncional(new BigDecimal("1"));
        } catch (Exception e) {
            Logger.getLogger(TransferenciaInventario.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void limpiarItem(){
        this.item= new InvmItem();
    }
    
    public void agregarItem(){
        try{
            if(this.item!=null && this.item.getInvmItemPK()!=null){
                if(this.item.getCantidadFuncional()!=null && this.item.getCantidadFuncional().compareTo(BigDecimal.ZERO)>0){
                    InvmItemxbodega ib = this.entity.find(InvmItemxbodega.class, new InvmItemxbodegaPK(this.item.getInvmItemPK().getGENCIAcodigo(), this.transferenciaModel.getOficina().getGenofiSecuencia(), 
                            this.transferenciaModel.getBodegaOrigen().getGenbodSecuencia(), this.item.getInvmItemPK().getINVITMcodigo()));
                    if(ib!=null && ib.getINVIBOstock().compareTo(this.item.getCantidadFuncional().add(this.obtenerCantidad(this.item.getInvmItemPK().getINVITMcodigo())))>=0){
                        this.agregarRegistroTemporal();
                        this.item = new InvmItem();
                    }else{
                        JsfUtil.messageWarning(null, "NO EXISTE STOCK SUFUFICIENTE", "");
                    }
                }else{
                    JsfUtil.messageWarning(null, "CANTIDAD MAYOR A CERO", "");
                }
            }else{
                JsfUtil.messageWarning(null, "REALIZAR BUSQUEDA", "");
            }
        } catch (Exception e) {
            Logger.getLogger(TransferenciaInventario.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void elimiarItemRegistro(TempAjusteTransferenciaInv i){
        try{
            this.tempTransferenciaInvList.remove(i);
        } catch (Exception e) {
            Logger.getLogger(TransferenciaInventario.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void previsualizacionDatos(){
        try{
            if(this.tempTransferenciaInvList!=null && !this.tempTransferenciaInvList.isEmpty() && this.transferenciaModel.getBodegaOrigen()!=null
                    && (this.transferenciaModel.getTipoMovimiento().getGenrDocumentoPK().getGENTDOcodigo().equalsIgnoreCase("TEP")?(this.transferenciaModel.getBodegaDestino()!=null):true)){
                if(this.transferenciaModel.getTipoMovimiento().getGenrDocumentoPK().getGENTDOcodigo().equalsIgnoreCase("TEP") 
                        && this.transferenciaModel.getBodegaOrigen()!=null && this.transferenciaModel.getBodegaDestino()!=null
                        && this.transferenciaModel.getBodegaOrigen().equals(this.transferenciaModel.getBodegaDestino())){
                    JsfUtil.messageWarning(null, "BODEGAS DEBE SER DIFERENTES", "");
                    return;                    
                }
                this.posService.registroTablaTemporal("SP_INVP_CREARTEMPORAL", this.nombreTabla);
                this.invService.insertTableTemp(this.nombreTabla, this.tempTransferenciaInvList,this.transferenciaModel);
                for (TempAjusteTransferenciaInv tempAjusteTransferenciaInv : this.tempTransferenciaInvList) {
                    System.out.println(tempAjusteTransferenciaInv.getINVDOT_faltantecantifunci());
                }
                List<TempAjusteTransferenciaInv> lt = this.tempTransferenciaInvList.stream().filter(t->t.getINVDOT_faltantecantifunci().compareTo(BigDecimal.ZERO)>0).collect(Collectors.toList());
                ResultadoTransferenciaAjuste resultado=this.invService.registrarTransferenciaAjusteInventario(this.nombreTabla, this.transferenciaModel, this.userSession.getCbpsegmUsuario().getSegusuLogin());
                if(resultado!=null){
                    System.out.println(resultado.getInvcotNumsecuenc()+" - "+resultado.getInvcotNumdocumen()+" - "+resultado.getInvNumbodega());
                    if(lt!=null && !lt.isEmpty()){
                        System.out.println("REALIZA AJUSTE");
                        for (TempAjusteTransferenciaInv t : lt) {
                            t.setGENTDO_codigo("AJE");
                            t.setINVCOT_motvajuste("05");
                            t.setINVDOT_cantcajas(new BigDecimal(t.getINVDOT_faltantecantifunci().intValue()/t.getINVITM_uniembala().intValue()));
                            t.setINVDOT_cantunida(new BigDecimal(t.getINVDOT_faltantecantifunci().intValue()%t.getINVITM_uniembala().intValue()));
                            t.setINVDOT_cantifunci(t.getINVDOT_faltantecantifunci());
                            t.setINVDOT_subtotamov(t.getINVDOT_precio().multiply(t.getINVDOT_cantifunci()));
                        }
                        String nombreTablaAjuste = "X_CBPAJUSINV"+this.userSession.getCbpsegmUsuario().getSegusuLogin()+"PTOVENTA";
                        this.transferenciaModel.setTipoMovimiento(new GenrDocumento(this.userSession.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia(), this.transferenciaModel.getOficina().getGenofiSecuencia(), "INV", "AJE"));
                        this.posService.registroTablaTemporal("SP_INVP_CREARTEMPORAL", nombreTablaAjuste);
                        this.invService.insertTableTemp(nombreTablaAjuste, lt,this.transferenciaModel);
                        resultado=this.invService.registrarTransferenciaAjusteInventario(nombreTablaAjuste, this.transferenciaModel, this.userSession.getCbpsegmUsuario().getSegusuLogin());
                        System.out.println(resultado.getInvcotNumsecuenc()+" - "+resultado.getInvcotNumdocumen()+" - "+resultado.getInvNumbodega());
                    }
                    this.posService.eliminarTabla(this.nombreTabla);
                    this.seleccionarTransferencia(new Long(resultado.getInvcotNumsecuenc()));
                    JsfUtil.executeJS("PF('dlgTransferencia').show();");
                    this.init();
                }else{
                    JsfUtil.messageError(null, "ERROR AL GRABAR", "");
                }
            }else{
                JsfUtil.messageWarning(null, "DATOS INCOMPLETOS", "");
            }
        } catch (Exception e) {
            Logger.getLogger(TransferenciaInventario.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void seleccionarTransferencia(Long secuencia){
        this.cabOtros = this.entity.find(InvtCabotros.class, secuencia);
        this.parametros = new HashMap<>();
        this.parametros.put("invtDetotrosPK.iNVCOTnumsecuenc", secuencia);
        this.detOtros = this.entity.findAllByParameter(InvtDetotros.class, this.parametros, null);
    }
    
    public void buscarTransferencia(){
        try{
            InvtCabotros cab=null;
            if(this.transferenciaModel.getSecuenciaCabOtros()!=null){
                this.parametros = new HashMap<>();
                this.parametros.put("iNVCOTnumsecuenc", this.transferenciaModel.getSecuenciaCabOtros());
                this.parametros.put("gENOFIcodigo", this.transferenciaModel.getOficina().getGenofiSecuencia());
                this.parametros.put("gENBODcodigorela", this.transferenciaModel.getBodegaOrigen().getGenbodSecuencia());
                this.parametros.put("gENTDOcodigo", "TEP");
                cab = this.entity.findObject(InvtCabotros.class, this.parametros);
            }
            if(cab!=null){
                this.transferenciaModel.setCabOtros(cab);
                this.seleccionarTransferencia();
            }else{
                List<String> c = new ArrayList<>();
                List<String> o = new ArrayList<>();
                c.add("TEP");
                o.add(this.transferenciaModel.getOficina().getGenofiSecuencia());
                this.cabotrosEgresoLazy = new InvtCabotrosLazy(c, o, this.transferenciaModel.getBodegaOrigen().getGenbodSecuencia());
                JsfUtil.update("frmdlgTransferencias");
                JsfUtil.executeJS("PF('dlgTransferencias').show();");
            }
        } catch (Exception e) {
            Logger.getLogger(TransferenciaInventario.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void seleccionarTransferencia(){
        try{
            this.transferenciaModel.setSecuenciaCabOtros(this.transferenciaModel.getCabOtros().getINVCOTnumsecuenc());
            this.parametros = new HashMap<>();
            this.parametros.put("invtDetotrosPK.iNVCOTnumsecuenc", this.transferenciaModel.getSecuenciaCabOtros());
            List<InvtDetotros> detalles = this.entity.findAllByParameter(InvtDetotros.class, this.parametros, null);
            this.tempTransferenciaInvList = new ArrayList<>();
            for (InvtDetotros d : detalles) {
                this.tempTransferenciaInvList.add(new TempAjusteTransferenciaInv(null, null, null, d.getGENCIAcodigo(), d.getGENOFIcodigo(),
                        this.transferenciaModel.getTipoMovimiento().getGenrDocumentoPK().getGENMODcodigo(), this.transferenciaModel.getTipoMovimiento().getGenrDocumentoPK().getGENTDOcodigo(),
                        null, this.transferenciaModel.getBodegaOrigen().getGenbodSecuencia(), d.getINVITMcodigo(), null, null, d.getINVDOTunidembala(), d.getINVDOTprecio(), 
                        d.getINVDOTcantifunci(), d.getINVDOTcanticaja(), d.getINVDOTcantiunida(), null, null, d.getINVDOTsubtotamov(), d.getNombreItem(), null));
            }
        } catch (Exception e) {
            Logger.getLogger(TransferenciaInventario.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void generarDocumento(){
        try{
            if(datosSession!=null){
                datosSession.borrarDatos();
                datosSession.setPath(FacesContext.getCurrentInstance().getExternalContext().getRealPath("//"));
                datosSession.setTieneDatasource(Boolean.FALSE);
                datosSession.setNombreReporte("inventario//transferenciaInventario");
                JsfUtil.redirectNewTab("/CamilaPOS/Documento");
            }
        } catch (Exception e) {
            Logger.getLogger(TransferenciaInventario.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public CbpgenrOficinaLazyTest getOficinasLazy() {
        return oficinasLazy;
    }

    public void setOficinasLazy(CbpgenrOficinaLazyTest oficinasLazy) {
        this.oficinasLazy = oficinasLazy;
    }

    public TransferenciaAjusteInventario getTransferenciaModel() {
        return transferenciaModel;
    }

    public void setTransferenciaModel(TransferenciaAjusteInventario transferenciaModel) {
        this.transferenciaModel = transferenciaModel;
    }

    public BigDecimal getNumTransaccion() {
        return numTransaccion;
    }

    public void setNumTransaccion(BigDecimal numTransaccion) {
        this.numTransaccion = numTransaccion;
    }

    public List<CbpgenrBodega> getBodegaOrigenList() {
        return bodegaOrigenList;
    }

    public void setBodegaOrigenList(List<CbpgenrBodega> bodegaOrigenList) {
        this.bodegaOrigenList = bodegaOrigenList;
    }

    public List<CbpgenrBodega> getBodegaDestinoList() {
        return bodegaDestinoList;
    }

    public void setBodegaDestinoList(List<CbpgenrBodega> bodegaDestinoList) {
        this.bodegaDestinoList = bodegaDestinoList;
    }

    public InvmItemxbodegaLazy getItemsBodegaLazy() {
        return itemsBodegaLazy;
    }

    public void setItemsBodegaLazy(InvmItemxbodegaLazy itemsBodegaLazy) {
        this.itemsBodegaLazy = itemsBodegaLazy;
    }

    public List<GenrDocumento> getTiposMovimiento() {
        return tiposMovimiento;
    }

    public void setTiposMovimiento(List<GenrDocumento> tiposMovimiento) {
        this.tiposMovimiento = tiposMovimiento;
    }

    public InvmItem getItem() {
        return item;
    }

    public void setItem(InvmItem item) {
        this.item = item;
    }

    public InvmItemxbodega getItemLazy() {
        return itemLazy;
    }

    public void setItemLazy(InvmItemxbodega itemLazy) {
        this.itemLazy = itemLazy;
    }

    public List<TempAjusteTransferenciaInv> getTempTransferenciaInvList() {
        return tempTransferenciaInvList;
    }

    public void setTempTransferenciaInvList(List<TempAjusteTransferenciaInv> tempTransferenciaInvList) {
        this.tempTransferenciaInvList = tempTransferenciaInvList;
    }

    public TempAjusteTransferenciaInv getTempTransferenciaInv() {
        return tempTransferenciaInv;
    }

    public void setTempTransferenciaInv(TempAjusteTransferenciaInv tempTransferenciaInv) {
        this.tempTransferenciaInv = tempTransferenciaInv;
    }

    public InvtCabotrosLazy getCabotrosLazy() {
        return cabotrosLazy;
    }

    public void setCabotrosLazy(InvtCabotrosLazy cabotrosLazy) {
        this.cabotrosLazy = cabotrosLazy;
    }

    public InvtCabotrosLazy getCabotrosEgresoLazy() {
        return cabotrosEgresoLazy;
    }

    public void setCabotrosEgresoLazy(InvtCabotrosLazy cabotrosEgresoLazy) {
        this.cabotrosEgresoLazy = cabotrosEgresoLazy;
    }

    public InvtCabotros getCabOtros() {
        return cabOtros;
    }

    public void setCabOtros(InvtCabotros cabOtros) {
        this.cabOtros = cabOtros;
    }

    public List<InvtDetotros> getDetOtros() {
        return detOtros;
    }

    public void setDetOtros(List<InvtDetotros> detOtros) {
        this.detOtros = detOtros;
    }

    
}
