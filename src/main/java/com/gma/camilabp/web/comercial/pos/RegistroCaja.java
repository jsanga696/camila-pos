/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.comercial.pos;

import com.gma.camilabp.domain.CbpgenmDenominacion;
import com.gma.camilabp.domain.CbpgenrRegistroDenominacion;
import com.gma.camilabp.domain.CbpsegmTurno;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.service.PuntoVentaLocal;
import com.gma.camilabp.web.session.UserSession;
import com.gma.camilabp.web.util.JsfUtil;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

/**
 *
 * @author userdb6
 */
@Named(value = "registroCaja")
@ViewScoped
public class RegistroCaja implements Serializable {

    @EJB
    private PuntoVentaLocal posService;
    @EJB
    private EntityManagerLocal entity;
    @Inject
    private UserSession userSession;

    protected Date fechaRegistro;
    protected List<CbpgenmDenominacion> denominaciones;
    protected CbpgenrRegistroDenominacion registro;
    private Collection listaUpdate;
    protected CbpsegmTurno turno;
    private Map<String, Object> parametros;

    @PostConstruct
    public void init() {
        try {
            fechaRegistro = new Date();
            this.parametros= new HashMap<>();
            this.parametros.put("gendenTiporegistro", 1L);
            denominaciones = entity.findAllByParameter(CbpgenmDenominacion.class, this.parametros,null);
            registro = new CbpgenrRegistroDenominacion();
            if(this.userSession.getTurno()!=null)
                turno = entity.find(CbpsegmTurno.class, userSession.getTurno().getSegturCodigo());
            listaUpdate = new ArrayList<>();
            denominaciones.forEach((d) -> {
                listaUpdate.add("mainForm:dtDetalleRegistro:" + denominaciones.indexOf(d) + ":valor");
            });
            listaUpdate.add("mainForm:datosResumen");
        } catch (Exception e) {
            Logger.getLogger(RegistroCaja.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public void calcular() {
        try {
            registro.setGenrdeMoneda(BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP));
            registro.setGenrdeBillete(BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP));
            registro.setGenrdeFijo(BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP));
            denominaciones.forEach((d) -> {
                if(d.getCantidad()!=null && d.getCantidad().compareTo(0L)>0){
                    d.setTotal(d.getGendenValor().multiply(new BigDecimal(d.getCantidad())));
                    switch (d.getGendenTipo().intValue()) {
                        case 1:
                            registro.setGenrdeFijo(registro.getGenrdeFijo().add(d.getTotal()).setScale(2, RoundingMode.HALF_UP));
                            break;
                        case 2:
                            registro.setGenrdeMoneda(registro.getGenrdeMoneda().add(d.getTotal()).setScale(2, RoundingMode.HALF_UP));
                            break;
                        case 3:
                            registro.setGenrdeBillete(registro.getGenrdeBillete().add(d.getTotal()).setScale(2, RoundingMode.HALF_UP));
                            break;
                        default:
                            break;
                    }
                }else{
                    d.setCantidad(0L);
                    d.setTotal(new BigDecimal("0.00"));
                }
            });
            registro.setGenrdeMonto(registro.getGenrdeMoneda().add(registro.getGenrdeBillete()).add(registro.getGenrdeFijo()).setScale(2, RoundingMode.HALF_UP));
            JsfUtil.update(listaUpdate);
        } catch (Exception e) {
            Logger.getLogger(RegistroCaja.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void grabarRegistroCaja(){
        try{
            if(userSession.getTurno()!=null){
                if(this.registro.getGenrdeMonto()!=null && this.registro.getGenrdeMonto().compareTo(BigDecimal.ZERO)>0){
                    if(registro.getGenrdeCodigo()==null){
                        registro.setGenrdeMonetario(registro.getGenrdeMonto());
                        CbpgenrRegistroDenominacion r = posService.registrarValoresCaja(registro, denominaciones, userSession.getTurno());
                        turno = entity.find(CbpsegmTurno.class, userSession.getTurno().getSegturCodigo());
                        if(r!=null){
                            JsfUtil.messageInfo(null, "REGISTRO EXITOSO. "+r.getGenrdeMonto(), "");
                        }else{
                            JsfUtil.messageWarning(null, "REGISTRO DE CAJA NO VALIDO", "");
                        }
                    }
                }else{
                    JsfUtil.messageWarning(null, "REGISTRO DE CAJA NO VALIDO", "");
                }
            }else{
                JsfUtil.messageWarning(null, "DEBE EXISTIR TURNO ABIERTO", "");
            }
        } catch (Exception e) {
            Logger.getLogger(RegistroCaja.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public List<CbpgenmDenominacion> getDenominaciones() {
        return denominaciones;
    }

    public void setDenominaciones(List<CbpgenmDenominacion> denominaciones) {
        this.denominaciones = denominaciones;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public CbpgenrRegistroDenominacion getRegistro() {
        return registro;
    }

    public void setRegistro(CbpgenrRegistroDenominacion registro) {
        this.registro = registro;
    }

    public CbpsegmTurno getTurno() {
        return turno;
    }

    public void setTurno(CbpsegmTurno turno) {
        this.turno = turno;
    }

}
