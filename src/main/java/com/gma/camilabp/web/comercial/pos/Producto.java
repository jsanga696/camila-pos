/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.comercial.pos;

import com.gma.camilabp.domain.CbpgenrOficina;
import com.gma.camilabp.domain.CbpgenrPoliticabase;
import com.gma.camilabp.domain.INVRdetLISTAPRECIO;
import com.gma.camilabp.domain.InvmItem;
import com.gma.camilabp.domain.InvmItemxbodega;
import com.gma.camilabp.sql.QuerysPos;
import com.gma.camilabp.web.lazy.InvmItemLazy;
import com.gma.camilabp.web.lazy.InvmItemxbodegaLazy;
import com.gma.camilabp.web.lazy.InvrDetListaPrecioLazy;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.service.PuntoVentaLocal;
import com.gma.camilabp.web.session.UserSession;
import com.gma.camilabp.web.util.JsfUtil;
import com.gma.camilabp.web.util.Utilidades;
import java.io.File;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import org.primefaces.event.FileUploadEvent;
/**
 *
 * @author userdb6
 */
@Named(value = "producto")
@ViewScoped
public class Producto implements Serializable{
    @Inject
    private UserSession userSession;
    @EJB
    private PuntoVentaLocal posService;
    @EJB
    private EntityManagerLocal entity;
    
    protected InvmItemLazy productosLazy;
    protected InvmItem item;
    protected InvmItem itemConsulta;
    protected InvmItem itemConsultado;
    protected InvrDetListaPrecioLazy itemsPrecioLazy;
    protected List<INVRdetLISTAPRECIO> preciosItem;
    protected List<InvmItemxbodega> stockItem;
    protected List<InvmItemxbodega> itemBodegas;
    protected List<INVRdetLISTAPRECIO> itemPrecios;
    protected InvmItemxbodegaLazy itemsBodegaLazy;
    protected INVRdetLISTAPRECIO itemPrecio;
    protected List<CbpgenrPoliticabase> politicas;
    protected String directorio;
    protected String directorioWar;
    
    private Map<String, Object> parametros;
    
    @PostConstruct
    public void init() {
        try{
            System.out.println("INCIO Producto "+new Date());
            productosLazy= new InvmItemLazy();
            this.limpiarItem();
            if(this.userSession.getCaja()!=null && this.userSession.getCaja().getGENBODcodigo()!=null){
                this.itemsBodegaLazy= new InvmItemxbodegaLazy(this.userSession.getCaja().getGENBODcodigo(), null, null);
                itemsPrecioLazy = new InvrDetListaPrecioLazy(this.userSession.getCaja().getCablistaprecio().getINVLPRsecuencia());
            }
            this.directorio=(String) posService.getValorDefault(1, this.userSession.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia(), null, "DIRECTORIOIMG");
            this.directorioWar=(String) posService.getValorDefault(1, this.userSession.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia(), null, "DIRIMGWAR");
        } catch (Exception e) {
            Logger.getLogger(Producto.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    
    public void buscarItem(){
        try{
            System.out.println("buscarItem"+new Date());
            this.politicas = new ArrayList<>();
            this.preciosItem= new ArrayList<>();
            this.itemConsultado= new InvmItem();
            this.stockItem = new ArrayList<>();
            if(this.itemConsulta.getCodigoItem()==null && this.itemConsulta.getINVITMnombre()==null&& this.itemConsulta.getINVITMcodigbarra()==null){
                JsfUtil.messageWarning(null, "INGRESE DATOS PARA LA BUSQUEDA", "");
            }else{
                List<InvmItem> i= posService.consultarItem(this.itemConsulta.getCodigoItem()!=null?this.itemConsulta.getCodigoItem().trim():null, this.itemConsulta.getINVITMnombre()!=null?this.itemConsulta.getINVITMnombre().trim().toUpperCase():null,this.itemConsulta.getINVITMcodigbarra()!=null?this.itemConsulta.getINVITMcodigbarra().trim():null);
                if(i==null || i.isEmpty()){
                    JsfUtil.messageInfo(null, "REGISTRO NO ENCONTRADO", "");
                }else{
                    if(i.size()==1){
                        CbpgenrOficina of;
                        this.itemConsultado= i.get(0);
                        //IMAGEN
                        File fileNew = new File(this.directorio, this.itemConsultado.getINVITMidcodigo()+".png");
                        System.out.println(fileNew.exists());
                        if(fileNew.exists()){
                            Utilidades.subirArchivosRepositorioGeneral(Files.readAllBytes(fileNew.toPath()), this.itemConsultado.getINVITMidcodigo()+".png", this.directorioWar);
                        }
                        parametros= new HashMap<>();
                        parametros.put("iNVRdetLISTAPRECIOPK.iNVITMcodigo", this.itemConsultado.getInvmItemPK().getINVITMcodigo());
                        if(this.userSession.getCaja()!=null)
                            parametros.put("iNVRdetLISTAPRECIOPK.iNVLPRsecuencia", this.userSession.getCaja().getCablistaprecio().getINVLPRsecuencia());
                        this.preciosItem= this.entity.findAllByParameter(INVRdetLISTAPRECIO.class, parametros,null);
                        parametros= new HashMap<>();
                        parametros.put("invmItemxbodegaPK.iNVITMcodigo", this.itemConsultado.getInvmItemPK().getINVITMcodigo());
                        parametros.put("invmItemxbodegaPK.gENCIAcodigo", this.itemConsultado.getInvmItemPK().getGENCIAcodigo());
                        if(this.userSession.getCaja()!=null){
                            parametros.put("invmItemxbodegaPK.gENOFIcodigo", this.userSession.getCaja().getGENOFIcodigo());
                        } else{
                            of=(CbpgenrOficina) Utilidades.getFirstElement(this.userSession.getOficinas());
                            if(of!=null)
                                parametros.put("invmItemxbodegaPK.gENOFIcodigo", of.getGenofiSecuencia());
                        }
                        this.stockItem= this.entity.findAllByParameter(InvmItemxbodega.class, parametros,"bodega");
                        this.parametros= new HashMap<>();
                        this.parametros.put("item", this.itemConsultado.getInvmItemPK().getINVITMcodigo());
                        this.parametros.put("secuenciaCompania", this.itemConsultado.getInvmItemPK().getGENCIAcodigo());
                        if(this.userSession.getCaja()!=null){
                            this.parametros.put("bodega", this.userSession.getCaja().getGENBODcodigo());
                            this.politicas= this.entity.findAll(QuerysPos.politicasItemBodega, this.parametros);
                        }else{
                            this.politicas= this.entity.findAll(QuerysPos.politicasItem, this.parametros);
                        }
                        for (INVRdetLISTAPRECIO ip : this.preciosItem) {
                            for (CbpgenrPoliticabase politica : this.politicas) {
                                politica.setValorFinal(ip.getValor().subtract(ip.getValor().multiply(politica.getGenpolPorcentaje()).divide(new BigDecimal("100"))));
                            }
                            break;
                        }
                    }else{
                        JsfUtil.messageInfo(null, "EXISTE MAS DE UN REGISTRO", "");
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(Producto.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void cargarDatosItem(){
        try{
            CbpgenrOficina of;
            parametros= new HashMap<>();
            parametros.put("invmItemxbodegaPK.iNVITMcodigo", this.item.getInvmItemPK().getINVITMcodigo());
            parametros.put("invmItemxbodegaPK.gENCIAcodigo", this.item.getInvmItemPK().getGENCIAcodigo());
            if(this.userSession.getCaja()!=null){
                parametros.put("invmItemxbodegaPK.gENOFIcodigo", this.userSession.getCaja().getGENOFIcodigo());
            } else{
                of=(CbpgenrOficina) Utilidades.getFirstElement(this.userSession.getOficinas());
                if(of!=null)
                    parametros.put("invmItemxbodegaPK.gENOFIcodigo", of.getGenofiSecuencia());
            }
            this.itemBodegas= this.entity.findAllByParameter(InvmItemxbodega.class, parametros,"bodega");
            parametros= new HashMap<>();
            parametros.put("iNVRdetLISTAPRECIOPK.iNVITMcodigo", this.item.getInvmItemPK().getINVITMcodigo());
            this.itemPrecios = this.entity.findAllByParameter(INVRdetLISTAPRECIO.class, parametros,null);
        } catch (Exception e) {
            Logger.getLogger(Producto.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void abrirDialogoAvtaulizacion(Long dialogo){
        try{
            if(this.itemConsultado !=null && this.itemConsultado.getInvmItemPK()!=null && this.itemConsultado.getInvmItemPK().getINVITMcodigo()!=null){
                JsfUtil.update(dialogo==1L?"frmdlgCodBarra":"frmdlgImage");
                JsfUtil.executeJS(dialogo==1L?"PF('dlgActCodigoBarra').show();":"PF('dlgActImage').show();");
            }else{
                JsfUtil.messageInfo(null, "REALIZE LA BUSQUEDA DEL ITEM", "");
            }
        } catch (Exception e) {
            Logger.getLogger(Producto.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void actualizarItem(){
        try{
            System.out.println("actualizarItem"+ new Date());
            if(this.itemConsultado.getCodigoBarra()!=null){
                this.itemConsultado.setINVITMcodigbarra(this.itemConsultado.getCodigoBarra());
                entity.executeProcedure("UPDATE INVM_ITEM SET INVITM_codigbarra='" + this.itemConsultado.getCodigoBarra() + "'"+" WHERE INVITM_codigo='"+this.itemConsultado.getInvmItemPK().getINVITMcodigo()+"'", null);
                JsfUtil.executeJS("PF('dlgActCodigoBarra').hide();");
            }else{
                JsfUtil.messageInfo(null, "INGRESE CODIGO DE BARRA NUEVO", "");
            }
        } catch (Exception e) {
            Logger.getLogger(Producto.class.getName()).log(Level.SEVERE, null, e);
        }
    }
     
    public void handleFileUpload(FileUploadEvent event) {           
        try{
            if(this.itemConsultado!=null){
                if(this.directorio!=null){
                    if(Utilidades.subirArchivosRepositorioGeneral(event.getFile().getContents(), this.itemConsultado.getINVITMidcodigo()+".png", this.directorio)){
                        JsfUtil.messageInfo(null, "ARCHIVO CARGADO. CODIGO:"+this.itemConsultado.getInvmItemPK().getINVITMcodigo()+" Archivo:"+this.itemConsultado.getINVITMidcodigo()+".png", "");
                    }else{
                        JsfUtil.messageWarning(null, "REPITA EL PROCESO. NO SE PUDO CARGAR EL ARCHIVO", "");
                    }
                }else{
                    JsfUtil.messageWarning(null, "NO EXISTE DIRECTORIO PARA IMAGENES", "");
                }                
            }else{
                JsfUtil.messageInfo(null, "REALIZE LA BUSQUEDA DEL ITEM", "");
            }
        }catch (Exception e) {
            Logger.getLogger(Producto.class.getName()).log(Level.SEVERE, null, e);
        }
    }        
    
    public void limpiarItem(){
        this.itemConsulta = new InvmItem();
    }

    public InvmItemLazy getProductosLazy() {
        return productosLazy;
    }

    public void setProductosLazy(InvmItemLazy productosLazy) {
        this.productosLazy = productosLazy;
    }

    public InvmItem getItem() {
        return item;
    }

    public void setItem(InvmItem item) {
        this.item = item;
    }

    public InvrDetListaPrecioLazy getItemsPrecioLazy() {
        return itemsPrecioLazy;
    }

    public void setItemsPrecioLazy(InvrDetListaPrecioLazy itemsPrecioLazy) {
        this.itemsPrecioLazy = itemsPrecioLazy;
    }

    public InvmItem getItemConsulta() {
        return itemConsulta;
    }

    public void setItemConsulta(InvmItem itemConsulta) {
        this.itemConsulta = itemConsulta;
    }

    public InvmItem getItemConsultado() {
        return itemConsultado;
    }

    public void setItemConsultado(InvmItem itemConsultado) {
        this.itemConsultado = itemConsultado;
    }

    public List<INVRdetLISTAPRECIO> getPreciosItem() {
        return preciosItem;
    }

    public void setPreciosItem(List<INVRdetLISTAPRECIO> preciosItem) {
        this.preciosItem = preciosItem;
    }

    public List<InvmItemxbodega> getStockItem() {
        return stockItem;
    }

    public void setStockItem(List<InvmItemxbodega> stockItem) {
        this.stockItem = stockItem;
    }

    public List<InvmItemxbodega> getItemBodegas() {
        return itemBodegas;
    }

    public void setIntemBodegas(List<InvmItemxbodega> itemBodegas) {
        this.itemBodegas = itemBodegas;
    }

    public List<INVRdetLISTAPRECIO> getItemPrecios() {
        return itemPrecios;
    }

    public void setItemPrecios(List<INVRdetLISTAPRECIO> itemPrecios) {
        this.itemPrecios = itemPrecios;
    }

    public InvmItemxbodegaLazy getItemsBodegaLazy() {
        return itemsBodegaLazy;
    }

    public void setItemsBodegaLazy(InvmItemxbodegaLazy itemsBodegaLazy) {
        this.itemsBodegaLazy = itemsBodegaLazy;
    }

    public INVRdetLISTAPRECIO getItemPrecio() {
        return itemPrecio;
    }

    public void setItemPrecio(INVRdetLISTAPRECIO itemPrecio) {
        this.itemPrecio = itemPrecio;
    }    

    public List<CbpgenrPoliticabase> getPoliticas() {
        return politicas;
    }

    public void setPoliticas(List<CbpgenrPoliticabase> politicas) {
        this.politicas = politicas;
    }

    
}
