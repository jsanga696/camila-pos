/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.comercial.politica;

import com.gma.camilabp.domain.CbpgenrBodega;
import com.gma.camilabp.domain.CbpgenrOficina;
import com.gma.camilabp.domain.CbpgenrPoliticabase;
import com.gma.camilabp.domain.CbpgenrPoliticabodega;
import com.gma.camilabp.domain.CbpgenrPoliticaitem;
import com.gma.camilabp.domain.CbpsegmOpcion;
import com.gma.camilabp.domain.InvmItem;
import com.gma.camilabp.model.consultas.SaldoInventarioModel;
import com.gma.camilabp.model.consultas.TipoFiltro;
import com.gma.camilabp.sql.QuerysPos;
import com.gma.camilabp.web.lazy.CbpgenrPoliticaLazy;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.service.InventarioLocal;
import com.gma.camilabp.web.service.PuntoVentaLocal;
import com.gma.camilabp.web.session.UserSession;
import com.gma.camilabp.web.util.JsfUtil;
import com.gma.camilabp.web.util.StringValues;
import com.gma.camilabp.web.util.Utilidades;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import org.primefaces.model.DualListModel;

/**
 *
 * @author Acer
 */
@Named(value = "gestionPoliticas")
@ViewScoped
public class GestionPoliticas implements Serializable{
    @Inject
    private UserSession userSession;
    @EJB
    private EntityManagerLocal entity;
    @EJB
    private PuntoVentaLocal posService;
    @EJB
    private InventarioLocal inventarioService;

    protected CbpgenrPoliticabase politica;
    protected CbpgenrPoliticabase politicaSeleccionada;
    protected List<CbpgenrOficina> oficinaList;
    protected CbpgenrPoliticaLazy politicaLazy;
    protected List<InvmItem> itemSource;
    protected List<InvmItem> itemSourceSelecction;
    protected List<InvmItem> itemSourceFilter;
    protected List<InvmItem> itemTarget;
    protected List<InvmItem> itemTargetSelecction;
    protected DualListModel<CbpgenrBodega> bodegas;
    protected List<TipoFiltro> filtros;
    protected SaldoInventarioModel modelSaldoInventario;
    protected List<TipoFiltro> consultaFiltro;
    protected Boolean aplicaTodos = Boolean.FALSE;
    protected Long grupo=0L;
    
    private Map<String, Object> parametros;
    
    @PostConstruct
    public void init() {
        try{
            this.politica = new CbpgenrPoliticabase();
            this.politicaLazy = new CbpgenrPoliticaLazy();
            this.parametros= new HashMap<>();
            this.itemTarget = new ArrayList<>();
            this.modelSaldoInventario = new SaldoInventarioModel();
            
            this.filtros=inventarioService.nivelesInventario(this.userSession.getCbpsegmUsuario().getGenciaCodigo());
            this.itemSource = this.entity.findAll(InvmItem.class);
            this.politica.setGenciaCodigo(this.userSession.getCbpsegmUsuario().getGenciaCodigo());
            this.parametros.put("genciaCodigo",this.userSession.getCbpsegmUsuario().getGenciaCodigo().getGenciaCodigo());
            this.oficinaList = this.entity.findAllByParameter(CbpgenrOficina.class, this.parametros, null);
            this.parametros= new HashMap<>();
            this.parametros.put("genofiCodigo",(CbpgenrOficina)Utilidades.getFirstElement(this.oficinaList));
            this.bodegas = new DualListModel<>(this.entity.findAllByParameter(CbpgenrBodega.class, this.parametros, null), new ArrayList<>());
            
        } catch (Exception e) {
            Logger.getLogger(GestionPoliticas.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void cantidadIngresada(){
        try{
            this.itemSource = this.entity.findAll(InvmItem.class);
            if(this.politica.getGenpolCantidad()!=null){
                this.parametros= new HashMap<>();
                this.parametros.put("genciaSecuencia",this.politica.getGenciaCodigo().getGenciaSecuencia());
                this.parametros.put("cantidad",this.politica.getGenpolCantidad());
                List<InvmItem> itemsEnPoliticas=this.entity.findAll(QuerysPos.itemsPoliticaPorCantidad, this.parametros);
                this.itemSource.removeAll(itemsEnPoliticas);
            }
        } catch (Exception e) {
            Logger.getLogger(GestionPoliticas.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void agregarSeleccionados(){
        try{
            this.itemSourceSelecction.stream().filter((invmItem) -> (this.grupo!=null)).forEachOrdered((invmItem) -> {
                invmItem.setGrupo(this.grupo);
            });
            this.itemTarget.addAll(this.itemSourceSelecction);
            this.itemSource.removeAll(this.itemSourceSelecction);            
            this.itemSourceSelecction= new ArrayList<>();
        } catch (Exception e) {
            Logger.getLogger(GestionPoliticas.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void agregarTodos(){
        try{
            this.itemSource.stream().filter((invmItem) -> (this.grupo!=null)).forEachOrdered((invmItem) -> {
                invmItem.setGrupo(this.grupo);
            });            
            this.itemTarget.addAll(this.itemSource);
            this.itemSource=new ArrayList<>();
            this.itemSourceSelecction=new ArrayList<>();
        } catch (Exception e) {
            Logger.getLogger(GestionPoliticas.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void eliminarSeleccionados(){
        try{
            this.itemSource.addAll(this.itemTargetSelecction);
            this.itemTarget.removeAll(this.itemTargetSelecction);            
            this.itemTargetSelecction= new ArrayList<>();
        } catch (Exception e) {
            Logger.getLogger(GestionPoliticas.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void eliminarTodos(){
        try{
            this.itemSource.addAll(this.itemTarget);
            this.itemTarget=new ArrayList<>();
            this.itemTargetSelecction=new ArrayList<>();
        } catch (Exception e) {
            Logger.getLogger(GestionPoliticas.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public boolean validarPolitica(CbpgenrPoliticabase p, List<InvmItem> li, List<CbpgenrBodega> lb){
        if(p.getGenpolNombre()==null || p.getGenpolCantidad()==null || p.getGenpolPorcentaje()==null || li==null || li.isEmpty() || lb==null || lb.isEmpty()){            
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }
    
    public void registarPolitica(Long tipo){
        try{
            if(tipo==1L){
                this.parametros= new HashMap<>();
                this.parametros.put("genpolCantidad", this.politica.getGenpolCantidad());
                this.parametros.put("genpolPorcentaje", this.politica.getGenpolPorcentaje());
                CbpgenrPoliticabase pb = this.entity.findObject(CbpgenrPoliticabase.class, this.parametros);
                if(pb!=null){
                    JsfUtil.messageInfo(null, "Politica Registrada. Politica: "+pb.getGenpolCodigo()+"-"+pb.getGenpolNombre(), "");
                    return;
                }
            }
            if(this.validarPolitica(tipo==1L?this.politica:this.politicaSeleccionada, this.itemTarget, this.bodegas.getTarget())){                
                this.posService.grabarPolitica(tipo==1L?this.politica:this.politicaSeleccionada, this.userSession.getCbpsegmUsuario(),this.itemTarget, this.bodegas.getTarget());
                JsfUtil.messageInfo(null, StringValues.msgRegistroExitoso, "");
                this.init();
            }else{
                JsfUtil.messageWarning(null, "Campos Obligatorios. Debe seleccionar Item(s) y Bodega(s)", "");
            }
        } catch (Exception e) {
            Logger.getLogger(GestionPoliticas.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void seleccionPolitica(){
        try{
            this.grupo = 0L;
            this.itemSource = this.entity.findAll(InvmItem.class);
            this.itemTargetSelecction = new ArrayList<>();
            this.itemSourceSelecction = new ArrayList<>();
            this.parametros= new HashMap<>();
            this.parametros.put("genciaSecuencia", this.politicaSeleccionada.getGenciaCodigo().getGenciaSecuencia());
            this.parametros.put("politica", this.politicaSeleccionada);
            this.itemTarget = this.entity.findAll(QuerysPos.itemsPolitica, this.parametros);
            for (InvmItem invmItem : this.itemTarget) {
                for (CbpgenrPoliticaitem cbpgenrPoliticaitem : this.politicaSeleccionada.getCbpgenrPoliticaitemCollection()) {
                    if(invmItem.getInvmItemPK().getINVITMcodigo().trim().equalsIgnoreCase(cbpgenrPoliticaitem.getINVITMcodigo().trim())){
                        invmItem.setGrupo(cbpgenrPoliticaitem.getGenpitGrupo());
                        break;
                    }
                }
            }
            this.itemSource.removeAll(this.itemTarget);
            
            this.parametros= new HashMap<>();
            this.parametros.put("politica", this.politicaSeleccionada);
            List<CbpgenrBodega> bodegaTarget= this.entity.findAll(QuerysPos.bodegasPolitica, this.parametros);
            this.parametros= new HashMap<>();
            this.parametros.put("genofiCodigo",(CbpgenrOficina)Utilidades.getFirstElement(this.oficinaList));
            List<CbpgenrBodega> bodegaSource=this.entity.findAllByParameter(CbpgenrBodega.class, this.parametros, null);
            bodegaSource.removeAll(bodegaTarget);
            this.parametros= new HashMap<>();
            this.parametros.put("genofiCodigo",(CbpgenrOficina)Utilidades.getFirstElement(this.oficinaList));
            this.bodegas = new DualListModel<>(bodegaSource, bodegaTarget);
        } catch (Exception e) {
            Logger.getLogger(GestionPoliticas.class.getName()).log(Level.SEVERE, null, e);
        }
    }    
    
    public void busquedaFiltro(){
        try{
            String query="";
            String queryRegistro="";
            this.consultaFiltro=null;
            this.modelSaldoInventario.setNivel(null);
            this.modelSaldoInventario.limpiarDatosFiltro();
            if(this.modelSaldoInventario.getCodigoFiltro()!=null){
                switch(this.modelSaldoInventario.getCodigoFiltro()){
                    case "N1":
                        query=QuerysPos.filtroNivel1;
                        queryRegistro=QuerysPos.itemFiltroNivel1;
                        break;
                    case "N2":
                        query=QuerysPos.filtroNivel2;
                        queryRegistro=QuerysPos.itemFiltroNivel2;
                        break;
                    case "N3":
                        query=QuerysPos.filtroNivel3;
                        queryRegistro=QuerysPos.itemFiltroNivel3;
                        break;
                    case "N4":
                        query=QuerysPos.filtroNivel4;
                        queryRegistro=QuerysPos.itemFiltroNivel4;
                        break;
                    default:
                        break;
                }
                if(this.modelSaldoInventario.getCodigoBusquedaFiltro()==null || this.modelSaldoInventario.getCodigoBusquedaFiltro().trim().length()==0){
                    this.parametros= new HashMap<>();
                    this.parametros.put("0", userSession.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia());                    
                    this.consultaFiltro= this.entity.findAllSQL(query, parametros, TipoFiltro.class);
                    JsfUtil.update("frmdlgNivel");
                    JsfUtil.update("frmdlgNivel:dtNivel");
                    JsfUtil.executeJS("PF('dlgNivel').show();");
                }else{
                    this.parametros= new HashMap<>();
                    this.parametros.put("0", userSession.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia());
                    this.parametros.put("1", this.modelSaldoInventario.getCodigoBusquedaFiltro().trim().toUpperCase());
                    TipoFiltro tipo = (TipoFiltro) this.entity.findObject("SQL", queryRegistro, this.parametros, TipoFiltro.class, Boolean.FALSE);
                    if(tipo!=null){
                        this.modelSaldoInventario.setCodigoBusquedaFiltro(tipo.getCodigo());
                        this.modelSaldoInventario.setDatoBusquedaFiltro(tipo.getDato());
                    }
                    this.parametros= new HashMap<>();
                    this.parametros.put(this.modelSaldoInventario.getCodigoFiltro().equalsIgnoreCase("N1")?"iNVIN1codin1item": 
                                        this.modelSaldoInventario.getCodigoFiltro().equalsIgnoreCase("N2")?"iNVIN2codin2item":
                                        this.modelSaldoInventario.getCodigoFiltro().equalsIgnoreCase("N3")?"iNVIN3codin3item":"iNVIN4codin4item", this.modelSaldoInventario.getCodigoBusquedaFiltro().trim().toUpperCase());
                    this.itemSource = this.entity.findAllByParameter(InvmItem.class, this.parametros, null);
                }
            }else{
                this.itemSource = this.entity.findAll(InvmItem.class);
            }
        } catch (Exception e) {
            Logger.getLogger(GestionPoliticas.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void seleccionarDato(){
        if(this.modelSaldoInventario.getNivel()!=null){
            this.modelSaldoInventario.setCodigoBusquedaFiltro(this.modelSaldoInventario.getNivel().getCodigo());
            this.busquedaFiltro();
        }
    }

    public CbpgenrPoliticabase getPolitica() {
        return politica;
    }

    public void setPolitica(CbpgenrPoliticabase politica) {
        this.politica = politica;
    }

    public List<CbpgenrOficina> getOficinaList() {
        return oficinaList;
    }

    public void setOficinaList(List<CbpgenrOficina> oficinaList) {
        this.oficinaList = oficinaList;
    }

    public CbpgenrPoliticabase getPoliticaSeleccionada() {
        return politicaSeleccionada;
    }

    public void setPoliticaSeleccionada(CbpgenrPoliticabase politicaSeleccionada) {
        this.politicaSeleccionada = politicaSeleccionada;
    }

    public CbpgenrPoliticaLazy getPoliticaLazy() {
        return politicaLazy;
    }

    public void setPoliticaLazy(CbpgenrPoliticaLazy politicaLazy) {
        this.politicaLazy = politicaLazy;
    }

    public List<InvmItem> getItemSource() {
        return itemSource;
    }

    public void setItemSource(List<InvmItem> itemSource) {
        this.itemSource = itemSource;
    }

    public List<InvmItem> getItemSourceSelecction() {
        return itemSourceSelecction;
    }

    public void setItemSourceSelecction(List<InvmItem> itemSourceSelecction) {
        this.itemSourceSelecction = itemSourceSelecction;
    }

    public List<InvmItem> getItemTarget() {
        return itemTarget;
    }

    public void setItemTarget(List<InvmItem> itemTarget) {
        this.itemTarget = itemTarget;
    }

    public List<InvmItem> getItemTargetSelecction() {
        return itemTargetSelecction;
    }

    public void setItemTargetSelecction(List<InvmItem> itemTargetSelecction) {
        this.itemTargetSelecction = itemTargetSelecction;
    }

    public List<InvmItem> getItemSourceFilter() {
        return itemSourceFilter;
    }

    public void setItemSourceFilter(List<InvmItem> itemSourceFilter) {
        this.itemSourceFilter = itemSourceFilter;
    }

    public DualListModel<CbpgenrBodega> getBodegas() {
        return bodegas;
    }

    public void setBodegas(DualListModel<CbpgenrBodega> bodegas) {
        this.bodegas = bodegas;
    }

    public List<TipoFiltro> getFiltros() {
        return filtros;
    }

    public void setFiltros(List<TipoFiltro> filtros) {
        this.filtros = filtros;
    }

    public SaldoInventarioModel getModelSaldoInventario() {
        return modelSaldoInventario;
    }

    public void setModelSaldoInventario(SaldoInventarioModel modelSaldoInventario) {
        this.modelSaldoInventario = modelSaldoInventario;
    }

    public List<TipoFiltro> getConsultaFiltro() {
        return consultaFiltro;
    }

    public void setConsultaFiltro(List<TipoFiltro> consultaFiltro) {
        this.consultaFiltro = consultaFiltro;
    }

    public Boolean getAplicaTodos() {
        return aplicaTodos;
    }

    public void setAplicaTodos(Boolean aplicaTodos) {
        this.aplicaTodos = aplicaTodos;
    }

    public Long getGrupo() {
        return grupo;
    }

    public void setGrupo(Long grupo) {
        this.grupo = grupo;
    }

}
