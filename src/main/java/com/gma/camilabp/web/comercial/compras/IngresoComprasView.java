/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.comercial.compras;

import com.gma.camilabp.domain.CbpgenrOficina;
import com.gma.camilabp.domain.ComtCabcompra;
import com.gma.camilabp.domain.ComtDetcompra;
import com.gma.camilabp.domain.CxpmProveedor;
import com.gma.camilabp.domain.CxppPlanpago;
import com.gma.camilabp.domain.CxppTiposdocumentos;
import com.gma.camilabp.domain.CxprRetenciones;
import com.gma.camilabp.domain.GenpParamgenerales;
import com.gma.camilabp.domain.GenpTalonarios;
import com.gma.camilabp.domain.GenrMoneda;
import com.gma.camilabp.domain.InvmItem;
import com.gma.camilabp.domain.MfrmPagoSRI;
import com.gma.camilabp.domain.Rcredtri;
import com.gma.camilabp.domain.Rtipcomp;
import com.gma.camilabp.web.lazy.ComtCabcompraLazy;
import com.gma.camilabp.web.lazy.InvmItemLazy;
import com.gma.camilabp.web.lazy.InvrDetListaPrecioLazy;
import com.gma.camilabp.web.lazy.ProveedoresLazy;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.service.GeneralLocal;
import com.gma.camilabp.web.service.IngresoCompraLocal;
import com.gma.camilabp.web.session.ServletSession;
import com.gma.camilabp.web.session.UserSession;
import com.gma.camilabp.web.util.JsfUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author userdb5
 */
@Named(value = "ingresoComprasView")
@ViewScoped
public class IngresoComprasView implements Serializable {
    
    @Inject
    private ServletSession datosSession;
    @Inject
    private UserSession session;
    @EJB
    private EntityManagerLocal entity;
    @EJB
    private GeneralLocal generalEjb;
    @EJB
    private IngresoCompraLocal comprasEjb;
    
    private ProveedoresLazy proveedores;
    private CxpmProveedor proveedor;
    private List<CbpgenrOficina> oficinas;
    private CbpgenrOficina oficina;
    private List<GenpParamgenerales> provincias;
    private Date fechaFactura, fechaEmision;
    private List<GenrMoneda> monedas;
    private InvmItemLazy productosLazy;
    private InvrDetListaPrecioLazy itemsPrecioLazy;
    private InvmItem item;
    private ComtCabcompra ingBodega;
    private List<InvmItem> items;
    private List<MfrmPagoSRI> formasPago;
    private List<Rtipcomp> tiposComprobante;
    private List<CxppTiposdocumentos> tiposDocs;
    private List<CxppPlanpago> planesPago;
    private List<Rcredtri> sustentosTributarios;
    private List<GenpTalonarios> talonarios;
    private List<CxprRetenciones> ivaList;
    private List<CxprRetenciones> fuenteList;
    private HashMap<String, Object> dataIngreso;
    private ComtCabcompraLazy ingBodegaLazy;
    
    @PostConstruct
    public void initView() {
        try{
            proveedores = new ProveedoresLazy();
            oficinas = entity.findAll(CbpgenrOficina.class);
            formasPago = entity.findAll(MfrmPagoSRI.class);
            tiposComprobante = entity.findAll(Rtipcomp.class);
            tiposDocs = entity.findAll(CxppTiposdocumentos.class);
            planesPago = entity.findAll(CxppPlanpago.class);
            sustentosTributarios = entity.findAll(Rcredtri.class);
            monedas = entity.findAll(GenrMoneda.class);
            talonarios = entity.findAll(GenpTalonarios.class);
            ivaList = entity.findListByParameters("SELECT m FROM CxprRetenciones m WHERE m.cXPRETestado = :estado AND m.cXPRETtipo = :tipo", new String[]{"estado", "tipo"}, new Object[]{"A", "I"});
            fuenteList = entity.findListByParameters("SELECT m FROM CxprRetenciones m WHERE m.cXPRETestado = :estado AND m.cXPRETtipo = :tipo", new String[]{"estado", "tipo"}, new Object[]{"A", "F"});
            provincias = generalEjb.obtenerParametrosGeneralesPorCodigo("023");
            productosLazy = new InvmItemLazy();
            ingBodegaLazy = new ComtCabcompraLazy();
            itemsPrecioLazy = new InvrDetListaPrecioLazy();
            items = new ArrayList();
            fechaFactura = new Date();
            dataIngreso = new HashMap();
            dataIngreso.put("Oficina", new CbpgenrOficina());
            dataIngreso.put("formaPago", new MfrmPagoSRI());
            dataIngreso.put("tipoComprobante", new Rtipcomp());
            dataIngreso.put("tipoDoc", new CxppTiposdocumentos());
            dataIngreso.put("planPago", new CxppPlanpago());
            dataIngreso.put("sustentoTrib", new Rcredtri());
            dataIngreso.put("moneda", new GenrMoneda());
            dataIngreso.put("provincia", new GenpParamgenerales());
            dataIngreso.put("talonario", new GenpTalonarios());
            dataIngreso.put("ret_fuente", new GenpTalonarios());
            dataIngreso.put("ret_iva", new GenpTalonarios());
            dataIngreso.put("ret_iva", new GenpTalonarios());
            
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public void onIngBodegaSelect(SelectEvent event){
        ingBodega = (ComtCabcompra)event.getObject();
        HashMap map = new HashMap();
        ingBodega.setDetalle(entity.findListByParameters("SELECT m FROM ComtDetcompra m WHERE m.comtDetcompraPK.cOMCCMnumsecucial = :cOMCCMnumsecucial"
                , new String[]{"cOMCCMnumsecucial"}, new Object[]{ingBodega.getCOMCCMnumsecucial()}));
        this.oficina = null;
        this.proveedor = null;
        this.dataIngreso.put("oficina", (CbpgenrOficina)entity.find(CbpgenrOficina.class, Long.parseLong(ingBodega.getGENOFIcodigo())));
        this.dataIngreso.put("moneda", entity.find(GenrMoneda.class, ingBodega.getGENMONcodigo()));
        map.put("gENCIAcodigo", ingBodega.getGENCIAcodigo());
        map.put("gENOFIcodigo", ingBodega.getGENOFIcodigo());
        map.put("cXPPROcodigo", ingBodega.getCXPPROcodigo());
        this.dataIngreso.put("proveedor", entity.findObject("HQL", "SELECT m FROM CxpmProveedor m WHERE m.cxpmProveedorPK.gENCIAcodigo = :gENCIAcodigo AND "
                + "m.cxpmProveedorPK.gENOFIcodigo = :gENOFIcodigo AND m.cxpmProveedorPK.cXPPROcodigo = :cXPPROcodigo", 
                map, CxpmProveedor.class, Boolean.TRUE));
        for(ComtDetcompra det : ingBodega.getDetalle()){
            HashMap m = new HashMap();
            m.put("invmItemPK.iNVITMcodigo", det.getINVITMcodigo());
            det.setProducto((InvmItem)entity.findObject(InvmItem.class, m));
        }
        System.out.println(this.getDataIngreso().get("proveedor"));
        this.dataIngreso.put("bodega", ivaList);
        this.dataIngreso.put("nFactura", ivaList);
        this.dataIngreso.put("provincia", ivaList);
        this.dataIngreso.put("fechaFactura", null);
        this.dataIngreso.put("observacion", null);
        this.fechaEmision = ingBodega.getCOMCCMfechingreg();
        this.fechaFactura = ingBodega.getCXPDOCfechafactura();
        JsfUtil.executeJS("PF('wdIngBod').hide()");
        JsfUtil.update(":mainForm");
    }
    
    public void onProductoSelect(SelectEvent event){
        this.item = (InvmItem)event.getObject();
        items.add(item);
        JsfUtil.executeJS("PF('wdProductos').hide()");
    }
    
    public void validarProveedor(){
        if(proveedor != null)
            JsfUtil.executeJS("PF(wdProveedores).hide()");
        else
            JsfUtil.messageInfo(null, "Info", "Debe seleccionar un proveedor");
    }

    public ProveedoresLazy getProveedores() {
        return proveedores;
    }

    public void setProveedores(ProveedoresLazy proveedores) {
        this.proveedores = proveedores;
    }

    public ServletSession getDatosSession() {
        return datosSession;
    }

    public void setDatosSession(ServletSession datosSession) {
        this.datosSession = datosSession;
    }

    public UserSession getSession() {
        return session;
    }

    public void setSession(UserSession session) {
        this.session = session;
    }

    public CxpmProveedor getProveedor() {
        return proveedor;
    }

    public void setProveedor(CxpmProveedor proveedor) {
        this.proveedor = proveedor;
    }

    public List<CbpgenrOficina> getOficinas() {
        return oficinas;
    }

    public void setOficinas(List<CbpgenrOficina> oficinas) {
        this.oficinas = oficinas;
    }

    public CbpgenrOficina getOficina() {
        return oficina;
    }

    public void setOficina(CbpgenrOficina oficina) {
        this.oficina = oficina;
    }

    public List<GenpParamgenerales> getProvincias() {
        return provincias;
    }

    public void setProvincias(List<GenpParamgenerales> provincias) {
        this.provincias = provincias;
    }

    public Date getFechaFactura() {
        return fechaFactura;
    }

    public void setFechaFactura(Date fechaFactura) {
        this.fechaFactura = fechaFactura;
    }

    public List<GenrMoneda> getMonedas() {
        return monedas;
    }

    public void setMonedas(List<GenrMoneda> monedas) {
        this.monedas = monedas;
    }

    public HashMap<String, Object> getDataIngreso() {
        return dataIngreso;
    }

    public void setDataIngreso(HashMap<String, Object> dataIngreso) {
        this.dataIngreso = dataIngreso;
    }
    
    public InvmItemLazy getProductosLazy() {
        return productosLazy;
    }

    public void setProductosLazy(InvmItemLazy productosLazy) {
        this.productosLazy = productosLazy;
    }

    public InvrDetListaPrecioLazy getItemsPrecioLazy() {
        return itemsPrecioLazy;
    }

    public void setItemsPrecioLazy(InvrDetListaPrecioLazy itemsPrecioLazy) {
        this.itemsPrecioLazy = itemsPrecioLazy;
    }

    public InvmItem getItem() {
        return item;
    }

    public void setItem(InvmItem item) {
        this.item = item;
    }

    public List<InvmItem> getItems() {
        return items;
    }

    public void setItems(List<InvmItem> items) {
        this.items = items;
    }

    public List<MfrmPagoSRI> getFormasPago() {
        return formasPago;
    }

    public void setFormasPago(List<MfrmPagoSRI> formasPago) {
        this.formasPago = formasPago;
    }

    public List<Rtipcomp> getTiposComprobante() {
        return tiposComprobante;
    }

    public void setTiposComprobante(List<Rtipcomp> tiposComprobante) {
        this.tiposComprobante = tiposComprobante;
    }

    public List<CxppTiposdocumentos> getTiposDocs() {
        return tiposDocs;
    }

    public void setTiposDocs(List<CxppTiposdocumentos> tiposDocs) {
        this.tiposDocs = tiposDocs;
    }

    public List<CxppPlanpago> getPlanesPago() {
        return planesPago;
    }

    public void setPlanesPago(List<CxppPlanpago> planesPago) {
        this.planesPago = planesPago;
    }

    public List<Rcredtri> getSustentosTributarios() {
        return sustentosTributarios;
    }

    public void setSustentosTributarios(List<Rcredtri> sustentosTributarios) {
        this.sustentosTributarios = sustentosTributarios;
    }

    public List<GenpTalonarios> getTalonarios() {
        return talonarios;
    }

    public void setTalonarios(List<GenpTalonarios> talonarios) {
        this.talonarios = talonarios;
    }

    public List<CxprRetenciones> getIvaList() {
        return ivaList;
    }

    public void setIvaList(List<CxprRetenciones> ivaList) {
        this.ivaList = ivaList;
    }

    public List<CxprRetenciones> getFuenteList() {
        return fuenteList;
    }

    public void setFuenteList(List<CxprRetenciones> fuenteList) {
        this.fuenteList = fuenteList;
    }

    public ComtCabcompraLazy getIngBodegaLazy() {
        return ingBodegaLazy;
    }

    public void setIngBodegaLazy(ComtCabcompraLazy ingBodegaLazy) {
        this.ingBodegaLazy = ingBodegaLazy;
    }

    public ComtCabcompra getIngBodega() {
        return ingBodega;
    }

    public void setIngBodega(ComtCabcompra ingBodega) {
        this.ingBodega = ingBodega;
    }

    public Date getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(Date fechaEmision) {
        this.fechaEmision = fechaEmision;
    }
    
}
