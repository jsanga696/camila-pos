/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.comercial.pos;

import com.gma.camilabp.config.SisVars;
import com.gma.camilabp.domain.CbpgenrCompania;
import com.gma.camilabp.domain.FacrMotvdevolu;
import com.gma.camilabp.domain.FactCabvtacaja;
import com.gma.camilabp.domain.FactCabventa;
import com.gma.camilabp.model.consultas.RptCobro;
import com.gma.camilabp.model.consultas.RtpFactura;
import com.gma.camilabp.web.lazy.FactCabvtacajaLazy;
import com.gma.camilabp.web.lazy.LazyDataModelGen;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.service.PuntoVentaLocal;
import com.gma.camilabp.web.session.ServletSession;
import com.gma.camilabp.web.session.UserSession;
import com.gma.camilabp.web.util.JsfUtil;
import com.gma.camilabp.web.util.StringValues;
import com.gma.camilabp.web.util.Utilidades;
import java.io.Serializable;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

/**
 *
 * @author userdb6
 */
@Named(value = "transacciones")
@ViewScoped
public class Transacciones implements Serializable {

    @Inject
    private ServletSession datosSession;
    @Inject
    private UserSession userSession;
    @EJB
    private EntityManagerLocal entity;

    @EJB
    private PuntoVentaLocal posService;

    protected FactCabvtacajaLazy ventasLazy;
    protected LazyDataModelGen<FactCabventa> devoluciones;
    private Map<String, Object> parametros;
    protected FactCabvtacajaLazy ventasHistorialLazy;

    @PostConstruct
    public void init() {
        try {
            this.ventasLazy = new FactCabvtacajaLazy(userSession.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia(), userSession.getCaja().getGENOFIcodigo(), this.userSession.getCbpsegmUsuario().getSegusuLogin().equalsIgnoreCase("ADM")?null:userSession.getTurno().getSegturNumero().intValue());
            this.ventasHistorialLazy = new FactCabvtacajaLazy(userSession.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia(), userSession.getCaja().getGENOFIcodigo(), userSession.getCaja()!=null?userSession.getCaja().getGENCAJcodigo():null);
            String[] filterss = {"factCabventaPK.gENCIAcodigo", "factCabventaPK.gENOFIcodigo", "gENTDOcodigo","fACCVTusuaingreg"};
            Object[] filtersValue = {userSession.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia(), userSession.getCaja().getGENOFIcodigo(), "DFA",this.userSession.getCbpsegmUsuario().getSegusuLogin()};
            devoluciones = new LazyDataModelGen<>(FactCabventa.class, filterss, filtersValue, "factCabventaPK.fACCVTnumsecuenc", "factCabventaPK.fACCVTnumsecuenc");
        } catch (Exception e) {
            Logger.getLogger(Transacciones.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public void imprimirFactura(FactCabvtacaja v) {
        try {
            if (datosSession != null) {
                datosSession.borrarDatos();
                parametros = new HashMap<>();
                parametros.put("0", userSession.getCbpsegmUsuario().getSegusuLogin());
                parametros.put("1", v.getFacvtcSecuenvta().intValue());
                entity.executeProcedure("EXEC POS_REPORTEFACTURA ?,?", parametros);
                parametros = new HashMap<>();
                parametros.put("genciaSecuencia", v.getGENCIAcodigo());
                datosSession.setTieneDatasource(Boolean.FALSE);
                datosSession.setNombreReporte("posFactura");
                datosSession.agregarParametro("SUBREPORT_DIR", datosSession.getPath() + "/reportes/pos/");
                datosSession.agregarParametro("COMPANIA", entity.findObject(CbpgenrCompania.class, parametros));                
                datosSession.agregarParametro("FACVTC_SECUENVTA", v.getFacvtcSecuenvta().intValue());
                datosSession.agregarParametro("USUARIO_REPORTE", userSession.getCbpsegmUsuario().getSegusuLogin());
                parametros = new HashMap<>();
                parametros.put("0", v.getFacvtcSecuenvta().intValue());
                parametros.put("1", userSession.getCbpsegmUsuario().getSegusuLogin());
                datosSession.agregarParametro("FACTURA", entity.findAllSQL("SELECT * FROM RPTFACTURAPOS WHERE secuencia=? AND usuario_reporte=?", parametros, RtpFactura.class));
                datosSession.agregarParametro("COBROS", entity.findAllSQL("SELECT * FROM RPTCOBROPOS WHERE rpt_secuencia=? AND usuario_reporte=? ", parametros, RptCobro.class));
                datosSession.agregarParametro("CODIGO", posService.getValorDefault(1, userSession.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia(), null, "CODIGO"));
                datosSession.agregarParametro("FORMATO", posService.getValorDefault(1, this.userSession.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia(), null, "POSFORMATO"+(this.userSession.getCaja()!=null?this.userSession.getCaja().getGENBODcodigo():"")));
                JsfUtil.redirectNewTab("/CamilaPOS/Factura");
            }
        } catch (Exception e) {
            Logger.getLogger(Transacciones.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public void imprimirFacturaDev(FactCabventa dev) {
        try {
            if (datosSession != null) {
                parametros = new HashMap<>();
                parametros.put("0", userSession.getCbpsegmUsuario().getSegusuLogin());
                parametros.put("1", dev.getFactCabventaPK().getFACCVTnumsecuenc());
                entity.executeProcedure("EXEC POS_REPORTEFACTURADEV ?,?", parametros);
                datosSession.setTieneDatasource(Boolean.TRUE);
                datosSession.agregarParametro("FORMATO", posService.getValorDefault(1, this.userSession.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia(), null, "POSFORMATO"+(this.userSession.getCaja()!=null?this.userSession.getCaja().getGENBODcodigo():"")));
                List<String> lineas = new ArrayList<>();
                lineas.add(userSession.getCaja().getGENOFIcodigo()+"-"+userSession.getCaja().getOficina());
                lineas.add("RUC:"+userSession.getCbpsegmUsuario().getGenciaCodigo().getGenciaRuc());
                lineas.add("TLFN:"+userSession.getCbpsegmUsuario().getGenciaCodigo().getGenciaTelefono());
                FactCabvtacaja factDev = entity.find(FactCabvtacaja.class, dev.getFACCVTsecdocurel());
                lineas.add("IDENTIFICACION:"+dev.getCXCCLIcodigo()+"-"+factDev.getFACVTCclierucci());
                lineas.add("CLIENTE:"+factDev.getFACVTCclienombre());
                lineas.add("-------------------");
                lineas.add("<pre>Codigo\t\tDescripcion\nCant.\tPrecio\tDescuento\tValor<pre/>");
                parametros = new HashMap<>();
                parametros.put("0", dev.getFactCabventaPK().getFACCVTnumsecuenc());
                parametros.put("1", userSession.getCbpsegmUsuario().getSegusuLogin());
                List<RtpFactura> df = entity.findAllSQL("SELECT * FROM RPTFACTURAPOS WHERE secuencia=? AND usuario_reporte=?", parametros, RtpFactura.class);
                df.forEach((d) -> {
                    lineas.add("<pre>"+d.getInvitm_codigo()+(d.getInvitm_codigo().trim().length()>10?"\t":"\t\t")+d.getInvitm_nombre()+"\n"+d.getFcdvc_cantcajas().intValue()+"-"+d.getFacdvc_cantunidad().setScale(2, RoundingMode.HALF_UP)+"\t"+d.getFacdvc_preciomov().setScale(2, RoundingMode.HALF_UP)+"\t"+d.getDesctomov().setScale(2, RoundingMode.HALF_UP)+"\t\t"+d.getFacdvc_subtotmov().setScale(2, RoundingMode.HALF_UP)+"<pre/>");
                });
                lineas.add("-------------------");
                lineas.add("NUM_FACTURA_APLICADA:"+factDev.getDocumentoFact());
                parametros = new HashMap<>();
                parametros.put("facrMotvdevoluPK.FACMDEcodigo", dev.getFACCVTmotvdevolu());
                parametros.put("facrMotvdevoluPK.GENMODcodigo", "FAC");
                lineas.add("MOTIVO_CAMBIO:"+entity.findObject(FacrMotvdevolu.class, parametros).getFACMDEdescripcio());                
                datosSession.setDataSource(lineas);        
                JsfUtil.redirectNewTab("/CamilaPOS/Transaccion");
            }
        } catch (Exception e) {
            Logger.getLogger(Transacciones.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public void imprimirNotaCredito(FactCabventa dev) {
        datosSession.setTieneDatasource(Boolean.TRUE);
        datosSession.agregarParametro("FORMATO", posService.getValorDefault(1, this.userSession.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia(), null, "POSFORMATO"+(this.userSession.getCaja()!=null?this.userSession.getCaja().getGENBODcodigo():"")));
        List<String> lineas = new ArrayList<>();
        lineas.add("NOTA DE CREDITO");
        lineas.add("-------------------");
        lineas.add("USUARIO:"+ userSession.getCbpsegmUsuario().getSegusuLogin()+"-"+userSession.getCbpsegmUsuario().getSegusuAbreviado());
        lineas.add("OFICINA:"+ userSession.getCaja().getGENOFIcodigo()+"-"+userSession.getCaja().getOficina());
        lineas.add("PTO. VENTA:"+ userSession.getCaja().getGENBODcodigo()+"-"+userSession.getCaja().getBodega());
        lineas.add("CAJA:"+userSession.getCaja().getGENCAJcodigo()+userSession.getCaja().getGENCAJdescripcio());
        lineas.add("-------------------");
        lineas.add("#NC:"+dev.getFACCVTnumdocumen());
        lineas.add("SALDO NC:"+dev.getFACCVTnetomov());
        FactCabvtacaja factDev = entity.find(FactCabvtacaja.class, dev.getFACCVTsecdocurel());
        lineas.add("FACT. APLICADA: "+factDev.getDocumentoFact());
        lineas.add("IDENTIFICACION:"+factDev.getCXCCLIcodigo()+"-"+factDev.getFACVTCclierucci());
        lineas.add("CLIENTE:"+factDev.getFACVTCclienombre());
        lineas.add("F. NC:"+Utilidades.getDateHHmm(dev.getFACCVTfechaemisi()));
        lineas.add("FECHA:"+Utilidades.getDateHHmm(new Date()));
        datosSession.setDataSource(lineas);        
        JsfUtil.redirectNewTab("/CamilaPOS/Transaccion");
    }

    public void anularDocumento(FactCabvtacaja v) {
        try {
            if (v != null && v.getFACVTCestado().equalsIgnoreCase("A")) {
                if (posService.anulacionFactura(v, userSession.getCaja())) {
                    JsfUtil.messageInfo(null, StringValues.msgRegistroExitoso, "");
                    this.init();
                } else {
                    JsfUtil.messageWarning(null, StringValues.msgRegistroNoExitoso, "");
                }
            } else {
                JsfUtil.messageWarning(null, "FACTURA YA SE ENCUENTRA ANULADA", "");
            }
        } catch (Exception e) {
            Logger.getLogger(Transacciones.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public void devolucionArticulos(FactCabvtacaja v) {
        try {
            if (v.getFACVTCestado().contains("A")) {
                datosSession.agregarParametro("factura", v);
                JsfUtil.redirectFaces2(SisVars.urlbaseFaces + "pages/pv/devoluciones/registroDevolucion.xhtml");
            } else {
                JsfUtil.messageError(null, "FACTURA SE ENCUENTRA INACTIVA", null);
            }
        } catch (Exception e) {
            Logger.getLogger(Transacciones.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public FactCabvtacajaLazy getVentasLazy() {
        return ventasLazy;
    }

    public void setVentasLazy(FactCabvtacajaLazy ventasLazy) {
        this.ventasLazy = ventasLazy;
    }

    public LazyDataModelGen<FactCabventa> getDevoluciones() {
        return devoluciones;
    }

    public void setDevoluciones(LazyDataModelGen<FactCabventa> devoluciones) {
        this.devoluciones = devoluciones;
    }

    public FactCabvtacajaLazy getVentasHistorialLazy() {
        return ventasHistorialLazy;
    }

    public void setVentasHistorialLazy(FactCabvtacajaLazy ventasHistorialLazy) {
        this.ventasHistorialLazy = ventasHistorialLazy;
    }

}
