/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.comercial.pos;

import com.gma.camilabp.domain.BanmCuentacaja;
import com.gma.camilabp.domain.BanmEntidad;
import com.gma.camilabp.domain.CbpgenrOficina;
import com.gma.camilabp.domain.CbpgenrUsuarioxofi;
import com.gma.camilabp.domain.CbpsegmTurno;
import com.gma.camilabp.domain.CbpsegmUsuario;
import com.gma.camilabp.domain.CxcrRuta;
import com.gma.camilabp.domain.FacrVendedor;
import com.gma.camilabp.domain.GenmCaja;
import com.gma.camilabp.domain.GenpParamgenerales;
import com.gma.camilabp.domain.GenpTalonarios;
import com.gma.camilabp.domain.InvrCablistaprecio;
import com.gma.camilabp.domain.MEmpleados;
import com.gma.camilabp.sql.QuerysPos;
import com.gma.camilabp.web.lazy.GenmCajaLazy;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.service.PuntoVentaLocal;
import com.gma.camilabp.web.session.UserSession;
import com.gma.camilabp.web.util.JsfUtil;
import com.gma.camilabp.web.util.StringValues;
import com.gma.camilabp.web.util.Utilidades;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

/**
 *
 * @author userdb6
 */
@Named(value = "caja")
@ViewScoped
public class Caja implements Serializable{
    @EJB
    private PuntoVentaLocal posService;
    @EJB
    private EntityManagerLocal entity;
    @Inject
    private UserSession userSession;
    
    private Map<String, Object> parametros;
    
    protected GenmCaja caja;
    protected CbpgenrOficina oficina;
    protected List<CbpgenrOficina> oficinas;
    protected List<CbpgenrUsuarioxofi> usuarios;
    protected List<FacrVendedor> vendedores;
    protected GenmCajaLazy cajasLazy;
    protected GenmCaja cajaSeleccionada;
    protected List<GenpTalonarios> talonarios;
    protected List<GenpTalonarios> talonariosDevoluiones;
    protected List<InvrCablistaprecio> cablistaprecios;
    protected List<CxcrRuta> rutas;
    protected List<MEmpleados> empleados;
    protected List<GenpParamgenerales> tipoEntiades;
    protected List<BanmEntidad> entidades;
    protected List<BanmCuentacaja> cuentasCaja;
    
    @PostConstruct
    public void init() {
        try{
            cajasLazy = new GenmCajaLazy(userSession.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia(), null, userSession.getCaja()!=null?userSession.getCaja().getGENBODcodigo():null);
            caja = new GenmCaja();
            parametros= new HashMap<>();
            parametros.put(StringValues.campoCompania, userSession.getCbpsegmUsuario().getGenciaCodigo().getGenciaCodigo());
            oficinas = entity.findAllByParameter(CbpgenrOficina.class, parametros,null);
            oficina= (CbpgenrOficina) Utilidades.getFirstElement(oficinas);
            parametros= new HashMap<>();
            parametros.put("cXCRUTtipo", 'V');
            rutas = entity.findAllByParameter(CxcrRuta.class, parametros,null);            
            parametros= new HashMap<>();
            parametros.put("gENPGEaplicacion", "BAN");
            parametros.put("genpParamgeneralesPK.gENPGEtabla", "009");
            this.tipoEntiades = this.entity.findAllByParameter(GenpParamgenerales.class, parametros, "gENPGEnombre");
            this.seleccionOficina();
        } catch (Exception e) {
            Logger.getLogger(Caja.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void seleccionOficina(){
        this.caja.setSEGUSUcodigo(null);
        this.caja.setFACVDRcodigo(null);
        this.cargarDatosOficina(this.oficina);
        parametros= new HashMap<>();
        parametros.put("mEmpleadosPK.gENOFIcodigo", this.oficina!=null?this.oficina.getGenofiSecuencia():null);
        this.empleados = entity.findAllByParameter(MEmpleados.class, parametros, null);
    }
    
    public void seleccionTipoEntidad(){
        this.caja.setBanentCodigo(null);
        this.caja.setBanctaCodigo(null);
        parametros= new HashMap<>();
        parametros.put("banmEntidadPK.banentTipo",this.caja.getGenpgeCodigo());
        this.entidades = this.entity.findAllByParameter(BanmEntidad.class, parametros, "banentNombre");
    }
    
    public void seleccionEntidad(){
        this.caja.setBanctaCodigo(null);
        parametros= new HashMap<>();
        parametros.put("banmCuentacajaPK.bANENTtipo", this.caja.getGenpgeCodigo());
        parametros.put("banmCuentacajaPK.bANENTcodigo", this.caja.getBanentCodigo());
        this.cuentasCaja = entity.findAllByParameter(BanmCuentacaja.class, parametros, "bANCTAnombre");
    }
    
    public void seleccionTipoEntidadEdit(){
        this.cajaSeleccionada.setBanentCodigo(null);
        this.cajaSeleccionada.setBanctaCodigo(null);
        parametros= new HashMap<>();
        parametros.put("banmEntidadPK.banentTipo",this.cajaSeleccionada.getGenpgeCodigo());
        this.entidades = this.entity.findAllByParameter(BanmEntidad.class, parametros, "banentNombre");
    }
    
    public void seleccionEntidadEdit(){
        this.cajaSeleccionada.setBanctaCodigo(null);
        parametros= new HashMap<>();
        parametros.put("banmCuentacajaPK.bANENTtipo", this.cajaSeleccionada.getGenpgeCodigo());
        parametros.put("banmCuentacajaPK.bANENTcodigo", this.cajaSeleccionada.getBanentCodigo());
        this.cuentasCaja = entity.findAllByParameter(BanmCuentacaja.class, parametros, "bANCTAnombre");
    }
    
    public void cargarDatosOficina(CbpgenrOficina o){
        try{
            if(o!=null){
                parametros= new HashMap<>();
                parametros.put("oficina", o);
                parametros.put("estado", Boolean.TRUE);
                this.usuarios = entity.findAll(QuerysPos.usuariosActivosByOficina, parametros);
                parametros= new HashMap<>();
                parametros.put("facrVendedorPK.gENOFIcodigo", o.getGenofiSecuencia());
                parametros.put("fACVDRestactivo", 'A');
                parametros.put("fACVDRtipovend", "C");
                this.vendedores = entity.findAllByParameter(FacrVendedor.class, parametros, "fACVDRnombre");
                parametros= new HashMap<>();
                parametros.put(StringValues.campoOficina, o.getGenofiSecuencia());
                parametros.put("gENTALtipotalon", "F");
                parametros.put("gENTALestado", "A");
                this.talonarios = entity.findAllByParameter(GenpTalonarios.class, parametros, null);
                List<String> listaTipoTalonarios= new ArrayList<>();
                listaTipoTalonarios.add("NL");
                listaTipoTalonarios.add("NF");
                //parametros.put("gENTALtipotalon", "NL");
                parametros.put("gENTALtipotalon", listaTipoTalonarios);
                this.talonariosDevoluiones = entity.findAllByParameter(GenpTalonarios.class, parametros, null);
                parametros= new HashMap<>();
                parametros.put(StringValues.campoOficina, o.getGenofiSecuencia());
                parametros.put("iNVLPRtipoLista", "P");
                this.cablistaprecios = entity.findAllByParameter(InvrCablistaprecio.class, parametros, null);
            }
        } catch (Exception e) {
            Logger.getLogger(Caja.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void cargarDatosCaja(){
        if(this.cajaSeleccionada!=null){
            parametros= new HashMap<>();
            parametros.put("genofiSecuencia", this.cajaSeleccionada.getGENOFIcodigo());
            CbpgenrOficina o = entity.findObject(CbpgenrOficina.class, parametros);
            this.cargarDatosOficina(o);
            parametros= new HashMap<>();
            parametros.put("banmEntidadPK.banentTipo",this.cajaSeleccionada.getGenpgeCodigo());
            this.entidades = this.entity.findAllByParameter(BanmEntidad.class, parametros, "banentNombre");
            parametros= new HashMap<>();
            parametros.put("banmCuentacajaPK.bANENTtipo", this.cajaSeleccionada.getGenpgeCodigo());
            parametros.put("banmCuentacajaPK.bANENTcodigo", this.cajaSeleccionada.getBanentCodigo());
            this.cuentasCaja = entity.findAllByParameter(BanmCuentacaja.class, parametros, "bANCTAnombre");
        }
    }
    
    public void grabarCaja(){
        GenmCaja cajaConsulta;
        try{
            parametros= new HashMap<>();
            parametros.put("sEGUSUcodigo", caja.getSEGUSUcodigo());
            parametros.put("gENCAJestado", "A");
            cajaConsulta = entity.findObject(GenmCaja.class, parametros);            
            if(cajaConsulta==null){
                parametros= new HashMap<>();
                parametros.put("gENCAJcodigo", caja.getGENCAJcodigo());
                parametros.put("gENOFIcodigo", oficina.getGenofiSecuencia());
                cajaConsulta = entity.findObject(GenmCaja.class, parametros);
                if(cajaConsulta==null){
                    parametros= new HashMap<>();
                    parametros.put("genpgeCodigo", caja.getGenpgeCodigo());
                    parametros.put("banentCodigo", caja.getBanentCodigo());
                    parametros.put("banctaCodigo", caja.getBanctaCodigo());
                    cajaConsulta = entity.findObject(GenmCaja.class, parametros);
                    if(cajaConsulta==null){
                        GenmCaja c = posService.grabarCaja(caja,userSession.getCbpsegmUsuario().getGenciaCodigo(), oficina);
                        if(c!=null){
                            cajasLazy = new GenmCajaLazy(userSession.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia(), null, userSession.getCaja()!=null?userSession.getCaja().getGENBODcodigo():null);
                            JsfUtil.messageInfo(null, StringValues.msgRegistroExitoso, "");
                        }
                        else
                            JsfUtil.messageError(null, StringValues.msgRegistroNoExitoso, "");
                    }else{
                        JsfUtil.messageWarning(null, "CTA. CAJA REGISTRADA "+caja.getBanctaCodigo(), "");
                    }
                }else{
                    JsfUtil.messageWarning(null, "CODIGO REGISTRADO "+cajaConsulta.getGENCAJcodigo(), "");
                }
            }else{
                JsfUtil.messageWarning(null, "USUARIO CON CAJA ASIGNADA "+cajaConsulta.getGENCAJcodigo(), "");
            }
        } catch (Exception e) {
            Logger.getLogger(Caja.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void modificacionCaja(){
        GenmCaja cajaConsulta=null;
        CbpsegmTurno t =null;
        try{
            if(this.cajaSeleccionada!=null){
                parametros= new HashMap<>();
                if(this.cajaSeleccionada.getGENCAJestado().equalsIgnoreCase("A")){
                    parametros.put("sEGUSUcodigo", this.cajaSeleccionada.getSEGUSUcodigo());
                    parametros.put("gENCAJestado", "A");
                    cajaConsulta = entity.findObject(GenmCaja.class, parametros);
                }else{
                    //VALIDAR CONDICION PARA INACTIVAR -- NO POSEA TURNO ACTIVO
                    cajaConsulta = entity.find(GenmCaja.class, this.cajaSeleccionada.getFacscaIdsececaja());
                    parametros.put("segusuLogin", cajaConsulta.getSEGUSUcodigo());
                    cajaConsulta=null;
                    CbpsegmUsuario u = entity.findObject(CbpsegmUsuario.class, parametros);
                    t = posService.consultarTurno(u);
                    if(t!=null)
                        JsfUtil.messageWarning(null, "USUARIO CON TURNO ASIGNADO "+t.getSegturNumero(), "");
                }
                if(t==null && (cajaConsulta==null || this.cajaSeleccionada.equals(cajaConsulta))){
                    parametros= new HashMap<>();
                    parametros.put("genpgeCodigo", this.cajaSeleccionada.getGenpgeCodigo());
                    parametros.put("banentCodigo", this.cajaSeleccionada.getBanentCodigo());
                    parametros.put("banctaCodigo", this.cajaSeleccionada.getBanctaCodigo());
                    cajaConsulta = entity.findObject(GenmCaja.class, parametros);
                    if(cajaConsulta==null || this.cajaSeleccionada.equals(cajaConsulta)){
                        GenmCaja cc= (GenmCaja) entity.merge(this.cajaSeleccionada);
                        if(cc!=null)
                            JsfUtil.messageInfo(null, "REGISTRO EXITOSO", "");
                        else
                            JsfUtil.messageError(null, "REGISTRO NO EXITSO", "");
                    }else{
                        JsfUtil.messageWarning(null, "CTA. CAJA REGISTRADA "+this.cajaSeleccionada.getBanctaCodigo(), "");
                    }
                }else if (cajaConsulta!=null){
                    JsfUtil.messageWarning(null, "USUARIO CON CAJA ASIGNADA "+cajaConsulta.getGENCAJcodigo(), "");
                }
            }
        } catch (Exception e) {
            Logger.getLogger(Caja.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public GenmCaja getCaja() {
        return caja;
    }

    public void setCaja(GenmCaja caja) {
        this.caja = caja;
    }

    public CbpgenrOficina getOficina() {
        return oficina;
    }

    public void setOficina(CbpgenrOficina oficina) {
        this.oficina = oficina;
    }

    public List<CbpgenrOficina> getOficinas() {
        return oficinas;
    }

    public void setOficinas(List<CbpgenrOficina> oficinas) {
        this.oficinas = oficinas;
    }

    public List<CbpgenrUsuarioxofi> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<CbpgenrUsuarioxofi> usuarios) {
        this.usuarios = usuarios;
    }

    public List<FacrVendedor> getVendedores() {
        return vendedores;
    }

    public void setVendedores(List<FacrVendedor> vendedores) {
        this.vendedores = vendedores;
    }

    public GenmCajaLazy getCajasLazy() {
        return cajasLazy;
    }

    public void setCajasLazy(GenmCajaLazy cajasLazy) {
        this.cajasLazy = cajasLazy;
    }

    public GenmCaja getCajaSeleccionada() {
        return cajaSeleccionada;
    }

    public void setCajaSeleccionada(GenmCaja cajaSeleccionada) {
        this.cajaSeleccionada = cajaSeleccionada;
    }

    public List<GenpTalonarios> getTalonarios() {
        return talonarios;
    }

    public void setTalonarios(List<GenpTalonarios> talonarios) {
        this.talonarios = talonarios;
    }

    public List<InvrCablistaprecio> getCablistaprecios() {
        return cablistaprecios;
    }

    public void setCablistaprecios(List<InvrCablistaprecio> cablistaprecios) {
        this.cablistaprecios = cablistaprecios;
    }

    public List<CxcrRuta> getRutas() {
        return rutas;
    }

    public void setRutas(List<CxcrRuta> rutas) {
        this.rutas = rutas;
    }

    public List<GenpTalonarios> getTalonariosDevoluiones() {
        return talonariosDevoluiones;
    }

    public void setTalonariosDevoluiones(List<GenpTalonarios> talonariosDevoluiones) {
        this.talonariosDevoluiones = talonariosDevoluiones;
    }

    public List<MEmpleados> getEmpleados() {
        return empleados;
    }

    public void setEmpleados(List<MEmpleados> empleados) {
        this.empleados = empleados;
    }

    public List<GenpParamgenerales> getTipoEntiades() {
        return tipoEntiades;
    }

    public void setTipoEntiades(List<GenpParamgenerales> tipoEntiades) {
        this.tipoEntiades = tipoEntiades;
    }

    public List<BanmEntidad> getEntidades() {
        return entidades;
    }

    public void setEntidades(List<BanmEntidad> entidades) {
        this.entidades = entidades;
    }

    public List<BanmCuentacaja> getCuentasCaja() {
        return cuentasCaja;
    }

    public void setCuentasCaja(List<BanmCuentacaja> cuentasCaja) {
        this.cuentasCaja = cuentasCaja;
    }
    
}
