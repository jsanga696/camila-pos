/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.comercial.pos;

import com.gma.camilabo.enums.ActionsTransaccion;
import com.gma.camilabp.domain.BanmEntidad;
import com.gma.camilabp.domain.CbpgenrBodega;
import com.gma.camilabp.domain.CbpgenrCompania;
import com.gma.camilabp.domain.CbpposCabfacttemporal;
import com.gma.camilabp.domain.CbpsegmUsuario;
import com.gma.camilabp.domain.CxcmCliente;
import com.gma.camilabp.domain.CxctDocumento;
import com.gma.camilabp.domain.CxctDocumentoPK;
import com.gma.camilabp.domain.FacrVendedor;
import com.gma.camilabp.domain.GenmCaja;
import com.gma.camilabp.domain.GenpTalonarios;
import com.gma.camilabp.domain.InvmItem;
import com.gma.camilabp.domain.InvmItemPK;
import com.gma.camilabp.domain.InvmItemxbodega;
import com.gma.camilabp.domain.MEmpleados;
import com.gma.camilabp.model.consultas.Facturacion;
import com.gma.camilabp.model.consultas.RptCobro;
import com.gma.camilabp.model.consultas.RtpFactura;
import com.gma.camilabp.model.consultas.TempCbpCobUsuarioOpcion;
import com.gma.camilabp.model.consultas.TempCbpPcUsuarioOpcion;
import com.gma.camilabp.model.logica.CxprRetencionesDetalleTemp;
import com.gma.camilabp.model.logica.CxprRetencionesTemp;
import com.gma.camilabp.model.logica.Pago;
import com.gma.camilabp.model.logica.TipoPago;
import com.gma.camilabp.model.logica.ValoresPago;
import com.gma.camilabp.model.logica.Venta;
import com.gma.camilabp.sql.QuerysPos;
import com.gma.camilabp.web.lazy.CbpposCabfacttemporalLazy;
import com.gma.camilabp.web.lazy.CxcmClienteLazy;
import com.gma.camilabp.web.lazy.InvmItemLazy;
import com.gma.camilabp.web.service.BitacoraLocal;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.service.PuntoVentaLocal;
import com.gma.camilabp.web.service.RetencionesServiceLocal;
import com.gma.camilabp.web.service.TransaccionalLocal;
import com.gma.camilabp.web.session.ServletSession;
import com.gma.camilabp.web.session.UserSession;
import com.gma.camilabp.web.util.JsfUtil;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import org.apache.commons.lang3.math.NumberUtils;

/**
 *
 * @author userdb6
 */
@Named(value = "registroVenta")
@ViewScoped
public class RegistroVenta implements Serializable{
    @EJB
    private PuntoVentaLocal posService;
    @EJB(beanName = "entityManagerService")
    private EntityManagerLocal entity;
    @EJB
    private RetencionesServiceLocal retencionesService;
    @EJB
    private TransaccionalLocal transaccionalService;
    @EJB
    private BitacoraLocal bitacora;
    @Inject
    private UserSession userSession;
    @Inject
    private ServletSession datosSession;
    
    private GenmCaja caja;
    private Map<String, Object> parametros;    
    protected CxcmCliente clienteLazy;
    protected CxcmClienteLazy clientesLazy;
    protected InvmItem item=new InvmItem();
    protected InvmItem itemLazy;
    protected InvmItemLazy itemsLazy;
    protected List<TempCbpPcUsuarioOpcion> listItems;
    protected TempCbpPcUsuarioOpcion itemTemp;            
    private String nombreTabla;
    private String nombreTablaCobro;
    private String codigoCliente;
    protected Boolean politicasAplicadas=Boolean.FALSE;
    
    protected ValoresPago valoresPago;
    protected Pago pago;
    private List<BanmEntidad> entidadesBanco;
    private List<BanmEntidad> entidadesTarjetas;
    protected CbpposCabfacttemporalLazy registrosStandby;
    protected CbpposCabfacttemporal registroTemp;
    protected List<CxctDocumento> documentosPago;
    protected List<FacrVendedor> vendedores;    
    protected CxprRetencionesTemp retencion;
    protected MEmpleados empleado;
    private int valor=0;
    protected Venta venta;
    protected String tipoEliminacion;
    
    @PostConstruct
    public void init() {
        try{
            //CARGA INICIAL
            System.out.println(new Date());
            this.venta = new Venta();
            this.itemsLazy = new InvmItemLazy('T','A','S',null,null);
            this.caja = userSession.getCaja();
            this.venta.setTalonario(entity.find(GenpTalonarios.class, userSession.getCaja().getGentalSecuencia().getGENTALsecuencia()));
            this.venta.setNumDocumento(posService.secuenciaDocumento(this.caja.getGENCIAcodigo(), this.venta.getTalonario().getGENTALsecuencia().intValue(), "C"));
            if(userSession.getFormaRv()){
                parametros= new HashMap<>();
                parametros.put("genbodSecuencia", userSession.getCaja().getGENBODcodigo());
                CbpgenrBodega bod= entity.findObject(CbpgenrBodega.class, parametros);
                this.venta.setVendedor(userSession.getCaja().getFACVDRcodigo());
                vendedores= new ArrayList<>();
                parametros= new HashMap<>();
                parametros.put("facrVendedorPK.fACVDRcodigo", this.venta.getVendedor());
                vendedores.add(entity.findObject(FacrVendedor.class, parametros));
                parametros= new HashMap<>();
                parametros.put("fACSUPcodigo", bod.getFacsupCodigo());
                parametros.put("fACVDRestactivo", 'A');
                vendedores.addAll(entity.findAllByParameter(FacrVendedor.class, parametros,null));
            }
            
            parametros= new HashMap<>();
            parametros.put("banmEntidadPK.genciaCodigo", this.caja.getGENCIAcodigo());
            parametros.put("banmEntidadPK.banentTipo", "BC");
            parametros.put("banentEstado", 'A');
            entidadesBanco= entity.findAllByParameter(BanmEntidad.class, parametros,null);
            parametros.put("banmEntidadPK.banentTipo", "TC");
            entidadesTarjetas= entity.findAllByParameter(BanmEntidad.class, parametros,null);
            //CONSULTA DE DATOS GRABADOS
            nombreTablaCobro="X_CBPFACTCOB"+userSession.getCbpsegmUsuario().getSegusuLogin()+"PTOVENTA";
            nombreTabla="X_CBPFACT"+userSession.getCbpsegmUsuario().getSegusuLogin()+"PTOVENTA";      
            //VERIFICACION DE REGISTRO DE CLIENTE
            if(userSession.getRegistrarCliente() && userSession.getCodigoCliente()!=null){
                this.venta.getCliente().setCodigoCliente(userSession.getCodigoCliente());
                this.buscarCliente();
            }                        
            List<TempCbpPcUsuarioOpcion> l=posService.consultarTablaTemporal(nombreTabla);
            if(l!=null){
                listItems = l;
                this.codigoCliente=posService.consultarCodigoClienteTemporal(nombreTabla);
                if(codigoCliente!=null){
                    this.venta.getCliente().setCodigoCliente(codigoCliente);
                    this.buscarCliente();
                }
            }else{
                listItems = new ArrayList<>();
                if(!posService.registroTablaTemporal("SP_FACP_TEMPSAVEFACSERVICE",nombreTabla))
                    JsfUtil.messageError(null, "EXISTE UN ERROR EN EL SISTEMA", "");
            }
            valoresPago=posService.procesarValoresRegistrados(listItems);
            if (this.venta.getCliente().getCodigoCliente()==null) {
                this.venta.setConsumidorFinal(Boolean.TRUE);
                this.consultarConsumidorFinal();
            }else{
                if(this.venta.getCliente().getCXCCLIrucci().trim().equalsIgnoreCase(userSession.getCodigoCF().trim()))
                    this.venta.setConsumidorFinal(Boolean.TRUE);
            }
            System.out.println(new Date());
        } catch (Exception e) {
            Logger.getLogger(RegistroVenta.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void consultarConsumidorFinal(){
        try{
            this.venta.setCliente(new CxcmCliente());
            if (this.venta.getConsumidorFinal()) {
                this.venta.getCliente().setCXCCLIrucci(userSession.getCodigoCF());
                this.venta.setConsumidorFinal(Boolean.FALSE);
                this.buscarCliente();
                this.venta.setConsumidorFinal(Boolean.TRUE);
            }
        } catch (Exception e) {
            Logger.getLogger(RegistroVenta.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void actualizarCodigoCliente(){
        try{
            if(this.clienteLazy!=null)
                this.venta.setCliente(this.clienteLazy);
            this.clienteLazy=null;
            this.venta.getCliente().setCodigoCliente(this.venta.getCliente().getCxcmClientePK().getCXCCLIcodigo());
            if(!this.venta.getCliente().getCxcmClientePK().getCXCCLIcodigo().trim().equalsIgnoreCase(codigoCliente)){
                codigoCliente=this.venta.getCliente().getCxcmClientePK().getCXCCLIcodigo().trim();
                parametros= new HashMap<>();
                parametros.put("mEmpleadosPK.gENCIAcodigo", this.venta.getCliente().getCxcmClientePK().getGENCIAcodigo());
                parametros.put("mEmpleadosPK.gENOFIcodigo", this.venta.getCliente().getCxcmClientePK().getGENOFIcodigo());
                parametros.put("cXCCLIcodigo", this.venta.getCliente().getCxcmClientePK().getCXCCLIcodigo().trim());
                parametros.put("nOMEMPestado", "A");
                this.empleado = this.entity.findObject(MEmpleados.class, parametros);
                posService.actualizarCodigoClienteTemporal(nombreTabla, codigoCliente,this.empleado!=null?this.empleado.getMEmpleadosPK().getGENCIAcodigo():this.caja.getGENCIAcodigo(),this.empleado!=null?this.empleado.getMEmpleadosPK().getGENOFIcodigo():this.caja.getGENOFIcodigo(),this.empleado!=null?this.venta.getCliente().getCXCCLIcodlistaprecio():this.caja.getCablistaprecio().getINVLPRcodigo());
                this.listItems=posService.consultarTablaTemporal(nombreTabla);
                valoresPago=posService.procesarValoresRegistrados(listItems);
                this.politicasAplicadas=Boolean.FALSE;
            }
        } catch (Exception e) {
            Logger.getLogger(RegistroVenta.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void buscarCliente(){
        try{
            this.empleado= null;
            if(!this.venta.getConsumidorFinal()){
                if(this.venta.getCliente().getCodigoCliente()==null && this.venta.getCliente().getCXCCLIrucci()==null && this.venta.getCliente().getCXCCLIrazonsocia()==null){
                    this.clientesLazy= new CxcmClienteLazy(this.caja.getGENCIAcodigo(), this.caja.getGENOFIcodigo(),null,null,null);
                    JsfUtil.updateexecuteJS("frmdlgCliente","PF('dlgCliente').show();");
                }else{
                    List<CxcmCliente> c= posService.consultarCliente(this.venta.getCliente().getCodigoCliente()!=null?this.venta.getCliente().getCodigoCliente().toUpperCase():null,this.venta.getCliente().getCXCCLIrucci(), this.venta.getCliente().getCXCCLIrazonsocia()!=null?this.venta.getCliente().getCXCCLIrazonsocia().trim().toUpperCase():null);
                    if(c==null || c.isEmpty()){
                        JsfUtil.messageInfo(null, "REGISTRO NO ENCONTRADO", "");
                    }else{
                        if(c.size()==1){
                            this.venta.setCliente(c.get(0));
                            this.actualizarCodigoCliente();
                        }else{
                            this.clientesLazy = new CxcmClienteLazy(this.caja.getGENCIAcodigo(), this.caja.getGENOFIcodigo(),this.venta.getCliente().getCodigoCliente()!=null?this.venta.getCliente().getCodigoCliente().toUpperCase():null,this.venta.getCliente().getCXCCLIrucci(),this.venta.getCliente().getCXCCLIrazonsocia()!=null?this.venta.getCliente().getCXCCLIrazonsocia().trim().toUpperCase():null);
                            JsfUtil.messageWarning(null, "MULTIPLES REGISTROS - CONSULTE POR CODIGO", "");
                            JsfUtil.updateexecuteJS("frmdlgCliente","PF('dlgCliente').show();");
                        }
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(RegistroVenta.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void registroCliente(){
        if(!this.venta.getConsumidorFinal()){
            userSession.setRegistrarCliente(Boolean.TRUE);
            JsfUtil.redirectFaces("/faces/pages/cxc/cliente.xhtml");
        }
    }
    
    public void limpiarCliente(){
        if(!this.venta.getConsumidorFinal())
            this.venta.setCliente(new CxcmCliente());
    }    
    
    public void actualizarCodigoItem(){
        try{
            if(this.itemLazy!=null)
                this.item=this.itemLazy;
            this.itemLazy=null;
            this.item.setCodigoItem(this.item.getInvmItemPK().getINVITMcodigo());
        } catch (Exception e) {
            Logger.getLogger(RegistroVenta.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void seleccionarItemLazy(){
        try{
            valor++;
            if(valor>1){
                if(this.itemLazy!=null){
                    this.itemLazy.setCantidadFuncional(BigDecimal.ONE);
                    this.actualizarCodigoItem();
                    JsfUtil.update("mainForm:formVentaCabecera");
                    JsfUtil.executeJS("PF('dlgProducto').hide();focusById('mainForm:cantidadCabeceraProducto');");
                }
                valor =0;
            }            
        } catch (Exception e) {
            Logger.getLogger(RegistroVenta.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void buscarItem(){
        try{
            if(this.item.getCodigoItem()==null && this.item.getINVITMnombre()==null&& this.item.getINVITMcodigbarra()==null){
                if(userSession.getVisualizarItem()){
                    itemsLazy = new InvmItemLazy('T','A','S',null,null);
                    JsfUtil.updateexecuteJS("frmdlgProducto","PF('dlgProducto').show();");
                }else{
                    JsfUtil.messageInfo(null, "INGRESE DATOS PARA LA BUSQUEDA", "");
                }                
            }else{
                List<InvmItem> i= posService.consultarItem(this.item.getCodigoItem()!=null?this.item.getCodigoItem().trim():null, this.item.getINVITMnombre()!=null?this.item.getINVITMnombre().trim().toUpperCase():null,this.item.getINVITMcodigbarra()!=null?this.item.getINVITMcodigbarra().trim():null);
                if(i==null || i.isEmpty()){
                    JsfUtil.messageInfo(null, "REGISTRO NO ENCONTRADO", "");
                }else{
                    if(i.size()==1){
                        this.item= i.get(0);
                        this.actualizarCodigoItem();
                        if(this.item.getINVITMcodigbarra()!=null)
                            this.item.setCantidadFuncional(BigDecimal.ONE);
                    }else{
                        if(userSession.getVisualizarItem()){
                            this.itemsLazy = new InvmItemLazy('T','A','S',this.item.getCodigoItem()!=null?this.item.getCodigoItem().trim():null,this.item.getINVITMnombre()!=null?this.item.getINVITMnombre().trim().toUpperCase():null);
                            JsfUtil.updateexecuteJS("frmdlgProducto","PF('dlgProducto').show();");
                        }else{
                            JsfUtil.messageInfo(null, "MAS DE UN PRODUCTO CON EL MISMO CODIGO", "");
                        }
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(RegistroVenta.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void limpiarItem(){
        this.item= new InvmItem();
    }
    
    public void actualizarRegistro(){
        Collection c= new ArrayList<>();                
        if(this.itemTemp.getFacdpdCantfuncional()!=null && this.itemTemp.getFacdpdCantfuncional().compareTo(BigDecimal.ZERO)>0){
            InvmItem ic= this.entity.find(InvmItem.class, new InvmItemPK(this.itemTemp.getGenciaCodigo(), this.itemTemp.getInvitmCodigo()));            
            if(this.userSession.getValidacionStock()?(this.itemTemp.getFacdpdCantfuncional().compareTo(ic.getINVITMstock())<=0):true){
                InvmItemxbodega ibc= null;
                if(this.userSession.getValidacionStock()){
                    parametros= new HashMap<>();
                    parametros.put("invmItemxbodegaPK.iNVITMcodigo", ic.getInvmItemPK().getINVITMcodigo());
                    parametros.put("invmItemxbodegaPK.gENBODcodigo", this.caja.getGENBODcodigo());            
                    ibc = this.entity.findObject(InvmItemxbodega.class, parametros);
                    if(ibc!=null && ibc.getDisponible().compareTo(BigDecimal.ZERO)>0){
                        if(this.itemTemp.getFacdpdCantfuncional().compareTo(ibc.getDisponible())>0 && ibc.getINVIBOstkreserva().compareTo(BigDecimal.ZERO)>0)
                            JsfUtil.messageInfo(null, "PRODUCTO CON STOCK RESERVADO: "+ibc.getINVIBOstkreserva(), "");
                        this.itemTemp.setFacdpdCantfuncional(this.itemTemp.getFacdpdCantfuncional().compareTo(ibc.getDisponible())>0?ibc.getDisponible():this.itemTemp.getFacdpdCantfuncional());                        
                    }else{
                        JsfUtil.messageInfo(null, "VERIFICAR STOCK", "");
                        return;
                    }
                }
                for (TempCbpPcUsuarioOpcion listItem : this.listItems) {
                    if(listItem.equals(this.itemTemp)){
                        int i = this.listItems.indexOf(listItem);
                        c.add("mainForm:formVentaDetalle:"+i+":subtotal");
                        c.add("mainForm:formVentaDetalle:"+i+":descuento");
                        c.add("mainForm:formVentaDetalle:"+i+":impuesto");
                        c.add("mainForm:formVentaDetalle:"+i+":total");
                        this.valoresPago.actualizarValores(this.itemTemp, 2);
                        this.itemTemp = posService.consultarTemporal(nombreTabla, posService.insertarRegistroTablaTemporal(this.nombreTabla, this.itemTemp));
                        this.listItems.set(this.listItems.indexOf(listItem), this.itemTemp);
                        break;
                    }
                }
                this.valoresPago.actualizarValores(this.itemTemp, 1);
                this.valoresPago.actualizarValoresTotales();
                JsfUtil.update(c);
                if(this.venta.getCliente().getDisponibleCliente().compareTo(BigDecimal.ZERO)>0 
                        && this.venta.getCliente().getDisponibleCliente().compareTo(this.valoresPago.getTotal())<1){
                    JsfUtil.messageInfo(null, "VERIFICAR CREDITO DE CLIENTE", "");
                }
            }else{
                TempCbpPcUsuarioOpcion itt = posService.consultarTemporal(nombreTabla, this.itemTemp.getIdSecuencia());
                this.itemTemp.setFacdpdCantfuncional(itt.getFacdpdCantfuncional());
                JsfUtil.messageInfo(null, "VERIFICAR STOCK", "");
            }
            JsfUtil.update("mainForm:formVentaDetalle");
        }else{
            JsfUtil.messageWarning(null, "CANTIDAD DEBE SER MAYOR A CERO", "");
        }
    }
    
    public void agregarItem(){
        try{
            if(listItems==null)
                listItems= new ArrayList<>();
            if(item!=null && item.getInvmItemPK()!=null && item.getCantidadFuncional()!=null && item.getCantidadFuncional().compareTo(BigDecimal.ZERO)>0){
                if(this.validarClienteSeleccionado()){                    
                    if(this.userSession.getValidacionStock()?(item.getCantidadFuncional().compareTo(item.getINVITMstock())<=0):true){
                        //VALIDAR EXISTENCIA PARA ACTUALIZAR LA CANTIDAD FUNCIONAL
                        InvmItemxbodega ib= null;
                        if(this.userSession.getValidacionStock()){
                            parametros= new HashMap<>();
                            parametros.put("invmItemxbodegaPK.iNVITMcodigo", this.item.getInvmItemPK().getINVITMcodigo());
                            parametros.put("invmItemxbodegaPK.gENBODcodigo", this.caja.getGENBODcodigo());
                            ib = this.entity.findObject(InvmItemxbodega.class, parametros);
                            if(ib!=null){
                                if(ib.getDisponible().compareTo(BigDecimal.ZERO)>0){
                                    if(item.getCantidadFuncional().compareTo(ib.getDisponible())>0 && ib.getINVIBOstkreserva().compareTo(BigDecimal.ZERO)>0)
                                        JsfUtil.messageInfo(null, "PRODUCTO CON STOCK RESERVADO: "+ib.getINVIBOstkreserva(), "");                                           
                                    //AQUI TENGO Q MODIFICAR LA CANTIDAD FUNCIONAL 
                                    item.setCantidadFuncional(item.getCantidadFuncional().compareTo(ib.getDisponible())>0?ib.getDisponible():item.getCantidadFuncional());
                                }else{
                                    JsfUtil.messageInfo(null, "NO HAY DISPONIBLE EN BODEGA", "");
                                    return;
                                }
                            }else{
                                JsfUtil.messageInfo(null, "ITEN NO SE ENCUENTRA EN BODEGA", "");
                                return;
                            }
                        }                        
                        //AQUI DEBE LLEGAR UN VALOR
                        TempCbpPcUsuarioOpcion rt=new TempCbpPcUsuarioOpcion(null, item.getInvmItemPK().getGENCIAcodigo(),this.caja.getGENOFIcodigo(), 
                                new Date(), this.venta.getCliente().getCxcmClientePK().getCXCCLIcodigo(), userSession.getFormaRv()?this.venta.getVendedor():userSession.getCaja().getFACVDRcodigo(), item.getInvmItemPK().getINVITMcodigo(),item.getCantidadFuncional(),
                                item.getINVITMprecilista(), item.getINVITMnombre(), this.empleado!=null?this.venta.getCliente().getCXCCLIcodlistaprecio():userSession.getCaja().getCablistaprecio().getINVLPRcodigo(), userSession.getCaja().getCxcrutCodigo(),
                                item.getINVITMpeso(), item.getINVITMvolumen(), item.getINVITMunidembala());
                        TempCbpPcUsuarioOpcion r=this.existeItemRegistrado(item);
                        if(r!=null){
                            rt.setIdSecuencia(r.getIdSecuencia());
                            if(this.userSession.getValidacionStock() && ib!=null)
                                rt.setFacdpdCantfuncional(r.getFacdpdCantfuncional().add(item.getCantidadFuncional()).compareTo(ib.getDisponible())>0?ib.getDisponible():r.getFacdpdCantfuncional().add(item.getCantidadFuncional()));
                            else
                                rt.setFacdpdCantfuncional(r.getFacdpdCantfuncional().add(item.getCantidadFuncional()));
                            listItems.remove(r);
                            this.valoresPago.actualizarValores(r, 2);
                        }
                        Integer b=posService.insertarRegistroTablaTemporal(this.nombreTabla, rt);
                        if(b!=null){
                            this.item= new InvmItem();
                            rt= posService.consultarTemporal(nombreTabla, b);
                            if(rt.getFacdpdNeto()!=null && rt.getFacdpdNeto().compareTo(BigDecimal.ZERO)>0){
                                listItems.add(rt);
                                this.politicasAplicadas=Boolean.FALSE;
                                this.valoresPago.actualizarValores(rt, 1);
                                this.valoresPago.actualizarValoresTotales();
                            }else{
                                posService.borrarTemporal(nombreTabla, b, null,null);
                                JsfUtil.messageWarning(null, "ITEM CON VALOR CERO", "");
                            }
                        }
                        
                    }else{
                        JsfUtil.messageInfo(null, "VERIFICAR STOCK", "");
                    }
                    if(this.venta.getCliente().getDisponibleCliente().compareTo(BigDecimal.ZERO)>0 
                            && this.venta.getCliente().getDisponibleCliente().compareTo(this.valoresPago.getTotal())<1){
                        JsfUtil.messageInfo(null, "VERIFICAR CREDITO DE CLIENTE", "");
                    }
                }
            }else{
                JsfUtil.messageInfo(null, "REALIZAR BUSQUEDA", "");
            }
        } catch (Exception e) {
            Logger.getLogger(RegistroVenta.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public Boolean validarClienteSeleccionado(){
        if(this.venta.getCliente()!=null && this.venta.getCliente().getCxcmClientePK()!=null)
            return true;
        JsfUtil.messageInfo(null, "REALIZAR BUSQUEDA CLIENTE", "");
        return  false;
    }
    
    public void seleccionarItemTemporal(TempCbpPcUsuarioOpcion it){
        this.itemTemp=it;
    }
    
    public void autorizarTransaccion(){
        if(this.userSession.getPasswordSupervisor()!=null){
            if(this.posService.validarClaveSupervisor(this.caja, this.userSession.getPasswordSupervisor())){
                if(this.userSession.getControlPassword()){
                    parametros= new HashMap<>();
                    parametros.put("genbodSecuencia", userSession.getCaja().getGENBODcodigo());
                    CbpgenrBodega bod= entity.findObject(CbpgenrBodega.class, parametros);                
                    if(this.posService.validarTransaccionSupervisor(bod, this.entity.find(CbpsegmUsuario.class, this.posService.idSupervisor(userSession.getCaja())))){
                        switch (this.tipoEliminacion){
                            case "item":
                                this.eliminarItemRegistroSeleccionado();
                                break;
                            case "standby":
                                this.eliminarItemRegistroStandBy();
                                break;
                        }                        
                        this.userSession.setPasswordSupervisor(null);
                        JsfUtil.executeJS("PF('dlgAutorizacion').hide();");
                    }else{
                        JsfUtil.messageInfo(null, "ACTUALIZAR CONTRASEÑA", "");
                    }
                }else{
                    switch (this.tipoEliminacion){
                        case "item":
                            this.eliminarItemRegistroSeleccionado();
                            break;
                        case "standby":
                            this.eliminarItemRegistroStandBy();
                            break;
                    }
                    this.userSession.setPasswordSupervisor(null);
                    JsfUtil.executeJS("PF('dlgAutorizacion').hide();");
                }
            }else{
                JsfUtil.messageInfo(null, "CONSTRASEÑA INCORRECTA", "");
            }
        }else{
            JsfUtil.messageInfo(null, "CONSTRASEÑA INCORRECTA", "");
        }
    }
    
    public void eliminarItemRegistroSeleccionado(){
        try{
            if(this.itemTemp!=null){
                this.elimiarItemRegistro(this.itemTemp);
                this.itemTemp=null;
            }
        } catch (Exception e) {
            Logger.getLogger(RegistroVenta.class.getName()).log(Level.SEVERE, null, e);
        }
    } 
    
    public void eliminarItemRegistroStandBy(){
        try{
            if(this.registroTemp!=null){
                this.registroTemp.setCbpfaccEstado("E");
                this.entity.merge(this.registroTemp);
                this.bitacora.saveActionLog(ActionsTransaccion.ELIMINAR_STANDBY, ""+this.registroTemp.getCbpfaccCodigo(), this.posService.idSupervisor(this.caja));
                this.registroTemp=null;
                this.cargarTransaccionesStandby();
                JsfUtil.update("frmdlgRegistro");
            }else{
                JsfUtil.messageInfo(null, "SELECCIONE REGISTRO", "");
            }
        } catch (Exception e) {
            Logger.getLogger(RegistroVenta.class.getName()).log(Level.SEVERE, null, e);
        }
    } 
    
    public void elimiarItemRegistro(TempCbpPcUsuarioOpcion it){
        try{
            if(it!=null && !listItems.isEmpty()){
                listItems.remove(it);
                posService.borrarTemporal(nombreTabla, it.getIdSecuencia(), it.getInvitmCodigo(), this.caja);
                this.valoresPago.actualizarValores(it, 0);
                this.valoresPago.actualizarValoresTotales();
                this.politicasAplicadas=Boolean.FALSE;
                JsfUtil.update("mainForm:formVentaCabecera");
                JsfUtil.update("mainForm:formVentaDetalle");
            }
        } catch (Exception e) {
            Logger.getLogger(RegistroVenta.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void aplicarPoliticas(){
        try{
            posService.aplicarPoliciticas(nombreTabla, this.caja.getGENCIAcodigo(), this.caja.getGENOFIcodigo(), userSession.getFormaRv(), this.venta.getVendedor());
            listItems=posService.consultarTablaTemporal(nombreTabla);
            valoresPago=posService.procesarValoresRegistrados(listItems);
            this.politicasAplicadas= Boolean.TRUE;
            JsfUtil.messageInfo(null, "POLITICAS APLICADAS", "");
        } catch (Exception e) {
            Logger.getLogger(RegistroVenta.class.getName()).log(Level.SEVERE, null, e);
        }
    }    
    
    public void prepararPago(){
        try{
            if(this.politicasAplicadas){
                if(this.validarClienteSeleccionado()){
                    posService.borrarTemporalPorTipoPago(nombreTablaCobro, 4);
                    posService.borrarTemporalPorTipoPago(nombreTablaCobro, 5);
                    List<TempCbpCobUsuarioOpcion> lc=posService.consultarTablaTemporalCobro(nombreTablaCobro);
                    this.pago = new Pago(valoresPago.getTotal());
                    if(lc!=null){
                        this.pago.tablaClase(lc);                
                    }else{
                        if(!posService.registroTablaTemporal("POS_FACP_TEMPSAVECOBSERVICE",nombreTablaCobro))
                            JsfUtil.messageError(null, "EXISTE UN ERROR EN EL SISTEMA", "");
                    }
                    this.venta.getCliente().setSaldo((BigDecimal) entity.findObject("SQL", "SELECT ISNULL(SUM(d.CXCDEU_saldomov),0.00) FROM CXCT_DOCUMENTO d (NOLOCK) WHERE d.CXCCLI_CODIGO ='"+this.venta.getCliente().getCxcmClientePK().getCXCCLIcodigo().trim()+"' AND d.CXCDEU_saldomov > 0 AND  d.GENTDO_codigo IN ('FAC', 'NDB', 'NDS', 'PRO') AND  d.GENOFI_codigo ='"+this.venta.getCliente().getCxcmClientePK().getGENOFIcodigo().trim()+"' AND  d.GENCIA_codigo ='"+this.venta.getCliente().getCxcmClientePK().getGENCIAcodigo().trim()+"'", null, null, Boolean.TRUE));
                    this.venta.getCliente().setDisponible(this.venta.getCliente().getCXCCLIcupocredit().subtract(this.venta.getCliente().getSaldo()));
                    this.pago.setValorDisponibleCliente(this.venta.getCliente().getDisponible());
                    this.pago.setHabilitarCredito(!"EFE".equalsIgnoreCase(this.venta.getCliente().getCxcfpgCodigo()) && !"99".equalsIgnoreCase(this.venta.getCliente().getCxctpgCodigo()) && this.venta.getCliente().getDisponible().compareTo(BigDecimal.ZERO)>0);
                    this.pago.setCredito(this.pago.getValorCredito().compareTo(BigDecimal.ZERO)>0);                    
                    parametros= new HashMap<>();
                    parametros.put("oficina", this.venta.getCliente().getCxcmClientePK().getGENOFIcodigo());
                    parametros.put("cliente", this.venta.getCliente().getCxcmClientePK().getCXCCLIcodigo());
                    this.documentosPago = this.entity.findAll(QuerysPos.documentosCreditosByCliente, parametros);
                    this.pago.calcularValores();
                    this.pago.calcularValoresTotales();
                }
            }else{
                JsfUtil.messageWarning(null, "DEBE APLICAR POLITICAS", "");
            }
        } catch (Exception e) {
            Logger.getLogger(RegistroVenta.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    /**
     * 
     * @param tt tipoTransaccion 0:Efectivo 1:Agregar 2:Eliminar 3:Credito
     * @param tp tipoPago 1:Efectivo 2:Cheque 3:Tarjeta 4:Documentos/Retencion 5:Credito
     * @param tipoPago objeto TipoPago
     */
    public void calcularValoresPago(int tt, int tp, TipoPago tipoPago){
        TempCbpCobUsuarioOpcion r;
        Integer idSecuencia;
        try{
            switch(tt){
                case 0:
                    if(this.pago.getValorEfectivo()==null)
                        this.pago.setValorEfectivo(BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP));
                    if(this.pago.getValorEfectivo().compareTo(BigDecimal.ZERO)>0){
                        if(this.pago.getValorEfectivo().compareTo(this.pago.getValorEfectivoAnterior())!=0){
                            r= new TempCbpCobUsuarioOpcion(null, null, this.pago.getValorEfectivo(),null, tp);
                            if(this.pago.getIdSecuencia()!=null){
                                r.setIdSecuencia(this.pago.getIdSecuencia());
                                posService.actualizarRegistroTablaTemporalCobro(nombreTablaCobro, r);
                            }else{                         
                                idSecuencia=posService.insertarRegistroTablaTemporalCobro(nombreTablaCobro, r);
                                this.pago.setIdSecuencia(idSecuencia);
                            }                     
                            this.pago.setValorEfectivoAnterior(this.pago.getValorEfectivo());
                        }
                    }else{
                        if(this.pago.getValorEfectivo().compareTo(this.pago.getValorEfectivoAnterior())!=0){
                            posService.borrarTemporal(nombreTablaCobro, this.pago.getIdSecuencia(),null,null);
                            this.pago.setIdSecuencia(null);
                            this.pago.setValorEfectivoAnterior(this.pago.getValorEfectivo());
                        }
                    }
                    break;
                case 1:
                    switch(tp){
                        case 2:
                            if(this.validarValorPago(this.pago.getPagoCheque().getValor())){
                                if(!this.pago.getPagosCheque().contains(this.pago.getPagoCheque())){
                                    r= new TempCbpCobUsuarioOpcion(this.pago.getPagoCheque(), this.caja);
                                    idSecuencia=posService.insertarRegistroTablaTemporalCobro(nombreTablaCobro, r);
                                    this.pago.getPagoCheque().setIdSecuencia(idSecuencia);
                                }else{
                                    JsfUtil.messageWarning(null, "DATOS REPETIDOS", "");
                                    return;
                                }
                            }
                            break;
                        case 3:
                            if(this.validarValorPago(this.pago.getPagoTarjeta().getValor())){
                                if(NumberUtils.isDigits(this.pago.getPagoTarjeta().getFechaValidez()) && this.pago.getPagoTarjeta().getFechaValidez().length()==6){
                                    if(!this.pago.getPagosTarjeta().contains(this.pago.getPagoTarjeta())){
                                        r= new TempCbpCobUsuarioOpcion(this.pago.getPagoTarjeta(), this.caja);
                                        idSecuencia=posService.insertarRegistroTablaTemporalCobro(nombreTablaCobro, r);
                                        this.pago.getPagoTarjeta().setIdSecuencia(idSecuencia);
                                    }else{
                                        JsfUtil.messageWarning(null, "DATOS REPETIDOS", "");
                                        return;
                                    }
                                }else{
                                    JsfUtil.messageWarning(null, "FECHA VALIDEZ INCORRECTA", "");
                                    return;
                                }
                            }
                            break;
                        case 4:
                            posService.borrarTemporalPorTipoPago(nombreTablaCobro, 4);
                            this.pago.getPagosDocumento().forEach((cxctDocumento) -> {
                                TempCbpCobUsuarioOpcion rt= new TempCbpCobUsuarioOpcion(null, null, cxctDocumento.getCXCDEUsaldomov().abs(),null, 4);
                                //rt.setCxcdeuCodent(cxctDocumento.getGENTDOcodigo());
                                rt.setCxcdeuNumcuenta(""+cxctDocumento.getCxctDocumentoPK().getCXCDEUnumesecuen());
                                rt.setCxcdeuNumcheque(cxctDocumento.getCXCDEUnumdocumen());
                                posService.insertarRegistroTablaTemporalCobro(nombreTablaCobro, rt);
                            });
                            if(this.pago.getRetencion()){
                                TempCbpCobUsuarioOpcion rt= new TempCbpCobUsuarioOpcion(null, null, retencion.getTotal(),null, 4);
                                rt.setCxcdeuCodent("RT");
                                idSecuencia=posService.insertarRegistroTablaTemporalCobro(nombreTablaCobro, rt);
                                CxctDocumento d = new CxctDocumento(new CxctDocumentoPK(userSession.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia(), retencion.getSecuenciafactura()));
                                d.setCXCDEUsaldomov(retencion.getTotal());
                                this.pago.setIdSecuenciaRet(idSecuencia);
                                this.pago.getPagosDocumento().add(d);                                
                            }
                            break;
                        default:
                            break;
                    }
                    this.pago.agregarTipoPago(tp);
                    break;
                case 2:
                    r= new TempCbpCobUsuarioOpcion(tipoPago.getIdSecuencia());
                    if(posService.eliminarRegistroTablaTemporalCobro(nombreTablaCobro, r)!=null)
                        this.pago.eliminarTipoPago(tp, tipoPago);
                    break;                
                default:
                    break;
            }
            this.pago.calcularValores();
            this.pago.calcularValoresTotales();
            if(this.pago.getHabilitarCredito()){
                posService.borrarTemporalPorTipoPago(nombreTablaCobro, 5);
                if(this.pago.getValorCredito().compareTo(BigDecimal.ZERO)>0)
                    posService.insertarRegistroTablaTemporalCobro(nombreTablaCobro, new TempCbpCobUsuarioOpcion(null, null, this.pago.getValorCredito(),null, 5));
            }

        } catch (Exception e) {
            Logger.getLogger(RegistroVenta.class.getName()).log(Level.SEVERE, null, e);
        }
    }    
    
    public void realizarPago(){
        try{
            if(datosSession!=null){
                String validacionCobro=(String) posService.getValorDefault(1, this.caja.getGENCIAcodigo(), null, "COBROPOS");
                if(validacionCobro!=null && validacionCobro.equalsIgnoreCase("S")){
                    if(this.pago.getValorCobro().compareTo(this.pago.getValorTotal())>0){
                        JsfUtil.messageInfo(null, "VALOR DE PAGO DEBE SER MAYOR O IGUAL A VALOR A COBRAR", "");
                        return;
                    }
                }
                //PAGO POR ENCIMA DEL VALOR DE COBRO
                if(!this.pago.getExcedente() && this.pago.getValorDiferencia().compareTo(this.pago.getValorCambioEfectivo())>0){
                    this.pago.setExcedente(Boolean.TRUE);
                    JsfUtil.updateexecuteJS("frmdlgExcedente", "PF('dlgExcedente').show();");
                    return;
                }
                this.calcularValoresPago(0, 1, null);
                Facturacion rf =this.transaccionalService.generarFacturaCobro(this.nombreTabla, this.caja, this.userSession.getFecha(), this.userSession.getTurno(), userSession.getFormaRv(), this.venta.getVendedor(),nombreTablaCobro, this.valoresPago, this.pago);
                if(rf!=null && rf.getFactura()!=null && rf.getFactura().getSecuencia()!=null){
                    posService.grabaDetalleCobro(rf, posService.consultarTablaTemporalCobro(nombreTablaCobro));
                    posService.eliminarTabla(this.nombreTabla);
                    posService.eliminarTabla(this.nombreTablaCobro);
                    JsfUtil.executeJS("PF('dlgPago').hide();");
                    JsfUtil.executeJS("PF('dlgExcedente').hide();");
                    parametros= new HashMap<>();
                    parametros.put("0", userSession.getCbpsegmUsuario().getSegusuLogin());
                    parametros.put("1", rf.getFactura().getSecuencia());
                    entity.executeProcedure("EXEC POS_REPORTEFACTURA ?,?", parametros);
                    
                    parametros = new HashMap<>();
                    parametros.put("genciaSecuencia", userSession.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia());
                    datosSession.agregarParametro("COMPANIA", entity.findObject(CbpgenrCompania.class, parametros));
                    datosSession.agregarParametro("FACVTC_SECUENVTA", rf.getFactura().getSecuencia());
                    datosSession.agregarParametro("USUARIO_REPORTE", userSession.getCbpsegmUsuario().getSegusuLogin());
                    parametros = new HashMap<>();
                    parametros.put("0", rf.getFactura().getSecuencia());
                    parametros.put("1", userSession.getCbpsegmUsuario().getSegusuLogin());
                    datosSession.agregarParametro("FACTURA", entity.findAllSQL("SELECT * FROM RPTFACTURAPOS WHERE secuencia=? AND usuario_reporte=?", parametros, RtpFactura.class));
                    datosSession.agregarParametro("COBROS", entity.findAllSQL("SELECT * FROM RPTCOBROPOS WHERE rpt_secuencia=? AND usuario_reporte=? ", parametros, RptCobro.class));
                    datosSession.agregarParametro("CODIGO", posService.getValorDefault(1, userSession.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia(), null, "CODIGO"));
                    datosSession.agregarParametro("FORMATO", posService.getValorDefault(1, this.userSession.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia(), null, "POSFORMATO"+this.caja.getGENBODcodigo()));
                    JsfUtil.redirectNewTab("/CamilaPOS/Factura");
                    
                    userSession.setRegistrarCliente(Boolean.FALSE);
                    userSession.setCodigoCliente(null);
                    this.init();
                }else{
                    JsfUtil.messageError(null, "EXISTE UN ERROR EN EL SISTEMA", "");
                }
            }
        } catch (Exception e) {
            Logger.getLogger(RegistroVenta.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void cancelarPagoExcedente(){
        this.pago.setExcedente(Boolean.FALSE);
    }
    
    public void realizarPagoDirecto(){
        try{
            this.aplicarPoliticas();
            posService.registroTablaTemporal("POS_FACP_TEMPSAVECOBSERVICE",nombreTablaCobro);
            this.prepararPago();
            this.pago.setValorEfectivo(this.pago.getValorCobro());
            this.calcularValoresPago(0, 1, null);
            this.realizarPago();
            JsfUtil.executeJS("PF('dlgConfirmDialog').hide();");
        } catch (Exception e) {
            Logger.getLogger(RegistroVenta.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public Boolean validarValorPago(BigDecimal valor){
        if(valor!=null && valor.compareTo(BigDecimal.ZERO)>0)
            return Boolean.TRUE;
        JsfUtil.messageWarning(null, "VALORES DEBEN SER MAYORES A CERO", "");
        return Boolean.FALSE;
    }
    
    public TempCbpPcUsuarioOpcion existeItemRegistrado(InvmItem i){
        try{
            for (TempCbpPcUsuarioOpcion listItem : this.listItems)
                if(listItem.getInvitmCodigo().equalsIgnoreCase(i.getInvmItemPK().getINVITMcodigo()))
                    return listItem;
        } catch (Exception e) {
            Logger.getLogger(RegistroVenta.class.getName()).log(Level.SEVERE, null, e);
        }
        return null;
    }
    
    public void registrarTransaccionStandby(){
        try{
            Boolean registroStandby = posService.registroTransaccionStandby(nombreTabla, userSession.getTurno());
            if(registroStandby){
                posService.eliminarTabla(nombreTabla);
                this.init();                
                JsfUtil.messageInfo(null, "TRANSACCION ALMACENADA", "");
            }else{
                JsfUtil.messageError(null, "EXISTE UN ERROR EN EL SISTEMA O VENTA SIN REGISTROS", "");
            }
        } catch (Exception e) {
            Logger.getLogger(RegistroVenta.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void cargarTransaccionesStandby(){
        try{
            this.registrosStandby = new CbpposCabfacttemporalLazy("A", userSession.getTurno().getSegturNumero());
        } catch (Exception e) {
            Logger.getLogger(RegistroVenta.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void cargarTransaccion(){
        try{
            if(this.registroTemp!=null){
                posService.registroTablaTemporal("SP_FACP_TEMPSAVEFACSERVICE",nombreTabla);
                posService.cargarTransaccionesStandby(this.nombreTabla, this.registroTemp);
                this.venta.setConsumidorFinal(Boolean.FALSE);
                this.init();
                JsfUtil.messageInfo(null, "TRANSACCION CARGADA", "");
            }
        } catch (Exception e) {
            Logger.getLogger(RegistroVenta.class.getName()).log(Level.SEVERE, null, e);
        }
    }    
    
    public void aplicarRetencion(){
        try{
            this.retencion= null;
            if(this.pago.getRetencion()){
                retencionesService.registrarDatosRetencionTemporal(userSession.getCbpsegmUsuario().getSegusuLogin(), nombreTabla);
                parametros= new HashMap<>();
                parametros.put("0", userSession.getCbpsegmUsuario().getSegusuLogin());
                parametros.put("1", "X");
                this.retencion = (CxprRetencionesTemp) entity.findObject("SQL", QuerysPos.datosRetencionUser, parametros, CxprRetencionesTemp.class, Boolean.TRUE);
                if (retencion != null) {
                    parametros.put("1", retencion.getNum_factura());
                    retencion.setDetalle(entity.findAllSQL(QuerysPos.datosRetencionUserDetalle, parametros, CxprRetencionesDetalleTemp.class));
                }
                this.calcularValoresPago(1, 4, null);
            }else{
                retencionesService.getEliminarDatosTemporales("X");
                this.pago.eliminarRetencion();
                TipoPago tpr = new TipoPago();
                tpr.setIdSecuencia(this.pago.getIdSecuenciaRet());
                this.calcularValoresPago(2, 4, tpr);
            }
        } catch (Exception e) {
            Logger.getLogger(RegistroVenta.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public CxcmClienteLazy getClientesLazy() {
        return clientesLazy;
    }

    public void setClientesLazy(CxcmClienteLazy clientesLazy) {
        this.clientesLazy = clientesLazy;
    }

    public InvmItem getItem() {
        return item;
    }

    public void setItem(InvmItem item) {
        this.item = item;
    }

    public InvmItemLazy getItemsLazy() {
        return itemsLazy;
    }

    public void setItemsLazy(InvmItemLazy itemsLazy) {
        this.itemsLazy = itemsLazy;
    }

    public List<TempCbpPcUsuarioOpcion> getListItems() {
        return listItems;
    }

    public void setListItems(List<TempCbpPcUsuarioOpcion> listItems) {
        this.listItems = listItems;
    }

    public ValoresPago getValoresPago() {
        return valoresPago;
    }

    public void setValoresPago(ValoresPago valoresPago) {
        this.valoresPago = valoresPago;
    }

    public Pago getPago() {
        return pago;
    }

    public void setPago(Pago pago) {
        this.pago = pago;
    }

    public List<BanmEntidad> getEntidadesBanco() {
        return entidadesBanco;
    }

    public void setEntidadesBanco(List<BanmEntidad> entidadesBanco) {
        this.entidadesBanco = entidadesBanco;
    }

    public List<BanmEntidad> getEntidadesTarjetas() {
        return entidadesTarjetas;
    }

    public void setEntidadesTarjetas(List<BanmEntidad> entidadesTarjetas) {
        this.entidadesTarjetas = entidadesTarjetas;
    }

    public TempCbpPcUsuarioOpcion getItemTemp() {
        return itemTemp;
    }

    public void setItemTemp(TempCbpPcUsuarioOpcion itemTemp) {
        this.itemTemp = itemTemp;
    }

    public InvmItem getItemLazy() {
        return itemLazy;
    }

    public void setItemLazy(InvmItem itemLazy) {
        this.itemLazy = itemLazy;
    }

    public CxcmCliente getClienteLazy() {
        return clienteLazy;
    }

    public void setClienteLazy(CxcmCliente clienteLazy) {
        this.clienteLazy = clienteLazy;
    }

    public Boolean getPoliticasAplicadas() {
        return politicasAplicadas;
    }

    public void setPoliticasAplicadas(Boolean politicasAplicadas) {
        this.politicasAplicadas = politicasAplicadas;
    }

    public CbpposCabfacttemporalLazy getRegistrosStandby() {
        return registrosStandby;
    }

    public void setRegistrosStandby(CbpposCabfacttemporalLazy registrosStandby) {
        this.registrosStandby = registrosStandby;
    }

    public CbpposCabfacttemporal getRegistroTemp() {
        return registroTemp;
    }

    public void setRegistroTemp(CbpposCabfacttemporal registroTemp) {
        this.registroTemp = registroTemp;
    }

    public List<CxctDocumento> getDocumentosPago() {
        return documentosPago;
    }

    public void setDocumentosPago(List<CxctDocumento> documentosPago) {
        this.documentosPago = documentosPago;
    }

    public List<FacrVendedor> getVendedores() {
        return vendedores;
    }

    public void setVendedores(List<FacrVendedor> vendedores) {
        this.vendedores = vendedores;
    }

    public CxprRetencionesTemp getRetencion() {
        return retencion;
    }

    public void setRetencion(CxprRetencionesTemp retencion) {
        this.retencion = retencion;
    }

    public Venta getVenta() {
        return venta;
    }

    public void setVenta(Venta venta) {
        this.venta = venta;
    }

    public String getTipoEliminacion() {
        return tipoEliminacion;
    }

    public void setTipoEliminacion(String tipoEliminacion) {
        this.tipoEliminacion = tipoEliminacion;
    }

}
