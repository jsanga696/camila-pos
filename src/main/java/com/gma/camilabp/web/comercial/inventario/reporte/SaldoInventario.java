/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.comercial.inventario.reporte;

import com.gma.camilabo.enums.TiposItem;
import com.gma.camilabo.enums.TiposSaldosInventario;
import com.gma.camilabp.domain.CbpgenrOficina;
import com.gma.camilabp.domain.CbpsegmOpcion;
import com.gma.camilabp.domain.CxpmProveedor;
import com.gma.camilabp.domain.CxpmProveedorPK;
import com.gma.camilabp.domain.InvrCablistaprecio;
import com.gma.camilabp.model.consultas.InveRepdiasInventario;
import com.gma.camilabp.model.consultas.SaldoInventarioModel;
import com.gma.camilabp.model.consultas.TipoFiltro;
import com.gma.camilabp.sql.QuerysPos;
import com.gma.camilabp.web.lazy.CbpgenrOficinaLazyTest;
import com.gma.camilabp.web.lazy.CxpmProveedorLazy;
import com.gma.camilabp.web.lazy.InvmItemLazy;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.service.InventarioLocal;
import com.gma.camilabp.web.session.ServletSession;
import com.gma.camilabp.web.session.UserSession;
import com.gma.camilabp.web.util.JsfUtil;
import com.gma.camilabp.web.util.StringValues;
import com.gma.camilabp.web.util.Utilidades;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

/**
 *
 * @author userdb6
 */
@Named(value = "saldoInventario")
@ViewScoped
public class SaldoInventario implements Serializable{    
    @Inject
    private UserSession userSession;
    @Inject
    private ServletSession datosSession;
    @EJB
    private EntityManagerLocal entity;
    @EJB
    private InventarioLocal inventarioService;
    
    protected SaldoInventarioModel modelSaldoInventario;
    protected CbpgenrOficinaLazyTest oficinasLazy;
    protected CxpmProveedorLazy proveedoresLazy;
    protected InvmItemLazy itemsLazy;
    protected List<TiposSaldosInventario> listTiposSaldos;
    protected List<TiposItem> listTiposItem;
    protected List<TipoFiltro> filtros;
    protected List<TipoFiltro> consultaFiltro;
    protected List<TipoFiltro> consultaFiltroValues;
    protected List<InvrCablistaprecio> cablistaprecios;
    protected List<InveRepdiasInventario> listInventario;
    protected List<TipoFiltro> listado;
    
    private Map<String, Object> parametros;
    private int validador=0;

    @PostConstruct
    public void init() {
        try{
            this.modelSaldoInventario = new SaldoInventarioModel();
            this.modelSaldoInventario.setOficina((CbpgenrOficina) Utilidades.getFirstElement(userSession.getOficinas()));
            this.seleccionOficina();
            this.oficinasLazy = new CbpgenrOficinaLazyTest(Utilidades.getIdsOfList(userSession.getOficinas()));
            this.listTiposSaldos= new ArrayList<>();
            this.listTiposItem= new ArrayList<>();
            this.listTiposSaldos.addAll(Arrays.asList(TiposSaldosInventario.values()));
            this.listTiposItem.addAll(Arrays.asList(TiposItem.values()));
            this.filtros= new ArrayList<>();
            this.filtros.add(new TipoFiltro("PROV", "PROVEEDOR"));
            this.filtros.addAll(inventarioService.nivelesInventario(userSession.getCbpsegmUsuario().getGenciaCodigo()));
            this.filtros.add(new TipoFiltro("PR", "PRODUCTO"));
        } catch (Exception e) {
            Logger.getLogger(SaldoInventario.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void seleccionOficina(){
        this.modelSaldoInventario.setSecuenciaOficina(this.modelSaldoInventario.getOficina().getGenofiSecuencia());
        this.parametros= new HashMap<>();
        this.parametros.put(StringValues.campoOficina, this.modelSaldoInventario.getOficina().getGenofiSecuencia());
        this.parametros.put("iNVLPRtipoLista", "P");
        this.cablistaprecios = entity.findAllByParameter(InvrCablistaprecio.class, parametros,null);
    }
    
    public void busquedaOficina(){
        try{
            if(this.modelSaldoInventario.getSecuenciaOficina()!=null && this.modelSaldoInventario.getSecuenciaOficina().trim().length()>0){
                this.parametros= new HashMap<>();
                parametros.put("genofiSecuencia", this.modelSaldoInventario.getSecuenciaOficina().trim());
                this.modelSaldoInventario.setOficina(this.entity.findObject(CbpgenrOficina.class, parametros));
            }else{
                this.modelSaldoInventario.setOficina(new CbpgenrOficina());
            }
            if(this.modelSaldoInventario.getOficina()==null || this.modelSaldoInventario.getOficina().getGenofiCodigo()==null){
                this.cablistaprecios = null;
                JsfUtil.update("frmdlgOficina");
                JsfUtil.executeJS("PF('dlgOficina').show();");
            }else{
                this.seleccionOficina();
            }
        } catch (Exception e) {
            Logger.getLogger(SaldoInventario.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void busquedaFiltro(){
        try{
            String query="";
            this.consultaFiltro=null;
            this.modelSaldoInventario.setNivel(null);
            this.consultaFiltroValues=null;
            if(this.modelSaldoInventario.getFiltro()){
                this.modelSaldoInventario.limpiarDatosFiltro();
                if(this.modelSaldoInventario.getCodigoBusquedaFiltro()==null || this.modelSaldoInventario.getCodigoBusquedaFiltro().trim().length()==0){
                    this.parametros= new HashMap<>();
                    this.parametros.put("0", userSession.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia());
                    switch(this.modelSaldoInventario.getCodigoFiltro()){
                        case "PROV":
                            this.proveedoresLazy = new CxpmProveedorLazy(null, userSession.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia());
                            JsfUtil.update("frmdlgProveedor");
                            JsfUtil.executeJS("PF('dlgProveedor').show();");
                            break;
                        case "PR":
                            this.itemsLazy= new InvmItemLazy();
                            JsfUtil.update("frmdlgProducto");
                            JsfUtil.executeJS("PF('dlgProducto').show();");
                            break;
                        default:
                            query=this.inventarioService.queryNivel(this.modelSaldoInventario.getCodigoFiltro());
                            this.consultaFiltro= this.entity.findAllSQL(query, parametros, TipoFiltro.class);
                            JsfUtil.update("frmdlgNivel");
                            JsfUtil.executeJS("PF('dlgNivel').show();");
                            break;
                    }
                }else{
                    this.parametros= new HashMap<>();
                    System.out.println(this.modelSaldoInventario.getCodigoBusquedaFiltro().trim().toUpperCase());
                    switch(this.modelSaldoInventario.getCodigoFiltro()){
                        case "PROV":
                            this.modelSaldoInventario.setProvedor(this.entity.find(CxpmProveedor.class, new CxpmProveedorPK(userSession.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia(), this.modelSaldoInventario.getOficina().getGenofiSecuencia(), this.modelSaldoInventario.getCodigoBusquedaFiltro().trim().toUpperCase())));
                            if(this.modelSaldoInventario.getProvedor()!=null){
                                this.modelSaldoInventario.setDatoBusquedaFiltro(this.modelSaldoInventario.getProvedor().getCXPPROrazonsocia());
                            }
                            break;
                        case "N1":
                            break;
                        case "N2":
                            break;
                        case "N3":
                            break;
                        case "N4":
                            break;
                        case "PR":
                            break;
                        default:
                            break;
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(SaldoInventario.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void seleccionarDato(){
        switch(this.modelSaldoInventario.getCodigoFiltro()){
            case "PROV":
                if(this.modelSaldoInventario.getProvedor()!=null){
                    this.modelSaldoInventario.setDatoBusquedaFiltro(this.modelSaldoInventario.getProvedor().getCXPPROrazonsocia());
                    this.modelSaldoInventario.setCodigoBusquedaFiltro(this.modelSaldoInventario.getProvedor().getCxpmProveedorPK().getCXPPROcodigo());
                }
                break;            
            case "PR":
                if(this.modelSaldoInventario.getItem()!=null){
                    this.modelSaldoInventario.setDatoBusquedaFiltro(this.modelSaldoInventario.getItem().getINVITMnombre());
                    this.modelSaldoInventario.setCodigoBusquedaFiltro(this.modelSaldoInventario.getItem().getInvmItemPK().getINVITMcodigo());
                }
                break;
            default:
                System.out.println(this.modelSaldoInventario.getNivel());
                if(this.modelSaldoInventario.getNivel()!=null){
                    this.modelSaldoInventario.setDatoBusquedaFiltro(this.modelSaldoInventario.getNivel().getDato());
                    this.modelSaldoInventario.setCodigoBusquedaFiltro(this.modelSaldoInventario.getNivel().getCodigo());
                }
                break;
        }
    }
    
    public Boolean validarFormulario(SaldoInventarioModel s){
        if(s==null){
            JsfUtil.messageWarning(null, "REALIZAR EL PROCESO NUEVAMENTE", "");
            return Boolean.FALSE;
        }else if(s.getOficina()==null){
            JsfUtil.messageWarning(null, "DEBE SELECCIONAR OFICINA", "");
            return Boolean.FALSE;
        }else if(s.getEstadosItem()==null || s.getEstadosItem().isEmpty()){
            JsfUtil.messageWarning(null, "DEBE SELECCIONAR ESTADO(S)", "");
            return Boolean.FALSE;
        }else if(s.getBodegas()==null || s.getBodegas().isEmpty()){
            JsfUtil.messageWarning(null, "DEBE SELECCIONAR BODEGA(S)", "");
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }
    
    public void realizarConsulta(){
        try{
            if(this.validarFormulario(this.modelSaldoInventario)){
                this.parametros= new HashMap<>();
                this.parametros.put("segopcUrl", userSession.obtenerUrl());
                CbpsegmOpcion opc = this.entity.findObject(CbpsegmOpcion.class, this.parametros);
                this.modelSaldoInventario.setUsuario(userSession.getCbpsegmUsuario().getSegusuLogin());
                this.modelSaldoInventario.setOpcion(opc.getSegopcSecuencia());
                this.modelSaldoInventario.setMaquina(JsfUtil.getNamePC());
                this.modelSaldoInventario.setCodigoCompania(userSession.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia());
                this.listInventario=inventarioService.reporteSaldoInventario(this.modelSaldoInventario);
                this.listado = new ArrayList<>();
                validador=0;
                this.listInventario.stream().map((i) -> new TipoFiltro(i.getINVIN1_codin1item().trim(), i.getINVIN1_descripcio())).forEachOrdered((t) -> {                                             
                    if(!this.listado.contains(t)){
                        List<InveRepdiasInventario> l= this.listInventario.stream().filter(n1->n1.getINVIN1_codin1item().equalsIgnoreCase(t.getCodigo())).collect(Collectors.toList());
                        t.setFuncional(new BigDecimal(l.stream().mapToInt(f->f.getINVITM_stockreal()).sum()));
                        t.setUnidades(new BigDecimal(l.stream().mapToInt(u->u.getINVITM_unidades()).sum()));
                        t.setCaja(new BigDecimal(l.stream().mapToInt(c->c.getINVITM_cajas()).sum()));
                        this.listado.add(t);
                        validador=0;
                    }else{
                        if(validador==0){
                            List<InveRepdiasInventario> l= this.listInventario.stream().filter(n1->n1.getINVIN1_codin1item().trim().equalsIgnoreCase(t.getCodigo())).collect(Collectors.toList());
                            l.stream().map((i1) -> new TipoFiltro(i1.getINVIN2_codin2item().trim(), i1.getINVIN2_descripcio())).forEachOrdered((t1) -> {                                                                                                      
                                if(!this.listado.get(this.listado.indexOf(t)).getLista().contains(t1)){
                                    t1.setFuncional(new BigDecimal(l.stream().filter(n2->n2.getINVIN2_codin2item().equalsIgnoreCase(t1.getCodigo())).mapToInt(f->f.getINVITM_stockreal()).sum()));
                                    t1.setUnidades(new BigDecimal(l.stream().filter(n2->n2.getINVIN2_codin2item().equalsIgnoreCase(t1.getCodigo())).mapToInt(u->u.getINVITM_unidades()).sum()));
                                    t1.setCaja(new BigDecimal(l.stream().filter(n2->n2.getINVIN2_codin2item().equalsIgnoreCase(t1.getCodigo())).mapToInt(c->c.getINVITM_cajas()).sum()));
                                    this.listado.get(this.listado.indexOf(t)).getLista().add(t1);
                                }
                            });
                            validador=1;
                        }
                    }
                });
                
                listado.forEach((tipoFiltro) -> {
                    tipoFiltro.getLista().forEach((tipoFiltro1) -> {
                        List<InveRepdiasInventario> l= this.listInventario.stream().
                                filter(n1->n1.getINVIN1_codin1item().trim().equalsIgnoreCase(tipoFiltro.getCodigo()) 
                                        && n1.getINVIN2_codin2item().trim().equalsIgnoreCase(tipoFiltro1.getCodigo())).collect(Collectors.toList());
                        l.stream().map((i) -> new TipoFiltro(i.getINVIN3_codin3item().trim(), i.getINVIN3_descripcio())).forEachOrdered((t) -> {                                                           
                            if(!tipoFiltro1.getLista().contains(t)){
                                List<InveRepdiasInventario> l1= l.stream().filter(nl1->nl1.getINVIN3_codin3item().trim().equalsIgnoreCase(t.getCodigo())).collect(Collectors.toList());
                                t.setFuncional(new BigDecimal(l1.stream().mapToInt(c->c.getINVITM_stockreal()).sum()));
                                t.setUnidades(new BigDecimal(l1.stream().mapToInt(c->c.getINVITM_unidades()).sum()));
                                t.setCaja(new BigDecimal(l1.stream().mapToInt(c->c.getINVITM_cajas()).sum()));
                                tipoFiltro1.getLista().add(t);
                            }
                        });
                    });
                });
                listado.forEach((tipoFiltro) -> {
                    tipoFiltro.getLista().forEach((tipoFiltro1) -> {
                        tipoFiltro1.getLista().forEach((tipoFiltro2) -> {
                            List<InveRepdiasInventario> l= this.listInventario.stream().
                                    filter(n1->n1.getINVIN1_codin1item().trim().equalsIgnoreCase(tipoFiltro.getCodigo()) 
                                            && n1.getINVIN2_codin2item().trim().equalsIgnoreCase(tipoFiltro1.getCodigo())
                                            && n1.getINVIN3_codin3item().trim().equalsIgnoreCase(tipoFiltro2.getCodigo())
                                    ).collect(Collectors.toList());
                            l.stream().map((i) -> new TipoFiltro(i.getINVIN4_codin4item().trim(), i.getINVIN4_descripcio())).forEachOrdered((t) -> {                                                                             
                                if(!tipoFiltro2.getLista().contains(t)){
                                    List<InveRepdiasInventario> l1= l.stream().filter(nl1->nl1.getINVIN4_codin4item().trim().equalsIgnoreCase(t.getCodigo())).collect(Collectors.toList());
                                    t.setFuncional(new BigDecimal(l1.stream().mapToInt(c->c.getINVITM_stockreal()).sum()));
                                    t.setUnidades(new BigDecimal(l1.stream().mapToInt(c->c.getINVITM_unidades()).sum()));
                                    t.setCaja(new BigDecimal(l1.stream().mapToInt(c->c.getINVITM_cajas()).sum()));
                                    t.setListaItem(l.stream().filter(item->item.getINVIN4_codin4item().trim().equalsIgnoreCase(t.getCodigo())).collect(Collectors.toList()));
                                    tipoFiltro2.getLista().add(t);
                                }
                            });
                        });
                    });
                });
                
            }            
        } catch (Exception e) {
            Logger.getLogger(SaldoInventario.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void generarReporte(){
        try{
            if(datosSession!=null){
                datosSession.borrarDatos();
                datosSession.setTieneDatasource(Boolean.TRUE);
                datosSession.setDataSource(this.listado);
                datosSession.setNombreReporte("inventario/saldoInventario");
                datosSession.agregarParametro("USUARIO",this.userSession.getCbpsegmUsuario().getSegusuLogin());
                datosSession.agregarParametro("COMPANIA",this.modelSaldoInventario.getCodigoCompania());
                datosSession.agregarParametro("OFICINA",this.modelSaldoInventario.getOficina().getGenofiSecuencia()+"-"+this.modelSaldoInventario.getOficina().getGenofiNombre());
                datosSession.agregarParametro("TIPO_REPORTE",this.modelSaldoInventario.getCodigoTipoSaldoInventario());
                datosSession.agregarParametro("TIPO_PRODUCTO",this.modelSaldoInventario.getCodigoTipoItem());
                datosSession.agregarParametro("ESTADOS",this.modelSaldoInventario.getEstadosItem());
                datosSession.agregarParametro("STOCK",this.modelSaldoInventario.getCodigoStock());
                datosSession.agregarParametro("BODEGAS", this.modelSaldoInventario.getCadenaBodega());
                datosSession.agregarParametro("FILTRO", this.modelSaldoInventario.getFiltro()?(this.modelSaldoInventario.getCodigoBusquedaFiltro()+"-"+this.modelSaldoInventario.getDatoBusquedaFiltro()):"");
                datosSession.agregarParametro("FECHA_CORTE",this.modelSaldoInventario.getCorte()?this.modelSaldoInventario.getFechaCorte():"");
                datosSession.agregarParametro("TIPO_LISTA",this.modelSaldoInventario.getCablistaprecio().getINVLPRnombre());
                datosSession.agregarParametro("TIPO_COSTO",this.modelSaldoInventario.getTipoCostoReporte());
                JsfUtil.redirectNewTab("/CamilaPOS/Documento");
            }
        } catch (Exception e) {
            Logger.getLogger(SaldoInventario.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public List<TiposSaldosInventario> getListTiposSaldos() {
        return listTiposSaldos;
    }

    public void setListTiposSaldos(List<TiposSaldosInventario> listTiposSaldos) {
        this.listTiposSaldos = listTiposSaldos;
    }

    public List<TiposItem> getListTiposItem() {
        return listTiposItem;
    }

    public void setListTiposItem(List<TiposItem> listTiposItem) {
        this.listTiposItem = listTiposItem;
    }

    public SaldoInventarioModel getModelSaldoInventario() {
        return modelSaldoInventario;
    }

    public void setModelSaldoInventario(SaldoInventarioModel modelSaldoInventario) {
        this.modelSaldoInventario = modelSaldoInventario;
    }

    public CbpgenrOficinaLazyTest getOficinasLazy() {
        return oficinasLazy;
    }

    public void setOficinasLazy(CbpgenrOficinaLazyTest oficinasLazy) {
        this.oficinasLazy = oficinasLazy;
    }

    public List<TipoFiltro> getFiltros() {
        return filtros;
    }

    public void setFiltros(List<TipoFiltro> filtros) {
        this.filtros = filtros;
    }

    public List<TipoFiltro> getConsultaFiltro() {
        return consultaFiltro;
    }

    public void setConsultaFiltro(List<TipoFiltro> consultaFiltro) {
        this.consultaFiltro = consultaFiltro;
    }

    public CxpmProveedorLazy getProveedoresLazy() {
        return proveedoresLazy;
    }

    public void setProveedoresLazy(CxpmProveedorLazy proveedoresLazy) {
        this.proveedoresLazy = proveedoresLazy;
    }

    public InvmItemLazy getItemsLazy() {
        return itemsLazy;
    }

    public void setItemsLazy(InvmItemLazy itemsLazy) {
        this.itemsLazy = itemsLazy;
    }

    public List<TipoFiltro> getConsultaFiltroValues() {
        return consultaFiltroValues;
    }

    public void setConsultaFiltroValues(List<TipoFiltro> consultaFiltroValues) {
        this.consultaFiltroValues = consultaFiltroValues;
    }

    public List<InvrCablistaprecio> getCablistaprecios() {
        return cablistaprecios;
    }

    public void setCablistaprecios(List<InvrCablistaprecio> cablistaprecios) {
        this.cablistaprecios = cablistaprecios;
    }

    public List<InveRepdiasInventario> getListInventario() {
        return listInventario;
    }

    public void setListInventario(List<InveRepdiasInventario> listInventario) {
        this.listInventario = listInventario;
    }

    public List<TipoFiltro> getListado() {
        return listado;
    }

    public void setListado(List<TipoFiltro> listado) {
        this.listado = listado;
    }
    
}
