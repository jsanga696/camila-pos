/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.comercial.cxc.depositotc;

import com.gma.camilabp.domain.BanmCuentacaja;
import com.gma.camilabp.domain.BanmEntidad;
import com.gma.camilabp.domain.CbpgenrBodega;
import com.gma.camilabp.domain.CbpgenrOficina;
import com.gma.camilabp.domain.CxctDetdocumcr;
import com.gma.camilabp.model.consultas.IngresoRegDepoCheque;
import com.gma.camilabp.model.consultas.TarjCredModel;
import com.gma.camilabp.sql.QueryPosCxc;
import com.gma.camilabp.web.lazy.CbpgenrBodegaLazy;
import com.gma.camilabp.web.lazy.CbpgenrOficinaLazyTest;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.service.GeneralLocal;
import com.gma.camilabp.web.service.TarjetaCreditoLocal;
import com.gma.camilabp.web.session.UserSession;
import com.gma.camilabp.web.util.JsfUtil;
import com.gma.camilabp.web.util.Utilidades;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author userdb5
 */
@Named(value = "depTarjCredView")
@ViewScoped
public class DepTarjCredView implements Serializable{
    
    @Inject
    private UserSession session;
    @EJB
    private EntityManagerLocal entity;
    @EJB
    private GeneralLocal generalEjb;
    @EJB
    private TarjetaCreditoLocal tcEjb;
    
    private CbpgenrOficinaLazyTest oficinasLazy;
    private HashMap<String, Object> dataIngreso;
    private List<CxctDetdocumcr> documcrList;
    private List<String> opciones, usuarios, cajas, filtersSelected;
    private CbpgenrBodegaLazy bodegasLazy;
    private List<TarjCredModel> pagosFiltered, pagosSelected;
    private List<BanmCuentacaja> cuentasBancosList;
    private String nombreTablaTemp;
    private IngresoRegDepoCheque modelIngreso;
    
    @PostConstruct
    public void initView(){
        HashMap numDeposito =  new HashMap();
        this.oficinasLazy = new CbpgenrOficinaLazyTest(Utilidades.getIdsOfList(this.session.getOficinas()));
        dataIngreso = new HashMap<String, Object>();
        dataIngreso.put("oficina", (CbpgenrOficina)Utilidades.getFirstElement(this.session.getOficinas()));
        llenarLists();
        dataIngreso.put("filtro", 1);
        dataIngreso.put("allUsers", Boolean.FALSE);
        dataIngreso.put("allCajas", Boolean.FALSE);
        dataIngreso.put("turno", 0);
        dataIngreso.put("guardarDatos", Boolean.FALSE);
        dataIngreso.put("desde", new Date());
        dataIngreso.put("hasta", new Date());
        dataIngreso.put("usuarioSelected", this.session.getCbpsegmUsuario().getSegusuLogin());
        if(this.cajas != null && !this.cajas.isEmpty())
            dataIngreso.put("cajaSelected", this.cajas.get(0));
        else
            dataIngreso.put("cajaSelected", "");
        dataIngreso.put("banco", new BanmEntidad());
        dataIngreso.put("fechaDeposito", new Date());
        dataIngreso.put("ctaBanco", new BanmCuentacaja());        
        dataIngreso.put("tc", new CbpgenrOficina());
        dataIngreso.put("num_papeleta", "0");
        dataIngreso.put("valEst", "");
        this.dataIngreso.put("val_comision", BigDecimal.ZERO);
        this.dataIngreso.put("monto_dep", BigDecimal.ZERO);
        
        /*deposito.put(this, cajas);
        deposito.put(this, cajas);*/
        
        numDeposito.put("0", session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia());
        numDeposito.put("1", "" + ((CbpgenrOficina)dataIngreso.get("oficina")).getGenofiSecuencia());
        numDeposito.put("2", "CXC");
        numDeposito.put("3", "DCT");
        numDeposito.put("4", "");
        numDeposito.put("5", "");
        numDeposito.put("6", "");
        numDeposito.put("7", "");
        numDeposito.put("8", "G");
        
        dataIngreso.put("numDeposito", entity.findObject("SQL", "DECLARE @NUM_SECUENCIAL int EXECUTE sp_geNC_NUMSECUENC "
                + "?,?,?,?,?,?,?,?,?,@NUM_SECUENCIAL OUT SELECT @NUM_SECUENCIAL", numDeposito, null, Boolean.TRUE));
         
        dataIngreso.put("valOfi", ((CbpgenrOficina)dataIngreso.get("oficina")).getGenofiSecuencia());
        dataIngreso.put("valCta", "");
        dataIngreso.put("establecimiento", new CbpgenrBodega());
        cuentasBancosList = entity.findListByParameters("SELECT b FROM BanmCuentacaja b WHERE b.banmCuentacajaPK.bANENTtipo = :BANENTtipo "
                + "AND b.banmCuentacajaPK.gENCIAcodigo = :gENCIAcodigo AND b.gENOFIcodigo = :gENOFIcodigo", new String[]{"BANENTtipo", "gENCIAcodigo", "gENOFIcodigo"}, 
                new Object[]{"TC", session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia(), ((CbpgenrOficina)dataIngreso.get("oficina")).getGenofiSecuencia()});
        
        this.obtenerFiltro();
        nombreTablaTemp = "X_CBP_" + session.getCbpsegmUsuario().getSegusuLogin()+"_CXCP_REGDEPOTC";
        String resultado = (String) entity.findObject("SQL", "SELECT CAST(NAME AS VARCHAR(50)) AS NAME FROM SYSOBJECTS WHERE name = '" + 
                nombreTablaTemp + "'", null, null, Boolean.FALSE);
        if (resultado == null){
            HashMap parametros = new HashMap();
            parametros.put("0", nombreTablaTemp);
            entity.executeProcedure("EXECUTE POS_CBP_TEMPDEPOSITOTC ?", parametros);
        }else{
            String query = QueryPosCxc.getPagosTCTemp;
            query = query.replace("-nombre-tabla-temp-", nombreTablaTemp);
            pagosFiltered = entity.findAllSQL(query, null, TarjCredModel.class);
            completarDatos();
        }
        this.llenarCampos();
    }
    
    public void llenarCampos(){        
        dataIngreso.put("flujo_caja", pagosFiltered.stream().mapToDouble(c->c.getCXCDPG_valormov().doubleValue()).sum());
        dataIngreso.put("monto_dep", pagosFiltered.stream().mapToDouble(c -> c.getValorTotalAlDeposito().doubleValue()).sum());
    }
    
    public void busquedaCtaBanco(){
        try{
            Object o = this.entity.findListByParameters("SELECT bcc FROM BanmCuentacaja bcc WHERE bcc.banmCuentacajaPK.gENCIAcodigo = :gENCIAcodigo AND "
                    + "bcc.gENOFIcodigo = :gENOFIcodigo AND bcc.banmCuentacajaPK.bANENTtipo = :bANENTtipo AND bcc.banmCuentacajaPK.bANCTAcodigo = :bANCTAcodigo", 
                    new String[]{"gENCIAcodigo", "gENOFIcodigo", "bANENTtipo", "bANCTAcodigo"}, 
                    new Object[]{session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia(),
                        ((CbpgenrOficina)dataIngreso.get("oficina")).getGenofiSecuencia(), 
                        "TC", dataIngreso.get("valCta")});
            if(o != null && !((List)o).isEmpty()){
                this.dataIngreso.put("ctaBanco", ((List)o).get(0));
            }else{
                JsfUtil.update("ctsBcoFrm");
                JsfUtil.executeJS("PF('wdBancos').show();");
            }
        }catch(Exception e){
            Logger.getLogger(DepTarjCredView.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void busquedaOficina(){
        try{
            Object o = this.generalEjb.consultarOficina((String)this.dataIngreso.get("valOfi"));
            if(o != null)
                this.dataIngreso.put("oficina", (CbpgenrOficina)o);
            else{
                JsfUtil.update("frmdlgOficina");
                JsfUtil.executeJS("PF('dlgOficina').show();");
            }
        } catch (Exception e) {
            Logger.getLogger(DepTarjCredView.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void busquedaBodega(){
        try{
            HashMap m = new HashMap();
            m.put("genbodSecuencia", dataIngreso.get("valEst"));
            m.put("genofiCodigo", ((CbpgenrOficina)dataIngreso.get("oficina")));
            Object o =  this.entity.findObject(CbpgenrBodega.class, m);
            if(o != null)
                this.dataIngreso.put("establecimiento", (CbpgenrBodega)o);
            else{
                JsfUtil.update("establecimientosFrm");
                JsfUtil.executeJS("PF('wdEstablecimiento').show();");
            }
        } catch (Exception e) {
            Logger.getLogger(DepTarjCredView.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void selectEstablecimiento(SelectEvent event){
        dataIngreso.put("valEst", ((CbpgenrBodega)dataIngreso.get("establecimiento")).getGenbodSecuencia());
    }
    
    public void llenarLists(){
        cajas = entity.findListByParameters("SELECT DISTINCT caja.gENCAJcodigo FROM GenmCaja caja "
                + "WHERE caja.gENCIAcodigo = :gENCIAcodigo AND caja.gENOFIcodigo = :gENOFIcodigo "
                + "ORDER BY caja.gENCAJcodigo ASC", new String[]{"gENCIAcodigo", "gENOFIcodigo"}, 
                new Object[]{session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia(),
                    ((CbpgenrOficina)dataIngreso.get("oficina")).getGenofiSecuencia()});
        usuarios = entity.findListByParameters("SELECT DISTINCT usuario.segusuLogin FROM CbpsegmUsuario usuario "
                + "WHERE usuario.genciaCodigo.genciaCodigo = :genciaCodigo ORDER BY usuario.segusuLogin ASC", 
                new String[]{"genciaCodigo"}, new Object[]{session.getCbpsegmUsuario().getGenciaCodigo().getGenciaCodigo()});
        this.selectOficina(null);
    }
    
    public void completarDatos(){
        HashMap m = new HashMap();
        m.put("0", session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia());
        for(TarjCredModel pf : pagosFiltered){
            m.put("1", ((CbpgenrOficina)dataIngreso.get("oficina")).getGenofiSecuencia());
            m.put("2", pf.getCXCCLI_codigo());
            String nombre = (String)entity.findObject("SQL", "SELECT CXCCLI_razonsocia FROM CXCM_CLIENTE WHERE GENCIA_codigo = ? AND GENOFI_codigo = ? AND CXCCLI_codigo = ?", m, null, Boolean.FALSE);
            pf.setNombreCliente(nombre);
            
            m.put("1", pf.getGENBAN_tipo());
            m.put("2", pf.getGENBAN_codigo());
            String nombreBanco = (String)entity.findObject("SQL", "SELECT BANENT_nombre FROM BANM_ENTIDAD WHERE GENCIA_codigo = ? AND BANENT_tipo = ? AND BANENT_codigo = ?", m, null, Boolean.FALSE);
            pf.setNombreBanco(nombreBanco);
        }
    }
    
    public void selectOficina(SelectEvent event){
        dataIngreso.put("valOfi", ((CbpgenrOficina)dataIngreso.get("oficina")).getGenofiCodigo());
        bodegasLazy = new CbpgenrBodegaLazy(((CbpgenrOficina)dataIngreso.get("oficina")).getGenofiCodigo());
    }
    
    public void guardarDatos(){
        try{
            if(pagosSelected.isEmpty()){
                JsfUtil.messageInfo(null, "Debe seleccionar una o varias transacciones para continuar", "");
                return;
            }
            
            dataIngreso.put("guardarDatos", Boolean.TRUE);
            this.llenarDatos();
            modelIngreso = new IngresoRegDepoCheque();
            modelIngreso.setNAMETABLE(nombreTablaTemp);
            modelIngreso.setBANCTA_codigo(((BanmCuentacaja)dataIngreso.get("ctaBanco")).getBanmCuentacajaPK().getBANCTAcodigo());
            modelIngreso.setBANENT_codigo(((BanmCuentacaja)dataIngreso.get("ctaBanco")).getBanmCuentacajaPK().getBANENTcodigo());
            modelIngreso.setFECHA_REGISTRO(new Date());
            modelIngreso.setGENCIA_codigo(session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia());
            modelIngreso.setGENOFI_codigo(((CbpgenrOficina)dataIngreso.get("oficina")).getGenofiSecuencia());
            modelIngreso.setNUM_PAPELETA(""+dataIngreso.get("num_papeleta"));
            modelIngreso.setSEGNAQ_codigo(JsfUtil.getNamePC());
            modelIngreso.setSEGUSU_codigo(session.getCbpsegmUsuario().getSegusuLogin());
            tcEjb.guardarTarjetaCredito(modelIngreso, null);
            
            String borrarTabla = QueryPosCxc.borrarDataTempTb;
            borrarTabla = borrarTabla.replace("-nombre-tabla-temp-", nombreTablaTemp);
            
            entity.executeSQL(borrarTabla, null);
            
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public void filtrar(){
        HashMap map = new HashMap();
        Integer i = 0;
        
        String query = QueryPosCxc.getPagosTC;
        
        Collections.sort(this.filtersSelected);
        
        for(String s : this.filtersSelected){
            
            if(s.equals("1")){
                    map.put(i+"", dataIngreso.get("turno"));
                    query = query + " AND sc.FACSCA_idsececaja = ?";
            }
            if(s.equals("2")){
                map.put(i+"", dataIngreso.get("desde"));
                i = i + 1;
                map.put(i+"", dataIngreso.get("hasta"));
                query = query + " AND det.CXCDPG_fecha BETWEEN ? AND ?";
            }
            if(s.equals("3")){
                if(!(Boolean)dataIngreso.get("allUsers")){
                    map.put(i+"", dataIngreso.get("usuarioSelected"));
                    query = query + " AND cab.FACCVT_usuaingreg = ?";
                }
            }
            if(s.equals("4")){
                if(!(Boolean)dataIngreso.get("allCajas")){
                    map.put(i+"", dataIngreso.get("cajaSelected"));
                    query = query + " AND sc.GENCAJ_codigo = ?";
                }
            }
            i = i + 1;
        }
        pagosFiltered = entity.findAllSQL(query, map, TarjCredModel.class);
        this.llenarDatos();
        this.llenarCampos();
    }
    
    public void llenarDatos(){
        entity.executeSQL("DELETE FROM " +nombreTablaTemp, null);
        String query = "INSERT INTO " + this.nombreTablaTemp + " (CXCDEU_numesecuen, CXCCCR_numsecuen, "
                + "CXCDPG_numlinea, CXCDEU_valorpago, CXCCLI_nombre, CXCDPG_secubanco) VALUES (?,?,?,?,?,?)";
        HashMap m = new HashMap();
        HashMap temp = new HashMap();
        m.put("0", session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia());
        m.put("1", ((CbpgenrOficina)dataIngreso.get("oficina")).getGenofiSecuencia());
        if((Boolean)dataIngreso.get("guardarDatos")){
            for(TarjCredModel ptc : pagosSelected){
                temp.put("0", 0);
                temp.put("1", ptc.getCXCCCR_numdocumcr());
                temp.put("2", ptc.getCXCDPG_numlinea());
                temp.put("3", ptc.getCXCDPG_valormov());
                temp.put("4", ptc.getCXCCLI_codigo());
                temp.put("5", ptc.getCXCDPG_numerosec());
                entity.executeSQL(query, temp);
            }
        }else{
            for(TarjCredModel ptc : pagosFiltered){
                temp.put("0", 0);
                temp.put("1", ptc.getCXCCCR_numdocumcr());
                temp.put("2", ptc.getCXCDPG_numlinea());
                temp.put("3", ptc.getCXCDPG_valormov());
                temp.put("4", ptc.getCXCCLI_codigo());
                temp.put("5", ptc.getCXCDPG_numerosec());
                entity.executeSQL(query, temp);
            }
        }
        completarDatos();
        dataIngreso.put("guardarDatos", Boolean.FALSE);
    }
    
    public void verData(){
        if(this.pagosSelected != null)
            System.out.println(this.pagosSelected.size());
        else
            System.out.println("NULL");
    }
    
    public void selectCtaBco(SelectEvent event){
        HashMap map = new HashMap();
        this.dataIngreso.put("valCta", ((BanmCuentacaja)dataIngreso.get("ctaBanco")).getBanmCuentacajaPK().getBANCTAcodigo());
        map.put("tipo", ((BanmCuentacaja)dataIngreso.get("ctaBanco")).getBanmCuentacajaPK().getBANENTtipo());
        map.put("codigo", ((BanmCuentacaja)dataIngreso.get("ctaBanco")).getBanmCuentacajaPK().getBANENTcodigo());
        
        this.dataIngreso.put("banco", entity.findObject("HQL", "SELECT b FROM BanmEntidad b WHERE "
                + "b.banmEntidadPK.banentTipo = :tipo AND b.banmEntidadPK.banentCodigo = :codigo", map, BanmEntidad.class, Boolean.FALSE));
    }
    
    public void sumarComision(){
        BigDecimal total = BigDecimal.ZERO;
        Collection listaUpdate = new ArrayList();
        
        for(TarjCredModel temp : pagosFiltered){
            int i = this.pagosFiltered.indexOf(temp);
            listaUpdate.add("mainForm:tab_ingreso:dtPago:"+i+":val_total");
            listaUpdate.add("mainForm:tab_ingreso:dtPago:"+i+":val_com");
            if(temp.getValorComision().compareTo(temp.getCXCDPG_valormov()) == 1){
                temp.setValorComision(BigDecimal.ZERO);
                return;
            }
            total = total.add(temp.getValorComision());
        }
        dataIngreso.put("val_comision", total);
        dataIngreso.put("monto_dep", pagosFiltered.stream().mapToDouble(c -> c.getValorTotalAlDeposito().doubleValue()).sum());
        listaUpdate.add("mainForm:tab_ingreso:val_total_comision");
        listaUpdate.add("mainForm:tab_ingreso:monto_deposito");
        JsfUtil.update(listaUpdate);
    }
    
    public void selectTC(SelectEvent event){
    
    }
    
    public void prepararTC(){
        
    }
    
    public void obtenerFiltro(){
        switch(Integer.parseInt(dataIngreso.get("filtro")+"")){
            case 1:                
                opciones = entity.findListByParameters("SELECT DISTINCT doc.cXCDPGfecha FROM CxctDetdocumcr doc "
                        + "ORDER BY doc.cXCDPGfecha ASC", new String[]{}, new Object[]{});
                break;
                
            case 2:
                opciones = entity.findListByParameters("SELECT DISTINCT caja.fACSCAidsececaja FROM FactCabvtacaja caja "
                        + "ORDER BY caja.fACSCAidsececaja ASC", new String[]{}, new Object[]{});
                break;
        }
    }

    public HashMap<String, Object> getDataIngreso() {
        return dataIngreso;
    }

    public void setDataIngreso(HashMap<String, Object> dataIngreso) {
        this.dataIngreso = dataIngreso;
    }

    public List<CxctDetdocumcr> getDocumcrList() {
        return documcrList;
    }

    public void setDocumcrList(List<CxctDetdocumcr> documcrList) {
        this.documcrList = documcrList;
    }

    public List<String> getOpciones() {
        return opciones;
    }

    public void setOpciones(List<String> opciones) {
        this.opciones = opciones;
    }

    public List<String> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<String> usuarios) {
        this.usuarios = usuarios;
    }

    public List<String> getCajas() {
        return cajas;
    }

    public void setCajas(List<String> cajas) {
        this.cajas = cajas;
    }

    public CbpgenrOficinaLazyTest getOficinasLazy() {
        return oficinasLazy;
    }

    public void setOficinasLazy(CbpgenrOficinaLazyTest oficinasLazy) {
        this.oficinasLazy = oficinasLazy;
    }

    public CbpgenrBodegaLazy getBodegasLazy() {
        return bodegasLazy;
    }

    public void setBodegasLazy(CbpgenrBodegaLazy bodegasLazy) {
        this.bodegasLazy = bodegasLazy;
    }

    public List<TarjCredModel> getPagosFiltered() {
        return pagosFiltered;
    }

    public void setPagosFiltered(List<TarjCredModel> pagosFiltered) {
        this.pagosFiltered = pagosFiltered;
    }

    public List<String> getFiltersSelected() {
        return filtersSelected;
    }

    public void setFiltersSelected(List<String> filtersSelected) {
        this.filtersSelected = filtersSelected;
    }

    public List<TarjCredModel> getPagosSelected() {
        return pagosSelected;
    }

    public void setPagosSelected(List<TarjCredModel> pagosSelected) {
        this.pagosSelected = pagosSelected;
    }

    public List<BanmCuentacaja> getCuentasBancosList() {
        return cuentasBancosList;
    }

    public void setCuentasBancosList(List<BanmCuentacaja> cuentasBancosList) {
        this.cuentasBancosList = cuentasBancosList;
    }
    
}
