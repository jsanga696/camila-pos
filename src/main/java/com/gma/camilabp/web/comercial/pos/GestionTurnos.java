/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.comercial.pos;

import com.gma.camilabo.enums.ActionsTransaccion;
import com.gma.camilabp.domain.CbpgenrOficina;
import com.gma.camilabp.domain.CbpgenrRegistroDenominacion;
import com.gma.camilabp.domain.CbpsegmTurno;
import com.gma.camilabp.domain.FactSesioncaja;
import com.gma.camilabp.domain.GenmCaja;
import com.gma.camilabp.model.consultas.Estadistica;
import com.gma.camilabp.model.consultas.FormaPago;
import com.gma.camilabp.model.consultas.Importe;
import com.gma.camilabp.sql.QuerysPos;
import com.gma.camilabp.web.lazy.CbpsegmTurnoLazy;
import com.gma.camilabp.web.lazy.FactCabvtacajaLazy;
import com.gma.camilabp.web.service.BitacoraLocal;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.service.PuntoVentaLocal;
import com.gma.camilabp.web.session.ServletSession;
import com.gma.camilabp.web.session.UserSession;
import com.gma.camilabp.web.util.JsfUtil;
import com.gma.camilabp.web.util.Utilidades;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

/**
 *
 * @author userdb6
 */
@Named(value = "gestionTurnos")
@ViewScoped
public class GestionTurnos implements Serializable{
    @EJB
    private PuntoVentaLocal posService;
    @EJB
    private EntityManagerLocal entity;
    @EJB
    private BitacoraLocal bitacora;
    @Inject
    private ServletSession datosSession;
    @Inject
    private UserSession userSession;
    
    private Map<String, Object> parametros;
    protected CbpsegmTurnoLazy turnosDia;
    protected CbpsegmTurnoLazy turnosHistorial;
    protected CbpsegmTurno turno;
    protected List<CbpgenrRegistroDenominacion> asignaciones;
    protected List<CbpgenrRegistroDenominacion> egresos;
    protected List<CbpgenrRegistroDenominacion> cierre;
    protected CbpgenrRegistroDenominacion rd;
    protected FactCabvtacajaLazy ventasLazy;
    
    
    @PostConstruct
    public void init() {
        try{
            turnosDia = new CbpsegmTurnoLazy(Utilidades.getDate(new Date()),null,null,1);
            turnosHistorial = new CbpsegmTurnoLazy(Utilidades.getDate(new Date()),null,null,0);
        } catch (Exception e) {
            Logger.getLogger(Transacciones.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void seleccionTurno(){
        try{
            this.asignaciones= new ArrayList<>();
            this.cierre = new ArrayList<>();
            if(this.turno!=null){
                parametros= new HashMap<>();
                parametros.put("segturCodigo", this.turno);
                parametros.put("genrdeTipo", 1L);
                this.asignaciones= entity.findAllByParameter(CbpgenrRegistroDenominacion.class, parametros,null);
                parametros.put("genrdeTipo", 2L);
                this.egresos= entity.findAllByParameter(CbpgenrRegistroDenominacion.class, parametros,null);
                parametros.put("genrdeTipo", 3L);
                this.cierre= entity.findAllByParameter(CbpgenrRegistroDenominacion.class, parametros,null);
                this.parametros = new HashMap<>();
                this.parametros.put("gENCAJcodigo", this.turno.getSegusuCodigo().getCodigoCaja());
                GenmCaja caja = this.entity.findObject(GenmCaja.class, this.parametros);
                ventasLazy = new FactCabvtacajaLazy(caja.getGENCIAcodigo(), caja.getGENOFIcodigo(), this.turno.getSegturNumero().intValue());
            }
        } catch (Exception e) {
            Logger.getLogger(Transacciones.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void inactivarRegistro(){
        try{
            if(this.rd!=null){
                Long countTransacciones = this.posService.cantidadTransaccionesTurno(this.turno);
                if(countTransacciones.equals(0L)){
                    this.rd.setEstado(Boolean.FALSE);
                    entity.merge(this.rd);
                    parametros= new HashMap<>();
                    parametros.put("fACSCAidsececaja", this.turno.getSegturNumero().intValue());
                    FactSesioncaja sc=  entity.findObject(FactSesioncaja.class, parametros);
                    sc.setFACSCAsalinimov(sc.getFACSCAsalinimov().subtract(this.rd.getGenrdeMonto()));
                    entity.merge(sc);
                    bitacora.saveActionLog(ActionsTransaccion.INACTIVAR_ASIGNACION, this.rd.getGenrdeMonto().toString());
                }else{
                    JsfUtil.messageWarning(null, "NO DEBE HABER REALIZADO TRANSACCIONES", "");
                }
            }
            this.turno = entity.find(CbpsegmTurno.class, this.turno.getSegturCodigo());
        } catch (Exception e) {
            Logger.getLogger(Transacciones.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void cierreTurno(){
        if(datosSession!=null){
            posService.reporteCaja(this.userSession.getCbpsegmUsuario().getSegusuLogin(), this.turno.getSegturNumero());
            datosSession.borrarDatos();
            datosSession.setTieneDatasource(Boolean.FALSE);
            datosSession.setNombreReporte("cierreTurno");
            datosSession.agregarParametro("NUMERO_TURNO",this.turno.getSegturNumero());
            JsfUtil.redirectNewTab("/CamilaPOS/Documento");
        }
    }
    
    public void transaccionesPreliminar(){
        /*if(datosSession!=null){
            posService.reporteCaja(this.userSession.getCbpsegmUsuario().getSegusuLogin(), this.turno.getSegturNumero());
            datosSession.borrarDatos();
            datosSession.setTieneDatasource(Boolean.FALSE);
            datosSession.setNombreReporte("resumenTransacciones");
            datosSession.agregarParametro("SUBREPORT_DIR", datosSession.getPath()+ "/reportes/pos/");
            datosSession.agregarParametro("NUMERO_TURNO",this.turno.getSegturNumero());
            JsfUtil.redirectNewTab("/camilaPOS/Documento");
        }*/
        if(datosSession!=null){
            posService.reporteCaja(this.userSession.getCbpsegmUsuario().getSegusuLogin(), this.turno.getSegturNumero());
            datosSession.borrarDatos();
            this.parametros = new HashMap<>();
            this.parametros.put("gENCAJcodigo", this.turno.getSegusuCodigo().getCodigoCaja());
            GenmCaja caja = this.entity.findObject(GenmCaja.class, this.parametros);
            datosSession.agregarParametro("CAJA", caja);
            this.parametros = new HashMap<>();
            this.parametros.put("genofiSecuencia", caja.getGENOFIcodigo());
            datosSession.agregarParametro("OFICINA", this.entity.findObject(CbpgenrOficina.class, this.parametros));            
            datosSession.agregarParametro("USUARIO", this.turno.getSegusuCodigo());
            datosSession.agregarParametro("TURNO", this.turno);
            this.parametros = new HashMap<>();
            this.parametros.put("0", this.turno.getSegturNumero());
            this.parametros.put("1", this.userSession.getCbpsegmUsuario().getSegusuLogin());
            datosSession.agregarParametro("IMPORTE", entity.findObject("SQL",QuerysPos.importe, this.parametros, Importe.class, Boolean.TRUE));
            datosSession.agregarParametro("ESTADISTICA", entity.findObject("SQL",QuerysPos.estadistica, this.parametros, Estadistica.class, Boolean.TRUE));
            datosSession.agregarParametro("TOTALES", entity.findObject("SQL",QuerysPos.formaPago, this.parametros, FormaPago.class, Boolean.TRUE));
            datosSession.agregarParametro("FORMATO", posService.getValorDefault(1, this.userSession.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia(), null, "POSFORMATO"));
            datosSession.agregarParametro("RESTRINGIR", "S".equalsIgnoreCase((String)posService.getValorDefault(1, this.userSession.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia(), null, "RPTVALORES"))?!posService.esSupervisor(this.userSession.getCbpsegmUsuario()):false);
            JsfUtil.redirectNewTab("/CamilaPOS/Preliminar");
        }
    }
    
    public void transaccionesDefinitivo(){
        /*if(datosSession!=null){
            posService.reporteCaja(this.userSession.getCbpsegmUsuario().getSegusuLogin(), this.turno.getSegturNumero());
            datosSession.borrarDatos();
            datosSession.setTieneDatasource(Boolean.FALSE);
            datosSession.setNombreReporte("resumenTransaccionesCierre");
            datosSession.agregarParametro("NUMERO_TURNO",this.turno.getSegturNumero());
            JsfUtil.redirectNewTab("/camilaPOS/Documento");                        
        }*/
        
        if(datosSession!=null){
            datosSession.borrarDatos();
            if(this.turno.getSegturEstado()){
                posService.reporteCaja(this.userSession.getCbpsegmUsuario().getSegusuLogin(), this.turno.getSegturNumero());
            }else{
                this.parametros = new HashMap<>();
                this.parametros.put("0", this.turno.getSegturNumero());
                this.parametros.put("1", this.userSession.getCbpsegmUsuario().getSegusuLogin());
                String usuario = (String) this.entity.findObject("SQL", "SELECT DISTINCT usuario_reporte FROM RPTIMPORTE WHERE turno=? AND usuario_reporte=?", this.parametros, null, Boolean.TRUE);
                if(usuario==null)
                    posService.reporteCaja(this.userSession.getCbpsegmUsuario().getSegusuLogin(), this.turno.getSegturNumero());
            }
            this.parametros = new HashMap<>();
            this.parametros.put("gENCAJcodigo", this.turno.getSegusuCodigo().getCodigoCaja());
            GenmCaja caja = this.entity.findObject(GenmCaja.class, this.parametros);
            datosSession.agregarParametro("CAJA", caja);
            this.parametros = new HashMap<>();
            this.parametros.put("genofiSecuencia", caja.getGENOFIcodigo());
            datosSession.agregarParametro("OFICINA", this.entity.findObject(CbpgenrOficina.class, this.parametros));            
            datosSession.agregarParametro("USUARIO", this.turno.getSegusuCodigo());
            datosSession.agregarParametro("TURNO", this.turno);
            this.parametros = new HashMap<>();
            this.parametros.put("0", this.turno.getSegturNumero());
            this.parametros.put("1", this.userSession.getCbpsegmUsuario().getSegusuLogin());
            datosSession.agregarParametro("FORMATO",posService.getValorDefault(1, this.userSession.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia(), null, "POSFORMATO"));
            datosSession.agregarParametro("IMPORTE", entity.findObject("SQL",QuerysPos.importe, this.parametros, Importe.class, Boolean.TRUE));
            datosSession.agregarParametro("ESTADISTICA", entity.findObject("SQL",QuerysPos.estadistica, this.parametros, Estadistica.class, Boolean.TRUE));
            datosSession.agregarParametro("TOTALES", entity.findObject("SQL",QuerysPos.formaPago, this.parametros, FormaPago.class, Boolean.TRUE));
            JsfUtil.redirectNewTab("/CamilaPOS/Cierre");
        }
    }

    public CbpsegmTurnoLazy getTurnosDia() {
        return turnosDia;
    }

    public void setTurnosDia(CbpsegmTurnoLazy turnosDia) {
        this.turnosDia = turnosDia;
    }

    public CbpsegmTurnoLazy getTurnosHistorial() {
        return turnosHistorial;
    }

    public void setTurnosHistorial(CbpsegmTurnoLazy turnosHistorial) {
        this.turnosHistorial = turnosHistorial;
    }

    public CbpsegmTurno getTurno() {
        return turno;
    }

    public void setTurno(CbpsegmTurno turno) {
        this.turno = turno;
    }

    public List<CbpgenrRegistroDenominacion> getAsignaciones() {
        return asignaciones;
    }

    public void setAsignaciones(List<CbpgenrRegistroDenominacion> asignaciones) {
        this.asignaciones = asignaciones;
    }

    public List<CbpgenrRegistroDenominacion> getCierre() {
        return cierre;
    }

    public void setCierre(List<CbpgenrRegistroDenominacion> cierre) {
        this.cierre = cierre;
    }

    public CbpgenrRegistroDenominacion getRd() {
        return rd;
    }

    public void setRd(CbpgenrRegistroDenominacion rd) {
        this.rd = rd;
    }

    public List<CbpgenrRegistroDenominacion> getEgresos() {
        return egresos;
    }

    public void setEgresos(List<CbpgenrRegistroDenominacion> egresos) {
        this.egresos = egresos;
    }

    public FactCabvtacajaLazy getVentasLazy() {
        return ventasLazy;
    }

    public void setVentasLazy(FactCabvtacajaLazy ventasLazy) {
        this.ventasLazy = ventasLazy;
    }
    
}
