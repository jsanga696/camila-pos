/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.comercial.compras;

import com.gma.camilabp.domain.CbpgenrBodega;
import com.gma.camilabp.domain.CbpgenrOficina;
import com.gma.camilabp.domain.ComtCabcompra;
import com.gma.camilabp.domain.ComtCabpedsugerido;
import com.gma.camilabp.domain.ComtDetcompra;
import com.gma.camilabp.domain.ComtDetpedsugerido;
import com.gma.camilabp.domain.CxpmProveedor;
import com.gma.camilabp.domain.CxprEstableprovee;
import com.gma.camilabp.domain.GenpParamgenerales;
import com.gma.camilabp.domain.GenrMoneda;
import com.gma.camilabp.domain.InvmItem;
import com.gma.camilabp.domain.InvmItemPK;
import com.gma.camilabp.model.consultas.IngresoBodegaDetModel;
import com.gma.camilabp.model.consultas.IngresoBodegaModel;
import com.gma.camilabp.model.consultas.ResultadoIngresoCompra;
import com.gma.camilabp.model.consultas.TipoFiltro;
import com.gma.camilabp.sql.QuerysPos;
import com.gma.camilabp.web.lazy.CbpgenrBodegaLazy;
import com.gma.camilabp.web.lazy.CbpgenrOficinaLazyTest;
import com.gma.camilabp.web.lazy.ComtCabcompraLazy;
import com.gma.camilabp.web.lazy.ComtCabpedsugeridoLazy;
import com.gma.camilabp.web.lazy.CxpmProveedorLazy;
import com.gma.camilabp.web.lazy.CxprEstableproveeLazy;
import com.gma.camilabp.web.lazy.InvmItemLazy;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.service.GeneralLocal;
import com.gma.camilabp.web.service.IngresoCompraLocal;
import com.gma.camilabp.web.service.PuntoVentaLocal;
import com.gma.camilabp.web.session.UserSession;
import com.gma.camilabp.web.util.JsfUtil;
import com.gma.camilabp.web.util.StringValues;
import com.gma.camilabp.web.util.Utilidades;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author userdb5
 */
@Named(value = "ingresoBodegaView")
@ViewScoped
public class IngresoBodegaView implements Serializable {
    @Inject
    private UserSession session;
    @EJB
    private EntityManagerLocal entity;
    @EJB
    private GeneralLocal generalEjb;
    @EJB
    private IngresoCompraLocal comprasEjb;
    @EJB
    private PuntoVentaLocal posService;

    private CbpgenrOficinaLazyTest oficinasLazy;
    private InvmItem item;
    private InvmItemLazy productosLazy;
    private InvmItem itemLazy;
    private List<InvmItem> items;
    private HashMap<String, Object> dataIngreso;
    private List<GenrMoneda> monedas;
    private List<GenpParamgenerales> provincias;
    private ComtCabpedsugeridoLazy sugeridoList;
    private ComtCabpedsugerido sugerido;
    private IngresoBodegaModel compra;
    private CbpgenrBodegaLazy bodegasLazy;
    private CxprEstableproveeLazy facturasLazy;
    private List<ComtDetpedsugerido> detalleSugerido;
    protected CxpmProveedorLazy proveedoresLazy;
    protected InvmItem itemRegistro;
    protected ComtCabcompraLazy ingBodegaLazy;
    protected ComtCabcompra ingBodega;
    private List<TipoFiltro> resumen;
    private BigDecimal impPlastico;
    private Boolean cargaSugerido=Boolean.FALSE;

    @PostConstruct
    public void initView() {
        this.compra = new IngresoBodegaModel();        
        this.ingBodegaLazy = new ComtCabcompraLazy("COM","PDS");
        this.oficinasLazy = new CbpgenrOficinaLazyTest(Utilidades.getIdsOfList(this.session.getOficinas()));
        dataIngreso = new HashMap<>();
        dataIngreso.put("oficina", (CbpgenrOficina) Utilidades.getFirstElement(this.session.getOficinas()));
        compra.setGENOFI_codigo(((CbpgenrOficina) dataIngreso.get("oficina")).getGenofiSecuencia());
        dataIngreso.put("proveedor", new CxpmProveedor());
        dataIngreso.put("factura", null);        
        dataIngreso.put("bodega", new CbpgenrBodega());
        dataIngreso.put("conSugerido", Boolean.TRUE);
        provincias = generalEjb.obtenerParametrosGeneralesPorCodigo("023");
        this.compra.setCOMCCM_numdocumen(this.generalEjb.numeroDocumentoCompra(this.session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia(), ((CbpgenrOficina) dataIngreso.get("oficina")).getGenofiSecuencia(), "COMCXPCOMPRA"));
        this.item = new InvmItem();
        productosLazy = new InvmItemLazy();
        monedas = entity.findAll(GenrMoneda.class);
        items = new ArrayList();
        bodegasLazy = null;
        sugeridoList = null;
        this.itemRegistro = new InvmItem();
        this.impPlastico = new BigDecimal(""+this.posService.getValorDefault(1, this.session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia(), null, "IMPREDPLAS"));        
    }
    
    public void busquedaOficina() {
        try {
            Object o = this.generalEjb.consultarOficina(this.compra.getGENOFI_codigo());
            if (o != null)
                this.dataIngreso.put("oficina", (CbpgenrOficina) o);
            else
                JsfUtil.updateexecuteJS("frmdlgOficina", "PF('dlgOficina').show();");
        } catch (Exception e) {
            Logger.getLogger(IngresoBodegaView.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public void selectOficina() {
        this.compra.setGENOFI_codigo(((CbpgenrOficina) dataIngreso.get("oficina")).getGenofiSecuencia());
    }

    public void buscarProveedorCampo() {
        try {
            if (dataIngreso.get("oficina")== null) {
                JsfUtil.messageError(null, "Seleccione una oficina", "");
                return;
            }
            String secuenciaOficina = ((CbpgenrOficina)this.dataIngreso.get("oficina")).getGenofiSecuencia();
            this.proveedoresLazy = new CxpmProveedorLazy(secuenciaOficina, this.session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia());
            this.dataIngreso.put("proveedor", this.generalEjb.consultarProveedor(this.session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia(), secuenciaOficina, this.compra.getCXPPRO_codigo()!=null?(this.compra.getCXPPRO_codigo()).toUpperCase():null));
            this.seleccionarProveedor();
        } catch (Exception e) {
            Logger.getLogger(IngresoBodegaView.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void seleccionarProveedor(){
        this.compra.setCXPPRO_codigo(((CxpmProveedor)dataIngreso.get("proveedor")).getCxpmProveedorPK()!=null?((CxpmProveedor)dataIngreso.get("proveedor")).getCxpmProveedorPK().getCXPPROcodigo():null);        
    }

    public void validarFactura() {
        CxpmProveedor p=(CxpmProveedor) dataIngreso.get("proveedor");
        if (dataIngreso.get("proveedor") == null || p.getCxpmProveedorPK()==null || p.getCxpmProveedorPK().getCXPPROcodigo()==null) {
            JsfUtil.messageInfo(null, "Debe seleccionar previamente el proveedor.", "");
            return;
        }
        facturasLazy = new CxprEstableproveeLazy(p);
        JsfUtil.updateexecuteJS("idDlgFactura","PF('wdFactura').show()");
    }

    public void setFactura() {
        this.compra.setCodigoTalonario(((CxprEstableprovee) dataIngreso.get("factura")).getCXPPROcodestableci() + ((CxprEstableprovee) dataIngreso.get("factura")).getCXPPROcodserie());
    }

    public void busquedaBodega() {
        try {
            HashMap m = new HashMap();
            m.put("genbodSecuencia", this.compra.getCodigoBodega());
            m.put("genofiCodigo", ((CbpgenrOficina) dataIngreso.get("oficina")));
            Object o = this.entity.findObject(CbpgenrBodega.class, m);
            if (o != null) {
                this.dataIngreso.put("bodega", (CbpgenrBodega) o);
            } else {
                bodegasLazy = new CbpgenrBodegaLazy(((CbpgenrOficina) dataIngreso.get("oficina")).getGenofiCodigo());
                JsfUtil.updateexecuteJS("bodegasFrm","PF('wdBodegas').show();");
            }
        } catch (Exception e) {
            Logger.getLogger(IngresoBodegaView.class.getName()).log(Level.SEVERE, null, e);
        }
    }    

    public void actualizarSugerido() {
        try {
            this.sugeridoList = null;
            if (this.dataIngreso.get("oficina") != null) {
                this.sugeridoList = new ComtCabpedsugeridoLazy(((CbpgenrOficina) this.dataIngreso.get("oficina")).getGenofiSecuencia(),"G");
            }
        } catch (Exception e) {
            Logger.getLogger(IngresoBodegaView.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public void onBodegaSelect(SelectEvent event) {
        dataIngreso.put("bodega", (CbpgenrBodega) event.getObject());
        this.compra.setCodigoBodega(((CbpgenrBodega) dataIngreso.get("bodega")).getGenbodSecuencia());
        JsfUtil.executeJS("PF('wdBodegas').hide()");
    }        

    public void sumarValores() {
        BigDecimal subtotal = BigDecimal.ZERO, descuento = BigDecimal.ZERO, impuesto = BigDecimal.ZERO, neto = BigDecimal.ZERO, otroImpuesto = BigDecimal.ZERO;
        for (InvmItem it : items) {
            subtotal = subtotal.add(it.getSubtotalCosto());
            descuento = descuento.add(it.getDescuento());
            impuesto = impuesto.add(it.getValorImpuesto());
            neto = neto.add(it.getValorConImpuesto());
            otroImpuesto = otroImpuesto.add(it.getImpPlastico().add(it.getIce()));
        }
        dataIngreso.put("subtotal", subtotal.setScale(2, BigDecimal.ROUND_HALF_UP));
        dataIngreso.put("descuento", descuento);
        dataIngreso.put("impuesto", impuesto);
        dataIngreso.put("neto", neto);
        dataIngreso.put("total_pagar", neto);
        dataIngreso.put("otros_imp", otroImpuesto);
    }

    public void calcularCantidades(Integer row, String colName) {
        InvmItem data = items.get(row);
        switch (colName){
            case "cantidades":
                data.actualizarCantidades(Utilidades.procesarCantidades(data.getUnidad(), data.getINVITMunidembala(), data.getCajas()));
                data.setSubtotalCosto(data.getCostoItem().multiply(data.getCantidadFuncional()).setScale(4, BigDecimal.ROUND_HALF_UP));
                break;
            case "unidad":
                data.setCostoItem(data.getCostoItem().setScale(4, BigDecimal.ROUND_HALF_UP));
                data.setCostoCaja(data.getCostoItem().multiply(data.getINVITMunidembala()).setScale(4, BigDecimal.ROUND_HALF_UP));
                break;
            case "caja":
                data.setCostoCaja(data.getCostoCaja().setScale(4, BigDecimal.ROUND_HALF_UP));
                data.setCostoItem(data.getCostoCaja().divide(data.getINVITMunidembala(),4,BigDecimal.ROUND_HALF_UP));
                break;
            case "pordescuento":
                data.setDescuento((data.getSubtotalCosto().multiply(data.getPorcDesc()).divide(BigDecimal.valueOf(100.0)).setScale(2, BigDecimal.ROUND_HALF_UP)));
                break;
            case "descuento":
                data.setPorcDesc(data.getDescuento().divide(data.getSubtotalCosto(), 4, BigDecimal.ROUND_HALF_UP).multiply(BigDecimal.valueOf(100.0)).setScale(4, BigDecimal.ROUND_HALF_UP));
                break;
            default:
                break;
        }                
        data.setSubtotalCosto(data.getCostoItem().multiply(data.getCantidadFuncional()).setScale(2, BigDecimal.ROUND_HALF_UP));
        data.setValorImpuesto(this.generalEjb.valorImpuestoItem(data.getInvmItemPK().getINVITMcodigo(), data.getInvmItemPK().getGENCIAcodigo(), data.getSubtotalCosto().subtract(data.getDescuento()), this.compra.getCXPDOC_fechafactura()));
        data.setValorConImpuesto(data.getSubtotalCosto().subtract(data.getDescuento()).add(data.getValorImpuesto()));
        data.setImpPlastico(data.getINVITMimpplastico().equals('S')?this.impPlastico.multiply(data.getCantidadFuncional()):new BigDecimal("0.00"));
        data.setCostoCompra((data.getCantidadFuncional()!=null&&data.getCantidadFuncional().compareTo(BigDecimal.ZERO)>0)?(data.getSubtotalCosto().subtract(data.getDescuento())).divide(data.getCantidadFuncional(), 4, BigDecimal.ROUND_HALF_UP):BigDecimal.ZERO);
        data.setPorcentajeIce((data.getSubtotalCosto().subtract(data.getDescuento())).compareTo(BigDecimal.ZERO)>0?(data.getIce().divide(data.getSubtotalCosto().subtract(data.getDescuento()), 4, BigDecimal.ROUND_HALF_UP).multiply(BigDecimal.valueOf(100.0)).setScale(4, BigDecimal.ROUND_HALF_UP)):BigDecimal.ZERO);
        sumarValores();
    }
    
    public void eliminarItemRegistro(InvmItem i){
        this.items.remove(i);
        this.sumarValores();
    }  

    public void onSugeridoSelect() {
        try {
            this.items = new ArrayList();            
            this.detalleSugerido = entity.findListByParameters("SELECT m FROM ComtDetpedsugerido m WHERE m.comtDetpedsugeridoPK.cOMPDSsecuencia = :cOMPDSsecuencia ", new String[]{"cOMPDSsecuencia"}, new Object[]{sugerido.getCOMPDSsecuencia()});            
            this.cargaSugerido=Boolean.TRUE;
            for (ComtDetpedsugerido d : this.detalleSugerido) {
                this.item=this.entity.find(InvmItem.class, new InvmItemPK(this.sugerido.getGENCIAcodigo(), d.getINVITMcodigo()));
                this.item.setDisponible(d.getCOMPDScantisugeridas());
                this.item.setItemSugerido(Boolean.TRUE);
                this.agregarItem();
            }
            this.cargaSugerido=Boolean.FALSE;
            this.compra.setGENOFI_codigo(this.sugerido.getGENOFIcodigo());
            this.compra.setCXPPRO_codigo(this.sugerido.getCXPPROcodigo());
            this.compra.setCodigoBodega(this.sugerido.getCOMPDScodbodega());
            this.busquedaOficina();
            this.buscarProveedorCampo();            
            this.busquedaBodega();
            this.compra.setCOMCCM_numsecrel(sugerido.getCOMPDSsecuencia());
            this.compra.setCOMCCM_numdocurel(sugerido.getCOMPDSnumero().toString());
            JsfUtil.executeJS("PF('wdSugComp').hide()");
        } catch (Exception e) {
            Logger.getLogger(IngresoBodegaView.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public void busquedaItem() {
        try {
            if (this.item.getCodigoItem() == null && this.item.getINVITMnombre() == null && this.item.getINVITMcodigbarra() == null) {
                JsfUtil.updateexecuteJS("frmdlgItem","PF('dlgItem').show();");
            } else {
                List<InvmItem> i = posService.consultarItem(this.item.getCodigoItem() != null ? this.item.getCodigoItem().trim() : null, this.item.getINVITMnombre() != null ? this.item.getINVITMnombre().trim().toUpperCase() : null, this.item.getINVITMcodigbarra() != null ? this.item.getINVITMcodigbarra().trim() : null);
                if (i == null || i.isEmpty()) {
                    JsfUtil.messageInfo(null, "REGISTRO NO ENCONTRADO", "");
                } else {
                    if (i.size() == 1) {
                        this.item = i.get(0);
                        this.actualizarCodigoItem();
                    } else {
                        JsfUtil.updateexecuteJS("frmdlgItem","PF('dlgItem').show();");
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(IngresoBodegaView.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void registrarItem() {
        try {
            this.itemRegistro.getInvmItemPK().setGENCIAcodigo(this.session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia());
            this.itemRegistro.getInvmItemPK().setINVITMcodigo(this.itemRegistro.getInvmItemPK().getINVITMcodigo().toUpperCase());
            InvmItem it = this.entity.find(InvmItem.class, this.itemRegistro.getInvmItemPK());
            if(it==null && this.itemRegistro.getCantidadFuncional().compareTo(BigDecimal.ZERO)>0 && this.itemRegistro.getINVIN1codin1item()!=null
            && this.itemRegistro.getINVIN2codin2item()!=null && this.itemRegistro.getINVIN3codin3item()!=null && this.itemRegistro.getINVIN4codin4item()!=null){                
                this.item = this.generalEjb.registrarItem(this.itemRegistro, this.session.getCbpsegmUsuario());
                this.item.setCantidadFuncional(this.itemRegistro.getCantidadFuncional());
                this.agregarItem();
                this.itemRegistro = new InvmItem();
                JsfUtil.update("mainForm:contenedor:pnlDatos");
                JsfUtil.updateexecuteJS("mainForm:contenedor:dtProductos","PF('dlgRegistroItem').hide();");
                JsfUtil.messageInfo(null, StringValues.msgRegistroExitoso, "");
            }else{
                JsfUtil.messageWarning(null, "CODIGO EXISTENTE - CANTIDAD MAYOR A CERO - NIVELES", "");
            }
        } catch (Exception e) {
            Logger.getLogger(IngresoBodegaView.class.getName()).log(Level.SEVERE, null, e);
            JsfUtil.messageError(null, StringValues.msgRegistroNoExitoso, "");
        }
    }
    
    public void actualizarCodigoItem(){
        this.item.setCodigoItem(this.item.getInvmItemPK().getINVITMcodigo().trim());
    }
    
    public void limpiarItem(){
        this.item = new InvmItem();
    }

    public void onProductoSelect() {
        this.item = this.entity.find(InvmItem.class, new InvmItemPK(this.itemLazy.getInvmItemPK().getGENCIAcodigo(), this.itemLazy.getInvmItemPK().getINVITMcodigo())); 
        this.item.setCodigoItem(this.item.getInvmItemPK().getINVITMcodigo());
    }
    
    public void agregarItem() {
        if(this.cargaSugerido?true:(this.item!=null && this.item.getInvmItemPK()!=null && this.item.getCantidadFuncional()!=null?(this.item.getCantidadFuncional().compareTo(BigDecimal.ZERO)>0):false)){            
            BigDecimal precioItem = this.comprasEjb.precioItemLista("C", this.item.getInvmItemPK().getINVITMcodigo());
            item.setCostoCaja((precioItem!=null?precioItem:BigDecimal.ZERO).multiply(item.getINVITMunidembala()).setScale(4, BigDecimal.ROUND_HALF_UP));
            item.setCostoItem((precioItem!=null?precioItem:BigDecimal.ZERO).setScale(4, BigDecimal.ROUND_HALF_UP));
            boolean c = this.items.contains(item);
            this.item.setCajas(c?(this.item.getCantidadFuncional().add(this.items.get(this.items.indexOf(item)).getCajas())):this.item.getCantidadFuncional());
            this.item.actualizarCantidades(Utilidades.procesarCantidades(c?(this.item.getUnidad().add(this.items.get(this.items.indexOf(item)).getUnidad())):this.item.getUnidad(), this.item.getINVITMunidembala(), this.item.getCajas()));                        
            if(c){
                item.setItemSugerido(this.items.get(this.items.indexOf(item)).getItemSugerido());
                this.item.setDisponible(this.items.get(this.items.indexOf(item)).getDisponible());
                items.set(this.items.indexOf(item), item);
            }else{
                items.add(item);
            }
            this.calcularCantidades(items.indexOf(item), "agregar");
            this.limpiarItem();
        }else{
            JsfUtil.messageWarning(null, "CANTIDAD MAYOR A CERO", "");
        }
    }
    
    public void consultarResumenFactura(){
        try{
            this.resumen = null;
            if(dataIngreso.get("proveedor")!=null && this.compra.getCOMCCM_numfacprov()!=null && this.compra.getCodigoTalonario()!=null){
                this.resumen = this.comprasEjb.compraPorNumeroFactura(((CxpmProveedor)dataIngreso.get("proveedor")).getCxpmProveedorPK().getGENOFIcodigo(),
                    ((CxpmProveedor)dataIngreso.get("proveedor")).getCxpmProveedorPK().getCXPPROcodigo(), this.compra.getCodigoTalonario()+"-"+this.compra.getCOMCCM_numfacprov(), Boolean.FALSE);
            }
        } catch (Exception e) {
            Logger.getLogger(IngresoBodegaView.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void guardarDatos() {
        try{
            if (this.dataIngreso.get("proveedor") == null || this.compra.getCOMCCM_fecrecepci() == null || this.compra.getCXPDOC_fechafactura() == null 
                    || this.compra.getCOMCCM_descrilarg() == null || this.compra.getGENMON_codigo() == null || this.compra.getCOMCCM_numfacprov() == null 
                    || this.items == null || this.items.isEmpty() || this.items.stream().filter(i-> i.getCantidadFuncional().compareTo(BigDecimal.ZERO)>0).count()==0) {
                JsfUtil.messageInfo(null, "Debe ingresar todos los datos del formulario.", "");
                return;
            }
            if((Boolean)this.dataIngreso.get("conSugerido") && this.compra.getCOMCCM_numsecrel()==null){
                JsfUtil.messageInfo(null, "Seleccione Sugerido de Compra.", "");
                return;
            }
            
            compra.setGENCIA_codigo(session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia());
            compra.setGENOFI_codigo(((CbpgenrOficina)dataIngreso.get("oficina")).getGenofiSecuencia());
            compra.setCXPPRO_codigo(((CxpmProveedor) dataIngreso.get("proveedor")).getCxpmProveedorPK().getCXPPROcodigo());            
            compra.setCOMCCM_descricort(compra.getCOMCCM_descrilarg());
            compra.setCOMCCM_estado("G");            
            compra.setACCION("A");
            compra.setCOMCCM_subtotamov((BigDecimal) dataIngreso.get("subtotal"));
            compra.setCOMCCM_desctomov((BigDecimal) dataIngreso.get("descuento"));
            compra.setCOMCCM_totimpumov((BigDecimal)dataIngreso.get("impuesto"));
            compra.setCOMCCM_netomov((BigDecimal)dataIngreso.get("neto"));
            compra.setSEGUSU_codigo(session.getCbpsegmUsuario().getSegusuLogin());
            compra.setCOMTDO_codigorel("PDS");
            compra.setCOMCCM_numfacprov(this.compra.getCodigoTalonario()+"-"+this.compra.getCOMCCM_numfacprov());
            
            List<IngresoBodegaDetModel> detalle = new ArrayList();
            IngresoBodegaDetModel d;            
            for (InvmItem t : items.stream().filter(i-> i.getCantidadFuncional().compareTo(BigDecimal.ZERO)>0).collect(Collectors.toList())) {
                d = new IngresoBodegaDetModel();
                d.setCOMDCM_canticaja(t.getCajas());
                d.setCOMDCM_cantiunida(t.getUnidad());
                d.setINVITM_codigo(t.getInvmItemPK().getINVITMcodigo());
                d.setCOMDCM_cantifunci(t.getCantidadFuncional());
                d.setCOMDCM_preciomov(t.getSubtotalCosto().subtract(t.getDescuento())); // Subtotal - Descuento
                d.setCOMDCM_unidembala(t.getINVITMunidembala().intValue()); // @COMDCM_unidembala ES INT EN EL SP Y BIGDECIMAL EN EL PRODUCTO
                d.setCOMDCM_peso(t.getINVITMpeso());
                d.setCOMDCM_volumen(t.getINVITMvolumen());
                d.setCOMDCM_porcdescto(t.getPorcDesc());
                d.setCOMDCM_subtotamov(t.getSubtotalCosto());
                d.setCOMDCM_desctomov(t.getDescuento());
                d.setCOMDCM_totimpumov(BigDecimal.ZERO); // NO SE COMO OBTENER ESTE VALOR TODAVIA
                d.setCOMDCM_otrosimpuestos(BigDecimal.ZERO);
                d.setCOMDCM_netomov(d.getCOMDCM_preciomov());
                d.setCXPICA_codclase(1); // NO ESTOY SEGURO DE QUE QUE VA
                d.setCOMCCM_prepostea("S");
                d.setCOMDCM_codimpICE("");
                d.setCOMDCM_valimpICE(null);
                detalle.add(d);
            }
            ResultadoIngresoCompra rc = comprasEjb.guardarIngresoBodega(compra, detalle, dataIngreso, this.session.getCbpsegmUsuario());
            if (rc!=null) {
                this.initView();
                JsfUtil.messageInfo(null, StringValues.msgRegistroExitoso+" COMPRA:"+rc.getCOMCCM_numsecucial()+"-"+rc.getCOMCCM_numdocumen(), "");
            } else {
                JsfUtil.messageError(null, StringValues.msgRegistroNoExitoso, "");
            }
        } catch (Exception e) {
            Logger.getLogger(IngresoBodegaView.class.getName()).log(Level.SEVERE, null, e);
            JsfUtil.messageError(null, StringValues.msgRegistroNoExitoso, "");
        }
    }
    
    public void seleccionarCompra(){
        HashMap parametros = new HashMap();
        parametros.put("comtDetcompraPK.cOMCCMnumsecucial", this.ingBodega.getCOMCCMnumsecucial());
        this.ingBodega.setDetalle(this.entity.findAllByParameter(ComtDetcompra.class, parametros, null));
    }
    
    public void cargarCompra(){
        this.initView();
        if(this.ingBodega!=null){
            this.sugerido = this.entity.find(ComtCabpedsugerido.class, this.ingBodega.getCOMCCMnumsecrel().longValue());
            this.onSugeridoSelect();
            this.compra.cargarDatosCompra(this.ingBodega);
            this.busquedaOficina();
            this.buscarProveedorCampo();            
            this.busquedaBodega();
            for (ComtDetcompra dc : this.ingBodega.getDetalle()) {
                this.item=this.entity.find(InvmItem.class, new InvmItemPK(this.ingBodega.getGENCIAcodigo(), dc.getINVITMcodigo()));
                this.item.setUnidad(dc.getCOMDCMcantiunida());
                this.item.setCantidadFuncional(dc.getCOMDCMcanticaja());
                this.item.setItemSugerido(Boolean.TRUE);
                this.agregarItem();
            }
        }
    }
    
    public List<TipoFiltro> listNivel1(){
        HashMap p = new HashMap();
        p.put("0", this.session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia());
        return this.entity.findAllSQL(QuerysPos.filtroNivel1, p, TipoFiltro.class);
    }
    
    public List<TipoFiltro> listNivel2(){
        HashMap p = new HashMap();
        if(this.itemRegistro.getINVIN1codin1item()!=null){
            p.put("0", this.session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia());
            p.put("1", this.itemRegistro.getINVIN1codin1item());
            return this.entity.findAllSQL(QuerysPos.filtroNivel2ByN1, p, TipoFiltro.class);
        }
        return null;
    }
    
    public List<TipoFiltro> listNivel3(){
        HashMap p = new HashMap();
        if(this.itemRegistro.getINVIN1codin1item()!=null && this.itemRegistro.getINVIN2codin2item()!=null){
            p.put("0", this.session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia());
            p.put("1", this.itemRegistro.getINVIN1codin1item());
            p.put("2", this.itemRegistro.getINVIN2codin2item());
            return this.entity.findAllSQL(QuerysPos.filtroNivel3ByN2, p, TipoFiltro.class);
        }
        return null;
    }
    
    public List<TipoFiltro> listNivel4(){
        HashMap p = new HashMap();
        if(this.itemRegistro.getINVIN1codin1item()!=null && this.itemRegistro.getINVIN2codin2item()!=null && this.itemRegistro.getINVIN3codin3item()!=null){
            p.put("0", this.session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia());
            p.put("1", this.itemRegistro.getINVIN1codin1item());
            p.put("2", this.itemRegistro.getINVIN2codin2item());
            p.put("3", this.itemRegistro.getINVIN3codin3item());
            return this.entity.findAllSQL(QuerysPos.filtroNivel4ByN3, p, TipoFiltro.class);
        }
        return null;
    }

    public HashMap<String, Object> getDataIngreso() {
        return dataIngreso;
    }

    public void setDataIngreso(HashMap<String, Object> dataIngreso) {
        this.dataIngreso = dataIngreso;
    }

    public List<GenrMoneda> getMonedas() {
        return monedas;
    }

    public void setMonedas(List<GenrMoneda> monedas) {
        this.monedas = monedas;
    }

    public List<InvmItem> getItems() {
        return items;
    }

    public void setItems(List<InvmItem> items) {
        this.items = items;
    }

    public List<GenpParamgenerales> getProvincias() {
        return provincias;
    }

    public void setProvincias(List<GenpParamgenerales> provincias) {
        this.provincias = provincias;
    }

    public InvmItemLazy getProductosLazy() {
        return productosLazy;
    }

    public void setProductosLazy(InvmItemLazy productosLazy) {
        this.productosLazy = productosLazy;
    }

    public CbpgenrBodegaLazy getBodegasLazy() {
        return bodegasLazy;
    }

    public void setBodegasLazy(CbpgenrBodegaLazy bodegasLazy) {
        this.bodegasLazy = bodegasLazy;
    }

    public CxprEstableproveeLazy getFacturasLazy() {
        return facturasLazy;
    }

    public void setFacturasLazy(CxprEstableproveeLazy facturasLazy) {
        this.facturasLazy = facturasLazy;
    }

    public ComtCabpedsugeridoLazy getSugeridoList() {
        return sugeridoList;
    }

    public void setSugeridoList(ComtCabpedsugeridoLazy sugeridoList) {
        this.sugeridoList = sugeridoList;
    }

    public ComtCabpedsugerido getSugerido() {
        return sugerido;
    }

    public void setSugerido(ComtCabpedsugerido sugerido) {
        this.sugerido = sugerido;
    }

    public List<ComtDetpedsugerido> getDetalleSugerido() {
        return detalleSugerido;
    }

    public void setDetalleSugerido(List<ComtDetpedsugerido> detalleSugerido) {
        this.detalleSugerido = detalleSugerido;
    }

    public CbpgenrOficinaLazyTest getOficinasLazy() {
        return oficinasLazy;
    }

    public void setOficinasLazy(CbpgenrOficinaLazyTest oficinasLazy) {
        this.oficinasLazy = oficinasLazy;
    }

    public InvmItem getItem() {
        return item;
    }

    public void setItem(InvmItem item) {
        this.item = item;
    }

    public InvmItem getItemLazy() {
        return itemLazy;
    }

    public void setItemLazy(InvmItem itemLazy) {
        this.itemLazy = itemLazy;
    }

    public CxpmProveedorLazy getProveedoresLazy() {
        return proveedoresLazy;
    }

    public void setProveedoresLazy(CxpmProveedorLazy proveedoresLazy) {
        this.proveedoresLazy = proveedoresLazy;
    }

    public InvmItem getItemRegistro() {
        return itemRegistro;
    }

    public void setItemRegistro(InvmItem itemRegistro) {
        this.itemRegistro = itemRegistro;
    }

    public ComtCabcompraLazy getIngBodegaLazy() {
        return ingBodegaLazy;
    }

    public void setIngBodegaLazy(ComtCabcompraLazy ingBodegaLazy) {
        this.ingBodegaLazy = ingBodegaLazy;
    }

    public ComtCabcompra getIngBodega() {
        return ingBodega;
    }

    public void setIngBodega(ComtCabcompra ingBodega) {
        this.ingBodega = ingBodega;
    }

    public IngresoBodegaModel getCompra() {
        return compra;
    }

    public void setCompra(IngresoBodegaModel compra) {
        this.compra = compra;
    }

    public List<TipoFiltro> getResumen() {
        return resumen;
    }

    public void setResumen(List<TipoFiltro> resumen) {
        this.resumen = resumen;
    }
    //656
}
