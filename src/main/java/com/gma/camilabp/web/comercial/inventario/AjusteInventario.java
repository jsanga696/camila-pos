/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.comercial.inventario;

import com.gma.camilabp.domain.CbpgenrBodega;
import com.gma.camilabp.domain.CbpgenrOficina;
import com.gma.camilabp.domain.GenrDocumento;
import com.gma.camilabp.domain.GenrDocumentoPK;
import com.gma.camilabp.domain.InvmItem;
import com.gma.camilabp.domain.InvrMotvajuste;
import com.gma.camilabp.domain.InvrMotvajustePK;
import com.gma.camilabp.model.consultas.TempAjusteTransferenciaInv;
import com.gma.camilabp.model.logica.TransferenciaAjusteInventario;
import com.gma.camilabp.sql.QuerysPos;
import com.gma.camilabp.web.lazy.CbpgenrBodegaLazyTest;
import com.gma.camilabp.web.lazy.CbpgenrOficinaLazyTest;
import com.gma.camilabp.web.lazy.InvmItemLazy;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.service.GeneralLocal;
import com.gma.camilabp.web.service.InventarioLocal;
import com.gma.camilabp.web.service.PuntoVentaLocal;
import com.gma.camilabp.web.session.UserSession;
import com.gma.camilabp.web.util.JsfUtil;
import com.gma.camilabp.web.util.Utilidades;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

/**
 *
 * @author userdb6
 */
@Named(value = "ajusteInventario")
@ViewScoped
public class AjusteInventario implements Serializable{
    @Inject
    private UserSession userSession;
    @EJB
    private EntityManagerLocal entity;
    @EJB
    private PuntoVentaLocal posService;
    @EJB
    private GeneralLocal generalService;
    @EJB
    private InventarioLocal invService;
    
    protected CbpgenrOficinaLazyTest oficinasLazy;
    protected CbpgenrBodegaLazyTest bodegasLazy;
    protected InvmItemLazy itemsLazy;
    protected InvmItem itemLazy;
    protected List<InvrMotvajuste> motivosAjuste;
    protected List<GenrDocumento> tiposMovimiento;
    protected List<InvmItem> itemsTransaccion;
    
    private Map<String, Object> parametros;
    
    protected CbpgenrOficina oficina;
    protected String secuenciaOficina;
    protected Date fechaEmision;
    protected CbpgenrBodega bodega;
    protected String secuenciaBodega;
    protected InvmItem item=new InvmItem();
    protected GenrDocumento tipoMovimiento;
    protected InvrMotvajuste motivoAjuste;
    protected InvmItem itemTransaccion;
    protected BigDecimal numTransaccion;
    protected List<TempAjusteTransferenciaInv> tempAjusteInvList;
    protected TransferenciaAjusteInventario ajusteModel;
    
    private String nombreTabla;
    
    @PostConstruct
    public void init() {
        try{
            this.nombreTabla="X_CBPAJUSINV"+this.userSession.getCbpsegmUsuario().getSegusuLogin()+"PTOVENTA";
            this.tempAjusteInvList = this.invService.getAjusteTransferenciaTemporalList(this.nombreTabla);
            if(this.tempAjusteInvList!=null && !this.tempAjusteInvList.isEmpty()){
                this.parametros= new HashMap<>();
                this.parametros.put("genciaCodigo",this.userSession.getCbpsegmUsuario().getGenciaCodigo().getGenciaCodigo());
                this.parametros.put("genofiSecuencia", (String)this.entity.findObject("SQL", QuerysPos.selectDistinct.replaceAll("column", "GENOFI_codigo").replaceAll("table", nombreTabla), null, null, Boolean.TRUE));
                this.ajusteModel.setOficina(this.entity.findObject(CbpgenrOficina.class, this.parametros));
                this.parametros= new HashMap<>();
                this.parametros.put("genofiCodigo", this.ajusteModel.getOficina());
                this.parametros.put("genbodSecuencia", (String)this.entity.findObject("SQL", QuerysPos.selectDistinct.replaceAll("column", "GENBOD_codigo").replaceAll("table", nombreTabla), null, null, Boolean.TRUE));
                this.ajusteModel.setBodegaOrigen(this.entity.findObject(CbpgenrBodega.class, this.parametros));
                this.parametros.put("genbodSecuencia", (String)this.entity.findObject("SQL", QuerysPos.selectDistinct.replaceAll("column", "GENBODDES_codigo").replaceAll("table", nombreTabla), null, null, Boolean.TRUE));
                this.ajusteModel.setBodegaDestino(this.entity.findObject(CbpgenrBodega.class, this.parametros));
            }
            this.oficinasLazy = new CbpgenrOficinaLazyTest(Utilidades.getIdsOfList(userSession.getOficinas()));
            this.oficina = (CbpgenrOficina)Utilidades.getFirstElement(userSession.getOficinas());
            this.secuenciaOficina = this.oficina.getGenofiSecuencia();
            this.numTransaccion = (this.generalService.secuenciaModulo("INV", this.userSession.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia())).add(BigDecimal.ONE);
            this.fechaEmision = new Date();                 
            this.itemsTransaccion = new ArrayList<>();
            this.cargarTiposMovimiento();
        } catch (Exception e) {
            Logger.getLogger(AjusteInventario.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void cargarTiposMovimiento(){
        try{
            List<String> codigos= new ArrayList<>();
            codigos.add("AJE");
            codigos.add("AJI");
            codigos.add("AJS");
            codigos.add("TRI");
            codigos.add("TRE"); 
            this.parametros= new HashMap<>();
            this.parametros.put("genrDocumentoPK.gENCIAcodigo",userSession.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia());
            this.parametros.put("genrDocumentoPK.gENOFIcodigo",this.oficina.getGenofiSecuencia());
            this.parametros.put("genrDocumentoPK.gENMODcodigo", "INV");
            this.parametros.put("gENTDOestado", 'A');            
            this.parametros.put("genrDocumentoPK.gENTDOcodigo", codigos);
            this.tiposMovimiento = this.entity.findAllByParameter(GenrDocumento.class, parametros,null);
            this.tipoMovimiento = (GenrDocumento) Utilidades.getFirstElement(this.tiposMovimiento);
            this.seleccionTipoMovimiento();
        } catch (Exception e) {
            Logger.getLogger(AjusteInventario.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void seleccionTipoMovimiento(){
        try{
            this.parametros= new HashMap<>();
            this.parametros.put("genrDocumentoPK.gENCIAcodigo", userSession.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia());
            this.parametros.put("genrDocumentoPK.gENOFIcodigo", this.oficina.getGenofiSecuencia());
            this.parametros.put("genrDocumentoPK.gENMODcodigo", "INV");
            this.parametros.put("genrDocumentoPK.gENTDOcodigo", this.tipoMovimiento.getGenrDocumentoPK().getGENTDOcodigo());
            this.tipoMovimiento = this.entity.findObject(GenrDocumento.class, this.parametros);
            parametros= new HashMap<>();
            parametros.put("invrMotvajustePK.gENCIAcodigo",userSession.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia());
            parametros.put("iNVMAJtipouso", 'E');
            parametros.put("iNVMAJtipo", this.tipoMovimiento.getGENTDOtipo());
            this.motivosAjuste = this.entity.findAllByParameter(InvrMotvajuste.class, parametros,null);
            this.motivoAjuste = (InvrMotvajuste) Utilidades.getFirstElement(this.motivosAjuste);
        } catch (Exception e) {
            Logger.getLogger(AjusteInventario.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void seleccionMotivoAjuste(){
        this.parametros= new HashMap<>();
        this.parametros.put("invrMotvajustePK.gENCIAcodigo", userSession.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia());
        this.parametros.put("invrMotvajustePK.iNVMAJcodigo", this.motivoAjuste.getInvrMotvajustePK().getINVMAJcodigo());
        this.motivoAjuste = this.entity.findObject(InvrMotvajuste.class, this.parametros);
    }
    
    public void busquedaOficina(){
        try{
            this.oficina = this.generalService.consultarOficina(this.secuenciaOficina);
            if(this.oficina!=null && this.oficina.getGenofiCodigo()!=null){
                this.cargarTiposMovimiento();
            }else{
                this.tiposMovimiento = null;
                this.tipoMovimiento = new GenrDocumento( new GenrDocumentoPK());
                this.motivosAjuste = null;
                this.motivoAjuste = new InvrMotvajuste( new InvrMotvajustePK());
            }
        } catch (Exception e) {
            Logger.getLogger(AjusteInventario.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void seleccionOficina(){
        this.secuenciaOficina=this.oficina.getGenofiSecuencia();
        this.cargarTiposMovimiento();
    }
    
    public void busquedaBodega(){
        try{
            System.out.println("1");
            if(this.secuenciaOficina!=null && this.secuenciaOficina.trim().length()>0){
                System.out.println("2");
                if(this.secuenciaBodega!=null && this.secuenciaBodega.trim().length()>0){
                    System.out.println("4");
                    parametros= new HashMap<>();
                    parametros.put("genbodSecuencia", this.secuenciaBodega);
                    parametros.put("genofiCodigo", this.oficina);
                    this.bodega = this.entity.findObject(CbpgenrBodega.class, parametros);
                    if(this.bodega==null)
                        this.bodega = new CbpgenrBodega();
                }else{
                    this.bodega = new CbpgenrBodega();
                }
                System.out.println("5");
                System.out.println(this.bodega.getGenbodSecuencia());
                if(this.bodega==null || this.bodega.getGenbodSecuencia()==null || this.bodega.getGenbodSecuencia().length()==0){
                    this.bodegasLazy = new CbpgenrBodegaLazyTest(this.oficina.getGenofiCodigo(), this.oficina.getGenciaCodigo());
                    JsfUtil.update("frmdlgBodega");
                    JsfUtil.executeJS("PF('dlgBodega').show();");
                }
            }else{
                JsfUtil.messageWarning(null, "REALICE LA BUSQUEDA DE OFICINA", "");
            }     
        } catch (Exception e) {
            Logger.getLogger(AjusteInventario.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void seleccionBodega(){
        this.secuenciaBodega=this.bodega.getGenbodSecuencia();
    }
    
    public void busquedaItem(){
        System.out.println("BUSQUEDA ITEM");
        try{
            if(this.item.getCodigoItem()==null && this.item.getINVITMnombre()==null&& this.item.getINVITMcodigbarra()==null){
                itemsLazy = new InvmItemLazy('T','A','S',null,null);
                JsfUtil.update("frmdlgProducto");
                JsfUtil.executeJS("PF('dlgProducto').show();");
            }else{
                List<InvmItem> i= posService.consultarItem(this.item.getCodigoItem()!=null?this.item.getCodigoItem().trim():null, this.item.getINVITMnombre()!=null?this.item.getINVITMnombre().trim().toUpperCase():null,this.item.getINVITMcodigbarra()!=null?this.item.getINVITMcodigbarra().trim():null);
                if(i==null || i.isEmpty()){
                    JsfUtil.messageInfo(null, "REGISTRO NO ENCONTRADO", "");
                }else{
                    if(i.size()==1){
                        this.item= i.get(0);
                        this.actualizarCodigoItem();
                    }else{
                        this.itemsLazy = new InvmItemLazy('T','A','S',this.item.getCodigoItem()!=null?this.item.getCodigoItem().trim():null,this.item.getINVITMnombre()!=null?this.item.getINVITMnombre().trim().toUpperCase():null);
                        JsfUtil.update("frmdlgProducto");
                        JsfUtil.executeJS("PF('dlgProducto').show();");
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(AjusteInventario.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void actualizarCodigoItem(){
        try{
            if(this.itemLazy!=null)
                this.item=this.itemLazy;
            this.itemLazy=null;
            this.item.setCodigoItem(this.item.getInvmItemPK().getINVITMcodigo());
        } catch (Exception e) {
            Logger.getLogger(AjusteInventario.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void limpiarItem(){
        this.item= new InvmItem();
    }
    
    public void agregarItem(){
        try{
            if(this.item!=null && this.item.getInvmItemPK()!=null){
                if(this.item.getCantidadFuncional()!=null && this.item.getCantidadFuncional().compareTo(BigDecimal.ZERO)>0){
                    if(this.itemsTransaccion.contains(this.item)){
                        this.itemsTransaccion.stream().filter((invmItem) -> (invmItem.equals(this.item))).forEachOrdered((invmItem) -> {
                            InvmItem i = this.entity.find(InvmItem.class, invmItem.getInvmItemPK());
                            i.setCantidadFuncional(this.item.getCantidadFuncional().add(invmItem.getCantidadFuncional()));
                            this.itemsTransaccion.set(this.itemsTransaccion.indexOf(invmItem), i);
                        });
                    }else{
                        this.itemsTransaccion.add(item);
                    }
                }else{
                    JsfUtil.messageWarning(null, "CANTIDAD MAYOR A CERO", "");
                }
            }else{
                JsfUtil.messageWarning(null, "REALIZAR BUSQUEDA", "");
            }
        } catch (Exception e) {
            Logger.getLogger(AjusteInventario.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void elimiarItemRegistro(InvmItem i){
        try{
            this.itemsTransaccion.remove(i);
        } catch (Exception e) {
            Logger.getLogger(AjusteInventario.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public CbpgenrOficinaLazyTest getOficinasLazy() {
        return oficinasLazy;
    }

    public void setOficinasLazy(CbpgenrOficinaLazyTest oficinasLazy) {
        this.oficinasLazy = oficinasLazy;
    }    

    public CbpgenrBodegaLazyTest getBodegasLazy() {
        return bodegasLazy;
    }

    public void setBodegasLazy(CbpgenrBodegaLazyTest bodegasLazy) {
        this.bodegasLazy = bodegasLazy;
    }

    public CbpgenrOficina getOficina() {
        return oficina;
    }

    public void setOficina(CbpgenrOficina oficina) {
        this.oficina = oficina;
    }

    public String getSecuenciaOficina() {
        return secuenciaOficina;
    }

    public void setSecuenciaOficina(String secuenciaOficina) {
        this.secuenciaOficina = secuenciaOficina;
    }

    public Date getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(Date fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public CbpgenrBodega getBodega() {
        return bodega;
    }

    public void setBodega(CbpgenrBodega bodega) {
        this.bodega = bodega;
    }

    public String getSecuenciaBodega() {
        return secuenciaBodega;
    }

    public void setSecuenciaBodega(String secuenciaBodega) {
        this.secuenciaBodega = secuenciaBodega;
    }

    public InvmItem getItem() {
        return item;
    }

    public void setItem(InvmItem item) {
        this.item = item;
    }

    public List<InvrMotvajuste> getMotivosAjuste() {
        return motivosAjuste;
    }

    public void setMotivosAjuste(List<InvrMotvajuste> motivosAjuste) {
        this.motivosAjuste = motivosAjuste;
    }

    public List<GenrDocumento> getTiposMovimiento() {
        return tiposMovimiento;
    }

    public void setTiposMovimiento(List<GenrDocumento> tiposMovimiento) {
        this.tiposMovimiento = tiposMovimiento;
    }

    public GenrDocumento getTipoMovimiento() {
        return tipoMovimiento;
    }

    public void setTipoMovimiento(GenrDocumento tipoMovimiento) {
        this.tipoMovimiento = tipoMovimiento;
    }
    

    public InvrMotvajuste getMotivoAjuste() {
        return motivoAjuste;
    }

    public void setMotivoAjuste(InvrMotvajuste motivoAjuste) {
        this.motivoAjuste = motivoAjuste;
    }

    public InvmItemLazy getItemsLazy() {
        return itemsLazy;
    }

    public void setItemsLazy(InvmItemLazy itemsLazy) {
        this.itemsLazy = itemsLazy;
    }

    public InvmItem getItemLazy() {
        return itemLazy;
    }

    public void setItemLazy(InvmItem itemLazy) {
        this.itemLazy = itemLazy;
    }

    public List<InvmItem> getItemsTransaccion() {
        return itemsTransaccion;
    }

    public void setItemsTransaccion(List<InvmItem> itemsTransaccion) {
        this.itemsTransaccion = itemsTransaccion;
    }

    public InvmItem getItemTransaccion() {
        return itemTransaccion;
    }

    public void setItemTransaccion(InvmItem itemTransaccion) {
        this.itemTransaccion = itemTransaccion;
    }

    public BigDecimal getNumTransaccion() {
        return numTransaccion;
    }

    public void setNumTransaccion(BigDecimal numTransaccion) {
        this.numTransaccion = numTransaccion;
    }
    
}
