/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.comercial.pos;

import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.service.PuntoVentaLocal;
import com.gma.camilabp.web.session.UserSession;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

/**
 *
 * @author userdb6
 */
@Named(value = "cierreCaja")
@ViewScoped
public class CierreCaja implements Serializable{
    @EJB
    private PuntoVentaLocal posService;
    @EJB
    private EntityManagerLocal entity;
    @Inject
    private UserSession userSession;
    
    @PostConstruct
    public void init() {
        try{
            
        } catch (Exception e) {
            Logger.getLogger(CierreCaja.class.getName()).log(Level.SEVERE, null, e);
        }
    }
}
