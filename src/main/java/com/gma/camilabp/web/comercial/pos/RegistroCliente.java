/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.comercial.pos;

import com.gma.camilabo.enums.ActionsTransaccion;
import com.gma.camilabp.domain.CxcmCliente;
import com.gma.camilabp.domain.CxcmClientePK;
import com.gma.camilabp.web.lazy.CxcmClienteLazy;
import com.gma.camilabp.web.service.BitacoraLocal;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.service.PuntoVentaLocal;
import com.gma.camilabp.web.session.UserSession;
import com.gma.camilabp.web.util.JsfUtil;
import com.gma.camilabp.web.util.StringValues;
import com.gma.camilabp.web.util.Utilidades;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

/**
 *
 * @author userdb6
 */
@Named(value = "registroCliente")
@ViewScoped
public class RegistroCliente implements Serializable{
    @Inject
    private UserSession userSession;
    @EJB
    private PuntoVentaLocal posService;
    @EJB
    private EntityManagerLocal entity;
    @EJB
    private BitacoraLocal bitacora;
    
    protected Boolean generacionAutomatica= Boolean.TRUE;
    protected CxcmCliente cliente;
    protected CxcmCliente clienteSeleccionado;
    protected CxcmClienteLazy clientesLazy;
    private Map<String, Object> parametros;
    
    @PostConstruct
    public void init() {
        try{
            this.generacionAutomatica = posService.generacionCodigoCliente(userSession.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia());
            cliente= new CxcmCliente();
            cliente.setCxcmClientePK(new CxcmClientePK());
            clientesLazy = new CxcmClienteLazy(userSession.getCaja().getGENCIAcodigo(), userSession.getCaja().getGENOFIcodigo(),null,null,null);            
        } catch (Exception e) {
            Logger.getLogger(RegistroCliente.class.getName()).log(Level.SEVERE, null, e);
        }
    }
        
    public Boolean consultarCliente(CxcmCliente clienteConsulta){
        Boolean existe=Boolean.FALSE;
        try{
            List<CxcmCliente> c = posService.consultarCliente(null, clienteConsulta.getCXCCLIrucci(),null);
            existe=(c!=null && !c.isEmpty());
        } catch (Exception e) {
            Logger.getLogger(RegistroCliente.class.getName()).log(Level.SEVERE, null, e);
        }
        return existe;
    }
    
    public void grabarCliente(){
        try{                                    
            if(validarFormularioCliente(this.cliente)){
                String resultado = posService.grabarCliente(cliente, userSession.getCaja(), this.generacionAutomatica);
                if(resultado!=null){
                    JsfUtil.messageInfo(null, StringValues.msgRegistroExitoso+" CODIGO: "+resultado, "");
                    clientesLazy = new CxcmClienteLazy();
                    if(userSession.getRegistrarCliente()){
                        userSession.setCodigoCliente(resultado);
                        JsfUtil.redirectFaces("/faces/pages/pv/registroVenta.xhtml");
                    }
                    bitacora.saveActionLog(ActionsTransaccion.REGISTRO_CLIENTE, resultado);
                }else{
                    JsfUtil.messageError(null, StringValues.msgRegistroNoExitoso, "");
                }
            }            
        } catch (Exception e) {
            Logger.getLogger(RegistroCliente.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void limpiarCliente(){
        try{
            cliente= new CxcmCliente();
        } catch (Exception e) {
            Logger.getLogger(RegistroCliente.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void modificarCliente(){
        try{
            if(validarFormularioCliente(this.clienteSeleccionado)){
                String resultado = posService.grabarCliente(this.clienteSeleccionado, userSession.getCaja(), this.generacionAutomatica);
                if(resultado!=null){
                    JsfUtil.messageInfo(null, StringValues.msgRegistroExitoso+" CODIGO: "+resultado, "");
                    clientesLazy = new CxcmClienteLazy();
                    bitacora.saveActionLog(ActionsTransaccion.MODIFICACION_CLIENTE, resultado);
                }else{
                    JsfUtil.messageError(null, StringValues.msgRegistroNoExitoso, "");
                }
            }else{
                JsfUtil.messageError(null, "REGISTRO NO EXITOSO. VERIFICAR DATOS", "");
            }
        } catch (Exception e) {
            Logger.getLogger(RegistroCliente.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public boolean validarFormularioCliente(CxcmCliente c){
        try{
            Boolean modificacion = c.getCxcmClientePK()!=null && c.getCxcmClientePK().getCXCCLIcodigo()!=null;
            if(!modificacion){
                if(!this.generacionAutomatica){
                    if(c.getCodigoCliente()!=null && !consultarCliente(c)
                        && "1".equalsIgnoreCase(posService.validarIdentificacion(c.getCXCCLItipo().toString(), c.getCXCCLIrucci()))
                        && !posService.existeCodigoCLiente(userSession.getCaja().getGENCIAcodigo(), userSession.getCaja().getGENOFIcodigo(), c.getCodigoCliente())
                            ){
                        return true;
                    }else if(!"1".equalsIgnoreCase(posService.validarIdentificacion(c.getCXCCLItipo().toString(), c.getCXCCLIrucci()))){
                        JsfUtil.messageWarning(null, "IDENTIFICACION NO VALIDA", "");
                    }else if(consultarCliente(c)){
                        JsfUtil.messageWarning(null, "CLIENTE CON IDENTIFICACION REGISTRADO", "");
                    }else if(posService.existeCodigoCLiente(userSession.getCaja().getGENCIAcodigo(), userSession.getCaja().getGENOFIcodigo(), c.getCodigoCliente())){
                        JsfUtil.messageWarning(null, "CODIGO NO VALIDO", "");
                    }
                    return false;
                }
            }
            
            if(modificacion?true:!consultarCliente(c) &&
                "1".equalsIgnoreCase(posService.validarIdentificacion(c.getCXCCLItipo().toString(), c.getCXCCLIrucci())) &&
                (c.getCXCCLIemail()!=null?Utilidades.validarEmailConExpresion(c.getCXCCLIemail()):true)
            ){
                return true;
            }else if (modificacion?false:consultarCliente(c)){
                JsfUtil.messageWarning(null, "CLIENTE CON IDENTIFICACION REGISTRADO", "");
            }else if(!"1".equalsIgnoreCase(posService.validarIdentificacion(c.getCXCCLItipo().toString(), c.getCXCCLIrucci()))){
                JsfUtil.messageWarning(null, "IDENTIFICACION NO VALIDA", "");
            }else if(c.getCXCCLIemail()!=null?!Utilidades.validarEmailConExpresion(c.getCXCCLIemail()):false){
                JsfUtil.messageWarning(null, "CORREO NO VALIDO", "");
            }
        } catch (Exception e) {
            Logger.getLogger(RegistroCliente.class.getName()).log(Level.SEVERE, null, e);
        }
        return false;
    }

    public CxcmCliente getCliente() {
        return cliente;
    }

    public void setCliente(CxcmCliente cliente) {
        this.cliente = cliente;
    }

    public CxcmClienteLazy getClientesLazy() {
        return clientesLazy;
    }

    public void setClientesLazy(CxcmClienteLazy clientesLazy) {
        this.clientesLazy = clientesLazy;
    }

    public CxcmCliente getClienteSeleccionado() {
        return clienteSeleccionado;
    }

    public void setClienteSeleccionado(CxcmCliente clienteSeleccionado) {
        this.clienteSeleccionado = clienteSeleccionado;
    }

    public Boolean getGeneracionAutomatica() {
        return generacionAutomatica;
    }

    public void setGeneracionAutomatica(Boolean generacionAutomatica) {
        this.generacionAutomatica = generacionAutomatica;
    }
    
}
