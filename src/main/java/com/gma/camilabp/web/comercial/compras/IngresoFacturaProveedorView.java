/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.comercial.compras;

import com.gma.camilabp.domain.CbpgenrBodega;
import com.gma.camilabp.domain.CbpgenrOficina;
import com.gma.camilabp.domain.ComtCabcompra;
import com.gma.camilabp.domain.ComtCabpedsugerido;
import com.gma.camilabp.domain.ComtDetcompra;
import com.gma.camilabp.domain.ComtDetpedsugerido;
import com.gma.camilabp.domain.CxpmProveedor;
import com.gma.camilabp.domain.CxpmProveedorPK;
import com.gma.camilabp.domain.CxppPlanpago;
import com.gma.camilabp.domain.CxprEstableprovee;
import com.gma.camilabp.domain.CxprRetenciones;
import com.gma.camilabp.domain.GenpParamgenerales;
import com.gma.camilabp.domain.GenpTalonarios;
import com.gma.camilabp.domain.GenrMoneda;
import com.gma.camilabp.domain.InvmItem;
import com.gma.camilabp.domain.InvmItemPK;
import com.gma.camilabp.domain.MfrmPagoSRI;
import com.gma.camilabp.domain.Rcredtri;
import com.gma.camilabp.domain.Rtipcomp;
import com.gma.camilabp.model.consultas.TipoFiltro;
import com.gma.camilabp.sql.QueryPosCxc;
import com.gma.camilabp.web.lazy.CbpgenrOficinaLazyTest;
import com.gma.camilabp.web.lazy.CxpmProveedorLazy;
import com.gma.camilabp.web.lazy.CxprEstableproveeLazy;
import com.gma.camilabp.web.lazy.InvmItemLazy;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.service.GeneralLocal;
import com.gma.camilabp.web.service.IngresoCompraLocal;
import com.gma.camilabp.web.session.UserSession;
import com.gma.camilabp.web.util.JsfUtil;
import com.gma.camilabp.web.util.Utilidades;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author userdb5
 */
@Named(value = "ingresoFacturaProveedorView")
@ViewScoped
public class IngresoFacturaProveedorView implements Serializable{
    
    @Inject
    private UserSession session;
    @EJB
    private EntityManagerLocal entity;
    @EJB
    private GeneralLocal generalEjb;
    @EJB
    private IngresoCompraLocal comprasEjb;
    
    private HashMap dataIngreso;
    private CbpgenrOficinaLazyTest oficinasLazy;
    private HashMap parametros;
    private List<GenrMoneda> monedas;
    private String numFactura;
    private List<GenpParamgenerales> provincias;
    protected CxpmProveedorLazy proveedoresLazy;
    private InvmItemLazy productosLazy;
    private CxprEstableproveeLazy facturasLazy;
    private ComtCabpedsugerido sugerido;
    private ComtCabcompra compra;
    private List<ComtCabpedsugerido> sugeridoList;
    private List<ComtDetpedsugerido> detalleSugerido;
    private List<ComtCabcompra> compraList;
    private List<ComtDetcompra> detalleCompra;
    private List<TipoFiltro> detalles;
    private List<CxppPlanpago> planesPago;
    private List<MfrmPagoSRI> formasPago;
    private List<CxprRetenciones> retencionesFuente;
    private List<CxprRetenciones> retencionesIva;
    private List<Rtipcomp> documentos;
    private List<Rcredtri> sustentos;
    private List<GenpTalonarios> talonarios;
    private List<InvmItem> items;
    
    
    @PostConstruct
    public void initView(){
        this.parametros = new HashMap();
        this.parametros.put("cxppPlanpagoPK.gENCIAcodigo", this.session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia());
        this.parametros.put("cXPPLPestado", 'A');
        this.planesPago = this.entity.findAllByParameter(CxppPlanpago.class, this.parametros,null);
        this.formasPago = this.entity.findAll(MfrmPagoSRI.class);
        this.parametros = new HashMap();
        this.parametros.put("cxprRetencionesPK.gENCIAcodigo", this.session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia());
        this.parametros.put("cXPRETestado", 'A');
        this.parametros.put("cXPRETtipo", 'F');
        this.retencionesFuente = this.entity.findAllByParameter(CxprRetenciones.class, this.parametros,null);
        this.parametros.put("cXPRETtipo", 'I');
        this.retencionesIva = this.entity.findAllByParameter(CxprRetenciones.class, this.parametros,null);
        this.documentos= this.entity.findAll(Rtipcomp.class);
        this.sustentos= this.entity.findAll(Rcredtri.class);
        dataIngreso = new HashMap();
        this.oficinasLazy = new CbpgenrOficinaLazyTest(Utilidades.getIdsOfList(this.session.getOficinas()));
        dataIngreso.put("oficina", (CbpgenrOficina)Utilidades.getFirstElement(this.session.getOficinas()));
        dataIngreso.put("proveedor", new CxpmProveedor());
        dataIngreso.put("valOfi", ((CbpgenrOficina)dataIngreso.get("oficina")).getGenofiSecuencia());
        dataIngreso.put("valProv", "");
        dataIngreso.put("valEst", "");
        dataIngreso.put("moneda", "");
        dataIngreso.put("observacion", "");
        dataIngreso.put("provincia", "");
        dataIngreso.put("proveedor", new CxpmProveedor());
        dataIngreso.put("fEmision", new Date());
        monedas = entity.findAll(GenrMoneda.class);
        provincias = generalEjb.obtenerParametrosGeneralesPorCodigo("023");
        productosLazy = new InvmItemLazy();
        facturasLazy = new CxprEstableproveeLazy();
        llenarDatos();
        this.detalles = new ArrayList<>();
    }        
    
    public void llenarDatos(){
        this.proveedoresLazy = new CxpmProveedorLazy(((CbpgenrOficina)dataIngreso.get("oficina")).getGenofiSecuencia(), this.session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia());
        this.parametros = new HashMap();
        this.parametros.put("genofiCodigo", ((CbpgenrOficina)dataIngreso.get("oficina")).getGenofiSecuencia());
        this.parametros.put("gENTALtipotalon", "RL");
        this.parametros.put("gENTALestado", "A");
        this.talonarios = this.entity.findAllByParameter(GenpTalonarios.class, this.parametros, null);
    }
    
    public void selectOficina(SelectEvent event){
        dataIngreso.put("valOfi", ((CbpgenrOficina)dataIngreso.get("oficina")).getGenofiSecuencia());
        JsfUtil.update("mainForm:secuenciaOficina");
        llenarDatos();
    }
    
    public void selectProveedor(){
        dataIngreso.put("valProv", ((CxpmProveedor)dataIngreso.get("proveedor")).getCxpmProveedorPK().getCXPPROcodigo());
        
    }
    
    public void actualizarOrden(){
        try{
            if(dataIngreso.get("proveedor")!=null && dataIngreso.get("nFactura")!=null && this.numFactura!=null){
                this.parametros = new HashMap();
                this.items = new ArrayList<>();
                this.detalles = this.comprasEjb.compraPorNumeroFactura(((CxpmProveedor)dataIngreso.get("proveedor")).getCxpmProveedorPK().getGENOFIcodigo(), ((CxpmProveedor)dataIngreso.get("proveedor")).getCxpmProveedorPK().getCXPPROcodigo(), this.numFactura+"-"+dataIngreso.get("nFactura"), Boolean.TRUE);
                if(this.detalles!=null){
                    for (TipoFiltro d : this.detalles) {
                        InvmItem i = this.entity.find(InvmItem.class, new InvmItemPK(this.session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia(), d.getCodigo()));
                        i.setDisponible(d.getCajasugerido());
                        i.setCajas(d.getCajacompra());
                        i.setUnidad(d.getUnidadcompra());
                        i.actualizarCantidades(Utilidades.procesarCantidades(i.getUnidad(), i.getINVITMunidembala(), i.getCajas()));
                        BigDecimal precioItem = this.comprasEjb.precioItemLista("C", i.getInvmItemPK().getINVITMcodigo());
                        i.setCostoCaja((precioItem!=null?precioItem:BigDecimal.ZERO).multiply(i.getINVITMunidembala()).setScale(4, BigDecimal.ROUND_HALF_UP));
                        i.setCostoItem((precioItem!=null?precioItem:BigDecimal.ZERO).setScale(4, BigDecimal.ROUND_HALF_UP));
                        this.items.add(i);
                    }
                }
                this.parametros.put("secuenciaOficina", ((CxpmProveedor)dataIngreso.get("proveedor")).getCxpmProveedorPK().getGENOFIcodigo());
                this.parametros.put("codigoProveedor", ((CxpmProveedor)dataIngreso.get("proveedor")).getCxpmProveedorPK().getCXPPROcodigo());
                this.parametros.put("numFactura", this.numFactura+"-"+dataIngreso.get("nFactura"));                
                this.compraList =this.entity.findAll(QueryPosCxc.compraPorNumeroFactura, this.parametros);
                this.sugeridoList = this.entity.findAll(QueryPosCxc.sugeridoPorNumeroFactura, this.parametros);
            }else{
                JsfUtil.messageWarning(null, "Ingrese Proveedor/Factura", "");
            }
        } catch (Exception e) {
            Logger.getLogger(IngresoFacturaProveedorView.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void calcularCantidades(Integer row, String colName) {
        
    }
    
    public void seleccionarSugerido(){
        this.parametros = new HashMap();
        this.parametros.put("comtDetpedsugeridoPK.cOMPDSsecuencia", this.sugerido.getCOMPDSsecuencia());
        this.detalleSugerido = this.entity.findAllByParameter(ComtDetpedsugerido.class, this.parametros, null);
    }
    
    public void seleccionarCompra(){
        this.parametros = new HashMap();
        this.parametros.put("comtDetcompraPK.cOMCCMnumsecucial", this.compra.getCOMCCMnumsecucial());
        this.detalleCompra = this.entity.findAllByParameter(ComtDetcompra.class, this.parametros, null);
    }

    public void busquedaOficina(){
        try{
            Object o = this.generalEjb.consultarOficina((String)this.dataIngreso.get("valOfi"));
            if(o != null){
                this.dataIngreso.put("oficina", (CbpgenrOficina)o);
            }else{
                JsfUtil.update("frmdlgOficina");
                JsfUtil.executeJS("PF('dlgOficina').show();");
            }
        } catch (Exception e) {
            Logger.getLogger(IngresoFacturaProveedorView.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void onBodegaSelect(SelectEvent event){
        dataIngreso.put("bodega", (CbpgenrBodega)event.getObject());
        JsfUtil.executeJS("PF('wdBodegas').hide()");
    }
    
    public void busquedaProveedor(){
        CxpmProveedorPK pk = new CxpmProveedorPK(session.getCaja().getGENCIAcodigo(), ((CbpgenrOficina)dataIngreso.get("oficina")).getGenofiSecuencia(), (String)dataIngreso.get("valProv"));
        CxpmProveedor cp = entity.find(CxpmProveedor.class, pk);
        if(cp != null){
            dataIngreso.put("valProv", cp.getCxpmProveedorPK().getCXPPROcodigo());
            dataIngreso.put("proveedor", cp);
        }else{
            JsfUtil.update("proveedoresFrm");
            JsfUtil.executeJS("PF('wdProveedores').show();");
        }
    }
    
    public void validarFactura() {
        if (dataIngreso.get("proveedor") == null) {
            JsfUtil.messageInfo(null, "Debe seleccionar previamente el proveedor.", "");
            return;
        }
        facturasLazy = new CxprEstableproveeLazy((CxpmProveedor) dataIngreso.get("proveedor"));
        JsfUtil.update("idDlgFactura");
        JsfUtil.executeJS("PF('wdFactura').show()");
    }
    
    public void setFactura() {
        this.numFactura = ((CxprEstableprovee) dataIngreso.get("factura")).getCXPPROcodestableci() + ((CxprEstableprovee) dataIngreso.get("factura")).getCXPPROcodserie();
    }
    
    public void registrarFactura(){
        try{
            System.out.println(this.dataIngreso.get("codPlan"));
            System.out.println(this.dataIngreso.get("forPago"));
            System.out.println(this.dataIngreso.get("retIva"));
            System.out.println(this.dataIngreso.get("retFuente"));
        } catch (Exception e) {
            Logger.getLogger(IngresoFacturaProveedorView.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public HashMap getDataIngreso() {
        return dataIngreso;
    }

    public void setDataIngreso(HashMap dataIngreso) {
        this.dataIngreso = dataIngreso;
    }

    public String getNumFactura() {
        return numFactura;
    }

    public void setNumFactura(String numFactura) {
        this.numFactura = numFactura;
    }

    public CbpgenrOficinaLazyTest getOficinasLazy() {
        return oficinasLazy;
    }

    public void setOficinasLazy(CbpgenrOficinaLazyTest oficinasLazy) {
        this.oficinasLazy = oficinasLazy;
    }

    public List<GenrMoneda> getMonedas() {
        return monedas;
    }

    public void setMonedas(List<GenrMoneda> monedas) {
        this.monedas = monedas;
    }

    public List<GenpParamgenerales> getProvincias() {
        return provincias;
    }

    public void setProvincias(List<GenpParamgenerales> provincias) {
        this.provincias = provincias;
    }

    public CxpmProveedorLazy getProveedoresLazy() {
        return proveedoresLazy;
    }

    public void setProveedoresLazy(CxpmProveedorLazy proveedoresLazy) {
        this.proveedoresLazy = proveedoresLazy;
    }


    public InvmItemLazy getProductosLazy() {
        return productosLazy;
    }

    public void setProductosLazy(InvmItemLazy productosLazy) {
        this.productosLazy = productosLazy;
    }

    public CxprEstableproveeLazy getFacturasLazy() {
        return facturasLazy;
    }

    public void setFacturasLazy(CxprEstableproveeLazy facturasLazy) {
        this.facturasLazy = facturasLazy;
    }

    public ComtCabpedsugerido getSugerido() {
        return sugerido;
    }

    public void setSugerido(ComtCabpedsugerido sugerido) {
        this.sugerido = sugerido;
    }

    public List<ComtDetpedsugerido> getDetalleSugerido() {
        return detalleSugerido;
    }

    public void setDetalleSugerido(List<ComtDetpedsugerido> detalleSugerido) {
        this.detalleSugerido = detalleSugerido;
    }

    public List<ComtCabpedsugerido> getSugeridoList() {
        return sugeridoList;
    }

    public void setSugeridoList(List<ComtCabpedsugerido> sugeridoList) {
        this.sugeridoList = sugeridoList;
    }

    public List<TipoFiltro> getDetalles() {
        return detalles;
    }

    public void setDetalles(List<TipoFiltro> detalles) {
        this.detalles = detalles;
    }

    public List<ComtCabcompra> getCompraList() {
        return compraList;
    }

    public void setCompraList(List<ComtCabcompra> compraList) {
        this.compraList = compraList;
    }

    public List<ComtDetcompra> getDetalleCompra() {
        return detalleCompra;
    }

    public void setDetalleCompra(List<ComtDetcompra> detalleCompra) {
        this.detalleCompra = detalleCompra;
    }

    public ComtCabcompra getCompra() {
        return compra;
    }

    public void setCompra(ComtCabcompra compra) {
        this.compra = compra;
    }

    public List<CxppPlanpago> getPlanesPago() {
        return planesPago;
    }

    public void setPlanesPago(List<CxppPlanpago> planesPago) {
        this.planesPago = planesPago;
    }

    public List<MfrmPagoSRI> getFormasPago() {
        return formasPago;
    }

    public void setFormasPago(List<MfrmPagoSRI> formasPago) {
        this.formasPago = formasPago;
    }

    public List<CxprRetenciones> getRetencionesFuente() {
        return retencionesFuente;
    }

    public void setRetencionesFuente(List<CxprRetenciones> retencionesFuente) {
        this.retencionesFuente = retencionesFuente;
    }

    public List<CxprRetenciones> getRetencionesIva() {
        return retencionesIva;
    }

    public void setRetencionesIva(List<CxprRetenciones> retencionesIva) {
        this.retencionesIva = retencionesIva;
    }

    public List<InvmItem> getItems() {
        return items;
    }

    public void setItems(List<InvmItem> items) {
        this.items = items;
    }

    public List<Rtipcomp> getDocumentos() {
        return documentos;
    }

    public void setDocumentos(List<Rtipcomp> documentos) {
        this.documentos = documentos;
    }

    public List<Rcredtri> getSustentos() {
        return sustentos;
    }

    public void setSustentos(List<Rcredtri> sustentos) {
        this.sustentos = sustentos;
    }

    public List<GenpTalonarios> getTalonarios() {
        return talonarios;
    }

    public void setTalonarios(List<GenpTalonarios> talonarios) {
        this.talonarios = talonarios;
    }
    
}
