/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.comercial.compras;

import com.gma.camilabp.domain.CbpgenrOficina;
import com.gma.camilabp.domain.ComtCabpedsugerido;
import com.gma.camilabp.domain.ComtDetpedsugerido;
import com.gma.camilabp.model.consultas.AgrupacionInventario;
import com.gma.camilabp.model.consultas.InveRepdiasInventario;
import com.gma.camilabp.model.consultas.SugerenciaCompraSecuencia;
import com.gma.camilabp.model.consultas.TipoFiltro;
import com.gma.camilabp.model.logica.SugeridoCompra;
import com.gma.camilabp.sql.QuerysPos;
import com.gma.camilabp.web.lazy.CbpgenrOficinaLazyTest;
import com.gma.camilabp.web.lazy.ComtCabpedsugeridoLazy;
import com.gma.camilabp.web.lazy.CxpmProveedorLazy;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.service.GeneralLocal;
import com.gma.camilabp.web.service.InventarioLocal;
import com.gma.camilabp.web.service.TransaccionalLocal;
import com.gma.camilabp.web.session.ServletSession;
import com.gma.camilabp.web.session.UserSession;
import com.gma.camilabp.web.util.JsfUtil;
import com.gma.camilabp.web.util.Utilidades;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author userdb2
 */
@Named(value = "sugerenciaCompra")
@ViewScoped
public class SugerenciaCompra implements Serializable {

    private static final Logger LOG = Logger.getLogger(SugerenciaCompra.class.getName());
    @Inject
    private ServletSession datosSession;
    @Inject
    private UserSession session;
    @EJB
    private EntityManagerLocal manager;
    @EJB
    private TransaccionalLocal service;
    @EJB
    private GeneralLocal generalService;
    @EJB
    private InventarioLocal inventarioService;
    
    private List<InveRepdiasInventario> sugerencias;
    private Date fechaProcesamiento;    
    private Boolean agrupacion=Boolean.FALSE;    
    private Boolean todosNiveles;
    private List<AgrupacionInventario> agrupaciones;
    protected SugeridoCompra sugeridoCompraModel;
    protected CbpgenrOficinaLazyTest oficinasLazy;
    protected CxpmProveedorLazy proveedoresLazy;
    protected ComtCabpedsugeridoLazy sugeridosLazy;
    protected ComtCabpedsugerido sugerido;
    protected List<ComtDetpedsugerido> detSugerido;
    private Map<String, Object> parametros;
    protected List<TipoFiltro> nivel2List;
    protected List<TipoFiltro> consultaFiltroList;

    @Inject
    private InventarioLocal inventarioLocal;
    private List<TipoFiltro> listAgrupacion;

    @PostConstruct
    public void init() {
        this.nivel2List = null;
        this.agrupaciones = null;
        this.sugeridosLazy = new ComtCabpedsugeridoLazy();
        this.oficinasLazy = new CbpgenrOficinaLazyTest(Utilidades.getIdsOfList(this.session.getOficinas()));        
        fechaProcesamiento = new Date();
        this.listAgrupacion = inventarioLocal.nivelesInventario(this.session.getCbpsegmUsuario().getGenciaCodigo());
        this.sugeridoCompraModel = new SugeridoCompra();
        this.sugeridoCompraModel.setOficina((CbpgenrOficina)Utilidades.getFirstElement(this.session.getOficinas()));
        this.seleccionOficina();
    }
    
    public void busquedaOficina(){
        try{
            this.sugeridoCompraModel.setOficina(this.generalService.consultarOficina(this.sugeridoCompraModel.getSecuenciaOficina()));
            this.seleccionOficina();
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "BuscarProveedor", e);
        }
    }
    
    public void seleccionOficina(){
        this.sugeridoCompraModel.setSecuenciaOficina(this.sugeridoCompraModel.getOficina()!=null?this.sugeridoCompraModel.getOficina().getGenofiSecuencia():null);
        this.sugeridoCompraModel.setNumero(1+this.generalService.numeroDocumento(this.session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia(), this.sugeridoCompraModel.getSecuenciaOficina(), "COM", "PDS"));
    }

    public void buscarProveedorCampo() {
        try {
            if (this.sugeridoCompraModel.getOficina() == null) {
                JsfUtil.messageError(null, "Seleccione una oficina", "");
                return;
            }
            this.proveedoresLazy = new CxpmProveedorLazy(this.sugeridoCompraModel.getOficina().getGenofiSecuencia(), this.session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia());
            this.sugeridoCompraModel.setProveedor(this.generalService.consultarProveedor(this.session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia(), this.sugeridoCompraModel.getOficina().getGenofiSecuencia(), this.sugeridoCompraModel.getSecuenciaProveedor()!=null?this.sugeridoCompraModel.getSecuenciaProveedor().toUpperCase():null));
            this.seleccionarProveedor();
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "BuscarProveedor", e);
        }
    }
    
    public void seleccionarProveedor(){
        try{
            this.sugeridoCompraModel.setSecuenciaProveedor(this.sugeridoCompraModel.getProveedor().getCxpmProveedorPK()!=null?this.sugeridoCompraModel.getProveedor().getCxpmProveedorPK().getCXPPROcodigo():null);
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "seleccionarProveedor", e);
        }
    }

    public void buscarSugerencia() {
        if (this.sugeridoCompraModel.getProveedor()== null) {
            JsfUtil.messageInfo(null, "Debe seleccionar proveedor", "");
            return;
        }
        if (this.sugeridoCompraModel.getBodega() == null) {
            JsfUtil.messageWarning(null, "Debe seleccionar una bodega", "");
            return;
        }
        if (this.sugeridoCompraModel.getDias() == null || this.sugeridoCompraModel.getDias() <1) {
            JsfUtil.messageWarning(null, "Ingrese Dias", "");
            return;
        }
        this.parametros = new HashMap<>();
        this.parametros.put("0", this.sugeridoCompraModel.getProveedor().getCxpmProveedorPK().getGENCIAcodigo());
        this.parametros.put("1", this.sugeridoCompraModel.getProveedor().getCxpmProveedorPK().getGENOFIcodigo());
        this.parametros.put("2", this.sugeridoCompraModel.getProveedor().getCxpmProveedorPK().getCXPPROcodigo());
        this.nivel2List = this.manager.findAllSQL(QuerysPos.filtroSugerido, this.parametros, TipoFiltro.class);
        this.parametros = new HashMap<>();
        parametros.put("0", this.session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia());
        parametros.put("1", this.sugeridoCompraModel.getOficina().getGenofiSecuencia());
        parametros.put("2", this.sugeridoCompraModel.getBodega().getGenbodSecuencia());
        parametros.put("3", this.sugeridoCompraModel.getProveedor().getCxpmProveedorPK().getCXPPROcodigo());
        parametros.put("4", this.fechaProcesamiento);
        parametros.put("5", this.sugeridoCompraModel.getDias());
        parametros.put("6", this.todosNiveles?this.sugeridoCompraModel.getFiltroNivel():null);//TIPO_CONSULTA
        parametros.put("7", this.todosNiveles?this.sugeridoCompraModel.getSecuenciaFiltro():null); //GENENT_codigo
        parametros.put("8", "");//TIPO_CONSULTA1
        parametros.put("9", "");//GENENT_codigo1
        parametros.put("10", this.sugeridoCompraModel.getAgrupadoPor()); //AGRUPADO_POR
        parametros.put("11", 0);
        sugerencias = manager.findAllSQL("exec SP_COMC_CONSPEDIDOSUGERIDO ?,?,?,?,?,?,?,?,?,?,?,?", parametros, InveRepdiasInventario.class);
        this.agrupar(sugerencias);
    }
    
    public void acumularCantidad(int index,InveRepdiasInventario item){
        try{
            Collection listaUpdate = new ArrayList<>();
            item.getTotalSug();
            this.nivel2List.stream().filter(n-> n.getCodigo().trim().equalsIgnoreCase(item.getINVIN2_codin2item_1().trim())).forEach(i -> i.setFuncional(i.getFuncional().add(item.getINVITM_ventapromedioQ())));
            listaUpdate.add("mainForm:contenedor:dtSugArt:"+index+":valTotSug");
            listaUpdate.add("mainForm:contenedor:dtSugArt:"+index+":valSumTotSug");
            listaUpdate.add("mainForm:contenedor:dtResumen");
            JsfUtil.update(listaUpdate);
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Error al registrar sugerencia de compra", e);
        }
    }

    public void aceptar() {
        try {
            if(this.agrupaciones!=null && this.agrupaciones.stream().filter(a-> a.getINVITM_CajasSugeridad()>0).count()>0){
                SugerenciaCompraSecuencia sc = service.registrarSugerencia("G", session.getCbpsegmUsuario(), this.sugeridoCompraModel, agrupaciones);
                if (sc != null) {
                    JsfUtil.messageInfo(null, "Sugerencia de compra procesada correctamente."+sc.getSecuencia()+"-"+sc.getNumero(), "");
                    this.init();
                } else {
                    JsfUtil.messageError(null, "Ocurrio un error al procesar sugerencia de compra.", "");
                }
            }else{
                JsfUtil.messageWarning(null, "Faltan Datos.", "");
            }
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Error al registrar sugerencia de compra", e);
        }
    }
    
    private void agrupar(List<InveRepdiasInventario> sugerencias) {
        try {
            if (sugerencias != null) {
                this.agrupaciones = new ArrayList<>();
                for (InveRepdiasInventario sgr : sugerencias) {
                    //AgrupacionInventario agrupacionInventario = new AgrupacionInventario(sgr.getINVIN1_descripcio());
                    String codigo;
                    String descripcion;
                    if(this.agrupacion){
                        codigo = this.sugeridoCompraModel.getAgrupadoPor().equalsIgnoreCase("N1")?sgr.getINVIN1_codin1item():this.sugeridoCompraModel.getAgrupadoPor().equalsIgnoreCase("N2")?sgr.getINVIN2_codin2item():this.sugeridoCompraModel.getAgrupadoPor().equalsIgnoreCase("N3")?sgr.getINVIN3_codin3item():sgr.getINVIN4_codin4item();
                        descripcion = this.sugeridoCompraModel.getAgrupadoPor().equalsIgnoreCase("N1")?sgr.getINVIN1_descripcio():this.sugeridoCompraModel.getAgrupadoPor().equalsIgnoreCase("N2")?sgr.getINVIN2_descripcio():this.sugeridoCompraModel.getAgrupadoPor().equalsIgnoreCase("N3")?sgr.getINVIN3_descripcio():sgr.getINVIN4_descripcio();
                    }else{
                        codigo = sgr.getINVIN1_codin1item();
                        descripcion = sgr.getINVIN1_descripcio();
                    }
                    AgrupacionInventario agrupacionInventario = new AgrupacionInventario(codigo, descripcion);
                    agrupacionInventario.setAgrupacion(this.sugeridoCompraModel.getAgrupadoPor());
                    if (this.agrupaciones.contains(agrupacionInventario)) {
                        // AGREGAMOS A LA LISTA DEL GRUPO EXISTENTE
                        AgrupacionInventario get = this.agrupaciones.get(this.agrupaciones.indexOf(agrupacionInventario));
                        get.getListadoItems().add(sgr);
                    } else {
                        // CREAMOS UNA LISTA PARA CADA GRUPO
                        List<InveRepdiasInventario> tempAgr = new ArrayList<>();
                        tempAgr.add(sgr);
                        agrupacionInventario.setListadoItems(tempAgr);
                        this.agrupaciones.add(agrupacionInventario);
                    }
                }
                agrupaciones.sort(Comparator.comparing(AgrupacionInventario::getINVIN1_codin1item));
            }
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "agrupar", e);
        }
    }
    
    public void seleccionarSugerido(){
        try{
            this.parametros = new HashMap<>();
            this.parametros.put("comtDetpedsugeridoPK.cOMPDSsecuencia", this.sugerido.getCOMPDSsecuencia());
            this.detSugerido = this.manager.findAllByParameter(ComtDetpedsugerido.class, this.parametros, "comtDetpedsugeridoPK.cOMPDSlinea");
        } catch (Exception e) {
            Logger.getLogger(SugerenciaCompra.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void busquedaFiltro(){
        try{
            String query="";
            this.parametros= new HashMap<>();
            this.parametros.put("0", this.session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia());
            if(this.sugeridoCompraModel.getSecuenciaFiltro()==null || this.sugeridoCompraModel.getSecuenciaFiltro().trim().length()==0){
                query =this.inventarioService.queryNivel(this.sugeridoCompraModel.getFiltroNivel());
                this.consultaFiltroList= this.manager.findAllSQL(query, this.parametros, TipoFiltro.class);
                JsfUtil.update("frmdlgNivel");
                JsfUtil.executeJS("PF('dlgNivel').show();");
            }else{
                switch(this.sugeridoCompraModel.getFiltroNivel()){
                    case "N1":
                        query=QuerysPos.itemFiltroNivel1;
                        break;
                    case "N2":
                        query=QuerysPos.itemFiltroNivel2;
                        break;
                    case "N3":
                        query=QuerysPos.itemFiltroNivel3;
                        break;
                    case "N4":
                        query=QuerysPos.itemFiltroNivel4;
                        break;
                    default:
                        break;
                }
                this.parametros.put("1", this.sugeridoCompraModel.getSecuenciaFiltro());
                this.sugeridoCompraModel.setNivel((TipoFiltro) this.manager.findObject("SQL", query, this.parametros, TipoFiltro.class, Boolean.TRUE));
                if(this.sugeridoCompraModel.getNivel()!=null){
                    this.seleccionFiltro();
                }else{
                    this.sugeridoCompraModel.setSecuenciaFiltro(null);
                    this.sugeridoCompraModel.setDatoFiltro(null);
                }
            }            
        } catch (Exception e) {
            Logger.getLogger(SugerenciaCompra.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    public void seleccionFiltro(){
        this.sugeridoCompraModel.setSecuenciaFiltro(this.sugeridoCompraModel.getNivel().getCodigo());
        this.sugeridoCompraModel.setDatoFiltro(this.sugeridoCompraModel.getNivel().getDato());
        this.buscarSugerencia();
    }
    
    public void generarDocumento(ComtCabpedsugerido sc){
        try{
            if(datosSession!=null){
                datosSession.borrarDatos();
                datosSession.setPath(FacesContext.getCurrentInstance().getExternalContext().getRealPath("//"));
                datosSession.setTieneDatasource(Boolean.FALSE);
                datosSession.agregarParametro("SECUENCIA_SUGERIDO", sc.getCOMPDSsecuencia());
                datosSession.setNombreReporte("compra//sugeridoCompra");
                JsfUtil.redirectNewTab("/CamilaPOS/Documento");
            }
        } catch (Exception e) {
            Logger.getLogger(SugerenciaCompra.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public Date getFechaProcesamiento() {
        return fechaProcesamiento;
    }

    public void setFechaProcesamiento(Date fechaProcesamiento) {
        this.fechaProcesamiento = fechaProcesamiento;
    }

    public Boolean getAgrupacion() {
        return agrupacion;
    }

    public void setAgrupacion(Boolean agrupacion) {
        this.agrupacion = agrupacion;
    }

    public List<InveRepdiasInventario> getSugerencias() {
        return sugerencias;
    }

    public void setSugerencias(List<InveRepdiasInventario> sugerencias) {
        this.sugerencias = sugerencias;
    }

    public List<TipoFiltro> getListAgrupacion() {
        return listAgrupacion;
    }

    public void setListAgrupacion(List<TipoFiltro> listAgrupacion) {
        this.listAgrupacion = listAgrupacion;
    }

    public Boolean getTodosNiveles() {
        return todosNiveles;
    }

    public void setTodosNiveles(Boolean todosNiveles) {
        this.todosNiveles = todosNiveles;
    }

    public List<AgrupacionInventario> getAgrupaciones() {
        return agrupaciones;
    }

    public void setAgrupaciones(List<AgrupacionInventario> agrupaciones) {
        this.agrupaciones = agrupaciones;
    }    

    public SugeridoCompra getSugeridoCompraModel() {
        return sugeridoCompraModel;
    }

    public void setSugeridoCompraModel(SugeridoCompra sugeridoCompraModel) {
        this.sugeridoCompraModel = sugeridoCompraModel;
    }

    public CbpgenrOficinaLazyTest getOficinasLazy() {
        return oficinasLazy;
    }

    public void setOficinasLazy(CbpgenrOficinaLazyTest oficinasLazy) {
        this.oficinasLazy = oficinasLazy;
    }

    public CxpmProveedorLazy getProveedoresLazy() {
        return proveedoresLazy;
    }

    public void setProveedoresLazy(CxpmProveedorLazy proveedoresLazy) {
        this.proveedoresLazy = proveedoresLazy;
    }

    public ComtCabpedsugeridoLazy getSugeridosLazy() {
        return sugeridosLazy;
    }

    public void setSugeridosLazy(ComtCabpedsugeridoLazy sugeridosLazy) {
        this.sugeridosLazy = sugeridosLazy;
    }

    public ComtCabpedsugerido getSugerido() {
        return sugerido;
    }

    public void setSugerido(ComtCabpedsugerido sugerido) {
        this.sugerido = sugerido;
    }

    public List<ComtDetpedsugerido> getDetSugerido() {
        return detSugerido;
    }

    public void setDetSugerido(List<ComtDetpedsugerido> detSugerido) {
        this.detSugerido = detSugerido;
    }

    public List<TipoFiltro> getNivel2List() {
        return nivel2List;
    }

    public void setNivel2List(List<TipoFiltro> nivel2List) {
        this.nivel2List = nivel2List;
    }

    public List<TipoFiltro> getConsultaFiltroList() {
        return consultaFiltroList;
    }

    public void setConsultaFiltroList(List<TipoFiltro> consultaFiltroList) {
        this.consultaFiltroList = consultaFiltroList;
    }

}
