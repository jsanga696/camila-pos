/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.comercial.pos.devoluciones;

import com.gma.camilabp.config.SisVars;
import com.gma.camilabp.domain.FacrMotvdevolu;
import com.gma.camilabp.domain.FactCabventa;
import com.gma.camilabp.domain.FactCabvtacaja;
import com.gma.camilabp.model.consultas.DatosReporte;
import com.gma.camilabp.model.consultas.TemDevolucionFactura;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.service.PuntoVentaLocal;
import com.gma.camilabp.web.session.ServletSession;
import com.gma.camilabp.web.session.UserSession;
import com.gma.camilabp.web.util.JsfUtil;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import org.primefaces.event.SelectEvent;
import com.gma.camilabp.web.util.Utilidades;
import javax.faces.context.FacesContext;

/**
 * Realiza el procesamiento de devolucion
 * <ul>
 * <li>Crea tabla temporal al iniciar la devolucion esto es justo cuando
 * selecciona los articulos</li>
 * </ul>
 *
 * @author userdb2
 */
@Named(value = "registroDevolucionBean")
@ViewScoped
public class RegistroDevolucionBean implements Serializable {
    @EJB
    private EntityManagerLocal entity;
    @EJB
    private PuntoVentaLocal puntoVentaServices;
    @Inject
    private ServletSession datosSession;
    @Inject
    private UserSession session;
    
    private FactCabvtacaja factura;
    private List<TemDevolucionFactura> detalleProductos;
    private List<TemDevolucionFactura> productosSeleccionados;
    private List<FacrMotvdevolu> motvdevolus;
    private String motivoDev;
    private String observaciones;
    private Map<String, Object> parametros;
    private String tablaTemp;

    @PostConstruct
    public void initView() {
        try{
            if (datosSession.getParametros() != null && datosSession.getParametros().containsKey("factura") ) {
                factura = (FactCabvtacaja) datosSession.getParametros().get("factura");
                if (factura != null) {
                    detalleProductos = puntoVentaServices.detalleFactura(factura.getFacvtcSecuenvta());

                    this.tablaTemp = "X_CBPDEVOLUCIONPTO"+session.getCbpsegmUsuario().getSegusuLogin()+factura.getFacvtcSecuenvta();
                    this.productosSeleccionados = this.puntoVentaServices.articulosADevolucion(this.tablaTemp);                    
                } else {
                    JsfUtil.redirectFaces("/faces/pages/pv/transacciones.xhtml");
                }
            } else {
                JsfUtil.redirectFaces("/faces/pages/pv/transacciones.xhtml");
            }
        } catch (Exception e) {
            Logger.getLogger(RegistroDevolucionBean.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public void inicarDevolucion() {
        if (productosSeleccionados == null || productosSeleccionados.isEmpty()) {
            JsfUtil.messageWarning(null, "DEBE SELECCIONAR POR LO MENOS UN ARTÍCULO", "");
            return;
        }
        List<TemDevolucionFactura> vacios = new ArrayList<>(productosSeleccionados.size());
        for (TemDevolucionFactura seleccionado : productosSeleccionados) {
            if (seleccionado.getCantidadFuncional().compareTo(BigDecimal.ZERO) == 0) {
                vacios.add(seleccionado);
            }
        }
        productosSeleccionados.removeAll(vacios);
        
                    
        //registramos los datos temporales del importe 
        this.parametros = new HashMap<>();
        this.parametros.put("0", tablaTemp);
        this.entity.executeProcedure("exec SP_FACP_CRETEMPSAVEDEVOLUCION ?", this.parametros);
        puntoVentaServices.registrarDevolucionTemp(tablaTemp, factura, productosSeleccionados);
        // consultamos catalogo de tipo de devolucion
        parametros = new HashMap<>();
        parametros.put("facmdeEstado", "A");
        parametros.put("facrMotvdevoluPK.GENMODcodigo", "FAC");
        motvdevolus = entity.findAllByParameter(FacrMotvdevolu.class, parametros, null);
        // TOMAMOS EL PRIMER ITEM PARA TENER UNO POR DEFECTO
        if (motvdevolus != null && motvdevolus.size() > -1) {
            motivoDev = motvdevolus.get(0).getFacrMotvdevoluPK().getFACMDEcodigo();
        }
        //Cambiamos de dialogo
        JsfUtil.executeJS("PF('dlgMotDev').show();");
    }

    public void cancelar() {
        if (puntoVentaServices.eliminarDatosTempDevolucion(tablaTemp)) {
            cerrar();
        } else {
            JsfUtil.messageWarning(null, "OCURRIO UN ERROR AL INTENTAR ELIMINAR DATOS TEMPORALES", "");
        }
    }

    public void respaldarDatos(TemDevolucionFactura devolucion) {
        if (devolucion != null) {
            if (devolucion.getFACDVT_cantfuncional().compareTo(devolucion.getCantidadFuncional()) == 1) {
                JsfUtil.messageWarning(null, "La cantidad a devolver no debe ser mayor a la disponible", "");
                devolucion.setFACDVT_cantfuncional(devolucion.getCantidadFuncional());                
            } 
            // BORRAMOS LA TABLA TEMPORAL
            parametros = new HashMap<>();
            parametros.put("0", tablaTemp);
            entity.executeProcedure("exec SP_FACP_CRETEMPSAVEDEVOLUCION ?", parametros);
            productosSeleccionados.set(productosSeleccionados.indexOf(devolucion), devolucion);
            // VOLVEMOS A INSERTAR LOS DATOS DE LA DEVOLUCION
            puntoVentaServices.registrarDevolucionTemp(tablaTemp, factura, productosSeleccionados);
        }
    }

    public void aceptar() {
        try{
            if (productosSeleccionados != null) {
                for (TemDevolucionFactura devolucion : productosSeleccionados) {
                    if (devolucion.getFACDVT_cantfuncional() == null) {
                        JsfUtil.messageWarning(null, "DEBE INGRESAR LA CANTIDAD A IMPORTAR", "");
                        return;
                    }
                    if (devolucion.getFACDVT_cantfuncional().compareTo(BigDecimal.ZERO) == 0) {
                        JsfUtil.messageWarning(null, "DEBE INGRESAR LA CANTIDAD A IMPORTAR", "");
                        return;
                    }
                }
            }
            String nunNotaCredito = puntoVentaServices.registarDevolucion(factura, motivoDev, observaciones, tablaTemp, session.getCbpsegmUsuario());
            if (nunNotaCredito != null) {
                System.out.println("Num Nota Credito " + nunNotaCredito);
                imprimirFactura(nunNotaCredito);
                this.puntoVentaServices.eliminarTabla(tablaTemp);
                JsfUtil.redirectFaces2(SisVars.urlbaseFaces + "pages/pv/transacciones.xhtml");
            } else {
                JsfUtil.messageWarning(null, "OCURRIO UN ERROR AL PROCESAR DEVOLUCION", "");
            }
        } catch (Exception e) {
            Logger.getLogger(RegistroDevolucionBean.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public void imprimirFactura(String nunNotaCredito) {
        try {
            if (datosSession != null) {
                datosSession.setPath(FacesContext.getCurrentInstance().getExternalContext().getRealPath("//"));
                // OBTENEMOS EL REGISTRO DE LADEVOLUCION
                parametros = new HashMap<>();
                parametros.put("fACCVTnumdocumen", Integer.valueOf(nunNotaCredito));
                parametros.put("fACCVTsecdocurel", factura.getFacvtcSecuenvta());
                parametros.put("gENTDOcodigo", "DFA");
                FactCabventa factDev = entity.findObject(FactCabventa.class, parametros);
                datosSession.setParametros(new HashMap());
                datosSession.setTieneDatasource(Boolean.TRUE);
                datosSession.setNombreReporte("devolucion/notaCredito");
                datosSession.agregarParametro("CAJERO", session.getCaja().getSEGUSUcodigo());
                datosSession.agregarParametro("ID_TIENDA", session.getCaja().getGENOFIcodigo());
                datosSession.agregarParametro("CAJA", session.getCodigoCF());
                datosSession.agregarParametro("TURNO", session.getTurno());
                datosSession.agregarParametro("NUM_NOTA_CREDITO", ("NCI-" + Utilidades.completarCadenaConCeros(nunNotaCredito, 6)));
                datosSession.agregarParametro("SALDO", factDev.getFACCVTnetomov());
                datosSession.agregarParametro("NUM_FACTURA", factura.getDocumentoFact());
                datosSession.agregarParametro("NOMBRE", factura.getFACVTCclienombre());
                // AGREGAMOS EL OTRO REPORTE A LA NOTA DE CREDITO
                DatosReporte facturadev = new DatosReporte();
                //PONEMOS LOS DATOS EN LA TABLA TEMPORAL DE LA FACTURA
                parametros = new HashMap<>();
                parametros.put("0", session.getCbpsegmUsuario().getSegusuLogin());
                parametros.put("1", factDev.getFactCabventaPK().getFACCVTnumsecuenc());
                entity.executeProcedure("EXEC POS_REPORTEFACTURADEV ?,?", parametros);
                // LLENAMOS LOS PARAMETROS PARA EL REPORTE QUE SE AGREGARA A LA NOTA DE CREDITO
                Map<String, Object> parametroFactDev = new HashMap<>();
                parametroFactDev.put("COMPANIA", session.getCbpsegmUsuario().getGenciaCodigo());
                parametroFactDev.put("SUBREPORT_DIR", datosSession.getPath() + "/reportes/pos/");
                parametroFactDev.put("FACVTC_SECUENVTA", factDev.getFactCabventaPK().getFACCVTnumsecuenc());
                parametroFactDev.put("USUARIO_REPORTE", session.getCbpsegmUsuario().getSegusuLogin());
                parametroFactDev.put("NUM_FACTURA_APLICADA", factura.getDocumentoFact());
                parametros = new HashMap<>();
                parametros.put("facrMotvdevoluPK.FACMDEcodigo", motivoDev);
                parametros.put("facrMotvdevoluPK.GENMODcodigo", "FAC");
                parametroFactDev.put("MOTIVO_CAMBIO", entity.findObject(FacrMotvdevolu.class, parametros).getFACMDEdescripcio());

                facturadev.setNombreReporte("devolucion/facturaDev");
                facturadev.setParametros(parametroFactDev);
                datosSession.agregarReporte(facturadev);
                // MOSTRAMOS EL DOCUMENTO
                JsfUtil.redirectNewTab("/CamilaPOS/Documento");
            }
        } catch (Exception e) {
            Logger.getLogger(RegistroDevolucionBean.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public void onRowSelect(SelectEvent event) {
        for (TemDevolucionFactura seleccionado : productosSeleccionados) {
            if (seleccionado.getCantidadFuncional().compareTo(BigDecimal.ZERO) == 0) {
                JsfUtil.messageWarning(null, "ITEM SIN DISPONIBILIDAD PARA DEVOLUCION", null);
                productosSeleccionados.remove(seleccionado);
                return;
            }
        }
    }

    public void cerrar() {
        JsfUtil.redirectFaces("/faces/pages/pv/transacciones.xhtml");
    }

    //<editor-fold defaultstate="collapsed" desc="GETTER AND SETTER">
    public FactCabvtacaja getFactura() {
        return factura;
    }

    public void setFactura(FactCabvtacaja factura) {
        this.factura = factura;
    }

    public List<TemDevolucionFactura> getDetalleProductos() {
        return detalleProductos;
    }

    public void setDetalleProductos(List<TemDevolucionFactura> detalleProductos) {
        this.detalleProductos = detalleProductos;
    }

    public List<TemDevolucionFactura> getProductosSeleccionados() {
        return productosSeleccionados;
    }

    public void setProductosSeleccionados(List<TemDevolucionFactura> productosSeleccionados) {
        this.productosSeleccionados = productosSeleccionados;
    }

    public void setMotivoDev(String motivoDev) {
        this.motivoDev = motivoDev;
    }

    public String getMotivoDev() {
        return motivoDev;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public List<FacrMotvdevolu> getMotvdevolus() {
        return motvdevolus;
    }

    public void setMotvdevolus(List<FacrMotvdevolu> motvdevolus) {
        this.motvdevolus = motvdevolus;
    }
//</editor-fold>

}
