/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.servlet;

import com.gma.camilabp.domain.CbpgenrOficina;
import com.gma.camilabp.domain.CbpsegmTurno;
import com.gma.camilabp.domain.CbpsegmUsuario;
import com.gma.camilabp.domain.GenmCaja;
import com.gma.camilabp.model.consultas.Estadistica;
import com.gma.camilabp.model.consultas.FormaPago;
import com.gma.camilabp.model.consultas.Importe;
import com.gma.camilabp.model.logica.Formato;
import com.gma.camilabp.web.session.ServletSession;
import com.gma.camilabp.web.util.Utilidades;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author userdb6
 */
@WebServlet(name = "Cierre", urlPatterns = {"/Cierre"})
public class Cierre extends HttpServlet {
    @Inject
    ServletSession datosSession;
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Cierre</title>");            
            out.println("</head>");
            out.println("<body>"); 
            
            if(datosSession!=null && !datosSession.getParametros().isEmpty()){                
                CbpgenrOficina o = (CbpgenrOficina) datosSession.getParametros().get("OFICINA");
                GenmCaja c  = (GenmCaja) datosSession.getParametros().get("CAJA");
                CbpsegmUsuario u  = (CbpsegmUsuario) datosSession.getParametros().get("USUARIO");
                CbpsegmTurno t = (CbpsegmTurno) datosSession.getParametros().get("TURNO");
                Importe imp = (Importe) datosSession.getParametros().get("IMPORTE");
                Estadistica est = (Estadistica) datosSession.getParametros().get("ESTADISTICA");
                FormaPago pag = (FormaPago) datosSession.getParametros().get("TOTALES");

                String formato = (String) datosSession.getParametros().get("FORMATO");
                Formato f = new Formato();
                f.getObtenrFormato(formato);

                out.println(Utilidades.formatoVisualHtml("--------------------------------", f.getEtiquetaInicial(), true, f.getEtiquetaFinal(), true));
                out.println(Utilidades.formatoVisualHtml("Resumen Transacciones Cierre", f.getEtiquetaInicial(), true, f.getEtiquetaFinal(), true));
                out.println(Utilidades.formatoVisualHtml("--------------------------------", f.getEtiquetaInicial(), true, f.getEtiquetaFinal(), true)); 
                out.println(Utilidades.formatoVisualHtml("Tienda:"+o.getGenofiSecuencia()+"-"+o.getGenofinomComercial(), f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                out.println(Utilidades.formatoVisualHtml("Punto de Venta:"+c.getGENBODcodigo()+"-"+c.getBodega(), f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                out.println(Utilidades.formatoVisualHtml("Fecha:"+Utilidades.getDateHHmm(new Date()), f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), true));
                out.println(Utilidades.formatoVisualHtml("Empleado:"+c.getGENCAJcodigo()+"-"+c.getSEGUSUcodigo()+"["+u.getSegusuNombre()+" "+u.getSegusuApellido()+"]", f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                out.println(Utilidades.formatoVisualHtml("Turno:"+t.getSegturNumero(), f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                out.println(Utilidades.formatoVisualHtml("Inicio:"+Utilidades.getDateHHmm(t.getSegturApertura()), f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                out.println(Utilidades.formatoVisualHtml("Cierre:"+Utilidades.getDateHHmm(t.getSegturCierre()), f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                out.println(Utilidades.formatoVisualHtml("Monto:"+(t.getSegturAsiginicial().add(t.getSegturAdicional())), f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), true));

                out.println("<br/>"+Utilidades.formatoVisualHtml("Importes Totales", f.getEtiquetaInicial(), true, f.getEtiquetaFinal(), true));
                out.println(Utilidades.formatoVisualHtml("--------------------------------", f.getEtiquetaInicial(), true, f.getEtiquetaFinal(), true));
                out.println(Utilidades.formatoVisualHtml(Utilidades.completarCadenaLeft("Ingreso Inicial:"+imp.getIngreso(), f.getLongitud(), '.'),f.getEtiquetaInicial(), true, f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                out.println(Utilidades.formatoVisualHtml(Utilidades.completarCadenaLeft("Ingreso Adicional:"+imp.getIngreso_adicional(), f.getLongitud(), '.'), f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                out.println(Utilidades.formatoVisualHtml(Utilidades.completarCadenaLeft("Gatos:"+imp.getGasto(), f.getLongitud(), '.'), f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), true));

                out.println("<br/>"+Utilidades.formatoVisualHtml("Estadisticas", f.getEtiquetaInicial(), true, f.getEtiquetaFinal(), true));
                out.println(Utilidades.formatoVisualHtml("--------------------------------", f.getEtiquetaInicial(), true, f.getEtiquetaFinal(), true));
                out.println(Utilidades.formatoVisualHtml(Utilidades.completarCadenaLeft("Transacciones:"+est.getTransaciones(), f.getLongitud(), '.'), f.getEtiquetaInicial(), true, f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                out.println(Utilidades.formatoVisualHtml(Utilidades.completarCadenaLeft("Anulaciones:"+est.getAnulada(), f.getLongitud(), '.'), f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                out.println(Utilidades.formatoVisualHtml(Utilidades.completarCadenaLeft("Monto Venta:"+est.getValor_venta(), f.getLongitud(), '.'), f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                out.println(Utilidades.formatoVisualHtml(Utilidades.completarCadenaLeft("Devoluciones:"+imp.getDevolucion(), f.getLongitud(), '.'), f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                out.println(Utilidades.formatoVisualHtml(Utilidades.completarCadenaLeft("Items Eliminados:"+est.getItems_eliminados(), f.getLongitud(), '.'), f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                out.println(Utilidades.formatoVisualHtml(Utilidades.completarCadenaLeft("Coladoradores:"+est.getColaboradores(), f.getLongitud(), '.'), f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                out.println(Utilidades.formatoVisualHtml(Utilidades.completarCadenaLeft("Clientes Registrados:"+est.getRegistrados(), f.getLongitud(), '.'), f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                out.println(Utilidades.formatoVisualHtml(Utilidades.completarCadenaLeft("Clientes en Venta:"+est.getClientes(), f.getLongitud(), '.'), f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), true));

                out.println("<br/>"+Utilidades.formatoVisualHtml("Totales Forma de Pago", f.getEtiquetaInicial(), true, f.getEtiquetaFinal(), true));
                out.println(Utilidades.formatoVisualHtml("--------------------------------", f.getEtiquetaInicial(), true, f.getEtiquetaFinal(), true));
                out.println(Utilidades.formatoVisualHtml(Utilidades.completarCadenaLeft("Efectivo:"+pag.getEfectivo(), f.getLongitud(), '.'), f.getEtiquetaInicial(), true, f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                out.println(Utilidades.formatoVisualHtml(Utilidades.completarCadenaLeft("Cheque:"+pag.getCheque(), f.getLongitud(), '.'), f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                out.println(Utilidades.formatoVisualHtml(Utilidades.completarCadenaLeft("Tarjeta:"+pag.getTarjeta(), f.getLongitud(), '.'), f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                out.println(Utilidades.formatoVisualHtml(Utilidades.completarCadenaLeft("Documento:"+pag.getDocumento(), f.getLongitud(), '.'), f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                out.println(Utilidades.formatoVisualHtml(Utilidades.completarCadenaLeft("Credito:"+pag.getCredito(), f.getLongitud(), '.'), f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), true));

                /*out.println("<br/>"+Utilidades.formatoVisualHtml("Detalle de Cierre", f.getEtiquetaInicial(), true, f.getEtiquetaFinal(), true));
                out.println(Utilidades.formatoVisualHtml("--------------------------------", f.getEtiquetaInicial(), true, f.getEtiquetaFinal(), true));
                out.println(Utilidades.formatoVisualHtml(Utilidades.completarCadenaLeft("Ingresado:"+pag.getEfectivo(), 25, '.'), f.getEtiquetaInicial(), true, f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                out.println(Utilidades.formatoVisualHtml(Utilidades.completarCadenaLeft("Sistema:"+pag.getCheque(), 25, '.'), f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                out.println(Utilidades.formatoVisualHtml(Utilidades.completarCadenaLeft("Diferencia:"+pag.getTarjeta(), 25, '.'), f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), true));*/
            }else{
                out.println("SIN DATOS");
            }
            out.println("</body>");
            out.println("</html>");
            out.close();
        } catch (Exception e){
            Logger.getLogger(Cierre.class.getName()).log(Level.SEVERE, null, e);
        }
        datosSession.borrarDatos();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
