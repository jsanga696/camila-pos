/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.servlet;

import com.gma.camilabp.model.logica.Formato;
import com.gma.camilabp.web.session.ServletSession;
import com.gma.camilabp.web.util.Utilidades;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Acer
 */
@WebServlet(name = "Transaccion", urlPatterns = {"/Transaccion"})
public class Transaccion extends HttpServlet {
    @Inject
    ServletSession datosSession;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            if(datosSession==null || datosSession.getParametros().isEmpty()){
                response.sendRedirect(request.getContextPath()+"/faces/index.xhtml");
            }else{
                String formato = (String) datosSession.getParametros().get("FORMATO");
                Formato f = new Formato();
                f.getObtenrFormato(formato);
                out.println("<!DOCTYPE html>");
                out.println("<html>");
                out.println("<head>");
                out.println("<title>Transaccion</title>");            
                out.println("</head>");
                out.println("<body>");
                this.datosSession.getDataSource().forEach((item) -> {
                    out.println(Utilidades.formatoVisualHtml((String) item, f.getEtiquetaInicial(), true, f.getEtiquetaFinal(), true));
                });
                out.println("</body>");
                out.println("</html>");
            }
            out.close();            
        } catch (Exception e){
            Logger.getLogger(Transaccion.class.getName()).log(Level.SEVERE, null, e);
        }
        datosSession.borrarDatos();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
