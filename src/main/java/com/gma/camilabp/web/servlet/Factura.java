/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.servlet;

import com.gma.camilabp.domain.CbpgenrCompania;
import com.gma.camilabp.model.consultas.RptCobro;
import com.gma.camilabp.model.consultas.RtpFactura;
import com.gma.camilabp.model.logica.Formato;
import com.gma.camilabp.web.session.ServletSession;
import com.gma.camilabp.web.util.Utilidades;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.RoundingMode;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author userdb6
 */
@WebServlet(name = "Factura", urlPatterns = {"/Factura"})
public class Factura extends HttpServlet {
    @Inject
    ServletSession datosSession;
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            if(datosSession==null || datosSession.getParametros().isEmpty()){
                response.sendRedirect(request.getContextPath()+"/faces/index.xhtml");
            }else{
                CbpgenrCompania c = (CbpgenrCompania) datosSession.getParametros().get("COMPANIA");
                List<RtpFactura> fac =  (List<RtpFactura>) datosSession.getParametros().get("FACTURA");
                List<RptCobro> cobros =  (List<RptCobro>) datosSession.getParametros().get("COBROS");
                String formato = (String) datosSession.getParametros().get("FORMATO");
                Formato f = new Formato();
                f.getObtenrFormato(formato);
                out.println("<!DOCTYPE html>");
                out.println("<html>");
                out.println("<head>");
                out.println("<title>Factura</title>");            
                out.println("</head>");
    //            out.println("<body onload='myFunction()'>");

                out.println("<body>");
                out.println(Utilidades.formatoVisualHtml(c.getGenciaNombrecomercial(), f.getEtiquetaInicial(), true, f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                out.println(Utilidades.formatoVisualHtml( c.getGenciaRuc(), f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                out.println(Utilidades.formatoVisualHtml(c.getGenciaTelefono(), f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                out.println(Utilidades.formatoVisualHtml(c.getGenciaDireccion(), f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                out.println(Utilidades.formatoVisualHtml(fac.get(0).getFactura(), f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                out.println(Utilidades.formatoVisualHtml(fac.get(0).getCodigo_cliente(), f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                out.println(Utilidades.formatoVisualHtml(fac.get(0).getCliente_nombre(), f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                out.println(Utilidades.formatoVisualHtml(fac.get(0).getIdentificacion(), f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                out.println(Utilidades.formatoVisualHtml(fac.get(0).getDireccion(), f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                out.println(Utilidades.formatoVisualHtml((fac.get(0).getTelefono()!=null?fac.get(0).getTelefono():""), f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                out.println(Utilidades.formatoVisualHtml(Utilidades.getDateHHmm(fac.get(0).getFecha()), f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                out.println(Utilidades.formatoVisualHtml("Codigo .. Descripcion", f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                out.println(Utilidades.formatoVisualHtml("Cantidad .. P. Unitario .. Valor", f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                for (RtpFactura d : fac) {
                    out.println(Utilidades.formatoVisualHtml(d.getInvitm_codigo()+" .. "+d.getInvitm_nombre(), f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                    out.println(Utilidades.formatoVisualHtml("["+d.getFcdvc_cantcajas().setScale(2, RoundingMode.HALF_UP)+"]"+d.getFacdvc_cantunidad().setScale(2, RoundingMode.HALF_UP)+" .. "+d.getFacdvc_preciomov()+" .. "+d.getFacdvc_subtotmov(), f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                }
                out.println("<br/>"+Utilidades.formatoVisualHtml(Utilidades.completarCadenaLeft("Subtotal Imponible:",f.getLongitud(),f.getCaracter())+fac.get(0).getSubtotamovimp(), f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                out.println(Utilidades.formatoVisualHtml(Utilidades.completarCadenaLeft("Subtotal Exenta:",f.getLongitud(),f.getCaracter())+fac.get(0).getSubtotamovexe(), f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                out.println(Utilidades.formatoVisualHtml(Utilidades.completarCadenaLeft("Subtotal:",f.getLongitud(),f.getCaracter())+fac.get(0).getSubtotamov(), f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                out.println(Utilidades.formatoVisualHtml(Utilidades.completarCadenaLeft("Descuento Imponible:",f.getLongitud(),f.getCaracter())+fac.get(0).getDesctoimp(), f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                out.println(Utilidades.formatoVisualHtml(Utilidades.completarCadenaLeft("Descuento Execta:",f.getLongitud(),f.getCaracter())+fac.get(0).getDesctoexe(), f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                out.println(Utilidades.formatoVisualHtml(Utilidades.completarCadenaLeft("Descuento:",f.getLongitud(),f.getCaracter())+fac.get(0).getDesctomov(), f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                out.println(Utilidades.formatoVisualHtml(Utilidades.completarCadenaLeft("IVA:",f.getLongitud(),f.getCaracter())+fac.get(0).getTotimpumov(), f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), true));
                out.println("<h1>========================</h1>");
                out.println(Utilidades.formatoVisualHtml(". Total:"+fac.get(0).getNetomov(), f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), true));

                for (RptCobro co : cobros) {
                    //$F{tipo}.equals("EF")?$F{valor}.add($P{CAMBIO}):$F{valor}
                    out.println(Utilidades.formatoVisualHtml(co.getTipo()+":"+(co.getTipo().equalsIgnoreCase("EF")?co.getValor().add(fac.get(0).getFacdvc_cambio()).setScale(2, RoundingMode.HALF_UP):co.getValor()), f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                    //$F{entidad}==null?null:($F{entidad}+" Cta."+$F{num_cuenta}+" Num."+$F{num_documento})
                    out.println(Utilidades.formatoVisualHtml((co.getEntidad()==null?"":(co.getEntidad()+"//"+co.getNum_cuenta()+"//"+co.getNum_documento())), f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                }

                out.println(Utilidades.formatoVisualHtml(Utilidades.completarCadenaLeft("Articulos:",f.getLongitud(),f.getCaracter())+fac.get(0).getTotunidpedi(), f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                out.println(Utilidades.formatoVisualHtml(Utilidades.completarCadenaLeft("Cambio:",f.getLongitud(),f.getCaracter())+fac.get(0).getFacdvc_cambio(), f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                out.println(Utilidades.formatoVisualHtml(Utilidades.completarCadenaLeft("Cajero:",f.getLongitud(),f.getCaracter())+fac.get(0).getCajero(), f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                out.println("<h1>========================</h1>");
                out.println(Utilidades.formatoVisualHtml("**GRACIAS POR SU VISITA**", f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                out.println(Utilidades.formatoVisualHtml(".Descarga tu comprobante autorizado, en: "+c.getGenciaEmail()+"</br>", f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                out.println(Utilidades.formatoVisualHtml("--------------------------------------", f.getEtiquetaInicial(), f.getAplicaEtiInicial(), f.getEtiquetaFinal(), f.getAplicaEtiFinal()));
                //out.println((String)datosSession.getParametros().get("CODIGO"));
                /*out.println("<table style='width:50%; font-size:26px;'>");
                out.println("<thead>");
                out.println("<tr>");
                out.println("<th style='text-align: left;'>Codigo</th>");
                out.println("<th style='text-align: left;'>Descripcion</th>");
                out.println("<th style='text-align: left;'>Cantidad</th>");
                out.println("<th style='text-align: left;'>P. Unitario</th>");
                out.println("<th style='text-align: left;'>Valor</th>");
                out.println("</tr>");
                out.println("</thead>");
                out.println("<tbody>");
                for (RtpFactura d : f) {
                    out.println("<tr>");
                    out.println("<td>"+d.getInvitm_codigo()+"</td>");
                    out.println("<td>"+d.getInvitm_nombre()+"</td>");
                    out.println("<td>"+"["+d.getFcdvc_cantcajas().setScale(2, RoundingMode.HALF_UP)+"]"+d.getFacdvc_cantunidad().setScale(2, RoundingMode.HALF_UP)+"</td>");
                    out.println("<td>"+d.getFacdvc_preciomov()+"</td>");
                    out.println("<td>"+d.getFacdvc_subtotmov()+"</td>");
                    out.println("</tr>");
                }
                out.println("</tbody>");
                out.println("</table>");*/
//                out.println("<script>function myFunction() { window.print();  window.close(); } </script>");            
                out.println("</body>");
                out.println("</html>");
            }
            out.close();         
        } catch (Exception e){
            Logger.getLogger(Factura.class.getName()).log(Level.SEVERE, null, e);
        }
        datosSession.borrarDatos();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
