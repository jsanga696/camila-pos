/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.servlet;

import com.gma.camilabp.model.consultas.LogBitacora;
import com.gma.camilabp.web.session.ServletSession;
import com.gma.camilabp.web.util.Utilidades;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author userdb6
 */
@WebServlet(name = "BitacoraLog", urlPatterns = {"/BitacoraLog"})
public class BitacoraLog extends HttpServlet {
    @Inject
    ServletSession datosSession;
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            List<LogBitacora> logs = (List<LogBitacora>) datosSession.getParametros().get("LOGS");
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>BitacoraLog</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1> USUARIO:" + logs.get(0).getSegusu_login()+"-"+logs.get(0).getSegusu_abreviado()+"-"+logs.get(0).getGencaj_codigo() + "</h1>");            
            out.println("<h1> TURNO:" + logs.get(0).getSegtur_numero()+"-"+Utilidades.getDateHHmm(logs.get(0).getSegtur_apertura())+"-- ACCESO:"+logs.get(0).getSegtur_maquina() + "</h1><br/>");
            out.println("<h1> FECHA___ACCION_________DETALLE<br/>");
            for (LogBitacora log : logs) {
                out.println("<h2>" +Utilidades.getDateHHmm(log.getCbpbit_fecha())+"//"+log.getCbpbit_accion()+"//"+log.getCbpbit_detalle()+ "</h2>");
            }
            out.println("</body>");
            out.println("</html>");
        }
        datosSession.borrarDatos();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
