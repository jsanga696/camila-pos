/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.servlet;

import com.gma.camilabp.model.consultas.DatosReporte;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.session.ServletSession;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRPrintPage;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
//import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
//import net.sf.jasperreports.view.JasperViewer;
import org.hibernate.Session;
import org.hibernate.engine.spi.SessionImplementor;

/**
 *
 * @author userdb6
 */
@WebServlet(name = "Documento", urlPatterns = {"/Documento"})
public class Documento extends HttpServlet {
    
    @EJB
    private EntityManagerLocal entity;
    
    @Inject
    private ServletSession datosSession;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, JRException, SQLException {
        Connection conn = null;
        JasperPrint jasperPrint = null;
        OutputStream outStream;
        if (datosSession==null || datosSession.getNombreReporte()==null) {
            return;
        }
        response.setContentType("application/pdf");
        response.addHeader("Content-disposition", "filename=" + datosSession.getNombreReporte() + ".pdf");        

        try {
            Session sess = entity.getSession();
            SessionImplementor sessImpl = (SessionImplementor) sess;
            conn = sessImpl.getJdbcConnectionAccess().obtainConnection();
            request.setCharacterEncoding("UTF-8");
            String ruta;
            ruta = getServletContext().getRealPath("//reportes//pos//" + datosSession.getNombreReporte() + ".jasper");

            if (datosSession.getTieneDatasource()) {
                JRDataSource dataSource = new JRBeanCollectionDataSource(new ArrayList());
                if (datosSession.getDataSource() != null) {
                    dataSource = new JRBeanCollectionDataSource(datosSession.getDataSource());
                }
                jasperPrint = JasperFillManager.fillReport(ruta, datosSession.getParametros(), dataSource);
            } else {
                jasperPrint = JasperFillManager.fillReport(ruta, datosSession.getParametros(), conn);
            }
            
            if(datosSession.getReportesAdicinales()!=null && !datosSession.getReportesAdicinales().isEmpty()){
                for (DatosReporte rep : datosSession.getReportesAdicinales()) {
                    JasperPrint jasperPrint2 = JasperFillManager.fillReport(datosSession.getPath()+"/reportes/pos/"+rep.getNombreReporte()+".jasper",rep.getParametros(),conn);
                    if (jasperPrint2.getPages() != null && jasperPrint2.getPages().size() > 0) {
                        for (JRPrintPage page : jasperPrint2.getPages()) {
                            jasperPrint.addPage(page);
                        }
                    }
                }
            }
            
            outStream = response.getOutputStream();
            JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);
            
            
            //JasperExportManager.exportReportToPdfFile(jasperPrint, "C:\\CamilaReports\\"+datosSession.getNombreReporte() + ".pdf");
            //JasperPrintManager.printReport(jasperPrint, true);
            //JasperViewer v = new JasperViewer(jasperPrint);
            //v.setVisible(true);
            
            outStream.flush();
            outStream.close();
            datosSession.borrarDatos();

        } catch (JRException | IOException | SQLException e) {
            Logger.getLogger(Documento.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JRException|SQLException ex) {
            Logger.getLogger(Documento.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JRException|SQLException ex) {
            Logger.getLogger(Documento.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
