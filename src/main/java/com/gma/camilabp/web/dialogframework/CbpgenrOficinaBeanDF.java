/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.dialogframework;
import com.gma.camilabp.domain.CbpgenrOficina;
import com.gma.camilabp.web.lazy.CbpgenrOficinaLazy;
import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;
import javax.faces.bean.ViewScoped;
import javax.inject.Named;
import org.primefaces.PrimeFaces;
//import org.primefaces.context.RequestContext;
/**
 *
 * @author userdb
 */

@Named(value = "cbpgenrOficinaBeanDF")
@ViewScoped
public class CbpgenrOficinaBeanDF implements Serializable{
     
    private CbpgenrOficinaLazy lazy = new CbpgenrOficinaLazy(true);

    public CbpgenrOficinaLazy getLazy() {
        return lazy;
    }

    public void setLazy(CbpgenrOficinaLazy lazy) {
        this.lazy = lazy;
    }

    public void abrirDF()
    {
        Map<String,Object> options = new HashMap<String, Object>();
        options.put("resizable", true);
        options.put("draggable", true);
        options.put("modal", true);
        options.put("responsive", true);  
        options.put("position", "center center");
        options.put("appendToBody", true);
        options.put("contentWidth", "100%"); 
        options.put("width", "70%");
        PrimeFaces.current().dialog().openDynamic("/pages/df/genrOficinaDF.xhtml", options, null);
    }
    
    public void seleccionado(CbpgenrOficina obj) {
        PrimeFaces.current().dialog().closeDynamic(obj);
    }
   
    
}