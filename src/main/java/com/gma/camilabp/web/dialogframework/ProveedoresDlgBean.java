/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.dialogframework;

import com.gma.camilabp.domain.CxpmProveedor;
import com.gma.camilabp.web.lazy.LazyDataModelGen;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.PrimeFaces;
//import org.primefaces.context.RequestContext;

/**
 *
 * @author userdb2
 */
@Named
@ViewScoped
public class ProveedoresDlgBean implements Serializable{
    
    private LazyDataModelGen<CxpmProveedor> proveedores;
    
    @PostConstruct
    public void init(){
        proveedores = new LazyDataModelGen<>(CxpmProveedor.class);
    }
    
    public void seleccionarProveedor(CxpmProveedor seleccionado){
        PrimeFaces.current().dialog().closeDynamic(seleccionado);
    }

    public LazyDataModelGen<CxpmProveedor> getProveedores() {
        return proveedores;
    }

    public void setProveedores(LazyDataModelGen<CxpmProveedor> proveedores) {
        this.proveedores = proveedores;
    }
    
    
}
