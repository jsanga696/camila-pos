/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.dialogframework;
import com.gma.camilabp.domain.CbpgenrBodega;
import com.gma.camilabp.web.lazy.CbpgenrBodegaLazy;
import com.gma.camilabp.web.util.JsfUtil;
import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;
import javax.faces.bean.ViewScoped;
import javax.inject.Named;
import org.primefaces.PrimeFaces;
/**
 *
 * @author userdb
 */

@Named(value = "cbpgenrBodegaBeanDF")
@ViewScoped
public class CbpgenrBodegaBeanDF implements Serializable{
    
    private CbpgenrBodegaLazy lazy = new CbpgenrBodegaLazy();
    
    public CbpgenrBodegaLazy getLazy() {
        return lazy;
    }

    public void setLazy(CbpgenrBodegaLazy lazy) {
        this.lazy = lazy;
    }

    public void abrirDF(Long idOfic)
    {
        System.out.println("///"+idOfic);
        Map<String,Object> options = new HashMap<String, Object>();
        options.put("resizable", true);
        options.put("draggable", true);
        options.put("modal", true);
        options.put("responsive", true);  
        options.put("position", "center center");
        options.put("appendToBody", true);
        PrimeFaces.current().dialog().openDynamic("/pages/df/genrBodegaDF.xhtml", options, null);
        
    }
    
    public void seleccionado(CbpgenrBodega obj) {
        PrimeFaces.current().dialog().closeDynamic(obj);
    }
   
    
}