/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.util;

import com.gma.camilabo.enums.TipoMovimiento;
import com.gma.camilabp.model.logica.ItemCantidades;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author dbriones
 */
public class Utilidades {
    private static final String EMAIL_REGEX = "^[\\w-]+(\\.[\\w-]+)*@[\\w-]+(\\.[\\w-]+)*(\\.[A-Za-z]{1,})$";
    
    /***
     * comprueba si la cadena es null o vacia con trim()
     * @param cad
     * @return 
     */
    public static Boolean isEmptyTrim(String cad){
        
        if(cad==null) return true;
        if(cad.trim().equals("")) return true;
        return false;
    }
    
    
    /***
     * Ejecuta trim(), si la cadena es vacia devuelve NULL
     * @param cad
     * @return 
     */
    public static String trimEmptyIsNull(String cad){
        
        if(cad.trim().isEmpty() ) return null;
        
        return cad.trim();
    }
    
    
    /***
     * comprueba si ambas cadenas son iguales con trim(), y ademas que ninguna sea null o vacia
     * @param cad1
     * @param cad2
     * @return 
     */
    public static Boolean eqTrim_NotNullOrEmpty(String cad1, String cad2){
        if( cad1==null || cad2==null) return false;
        if( cad1.trim().equals("") 
                || cad2.trim().equals("") ) return false;
        
        if(cad1.trim().equals(cad2.trim())) return true;
        
        return false;
    }
    
    public static String encloseApost(String cad){
        String res = "'"+cad+"'";
        return res;
    }
    public static synchronized boolean validarEmailConExpresion(String email) {
        return validatePattern(EMAIL_REGEX, email);
    }
    public static synchronized boolean validatePattern(final String patron, final String valor) {
        final Pattern patter = Pattern.compile(patron);
        final Matcher matcher = patter.matcher(valor);
        return matcher.matches();
    }
    
    public static int getYear(Date fecha){
        Calendar c= Calendar.getInstance();
        c.setTime(fecha);
        return c.get(Calendar.YEAR);
    }
    
    public static int getMonth(Date fecha){
        Calendar c= Calendar.getInstance();
        c.setTime(fecha);
        return c.get(Calendar.MONTH)+1;
    }
    
    public static String getSubstring(String cadena){
        if(cadena!=null && cadena.length()>0)
            cadena=cadena.substring(0, cadena.length()-1);
        return cadena;
    }
    
    public static Object getFirstElement(List l){
        Object o=null;
        if(l!=null && !l.isEmpty())
            return l.get(0);
        return o;
    }
    
    public static List<Long> getIdsOfList(List l){
        List<Long> ids = new ArrayList<>();
        l.forEach((object) -> {
            ids.add(Long.valueOf(ReflexionEntity.getIdEntity(object).split(":")[1]));
        });
        return ids;
    }
    
    public static List<String> getSecuenciaList(List l){
        List<String> secuencias = new ArrayList<>();
        l.forEach((object) -> {
            Field fiel[] = object.getClass().getDeclaredFields();
            for (Field f : fiel) {
                if(f.getName().endsWith("Secuencia")){
                    try {
                        f.setAccessible(true);
                        secuencias.add((String) f.get(object));
                        break;
                    } catch (IllegalArgumentException | IllegalAccessException ex) {
                        Logger.getLogger(Utilidades.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
        return secuencias;
    }
    
    public static Date getDate(Date d) throws ParseException{
        return new SimpleDateFormat("dd/MM/yyyy").parse(new SimpleDateFormat("dd/MM/yyy").format(d));
    }
    
    public static String getDateHHmm(Date d){
        try{
            return new SimpleDateFormat("dd/MM/yyyy HH:mm").format(d);
        }catch(Exception e){
            Logger.getLogger(Utilidades.class.getName()).log(Level.SEVERE, null, e);
        }
        return "";
        
    }
    
    public static String password(String tipo, String cadena){
        int valor;
        String resultado="";
        if(tipo.equalsIgnoreCase("E")){            
            for (int i = 0; i < cadena.length(); i++) {
                if(cadena.substring(i, i+1).equalsIgnoreCase("Ñ"))
                    resultado=resultado+"@";
                else{
                    if(((i+1)%2)!=0)
                        valor=3;
                    else
                        valor=2;
                    if((valor+(int)cadena.substring(i, i+1).charAt(0))>90)
                        resultado=resultado+ String.valueOf(valor+((int)cadena.substring(i, i+1).charAt(0))-90).trim();
                    else
                        resultado=resultado+ Character.toString((char)(valor+((int)cadena.substring(i, i+1).charAt(0)))).trim();
                }
            }
        }else if(tipo.equalsIgnoreCase("D")){
            for (int i = 0; i < cadena.length(); i++) {
                if(cadena.substring(i, i+1).equalsIgnoreCase("@"))
                    resultado=resultado+"Ñ";
                else{
                    if(((i+1)%2)!=0)
                        valor=3;
                    else
                        valor=2;
                    if((((int)cadena.substring(i, i+1).charAt(0))-valor)<49)
                        resultado=resultado+Character.toString((char)(((int)cadena.substring(i, i+1).charAt(0))-valor+42)).trim();
                    else
                        resultado=resultado+Character.toString((char)(((int)cadena.substring(i, i+1).charAt(0))-valor)).trim();
                }                    
            }
        }
        return resultado;
    }
    
    public static String completarCadenaConCeros(String cadena, Integer longitud) {
        if (cadena == null) {
            return null;
        }
        if (cadena.length() > longitud) {
            return cadena;
        }
        String ceros = "";
        for (int i = 0; i < longitud; i++) {
            ceros = ceros + "0";
        }
        int tamanio = cadena.length();
        ceros = ceros.substring(0, longitud - tamanio);
        cadena = ceros + cadena;
        return cadena;
    }
    
    public static String completarCadenaLeft(String cadena, int longitud, char caracter){
        if(cadena.length()>= longitud)
            return cadena;
        return String.format("%"+longitud+"s", cadena).substring(0, longitud-(cadena).length()).replace(' ', caracter)+cadena;
    }
    
    public static String formatoVisualHtml(String cadena, String etiquetaInicio, Boolean aplicaInicio, String etiquetaFin, Boolean aplicaFin){
        return (aplicaInicio?etiquetaInicio:"")+cadena+(aplicaFin?etiquetaFin:"<br/>");
    }
    
    public static String stringEnum(int i){
        TipoMovimiento[] tp=TipoMovimiento.values();
        for (int j = 0; j < tp.length; j++) {
            if(i==tp[j].getCode())
                return tp[j].getTipo();
        }
        return "";
    }
    
    public static int diferenciaDias(Date desde, Date hasta){
        return (int) ((hasta.getTime()-desde.getTime())/86400000);
    }
    
    public static Boolean subirArchivosRepositorioGeneral(byte[] bytes, String nombreDoc, String directorio) {
        try {
            File fileNew = new File(directorio, nombreDoc);
            OutputStream out = new FileOutputStream(fileNew);
            out.write(bytes);
            out.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    
    public static ItemCantidades procesarCantidades(BigDecimal u, BigDecimal f, BigDecimal c){
        ItemCantidades ic= new ItemCantidades();
        ic.setUnidad(new BigDecimal(u.intValue()%f.intValue()));
        ic.setFactor(f);
        ic.setCaja(c.add(new BigDecimal(u.intValue()/f.intValue())));
        ic.setFuncional(ic.getUnidad().add(ic.getCaja().multiply(ic.getFactor())));
        return ic;
    }
}
