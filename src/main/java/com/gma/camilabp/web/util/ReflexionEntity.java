/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.util;

import java.io.Serializable;
import org.hibernate.collection.internal.PersistentBag;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EmbeddedId;
import javax.persistence.Id;

/**
 *
 * @author userdb6
 */
public class ReflexionEntity implements Serializable{
    
    /**
     * Busca la clave primaria de la Clase lo busca por la anotación {@link Id}
     *
     * @param entry Clase a obtener a el id
     * @return Valor del campo como un {@link String}
     */
    public static String getIdEntity(Object entry) {
        Object entity = null;
        if (entry instanceof PersistentBag) {
            PersistentBag bag = (PersistentBag) entry;
            if (bag.size() > 0) {
                entity = bag.get(0);
            }
        } else {
            entity = entry;
        }

        if (entity != null) {
            String packages = entity.getClass().getPackage().getName().concat(".");
            String name = entity.getClass().getSimpleName();
            Field fiel[] = entity.getClass().getDeclaredFields();
            for (Field f : fiel) {
                if (isAnnotationType(f, Id.class)||isAnnotationType(f, EmbeddedId.class)) {
                    try {
                        f.setAccessible(true);
                        Object value = f.get(entity);
                        if (value != null) {
                            return packages.concat(name).concat(":").concat(value.toString());
                        } else {
                            return null;
                        }
                    } catch (IllegalArgumentException | IllegalAccessException ex) {
                        Logger.getLogger(ReflexionEntity.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
        return null;
    }
    
    public static String getIdEntityNew(Object entry) {
        Object entity = null;
        if (entry instanceof PersistentBag) {
            PersistentBag bag = (PersistentBag) entry;
            if (bag.size() > 0) {
                entity = bag.get(0);
            }
        } else {
            entity = entry;
        }

        if (entity != null) {
            String packages = entity.getClass().getPackage().getName().concat(".");
            String name = entity.getClass().getSimpleName();
            Field fiel[] = entity.getClass().getDeclaredFields();
            for (Field f : fiel) {
                if (isAnnotationType(f, Id.class)) {
                    try {
                        f.setAccessible(true);
                        Object value = f.get(entity);
                        if (value != null) {
                            return packages.concat(name).concat(":").concat(value.toString()).concat(":")
                                    .concat(value.getClass().getName());
                        } else {
                            return null;
                        }
                    } catch (IllegalArgumentException | IllegalAccessException ex) {
                        Logger.getLogger(ReflexionEntity.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
        return null;
    }
    
    public static Object instanceConsString(String type, String value) {
        Object obj = null;
        try {
            Class clazz = Class.forName(type);

            for (java.lang.reflect.Constructor con : clazz.getConstructors()) {
                for (Class parameterType : con.getParameterTypes()) {
                    if (parameterType.equals(String.class)) {
                        if (con.getParameterTypes().length == 1) {
                            obj = con.newInstance(value);
                            break;
                        }
                    }
                }
            }
        } catch (ClassNotFoundException | IllegalAccessException | IllegalArgumentException | InstantiationException
                | SecurityException | InvocationTargetException e) {
            return null;
        }
        return obj;
    }
    
    /**
     * Compara si existe la annotation de tipo class.
     *
     * @param field campoAsoc a verificar la annotation.
     * @param clazz Id.class
     * @return true si esta presente la annotation caso contrario false.
     */
    public static Boolean isAnnotationType(Field field, Class clazz) {
        return field.isAnnotationPresent(clazz);
    }

    public static Object instanceConsString(Class type, String value) {
        Object obj = null;
        try {
            Class clazz = type;
            for (java.lang.reflect.Constructor con : clazz.getConstructors()) {
                for (Class parameterType : con.getParameterTypes()) {
                    if (parameterType.equals(String.class)) {
                        if (con.getParameterTypes().length == 1) {
                            obj = con.newInstance(value);
                            break;
                        }
                    }
                }
            }
        } catch (IllegalAccessException | IllegalArgumentException | InstantiationException | SecurityException
                | InvocationTargetException e) {
            return null;
        }
        return obj;
    }
    
    public static Object getTypeObject(Class clazz, String nameField) {
        try {
            Field temp = null;
            Object ob = null;
            if (nameField.contains(".")) {
                String[] split = nameField.split("\\.");
                for (String sp : split) {
                    temp = clazz.getDeclaredField(sp);
                    temp.setAccessible(true);
                    try {
                        ob = temp.get(clazz.newInstance());
                    } catch (InstantiationException | IllegalAccessException | IllegalArgumentException ie) {
                        System.out.println("Error en getTypeObject: " + ie);
                    }
                    clazz = temp.getType();
                }
                return clazz;
            } else {
                temp = clazz.getDeclaredField(nameField);
                temp.setAccessible(true);
                return temp.getGenericType();
            }
        } catch (NoSuchFieldException | SecurityException e) {
            System.out.println("Error en getTypeObject: " + e);
        }
        return null;
    }
}
