/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.util;

import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.session.UserSession;
import java.util.HashMap;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author userdb5
 */
@Named(value = "getTextView")
@RequestScoped
public class GetTextView {
    
    @Inject
    private UserSession session;
    @EJB
    private EntityManagerLocal entity;
    
    private HashMap m;
    
    @PostConstruct
    public void initView(){        
        m = new HashMap();
        m.put("0", session.getCbpsegmUsuario().getGenciaCodigo().getGenciaSecuencia());
    }
    
    public String findNombreProveedor(String cod, String ofiSec){
        m.put("1", ofiSec);
        m.put("2", cod);
        return "" + entity.findObject("SQL","SELECT s.CXPPRO_razonsocia FROM CXPM_PROVEEDOR s WHERE s.GENCIA_codigo = ? AND "
                    + "GENOFI_codigo = ? AND CXPPRO_codigo = ?", m, null, false);
    }
    
    public String findNombreCliente(String cod, String ofiSec){
        m.put("1", ofiSec);
        m.put("2", cod);
        return "" + entity.findObject("SQL","SELECT s.CXCCLI_razonsocia FROM CXCM_CLIENTE s WHERE s.GENCIA_codigo = ? AND "
                    + "GENOFI_codigo = ? AND CXCCLI_codigo = ?", m, null, false);
    }
    
    public String findNombreEmpleado(Long cod, String ofiSec){
        m.put("1", ofiSec);
        m.put("2", cod.intValue());
        
        return "" + entity.findObject("SQL","SELECT CONCAT(s.NOMEMP_apellidos, ' ', s.NOMEMP_nombres) FROM M_EMPLEADOS s WHERE s.GENCIA_codigo = ? "
                + "AND GENOFI_codigo = ? AND NOMEMP_codigo = ?", m, null, false);
    }
}
