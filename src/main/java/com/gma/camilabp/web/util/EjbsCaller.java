/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.util;

import com.gma.camilabp.web.service.EntityManagerLocal;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;

/**
 *
 * @author userdb6
 */
public class EjbsCaller {
    private static EntityManagerLocal manager = null;

    public static EntityManagerLocal getManager() {
        try {
            manager = (EntityManagerLocal) new InitialContext().lookup("java:module/entityManagerService");
        } catch (Exception e) {
            manager = null;
            Logger.getLogger(EjbsCaller.class.getName()).log(Level.SEVERE, null, e);
        }
        return manager;
    }
    
}
