/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.session;

import com.gma.camilabp.model.consultas.DatosReporte;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author userdb6
 */
@Named
@SessionScoped
public class ServletSession implements Serializable {
    private Boolean tieneDatasource= Boolean.FALSE;
    private List dataSource;
    private Boolean onePagePerSheet = Boolean.FALSE;
    private Boolean fondoBlanco = Boolean.FALSE;
    private String subCarpeta;
    private String nombreReporte;
    private Map parametros = new HashMap();
    private String path;
    private List<DatosReporte> reportesAdicinales;
    
    public void borrarDatos() {
        this.tieneDatasource= Boolean.FALSE;
        this.onePagePerSheet = Boolean.FALSE;
        this.fondoBlanco = Boolean.FALSE;
        this.subCarpeta=null;
        this.nombreReporte=null;
        this.parametros= new HashMap();
        this.reportesAdicinales = new ArrayList<>();
    }
    
    public void agregarParametro(String nombre, Object value) {
        parametros.put(nombre, value);
    }
    
    public void agregarReporte(DatosReporte reporte){
        if(this.reportesAdicinales==null)
            this.reportesAdicinales = new ArrayList<>();
        this.reportesAdicinales.add(reporte);
    }

    public Boolean getTieneDatasource() {
        return tieneDatasource;
    }

    public void setTieneDatasource(Boolean tieneDatasource) {
        this.tieneDatasource = tieneDatasource;
    }

    public List getDataSource() {
        return dataSource;
    }

    public void setDataSource(List dataSource) {
        this.dataSource = dataSource;
    }

    public Boolean getOnePagePerSheet() {
        return onePagePerSheet;
    }

    public void setOnePagePerSheet(Boolean onePagePerSheet) {
        this.onePagePerSheet = onePagePerSheet;
    }

    public Boolean getFondoBlanco() {
        return fondoBlanco;
    }

    public void setFondoBlanco(Boolean fondoBlanco) {
        this.fondoBlanco = fondoBlanco;
    }

    public String getNombreReporte() {
        return nombreReporte;
    }

    public void setNombreReporte(String nombreReporte) {
        this.nombreReporte = nombreReporte;
    }    

    public String getSubCarpeta() {
        return subCarpeta;
    }

    public void setSubCarpeta(String subCarpeta) {
        this.subCarpeta = subCarpeta;
    }

    public Map getParametros() {
        return parametros;
    }

    public void setParametros(Map parametros) {
        this.parametros = parametros;
    }

    public String getPath() {
        return this.path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public List<DatosReporte> getReportesAdicinales() {
        return reportesAdicinales;
    }

    public void setReportesAdicinales(List<DatosReporte> reportesAdicinales) {
        this.reportesAdicinales = reportesAdicinales;
    }
    
}
