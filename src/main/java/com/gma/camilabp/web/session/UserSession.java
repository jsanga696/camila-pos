/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.web.session;


import com.gma.camilabp.domain.CbpgenrOficina;
import com.gma.camilabp.domain.CbpsegmTurno;
import com.gma.camilabp.domain.CbpsegmUsuario;
import com.gma.camilabp.domain.GenmCaja;
import com.gma.camilabp.model.logica.Render;
import com.gma.camilabp.web.util.JsfUtil;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
//import javax.faces.bean.ManagedBean;
//import javax.faces.bean.SessionScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpSession;
import jodd.util.StringUtil;

/**
 *
 * @author dbriones
 */
@Named
@SessionScoped
public class UserSession implements Serializable {
    
    protected String name = "invitado";
    protected CbpsegmUsuario segmUsuario;
    protected String urlSolicitada;
    protected Long companiaId = 1L;
    protected Date fecha;
    protected Boolean registrarCliente= Boolean.FALSE;
    protected String codigoCliente;
    protected CbpsegmTurno turno;
    private List<CbpgenrOficina> oficinas;
    protected GenmCaja caja;
    protected String codigoCF;
    protected Render render;
    protected Boolean formaRv = Boolean.FALSE;
    protected Boolean visualizarItem = Boolean.FALSE;
    protected String passwordSupervisor;
    protected Boolean visualizarDv = Boolean.TRUE;
    protected List<String> accesos;
    protected List<String> pagDefault;
    protected List<String> pagSinTurno;
    protected Boolean validacionStock= Boolean.TRUE;
    protected Boolean controlPassword= Boolean.FALSE;
    
    
    /** Creates a new instance of UserSession */
    public UserSession() {
    }
    
	public Boolean izAnyRol(String... roles){
        if(segmUsuario==null) return false;
        
        Boolean checked = false;
        return checked;
    }
    
    public void checkAnyRoles(List<String> roles){
        if(segmUsuario==null) 
            return;        
        Boolean checked = false;
        if(checked==false){
            System.out.println("DENIED checkAnyRoles()");
            JsfUtil.redirectFaces("/faces/denied.xhtml");
        }
    }
    
    public void logout() {
        this.setCbpsegmUsuario(null);
        this.setName("invitado");
        
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        try {
            ec.redirect(ec.getRequestContextPath() + "/faces/login.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(UserSession.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void redirectInvitado() {
        if (this.segmUsuario != null) {
            HttpServletRequest servletRequest = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            
            if(this.getPagDefault() != null)
                System.out.println("getPagDefault() IS NOT NULL");
            else
                System.out.println("getPagDefault() IS NULL");
            
            if(this.getAccesos() != null) 
                System.out.println("getAccesos() IS NOT NULL");
            else
                System.out.println("getAccesos() IS NULL");
            
            //PAGINAS DEFAULT - PAGINAS ASIGNADAS A USUARIOS
            if(this.getPagDefault()!=null && !this.getPagDefault().contains(servletRequest.getRequestURI().replaceFirst(servletRequest.getContextPath(), "")) 
                    && this.getAccesos()!=null && !this.getAccesos().contains(servletRequest.getRequestURI().replaceFirst(servletRequest.getContextPath(), ""))){
                System.out.println("DENIED redirectInvitado()");
                JsfUtil.redirectFaces("/faces/denied.xhtml");
                return;
            }
            //PRMITIDAS SIN TURNO
            if(this.turno==null){                
                if(this.getPagSinTurno()!=null && !this.getPagSinTurno().contains(servletRequest.getRequestURI().replaceFirst(servletRequest.getContextPath(), "")) ){
                    JsfUtil.redirectFaces("/faces/pages/pv/aperturaTurno.xhtml");
                    return;
                }
            }
            return;
        }

        this.persistReqUrl();
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        try {
            ec.redirect(ec.getRequestContextPath() + "/faces/login.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(UserSession.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void redirectUrlSolicitada(){
        if(this.getUrlSolicitada()!=null){
            try {
                ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
                ec.redirect(this.getUrlSolicitada());
                this.setUrlSolicitada(null);
                
            } catch (IOException ex) {
                Logger.getLogger(UserSession.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else{
            try {
                ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
                ec.redirect(ec.getRequestContextPath() + "/faces/index.xhtml");
            } catch (IOException ex) {
                Logger.getLogger(UserSession.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void persistReqUrl() {
        FacesContext ctx = FacesContext.getCurrentInstance();
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        // devuelve /myaplicacion/faces + /page.xhtml
        HttpServletRequest servletRequest = (HttpServletRequest) ctx.getExternalContext().getRequest();
        // returns something like "/myapplication/faces/home.faces?param=123"
        String fullURI = servletRequest.getRequestURI();
        if(StringUtil.isNotEmpty(servletRequest.getQueryString())) fullURI = fullURI + "?"+ servletRequest.getQueryString();
        //this.setUrlSolicitada(ec.getRequestContextPath() + ctx.getViewRoot().getViewId());
        this.setUrlSolicitada(fullURI);
        System.out.println("Url Solicidata Persistida: " + this.getUrlSolicitada());
    }
    
    /*public void logout(){
        ((HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true)).invalidate();
    }*/
    
    public String obtenerUrl(){
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        HttpServletRequest servletRequest = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        return servletRequest.getRequestURI().replaceFirst(ec.getRequestContextPath(), "");
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CbpsegmUsuario getCbpsegmUsuario() {
        return segmUsuario;
    }

    public void setCbpsegmUsuario(CbpsegmUsuario segmUsuario) {
        this.segmUsuario = segmUsuario;
    }
  
    public String getUrlSolicitada() {
        return urlSolicitada;
    }

    public void setUrlSolicitada(String urlSolicitada) {
        this.urlSolicitada = urlSolicitada;
    }

    public Long getCompaniaId() {
        return companiaId;
    }

    public void setCompaniaId(Long companiaId) {
        this.companiaId = companiaId;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Boolean getRegistrarCliente() {
        return registrarCliente;
    }

    public void setRegistrarCliente(Boolean registrarCliente) {
        this.registrarCliente = registrarCliente;
    }

    public String getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(String codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

    public CbpsegmTurno getTurno() {
        return turno;
    }

    public void setTurno(CbpsegmTurno turno) {
        this.turno = turno;
    }

    public List<CbpgenrOficina> getOficinas() {
        return oficinas;
    }

    public void setOficinas(List<CbpgenrOficina> oficinas) {
        this.oficinas = oficinas;
    }    

    public GenmCaja getCaja() {
        return caja;
    }

    public void setCaja(GenmCaja caja) {
        this.caja = caja;
    }

    public String getCodigoCF() {
        return codigoCF;
    }

    public void setCodigoCF(String codigoCF) {
        this.codigoCF = codigoCF;
    }

    public Render getRender() {
        return render;
    }

    public void setRender(Render render) {
        this.render = render;
    }

    public Boolean getFormaRv() {
        return formaRv;
    }

    public void setFormaRv(Boolean formaRv) {
        this.formaRv = formaRv;
    }

    public Boolean getVisualizarItem() {
        return visualizarItem;
    }

    public void setVisualizarItem(Boolean visualizarItem) {
        this.visualizarItem = visualizarItem;
    }

    public String getPasswordSupervisor() {
        return passwordSupervisor;
    }

    public void setPasswordSupervisor(String passwordSupervisor) {
        this.passwordSupervisor = passwordSupervisor;
    }

    public Boolean getVisualizarDv() {
        return visualizarDv;
    }

    public void setVisualizarDv(Boolean visualizarDv) {
        this.visualizarDv = visualizarDv;
    }

    public List<String> getAccesos() {
        return accesos;
    }

    public void setAccesos(List<String> accesos) {
        this.accesos = accesos;
    }

    public List<String> getPagDefault() {
        return pagDefault;
    }

    public void setPagDefault(List<String> pagDefault) {
        this.pagDefault = pagDefault;
    }

    public List<String> getPagSinTurno() {
        return pagSinTurno;
    }

    public void setPagSinTurno(List<String> pagSinTurno) {
        this.pagSinTurno = pagSinTurno;
    }

    public Boolean getValidacionStock() {
        return validacionStock;
    }

    public void setValidacionStock(Boolean validacionStock) {
        this.validacionStock = validacionStock;
    }

    public Boolean getControlPassword() {
        return controlPassword;
    }

    public void setControlPassword(Boolean controlPassword) {
        this.controlPassword = controlPassword;
    }

}