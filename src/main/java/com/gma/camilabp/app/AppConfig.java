/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.app;

import com.gma.camilabp.config.SisVars;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

/**
 *
 * @author dbriones
 */
@Named
@ApplicationScoped
public class AppConfig {

    public AppConfig() {
    }
    public String getUrlbase() {
        return SisVars.urlbase;
    }

    public String getFacesUrl() {
        return SisVars.facesUrl;
    }

    public String getUrlbaseFaces() {
        return SisVars.urlbaseFaces;
    }

    public String getFormatoArchivos() {
        return SisVars.formatoArchivos;
    }


}
