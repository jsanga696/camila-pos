/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.app;

import com.gma.camilabp.domain.BanmEntidad;
import com.gma.camilabp.domain.BanmEntidadPK;
import com.gma.camilabp.domain.InvmItemPK;
import com.gma.camilabp.web.service.EntityManagerLocal;
import com.gma.camilabp.web.util.ReflexionEntity;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.el.ELException;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import org.apache.commons.lang3.math.NumberUtils;

/**
 *
 * @author Henry Pilo
 */
@FacesConverter("entityConverter")
public class AppConverter implements Converter, Serializable {
    @EJB
    private EntityManagerLocal entity;
    
    @Override
    public Object getAsObject(FacesContext context, UIComponent c, String value) {
        if (value == null || value.isEmpty() == true || value.equals("")) {
            return null;
        }
        try {
            String[] p = value.split(":");
            if (p.length==1 || !NumberUtils.isNumber(p[1])) {
                if(p.length==1)
                    return null;
                if(p[1].contains("InvmItemPK")){
                    String codigoCompania = null, codigoItem = null;
                    String[] lista = p[1].split(",");
                    for (int i = 0; i < lista.length; i++) {
                        String valor=lista[i].split("=")[1].split("]")[0];
                        if(i==0)
                            codigoCompania=valor;
                        else
                            codigoItem=valor;
                    }
                    return entity.find(Class.forName(p[0]), new InvmItemPK(codigoCompania, codigoItem));
                }else if(p[1].contains("BanmEntidadPK")){
                    BanmEntidadPK pk = new BanmEntidadPK();
                    String[] lista = p[1].split(",");
                    for (int i = 0; i < lista.length; i++) {
                        String valor=lista[i].split("=")[1].split("]")[0];
                        System.out.println("VALOR "+i+": "+valor);
                        if(i==0)
                            pk.setGenciaCodigo(valor);
                        else if(i==1)
                            pk.setBanentTipo(valor);
                        else
                            pk.setBanentCodigo(valor);
                    }
                    BanmEntidad be = entity.find(BanmEntidad.class, pk);
                    return be;
                }
                return null;
            }
            return entity.find(Class.forName(p[0]), Long.valueOf(p[1]));
        } catch (NullPointerException | ELException | NumberFormatException e) {
            Logger.getLogger(AppConverter.class.getName()).log(Level.SEVERE, value, e);
        } catch (ClassNotFoundException | ConverterException ex) {
            Logger.getLogger(AppConverter.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent c, Object value) {
        if (value == null) {
            return null;
        }
        return ReflexionEntity.getIdEntity(value);
    }
}
