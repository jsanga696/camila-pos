/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabp.app;

import com.gma.camilabp.domain.converters.*;
import java.io.Serializable;
import javax.faces.bean.ApplicationScoped;
import javax.inject.Named;


/**
 *
 * @author desarrollo2
 */
@Named
@ApplicationScoped
public class AppConv implements Serializable{
   
   /* public GenrPaisConv getGenrPaisConv() {
        return new GenrPaisConv();
    }
    public CxppPlanpagoConv getCxppPlanpagoConv() {
        return new CxppPlanpagoConv();
    }
    */
    public CbpgenrCompaniaConv getCbpgenrCompaniaConv() {
        return new CbpgenrCompaniaConv();
    }
    
    public CbpgenrTablaenumConv getCbpgenrTablaenumConv() {
        return new CbpgenrTablaenumConv();
    }
    
    public BanmEntidadConv getBanmEntidadConv(){
        return new BanmEntidadConv();
    }

}
