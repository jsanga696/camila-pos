/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabo.enums;

/**
 *
 * @author Acer
 */
public enum TipoMovimiento {
    INGRESO         (1,"INGRESO"),
    EGRESO          (2,"EGRESO"),
    TRANSFERENCIA   (3,"TRANSFERENCIA");
    private final int code;
    private final String tipo;

    private TipoMovimiento(int code, String tipo) {
        this.code = code;
        this.tipo = tipo;
    }

    public int getCode() {
        return code;
    }

    public String getTipo() {
        return tipo;
    }
    
}
