/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabo.enums;

/**
 *
 * @author userdb6
 */
public enum TiposSaldosInventario {
    INVENTARIO    ("IN","INVENTARIO"),
    INV_VALORIZADO    ("IV","INV. VALORIZADO"),
    //INV_VAL_POR_OFICINA    ("VO","INV. VAL. POR OFICINA"),
    INV_VAL_PARA_OFICINA    ("IO","INV. VAL. PARA OFICINA"),
    //LISTA_PRECIO    ("LP","LISTA DE PRECIOS"),
    //DIAS_INVENTARIO    ("ID","DIAS INVENTARIO")
    ;
    private final String codigo;
    private final String tipo;

    private TiposSaldosInventario(String codigo, String tipo) {
        this.codigo = codigo;
        this.tipo = tipo;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getTipo() {
        return tipo;
    }

}
