/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabo.enums;

/**
 *
 * @author userdb6
 */
public enum TiposItem {
    TERMINADO   ("T,P","TERMINADO"),
    TODOS       ("TODOS","TODOS"),
    SERVICIO    ("S","SERVICIO"),
    INSUMO      ("I","INSUMO"),
    MATERIA_PRIMA ("M","MATERIA PRIMA"),
    PTO_VENTA   ("P","PTO. VENTA");
    private final String codigo;
    private final String tipo;

    private TiposItem(String codigo, String tipo) {
        this.codigo = codigo;
        this.tipo = tipo;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getTipo() {
        return tipo;
    }

}
