/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gma.camilabo.enums;

/**
 *
 * @author userdb6
 */
public enum ActionsTransaccion {
    REGISTRO_CLIENTE    (1,"REGISTRO CLIENTE"),
    REGISTRO_FACTURA    (2,"REGISTRO FACTURA"),
    FACTURA_STANDBY     (3,"FACTURA STANDBY"),
    CARGA_FACTURA       (4,"CARGA FACTURA STANDBY"),
    ANULAR_FACTURA      (5,"ANULACION FACTURA"),
    REGISTRO_COBRO      (6,"REGISTRO COBRO"),
    REGISTRO_TURNO      (7,"APERTURA TURNO"),
    CIERRE_TURNO        (8,"CIERRE TURNO"),
    MODIFICACION_CLIENTE    (9,"MODIFICACION CLIENTE"),
    ASIGNACION_VALORES      (10,"ASIGANCION DE VALORES"),
    INACTIVAR_ASIGNACION    (11,"INACTIVAR ASIGNACION"),
    ELIMINAR_ITEM           (12,"ELIMINAR ITEM"),
    ERROR                   (13,"ERROR"),
    DEVOLUCION              (14,"DEVOLUCION"),
    INGRESO_EQUIPO              (15,"INGRESO EQUIPO"),
    EDITAR_EQUIPO               (16,"EDITAR EQUIPO"),
    INGRESO_SOLICITUD_EQUIPO    (17,"INGRESO SOLICITUD EQUIPO"),
    CONSIGNAR_EQUIPO            (18,"CONSIGNAR EQUIPO"),
    MANTENIMIENTO_EQUIPO        (19,"MANTENIMIENTO EQUIPO"),
    DEVOLUCION_EQUIPO           (20,"DEVOLUCION EQUIPO"),
    ACEPTAR_SOLICITUD           (21,"ACEPTAR SOLICITUD"),
    RECHAZAR_SOLICITUD          (22,"RECHAZAR SOLICITUD"),
    INGRESO_MODELO              (23,"INGRESO MODELO"),
    EDITAR_MODELO               (24,"EDITAR MODELO"),
    INGRESO_RANGOS_CLIENTE      (25,"INGRESO RANGOS CLIENTE"),
    CARGA_STANDBY               (26,"CARGA DE STANDBY"),
    ELIMINAR_STANDBY            (27,"ELIMINAR STANDBY");
    
    private final int code;
    private final String action;

    private ActionsTransaccion(int code, String action) {
        this.code = code;
        this.action = action;
    }

    public int getCode() {
        return code;
    }

    public String getAction() {
        return action;
    }
    
}
