DEDARROLLO 
-----------
ALTER TABLE CBPSEGM_TURNO ADD segtur_asiginicial MONEY;
ALTER TABLE CBPSEGM_TURNO ADD segtur_adicional MONEY;
ALTER TABLE CBPSEGM_TURNO ADD segtur_egreso MONEY;
ALTER TABLE CBPSEGM_TURNO ADD segtur_valorcierre MONEY;
ALTER TABLE CBPSEGM_TURNO ADD segtur_efectivo MONEY;
ALTER TABLE CBPSEGM_TURNO ADD segtur_cheque MONEY;
ALTER TABLE CBPSEGM_TURNO ADD segtur_tarjeta MONEY;
ALTER TABLE CBPSEGM_TURNO ADD segtur_documento MONEY;
ALTER TABLE CBPSEGM_TURNO ADD segtur_credito MONEY;


ALTER TABLE CBPGENM_DENOMINACION ADD genden_tiporegistro BIGINT;
UPDATE CBPGENM_DENOMINACION SET genden_tiporegistro = 1;

INSERT INTO CBPGENM_DENOMINACION 
SELECT 0,BANENT_codigo, BANENT_nombre, 1.00,2
FROM BANM_ENTIDAD WHERE BANENT_tipo='BC' AND BANENT_estado='A'
AND BANENT_codigo NOT IN (SELECT genden_tipodes FROM CBPGENM_DENOMINACION WHERE genden_tiporegistro=2)
ORDER BY BANENT_codigo


ALTER TABLE CBPGENR_REGISTRO_DENOMINACION ADD genrde_monetario MONEY;
ALTER TABLE CBPGENR_REGISTRO_DENOMINACION ADD genrde_cheque MONEY;
ALTER TABLE CBPGENR_REGISTRO_DENOMINACION ADD genrde_documento MONEY;

ALTER TABLE RPTPAGO ADD documento MONEY;
ALTER TABLE RPTPAGO ADD credito MONEY;


ALTER TABLE CBPSEGM_TURNO ADD cierre_sistema MONEY;

INSERT INTO GENP_PARAMCOMPANIA VALUES ('1000','POS','CIERRE','S:REGISTRAR VALORES CIERRE N:OPCIONAL','N','U','X');
INSERT INTO GENP_PARAMCOMPANIA VALUES ('1000','POS','POSFORMATO','PARA VISUALIZAR LA INFORMACION SERVLET/IMPRESION','<h1>;S;</h1>;S;.;21','U','X');
INSERT INTO GENP_PARAMCOMPANIA VALUES ('1000','POS','POSFORMATO','PARA VISUALIZAR LA INFORMACION SERVLET/IMPRESION','<p>;N;</p>;N; ;0','U','X');
INSERT INTO GENP_PARAMCOMPANIA VALUES ('1000','POS','POSFORMATO30','PARA VISUALIZAR LA INFORMACION SERVLET/IMPRESION','<h1>;S;</h1>;S;.;21','U','X');
INSERT INTO GENP_PARAMCOMPANIA VALUES ('1000','POS','POSFORMATO40','PARA VISUALIZAR LA INFORMACION SERVLET/IMPRESION','<p>;N;</p>;N; ;0','U','X');
INSERT INTO GENP_PARAMCOMPANIA VALUES ('1000','POS','RPTVALORES','RESTRICCION DE VALORES EN REPORTES S:RESTRINGE SOLO USUARIOS SUP; N:SIN RESTRICCION','S','B','X');
INSERT INTO GENP_PARAMCOMPANIA VALUES ('1000','POS','POSTIPOVENTA','VALIDACION PARA INVENTARIO EN VENTA S:VALIDA STOCK N:NO VALIDA STOCK','N','U','X');

ALTER TABLE FACT_DETVTACAJA ADD FACDVT_cantfundev MONEY;


--------------------------------------
ALTER TABLE GENM_CAJA ADD NOMEMP_codigo int;
ALTER TABLE GENM_CAJA ADD GENPGE_codigo VARCHAR(15);
ALTER TABLE GENM_CAJA ADD BANENT_codigo VARCHAR(2);
ALTER TABLE GENM_CAJA ADD BANCTA_codigo VARCHAR(15);

ALTER TABLE CBPSEGM_TURNO ADD mov_ingreso MONEY;
ALTER TABLE CBPSEGM_TURNO ADD mov_egreso MONEY;
ALTER TABLE CBPSEGM_TURNO ADD mov_transferencia MONEY;

ALTER TABLE RPTIMPORTE ADD mov_ingreso MONEY;
ALTER TABLE RPTIMPORTE ADD mov_egreso MONEY;
ALTER TABLE RPTIMPORTE ADD mov_transferencia MONEY;

CREATE TABLE RPTMOVIMIENTO (
usuario_reporte	varchar(5), 
turno	int,
movimiento varchar(50),
valor money)

CREATE TABLE CBPGENR_RECAUDACION(
genrrec_id BIGINT IDENTITY(1,1) NOT NULL,
FACSCA_idsececaja INT,
segtur_numero BIGINT,
GENPGE_codigo VARCHAR(15),
BANENT_codigo VARCHAR(2),
BANCTA_codigo VARCHAR(15),
GENCIA_codigo VARCHAR(4),
BANTMV_codigo VARCHAR(3),
genrrec_codigo VARCHAR(10),
genrrec_ingreso DATETIME,
genrrec_movimiento DATETIME,
genrrec_forma VARCHAR(15),
genrrec_estado VARCHAR(1),
genrrec_referencia VARCHAR(20),
genrrec_papeleta VARCHAR(20),
genrrec_valor MONEY,
genrrec_destinatario VARCHAR(40),
genrrec_descripcion VARCHAR(50),
genrrec_documento VARCHAR(20),
genrrec_conciliacion VARCHAR(20),
genrrec_comprobante VARCHAR(20),
genrrec_tipotransaccion INT,
FACSCA_cajatransfer INT,
CONSTRAINT PK_CBPGENR_RECAUDACION PRIMARY KEY (genrrec_id))


CREATE TABLE CBPGENR_CONTROLTRANS(
genrcon_id BIGINT IDENTITY(1,1) NOT NULL,
genbod_codigo INT,
segusu_codigo_sup INT,
genrcon_fechaactpass DATETIME,
genrcon_periodo BIT,
genrcon_dias BIGINT,
genrcon_transaccion BIT,
genrcon_cantidad BIGINT,
genrcon_actualizar BIT,
genrcon_estado BIT,
segusu_codigo INT,
genrcon_registro DATETIME,
genrcon_actualizacion DATETIME,
CONSTRAINT PK_CBPGENR_CONTROLTRANS PRIMARY KEY (genrcon_id))





ALTER PROCEDURE
----------------
POS_REPORTES_CAJA





/****** Object:  StoredProcedure [dbo].[POS_REPORTEFACTURADEV]    Script Date: 07-Feb-19 4:30:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[POS_REPORTEFACTURADEV] 
@usuario_reporte			VARCHAR(4),
@FACVTC_secuenvta		INT

AS 
DECLARE @secuencia_cxc		INT;
SET NOCOUNT ON
DELETE FROM RPTFACTURAPOS WHERE usuario_reporte=@usuario_reporte;
DELETE FROM RPTCOBROPOS WHERE usuario_reporte=@usuario_reporte;


SELECT
SUM(FACDVC_subtotmov * case when FACVTI_porcentaje > 0 then 1 else 0 end) AS subtotalimp,
SUM(FACDVC_subtotmov * case when FACVTI_porcentaje = 0 OR FACVTI_porcentaje IS NULL then 1 else 0 end) AS subtotalexe,
SUM(FACDVC_desctomov * case when FACVTI_porcentaje > 0 then 1 else 0 end) AS descuentoimp,
SUM(FACDVC_desctomov * case when FACVTI_porcentaje = 0 OR FACVTI_porcentaje IS NULL then 1 else 0 end) AS descuentoexe
INTO #valoresimpexe
FROM FACT_DETVTACAJA d
LEFT JOIN FACT_DETVTAIMPT di ON (d.FACVTC_secuenvta=di.FACCVT_numsecuenc AND d.INVITM_codigo=di.INVITM_codigo)
WHERE d.FACVTC_secuenvta=@FACVTC_secuenvta


INSERT INTO RPTFACTURAPOS 
SELECT @usuario_reporte,@FACVTC_secuenvta,t.GENTAL_codestable+'-'+t.GENTAL_codptoemis+'-'+RIGHT('000000000' + CAST(f.FACCVT_numdocumen AS VARCHAR(9)), 9),
cl.CXCCLI_razonsocia, cl.CXCCLI_ruc_ci, cl.CXCCLI_direccion1, cl.CXCCLI_celular, cl.CXCCLI_fechafreccomp,
0.00,0.00,f.FACCVT_subtotamov, f.FACCVT_totimpumov, 0.00,0.00,f.FACCVT_desctomov, f.FACCVT_netomov, f.FACCVT_Totunidpedi,
d.INVITM_codigo, i.INVITM_nombre, d.FACDVT_cantiunida, d.FACDVT_preciomov, d.FACDVT_desctomov,d.FACDVT_subtotamov,
f.FACCVT_usuaingreg, d.FACDVT_canticaja, ISNULL(dc.gencob_cambio,0.00), f.CXCCLI_codigo
FROM FACT_CABVENTA f 
INNER JOIN CXCM_CLIENTE CL ON (f.CXCCLI_codigo =CL.CXCCLI_codigo AND f.GENCIA_codigo = CL.GENCIA_codigo AND f.GENOFI_codigo = cl.GENOFI_codigo) 
INNER JOIN GENP_TALONARIOS t ON (f.GENTAL_secuencia=t.GENTAL_secuencia)
INNER JOIN FACT_DETVENTA d ON (f.FACCVT_numsecuenc=d.FACCVT_numsecuenc)
INNER JOIN INVM_ITEM i ON (d.GENCIA_codigo=i.GENCIA_codigo AND d.INVITM_codigo=i.INVITM_codigo)
LEFT OUTER JOIN CBPGENR_COBRO dc ON (f.FACCVT_numsecuenc=dc.FACVTC_secuenvta AND dc.gencob_tipo=1)
WHERE f.FACCVT_numsecuenc=@FACVTC_secuenvta
ORDER BY d.FACDVT_linea
/*SELECT @usuario_reporte,@FACVTC_secuenvta,t.GENTAL_codestable+'-'+t.GENTAL_codptoemis+'-'+RIGHT('000000000' + CAST(fv.FACCVT_numdocumen AS VARCHAR(9)), 9),
f.FACVTC_clienombre, f.FACVTC_clierucci, f.FACVTC_cliedirec, f.FACVTC_telefonos, f.FACSCA_fecapertur,
0.00,0.00,f.FACVTC_subtotmov, f.FACVTC_impuesmov, 0.00,0.00,f.FACVTC_desctomov, f.FACVTC_netomov, fv.FACCVT_Totunidpedi,
d.INVITM_codigo, i.INVITM_nombre, d.FACDVC_cantunidad, d.FACDVC_preciomov, d.FACDVC_desctomov,d.FACDVC_subtotmov,
f.GENCAJ_codigo, d.FACDVC_cantcajas, ISNULL(dc.gencob_cambio,0.00), f.CXCCLI_codigo
FROM FACT_CABVTACAJA f
INNER JOIN FACT_CABVENTA fv ON (f.FACVTC_secuenvta=fv.FACCVT_numsecuenc)
INNER JOIN GENP_TALONARIOS t ON (fv.GENTAL_secuencia=t.GENTAL_secuencia)
INNER JOIN FACT_DETVTACAJA d ON (f.FACVTC_secuenvta=d.FACVTC_secuenvta)
INNER JOIN INVM_ITEM i ON (d.GENCIA_codigo=i.GENCIA_codigo AND d.INVITM_codigo=i.INVITM_codigo)
LEFT OUTER JOIN CBPGENR_COBRO dc ON (f.FACVTC_secuenvta=dc.FACVTC_secuenvta AND dc.gencob_tipo=1)
WHERE f.FACVTC_secuenvta=@FACVTC_secuenvta
ORDER BY d.FACDTC_secuelinea*/

UPDATE RPTFACTURAPOS SET subtotamovimp=v.subtotalimp , subtotamovexe=subtotalexe,
desctoimp=descuentoimp, desctoexe=descuentoexe
FROM #valoresimpexe v
WHERE usuario_reporte=@usuario_reporte AND secuencia=@FACVTC_secuenvta

DROP TABLE #valoresimpexe

SELECT @secuencia_cxc=CXCDDE_numsecuen FROM CXCT_DOCUAPLICA 
WHERE CXCDEU_numdocumen = (SELECT FACCVT_numdocumen FROM FACT_CABVENTA WHERE FACCVT_numsecuenc=@FACVTC_secuenvta) AND CXCCCR_numsecuen>0

INSERT INTO RPTCOBROPOS
SELECT @usuario_reporte,cda.CXCDDE_numsecuen, cda.CXCCCR_numsecuen, cdd.GENBAN_tipo, e.BANENT_nombre,
cdd.CXCDPG_cuentacte, cdd.CXCDPG_numerochq, cdd.CXCDPG_valormov, @FACVTC_secuenvta
FROM CXCT_DOCUAPLICA cda
INNER JOIN CXCT_DETDOCUMCR cdd ON (cda.CXCCCR_numsecuen=cdd.CXCDPG_numerosec AND cda.GENCIA_codigo=cdd.GENCIA_codigo AND cda.GENOFI_codigo=cdd.GENOFI_codigo)
LEFT OUTER JOIN BANM_ENTIDAD e ON (cdd.GENBAN_tipo=e.BANENT_tipo AND cdd.GENBAN_codigo=e.BANENT_codigo)
WHERE cda.CXCDDE_numsecuen=@secuencia_cxc

SET NOCOUNT OFF 






------------ MARZO -------------
insert into GENP_PARAMGENERALES values ('1000','012','T','Transferencia','BAN')
insert into GENR_TRANSCONTABLES values ('1000','BAN','00050','000','TRANSACCIONES TRANSFERNCIA PTO VENTA','A','C')
insert into GENR_TRANSCONTABLES values ('1000','BAN','00051','000','TRANSACCIONES TRANSFERNCIA INGR PTO VENTA','A','C')

INSERT INTO BANM_TIPOSMOVIMIENTOS VALUES ('1000','052','TRANSFERENCIA DE FONDOS DE CAJA PTO VTA','CR','E','BAN','1010101500','00050','000','N','N','S','S','A','S')
INSERT INTO BANM_TIPOSMOVIMIENTOS VALUES ('1000','053','TRANSFERENCIA INGR DE FONDOS DE CAJA PTO VTA','CR','I','BAN','1010101500','00051','000','N','N','S','S','A','S')

INSERT INTO GENP_PARAMCOMPANIA VALUES ('1000','POS','MOVINGRESO','CODIGO MOVIMIENTO INGRESO EN TRANSFERENCIA','053','U','X');
ALTER TABLE CBPGENR_RECAUDACION ADD  genrrec_codigo_ing VARCHAR(10);


CREATE TABLE CBPGENR_POLITICABASE(
genpol_codigo BIGINT IDENTITY(1,1) NOT NULL,
gencia_codigo BIGINT,
genofi_codigo BIGINT,
genpol_nombre VARCHAR(50),
genpol_cantidad BIGINT,
genpol_porcentaje NUMERIC(16,4),
genpol_estado BIT,
genpol_creusuario BIGINT,
genpol_crefecha DATETIME,
genpol_ediusuario BIGINT,
genpol_edifecha DATETIME,
CONSTRAINT PK_CBPGENR_POLITICABASE PRIMARY KEY (genpol_codigo))

CREATE TABLE CBPGENR_POLITICAITEM(
genpit_codigo BIGINT IDENTITY(1,1) NOT NULL,
genpol_codigo BIGINT,
INVITM_codigo VARCHAR(15),
CONSTRAINT PK_CBPGENR_POLITICAITEM PRIMARY KEY (genpit_codigo),
CONSTRAINT FK_POLITICABASEITEM FOREIGN KEY (genpol_codigo) REFERENCES CBPGENR_POLITICABASE(genpol_codigo))

CREATE TABLE CBPGENR_POLITICABODEGA(
genpbo_codigo BIGINT IDENTITY(1,1) NOT NULL,
genpol_codigo BIGINT,
genbod_codigo BIGINT,
CONSTRAINT PK_CBPGENR_POLITICABODEGA PRIMARY KEY (genpbo_codigo),
CONSTRAINT FK_POLITICABASEBODEGA FOREIGN KEY (genpol_codigo) REFERENCES CBPGENR_POLITICABASE(genpol_codigo))


ALTER TABLE FACR_SUPERVISOR ADD FACSUP_clave VARCHAR(20);
ALTER TABLE FACR_SUPERVISOR ADD FACSUP_editclave DATETIME;

ALTER TABLE CBPGENR_POLITICABODEGA ADD genpbo_estado bit;
ALTER TABLE CBPGENR_POLITICAITEM ADD genpit_estado bit;
ALTER TABLE CBPGENR_POLITICAITEM ADD genpit_grupo bigint;


ALTER TABLE CBPGENR_COBRO ADD gencob_codent varchar(3);
ALTER TABLE CBPGENR_COBRO ADD gencob_numcuenta varchar(30);
ALTER TABLE CBPGENR_COBRO ADD gencob_numcheque varchar(20);


--ACTUALIZAR
--POS_CONSULTAR_COBRO_TEMPORAL

--[SP_FACT_GENERARFACTURA_ESQUEMA3]

--CREAR
--SP_INVP_POSTEOEGR_LOTE
--[SP_INVP_GENMOVBODEGR_LOTE]

4408
4608
                            
                            
					
e15-c
e15-d

ALTER TABLE CBPGENR_COBRO ADD cxcccr_numsecuen INT;
ALTER TABLE CBPGENR_COBRO ADD cxcccr_numdocumcr VARCHAR(9);

ALTER TABLE INVT_DETMOVBOD ADD INVDMB_numlote VARCHAR(30)
ALTER TABLE INVT_DETMOVBOD ADD INVDMB_fechavenci DATETIME


INSERT INTO GENP_PARAMCOMPANIA VALUES ('1000','POS','DIRECTORIOIMG','RUTA DONDE SE ALMACENAN IMAGENES DE ITEMS','C:\\Users\\Acer\\Desktop\\CamilaPOS\\src\\main\\webapp\\resources\\imageItem','U','X');
INSERT INTO GENP_PARAMCOMPANIA VALUES ('1000','POS','DIRIMGWAR','RUTA DONDE SE ALMACENAN IMAGENES DE ITEMS','C:\\Users\\Acer\\Desktop\\CamilaPOS\\src\\main\\webapp\\resources\\imageItem','U','X');



-- ############### TRANSFERENCIA DE INVENTARIO ##################### --
INSERT INTO GENR_DOCUMENTO VALUES ('1000','1001','INV','TIP','TRANSFERENCIA - INGRESO',0,'I','A','N')
INSERT INTO GENR_DOCUMENTO VALUES ('1000','1001','INV','TEP','TRANSFERENCIA - EGRESO',0,'E','A','N')

INSERT INTO GENR_DOCUMENTO VALUES ('1000','1001','CXC','DCT','DEPOSITO DE TARJETAS',0,'CR','A','N')

UPDATE GENR_DOCUMENTO SET GENTDO_nombre='TRANSFER EGRESO PRODUCCION' WHERE GENTDO_codigo='TEP'




fstrNametableGrabado = "X_INGPRODUC" & gfstrObtengoNombreComputer + gstrCodUsuario    
sqlc = " EXEC SP_INVP_CREARTEMPORAL " & Repr(fstrNametableGrabado)
Para sacar los COSTOS de los Productos es en la tabla INVM_ITEM el campo INVITM_costoprom1

strSql = strSql & "INSERT INTO " & Trim$(fstrNametableGrabado) & "( "
strSql = strSql & " INVCOT_numsecuenc, INVCOT_numdocumen, GENCIA_codigo, GENOFI_codigo, GENMOD_codigo, "
strSql = strSql & " GENTDO_codigo, INVCOT_motvajuste, GENBOD_codigo, INVITM_codigo, INVDOT_precio, "
strSql = strSql & " INVDOT_cantifunci, INVDOT_cantcajas, INVDOT_cantunida, INVDOT_subtotamov )"
strSql = strSql & " SELECT "
If fintboton <> 1 Then
            strSql = strSql & Trim$(LblTransaccion.Caption) & ","   -- Secuencial de Transaccion
            strSql = strSql & Trim$(LblNumeroTra.Caption) & ","    -- Numero de documento 
 Else
            strSql = strSql & " 0, 0, "
End If
strSql = strSql & Repr(gstrCodCompania) & ","    -- Cod COmpania
strSql = strSql & Repr(txtCodOfic.Text) & ","    -- Cod Oficina
strSql = strSql & Repr("INV") & ","       
strSql = strSql & Repr("AJI") & ","   --- INGRESO  TIP;   EGRESO TEP
strSql = strSql & Repr(strTipoAjuste) & ","      -- cod Ajuste
strSql = strSql & Repr(txtCodBodega.Text) & ","    -- cod Bodega
strSql = strSql & Repr(strItem) & ","    -- cod Producto
strSql = strSql & Trim$(dblPrecio) & ","    -- Precio
strSql = strSql & Trim$(dblFactor) & ","  -- factor
strSql = strSql & Trim$(dblCantidad) & ","   ---    (Caja * Factor) + Unidades
strSql = strSql & Trim$(dblCaja) & ","  
strSql = strSql & Trim$(dblunidad) & ","
strSql = strSql & Trim$(dblSubtot)     --- Predio * Cantidad


strSql = " EXEC SP_INVP_GRABAAJUSTEINVENTARIO"
strSql = strSql & "  @NAME_TABLE         = " & Repr(fstrNametableGrabado)
strSql = strSql & ", @GENCIA_codigo      = " & Repr(gstrCodCompania)
strSql = strSql & ", @GENOFI_codigo      = " & Repr(txtCodOfic.Text)
strSql = strSql & ", @INVCOT_fechaemisi  = " & Repr(mskFecha.Text)
strSql = strSql & ", @INVCOT_descricort  = " & Repr(txtConcepto)
strSql = strSql & ", @INVCOT_motvajuste  = " & Repr(gfstrEntreCar(Trim$(cboTipoAjuste.Text), "<", ">"))
strSql = strSql & ", @INVCOT_codusuario  = " & Repr(gstrCodUsuario)








FACTURACION // REPORTES // REIMPRESION DE DOCUMENTOS // INVENTARIO // TRANSFERENCIA DE ING - EGR 
ALTER TABLE CBPGENR_COBRO ADD faccvt_numdocumen int

ALTER TABLE COMT_DETPEDSUGERIDO ADD COMPDS_cantunidadsugeridas NUMERIC(16,4)
ALTER TABLE COMT_DETPEDSUGERIDO ADD COMPDS_cantunidadrecibidas NUMERIC(16,4)
ALTER TABLE COMT_DETPEDSUGERIDO ADD COMPDS_cantfuncionalsugeridas  NUMERIC(16,4)
ALTER TABLE COMT_DETPEDSUGERIDO ADD COMPDS_cantfuncionalrecibidas  NUMERIC(16,4)
ALTER TABLE COMT_DETPEDSUGERIDO ADD COMPDS_unidembalasugeridas  NUMERIC(16,4)
ALTER TABLE COMT_DETPEDSUGERIDO ADD COMPDS_unidembalarecibidas  NUMERIC(16,4)


ALTER TABLE CBPSEGM_OPCION ADD segopc_menu bit;
ALTER TABLE CBPSEGM_OPCION ADD segopc_acceso bit;
ALTER TABLE CBPSEGM_OPCION ADD segopc_turno bit;
UPDATE CBPSEGM_OPCION SET segopc_menu=0, segopc_acceso=0, segopc_turno=0

--ACTAULIZAR SP_COMC_CONSPEDIDOSUGERIDO AL FINAL LOS NOMBRES DE LAS COLUMNAS 

