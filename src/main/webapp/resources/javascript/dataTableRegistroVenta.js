/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if (PrimeFaces.widget.DataTable) {
    PrimeFaces.widget.DataTable = PrimeFaces.widget.DataTable.extend({
        showCellEditor: function(c) {
            this.incellClick = true;
            var cell = null,
            $this = this;
            if(c) {
                cell = c;
                //remove contextmenu selection highlight
                if(this.contextMenuCell) {
                    this.contextMenuCell.parent().removeClass('ui-state-highlight');
                }
            }
            else {
                cell = this.contextMenuCell;
            }
            if(this.currentCell) {
                $this.saveCell(this.currentCell);
            }
            this.currentCell = cell;
            var cellEditor = cell.children('div.ui-cell-editor'),
            displayContainer = cellEditor.children('div.ui-cell-editor-output'),
            inputContainer = cellEditor.children('div.ui-cell-editor-input'),
            inputs = inputContainer.find(':input:enabled'),
            multi = inputs.length > 1;

            cell.addClass('ui-state-highlight ui-cell-editing');
            displayContainer.hide();
            inputContainer.show();
            inputs.eq(0).focus().select();
            //metadata
            if(multi) {
                var oldValues = [];
                for(var i = 0; i < inputs.length; i++) {
                    oldValues.push(inputs.eq(i).val());
                }
                cell.data('multi-edit', true);
                cell.data('old-value', oldValues);
            } 
            else {
                cell.data('multi-edit', false);
                cell.data('old-value', inputs.eq(0).val());
            }
            //bind events on demand
            if(!cell.data('edit-events-bound')) {
                cell.data('edit-events-bound', true);
                inputs.on('keydown.datatable-cell', function(e) {
                    var keyCode = $.ui.keyCode, shiftKey = e.shiftKey, key = e.which, input = $(this);
                    var oldvalue=cell.data('old-value');
                    if(cell.find('div.ui-cell-editor-input :input:enabled').eq(0).val()==0){
                        cell.find('div.ui-cell-editor-input :input:enabled').eq(0).val(cell.data('old-value'));
                    }
                    if(key === keyCode.ENTER || key === keyCode.NUMPAD_ENTER) {
                        $this.saveCell(cell);
                        $this.tabCell(cell, !shiftKey);                            
                        $this.unselectAllRows();
                        $this.selectRow($(e.target).closest('tr'));
                        if(oldvalue!=cell.find('div.ui-cell-editor-input :input:enabled').eq(0).val())
                            actualizarItem();
                        e.preventDefault();
                    }else if(key === keyCode.TAB) {
                        if(multi) {
                            var focusIndex = shiftKey ? input.index() - 1 : input.index() + 1;
                            if(focusIndex < 0 || (focusIndex === inputs.length)) {
                                $this.tabCell(cell, !shiftKey);                                
                            } else {
                                inputs.eq(focusIndex).focus();
                            }
                        }else {
                            $this.tabCell(cell, !shiftKey);
                            $this.unselectAllRows();
                            $this.selectRow($(e.target).closest('tr'));
                            if(oldvalue!=cell.find('div.ui-cell-editor-input :input:enabled').eq(0).val())
                                actualizarItem();
                        }
                        e.preventDefault();
                    }
                }).on('focus.datatable-cell click.datatable-cell', function(e) {$this.currentCell = cell;});
            }        
        },
        tabCell: function(cell, forward) {
            var targetCell = forward ? cell.next('td.ui-editable-column') : cell.prev('td.ui-editable-column');
            if(targetCell.length == 0) {
                var tabRow = forward ? cell.parent().next() : cell.parent().prev();
                targetCell = forward ? tabRow.children('td.ui-editable-column:first') : tabRow.children('td.ui-editable-column:last');
            }
            this.showCellEditor(targetCell);
        }
    });
}