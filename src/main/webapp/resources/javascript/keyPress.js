/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//document.getElementById('formId:inputId').focus();

$(document).ready( function() {    
    $(':focusable:input').each(function(){
        if(typeof($(this).attr('type'))  === "undefined") {
        }else{
            if($(this).attr('type').endsWith('text')){
                $(this).focus().select();
                return false;
            }
        }
    });
    focusById('codBarraCabeceraProducto');
  });
  
  function focusById(evt){      
    $(':focusable:input').each(function(){
    if(typeof($(this).attr('id'))  === "undefined") {
    }else{
        if($(this).attr('id').endsWith(evt)){
            $(this).focus().select();
            return false;
        }
    }});
  }
  
  function focusButtonById(evt){      
    $(':focusable:button').each(function(){
    if(typeof($(this).attr('id'))  === "undefined") {
    }else{
        if($(this).attr('id').endsWith(evt)){
            $(this).focus().select();
            return false;
        }
    }});
  }
  
jQuery.extend(jQuery.expr[':'], {
    focusable: function (el, index, selector) {
        return $(el).is('a, button, :input, [tabindex]');
    }
});

$(document).on('keydown',function(e){
    var id =$(this).attr('id');
    //F7->118(POLITICAS);
    //F8->119(PREPARAR PAGO);
    //F9->220(PAGO EFECTIVO);
    if (e.which == 118) {
        politicas();
    }else if (e.which == 119) {
        prepararPago();
    }else if (e.which == 120) {
        realizarPagoDirecto();
    }    
});

$(document).on('keypress', 'tbody',function(e){ 
    var id =$(this).attr('id');
    if (e.which == 17) {//CTRL+Q->17(ELIMINAR)        
        if(id.endsWith("mainForm:formVentaDetalle_data")){
            eliminarItem();
        }   
    }
});

$(document).on('keypress', 'input,select', function (e) {
    var id =$(this).attr('id');
    if (e.which == 2) {//CTRL+B->2(BUSCAR)        
        if(id.endsWith("CabeceraCliente")){
            buscarCliente();
        }else if(id.endsWith("CabeceraProducto")){
            buscarItem();
        }else{
            
        }      
    }else if (e.which == 17) {//CTRL+Q->17(LIMPIAR)
        if(id.endsWith("CabeceraCliente")){
            limpiarCliente();
        }else if(id.endsWith("CabeceraProducto")){
            limpiarItem();
        }else{
            
        }
    }else if (e.which == 9) {//CTRL+I->9(AGREGAR)
        if(id.endsWith("CabeceraProducto")){
            agregarItem();
        }else{
            
        }
    }else if (e.which == 25) {//CTRL+Y->25(REGISTRAR)
        if(id.endsWith("CabeceraCliente")){
            registrarCliente();
        }else{
            
        }
    }
    
    if (e.which == 13) {
        e.preventDefault();
        // Get all focusable elements on the page
        /*if(id.endsWith("valorEfectivo")){
            calculoEfectivo();
        }*/
        if(id.endsWith(":cantidadCabeceraProducto")){
            agregarItem();
            focusById('codBarraCabeceraProducto');
            return false;
        }else if(id.endsWith(":codBarraCabeceraProducto")){
            buscarItem();
            agregarItem();
            return false;
        }else if(id.endsWith(":addPagoCheque")){
            calcularValoresPagoCheque();
            return false;
        }else if(id.endsWith(":addPagoTarjeta")){
            calcularValoresPagoTarjeta();
            return false;
        }else if(id.endsWith(":secuenciaOficina")){
            busquedaOficina();
            return false;
        }else if(id.endsWith(":secuenciaBodega")){
            busquedaBodega();
            return false;
        }else if(id.endsWith("CabeceraCliente")){
            buscarCliente();
            return false;
        }else if(id.endsWith(":codigoCabeceraProducto")||id.endsWith(":descripcionCabeceraProducto")){
            buscarItem();
            focusById('cantidadCabeceraProducto');
            return false;
        }else if(id.endsWith(":codigoBusquedaFiltro")){
            busquedaFiltro();
            return false;
        }else if(id.endsWith(":secuenciaCtaBanco")){
            busquedaCtaBanco();
            return false;
        }else if(id.endsWith(":secuenciaProveedor")){
            busquedaProveedor();
            return false;
        }else if(id.endsWith(":secuenciaTransferencia")){
            busquedaTransferencia();
            return false;
        }else if(id.endsWith(":valorEfectivo")){
            focusButtonById(':btnProcesar');
        }else if(id.endsWith(":marca")){
            busquedaMarcaModelo();
            return false;
        }else if(id.endsWith(":modelo")){
            busquedaMarcaModelo();
            return false;
        }else if(id.endsWith(":placa")){
            busquedaEquipo();
            return false;
        }else if(id.endsWith(":solicitante")){
            busquedaSolicitante();
            return false;
        }else if(id.endsWith(":secuenciaLPrecios")){
            busquedaListaPrecios();
            return false;
        }
        
        var $canfocus = $(':focusable:input');
        var index = $canfocus.index(this) + 1;
        if (index >= $canfocus.length)
            index = 0;
        $canfocus.eq(index).focus().select();
    }
});